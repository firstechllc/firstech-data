﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="FirstechData.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>About</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="entypo-info"></i>
                <span>About</span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
        </div>
    </div>

</asp:Content>
