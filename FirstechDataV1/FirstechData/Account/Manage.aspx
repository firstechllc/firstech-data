﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="FirstechData.Account.Manage" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="#"><span class="entypo-home"></span></a></li>
        <li><a href="#">Manage Account</a></li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <h3 class="box-title"><i class="entypo-login"></i>
                <span>Change your account</span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <div>
                <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                    <p class="text-success"><%: SuccessMessage %></p>
                </asp:PlaceHolder>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p class="text-danger"><asp:Literal runat="server" ID="ErrorMessage" /></p>
                    <div class="form-horizontal">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 control-label">Email</asp:Label>
                            <div class="col-md-10">
                                <asp:Label runat="server" ID="Email" CssClass="form-control" Width="280" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The First Name field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"  Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Last Name field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 control-label">Store Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="StoreName" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Address field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="City" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The City field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="State" CssClass="form-control" MaxLength="20" Width="100"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="State" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The State field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Zip" CssClass="col-md-2 control-label">Zip</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Zip" CssClass="form-control" MaxLength="20" Width="100"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Zip" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The State field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Phone" CssClass="col-md-2 control-label">Phone</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Phone" CssClass="form-control" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Phone" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The State field is required." />
                            </div>
                        </div>

                        <dl class="dl-horizontal">
                            <dt>Password:</dt>
                            <dd>
                                <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Change]" Visible="false" ID="ChangePassword" runat="server" />
                                <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Create]" Visible="false" ID="CreatePassword" runat="server" />
                            </dd>
                            <dt>External Logins:</dt>
                            <dd><%: LoginsCount %>
                                <asp:HyperLink NavigateUrl="/Account/ManageLogins" Text="[Manage]" runat="server" />

                            </dd>
                            <%--
                                Phone Numbers can used as a second factor of verification in a two-factor authentication system.
                                See <a href="http://go.microsoft.com/fwlink/?LinkId=403804">this article</a>
                                for details on setting up this ASP.NET application to support two-factor authentication using SMS.
                                Uncomment the following blocks after you have set up two-factor authentication
                            --%>
                            <%--
                            <dt>Phone Number:</dt>
                            <% if (HasPhoneNumber)
                               { %>
                            <dd>
                                <asp:HyperLink NavigateUrl="/Account/AddPhoneNumber" runat="server" Text="[Add]" />
                            </dd>
                            <% }
                               else
                               { %>
                            <dd>
                                <asp:Label Text="" ID="PhoneNumber" runat="server" />
                                <asp:HyperLink NavigateUrl="/Account/AddPhoneNumber" runat="server" Text="[Change]" /> &nbsp;|&nbsp;
                                <asp:LinkButton Text="[Remove]" OnClick="RemovePhone_Click" runat="server" />
                            </dd>
                            <% } %>
                            --%>

                                <%--
                            <dt>Two-Factor Authentication:</dt>
                            <dd>
                                <p>
                                    There are no two-factor authentication providers configured. See <a href="http://go.microsoft.com/fwlink/?LinkId=403804">this article</a>
                                    for details on setting up this ASP.NET application to support two-factor authentication.
                                </p>
                                <% if (TwoFactorEnabled)
                                  { %> 
                                Enabled
                                <asp:LinkButton Text="[Disable]" runat="server" CommandArgument="false" OnClick="TwoFactorDisable_Click" />
                                <% }
                                  else
                                  { %> 
                                Disabled
                                <asp:LinkButton Text="[Enable]" CommandArgument="true" OnClick="TwoFactorEnable_Click" runat="server" />
                                <% } %>
                            </dd>
                                --%>
                        </dl>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" OnClick="UpdateUser_Click" Text="Update" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
