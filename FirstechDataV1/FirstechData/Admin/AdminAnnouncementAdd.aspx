﻿<%@ Page Title="Announcement" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminAnnouncementAdd.aspx.cs" Inherits="FirstechData.Admin.AdminAnnouncementAdd" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Announcement</li>
        <li>Add Announcement</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span><asp:Label runat="server" ID="TitleLabel"></asp:Label></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="ErrorLabel" />
                    </p>
                    <div class="form-horizontal">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="TitleTxt" CssClass="col-md-2 control-label">Title</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="TitleTxt" CssClass="form-control row" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTxt" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Title field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="StartDate" CssClass="col-md-2 control-label">Start Date</asp:Label>
                            <div class="col-md-2">
                                <asp:TextBox runat="server" ID="StartDate" ClientIDMode="Static" CssClass="form-control row date form_datetime" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="StartDate" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Start Date field is required." />
                                <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ValidationExpression="[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]" 
                                    ControlToValidate="StartDate" ErrorMessage="Invalid Start Date" />
                            </div>
                            <asp:Label runat="server" AssociatedControlID="EndDate" CssClass="col-md-2 control-label">End Date</asp:Label>
                            <div class="col-md-2">
                                <asp:TextBox runat="server" ID="EndDate" ClientIDMode="Static" CssClass="form-control row date form_datetime" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="EndDate" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The End Date field is required." />
                                <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ValidationExpression="[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]" 
                                    ControlToValidate="EndDate" ErrorMessage="Invalid End Date" />
                            </div>
                            <asp:Label runat="server" AssociatedControlID="EndDate" CssClass="col-md-2 control-label">Active</asp:Label>
                            <div class="col-md-2">
                                <asp:CheckBox runat="server" ID="ActiveChk" CssClass="form-control row no-border" Checked="true"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="editor" CssClass="col-md-2 control-label">Announcement</asp:Label>
                            <div class="col-md-10">
                                <CKEditor:CKEditorControl ID="editor" runat="server" Height="400" BasePath="/Scripts/js/ckeditor">
                                </CKEditor:CKEditorControl>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="editor" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Announcement field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <asp:Button runat="server" ID="SaveButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveButton_Click"></asp:Button>
                                <asp:Button runat="server" ID="CancelButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelButton_Click" CausesValidation="false"></asp:Button>
                            </div>
                            <div class="col-md-2 pull-right right-align">
                                <asp:Button runat="server" ID="DeleteButton" CssClass="button tiny bg-black radius" Text="Delete" OnClick="DeleteButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Announcement?');" CausesValidation="false"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end .timeline -->
            </div>
            <asp:HiddenField runat="server" ID="AnnouncementIdHidden" />
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#<%=StartDate.ClientID%>").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        format: 'mm/dd/yyyy'
                    }).on('changeDate', function(ev) { $(this).datepicker('hide'); });
                    $("#<%=EndDate.ClientID%>").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        format: 'mm/dd/yyyy'
                    }).on('changeDate', function (ev) { $(this).datepicker('hide'); });
                });
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
