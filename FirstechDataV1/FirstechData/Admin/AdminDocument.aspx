﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminDocument.aspx.cs" Inherits="FirstechData.Admin.AdminDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <div class="row">
                <div class="large-6 columns">
                    <h4 class="box-title"><i class="fontello-doc"></i><span>Document</span></h4>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
        </div>
        <div class="box-body " style="display: block;">
            <div class="row">
                <div class="large-12 columns">
                    <asp:Label runat="server" ID="CurrentFolder"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddButton" Text="(+) Add Document" OnClick="AddButton_Click"></asp:LinkButton>
                </div>
            </div>
            <asp:Panel runat="server" ID="AddDocumentPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddDocumentTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="DocumentTypeList" CssClass="col-md-2 control-label">Type</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:RadioButtonList runat="server" ID="DocumentTypeList" RepeatDirection="Horizontal" BorderWidth="0px" AutoPostBack="true" OnSelectedIndexChanged="DocumentTypeList_SelectedIndexChanged">
                                            <asp:ListItem Text="Document" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Folder" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <asp:Panel runat="server" ID="FolderPanel" Visible="false">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="FolderName" CssClass="col-md-2 control-label">Folder Name</asp:Label>
                                        <div class="large-6 columns">
                                            <asp:TextBox runat="server" ID="FolderName"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel runat="server" ID="DocumentPanel">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="FolderName" CssClass="col-md-2 control-label">Document</asp:Label>
                                        <div class="large-6 columns">
                                            <asp:Label runat="server" ID="ExistingDocument"></asp:Label>
                                            <asp:FileUpload runat="server" ID="AttachFile" />
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="ThisDocumentIdHidden" />
                                        <asp:Button runat="server" ID="AddDocumentButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddDocumentButton_Click" />
                                        <asp:Button runat="server" ID="CancelDocumentButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelDocumentButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="row">
                <div class="large-12 columns">
                    <asp:GridView ID="DocumentList" runat="server" AutoGenerateColumns="false" CssClass="footable" OnRowDataBound="DocumentList_RowDataBound"
                        OnRowDeleting="DocumentList_RowDeleting" OnSelectedIndexChanged="DocumentList_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="DocumentId" HeaderText="ID" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" ItemStyle-Width="80px" />
                            <asp:BoundField DataField="DocumentTypeStr" HeaderText="Type" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" ItemStyle-Width="150px" />
                            <asp:TemplateField HeaderText="Document">
                                <EditItemTemplate>
                                    <asp:HiddenField ID="DocumentIdHidden" runat="server" Value='<%# Bind("DocumentId") %>' />
                                    <asp:TextBox ID="DocumentNameTxt" runat="server" Text='<%# Bind("DocumentName") %>' Width="150px"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="DocumentIdHidden2" runat="server" Value='<%# Bind("DocumentId") %>' />
                                    <asp:HiddenField ID="DocumentType" runat="server" Value='<%# Bind("DocumentType") %>' />
                                    <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                                    <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                                    <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                                ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:CommandField>
                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:CommandField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField runat="server" ID="DocumentId" />
                    <asp:HiddenField runat="server" ID="SaveFolderHidden" />
                    <asp:HiddenField runat="server" ID="FullPathHidden" />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[id*=DocumentList]').footable();
        });
    </script>
</asp:Content>
