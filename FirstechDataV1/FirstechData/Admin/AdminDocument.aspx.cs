﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["Id"] != null && Request["Id"] != "")
                    DocumentId.Value = Request["Id"].ToString();
                else
                    DocumentId.Value = "0";

                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                FullPathHidden.Value = HttpContext.Current.Server.MapPath(SaveFolderHidden.Value);
                ShowDocument();
            }
        }

        private void ShowDocument()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (DocumentId.Value == "0")
                {
                    CurrentFolder.Text = "<a href='/Admin/AdminDocument'>/</a>";
                }
                else
                {
                    string Path = "";
                    FilePath(Con, int.Parse(DocumentId.Value), ref Path);
                    CurrentFolder.Text = Path;
                }

                string sql = "select DocumentId, ParentDocumentId, DocumentName, ";
                sql += "DocumentType, DocumentTypeStr = case when DocumentType = 0 then 'Folder' else 'File' end, AttachFile ";
                sql += "from dbo.Document ";
                sql += "where ParentDocumentId = @ParentDocumentId ";
                sql += "order by DocumentType, DocumentName";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@ParentDocumentId", SqlDbType.Int).Value = int.Parse(DocumentId.Value);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                DocumentList.DataSource = ds;
                DocumentList.DataBind();

                if (DocumentList != null && DocumentList.HeaderRow != null && DocumentList.HeaderRow.Cells.Count > 0)
                {
                    DocumentList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    DocumentList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    DocumentList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    DocumentList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    DocumentList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";

                    DocumentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void FilePath(SqlConnection Con, int DocumentId, ref string Path)
        {
            string sql = "select ParentDocumentId, DocumentName from dbo.Document ";
            sql += "where DocumentId = @DocumentId and DocumentType = 0";

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

            int ParentDocumentId = 0;
            string DocumentName = "";
            SqlDataReader reader = Cmd.ExecuteReader();
            if (reader.Read())
            {
                ParentDocumentId = int.Parse(reader["ParentDocumentId"].ToString());
                DocumentName = reader["DocumentName"].ToString();
            }

            reader.Close();

            if (ParentDocumentId == 0)
            {
                Path = " > <a href='/Admin/AdminDocument?Id=" + DocumentId + "'>" + DocumentName + "</a> " + Path;
                Path = "<a href='/Admin/AdminDocument'>/</a>" + Path;
            }
            else
            {
                Path = " > <a href='/Admin/AdminDocument?Id=" + DocumentId + "'>" + DocumentName + "</a> " + Path;
                FilePath(Con, ParentDocumentId, ref Path);
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            AddDocumentTitle.Text = "Add Document";
            AddDocumentButton.Text = "Add";
            ThisDocumentIdHidden.Value = "";
            ExistingDocument.Text = "";

            AddDocumentPanel.Visible = true;
        }

        protected void CancelDocumentButton_Click(object sender, EventArgs e)
        {
            AddDocumentPanel.Visible = false;
        }

        protected void AddDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (DocumentTypeList.SelectedValue == "0" && FolderName.Text.Trim() == "")
            {
                ShowError("Please enter Folder Name");
                return;
            }

            string DocumentName = "";
            string FileName1 = "";
            if (DocumentTypeList.SelectedValue == "1")
            {
                SaveFile(AttachFile.PostedFile, "Document", out DocumentName, out FileName1);
                if (FileName1 == "")
                {
                    ShowError("Please upload document");
                    return;
                }
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (ThisDocumentIdHidden.Value == "")
                {
                    string sql = "insert into dbo.Document ";
                    sql += "(ParentDocumentId, DocumentName, DocumentType, AttachFile, UpdatedBy, UpdatedDt) ";
                    sql += "select @ParentDocumentId, @DocumentName, @DocumentType, @AttachFile, @UpdatedBy, getdate() ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@ParentDocumentId", SqlDbType.Int).Value = int.Parse(DocumentId.Value);

                    // Folder
                    if (DocumentTypeList.SelectedValue == "0")
                    {
                        Cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = FolderName.Text.Trim();
                        Cmd.Parameters.Add("@DocumentType", SqlDbType.Int).Value = 0;
                        Cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = DBNull.Value;
                    }
                    else
                    {
                        Cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                        Cmd.Parameters.Add("@DocumentType", SqlDbType.Int).Value = 1;
                        Cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = FileName1;
                    }
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    if (DocumentTypeList.SelectedValue == "1")
                    {
                        string query = "select AttachFile from dbo.Document where DocumentId = @DocumentId";
                        SqlCommand Cmd2 = new SqlCommand(query, Con);
                        Cmd2.CommandType = CommandType.Text;

                        Cmd2.Parameters.Add("@DocumentId", SqlDbType.Int).Value = int.Parse(ThisDocumentIdHidden.Value);

                        SqlDataReader reader = Cmd2.ExecuteReader();
                        if(reader.Read())
                        {
                            if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                            {
                                string FilePath = FullPathHidden.Value + reader["AttachFile"].ToString();
                                if (File.Exists(FilePath))
                                {
                                    File.Delete(FilePath);
                                }
                            }
                        }
                        reader.Close();
                    }

                    string sql = "update dbo.Document ";
                    sql += "set DocumentName = @DocumentName, DocumentType=@DocumentType, AttachFile=@AttachFile, UpdatedBy=@UpdatedBy, UpdatedDt=getdate() ";
                    sql += "where DocumentId = @DocumentId";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    // Folder
                    if (DocumentTypeList.SelectedValue == "0")
                    {
                        Cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = FolderName.Text.Trim();
                        Cmd.Parameters.Add("@DocumentType", SqlDbType.Int).Value = 0;
                        Cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = DBNull.Value;
                    }
                    else
                    {
                        Cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                        Cmd.Parameters.Add("@DocumentType", SqlDbType.Int).Value = 1;
                        Cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = FileName1;
                    }
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = int.Parse(ThisDocumentIdHidden.Value);

                    Cmd.ExecuteNonQuery();
                }

                AddDocumentPanel.Visible = false;
                ShowDocument();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void SaveFile(HttpPostedFile PFile, string Prefix, out string DocumentName, out string FileName)
        {
            FileName = "";
            DocumentName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                DocumentName = filename;
                filename = Prefix + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPathHidden.Value + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void DocumentTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeDocumentType();
        }
        private void ChangeDocumentType()
        {
            if (DocumentTypeList.SelectedValue == "0")
            {
                FolderPanel.Visible = true;
                DocumentPanel.Visible = false;
            }
            else
            {
                FolderPanel.Visible = false;
                DocumentPanel.Visible = true;
            }
        }

        protected void DocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Folder/Document?');";
                    }
                }

                HiddenField DocumentIdHidden2 = e.Row.FindControl("DocumentIdHidden2") as HiddenField;
                if (DocumentIdHidden2 != null)
                {
                    HiddenField DocumentType = e.Row.FindControl("DocumentType") as HiddenField;
                    HiddenField DocumentName = e.Row.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Row.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Row.FindControl("DocumentLabel") as Label;
                    if (DocumentType.Value == "0")
                    {
                        DocumentLabel.Text = "<a href='/Admin/AdminDocument?Id=" + DocumentIdHidden2.Value + "'>" + DocumentName.Value + "</a>";
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }

            }

        }

        protected void DocumentList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField DocumentIdHidden2 = DocumentList.Rows[e.RowIndex].FindControl("DocumentIdHidden2") as HiddenField;
            if (DocumentIdHidden2 != null)
            {
                HiddenField DocumentType = DocumentList.Rows[e.RowIndex].FindControl("DocumentType") as HiddenField;
                SqlConnection Con = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    Con = new SqlConnection(connStr);
                    Con.Open();

                    DeleteDocument(Con, int.Parse(DocumentIdHidden2.Value), int.Parse(DocumentType.Value));

                    ShowDocument();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (Con != null)
                        Con.Close();
                }
            }
        }

        private void DeleteDocument(SqlConnection Con, int DocumentId, int DocumentType)
        {
            // Folder
            if (DocumentType == 0)
            {
                string sql = "select DocumentId, DocumentType from dbo.Document where ParentDocumentId = @DocumentId";
                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

                Dictionary<int, int> map = new Dictionary<int, int>();
                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    map.Add(int.Parse(reader["DocumentId"].ToString()), int.Parse(reader["DocumentType"].ToString()));
                }
                reader.Close();

                foreach(var pair in map)
                {
                    DeleteDocument(Con, pair.Key, pair.Value);
                }

                sql = "delete from dbo.Document where DocumentId = @DocumentId";
                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

                Cmd.ExecuteNonQuery();
            }
            else
            {
                string sql = "select AttachFile from dbo.Document where DocumentId = @DocumentId";
                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

                SqlDataReader reader = Cmd.ExecuteReader();
                if(reader.Read())
                {
                    if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                    {
                        string FilePath = FullPathHidden.Value + reader["AttachFile"].ToString();
                        if (File.Exists(FilePath))
                        {
                            File.Delete(FilePath);
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.Document where DocumentId = @DocumentId";
                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

                Cmd.ExecuteNonQuery();
            }
        }

        protected void DocumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HiddenField DocumentIdHidden2 = DocumentList.SelectedRow.FindControl("DocumentIdHidden2") as HiddenField;
            if (DocumentIdHidden2 != null)
            {
                AddDocumentTitle.Text = "Edit Document";
                AddDocumentButton.Text = "Save";
                ThisDocumentIdHidden.Value = DocumentIdHidden2.Value;

                HiddenField DocumentType = DocumentList.SelectedRow.FindControl("DocumentType") as HiddenField;
                DocumentTypeList.SelectedValue = DocumentType.Value;
                ChangeDocumentType();

                if (DocumentType.Value == "0")
                {
                    HiddenField DocumentName = DocumentList.SelectedRow.FindControl("DocumentName") as HiddenField;
                    FolderName.Text = DocumentName.Value;
                }
                else
                {
                    Label DocumentLabel = DocumentList.SelectedRow.FindControl("DocumentLabel") as Label;
                    ExistingDocument.Text = DocumentLabel.Text;
                }

                AddDocumentPanel.Visible = true;
            }

        }


    }
}