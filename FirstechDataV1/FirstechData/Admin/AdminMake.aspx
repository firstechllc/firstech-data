﻿<%@ Page Title="Admin Make" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminMake.aspx.cs" Inherits="FirstechData.Admin.AdminMake" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Make</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-warehouse"></i><span>Make</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add Make" OnClick="AddButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Label runat="server" id="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="MakeList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowSorting="true" 
                                OnRowCancelingEdit="MakeList_RowCancelingEdit" OnRowUpdating="MakeList_RowUpdating" 
                                OnRowDeleting="MakeList_RowDeleting" OnRowEditing="MakeList_RowEditing" OnRowDataBound="MakeList_RowDataBound" OnSorting="MakeList_Sorting" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Make" SortExpression="VehicleMakeName" >
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="VehicleMakeIdHidden" runat="server" Value='<%# Bind("VehicleMakeId") %>' />
                                            <asp:TextBox ID="MakeTxt" runat="server" Text='<%# Bind("VehicleMakeName") %>' Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="VehicleMakeIdHidden2" runat="server" Value='<%# Bind("VehicleMakeId") %>' />
                                            <asp:Label ID="MakeLabel" runat="server" Text='<%# Bind("VehicleMakeName") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country" SortExpression="CountryCode" >
                                        <EditItemTemplate>
                                            <asp:TextBox ID="CountryCodeTxt" runat="server" Text='<%# Bind("CountryCode") %>' Width="80px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CountryCodeLabel" runat="server" Text='<%# Bind("CountryCode") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField CancelText="Cancel" EditText="Edit" HeaderText="Edit" ShowEditButton="True" UpdateText="Save" ButtonType="Link" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=MakeList]').footable();
        });
    </script>
</asp:Content>
