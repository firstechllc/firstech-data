﻿<%@ Page Title="Admin Model" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminModel.aspx.cs" Inherits="FirstechData.Admin.AdminModel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Model</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-bus"></i><span>Model</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:TextBox runat="server" ID="SearchModelTxt" Width="150px" CssClass="col-sm-2" placeholder="Model to search"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search Model" OnClick="SearchButton_Click" />
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add Model" OnClick="AddButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="ModelList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowSorting="true" AllowPaging="true" PageSize="20"
                                OnRowCancelingEdit="ModelList_RowCancelingEdit" OnRowUpdating="ModelList_RowUpdating" OnPageIndexChanging="ModelList_PageIndexChanging"
                                OnRowDeleting="ModelList_RowDeleting" OnRowEditing="ModelList_RowEditing" OnRowDataBound="ModelList_RowDataBound" OnSorting="ModelList_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Model" SortExpression="VehicleModelName">
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="VehicleModelIdHidden" runat="server" Value='<%# Bind("VehicleModelId") %>' />
                                            <asp:TextBox ID="ModelTxt" runat="server" Text='<%# Bind("VehicleModelName") %>' Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="VehicleModelIdHidden2" runat="server" Value='<%# Bind("VehicleModelId") %>' />
                                            <asp:Label ID="ModelLabel" runat="server" Text='<%# Bind("VehicleModelName") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField CancelText="Cancel" EditText="Edit" HeaderText="Edit" ShowEditButton="True" UpdateText="Save" ButtonType="Link" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=ModelList]').footable();
        });
    </script>
</asp:Content>
