﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace FirstechData.Admin
{
    public partial class AdminModelAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleModel (VehicleModelName, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleModelName, getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleModelName", SqlDbType.NVarChar, 100).Value = Model.Text;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminModel");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminModel");
        }
    }
}