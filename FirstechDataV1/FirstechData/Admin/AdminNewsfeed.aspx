﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminNewsfeed.aspx.cs" Inherits="FirstechData.Admin.AdminNewsfeed" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="row" style="margin-top: -20px">
                <div class="large-12 columns">
                    <div class="box">
                        <div class="box-header bg-transparent">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                            </div>
                            <h3 class="box-title"><i class="icon-document"></i>
                                <span style="color: black; font-size: medium"><b>NEWSFEED</b></span>
                            </h3>
                        </div>
                        <div class="box-body " style="display: block;">
                            <div class="row">
                                <div class="large-12 columns">
                                    <asp:LinkButton runat="server" ID="AddNewsButton" Text="(+) Add News" OnClick="AddNewsButton_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <asp:Panel runat="server" ID="NewPanel" Visible="false">
                                        <div class="form-horizontal">
                                            <asp:ValidationSummary runat="server" CssClass="text-danger" />
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="TitleTxt" CssClass="col-md-2 control-label">Title</asp:Label>
                                                <div class="col-md-10">
                                                    <asp:TextBox runat="server" ID="TitleTxt" CssClass="form-control row" Width="100%" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTxt" Display="Dynamic"
                                                        CssClass="text-danger" ErrorMessage="The Title field is required." />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="UrlTxt" CssClass="col-md-2 control-label">URL</asp:Label>
                                                <div class="col-md-10">
                                                    <asp:TextBox runat="server" ID="UrlTxt" CssClass="form-control row" Width="100%" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UrlTxt" Display="Dynamic"
                                                        CssClass="text-danger" ErrorMessage="The URL field is required." />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="NewsfeedDate" CssClass="col-md-2 control-label">Date (MM/DD/YYYY)</asp:Label>
                                                <div class="col-md-10">
                                                    <asp:TextBox runat="server" ID="NewsfeedDate" ClientIDMode="Static" CssClass="form-control row date form_datetime" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NewsfeedDate" Display="Dynamic"
                                                        CssClass="text-danger" ErrorMessage="The Date field is required." />
                                                    <asp:CompareValidator ID="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="NewsfeedDate"
                                                        ErrorMessage="Invalid date"> </asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="SummaryTxt" CssClass="col-md-2 control-label">Summary</asp:Label>
                                                <div class="col-md-10">
                                                    <asp:TextBox runat="server" ID="SummaryTxt" CssClass="form-control row" Width="100%" Height="80px" TextMode="MultiLine" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="SummaryTxt" Display="Dynamic"
                                                        CssClass="text-danger" ErrorMessage="The Summary field is required." />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <asp:Button runat="server" ID="SaveButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveButton_Click"></asp:Button>
                                                    <asp:Button runat="server" ID="CancelButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelButton_Click" CausesValidation="false"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <br />
                                    <asp:ListView runat="server" ID="NewsFeedList" OnItemEditing="NewsFeedList_ItemEditing"
                                        OnItemCanceling="NewsFeedList_ItemCanceling" OnItemUpdating="NewsFeedList_ItemUpdating"
                                        OnItemDataBound="NewsFeedList_ItemDataBound" OnItemDeleting="NewsFeedList_ItemDeleting" >
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="large-12 columns">
                                                    <article class="reading-nest">
                                                        <h4><a href='<%# DataBinder.Eval(Container.DataItem, "NewsfeedUrl") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "NewsfeedTitle") %></a></h4>
                                                        <h6><%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %></h6>
                                                        <p>
                                                            <asp:Literal runat="server" ID="ContentLabel" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                                        </p>
                                                        <asp:HiddenField runat="server" ID="NewsfeedId" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedId") %>' />
                                                        <asp:LinkButton ID="EditButton" CssClass="button tiny bg-black radius" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="DeleteButton" CssClass="button tiny bg-black radius" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                                    </article>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal">
                                                <div class="form-horizontal">
                                                    <asp:ValidationSummary runat="server" CssClass="text-danger" />
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="TitleTxt" CssClass="col-md-2 control-label">Title</asp:Label>
                                                        <div class="col-md-10">
                                                            <asp:TextBox runat="server" ID="TitleTxt" CssClass="form-control row" Width="100%" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedTitle") %>' />
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTxt" Display="Dynamic"
                                                                CssClass="text-danger" ErrorMessage="The Title field is required." />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="UrlTxt" CssClass="col-md-2 control-label">URL</asp:Label>
                                                        <div class="col-md-10">
                                                            <asp:TextBox runat="server" ID="UrlTxt" CssClass="form-control row" Width="100%" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedUrl") %>' />
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="UrlTxt" Display="Dynamic"
                                                                CssClass="text-danger" ErrorMessage="The URL field is required." />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="NewsfeedDate" CssClass="col-md-2 control-label">Date (MM/DD/YYYY)</asp:Label>
                                                        <div class="col-md-10">
                                                            <asp:TextBox runat="server" ID="NewsfeedDate" ClientIDMode="Static" CssClass="form-control row date form_datetime" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %>' />
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="NewsfeedDate" Display="Dynamic"
                                                                CssClass="text-danger" ErrorMessage="The Date field is required." />
                                                            <asp:CompareValidator ID="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="NewsfeedDate"
                                                                ErrorMessage="Invalid date"> </asp:CompareValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="SummaryTxt" CssClass="col-md-2 control-label">Summary</asp:Label>
                                                        <div class="col-md-10">
                                                            <asp:TextBox runat="server" ID="SummaryTxt" CssClass="form-control row" Width="100%" Height="80px" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="SummaryTxt" Display="Dynamic"
                                                                CssClass="text-danger" ErrorMessage="The Summary field is required." />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-10">
                                                            <asp:HiddenField runat="server" ID="NewsfeedId" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedId") %>' />
                                                            <asp:LinkButton ID="UpdateButton" CssClass="button tiny bg-black radius" runat="server" CommandName="Update">Update</asp:LinkButton>
                                                            <asp:LinkButton ID="CancelButton" CssClass="button tiny bg-black radius" runat="server" CommandName="Cancel" CausesValidation="false">Cancel</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:ListView>
                                    <asp:DataPager ID="NewsFeedListPager" runat="server" PagedControlID="NewsFeedList" PageSize="5" OnPreRender="NewsFeedListPager_PreRender">
                                        <Fields>
                                            <asp:NumericPagerField ButtonType="Link" ButtonCount="10" />
                                        </Fields>
                                    </asp:DataPager>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
