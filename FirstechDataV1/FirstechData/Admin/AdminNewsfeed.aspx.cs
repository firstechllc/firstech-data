﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminNewsfeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("NewsfeedMenu");
                ShowNewsFeed();
            }
        }

        private void ShowNewsFeed()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select NewsfeedId, NewsfeedTitle, NewsfeedUrl, NewsfeedContent, NewsfeedDt ";
                sql += "from dbo.Newsfeed ";
                sql += "order by NewsfeedDt desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                NewsFeedList.DataSource = ds;
                NewsFeedList.DataBind();
            }
            catch (Exception ex)
            {
                //ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddNewsButton_Click(object sender, EventArgs e)
        {
            NewPanel.Visible = true;
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            DateTime dt;
            if (!DateTime.TryParse(NewsfeedDate.Text, out dt))
            {
                ShowError("Invalid Date");
                NewsfeedDate.Focus();
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.Newsfeed (NewsfeedTitle, NewsfeedUrl, NewsfeedDt, NewsfeedContent, UpdatedDt, UpdatedBy) ";
                sql += "select @NewsfeedTitle, @NewsfeedUrl, @NewsfeedDt, @NewsfeedContent, getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@NewsfeedTitle", SqlDbType.NVarChar, 200).Value = TitleTxt.Text;
                cmd.Parameters.Add("@NewsfeedUrl", SqlDbType.VarChar, 500).Value = UrlTxt.Text;
                cmd.Parameters.Add("@NewsfeedDt", SqlDbType.Date).Value = dt;
                cmd.Parameters.Add("@NewsfeedContent", SqlDbType.NVarChar).Value = SummaryTxt.Text;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                ShowNewsFeed();
                NewPanel.Visible = false;

                TitleTxt.Text = "";
                UrlTxt.Text = "";
                NewsfeedDate.Text = "";
                SummaryTxt.Text = "";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            NewPanel.Visible = false;
        }

        protected void NewsFeedListPager_PreRender(object sender, EventArgs e)
        {
            ShowNewsFeed();
        }

        protected void NewsFeedList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            NewsFeedList.EditIndex = e.NewEditIndex;
            ShowNewsFeed();
        }

        protected void NewsFeedList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            NewsFeedList.EditIndex = -1;
            ShowNewsFeed();
        }

        protected void NewsFeedList_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            TextBox EditTitleTxt = (NewsFeedList.Items[e.ItemIndex].FindControl("TitleTxt")) as TextBox;
            TextBox EditUrlTxt = (NewsFeedList.Items[e.ItemIndex].FindControl("UrlTxt")) as TextBox;
            TextBox EditNewsfeedDate = (NewsFeedList.Items[e.ItemIndex].FindControl("NewsfeedDate")) as TextBox;
            TextBox EditSummaryTxt = (NewsFeedList.Items[e.ItemIndex].FindControl("SummaryTxt")) as TextBox;
            HiddenField EditNewsfeedId = (NewsFeedList.Items[e.ItemIndex].FindControl("NewsfeedId")) as HiddenField;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.Newsfeed set NewsfeedTitle=@NewsfeedTitle, NewsfeedUrl=@NewsfeedUrl, ";
                sql += "NewsfeedDt=@NewsfeedDt, NewsfeedContent=@NewsfeedContent, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where NewsfeedId = @NewsfeedId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@NewsfeedTitle", SqlDbType.NVarChar, 200).Value = EditTitleTxt.Text;
                cmd.Parameters.Add("@NewsfeedUrl", SqlDbType.VarChar, 500).Value = EditUrlTxt.Text;
                cmd.Parameters.Add("@NewsfeedDt", SqlDbType.Date).Value = DateTime.Parse(EditNewsfeedDate.Text);
                cmd.Parameters.Add("@NewsfeedContent", SqlDbType.NVarChar).Value = EditSummaryTxt.Text;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = int.Parse(EditNewsfeedId.Value);

                cmd.ExecuteNonQuery();

                NewsFeedList.EditIndex = -1;
                ShowNewsFeed();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void NewsFeedList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                LinkButton DeleteButton = e.Item.FindControl("DeleteButton") as LinkButton;
                if (DeleteButton != null)
                {
                    DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Newsfeed?');";
                }
            }
        }

        protected void NewsFeedList_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            HiddenField NewsfeedId = NewsFeedList.Items[e.ItemIndex].FindControl("NewsfeedId") as HiddenField;
            if (NewsfeedId != null)
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "delete from dbo.Newsfeed ";
                    sql += "where NewsfeedId = @NewsfeedId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = int.Parse(NewsfeedId.Value);

                    cmd.ExecuteNonQuery();

                    ShowNewsFeed();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }


    }
}