﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminPrepAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MakeId.Value = Request["Make"];
                ModelId.Value = Request["Model"];
                YearId.Value = Request["Year"];
                Id.Value = Request["ID"];

                if (MakeId.Value == "" || ModelId.Value == "" || YearId.Value == "")
                {
                    return;
                }
                else
                {
                    LoadMake();

                    if (Id.Value != "")
                    {
                        LoadPrep(Id.Value);
                        TitleLabel.Text = "Edit Prep";
                    }
                    else
                    {
                        TitleLabel.Text = "Add Prep";
                    }
                }
            }
        }

        private void LoadMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeName from dbo.VehicleMake with (nolock) where VehicleMakeId=" + MakeId.Value;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    MakeLabel.Text = reader["VehicleMakeName"].ToString();
                }
                reader.Close();

                sql = "select VehicleModelName from dbo.VehicleModel with (nolock) where VehicleModelId=" + MakeId.Value;

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    ModelLabel.Text = reader["VehicleModelName"].ToString();
                }
                reader.Close();

                YearLabel.Text = YearId.Value;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void LoadPrep(string VehicleWirePrepId)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePrepId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePrep  ";
                sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepId);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Step.Text = reader["Step"].ToString();
                    editor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingFilename1.Value = reader["Attach1"].ToString();
                        AttachImage1.Visible = true;
                        AttachIamgeDelete1.Visible = true;
                        ReplaceImageLabel1.Visible = true;
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingFilename2.Value = reader["Attach2"].ToString();
                        AttachImage2.Visible = true;
                        AttachIamgeDelete2.Visible = true;
                        ReplaceImageLabel2.Visible = true;
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingFilename3.Value = reader["Attach3"].ToString();
                        AttachImage3.Visible = true;
                        AttachIamgeDelete3.Visible = true;
                        ReplaceImageLabel3.Visible = true;
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingFilename4.Value = reader["Attach4"].ToString();
                        AttachImage4.Visible = true;
                        AttachIamgeDelete4.Visible = true;
                        ReplaceImageLabel4.Visible = true;
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingFilename5.Value = reader["Attach5"].ToString();
                        AttachImage5.Visible = true;
                        AttachIamgeDelete5.Visible = true;
                        ReplaceImageLabel5.Visible = true;
                    }
                }
                reader.Close();

                AddButton.Text = "Update";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Make, string Model, string Year, out string FileName)
        {
            FileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                filename = "Prep_" + Make + "_" + Model + "_" + Year + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            ClearError();
            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string FileName1;
            SaveFile(AttachFile1.PostedFile, FullPath, MakeLabel.Text, ModelLabel.Text, YearLabel.Text, out FileName1);
            string FileName2;
            SaveFile(AttachFile2.PostedFile, FullPath, MakeLabel.Text, ModelLabel.Text, YearLabel.Text, out FileName2);
            string FileName3;
            SaveFile(AttachFile3.PostedFile, FullPath, MakeLabel.Text, ModelLabel.Text, YearLabel.Text, out FileName3);
            string FileName4;
            SaveFile(AttachFile4.PostedFile, FullPath, MakeLabel.Text, ModelLabel.Text, YearLabel.Text, out FileName4);
            string FileName5;
            SaveFile(AttachFile5.PostedFile, FullPath, MakeLabel.Text, ModelLabel.Text, YearLabel.Text, out FileName5);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (Id.Value == "")
                {
                    string sql = "insert into dbo.VehicleWirePrep (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select (select VehicleMakeModelYearId from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear), ";
                    sql += "@Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeId.Value);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelId.Value);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(YearId.Value);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = editor.Text;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (AttachIamgeDelete1.Checked && ExistingFilename1.Value != "")
                    {
                        if (File.Exists(FullPath + ExistingFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingFilename1.Value);
                        }
                    }
                    if (AttachIamgeDelete2.Checked && ExistingFilename2.Value != "")
                    {
                        if (File.Exists(FullPath + ExistingFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingFilename2.Value);
                        }
                    }
                    if (AttachIamgeDelete3.Checked && ExistingFilename3.Value != "")
                    {
                        if (File.Exists(FullPath + ExistingFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingFilename3.Value);
                        }
                    }
                    if (AttachIamgeDelete4.Checked && ExistingFilename4.Value != "")
                    {
                        if (File.Exists(FullPath + ExistingFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingFilename4.Value);
                        }
                    }
                    if (AttachIamgeDelete5.Checked && ExistingFilename5.Value != "")
                    {
                        if (File.Exists(FullPath + ExistingFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWirePrep ";
                    sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = editor.Text;

                    if (AttachIamgeDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachIamgeDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachIamgeDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachIamgeDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachIamgeDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(Id.Value);

                    cmd.ExecuteNonQuery();
                }

                Response.Redirect("/Admin/AdminPrep?Make=" + MakeId.Value + "&Model=" + ModelId.Value + "&Year=" + YearId.Value);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Admin/AdminPrep?Make=" + MakeId.Value + "&Model=" + ModelId.Value + "&Year=" + YearId.Value);
        }
    }
}