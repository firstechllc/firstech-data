﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminUsers.aspx.cs" Inherits="FirstechData.Admin.AdminUsers" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-user"></i>
            <span>Users</span>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <div class="row">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </div>
            <div class="row">
                <div class="form-group form-horizontal" style="height:40px">
                    <label for="ApprovedList" class="col-sm-1 control-label">Approved:</label>
                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="ApprovedList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ApprovedList_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="True" Value="1"></asp:ListItem>
                            <asp:ListItem Text="False" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:GridView ID="UserList" runat="server" AutoGenerateColumns="False" CssClass="footable"
                        AllowPaging="true" PageSize="20" OnPageIndexChanging="UserList_PageIndexChanging"
                        OnRowDataBound="UserList_RowDataBound" OnRowDeleting="UserList_RowDeleting" >
                        <Columns>
                            <asp:TemplateField HeaderText="Email" >
                                <ItemTemplate>
                                    <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                                    <asp:HiddenField ID="UserId" runat="server" Value='<%# Bind("Id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                            <asp:BoundField DataField="StoreName" HeaderText="Store Name" />
                            <asp:BoundField DataField="Address" HeaderText="Address" />
                            <asp:BoundField DataField="City" HeaderText="City" />
                            <asp:BoundField DataField="State" HeaderText="State" />
                            <asp:BoundField DataField="ZipCode" HeaderText="Zip" />
                            <asp:BoundField DataField="Phone" HeaderText="Phone" />
                            <asp:BoundField DataField="Approved" HeaderText="Approved" ItemStyle-Width="50px" />
                            <asp:CommandField DeleteText="Approve" ShowDeleteButton="True" ItemStyle-Width="50px" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[id*=UserList]').footable();
        });
    </script>
</asp:Content>
