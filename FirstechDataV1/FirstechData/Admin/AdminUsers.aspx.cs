﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using FirstechData.Models;

namespace FirstechData.Admin
{
    public partial class AdminUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.ChangeMenuCss("UsersMenu");
            LoadUserList();
        }

        private void LoadUserList()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            //var users = context.Users.Where(u => u.Approved == true);
            IQueryable<ApplicationUser> users = context.Users;

            if (ApprovedList.SelectedValue != "-1")
            {
                if (ApprovedList.SelectedValue == "1")
                {
                    users = users.Where(u => u.Approved == true);
                }
                else if (ApprovedList.SelectedValue == "0")
                {
                    users = users.Where(u => u.Approved == false);
                }
            }

            UserList.DataSource = users.ToList();
            UserList.DataBind();

            UserList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

            UserList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[10].Attributes["data-hide"] = "phone";
            UserList.HeaderRow.Cells[11].Attributes["data-hide"] = "phone";

            UserList.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void UserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UserList.PageIndex = e.NewPageIndex;
            LoadUserList();
        }

        protected void ApprovedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUserList();
        }

        protected void UserList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Approved = e.Row.Cells[10].Text;
                if (Approved == "True")
                {
                    LinkButton btn = (LinkButton)(e.Row.Cells[11].Controls[0]);
                    btn.Text = "Disapprove";
                }
            }
        }

        protected void UserList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField IdHidden = UserList.Rows[e.RowIndex].FindControl("UserId") as HiddenField;

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(IdHidden.Value);
            user.Approved = !user.Approved;

            IdentityResult result = manager.Update(user);
            if (result.Succeeded)
            {
                LoadUserList();
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}