﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminVehicleInfo.aspx.cs" Inherits="FirstechData.Admin.AdminVehicleInfo" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Vehicle Info</b></span><asp:HiddenField runat="server" ID="SaveFolderHidden" />
            </h3>
        </div>
        <div>
            <div class="row">
                <div class="form-group form-horizontal">
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3 text-right">
                        <asp:Button runat="server" ID="AddVehiclePanelButton" CssClass="button tiny bg-black radius" Text="Add Vehicle" OnClick="AddVehiclePanelButton_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label><asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>
    <asp:Panel runat="server" ID="NewVehiclePanel" Visible="false">
        <div class="box">
            <div class="box-header bg-transparent bor">
                <h3 class="box-title"><i></i>
                    <span style="color: black; font-size: medium">
                        <asp:Literal runat="server" ID="AddVehicleTitle"></asp:Literal></span></h3>
            </div>
            <div>
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="MakeList" CssClass="col-md-2 control-label">Make</asp:Label>
                            <div class="col-md-10">
                                <asp:DropDownList runat="server" ID="MakeList" CssClass="form-control" Width="250px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="ModelList" CssClass="col-md-2 control-label">Model</asp:Label>
                            <div class="col-md-10">
                                <asp:DropDownList runat="server" ID="ModelList" CssClass="form-control" Width="250px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Year" CssClass="col-md-2 control-label">Year</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Year" CssClass="form-control" MaxLength="4" Width="250px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" ID="AddVehicleButton" CssClass="button tiny bg-black radius" Text="Add" OnClick="AddVehicleButton_Click"></asp:Button>
                                <asp:Button runat="server" ID="CancelAddVehicleButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelAddVehicleButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="row bg-white" runat="server" id="TitlePanel" visible="false">
        <div class="large-6 columns" style="padding-top: 5px;">
            <h4>
                <asp:Literal runat="server" ID="CurrentVehicle"></asp:Literal></h4>
        </div>
        <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
            <asp:HiddenField runat="server" ID="VehicleMakeModelYearIdHidden" />
            <asp:Button runat="server" ID="EditVehicleButton" CssClass="button tiny bg-black radius no-margin" Text="Edit Vehicle" OnClick="EditVehicleButton_Click"></asp:Button>
            <asp:Button runat="server" ID="DeleteVehicleButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Vehicle" OnClick="DeleteVehicleButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Vehicle?');"></asp:Button>
        </div>
    </div>
    <ul class="tabs row" data-tab>
        <li class="tab-title active" runat="server" id="WiringPaneltab">
            <a href="#<%=WiringPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="WiringHeader" Text="Vehicle Wiring"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="FBPaneltab">
            <a href="#<%=FBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="FBResultHeader" Text="Facebook Result"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="DocumentPaneltab">
            <a href="#<%=DocumentPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DocumentHeader" Text="Documents"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linksdisassemblytab">
            <a href="#<%=linksdisassembly.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DisassemblyHeader" Text="Disassembly"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkspreptab">
            <a href="#<%=linksprep.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="PrepHeader" Text="Prep"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkroutingtab">
            <a href="#<%=linkrouting.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="RoutingHeader" Text="Routing/Placement"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="TSBPaneltab">
            <a href="#<%=TSBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="TSBHeader" Text="TSB"></asp:Literal></a>
        </li>
    </ul>
    <div class="tabs-content edumix-tab-horz">
        <div class="content active" runat="server" id="WiringPanel">
            <div class="row">
                <div class="large-12 columns">
                    <asp:Label runat="server" ID="NoteLabel"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Button runat="server" ID="EditNotePanelButton" Text="Edit Note" CssClass="button tiny bg-black radius" OnClick="EditNotePanelButton_Click" Visible="false" />
                </div>
            </div>
            <asp:Panel runat="server" ID="EditNotePanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Edit Note</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="editor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8" style="padding-left: 23px;">
                                        <CKEditor:CKEditorControl ID="editor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor"></CKEditor:CKEditorControl>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireNoteIdHidden" />
                                        <asp:Button runat="server" ID="SaveNoteButton" Text="Save" CssClass="button tiny bg-black radius" OnClick="SaveNoteButton_Click" />
                                        <asp:Button runat="server" ID="CancelNoteButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelNoteButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddWirePanelButton" Text="(+) Add Row" OnClick="AddWirePanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingWiringFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingWiringFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingWiringFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingWiringFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingWiringFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddWirePanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddWiringTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="WireFunctionList" CssClass="col-md-2 control-label">Wire Function</asp:Label>
                                    <div class="col-md-4 text-left">
                                        <asp:DropDownList runat="server" ID="WireFunctionList" CssClass="form-control no-margin no-padding" />
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="InstalltionTypeList" CssClass="col-md-2 control-label">Installtion Type</asp:Label>
                                    <div class="col-md-4 text-left">
                                        <asp:DropDownList runat="server" ID="InstalltionTypeList" CssClass="form-control no-margin no-padding" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Color" CssClass="col-md-2 control-label">Vehicle Color</asp:Label>
                                    <div class="col-md-4" style="padding-left: 23px;">
                                        <asp:TextBox runat="server" ID="Color" CssClass="form-control row" />
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="VehicleColor" CssClass="col-md-2 control-label">CM7X00/ADS Color</asp:Label>
                                    <div class="col-md-4" style="padding-left: 23px;">
                                        <asp:TextBox runat="server" ID="VehicleColor" CssClass="form-control row" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Location" CssClass="col-md-2 control-label">Location</asp:Label>
                                    <div class="col-md-4" style="padding-left: 23px;">
                                        <asp:TextBox runat="server" ID="Location" CssClass="form-control row" />
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="PinOut" CssClass="col-md-2 control-label">Pin Out</asp:Label>
                                    <div class="col-md-4" style="padding-left: 23px;">
                                        <asp:TextBox runat="server" ID="PinOut" CssClass="form-control row" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Polarity" CssClass="col-md-2 control-label">Polarity</asp:Label>
                                    <div class="col-md-4" style="padding-left: 23px;">
                                        <asp:TextBox runat="server" ID="Polarity" CssClass="form-control row" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <asp:Image runat="server" ID="AttachWiringImage1" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete1" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachWiringFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceWiringImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachWiringImage2" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete2" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachWiringFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceWiringImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachWiringImage3" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete3" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachWiringFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceWiringImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachWiringImage4" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete4" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachWiringFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceWiringImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachWiringImage5" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete5" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachWiringFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceWiringImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireFunctionIdHidden" />
                                        <asp:Button runat="server" ID="AddWireButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddWireButton_Click" />
                                        <asp:Button runat="server" ID="CancelWireButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelWireButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%"
                OnRowDataBound="WireList_RowDataBound" OnSelectedIndexChanged="WireList_SelectedIndexChanged" OnRowDeleting="WireList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Wire Function" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="WireFunctionNameLabel" runat="server" Text='<%# Bind("WireFunctionName") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireFunctionId" runat="server" Value='<%# Bind("VehicleWireFunctionId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="InstallationTypeName" HeaderText="Installation Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="Polarity" HeaderText="Polarity" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="FBPanel">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddFBPanelButton" Text="(+) Add Row" OnClick="AddFBPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingFBFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddFBPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddFBTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="FBeditor" CssClass="col-md-2 control-label">Facebook</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="FBeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="FBUrl" CssClass="col-md-2 control-label">URL</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="FBUrl" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <asp:Image runat="server" ID="AttachFBImage1" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachFBImageDelete1" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachFBImage2" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachFBImageDelete2" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachFBImage3" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachFBImageDelete3" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachFBImage4" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachFBImageDelete4" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachFBImage5" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachFBImageDelete5" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireFacebookIdHidden" />
                                        <asp:Button runat="server" ID="AddFBButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddFBButton_Click" />
                                        <asp:Button runat="server" ID="CancelFBButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelFBButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="FBList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="FBList_RowDataBound" Width="100%" OnSelectedIndexChanged="FBList_SelectedIndexChanged" OnRowDeleting="FBList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Facebook" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Note") %>
                            <br />URL: <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("URL") %>'></asp:Literal>
                            <asp:HiddenField ID="VehicleWireFacebookId" runat="server" Value='<%# Bind("VehicleWireFacebookId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="DocumentPanel">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddDocumentPanelButton" Text="(+) Add Row" OnClick="AddDocumentPanelButton_Click" Visible="false"></asp:LinkButton>
                </div>
            </div>
            <asp:Panel runat="server" ID="AddDocumentPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddDocumentTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Document</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Label runat="server" ID="ExistingDocument"></asp:Label><asp:HiddenField runat="server" ID="CurrentDocumentAttachFile" />
                                                <asp:FileUpload runat="server" ID="AttachFile" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleDocumentIdHidden" />
                                        <asp:Button runat="server" ID="AddDocumentButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddDocumentButton_Click" />
                                        <asp:Button runat="server" ID="CancelDocumentButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelDocumentButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="DocumentList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="DocumentList_RowDataBound" Width="100%" OnSelectedIndexChanged="DocumentList_SelectedIndexChanged" OnRowDeleting="DocumentList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Document" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="VehicleDocumentId" runat="server" Value='<%# Bind("VehicleDocumentId") %>' />
                            <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                            <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                            <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linksdisassembly">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddDisassemblyPanelButton" Text="(+) Add Row" OnClick="AddDisassemblyPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddDisassemblyPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddDisassemblyTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="DisassemblyStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="DisassemblyStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Disassemblyeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Disassemblyeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <asp:Image runat="server" ID="AttachDisassemblyImage1" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete1" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachDisassemblyImage2" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete2" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachDisassemblyImage3" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete3" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachDisassemblyImage4" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete4" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachDisassemblyImage5" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete5" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireDisassemblyIdHidden" />
                                        <asp:Button runat="server" ID="AddDisassemblyButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddDisassemblyButton_Click" />
                                        <asp:Button runat="server" ID="CancelDisassemblyButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelDisassemblyButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="DisassemblyList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="DisassemblyList_RowDataBound" Width="100%" OnSelectedIndexChanged="DisassemblyList_SelectedIndexChanged" OnRowDeleting="DisassemblyList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireDisassemblyId" runat="server" Value='<%# Bind("VehicleWireDisassemblyId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linksprep">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddPrepPanelButton" Text="(+) Add Row" OnClick="AddPrepPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddPrepPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddPrepTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="PrepStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="PrepStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Prepeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Prepeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <asp:Image runat="server" ID="AttachPrepImage1" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachPrepImageDelete1" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachPrepImage2" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachPrepImageDelete2" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachPrepImage3" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachPrepImageDelete3" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachPrepImage4" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachPrepImageDelete4" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachPrepImage5" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachPrepImageDelete5" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWirePrepIdHidden" />
                                        <asp:Button runat="server" ID="AddPrepButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddPrepButton_Click" />
                                        <asp:Button runat="server" ID="CancelPrepButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelPrepButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="PrepList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="PrepList_RowDataBound" Width="100%"
                OnSelectedIndexChanged="PrepList_SelectedIndexChanged" OnRowDeleting="PrepList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePrepId" runat="server" Value='<%# Bind("VehicleWirePrepId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linkrouting">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddRoutingPanelButton" Text="(+) Add Row" OnClick="AddRoutingPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddRoutingPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddRoutingTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="RoutingStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="RoutingStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Routingeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Routingeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <asp:Image runat="server" ID="AttachRoutingImage1" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachRoutingImageDelete1" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachRoutingImage2" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachRoutingImageDelete2" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachRoutingImage3" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachRoutingImageDelete3" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachRoutingImage4" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachRoutingImageDelete4" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <br />

                                        <asp:Image runat="server" ID="AttachRoutingImage5" Visible="false" />
                                        <asp:CheckBox runat="server" ID="AttachRoutingImageDelete5" Text="Delete?" Visible="false" />
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWirePlacementRoutingIdHidden" />
                                        <asp:Button runat="server" ID="AddRoutingButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddRoutingButton_Click" />
                                        <asp:Button runat="server" ID="CancelRoutingButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelRoutingButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="RoutingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="RoutingList_RowDataBound" Width="100%"
                OnSelectedIndexChanged="RoutingList_SelectedIndexChanged" OnRowDeleting="RoutingList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePlacementRoutingId" runat="server" Value='<%# Bind("VehicleWirePlacementRoutingId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="TSBPanel">
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <script type="text/javascript">
        $(function () {
        });
    </script>

</asp:Content>
