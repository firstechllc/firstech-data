﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminVehicleInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("VehicleMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                LoadVehicleMake();
                ShowModelList();
            }
        }

        private void LoadVehicleMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName from dbo.VehicleMake with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("Make", "-1"));

                MakeList.DataTextField = "VehicleMakeName";
                MakeList.DataValueField = "VehicleMakeId";
                MakeList.DataSource = ds2;
                MakeList.DataBind();

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ShowInfo(string info)
        {
            InfoLabel.Text = info;
            InfoLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }

        protected void ShowModelList()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += " order by VehicleModelName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ModelList.DataTextField = "VehicleModelName";
                ModelList.DataValueField = "VehicleModelId";
                ModelList.DataSource = ds;
                ModelList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowHideResult(false);
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " order by VehicleModelName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("Model", "-1"));

                ShowHideResult(false);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeYearList(true);
        }

        private void ChangeYearList(bool refresh)
        {
            ClearError();
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                VehicleYearList.Items.Clear();
                ShowHideResult(false);
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " and a.VehicleModelId = " + VehicleModelList.SelectedValue;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("Year", "-1"));

                if (refresh)
                {
                    ShowHideResult(false);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchInfo(0);
            NewVehiclePanel.Visible = false;
        }

        private void ShowHideResult(bool show)
        {
            AddWirePanelButton.Visible = show;
            EditNotePanelButton.Visible = show;
            AddDisassemblyPanelButton.Visible = show;
            AddPrepPanelButton.Visible = show;
            AddRoutingPanelButton.Visible = show;
            AddFBPanelButton.Visible = show;
            AddDocumentPanelButton.Visible = show;

            WireList.Visible = show;
            NoteLabel.Visible = show;
            DisassemblyList.Visible = show;
            PrepList.Visible = show;
            RoutingList.Visible = show;

            if (!show)
            {
                WiringHeader.Text = "Vehicle Wiring";
                DisassemblyHeader.Text = "Disassembly";
                FBResultHeader.Text = "Facebook Result";
                PrepHeader.Text = "Prep";
                RoutingHeader.Text = "Routing/Placement";
                TitlePanel.Visible = false;
                VehicleMakeModelYearIdHidden.Value = "";

                NewVehiclePanel.Visible = false;
                AddWirePanel.Visible = false;
                EditNotePanel.Visible = false;
                AddDisassemblyPanel.Visible = false;
                AddDocumentPanel.Visible = false;
                AddPrepPanel.Visible = false;
                AddRoutingPanel.Visible = false;
                AddFBPanel.Visible = false;
            }
        }

        private void ChangeVehicleTitle()
        {
            CurrentVehicle.Text = VehicleMakeList.SelectedItem.Text + " " + VehicleModelList.SelectedItem.Text + " " + VehicleYearList.SelectedItem.Text;
        }

        protected void SearchInfo(int tabIdx)
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                ShowHideResult(false);
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                ShowHideResult(false);
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                ShowHideResult(false);
                return;
            }

            if (tabIdx == 0)
            {
                ChangeVehicleTitle();
                TitlePanel.Visible = true;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeModelYearId from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear";
                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMakeModelYearIdHidden.Value = reader["VehicleMakeModelYearId"].ToString();
                }
                else
                {
                    reader.Close();
                    return;
                }
                reader.Close();

                string SaveFolder = SaveFolderHidden.Value;

                if (tabIdx == 0 || tabIdx == 1)
                {
                    sql = "select a.VehicleWireFunctionId, c.WireFunctionName, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, ";
                    sql += "VehicleColor, PinOut, ";
                    sql += "case when a.Attach1 is not null and a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end as Attach1, ";
                    sql += "case when a.Attach2 is not null and a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end as Attach2, ";
                    sql += "case when a.Attach3 is not null and a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end as Attach3, ";
                    sql += "case when a.Attach4 is not null and a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end as Attach4, ";
                    sql += "case when a.Attach5 is not null and a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end as Attach5 ";
                    sql += "from dbo.VehicleWireFunction a ";
                    sql += "join dbo.VehicleMakeModelYear b on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId ";
                    sql += "join dbo.WireFunction c on a.WireFunctionId = c.WireFunctionId ";
                    sql += "left join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                    sql += " order by c.SortOrder ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    WireList.DataSource = ds;
                    WireList.DataBind();

                    if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
                    {
                        WireList.Attributes["data-page"] = "false";

                        WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        WireList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                        WireList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                        WireList.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                        WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                        WireList.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                        WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                        //WiringTitle.Visible = true;
                        WiringHeader.Text = "Vehicle Wiring (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        WiringHeader.Text = "Vehicle Wiring (0)";
                        //ShowError("No wiring data available");
                        //WiringTitle.Visible = false;
                    }

                    AddWirePanel.Visible = false;
                    EditNotePanel.Visible = false;

                    sql = "select a.VehicleWireNoteId, a.Note ";
                    sql += "from dbo.VehicleWireNote a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

                    cmd = new SqlCommand(sql, Con);
                    cmd.CommandType = CommandType.Text;

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Note"] != null)
                        {
                            NoteLabel.Text = "Note: " + reader["Note"].ToString();
                            editor.Text = reader["Note"].ToString();
                        }
                        else
                        {
                            NoteLabel.Text = "Note:";
                        }

                        VehicleWireNoteIdHidden.Value = reader["VehicleWireNoteId"].ToString();
                    }
                    else
                    {
                        VehicleWireNoteIdHidden.Value = "";
                        NoteLabel.Text = "Note:";
                    }
                    reader.Close();
                }

                if (tabIdx == 0 || tabIdx == 2)
                {
                    sql = "select a.VehicleWireFacebookId, a.URL, a.Note, ";
                    sql += "case when a.Attach1 is not null and a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end as Attach1, ";
                    sql += "case when a.Attach2 is not null and a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end as Attach2, ";
                    sql += "case when a.Attach3 is not null and a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end as Attach3, ";
                    sql += "case when a.Attach4 is not null and a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end as Attach4, ";
                    sql += "case when a.Attach5 is not null and a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end as Attach5 ";
                    sql += "from dbo.VehicleWireFacebook a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    FBList.DataSource = ds;
                    FBList.DataBind();

                    if (FBList != null && FBList.HeaderRow != null && FBList.HeaderRow.Cells.Count > 0)
                    {
                        FBList.Attributes["data-page"] = "false";

                        FBList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        FBResultHeader.Text = "Facebook Result (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        FBResultHeader.Text = "Facebook Result (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 3)
                {
                    sql = "select a.VehicleDocumentId, a.DocumentName, a.AttachFile ";
                    sql += "from dbo.VehicleDocument a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    DocumentList.DataSource = ds;
                    DocumentList.DataBind();

                    if (DocumentList != null && DocumentList.HeaderRow != null && DocumentList.HeaderRow.Cells.Count > 0)
                    {
                        DocumentList.Attributes["data-page"] = "false";

                        DocumentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        DocumentHeader.Text = "Documents (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        DocumentHeader.Text = "Documents (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 4)
                {
                    sql = "select a.VehicleWireDisassemblyId, a.Step, a.Note, ";
                    sql += "case when a.Attach1 is not null and a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end as Attach1, ";
                    sql += "case when a.Attach2 is not null and a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end as Attach2, ";
                    sql += "case when a.Attach3 is not null and a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end as Attach3, ";
                    sql += "case when a.Attach4 is not null and a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end as Attach4, ";
                    sql += "case when a.Attach5 is not null and a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end as Attach5 ";
                    sql += "from dbo.VehicleWireDisassembly a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                    sql += " order by a.Step ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    DisassemblyList.DataSource = ds;
                    DisassemblyList.DataBind();

                    if (DisassemblyList != null && DisassemblyList.HeaderRow != null && DisassemblyList.HeaderRow.Cells.Count > 0)
                    {
                        DisassemblyList.Attributes["data-page"] = "false";

                        /*
                        DisassemblyList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        DisassemblyList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";

                        DisassemblyList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                        */

                        DisassemblyList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        DisassemblyHeader.Text = "Disassembly (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        DisassemblyHeader.Text = "Disassembly (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 5)
                {
                    sql = "select a.VehicleWirePrepId, a.Step, a.Note, ";
                    sql += "case when a.Attach1 is not null and a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end as Attach1, ";
                    sql += "case when a.Attach2 is not null and a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end as Attach2, ";
                    sql += "case when a.Attach3 is not null and a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end as Attach3, ";
                    sql += "case when a.Attach4 is not null and a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end as Attach4, ";
                    sql += "case when a.Attach5 is not null and a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end as Attach5 ";
                    sql += "from dbo.VehicleWirePrep a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                    sql += " order by a.Step ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    PrepList.DataSource = ds;
                    PrepList.DataBind();

                    if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
                    {
                        PrepList.Attributes["data-page"] = "false";

                        PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        PrepList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        PrepList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        PrepList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //PrepTitle.Visible = true;

                        PrepHeader.Text = "Prep (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        PrepHeader.Text = "Prep (0)";
                        //PrepTitle.Visible = false;
                    }
                }


                if (tabIdx == 0 || tabIdx == 6)
                {
                    sql = "select a.VehicleWirePlacementRoutingId, a.Step, a.Note, ";
                    sql += "case when a.Attach1 is not null and a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end as Attach1, ";
                    sql += "case when a.Attach2 is not null and a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end as Attach2, ";
                    sql += "case when a.Attach3 is not null and a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end as Attach3, ";
                    sql += "case when a.Attach4 is not null and a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end as Attach4, ";
                    sql += "case when a.Attach5 is not null and a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end as Attach5 ";
                    sql += "from dbo.VehicleWirePlacementRouting a ";
                    sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                    sql += " order by a.Step ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    RoutingList.DataSource = ds;
                    RoutingList.DataBind();

                    if (RoutingList != null && RoutingList.HeaderRow != null && RoutingList.HeaderRow.Cells.Count > 0)
                    {
                        RoutingList.Attributes["data-page"] = "false";

                        RoutingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        RoutingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        RoutingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        RoutingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        RoutingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        RoutingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        RoutingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //PlacementTitle.Visible = true;

                        RoutingHeader.Text = "Routing/Placement (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        RoutingHeader.Text = "Routing/Placement (0)";
                        //PlacementTitle.Visible = false;
                    }
                }

                ShowHideResult(true);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Prep?');";
                    }
                }
            }
        }

        protected void DisassemblyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Disassembly?');";
                    }
                }
            }
        }
        protected void RoutingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Routing/Plancement?');";
                    }
                }
            }
        }

        private void HideNoImageLink(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink a1 = ((HyperLink)(e.Row.FindControl("Image1Link")));
                HyperLink a2 = ((HyperLink)(e.Row.FindControl("Image2Link")));
                HyperLink a3 = ((HyperLink)(e.Row.FindControl("Image3Link")));
                HyperLink a4 = ((HyperLink)(e.Row.FindControl("Image4Link")));
                HyperLink a5 = ((HyperLink)(e.Row.FindControl("Image5Link")));

                if (a1.ImageUrl == "")
                {
                    a1.Visible = false;
                }
                if (a2.ImageUrl == "")
                {
                    a2.Visible = false;
                }
                if (a3.ImageUrl == "")
                {
                    a3.Visible = false;
                }
                if (a4.ImageUrl == "")
                {
                    a4.Visible = false;
                }
                if (a5.ImageUrl == "")
                {
                    a5.Visible = false;
                }
            }
        }

        protected void AddVehiclePanelButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Add Vehicle";
            AddVehicleButton.Text = "Add";

            NewVehiclePanel.Visible = true;
        }

        protected void CancelAddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();
            NewVehiclePanel.Visible = false;
        }

        protected void AddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            int year;
            if (!int.TryParse(Year.Text, out year))
            {
                ShowError("Please enter year");
                Year.Focus();
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (AddVehicleButton.Text == "Add")
                {
                    string sql = "insert into dbo.VehicleMakeModelYear (VehicleMakeId, VehicleModelId, VehicleYear, UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeId, @VehicleModelId, @VehicleYear, getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                    ShowInfo("Added vehicle successfully");
                    LoadVehicleMake();
                }
                else
                {
                    string sql = "update dbo.VehicleMakeModelYear set VehicleMakeId = @VehicleMakeId, VehicleModelId=@VehicleModelId, ";
                    sql += "VehicleYear=@VehicleYear, UpdatedDt=getdate(), UpdatedBy= @UpdatedBy ";
                    sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.ExecuteNonQuery();
                    ShowInfo("Updated vehicle successfully");

                    VehicleMakeList.SelectedValue = MakeList.SelectedValue;
                    VehicleModelList.SelectedValue = ModelList.SelectedValue;
                    ChangeYearList(false);
                    VehicleYearList.SelectedValue = Year.Text;
                    ChangeVehicleTitle();
                }

                Year.Text = "";
                NewVehiclePanel.Visible = false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Vehicle already exists");
                }
                else
                {
                    ShowError(ex.ToString());
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditVehicleButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Edit Vehicle";
            AddVehicleButton.Text = "Save";

            MakeList.SelectedValue = VehicleMakeList.SelectedValue;
            ModelList.SelectedValue = VehicleModelList.SelectedValue;
            Year.Text = VehicleYearList.SelectedValue;

            NewVehiclePanel.Visible = true;
        }

        protected void DeleteVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Wiring
                string sql = "delete from dbo.VehicleWireFunction where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Note
                sql = "delete from dbo.VehicleWireNote where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Disassembly
                sql = "delete from dbo.VehicleWireDisassembly where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Prep
                sql = "delete from dbo.VehicleWirePrep where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Routing
                sql = "delete from dbo.VehicleWirePlacementRouting where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Vehicle
                sql = "delete from dbo.VehicleMakeModelYear where VehicleMakeModelYearId = @VehicleMakeModelYearId";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                tran.Commit();

                VehicleMakeList.SelectedIndex = 0;
                VehicleModelList.Items.Clear();
                VehicleYearList.Items.Clear();
                ShowHideResult(false);

                ShowInfo("Deleted vehicle successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void AddWirePanelButton_Click(object sender, EventArgs e)
        {
            LoadWireInstall();

            VehicleColor.Text = "";
            Color.Text = "";
            PinOut.Text = "";
            Location.Text = "";
            Polarity.Text = "";

            AttachWiringImageDelete1.Checked = false;
            AttachWiringImageDelete2.Checked = false;
            AttachWiringImageDelete3.Checked = false;
            AttachWiringImageDelete4.Checked = false;
            AttachWiringImageDelete5.Checked = false;

            ExistingWiringFilename1.Value = "";
            AttachWiringImage1.Visible = false;
            AttachWiringImageDelete1.Visible = false;
            ReplaceWiringImageLabel1.Visible = false;

            ExistingWiringFilename2.Value = "";
            AttachWiringImage2.Visible = false;
            AttachWiringImageDelete2.Visible = false;
            ReplaceWiringImageLabel2.Visible = false;

            ExistingWiringFilename3.Value = "";
            AttachWiringImage3.Visible = false;
            AttachWiringImageDelete3.Visible = false;
            ReplaceWiringImageLabel3.Visible = false;

            ExistingWiringFilename4.Value = "";
            AttachWiringImage4.Visible = false;
            AttachWiringImageDelete4.Visible = false;
            ReplaceWiringImageLabel4.Visible = false;

            ExistingWiringFilename5.Value = "";
            AttachWiringImage5.Visible = false;
            AttachWiringImageDelete5.Visible = false;
            ReplaceWiringImageLabel5.Visible = false;

            AddWiringTitle.Text = "Add Wiring";
            AddWireButton.Text = "Add";
            AddWirePanel.Visible = true;
        }

        private void LoadWireInstall()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId, WireFunctionName from dbo.WireFunction with (nolock) where Inactive=0  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                WireFunctionList.DataTextField = "WireFunctionName";
                WireFunctionList.DataValueField = "WireFunctionId";
                WireFunctionList.DataSource = ds2;
                WireFunctionList.DataBind();

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            if (VehicleModelList.Items.Count <= 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Vehicle");
                return;
            }
            if (VehicleYearList.Items.Count <= 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Vehicle");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
                string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
                string Year = VehicleYearList.SelectedItem.Text;

                string FileName1;
                SaveFile(AttachWiringFile1.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName1);
                string FileName2;
                SaveFile(AttachWiringFile2.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName2);
                string FileName3;
                SaveFile(AttachWiringFile3.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName3);
                string FileName4;
                SaveFile(AttachWiringFile4.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName4);
                string FileName5;
                SaveFile(AttachWiringFile5.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName5);

                if (AddWireButton.Text == "Add")
                {
                    string sql = "insert into dbo.VehicleWireFunction (VehicleMakeModelYearId, ";
                    sql += "WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy, Inactive) ";
                    sql += "select @VehicleMakeModelYearId, ";
                    sql += "@WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy, 0";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionList.SelectedValue);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor.Text.Trim();
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Color.Text.Trim();
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut.Text.Trim();
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location.Text.Trim();
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity.Text.Trim();

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if ((AttachWiringImageDelete1.Checked && ExistingWiringFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingWiringFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingWiringFilename1.Value);
                        }
                    }
                    if ((AttachWiringImageDelete2.Checked && ExistingWiringFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingWiringFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingWiringFilename2.Value);
                        }
                    }
                    if ((AttachWiringImageDelete3.Checked && ExistingWiringFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingWiringFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingWiringFilename3.Value);
                        }
                    }
                    if ((AttachWiringImageDelete4.Checked && ExistingWiringFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingWiringFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingWiringFilename4.Value);
                        }
                    }
                    if ((AttachWiringImageDelete5.Checked && ExistingWiringFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingWiringFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingWiringFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWireFunction ";
                    sql += "set WireFunctionId = @WireFunctionId, InstallationTypeId=@InstallationTypeId, VehicleColor=@VehicleColor, ";
                    sql += "Colour=@Colour, PinOut=@PinOut, Location=@Location, Polarity=@Polarity, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionList.SelectedValue);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor.Text.Trim();
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Color.Text.Trim();
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut.Text.Trim();
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location.Text.Trim();
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity.Text.Trim();

                    if (AttachWiringImageDelete1.Visible && AttachWiringImageDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingWiringFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachWiringImageDelete2.Visible && AttachWiringImageDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingWiringFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachWiringImageDelete3.Visible && AttachWiringImageDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingWiringFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachWiringImageDelete4.Visible && AttachWiringImageDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingWiringFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachWiringImageDelete5.Visible && AttachWiringImageDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingWiringFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdHidden.Value);

                    cmd.ExecuteNonQuery();

                }

                VehicleColor.Text = "";
                Color.Text = "";
                PinOut.Text = "";
                Location.Text = "";
                Polarity.Text = "";

                SearchInfo(1);
                AddWirePanel.Visible = false;
                EditNotePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            AddWirePanel.Visible = false;
        }

        protected void WireList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Wiring?');";
                    }
                }
            }
        }

        protected void WireList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWireInstall();
            VehicleWireFunctionIdHidden.Value = ((HiddenField)(WireList.SelectedRow.FindControl("VehicleWireFunctionId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "select WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFunction WITH (NOLOCK) ";
                sql += "where VehicleWireFunctionId = @VehicleWireFunctionId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionList.SelectedValue = reader["WireFunctionId"].ToString();
                    InstalltionTypeList.SelectedValue = reader["InstallationTypeId"].ToString();
                    VehicleColor.Text = reader["VehicleColor"].ToString();
                    Color.Text = reader["Colour"].ToString();
                    PinOut.Text = reader["PinOut"].ToString();
                    Location.Text = reader["Location"].ToString();
                    Polarity.Text = reader["Polarity"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachWiringImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingWiringFilename1.Value = reader["Attach1"].ToString();
                        AttachWiringImage1.Visible = true;
                        AttachWiringImageDelete1.Visible = true;
                        ReplaceWiringImageLabel1.Visible = true;

                        AttachWiringImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename1.Value = "";
                        AttachWiringImage1.Visible = false;
                        AttachWiringImageDelete1.Visible = false;
                        ReplaceWiringImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachWiringImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingWiringFilename2.Value = reader["Attach2"].ToString();
                        AttachWiringImage2.Visible = true;
                        AttachWiringImageDelete2.Visible = true;
                        ReplaceWiringImageLabel2.Visible = true;

                        AttachWiringImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename2.Value = "";
                        AttachWiringImage2.Visible = false;
                        AttachWiringImageDelete2.Visible = false;
                        ReplaceWiringImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachWiringImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingWiringFilename3.Value = reader["Attach3"].ToString();
                        AttachWiringImage3.Visible = true;
                        AttachWiringImageDelete3.Visible = true;
                        ReplaceWiringImageLabel3.Visible = true;

                        AttachWiringImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename3.Value = "";
                        AttachWiringImage3.Visible = false;
                        AttachWiringImageDelete3.Visible = false;
                        ReplaceWiringImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachWiringImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingWiringFilename4.Value = reader["Attach4"].ToString();
                        AttachWiringImage4.Visible = true;
                        AttachWiringImageDelete4.Visible = true;
                        ReplaceWiringImageLabel4.Visible = true;

                        AttachWiringImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename4.Value = "";
                        AttachWiringImage4.Visible = false;
                        AttachWiringImageDelete4.Visible = false;
                        ReplaceWiringImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachWiringImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingWiringFilename5.Value = reader["Attach5"].ToString();
                        AttachWiringImage5.Visible = true;
                        AttachWiringImageDelete5.Visible = true;
                        ReplaceWiringImageLabel5.Visible = true;

                        AttachWiringImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename5.Value = "";
                        AttachWiringImage5.Visible = false;
                        AttachWiringImageDelete5.Visible = false;
                        ReplaceWiringImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddWiringTitle.Text = "Edit Wiring";
                AddWireButton.Text = "Save";
                AddWirePanel.Visible = true;
                EditNotePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void WireList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFunctionIdValue = ((HiddenField)(WireList.Rows[e.RowIndex].FindControl("VehicleWireFunctionId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireFunctionId, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFunction a ";
                sql += "where VehicleWireFunctionId=" + VehicleWireFunctionIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireFunction where VehicleWireFunctionId=" + VehicleWireFunctionIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                SearchInfo(1);

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void EditNotePanelButton_Click(object sender, EventArgs e)
        {
            EditNotePanel.Visible = true;
        }

        protected void CancelNoteButton_Click(object sender, EventArgs e)
        {
            EditNotePanel.Visible = false;
        }

        protected void SaveNoteButton_Click(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (VehicleWireNoteIdHidden.Value != "")
                {
                    string sql = "update dbo.VehicleWireNote set Note = @Note, UpdatedDt = getdate(), UpdatedBy = @UpdatedBy where VehicleWireNoteId=@VehicleWireNoteId ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = editor.Text;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    Cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteIdHidden.Value);

                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    string sql = "insert into dbo.VehicleWireNote (VehicleMakeModelYearId, Note, UpdatedBy, UpdatedDt) ";
                    sql += "select @VehicleMakeModelYearId, @Note, @UpdatedBy, getdate()";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                    Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = editor.Text;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    Cmd.ExecuteNonQuery();
                }

                NoteLabel.Text = editor.Text;
                EditNotePanel.Visible = false;
                SearchInfo(1);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddDisassemblyPanelButton_Click(object sender, EventArgs e)
        {
            AddDisassemblyTitle.Text = "Add Disassembly";
            AddDisassemblyButton.Text = "Add";
            VehicleWireDisassemblyIdHidden.Value = "";
            AddDisassemblyPanel.Visible = true;

            DisassemblyStep.Text = "";
            Disassemblyeditor.Text = "";

            AttachDisassemblyImageDelete1.Checked = false;
            AttachDisassemblyImageDelete2.Checked = false;
            AttachDisassemblyImageDelete3.Checked = false;
            AttachDisassemblyImageDelete4.Checked = false;
            AttachDisassemblyImageDelete5.Checked = false;

            ExistingDisassemblyFilename1.Value = "";
            AttachDisassemblyImage1.Visible = false;
            AttachDisassemblyImageDelete1.Visible = false;
            ReplaceDisassemblyImageLabel1.Visible = false;

            ExistingDisassemblyFilename2.Value = "";
            AttachDisassemblyImage2.Visible = false;
            AttachDisassemblyImageDelete2.Visible = false;
            ReplaceDisassemblyImageLabel2.Visible = false;

            ExistingDisassemblyFilename3.Value = "";
            AttachDisassemblyImage3.Visible = false;
            AttachDisassemblyImageDelete3.Visible = false;
            ReplaceDisassemblyImageLabel3.Visible = false;

            ExistingDisassemblyFilename4.Value = "";
            AttachDisassemblyImage4.Visible = false;
            AttachDisassemblyImageDelete4.Visible = false;
            ReplaceDisassemblyImageLabel4.Visible = false;

            ExistingDisassemblyFilename5.Value = "";
            AttachDisassemblyImage5.Visible = false;
            AttachDisassemblyImageDelete5.Visible = false;
            ReplaceDisassemblyImageLabel5.Visible = false;

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void ResetTab()
        {
            WiringPaneltab.Attributes.Add("Class", "tab-title");
            FBPaneltab.Attributes.Add("Class", "tab-title");
            DocumentPaneltab.Attributes.Add("Class", "tab-title");
            linksdisassemblytab.Attributes.Add("Class", "tab-title");
            linkspreptab.Attributes.Add("Class", "tab-title");
            linkroutingtab.Attributes.Add("Class", "tab-title");
            TSBPaneltab.Attributes.Add("Class", "tab-title");

            WiringPanel.Attributes.Add("Class", "content");
            FBPanel.Attributes.Add("Class", "content");
            DocumentPanel.Attributes.Add("Class", "content");
            linksdisassembly.Attributes.Add("Class", "content");
            linksprep.Attributes.Add("Class", "content");
            linkrouting.Attributes.Add("Class", "content");
            TSBPanel.Attributes.Add("Class", "content");
        }

        protected void CancelDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDisassemblyPanel.Visible = false;

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName)
        {
            string DocumentName;
            SaveFile(PFile, FullPath, Prefix, Make, Model, Year, out FileName, out DocumentName);
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName, out string DocumentName)
        {
            FileName = "";
            DocumentName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                DocumentName = filename;
                filename = Prefix + "_" + Make + "_" + Model + "_" + Year + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void AddDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (DisassemblyStep.Text == "")
            {
                ShowError("Please enter Step");
                DisassemblyStep.Focus();
                return;
            }
            if (Disassemblyeditor.Text == "")
            {
                ShowError("Please enter Note");
                Disassemblyeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachDisassemblyFile1.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName1);
            string FileName2;
            SaveFile(AttachDisassemblyFile2.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName2);
            string FileName3;
            SaveFile(AttachDisassemblyFile3.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName3);
            string FileName4;
            SaveFile(AttachDisassemblyFile4.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName4);
            string FileName5;
            SaveFile(AttachDisassemblyFile5.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName5);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWireDisassemblyIdHidden.Value == "")
                {
                    string sql = "insert into dbo.VehicleWireDisassembly (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(DisassemblyStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Disassemblyeditor.Text;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if ((AttachDisassemblyImageDelete1.Checked && ExistingDisassemblyFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename1.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete2.Checked && ExistingDisassemblyFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename2.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete3.Checked && ExistingDisassemblyFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename3.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete4.Checked && ExistingDisassemblyFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename4.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete5.Checked && ExistingDisassemblyFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWireDisassembly ";
                    sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(DisassemblyStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Disassemblyeditor.Text;

                    if (AttachDisassemblyImageDelete1.Visible && AttachDisassemblyImageDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingDisassemblyFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachDisassemblyImageDelete2.Visible && AttachDisassemblyImageDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingDisassemblyFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachDisassemblyImageDelete3.Visible && AttachDisassemblyImageDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingDisassemblyFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachDisassemblyImageDelete4.Visible && AttachDisassemblyImageDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingDisassemblyFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachDisassemblyImageDelete5.Visible && AttachDisassemblyImageDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingDisassemblyFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdHidden.Value);

                    cmd.ExecuteNonQuery();
                }

                SearchInfo(4);
                AddDisassemblyPanel.Visible = false;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void DisassemblyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireDisassemblyIdHidden.Value = ((HiddenField)(DisassemblyList.SelectedRow.FindControl("VehicleWireDisassemblyId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireDisassemblyId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireDisassembly  ";
                sql += "where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    DisassemblyStep.Text = reader["Step"].ToString();
                    Disassemblyeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachDisassemblyImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingDisassemblyFilename1.Value = reader["Attach1"].ToString();
                        AttachDisassemblyImage1.Visible = true;
                        AttachDisassemblyImageDelete1.Visible = true;
                        ReplaceDisassemblyImageLabel1.Visible = true;

                        AttachDisassemblyImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename1.Value = "";
                        AttachDisassemblyImage1.Visible = false;
                        AttachDisassemblyImageDelete1.Visible = false;
                        ReplaceDisassemblyImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachDisassemblyImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingDisassemblyFilename2.Value = reader["Attach2"].ToString();
                        AttachDisassemblyImage2.Visible = true;
                        AttachDisassemblyImageDelete2.Visible = true;
                        ReplaceDisassemblyImageLabel2.Visible = true;

                        AttachDisassemblyImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename2.Value = "";
                        AttachDisassemblyImage2.Visible = false;
                        AttachDisassemblyImageDelete2.Visible = false;
                        ReplaceDisassemblyImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachDisassemblyImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingDisassemblyFilename3.Value = reader["Attach3"].ToString();
                        AttachDisassemblyImage3.Visible = true;
                        AttachDisassemblyImageDelete3.Visible = true;
                        ReplaceDisassemblyImageLabel3.Visible = true;

                        AttachDisassemblyImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename3.Value = "";
                        AttachDisassemblyImage3.Visible = false;
                        AttachDisassemblyImageDelete3.Visible = false;
                        ReplaceDisassemblyImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachDisassemblyImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingDisassemblyFilename4.Value = reader["Attach4"].ToString();
                        AttachDisassemblyImage4.Visible = true;
                        AttachDisassemblyImageDelete4.Visible = true;
                        ReplaceDisassemblyImageLabel4.Visible = true;

                        AttachDisassemblyImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename4.Value = "";
                        AttachDisassemblyImage4.Visible = false;
                        AttachDisassemblyImageDelete4.Visible = false;
                        ReplaceDisassemblyImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachDisassemblyImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingDisassemblyFilename5.Value = reader["Attach5"].ToString();
                        AttachDisassemblyImage5.Visible = true;
                        AttachDisassemblyImageDelete5.Visible = true;
                        ReplaceDisassemblyImageLabel5.Visible = true;

                        AttachDisassemblyImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename5.Value = "";
                        AttachDisassemblyImage5.Visible = false;
                        AttachDisassemblyImageDelete5.Visible = false;
                        ReplaceDisassemblyImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddDisassemblyTitle.Text = "Edit Disassembly";
                AddDisassemblyButton.Text = "Save";
                AddDisassemblyPanel.Visible = true;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void DisassemblyList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireDisassemblyIdValue = ((HiddenField)(DisassemblyList.Rows[e.RowIndex].FindControl("VehicleWireDisassemblyId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireDisassemblyId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireDisassembly a ";
                sql += "where VehicleWireDisassemblyId=" + VehicleWireDisassemblyIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireDisassembly where VehicleWireDisassemblyId=" + VehicleWireDisassemblyIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddDisassemblyPanel.Visible = false;
                SearchInfo(4);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddPrepPanelButton_Click(object sender, EventArgs e)
        {
            AddPrepTitle.Text = "Add Prep";
            AddPrepButton.Text = "Add";
            VehicleWirePrepIdHidden.Value = "";
            AddPrepPanel.Visible = true;

            PrepStep.Text = "";
            Prepeditor.Text = "";

            AttachPrepImageDelete1.Checked = false;
            AttachPrepImageDelete2.Checked = false;
            AttachPrepImageDelete3.Checked = false;
            AttachPrepImageDelete4.Checked = false;
            AttachPrepImageDelete5.Checked = false;

            ExistingPrepFilename1.Value = "";
            AttachPrepImage1.Visible = false;
            AttachPrepImageDelete1.Visible = false;
            ReplacePrepImageLabel1.Visible = false;

            ExistingPrepFilename2.Value = "";
            AttachPrepImage2.Visible = false;
            AttachPrepImageDelete2.Visible = false;
            ReplacePrepImageLabel2.Visible = false;

            ExistingPrepFilename3.Value = "";
            AttachPrepImage3.Visible = false;
            AttachPrepImageDelete3.Visible = false;
            ReplacePrepImageLabel3.Visible = false;

            ExistingPrepFilename4.Value = "";
            AttachPrepImage4.Visible = false;
            AttachPrepImageDelete4.Visible = false;
            ReplacePrepImageLabel4.Visible = false;

            ExistingPrepFilename5.Value = "";
            AttachPrepImage5.Visible = false;
            AttachPrepImageDelete5.Visible = false;
            ReplacePrepImageLabel5.Visible = false;

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");

        }

        protected void CancelPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddPrepPanel.Visible = false;

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");
        }

        protected void AddPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (PrepStep.Text == "")
            {
                ShowError("Please enter Step");
                PrepStep.Focus();
                return;
            }
            if (Prepeditor.Text == "")
            {
                ShowError("Please enter Note");
                Prepeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachPrepFile1.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName1);
            string FileName2;
            SaveFile(AttachPrepFile2.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName2);
            string FileName3;
            SaveFile(AttachPrepFile3.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName3);
            string FileName4;
            SaveFile(AttachPrepFile4.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName4);
            string FileName5;
            SaveFile(AttachPrepFile5.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName5);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWirePrepIdHidden.Value == "")
                {
                    string sql = "insert into dbo.VehicleWirePrep (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(PrepStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Prepeditor.Text;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if ((AttachPrepImageDelete1.Checked && ExistingPrepFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename1.Value);
                        }
                    }
                    if ((AttachPrepImageDelete2.Checked && ExistingPrepFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename2.Value);
                        }
                    }
                    if ((AttachPrepImageDelete3.Checked && ExistingPrepFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename3.Value);
                        }
                    }
                    if ((AttachPrepImageDelete4.Checked && ExistingPrepFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename4.Value);
                        }
                    }
                    if ((AttachPrepImageDelete5.Checked && ExistingPrepFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWirePrep ";
                    sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(PrepStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Prepeditor.Text;

                    if (AttachPrepImageDelete1.Visible && AttachPrepImageDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingPrepFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachPrepImageDelete2.Visible && AttachPrepImageDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingPrepFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachPrepImageDelete3.Visible && AttachPrepImageDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingPrepFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachPrepImageDelete4.Visible && AttachPrepImageDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingPrepFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachPrepImageDelete5.Visible && AttachPrepImageDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingPrepFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdHidden.Value);

                    cmd.ExecuteNonQuery();
                }

                SearchInfo(5);
                AddPrepPanel.Visible = false;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void PrepList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePrepIdHidden.Value = ((HiddenField)(PrepList.SelectedRow.FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePrepId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePrep  ";
                sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    PrepStep.Text = reader["Step"].ToString();
                    Prepeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachPrepImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingPrepFilename1.Value = reader["Attach1"].ToString();
                        AttachPrepImage1.Visible = true;
                        AttachPrepImageDelete1.Visible = true;
                        ReplacePrepImageLabel1.Visible = true;

                        AttachPrepImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename1.Value = "";
                        AttachPrepImage1.Visible = false;
                        AttachPrepImageDelete1.Visible = false;
                        ReplacePrepImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachPrepImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingPrepFilename2.Value = reader["Attach2"].ToString();
                        AttachPrepImage2.Visible = true;
                        AttachPrepImageDelete2.Visible = true;
                        ReplacePrepImageLabel2.Visible = true;

                        AttachPrepImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename2.Value = "";
                        AttachPrepImage2.Visible = false;
                        AttachPrepImageDelete2.Visible = false;
                        ReplacePrepImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachPrepImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingPrepFilename3.Value = reader["Attach3"].ToString();
                        AttachPrepImage3.Visible = true;
                        AttachPrepImageDelete3.Visible = true;
                        ReplacePrepImageLabel3.Visible = true;

                        AttachPrepImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename3.Value = "";
                        AttachPrepImage3.Visible = false;
                        AttachPrepImageDelete3.Visible = false;
                        ReplacePrepImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachPrepImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingPrepFilename4.Value = reader["Attach4"].ToString();
                        AttachPrepImage4.Visible = true;
                        AttachPrepImageDelete4.Visible = true;
                        ReplacePrepImageLabel4.Visible = true;

                        AttachPrepImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename4.Value = "";
                        AttachPrepImage4.Visible = false;
                        AttachPrepImageDelete4.Visible = false;
                        ReplacePrepImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachPrepImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingPrepFilename5.Value = reader["Attach5"].ToString();
                        AttachPrepImage5.Visible = true;
                        AttachPrepImageDelete5.Visible = true;
                        ReplacePrepImageLabel5.Visible = true;

                        AttachPrepImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename5.Value = "";
                        AttachPrepImage5.Visible = false;
                        AttachPrepImageDelete5.Visible = false;
                        ReplacePrepImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddPrepTitle.Text = "Edit Prep";
                AddPrepButton.Text = "Save";
                AddPrepPanel.Visible = true;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void PrepList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePrepIdValue = ((HiddenField)(PrepList.Rows[e.RowIndex].FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWirePrepId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWirePrep a ";
                sql += "where VehicleWirePrepId=" + VehicleWirePrepIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWirePrep where VehicleWirePrepId=" + VehicleWirePrepIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddPrepPanel.Visible = false;
                SearchInfo(5);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void AddRoutingPanelButton_Click(object sender, EventArgs e)
        {
            AddRoutingTitle.Text = "Add Routing/Placement";
            AddRoutingButton.Text = "Add";
            VehicleWirePlacementRoutingIdHidden.Value = "";
            AddRoutingPanel.Visible = true;

            RoutingStep.Text = "";
            Routingeditor.Text = "";

            AttachRoutingImageDelete1.Checked = false;
            AttachRoutingImageDelete2.Checked = false;
            AttachRoutingImageDelete3.Checked = false;
            AttachRoutingImageDelete4.Checked = false;
            AttachRoutingImageDelete5.Checked = false;

            ExistingRoutingFilename1.Value = "";
            AttachRoutingImage1.Visible = false;
            AttachRoutingImageDelete1.Visible = false;
            ReplaceRoutingImageLabel1.Visible = false;

            ExistingRoutingFilename2.Value = "";
            AttachRoutingImage2.Visible = false;
            AttachRoutingImageDelete2.Visible = false;
            ReplaceRoutingImageLabel2.Visible = false;

            ExistingRoutingFilename3.Value = "";
            AttachRoutingImage3.Visible = false;
            AttachRoutingImageDelete3.Visible = false;
            ReplaceRoutingImageLabel3.Visible = false;

            ExistingRoutingFilename4.Value = "";
            AttachRoutingImage4.Visible = false;
            AttachRoutingImageDelete4.Visible = false;
            ReplaceRoutingImageLabel4.Visible = false;

            ExistingRoutingFilename5.Value = "";
            AttachRoutingImage5.Visible = false;
            AttachRoutingImageDelete5.Visible = false;
            ReplaceRoutingImageLabel5.Visible = false;

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");

        }

        protected void CancelRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddRoutingPanel.Visible = false;

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");
        }

        protected void AddRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (RoutingStep.Text == "")
            {
                ShowError("Please enter Step");
                RoutingStep.Focus();
                return;
            }
            if (Routingeditor.Text == "")
            {
                ShowError("Please enter Note");
                Routingeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachRoutingFile1.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName1);
            string FileName2;
            SaveFile(AttachRoutingFile2.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName2);
            string FileName3;
            SaveFile(AttachRoutingFile3.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName3);
            string FileName4;
            SaveFile(AttachRoutingFile4.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName4);
            string FileName5;
            SaveFile(AttachRoutingFile5.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName5);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWirePlacementRoutingIdHidden.Value == "")
                {
                    string sql = "insert into dbo.VehicleWirePlacementRouting (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(RoutingStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Routingeditor.Text;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if ((AttachRoutingImageDelete1.Checked && ExistingRoutingFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename1.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete2.Checked && ExistingRoutingFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename2.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete3.Checked && ExistingRoutingFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename3.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete4.Checked && ExistingRoutingFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename4.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete5.Checked && ExistingRoutingFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWirePlacementRouting ";
                    sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(RoutingStep.Text);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Routingeditor.Text;

                    if (AttachRoutingImageDelete1.Visible && AttachRoutingImageDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingRoutingFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachRoutingImageDelete2.Visible && AttachRoutingImageDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingRoutingFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachRoutingImageDelete3.Visible && AttachRoutingImageDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingRoutingFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachRoutingImageDelete4.Visible && AttachRoutingImageDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingRoutingFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachRoutingImageDelete5.Visible && AttachRoutingImageDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingRoutingFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdHidden.Value);

                    cmd.ExecuteNonQuery();
                }

                SearchInfo(6);
                AddRoutingPanel.Visible = false;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void RoutingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePlacementRoutingIdHidden.Value = ((HiddenField)(RoutingList.SelectedRow.FindControl("VehicleWirePlacementRoutingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePlacementRoutingId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePlacementRouting  ";
                sql += "where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    RoutingStep.Text = reader["Step"].ToString();
                    Routingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachRoutingImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingRoutingFilename1.Value = reader["Attach1"].ToString();
                        AttachRoutingImage1.Visible = true;
                        AttachRoutingImageDelete1.Visible = true;
                        ReplaceRoutingImageLabel1.Visible = true;

                        AttachRoutingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename1.Value = "";
                        AttachRoutingImage1.Visible = false;
                        AttachRoutingImageDelete1.Visible = false;
                        ReplaceRoutingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachRoutingImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingRoutingFilename2.Value = reader["Attach2"].ToString();
                        AttachRoutingImage2.Visible = true;
                        AttachRoutingImageDelete2.Visible = true;
                        ReplaceRoutingImageLabel2.Visible = true;

                        AttachRoutingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename2.Value = "";
                        AttachRoutingImage2.Visible = false;
                        AttachRoutingImageDelete2.Visible = false;
                        ReplaceRoutingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachRoutingImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingRoutingFilename3.Value = reader["Attach3"].ToString();
                        AttachRoutingImage3.Visible = true;
                        AttachRoutingImageDelete3.Visible = true;
                        ReplaceRoutingImageLabel3.Visible = true;

                        AttachRoutingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename3.Value = "";
                        AttachRoutingImage3.Visible = false;
                        AttachRoutingImageDelete3.Visible = false;
                        ReplaceRoutingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachRoutingImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingRoutingFilename4.Value = reader["Attach4"].ToString();
                        AttachRoutingImage4.Visible = true;
                        AttachRoutingImageDelete4.Visible = true;
                        ReplaceRoutingImageLabel4.Visible = true;

                        AttachRoutingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename4.Value = "";
                        AttachRoutingImage4.Visible = false;
                        AttachRoutingImageDelete4.Visible = false;
                        ReplaceRoutingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachRoutingImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingRoutingFilename5.Value = reader["Attach5"].ToString();
                        AttachRoutingImage5.Visible = true;
                        AttachRoutingImageDelete5.Visible = true;
                        ReplaceRoutingImageLabel5.Visible = true;

                        AttachRoutingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename5.Value = "";
                        AttachRoutingImage5.Visible = false;
                        AttachRoutingImageDelete5.Visible = false;
                        ReplaceRoutingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddRoutingTitle.Text = "Edit Routing/Placement";
                AddRoutingButton.Text = "Save";
                AddRoutingPanel.Visible = true;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void RoutingList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePlacementRoutingIdValue = ((HiddenField)(RoutingList.Rows[e.RowIndex].FindControl("VehicleWirePlacementRoutingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWirePlacementRoutingId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWirePlacementRouting a ";
                sql += "where VehicleWirePlacementRoutingId=" + VehicleWirePlacementRoutingIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWirePlacementRouting where VehicleWirePlacementRoutingId=" + VehicleWirePlacementRoutingIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddRoutingPanel.Visible = false;
                SearchInfo(6);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddFBPanelButton_Click(object sender, EventArgs e)
        {
            AddFBTitle.Text = "Add Facebook";
            AddFBButton.Text = "Add";
            VehicleWireFacebookIdHidden.Value = "";
            AddFBPanel.Visible = true;

            FBeditor.Text = "";
            FBUrl.Text = "";

            AttachFBImageDelete1.Checked = false;
            AttachFBImageDelete2.Checked = false;
            AttachFBImageDelete3.Checked = false;
            AttachFBImageDelete4.Checked = false;
            AttachFBImageDelete5.Checked = false;

            ExistingFBFilename1.Value = "";
            AttachFBImage1.Visible = false;
            AttachFBImageDelete1.Visible = false;
            ReplaceFBImageLabel1.Visible = false;

            ExistingFBFilename2.Value = "";
            AttachFBImage2.Visible = false;
            AttachFBImageDelete2.Visible = false;
            ReplaceFBImageLabel2.Visible = false;

            ExistingFBFilename3.Value = "";
            AttachFBImage3.Visible = false;
            AttachFBImageDelete3.Visible = false;
            ReplaceFBImageLabel3.Visible = false;

            ExistingFBFilename4.Value = "";
            AttachFBImage4.Visible = false;
            AttachFBImageDelete4.Visible = false;
            ReplaceFBImageLabel4.Visible = false;

            ExistingFBFilename5.Value = "";
            AttachFBImage5.Visible = false;
            AttachFBImageDelete5.Visible = false;
            ReplaceFBImageLabel5.Visible = false;

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void CancelFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddFBPanel.Visible = false;

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void AddFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (FBeditor.Text == "")
            {
                ShowError("Please enter Facebook Result");
                FBeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachFBFile1.PostedFile, FullPath, "FB", Make, Model, Year, out FileName1);
            string FileName2;
            SaveFile(AttachFBFile2.PostedFile, FullPath, "FB", Make, Model, Year, out FileName2);
            string FileName3;
            SaveFile(AttachFBFile3.PostedFile, FullPath, "FB", Make, Model, Year, out FileName3);
            string FileName4;
            SaveFile(AttachFBFile4.PostedFile, FullPath, "FB", Make, Model, Year, out FileName4);
            string FileName5;
            SaveFile(AttachFBFile5.PostedFile, FullPath, "FB", Make, Model, Year, out FileName5);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWireFacebookIdHidden.Value == "")
                {
                    string sql = "insert into dbo.VehicleWireFacebook (VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeModelYearId, @Note, @URL, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = FBeditor.Text;
                    cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = FBUrl.Text;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if ((AttachFBImageDelete1.Checked && ExistingFBFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename1.Value);
                        }
                    }
                    if ((AttachFBImageDelete2.Checked && ExistingFBFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename2.Value);
                        }
                    }
                    if ((AttachFBImageDelete3.Checked && ExistingFBFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename3.Value);
                        }
                    }
                    if ((AttachFBImageDelete4.Checked && ExistingFBFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename4.Value);
                        }
                    }
                    if ((AttachFBImageDelete5.Checked && ExistingFBFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename5.Value);
                        }
                    }

                    string sql = "update dbo.VehicleWireFacebook ";
                    sql += "set Note=@Note, URL=@URL, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWireFacebookId = @VehicleWireFacebookId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = FBeditor.Text;
                    cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = FBUrl.Text;

                    if (AttachFBImageDelete1.Visible && AttachFBImageDelete1.Checked)
                    {
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingFBFilename1.Value;
                        }
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 200).Value = FileName1;
                    }

                    if (AttachFBImageDelete2.Visible && AttachFBImageDelete2.Checked)
                    {
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingFBFilename2.Value;
                        }
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 200).Value = FileName2;
                    }

                    if (AttachFBImageDelete3.Visible && AttachFBImageDelete3.Checked)
                    {
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingFBFilename3.Value;
                        }
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 200).Value = FileName3;
                    }

                    if (AttachFBImageDelete4.Visible && AttachFBImageDelete4.Checked)
                    {
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingFBFilename4.Value;
                        }
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 200).Value = FileName4;
                    }

                    if (AttachFBImageDelete5.Visible && AttachFBImageDelete5.Checked)
                    {
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingFBFilename5.Value;
                        }
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 200).Value = FileName5;
                    }

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdHidden.Value);

                    cmd.ExecuteNonQuery();
                }

                SearchInfo(2);
                AddFBPanel.Visible = false;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Facebook Result?');";
                    }
                }
            }
        }

        protected void FBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireFacebookIdHidden.Value = ((HiddenField)(FBList.SelectedRow.FindControl("VehicleWireFacebookId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireFacebookId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireFacebook  ";
                sql += "where VehicleWireFacebookId = @VehicleWireFacebookId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    FBeditor.Text = reader["Note"].ToString();
                    FBUrl.Text = reader["URL"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        AttachFBImage1.ImageUrl = SaveFolder + reader["Attach1"].ToString();
                        ExistingFBFilename1.Value = reader["Attach1"].ToString();
                        AttachFBImage1.Visible = true;
                        AttachFBImageDelete1.Visible = true;
                        ReplaceFBImageLabel1.Visible = true;

                        AttachFBImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename1.Value = "";
                        AttachFBImage1.Visible = false;
                        AttachFBImageDelete1.Visible = false;
                        ReplaceFBImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        AttachFBImage2.ImageUrl = SaveFolder + reader["Attach2"].ToString();
                        ExistingFBFilename2.Value = reader["Attach2"].ToString();
                        AttachFBImage2.Visible = true;
                        AttachFBImageDelete2.Visible = true;
                        ReplaceFBImageLabel2.Visible = true;

                        AttachFBImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename2.Value = "";
                        AttachFBImage2.Visible = false;
                        AttachFBImageDelete2.Visible = false;
                        ReplaceFBImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        AttachFBImage3.ImageUrl = SaveFolder + reader["Attach3"].ToString();
                        ExistingFBFilename3.Value = reader["Attach3"].ToString();
                        AttachFBImage3.Visible = true;
                        AttachFBImageDelete3.Visible = true;
                        ReplaceFBImageLabel3.Visible = true;

                        AttachFBImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename3.Value = "";
                        AttachFBImage3.Visible = false;
                        AttachFBImageDelete3.Visible = false;
                        ReplaceFBImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        AttachFBImage4.ImageUrl = SaveFolder + reader["Attach4"].ToString();
                        ExistingFBFilename4.Value = reader["Attach4"].ToString();
                        AttachFBImage4.Visible = true;
                        AttachFBImageDelete4.Visible = true;
                        ReplaceFBImageLabel4.Visible = true;

                        AttachFBImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename4.Value = "";
                        AttachFBImage4.Visible = false;
                        AttachFBImageDelete4.Visible = false;
                        ReplaceFBImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        AttachFBImage5.ImageUrl = SaveFolder + reader["Attach5"].ToString();
                        ExistingFBFilename5.Value = reader["Attach5"].ToString();
                        AttachFBImage5.Visible = true;
                        AttachFBImageDelete5.Visible = true;
                        ReplaceFBImageLabel5.Visible = true;

                        AttachFBImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename5.Value = "";
                        AttachFBImage5.Visible = false;
                        AttachFBImageDelete5.Visible = false;
                        ReplaceFBImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddFBTitle.Text = "Edit Facebook";
                AddFBButton.Text = "Save";
                AddFBPanel.Visible = true;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFacebookIdValue = ((HiddenField)(FBList.SelectedRow.FindControl("VehicleWireFacebookId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireFacebookId, a.URL, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFacebook a ";
                sql += "where VehicleWireFacebookId=" + VehicleWireFacebookIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireFacebook where VehicleWireFacebookId=" + VehicleWireFacebookIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddFBPanel.Visible = false;
                SearchInfo(2);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddDocumentPanelButton_Click(object sender, EventArgs e)
        {
            AddDocumentTitle.Text = "Add Document";
            AddDocumentButton.Text = "Add";
            VehicleDocumentIdHidden.Value = "";
            AddDocumentPanel.Visible = true;

            ExistingDocument.Text = "";
            CurrentDocumentAttachFile.Value = "";

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void DocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[2].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this document?');";
                    }
                }

                HiddenField VehicleDocumentId = e.Row.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Row.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Row.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Row.FindControl("DocumentLabel") as Label;
                    DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                }
            }
        }

        protected void DocumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleDocumentIdHidden.Value = ((HiddenField)(DocumentList.SelectedRow.FindControl("VehicleDocumentId"))).Value;

            HiddenField AttachFile = DocumentList.SelectedRow.FindControl("AttachFile") as HiddenField;
            Label DocumentLabel = DocumentList.SelectedRow.FindControl("DocumentLabel") as Label;

            ExistingDocument.Text = DocumentLabel.Text;
            CurrentDocumentAttachFile.Value = AttachFile.Value;

            AddDocumentTitle.Text = "Edit Document";
            AddDocumentButton.Text = "Save";
            AddDocumentPanel.Visible = true;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void DocumentList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleDocumentIdValue = ((HiddenField)(DocumentList.Rows[e.RowIndex].FindControl("VehicleDocumentId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select AttachFile ";
                sql += "from dbo.VehicleDocument a ";
                sql += "where VehicleDocumentId=@VehicleDocumentId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["AttachFile"].ToString()))
                        {
                            File.Delete(FullPath + reader["AttachFile"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleDocument where VehicleDocumentId=@VehicleDocumentId";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                cmd.ExecuteNonQuery();

                AddDocumentPanel.Visible = false;
                SearchInfo(3);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDocumentPanel.Visible = false;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void AddDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            string DocumentName;
            SaveFile(AttachFile.PostedFile, FullPath, "Document", Make, Model, Year, out FileName1, out DocumentName);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleDocumentIdHidden.Value == "")
                {
                    string sql = "insert into dbo.VehicleDocument (VehicleMakeModelYearId, DocumentName, AttachFile, ";
                    sql += "UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeModelYearId, @DocumentName, @AttachFile, ";
                    sql += "getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                    cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = FileName1;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (File.Exists(FullPath + CurrentDocumentAttachFile.Value))
                    {
                        File.Delete(FullPath + CurrentDocumentAttachFile.Value);
                    }

                    string sql = "update dbo.VehicleDocument ";
                    sql += "set DocumentName=@DocumentName, AttachFile=@AttachFile, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleDocumentId = @VehicleDocumentId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                    cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 200).Value = FileName1;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdHidden.Value);

                    cmd.ExecuteNonQuery();
                }

                SearchInfo(3);
                AddDocumentPanel.Visible = false;

                ResetTab();
                DocumentPaneltab.Attributes.Add("Class", "tab-title active");
                DocumentPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}