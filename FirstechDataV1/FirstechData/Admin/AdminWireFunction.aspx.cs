﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminWireFunction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sortCriteria = "WireFunctionName";
                sortDir = "asc";
                ShowWireFunction();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowWireFunction()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId, WireFunctionName, SortOrder, UpdatedBy, UpdatedDt ";
                sql += "from dbo.WireFunction ";
                if (SearchWireFunctionTxt.Text.Trim() != "")
                {
                    sql += " where WireFunctionName like '%' + @WireFunctionName + '%' ";
                }
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                if (SearchWireFunctionTxt.Text.Trim() != "")
                {
                    Cmd.Parameters.Add("@WireFunctionName", SqlDbType.NVarChar, 100).Value = SearchWireFunctionTxt.Text;
                }

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireFunctionList.DataSource = ds;
                WireFunctionList.DataBind();

                if (WireFunctionList != null && WireFunctionList.HeaderRow != null && WireFunctionList.HeaderRow.Cells.Count > 0)
                {
                    WireFunctionList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    WireFunctionList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    WireFunctionList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    WireFunctionList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";

                    WireFunctionList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void WireFunctionList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            ClearError();
            WireFunctionList.EditIndex = -1;
            ShowWireFunction();
        }

        protected void WireFunctionList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ClearError();

            string WireFunctionId = ((HiddenField)(WireFunctionList.Rows[e.RowIndex].FindControl("WireFunctionIdHidden"))).Value; ;
            string WireFunction = ((TextBox)(WireFunctionList.Rows[e.RowIndex].FindControl("WireFunctionTxt"))).Text;
            string SortOrder = ((TextBox)(WireFunctionList.Rows[e.RowIndex].FindControl("SortOrderTxt"))).Text;

            if (WireFunction == "")
            {
                ShowError("Please enter Wire Function");
                return;
            }
            int SortOrderInt;
            if (!int.TryParse(SortOrder, out SortOrderInt))
            {
                ShowError("Please enter Sort Order");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.WireFunction set WireFunctionName = @WireFunctionName, SortOrder=@SortOrder, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where WireFunctionId=" + WireFunctionId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@WireFunctionName", SqlDbType.NVarChar, 100).Value = WireFunction;
                cmd.Parameters.Add("@SortOrder", SqlDbType.Int).Value = SortOrderInt;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                WireFunctionList.EditIndex = -1;
                ShowWireFunction();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void WireFunctionList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ClearError();
            WireFunctionList.EditIndex = e.NewEditIndex;
            ShowWireFunction();
        }

        protected void WireFunctionList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string WireFunctionId = ((HiddenField)(WireFunctionList.Rows[e.RowIndex].FindControl("WireFunctionIdHidden2"))).Value; ;
            string WireFunction = ((Label)(WireFunctionList.Rows[e.RowIndex].FindControl("WireFunctionLabel"))).Text;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "delete from dbo.WireFunction where WireFunctionId=" + WireFunctionId;
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                ShowWireFunction();
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("The DELETE statement conflicted with the REFERENCE constraint") > 0)
                    ShowError("There are Wireing, Prep, or Disassembly data of " + WireFunction + ". You cannot delete it.");
                else
                    ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void WireFunctionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this WireFunction?');";
                    }
                }
            }

        }

        protected void WireFunctionList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }
            ShowWireFunction();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminWireFunctionAdd");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowWireFunction();
            WireFunctionList.PageIndex = 0;
        }
    }
}