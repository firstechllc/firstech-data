﻿<%@ Page Title="Add Wire Function" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminWireFunctionAdd.aspx.cs" Inherits="FirstechData.Admin.AdminWireFunctionAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Wire Function</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-flow-merge"></i><span>Add Wire Function</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>
            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="WireFunction" CssClass="col-md-2 control-label">Wire Function</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="WireFunction" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="WireFunction" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Wire Function field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="SortOrder" CssClass="col-md-2 control-label">Sort Order</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="SortOrder" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="SortOrder" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Sort Order field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Add" CssClass="button tiny bg-black radius" />
                        <asp:Button runat="server" OnClick="CancelButton_Click" ID="CancelButton" Text="Cancel" CssClass="button tiny bg-black radius" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
