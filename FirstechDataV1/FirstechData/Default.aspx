﻿<%@ Page Title="Firstech Data" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstechData._Default" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .carousel-inner > .item > a > img{
           width:100%;
        }
    </style>
    <div class="row">
        <div class="large-12 columns">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <asp:HyperLink runat="server" ID="MainBannerLink1"><asp:Image runat="server" ID="MainBannerImage1" Visible="false" CssClass="CarouselCss" /></asp:HyperLink>
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption1"></asp:Label>
                        </div>
                    </div>
                    <div class="item">
                        <asp:HyperLink runat="server" ID="MainBannerLink2"><asp:Image runat="server" ID="MainBannerImage2" Visible="false" CssClass="CarouselCss" /></asp:HyperLink>
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption2"></asp:Label>
                        </div>
                    </div>
                    <div class="item">
                        <asp:HyperLink runat="server" ID="MainBannerLink3"><asp:Image runat="server" ID="MainBannerImage3" Visible="false" CssClass="CarouselCss" /></asp:HyperLink>
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption3"></asp:Label>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="large-4 columns">
            <a href="/Events.aspx"><img src="Content/Images/events.jpg" /></a>
        </div>
        <div class="large-4 columns">
            <a href="/Newsfeed.aspx"><img src="Content/Images/newsfeed.jpg" /></a>
        </div>
        <div class="large-4 columns">
            <a href="https://www.facebook.com/groups/compustaridatalinksupportgroup/" target="_blank"><img src="Content/Images/techfeed.jpg" /></a>
        </div>
        <!-- /.box -->
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorLabel" />
        </p>
    </div>
</asp:Content>
