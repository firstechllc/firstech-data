﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowInfo();
            }
        }

        private void ShowInfo()
        {
            SqlConnection Con = null;
            try
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select MainBannerCaption1, MainBannerImage1, MainBannerURL1, MainBannerCaption2, MainBannerImage2, MainBannerURL2, ";
                sql += "MainBannerCaption3, MainBannerImage3, MainBannerURL3, LeftBannerCaption, LeftBannerImage, LeftBannerURL, ";
                sql += "MiddleBannerCaption, MiddleBannerImage, MiddleBannerURL, RightBannerCaption, RightBannerImage, RightBannerURL ";
                sql += "from dbo.Homepage ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["MainBannerCaption1"] != null)
                        MainBannerCaption1.Text = reader["MainBannerCaption1"].ToString();
                    if (reader["MainBannerURL1"] != null)
                        MainBannerLink1.NavigateUrl = reader["MainBannerURL1"].ToString();
                    if (reader["MainBannerImage1"] != null && reader["MainBannerImage1"].ToString() != "")
                    {
                        MainBannerImage1.ImageUrl = SaveFolder + reader["MainBannerImage1"].ToString();
                        MainBannerImage1.Visible = true;
                    }

                    if (reader["MainBannerCaption2"] != null)
                        MainBannerCaption2.Text = reader["MainBannerCaption2"].ToString();
                    else
                        MainBannerCaption2.Text = MainBannerCaption1.Text;
                    if (reader["MainBannerURL2"] != null)
                        MainBannerLink2.NavigateUrl = reader["MainBannerURL2"].ToString();
                    else
                        MainBannerLink2.NavigateUrl = MainBannerLink1.NavigateUrl;
                    if (reader["MainBannerImage2"] != null && reader["MainBannerImage2"].ToString() != "")
                    {
                        MainBannerImage2.ImageUrl = SaveFolder + reader["MainBannerImage2"].ToString();
                        MainBannerImage2.Visible = true;
                    }
                    else
                    {
                        MainBannerImage2.ImageUrl = MainBannerImage1.ImageUrl;
                        MainBannerImage2.Visible = true;
                    }

                    if (reader["MainBannerCaption3"] != null)
                        MainBannerCaption3.Text = reader["MainBannerCaption3"].ToString();
                    else
                        MainBannerCaption3.Text = MainBannerCaption1.Text;
                    if (reader["MainBannerURL3"] != null)
                        MainBannerLink3.NavigateUrl = reader["MainBannerURL3"].ToString();
                    else
                        MainBannerLink3.NavigateUrl = MainBannerLink1.NavigateUrl;
                    if (reader["MainBannerImage3"] != null && reader["MainBannerImage3"].ToString() != "")
                    {
                        MainBannerImage3.ImageUrl = SaveFolder + reader["MainBannerImage3"].ToString();
                        MainBannerImage3.Visible = true;
                    }
                    else
                    {
                        MainBannerImage3.ImageUrl = MainBannerImage1.ImageUrl;
                        MainBannerImage3.Visible = true;
                    }

                    /*
                    if (reader["LeftBannerCaption"] != null)
                        LeftBannerCaption.Text = reader["LeftBannerCaption"].ToString();
                    if (reader["LeftBannerURL"] != null)
                        LeftAttachImage.PostBackUrl = reader["LeftBannerURL"].ToString();
                    if (reader["LeftBannerImage"] != null && reader["LeftBannerImage"].ToString() != "")
                    {
                        LeftAttachImage.ImageUrl = SaveFolder + reader["LeftBannerImage"].ToString();
                        LeftAttachImage.Visible = true;
                    }

                    if (reader["MiddleBannerCaption"] != null)
                        MiddleBannerCaption.Text = reader["MiddleBannerCaption"].ToString();
                    if (reader["MiddleBannerURL"] != null)
                        MiddleAttachImage.PostBackUrl = reader["MiddleBannerURL"].ToString();
                    if (reader["MiddleBannerImage"] != null && reader["MiddleBannerImage"].ToString() != "")
                    {
                        MiddleAttachImage.ImageUrl = SaveFolder + reader["MiddleBannerImage"].ToString();
                        MiddleAttachImage.Visible = true;
                    }

                    if (reader["RightBannerCaption"] != null)
                        RightBannerCaption.Text = reader["RightBannerCaption"].ToString();
                    if (reader["RightBannerURL"] != null)
                        RightAttachImage.PostBackUrl = reader["RightBannerURL"].ToString();
                    if (reader["RightBannerImage"] != null && reader["RightBannerImage"].ToString() != "")
                    {
                        RightAttachImage.ImageUrl = SaveFolder + reader["RightBannerImage"].ToString();
                        RightAttachImage.Visible = true;
                    }
                    */
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }
    }
}