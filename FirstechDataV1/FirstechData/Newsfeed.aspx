﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Newsfeed.aspx.cs" Inherits="FirstechData.Newsfeed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="row" style="margin-top: -20px">
                <div class="large-12 columns">
                    <div class="box">
                        <div class="box-header bg-transparent">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                            </div>
                            <h3 class="box-title"><i class="icon-document"></i>
                                <span style="color: black; font-size: medium"><b>NEWSFEED</b></span>
                            </h3>
                        </div>
                        <div class="box-body " style="display: block;">
                            <div class="row">
                                <div class="large-12 columns">
                                    <asp:ListView runat="server" ID="NewsFeedList">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="large-12 columns">
                                                    <article class="reading-nest">
                                                        <h4><a href='<%# DataBinder.Eval(Container.DataItem, "NewsfeedUrl") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "NewsfeedTitle") %></a></h4>
                                                        <h6><%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %></h6>
                                                        <p>
                                                            <asp:Literal runat="server" ID="ContentLabel" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                                        </p>
                                                    </article>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <asp:DataPager ID="NewsFeedListPager" runat="server" PagedControlID="NewsFeedList" PageSize="5" OnPreRender="NewsFeedListPager_PreRender">
                                        <Fields>
                                            <asp:NumericPagerField ButtonType="Link" ButtonCount="10" />
                                        </Fields>
                                    </asp:DataPager>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
