﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class Newsfeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowNewsFeed();
            }
        }

        private void ShowNewsFeed()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select NewsfeedId, NewsfeedTitle, NewsfeedUrl, NewsfeedContent, NewsfeedDt ";
                sql += "from dbo.Newsfeed ";
                sql += "order by NewsfeedDt desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                NewsFeedList.DataSource = ds;
                NewsFeedList.DataBind();
            }
            catch (Exception ex)
            {
                //ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void NewsFeedListPager_PreRender(object sender, EventArgs e)
        {
            ShowNewsFeed();
        }

    }
}