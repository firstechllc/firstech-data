﻿<%@ Page Title="Document" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="FirstechData.Document" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <h3 class="box-title"><i class="icon-document"></i>
                <span style="color: black; font-size: medium"><b>DOCUMENTS</b></span>
                <asp:HiddenField runat="server" ID="SaveFolderHidden" />
                <asp:HiddenField runat="server" ID="DocumentId" />
            </h3>
        </div>
            <div class="row">
                <div class="large-12 columns" style="padding-left:40px;">
                    <asp:Label runat="server" ID="CurrentFolder"></asp:Label>
                </div>
            </div>
        <div class="box-body " style="display: block;">
            <div class="row">
                <div class="large-12 columns">
                    <h5>
                        <asp:Literal runat="server" ID="CurrentFolderName"></asp:Literal></h5>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:DataList runat="server" ID="DocumentList" BorderWidth="0px" RepeatDirection="Vertical" RepeatColumns="1" OnItemDataBound="DocumentList_ItemDataBound">
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:HiddenField ID="DocumentIdHidden2" runat="server" Value='<%# Bind("DocumentId") %>' />
                            <asp:HiddenField ID="DocumentType" runat="server" Value='<%# Bind("DocumentType") %>' />
                            <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                            <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                            <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BackColor="White" />
                        <AlternatingItemStyle BackColor="White" />
                    </asp:DataList>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
