﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace FirstechData
{
    public partial class Document : System.Web.UI.Page
    {
        HashSet<string> AllowedIPs = new HashSet<string>()
        {
            "50.149.87.214",
            "198.22.123.14",
            "198.22.123.79",
            "198.22.123.103",
            "198.22.123.104",
            "198.22.123.105",
            "198.22.123.108",
            "198.22.123.109",
            "198.22.122.4",
            "70.60.1.174", 
            "199.60.113.30",
        };

        IPAddressRange IPRange1 = new IPAddressRange(IPAddress.Parse("198.22.122.0"), IPAddress.Parse("198.22.122.24"));
        IPAddressRange IPRange2 = new IPAddressRange(IPAddress.Parse("168.94.245.0"), IPAddress.Parse("168.94.245.24"));
        IPAddressRange IPRange3 = new IPAddressRange(IPAddress.Parse("168.94.239.0"), IPAddress.Parse("168.94.239.24"));

        protected void Page_Load(object sender, EventArgs e)
        {
            string ipAddress = IPAddressRange.GetIPAddress();
            IPAddress ipaddr = IPAddress.Parse(ipAddress);

            if (!AllowedIPs.Contains(ipAddress) && !IPRange1.IsInRange(ipaddr) && !IPRange2.IsInRange(ipaddr) && !IPRange3.IsInRange(ipaddr) && !User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FSearch");
            }

            if (!IsPostBack)
            {
                Master.ChangeMenuCss("DocumentMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Request["Id"] != null && Request["Id"] != "")
                    DocumentId.Value = Request["Id"].ToString();
                else
                    DocumentId.Value = "0";

                ShowDocument();
            }

        }

        private void ShowDocument()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (DocumentId.Value == "0")
                {
                    CurrentFolder.Text = "";
                }
                else
                {
                    string Path = "";
                    FilePath(Con, int.Parse(DocumentId.Value), ref Path);
                    CurrentFolder.Text = Path;
                }



                string sql = "select DocumentName from dbo.Document where DocumentId = @DocumentId ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = int.Parse(DocumentId.Value);

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    CurrentFolderName.Text = reader["DocumentName"].ToString();
                }
                reader.Close();

                sql = "select DocumentId, ParentDocumentId, DocumentName, ";
                sql += "DocumentType, DocumentTypeStr = case when DocumentType = 0 then 'Folder' else 'File' end, AttachFile ";
                sql += "from dbo.Document ";
                sql += "where ParentDocumentId = @ParentDocumentId ";
                sql += "order by DocumentType, DocumentName";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@ParentDocumentId", SqlDbType.Int).Value = int.Parse(DocumentId.Value);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                DocumentList.DataSource = ds;
                DocumentList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void FilePath(SqlConnection Con, int DocumentId, ref string Path)
        {
            string sql = "select ParentDocumentId, DocumentName from dbo.Document ";
            sql += "where DocumentId = @DocumentId and DocumentType = 0";

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@DocumentId", SqlDbType.Int).Value = DocumentId;

            int ParentDocumentId = 0;
            string DocumentName = "";
            SqlDataReader reader = Cmd.ExecuteReader();
            if (reader.Read())
            {
                ParentDocumentId = int.Parse(reader["ParentDocumentId"].ToString());
                DocumentName = reader["DocumentName"].ToString();
            }

            reader.Close();

            if (ParentDocumentId == 0)
            {
                Path = " > <a href='/Search/Document?Id=" + DocumentId + "'>" + DocumentName + "</a> " + Path;
                Path = "<a href='/Search/Document'>/</a>" + Path;
            }
            else
            {
                Path = " > <a href='/Search/Document?Id=" + DocumentId + "'>" + DocumentName + "</a> " + Path;
                FilePath(Con, ParentDocumentId, ref Path);
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void DocumentList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField DocumentIdHidden2 = e.Item.FindControl("DocumentIdHidden2") as HiddenField;
                HiddenField DocumentType = e.Item.FindControl("DocumentType") as HiddenField;
                HiddenField DocumentName = e.Item.FindControl("DocumentName") as HiddenField;
                HiddenField AttachFile = e.Item.FindControl("AttachFile") as HiddenField;
                Label DocumentLabel = e.Item.FindControl("DocumentLabel") as Label;

                if (DocumentType.Value == "0")
                {
                    DocumentLabel.Text = "<a href='/Search/Document?Id=" + DocumentIdHidden2.Value + "'>[" + DocumentName.Value + "]</a>";
                }
                else
                {
                    DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                }
            }
        }
    }
}