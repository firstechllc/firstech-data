﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using FirstechData.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;


namespace FirstechData
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            var user = manager.FindByEmail(Context.User.Identity.GetUserName());
            if (user != null)
            {
                var roles = manager.GetRoles(user.Id);
                if (user.Approved)
                {
                    if (roles.Contains("Administrator"))
                    {
                        AdminMenu.Visible = true;
                    }
                    else
                    {
                        AdminMenu.Visible = false;
                    }
                }
                else
                {
                    AdminMenu.Visible = false;
                }
            }
            else
            {
                AdminMenu.Visible = false;
            }

            //ShowInfo();
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public void ChangeMenuCss(string menuId)
        {
            if (menuId == "SearchByVehicleMenu")
            {
                SearchByVehicleMenu.Attributes["class"] = "menu-selected";
            }
            else if (menuId == "ContactMenu")
            {
                ContactMenu.Attributes["class"] = "menu-selected";
            }
            else if (menuId == "DocumentMenu")
            {
                DocumentMenu.Attributes["class"] = "menu-selected";
            }
        }

        /*
        private void ShowInfo()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select AnnouncementId, AnnouncementTitleSummary = case when len(AnnouncementTitle) > 23 then substring(AnnouncementTitle, 0, 23) + '...' else AnnouncementTitle end, ";
                sql += "AnnouncementTitle, AnnouncementContent, StartDate, EndDate ";
                sql += "from dbo.Announcement ";
                sql += "where IsActive=1 and StartDate <= @Date and EndDate >= @Date ";
                sql += "order by EndDate";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                Cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = d;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AnnouncementList.DataSource = ds;
                AnnouncementList.DataBind();

            }
            catch (Exception ex)
            {
                //ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }
        */
    }

}