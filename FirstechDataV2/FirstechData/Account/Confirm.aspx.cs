﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using FirstechData.Models;

namespace FirstechData.Account
{
    public partial class Confirm : Page
    {
        protected string StatusMessage
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string code = IdentityHelper.GetCodeFromRequest(Request);
            string userId = IdentityHelper.GetUserIdFromRequest(Request);
            if (code != null && userId != null)
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var result = manager.ConfirmEmail(userId, code);
                if (result.Succeeded)
                {
                    successPanel.Visible = true;

                    // Send confirmed email to administrator
                    string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                    var admin = manager.FindByEmail(AdminEmail);

                    var user = manager.FindById(userId);
                    string emailConfirmedMsg = user.Email + " (" + user.FirstName + " " + user.LastName + ") is confirmed.";
                    manager.SendEmailAsync(admin.Id, "Email Confirmed", emailConfirmedMsg);

                    return;
                }
            }
            successPanel.Visible = false;
            errorPanel.Visible = true;
        }
    }
}