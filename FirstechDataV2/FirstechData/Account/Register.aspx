﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FirstechData.Account.Register" Async="true" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="#"><span class="entypo-home"></span></a></li>
        <li><a href="#">Register</a></li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <h3 class="box-title"><i class="entypo-user-add"></i>
                <span>Create a new account</span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <p class="text-danger"><asp:Literal runat="server" ID="ErrorMessage" /></p>
            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        Please fill out the form below to apply for a FirstechData account. FirstechData is currently only available for authorized Firstech dealers, distributors, and strategic partners. For more information, please contact us (<a href="mailto:cstocklin@compustar.com">cstocklin@compustar.com</a>)
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The email field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The password field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" 
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                        <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The First Name field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"  Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Last Name field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="StoreName" CssClass="col-md-2 control-label">Store Name</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="StoreName" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="StoreName" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Store Name field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Address" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Address field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="City" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The City field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="State" CssClass="form-control" MaxLength="20" Width="100"/>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="State" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The State field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Zip" CssClass="col-md-2 control-label">Zip</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Zip" CssClass="form-control" MaxLength="20" Width="100"/>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Zip" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Zip field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Phone" CssClass="col-md-2 control-label">Phone</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Phone" CssClass="form-control" MaxLength="20" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Phone" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Phone field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="CreateUser_Click" Text="Submit" CssClass="btn btn-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        Upon submission, we will send you a confirmation message to your e-mail. Please click the link in the e-mail to confirm your registration, then please allow up to 2 business days for your account to be approved.
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
