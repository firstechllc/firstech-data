﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using FirstechData.Models;
using System.Text;

namespace FirstechData.Account
{
    public partial class Register : Page
    {
        protected async void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { 
                UserName = Email.Text, 
                Email = Email.Text,
                FirstName = FirstName.Text,
                LastName = LastName.Text,
                StoreName = StoreName.Text,
                Address = Address.Text,
                City = City.Text,
                State = State.Text,
                ZipCode = Zip.Text,
                Phone = Phone.Text,
            };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                string code = manager.GenerateEmailConfirmationToken(user.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);

                StringBuilder mailContent = new StringBuilder();
                mailContent.AppendLine("<!doctype html><html><head><title>Thank you for applying for Firstech Data</title></head>");
                mailContent.AppendLine("<body style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: arial, helvetica, sans-serif; color: #222222; font-size: 15px; background: #F5F5F5; line-height: 125%;\">");
                mailContent.AppendLine("	<table class=\"container\" width=\"100%\" bgcolor=\"#F5F5F5\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse;\">");
                mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
                mailContent.AppendLine("		<tr><td align=\"center\" class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
                mailContent.AppendLine("				<a href=\"http://www.firstechdata.com\" target=\"_blank\">");
                mailContent.AppendLine("					<img src=\"https://mlsvc01-prod.s3.amazonaws.com/b86ab128001/2e689763-98c4-4fd2-a038-facddf1e3b9c.png?ver=1466454321000\" alt=\"firstech data logo\" width=\"250\" height=\"87\">");
                mailContent.AppendLine("				</a></td></tr>");
                mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
                mailContent.AppendLine("		<tr><td align=\"center\">");
                mailContent.AppendLine("				<table class=\"content\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"table-layout:fixed; box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC;\">");
                mailContent.AppendLine("					<tr><td class=\"content-cell\" align=\"left\" style=\"word-break:break-all; padding: 15px 30px 15px 30px;\">");
                mailContent.AppendLine("							<p><b>Thank you registering for Firstech Data!</b></p>");
                mailContent.AppendLine("							<p><a class=\"confirmation-link\" href=\"" + callbackUrl + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Confirm My E-mail</a></p>");
                mailContent.AppendLine("							<p><font size='-1'>If you can't click on the link, just copy and paste it in your browser - " + callbackUrl + "</font></p>");
                mailContent.AppendLine("							<p>Please click the link above to confirm your e-mail so that we may begin reviewing your registration. Upon approval, you will receive confirmation that your account is active.</p>");
                mailContent.AppendLine("							<p>Please allow up to 2 business days for activation.</p>");
                mailContent.AppendLine("						</td></tr></table></td></tr>");
                mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
                mailContent.AppendLine("		<tr><td align=\"center\">");
                mailContent.AppendLine("				<table class=\"content secondary\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC; color: #888888; font-size: 12px;\">");
                mailContent.AppendLine("					<tr><td class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
                mailContent.AppendLine("							<p><a class=\"footer-link\" href=\"http://www.firstechdata.com\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">FirstechData.com</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
                mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Log In</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
                mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Register\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Register</a></p>");
                mailContent.AppendLine("							<p>Technical Support: (888)820-3690</p>");
                mailContent.AppendLine("							<p>Hours: Mon-Fri: 7AM-5PM PST</p>");
                mailContent.AppendLine("							<p>&copy; 2016 Firstech, LLC. All rights reserved. </p>");
                mailContent.AppendLine("						</td></tr></table></td></tr>");
                mailContent.AppendLine("	</table></body></html>");

                //await manager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");
                await manager.SendEmailAsync(user.Id, "Confirm your E-mail", mailContent.ToString());

                //signInManager.SignIn( user, isPersistent: false, rememberBrowser: false);
                //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                IdentityHelper.RedirectToReturnUrl("~/Account/RegistrationComplete", Response);
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}