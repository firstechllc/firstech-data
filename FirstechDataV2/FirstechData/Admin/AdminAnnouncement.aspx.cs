﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminAnnouncement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sortCriteria = "AnnouncementId";
                sortDir = "desc";

                Master.ChangeMenuCss("AnnouncementMenu");
                ShowModel();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowModel()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select AnnouncementId, AnnouncementTitle, StartDate, EndDate, ";
                sql += "Active=Case when IsActive=1 then 'Yes' else 'No' end, UpdatedBy, UpdatedDt ";
                sql += "from dbo.Announcement ";
                if (SearchTxt.Text.Trim() != "")
                {
                    sql += " where AnnouncementTitle like '%' + @SearchTxt + '%' OR AnnouncementContent like '%' + @SearchTxt + '%' ";
                }
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                if (SearchTxt.Text.Trim() != "")
                {
                    Cmd.Parameters.Add("@SearchTxt", SqlDbType.NVarChar, 100).Value = SearchTxt.Text;
                }

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AnnouncementList.DataSource = ds;
                AnnouncementList.DataBind();

                if (AnnouncementList != null && AnnouncementList.HeaderRow != null && AnnouncementList.HeaderRow.Cells.Count > 0)
                {
                    AnnouncementList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    AnnouncementList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    AnnouncementList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    AnnouncementList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    AnnouncementList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    AnnouncementList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";

                    AnnouncementList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }


        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminAnnouncementAdd");
        }

        protected void AnnouncementList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AnnouncementList.PageIndex = e.NewPageIndex;
            ShowModel();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowModel();
            AnnouncementList.PageIndex = 0;
        }
    }
}