﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminNewsfeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("NewsfeedMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                FullPathHidden.Value = HttpContext.Current.Server.MapPath(SaveFolderHidden.Value);

                ShowNewsFeed();
            }
        }

        private void ShowNewsFeed()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select NewsfeedId, NewsfeedTitle, NewsfeedUrl, NewsfeedContent, NewsfeedDt, NewsfeedImage ";
                sql += "from dbo.Newsfeed ";
                sql += "order by NewsfeedDt desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                NewsList.DataSource = ds;
                NewsList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddNewsButton_Click(object sender, EventArgs e)
        {
            AddNewsTitle.Text = "Add News";
            SaveButton.Text = "Add";

            ThisNewsIdHidden.Value = "";
            TitleTxt.Text = "";
            UrlTxt.Text = "";
            NewsfeedDate.Text = "";
            SummaryTxt.Text = "";

            NewPanel.Visible = true;
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            DateTime dt;
            if (!DateTime.TryParse(NewsfeedDate.Text, out dt))
            {
                ShowError("Invalid Date");
                NewsfeedDate.Focus();
                return;
            }

            string FileName1 = "";
            SaveFile(NewsfeedImage.PostedFile, "Newsfeed", out FileName1);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (ThisNewsIdHidden.Value == "")
                {
                    string sql = "insert into dbo.Newsfeed (NewsfeedTitle, NewsfeedUrl, NewsfeedDt, NewsfeedContent, NewsfeedImage, UpdatedDt, UpdatedBy) ";
                    sql += "select @NewsfeedTitle, @NewsfeedUrl, @NewsfeedDt, @NewsfeedContent, @NewsfeedImage, getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@NewsfeedTitle", SqlDbType.NVarChar, 200).Value = TitleTxt.Text;
                    cmd.Parameters.Add("@NewsfeedUrl", SqlDbType.VarChar, 500).Value = UrlTxt.Text;
                    cmd.Parameters.Add("@NewsfeedDt", SqlDbType.Date).Value = dt;
                    cmd.Parameters.Add("@NewsfeedContent", SqlDbType.NVarChar).Value = SummaryTxt.Text;
                    cmd.Parameters.Add("@NewsfeedImage", SqlDbType.NVarChar, 500).Value = FileName1;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (FileName1 != "")
                    {
                        string query = "select NewsfeedImage from dbo.Newsfeed where NewsfeedId = @NewsfeedId";
                        SqlCommand Cmd2 = new SqlCommand(query, conn);
                        Cmd2.CommandType = CommandType.Text;

                        Cmd2.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = int.Parse(ThisNewsIdHidden.Value);

                        SqlDataReader reader = Cmd2.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["NewsfeedImage"] != null && reader["NewsfeedImage"].ToString() != "")
                            {
                                string FilePath = FullPathHidden.Value + reader["NewsfeedImage"].ToString();
                                if (File.Exists(FilePath))
                                {
                                    File.Delete(FilePath);
                                }
                            }
                        }
                        reader.Close();
                    }

                    string sql = "update dbo.Newsfeed set NewsfeedTitle=@NewsfeedTitle, NewsfeedUrl=@NewsfeedUrl, ";
                    sql += "NewsfeedDt=@NewsfeedDt, NewsfeedContent=@NewsfeedContent, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                    if (FileName1 != "")
                    {
                        sql += " ,NewsfeedImage=@NewsfeedImage ";
                    }
                    sql += "where NewsfeedId = @NewsfeedId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@NewsfeedTitle", SqlDbType.NVarChar, 200).Value = TitleTxt.Text;
                    cmd.Parameters.Add("@NewsfeedUrl", SqlDbType.VarChar, 500).Value = UrlTxt.Text;
                    cmd.Parameters.Add("@NewsfeedDt", SqlDbType.Date).Value = dt;
                    cmd.Parameters.Add("@NewsfeedContent", SqlDbType.NVarChar).Value = SummaryTxt.Text;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    if (FileName1 != "")
                    {
                        cmd.Parameters.Add("@NewsfeedImage", SqlDbType.NVarChar, 500).Value = FileName1;
                    }
                    cmd.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = int.Parse(ThisNewsIdHidden.Value);

                    cmd.ExecuteNonQuery();

                }

                ShowNewsFeed();
                NewPanel.Visible = false;

                TitleTxt.Text = "";
                UrlTxt.Text = "";
                NewsfeedDate.Text = "";
                SummaryTxt.Text = "";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        private void SaveFile(HttpPostedFile PFile, string Prefix, out string FileName)
        {
            FileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                filename = Prefix + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPathHidden.Value + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            NewPanel.Visible = false;
        }

        protected void NewsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton DeleteButton = e.Row.FindControl("DeleteButton") as LinkButton;
                if (DeleteButton != null)
                {
                    DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Newsfeed?');";
                }
                HiddenField NFUrl = e.Row.FindControl("NFUrl") as HiddenField;
                HiddenField NFTitle = e.Row.FindControl("NFTitle") as HiddenField;
                HiddenField NFImage = e.Row.FindControl("NFImage") as HiddenField;
                HyperLink NFLink = e.Row.FindControl("NFLink") as HyperLink;
                Label NFLabel = e.Row.FindControl("NFLabel") as Label;
                Image ContentImage = e.Row.FindControl("ContentImage") as Image;

                if (NFLink != null)
                {
                    if (NFUrl.Value.Trim() != "")
                    {
                        NFLink.NavigateUrl = NFUrl.Value;
                        NFLink.Text = NFTitle.Value;

                        NFLabel.Visible = false;
                        NFLink.Visible = true;
                    }
                    else
                    {
                        NFLabel.Text = NFTitle.Value;

                        NFLabel.Visible = true;
                        NFLink.Visible = false;
                    }
                }

                if (NFImage.Value != "")
                {
                    ContentImage.ImageUrl = SaveFolderHidden.Value + NFImage.Value;
                }

                if (e.Row.Cells[2].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[2].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this News?');";
                    }
                }
            }
        }

        protected void NewsList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField NewsfeedId = NewsList.Rows[e.RowIndex].FindControl("NewsfeedId") as HiddenField;
            if (NewsfeedId != null)
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    DeleteNews(conn, int.Parse(NewsfeedId.Value));

                    ShowNewsFeed();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }

        private void DeleteNews(SqlConnection Con, int NewsfeedId)
        {
            string sql = "select NewsfeedImage from dbo.Newsfeed where NewsfeedId = @NewsfeedId";
            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = NewsfeedId;

            SqlDataReader reader = Cmd.ExecuteReader();
            if (reader.Read())
            {
                if (reader["NewsfeedImage"] != null && reader["NewsfeedImage"].ToString() != "")
                {
                    string FilePath = FullPathHidden.Value + reader["NewsfeedImage"].ToString();
                    if (File.Exists(FilePath))
                    {
                        File.Delete(FilePath);
                    }
                }
            }
            reader.Close();

            sql = "delete from dbo.Newsfeed where NewsfeedId = @NewsfeedId";
            Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@NewsfeedId", SqlDbType.Int).Value = NewsfeedId;

            Cmd.ExecuteNonQuery();
        }

        protected void NewsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HiddenField NewsfeedId = NewsList.SelectedRow.FindControl("NewsfeedId") as HiddenField;
            if (NewsfeedId != null)
            {
                AddNewsTitle.Text = "Edit News";
                SaveButton.Text = "Save";
                ThisNewsIdHidden.Value = NewsfeedId.Value;

                HiddenField NFTitle = NewsList.SelectedRow.FindControl("NFTitle") as HiddenField;
                HiddenField NFUrl = NewsList.SelectedRow.FindControl("NFUrl") as HiddenField;
                HiddenField NFDt = NewsList.SelectedRow.FindControl("NFDt") as HiddenField;
                HiddenField NFContent = NewsList.SelectedRow.FindControl("NFContent") as HiddenField;

                TitleTxt.Text = NFTitle.Value;
                UrlTxt.Text = NFUrl.Value;
                NewsfeedDate.Text = NFDt.Value;
                SummaryTxt.Text = NFContent.Value;

                NewPanel.Visible = true;
            }
        }

        protected void NewsList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            NewsList.PageIndex = e.NewPageIndex;
            ShowNewsFeed();
        }


    }
}