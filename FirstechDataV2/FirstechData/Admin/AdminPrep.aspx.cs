﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminPrep : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MakeId.Value = Request["Make"];
                ModelId.Value = Request["Model"];
                YearId.Value = Request["Year"];

                LoadVehicleMake(MakeId.Value);
            }
        }

        private void LoadVehicleMake(string MakeIdPreSelect)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName from dbo.VehicleMake with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("", "-1"));

                if (MakeIdPreSelect != "")
                {
                    VehicleMakeList.SelectedValue = MakeIdPreSelect;
                    ChangeModelList(ModelId.Value);
                }
                else
                {
                    PrepList.DataSource = null;
                    PrepList.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            ChangeModelList("");
        }

        protected void ChangeModelList(string ModelIdPreselect)
        {
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " order by VehicleModelName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("", "-1"));

                if (ModelIdPreselect != "")
                {
                    VehicleModelList.SelectedValue = ModelIdPreselect;
                    ChangeYearList(YearId.Value);
                }
                else
                {
                    PrepList.DataSource = null;
                    PrepList.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            ChangeYearList("");
        }

        protected void ChangeYearList(string YearPreselect) 
        {
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " and a.VehicleModelId = " + VehicleModelList.SelectedValue;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("", "-1"));

                if (YearPreselect != "")
                {
                    VehicleYearList.SelectedValue = YearPreselect;
                    Search();
                }
                else
                {
                    PrepList.DataSource = null;
                    PrepList.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            Search();
        }

        protected void Search()
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                return;
            }
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select a.VehicleWirePrepId, a.Step, a.Note ";
                sql += "from dbo.VehicleWirePrep a ";
                sql += "join dbo.VehicleMakeModelYear b on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId ";
                sql += "where b.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " and b.VehicleModelId = " + VehicleModelList.SelectedValue;
                sql += " and b.VehicleYear = " + VehicleYearList.SelectedValue;
                sql += " order by a.Step ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                PrepList.DataSource = ds;
                PrepList.DataBind();

                if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
                {
                    PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    PrepList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";

                    PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //WiringTitle.Visible = true;
                }
                else
                {
                    //ShowError("No wiring data available");
                    //WiringTitle.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                return;
            }

            Response.Redirect("/Admin/AdminPrepAdd?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue);
        }

        protected void PrepList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string VehicleWirePrepId = ((HiddenField)(PrepList.SelectedRow.FindControl("VehicleWirePrepId"))).Value;
            Response.Redirect("/Admin/AdminPrepAdd?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue + "&ID=" + VehicleWirePrepId);
        }

        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Prep?');";
                    }
                }
            }
        }

        protected void PrepList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePrepId = ((HiddenField)(PrepList.Rows[e.RowIndex].FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWirePrepId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWirePrep a ";
                sql += "where VehicleWirePrepId=" + VehicleWirePrepId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWirePrep where VehicleWirePrepId=" + VehicleWirePrepId;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                Search();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}