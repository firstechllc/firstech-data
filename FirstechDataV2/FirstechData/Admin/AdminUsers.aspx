﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminUsers.aspx.cs" Inherits="FirstechData.Admin.AdminUsers" Async="true" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-user"></i>
                <span>Users</span>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <div class="row">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </div>
            <div class="form-group form-horizontal" style="height: 40px">
                <div class="col-sm-3 col-xs-6">
                    <asp:TextBox runat="server" ID="SearchUserTxt" placeholder="Name, Email, Store Name to Search"></asp:TextBox>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search User" OnClick="SearchButton_Click" />
                </div>
            </div>
            <div class="row">
                <div class="form-group form-horizontal" style="height: 40px">
                    <label for="ApprovedList" class="col-sm-1 col-xs-6 control-label">Approved:</label>
                    <div class="col-sm-2 col-xs-6">
                        <asp:DropDownList runat="server" ID="ApprovedList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ApprovedList_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="True" Value="1"></asp:ListItem>
                            <asp:ListItem Text="False" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <label for="RoleList" class="col-sm-1 col-xs-6 control-label">User Type:</label>
                    <div class="col-sm-2 col-xs-6">
                        <asp:DropDownList runat="server" ID="RoleList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="RoleList_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="User" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Administrator" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <label for="RoleList" class="col-sm-2 col-xs-6 control-label">Email Confirmed:</label>
                    <div class="col-sm-2 col-xs-6">
                        <asp:DropDownList runat="server" ID="EmailConfirmedList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="EmailConfirmedList_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="True" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="False" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <asp:Button runat="server" ID="ExportButton" CssClass="button tiny bg-black radius" Text="Export" OnClick="ExportButton_Click"></asp:Button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Panel runat="server" ID="ListPanel" ScrollBars="Horizontal">
                        <asp:GridView ID="UserList" runat="server" AutoGenerateColumns="False" CssClass="footable"
                            AllowPaging="true" PageSize="20" OnPageIndexChanging="UserList_PageIndexChanging" AllowSorting="true" OnSorting="UserList_Sorting"
                            OnRowDataBound="UserList_RowDataBound" OnRowDeleting="UserList_RowDeleting" OnSelectedIndexChanged="UserList_SelectedIndexChanged"
                            OnRowCommand="UserList_RowCommand" OnRowEditing="UserList_RowEditing" OnRowCancelingEdit="UserList_RowCancelingEdit" OnRowUpdating="UserList_RowUpdating">
                            <Columns>
                                <asp:TemplateField HeaderText="Email" SortExpression="Email">
                                    <ItemTemplate>
                                        <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                                        <asp:HiddenField ID="UserId" runat="server" Value='<%# Bind("Id") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EmailEdit" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                        <asp:HiddenField ID="UserId2" runat="server" Value='<%# Bind("Id") %>' />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                                    <ItemTemplate>
                                        <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="FirstNameEdit" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                    <ItemTemplate>
                                        <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="LastNameEdit" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Store Name" SortExpression="StoreName">
                                    <ItemTemplate>
                                        <asp:Label ID="StoreNameLabel" runat="server" Text='<%# Bind("StoreName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="StoreNameEdit" runat="server" Text='<%# Bind("StoreName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="AddressLabel" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="AddressEdit" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City" SortExpression="City">
                                    <ItemTemplate>
                                        <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="CityEdit" runat="server" Text='<%# Bind("City") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State" SortExpression="State">
                                    <ItemTemplate>
                                        <asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="StateEdit" runat="server" Text='<%# Bind("State") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zip Code" SortExpression="ZipCode">
                                    <ItemTemplate>
                                        <asp:Label ID="ZipCodeLabel" runat="server" Text='<%# Bind("ZipCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ZipCodeEdit" runat="server" Text='<%# Bind("ZipCode") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                                    <ItemTemplate>
                                        <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="PhoneEdit" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="EmailConfirmed" HeaderText="Email Confirmed" SortExpression="EmailConfirmed" ReadOnly="true" />
                                <asp:BoundField DataField="Approved" HeaderText="Approved" ItemStyle-Width="50px" SortExpression="Approved" ReadOnly="true" />
                                <asp:CommandField ShowSelectButton="true" SelectText="Approve" ItemStyle-Width="50px" />
                                <asp:CommandField ShowEditButton="true" EditText="Edit" ItemStyle-Width="50px" />
                                <asp:CommandField ShowDeleteButton="true" DeleteText="Delete" ItemStyle-Width="50px" />
                                <asp:TemplateField HeaderText="Role">
                                    <ItemTemplate>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Change" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="ChangeRoleButton" Text="Change" CommandName="ChangeRole" CommandArgument='<%# Bind("Id") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reset Pwd" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="ResetPasswordButton" Text="Reset Pwd" CommandName="ResetPassword" CommandArgument='<%# Bind("Id") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="ResendValidationEmailButton" Text="Resend Email Validation" CommandName="SendValidationEmail" CommandArgument='<%# Bind("Id") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[id*=UserList]').footable();
        });
    </script>
</asp:Content>
