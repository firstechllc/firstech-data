﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using FirstechData.Models;
using System.Data;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace FirstechData.Admin
{
    public partial class AdminUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("UsersMenu");
                sortCriteria = "FirstName";
                sortDir = "asc";

                LoadUserList();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void LoadUserList()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            //var users = context.Users.Where(u => u.Approved == true);
            IQueryable<ApplicationUser> users = context.Users;

            if (SearchUserTxt.Text.Trim() != "")
            {
                users = users.Where(u => u.Email.ToUpper().IndexOf(SearchUserTxt.Text.Trim().ToUpper()) >= 0
                    || u.FirstName.ToUpper().IndexOf(SearchUserTxt.Text.Trim().ToUpper()) >= 0
                    || u.LastName.ToUpper().IndexOf(SearchUserTxt.Text.Trim().ToUpper()) >= 0
                    || u.StoreName.ToUpper().IndexOf(SearchUserTxt.Text.Trim().ToUpper()) >= 0); 
            }

            if (ApprovedList.SelectedValue != "-1")
            {
                if (ApprovedList.SelectedValue == "1")
                {
                    users = users.Where(u => u.Approved == true);
                }
                else if (ApprovedList.SelectedValue == "0")
                {
                    users = users.Where(u => u.Approved == false);
                }
            }

            if (EmailConfirmedList.SelectedValue != "-1")
            {
                if (EmailConfirmedList.SelectedValue == "1")
                {
                    users = users.Where(u => u.EmailConfirmed == true);
                }
                else if (EmailConfirmedList.SelectedValue == "0")
                {
                    users = users.Where(u => u.EmailConfirmed == false);
                }
            }

            if (RoleList.SelectedValue == "1")
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var superAdmins = roleManager.Roles.Single(b => b.Name == "Administrator").Users;

                var userList = users.ToList();
                foreach (var superAdmin in superAdmins)
                {
                    userList.RemoveAll(c => c.Id == superAdmin.UserId);
                }
                users = userList.AsQueryable();
            }
            else if (RoleList.SelectedValue == "2")
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var superAdmins = roleManager.Roles.Single(b => b.Name == "Administrator").Users;

                var userList = users.ToList();
                foreach (var superAdmin in superAdmins)
                {
                    userList.RemoveAll(c => c.Id != superAdmin.UserId);
                }
                users = userList.AsQueryable();
            }

            if (sortDir == "asc")
            {
                if (sortCriteria == "Email")
                    users = users.OrderBy(o => o.Email);
                else if (sortCriteria == "FirstName")
                    users = users.OrderBy(o => o.FirstName);
                else if (sortCriteria == "LastName")
                    users = users.OrderBy(o => o.LastName);
                else if (sortCriteria == "StoreName")
                    users = users.OrderBy(o => o.StoreName);
                else if (sortCriteria == "Address")
                    users = users.OrderBy(o => o.Address);
                else if (sortCriteria == "City")
                    users = users.OrderBy(o => o.City);
                else if (sortCriteria == "State")
                    users = users.OrderBy(o => o.State);
                else if (sortCriteria == "ZipCode")
                    users = users.OrderBy(o => o.ZipCode);
                else if (sortCriteria == "Phone")
                    users = users.OrderBy(o => o.Phone);
                else if (sortCriteria == "Approved")
                    users = users.OrderBy(o => o.Approved);
            }
            else
            {
                if (sortCriteria == "Email")
                    users = users.OrderByDescending(o => o.Email);
                else if (sortCriteria == "FirstName")
                    users = users.OrderByDescending(o => o.FirstName);
                else if (sortCriteria == "LastName")
                    users = users.OrderByDescending(o => o.LastName);
                else if (sortCriteria == "StoreName")
                    users = users.OrderByDescending(o => o.StoreName);
                else if (sortCriteria == "Address")
                    users = users.OrderByDescending(o => o.Address);
                else if (sortCriteria == "City")
                    users = users.OrderByDescending(o => o.City);
                else if (sortCriteria == "State")
                    users = users.OrderByDescending(o => o.State);
                else if (sortCriteria == "ZipCode")
                    users = users.OrderByDescending(o => o.ZipCode);
                else if (sortCriteria == "Phone")
                    users = users.OrderByDescending(o => o.Phone);
                else if (sortCriteria == "Approved")
                    users = users.OrderByDescending(o => o.Approved);
            }

            UserList.DataSource = users.ToList();

            UserList.DataBind();

            UserList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
            UserList.HeaderRow.Cells[1].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[2].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[3].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[4].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[5].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[6].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[7].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[8].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[9].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[10].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[11].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[12].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[13].Attributes["data-hide"] = "expand";
            UserList.HeaderRow.Cells[14].Attributes["data-hide"] = "expand";

            UserList.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void UserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UserList.PageIndex = e.NewPageIndex;
            LoadUserList();
        }

        protected void ApprovedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUserList();
        }

        protected void UserList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Approved = e.Row.Cells[10].Text;
                if (Approved == "True" && e.Row.Cells[11].Controls != null && e.Row.Cells[11].Controls.Count > 0)
                {
                    LinkButton btn = (LinkButton)(e.Row.Cells[11].Controls[0]);
                    btn.Text = "Disapprove";
                }

                if (e.Row.Cells[13].Controls != null && e.Row.Cells[13].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[13].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this User?');";
                    }
                }

                HiddenField UserIdHidden = e.Row.FindControl("UserId") as HiddenField;
                if (UserIdHidden == null)
                    UserIdHidden = e.Row.FindControl("UserId2") as HiddenField;
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                IList<string> roleNames = manager.GetRoles(UserIdHidden.Value);
                e.Row.Cells[14].Text = string.Join(" ", roleNames.ToArray()).Trim();

                if (e.Row.Cells[15].Controls != null && e.Row.Cells[15].Controls.Count > 1)
                {
                    LinkButton ChangeRoleButton = e.Row.Cells[15].Controls[1] as LinkButton;
                    if (ChangeRoleButton != null)
                    {
                        if (e.Row.Cells[14].Text == "Administrator")
                        {
                            ChangeRoleButton.Text = "To User";
                        }
                        else
                        {
                            ChangeRoleButton.Text = "To Admin";
                        }
                    }
                }

                string EmailConfirmed = e.Row.Cells[9].Text;
                if (e.Row.Cells[17].Controls != null && e.Row.Cells[17].Controls.Count > 1)
                {
                    LinkButton ResendValidationEmailBtn = (LinkButton)(e.Row.Cells[17].Controls[1]);
                    if (EmailConfirmed == "True")
                    {
                        ResendValidationEmailBtn.Visible = false;
                    }
                    else
                    {
                        ResendValidationEmailBtn.Visible = true;
                    }
                }
            }
        }

        protected void UserList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField IdHidden = UserList.Rows[e.RowIndex].FindControl("UserId") as HiddenField;

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(IdHidden.Value);

            IdentityResult result = manager.Delete(user);
            if (result.Succeeded)
            {
                LoadUserList();
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        protected void UserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HiddenField IdHidden = UserList.SelectedRow.FindControl("UserId") as HiddenField;

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(IdHidden.Value);
            user.Approved = !user.Approved;

            IdentityResult result = manager.Update(user);
            if (result.Succeeded)
            {
                if (user.Approved)
                {
                    SendApprovalEmail(user.Id, user.Email);
                }
                LoadUserList();
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        private void SendApprovalEmail(string userid, string email)
        {
            StringBuilder emailContent = new StringBuilder();

            emailContent.AppendLine("<!doctype html><html><head><title>Thank you for applying for Firstech Data</title></head>");
            emailContent.AppendLine("<body style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: arial, helvetica, sans-serif; color: #222222; font-size: 15px; background: #F5F5F5; line-height: 125%;\">");
            emailContent.AppendLine("    <table class=\"container\" width=\"100%\" bgcolor=\"#F5F5F5\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse;\">");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\" class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                <a href=\"http://www.firstechdata.com\" target=\"_blank\">");
            emailContent.AppendLine("                    <img src=\"https://mlsvc01-prod.s3.amazonaws.com/b86ab128001/2e689763-98c4-4fd2-a038-facddf1e3b9c.png?ver=1466454321000\" alt=\"firstech data logo\" width=\"250\" height=\"87\">");
            emailContent.AppendLine("                </a>");
            emailContent.AppendLine("            </td></tr>");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\">");
            emailContent.AppendLine("                <table class=\"content\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC;\">");
            emailContent.AppendLine("                    <tr><td class=\"content-cell\" align=\"left\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                            <p><b>Your account for Firstech Data has been approved!</b></p>");
            emailContent.AppendLine("                            <p><a class=\"confirmation-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Log Into Your Account</a></p>");
            emailContent.AppendLine("                            <p>Username: " + email + "</p>");
            emailContent.AppendLine("                            <p>Log into your account today and get connected to one of the most powerful tools in the 12-volt industry: <b>Firstech Data</b>!</p>");
            emailContent.AppendLine("                        </td></tr>");
            emailContent.AppendLine("                </table></td>");
            emailContent.AppendLine("        </tr>");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\">");
            emailContent.AppendLine("                <table class=\"content secondary\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC; color: #888888; font-size: 12px;\">");
            emailContent.AppendLine("                    <tr><td class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                            <p><a class=\"footer-link\" href=\"http://www.firstechdata.com\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">FirstechData.com</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            emailContent.AppendLine("                                <a class=\"footer-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Log In</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            emailContent.AppendLine("                                <a class=\"footer-link\" href=\"http://firstechdata.com/Account/Register\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Register</a></p>");
            emailContent.AppendLine("                            <p>Technical Support: (888)820-3690</p>");
            emailContent.AppendLine("                            <p>Hours: Mon-Fri: 7AM-5PM PST</p>");
            emailContent.AppendLine("                            <p>&copy; 2016 Firstech, LLC. All rights reserved. </p>");
            emailContent.AppendLine("                        </td>");
            emailContent.AppendLine("                    </tr>");
            emailContent.AppendLine("</table></td></tr></table></body></html>");

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            try
            { 
                manager.SendEmailAsync(userid, "Your account for Firstech Data has been approved!", emailContent.ToString());
            }
            catch (Exception)
            {
                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                var admin = manager.FindByEmail(AdminEmail);

                string title = "Sending Failure - " + "Your account for Firstech Data has been approved!";
                string content = "Sending Failure - " + email + "<br/>" + emailContent.ToString();
                manager.SendEmail(admin.Id, title, content);
            }
        }

        private async void SendEmailValidation(ApplicationUser user)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            string code = manager.GenerateEmailConfirmationToken(user.Id);
            string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);

            StringBuilder mailContent = new StringBuilder();
            mailContent.AppendLine("<!doctype html><html><head><title>Thank you for applying for Firstech Data</title></head>");
            mailContent.AppendLine("<body style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: arial, helvetica, sans-serif; color: #222222; font-size: 15px; background: #F5F5F5; line-height: 125%;\">");
            mailContent.AppendLine("	<table class=\"container\" width=\"100%\" bgcolor=\"#F5F5F5\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse;\">");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\" class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("				<a href=\"http://www.firstechdata.com\" target=\"_blank\">");
            mailContent.AppendLine("					<img src=\"https://mlsvc01-prod.s3.amazonaws.com/b86ab128001/2e689763-98c4-4fd2-a038-facddf1e3b9c.png?ver=1466454321000\" alt=\"firstech data logo\" width=\"250\" height=\"87\">");
            mailContent.AppendLine("				</a></td></tr>");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\">");
            mailContent.AppendLine("				<table class=\"content\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"table-layout:fixed; box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC;\">");
            mailContent.AppendLine("					<tr><td class=\"content-cell\" align=\"left\" style=\"word-break:break-all; padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("							<p><b>Thank you registering for Firstech Data!</b></p>");
            mailContent.AppendLine("							<p><a class=\"confirmation-link\" href=\"" + callbackUrl + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Confirm My E-mail</a></p>");
            mailContent.AppendLine("							<p><font size='-1'>If you can't click on the link, just copy and paste it in your browser - " + callbackUrl + "</font></p>");
            mailContent.AppendLine("							<p>Please click the link above to confirm your e-mail so that we may begin reviewing your registration. Upon approval, you will receive confirmation that your account is active.</p>");
            mailContent.AppendLine("							<p>Please allow up to 2 business days for activation.</p>");
            mailContent.AppendLine("						</td></tr></table></td></tr>");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\">");
            mailContent.AppendLine("				<table class=\"content secondary\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC; color: #888888; font-size: 12px;\">");
            mailContent.AppendLine("					<tr><td class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("							<p><a class=\"footer-link\" href=\"http://www.firstechdata.com\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">FirstechData.com</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Log In</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Register\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Register</a></p>");
            mailContent.AppendLine("							<p>Technical Support: (888)820-3690</p>");
            mailContent.AppendLine("							<p>Hours: Mon-Fri: 7AM-5PM PST</p>");
            mailContent.AppendLine("							<p>&copy; 2016 Firstech, LLC. All rights reserved. </p>");
            mailContent.AppendLine("						</td></tr></table></td></tr>");
            mailContent.AppendLine("	</table></body></html>");

            //await manager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");
            try
            { 
                await manager.SendEmailAsync(user.Id, "Confirm your E-mail", mailContent.ToString());
            }
            catch (Exception)
            {
                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                var admin = manager.FindByEmail(AdminEmail);

                string title = "Sending Failure - " + "Confirm your E-mail";
                string content = "Sending Failure - " + user.Email + "<br/>" + mailContent.ToString();
                await manager.SendEmailAsync(admin.Id, title, content);
            }
        }

        protected void UserList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }

            LoadUserList();
        }

        protected void RoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUserList();
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from AspNetUsers", conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                StringBuilder excel = new StringBuilder();

                excel.AppendLine("Email\tFirst Name\tLast Name\tStore Name\tAddress\tCity\tState\tZip Code\tPhone\tEmail Confirmed\tApproved\tRole");
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                while (reader.Read())
                {
                    excel.Append(reader["Email"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["FirstName"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["LastName"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["StoreName"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["Address"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["City"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["State"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["ZipCode"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["Phone"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["EmailConfirmed"].ToString());
                    excel.Append("\t");
                    excel.Append(reader["Approved"].ToString());
                    excel.Append("\t");

                    string Id = reader["Id"].ToString();
                    IList<string> roleNames = manager.GetRoles(Id);

                    excel.Append(string.Join(" ", roleNames.ToArray()).Trim());
                    excel.AppendLine("");
                }

                reader.Close();
                conn.Close();

                string filename = "Users.xls";
                string content_type = "text/csv";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = content_type;
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.Charset = "";
                this.EnableViewState = false;
                Response.Write(excel.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected async void UserList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            string UserId = (string)e.CommandArgument;
            if (e.CommandName != "ChangeRole" && e.CommandName != "ResetPassword" && e.CommandName != "SendValidationEmail")
            {
                return;
            }

            try
            {
                if (e.CommandName == "ChangeRole")
                {
                    LinkButton ChangeRoleButton = e.CommandSource as LinkButton;
                    if (ChangeRoleButton != null)
                    {
                        if (ChangeRoleButton.Text == "To User")
                        {
                            ApplicationDbContext context = new ApplicationDbContext();
                            var userStore = new UserStore<ApplicationUser>(context);
                            var userManager = new UserManager<ApplicationUser>(userStore);

                            userManager.RemoveFromRole(UserId, "Administrator");

                            LoadUserList();
                        }
                        else if (ChangeRoleButton.Text == "To Admin")
                        {
                            ApplicationDbContext context = new ApplicationDbContext();
                            var userStore = new UserStore<ApplicationUser>(context);
                            var userManager = new UserManager<ApplicationUser>(userStore);

                            userManager.AddToRole(UserId, "Administrator");

                            LoadUserList();
                        }
                    }
                }
                else if (e.CommandName == "ResetPassword")
                {
                    ApplicationDbContext context = new ApplicationDbContext();
                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);
                    string newPasswd = "123456";
                    string hashedNewPasswd = userManager.PasswordHasher.HashPassword(newPasswd);
                    var user = await userStore.FindByIdAsync(UserId);
                    await userStore.SetPasswordHashAsync(user, hashedNewPasswd);
                    await userStore.UpdateAsync(user);

                    ShowError("Reset password successfully.");
                }
                else if (e.CommandName == "SendValidationEmail")
                {
                    ApplicationDbContext context = new ApplicationDbContext();
                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);
                    var user = await userStore.FindByIdAsync(UserId);
                    SendEmailValidation(user);

                    ShowError("Sent validation email successfully.");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
            }
        }

        protected void EmailConfirmedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUserList();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ClearError();
            LoadUserList();
        }

        protected void UserList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            UserList.EditIndex = e.NewEditIndex;
            LoadUserList();
        }

        protected void UserList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            UserList.EditIndex = -1;
            LoadUserList();
        }

        protected async void UserList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            HiddenField UserIdHidden = UserList.Rows[e.RowIndex].FindControl("UserId2") as HiddenField;
            if (UserIdHidden != null)
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var user = manager.FindById(UserIdHidden.Value);

                string Email = ((TextBox)UserList.Rows[e.RowIndex].FindControl("EmailEdit")).Text;
                string FirstName = ((TextBox)UserList.Rows[e.RowIndex].FindControl("FirstNameEdit")).Text;
                string LastName = ((TextBox)UserList.Rows[e.RowIndex].FindControl("LastNameEdit")).Text;
                string StoreName = ((TextBox)UserList.Rows[e.RowIndex].FindControl("StoreNameEdit")).Text;
                string Address = ((TextBox)UserList.Rows[e.RowIndex].FindControl("AddressEdit")).Text;
                string City = ((TextBox)UserList.Rows[e.RowIndex].FindControl("CityEdit")).Text;
                string State = ((TextBox)UserList.Rows[e.RowIndex].FindControl("StateEdit")).Text;
                string ZipCode = ((TextBox)UserList.Rows[e.RowIndex].FindControl("ZipCodeEdit")).Text;
                string Phone = ((TextBox)UserList.Rows[e.RowIndex].FindControl("PhoneEdit")).Text;

                user.Email = Email;
                user.FirstName = FirstName;
                user.LastName = LastName;
                user.StoreName = StoreName;
                user.Address = Address;
                user.City = City;
                user.State = State;
                user.ZipCode = ZipCode;
                user.Phone = Phone;

                await manager.UpdateAsync(user);
                
            }

            UserList.EditIndex = -1;
            LoadUserList();
        }
    }
}