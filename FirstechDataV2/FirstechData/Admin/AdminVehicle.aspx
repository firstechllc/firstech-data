﻿<%@ Page Title="Admin Vehicle" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminVehicle.aspx.cs" Inherits="FirstechData.Admin.AdminVehicle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Vehicle</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-bus"></i><span>Vehicle</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:DropDownList runat="server" ID="SampleMakeList" CssClass="col-sm-2" placeholder="Make to search"></asp:DropDownList>
                            <asp:DropDownList runat="server" ID="SampleModelList" CssClass="col-sm-2" placeholder="Model to search"></asp:DropDownList>
                            <asp:TextBox runat="server" ID="SearchYearTxt" Width="150px" CssClass="col-sm-2" placeholder="Year to search"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search Vehicle" OnClick="SearchButton_Click" />
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add Vehicle" OnClick="AddButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="VehicleList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowSorting="true" AllowPaging="true" PageSize="20"
                                OnRowCancelingEdit="VehicleList_RowCancelingEdit" OnRowUpdating="VehicleList_RowUpdating" OnPageIndexChanging="VehicleList_PageIndexChanging" 
                                OnRowDeleting="VehicleList_RowDeleting" OnRowEditing="VehicleList_RowEditing" OnRowDataBound="VehicleList_RowDataBound" OnSorting="VehicleList_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Make" SortExpression="VehicleMakeName">
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="VehicleMakeModelYearIdHidden" runat="server" Value='<%# Bind("VehicleMakeModelYearId") %>' />
                                            <asp:DropDownList runat="server" ID="MakeList"></asp:DropDownList>
                                            <asp:HiddenField ID="VehicleMakeIdHidden" runat="server" Value='<%# Bind("VehicleMakeId") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="VehicleMakeModelYearIdHidden2" runat="server" Value='<%# Bind("VehicleMakeModelYearId") %>' />
                                            <asp:Label ID="MakeLabel" runat="server" Text='<%# Bind("VehicleMakeName") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Model" SortExpression="VehicleModelName">
                                        <EditItemTemplate>
                                            <asp:DropDownList runat="server" ID="ModelList"></asp:DropDownList>
                                            <asp:HiddenField ID="VehicleModelIdHidden" runat="server" Value='<%# Bind("VehicleModelId") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ModelLabel" runat="server" Text='<%# Bind("VehicleModelName") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Year" SortExpression="VehicleYear">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="YearTxt" runat="server" Text='<%# Bind("VehicleYear") %>' Width="80px" MaxLength="4"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="YearLabel" runat="server" Text='<%# Bind("VehicleYear") %>' BorderWidth="0"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField CancelText="Cancel" EditText="Edit" HeaderText="Edit" ShowEditButton="True" UpdateText="Save" ButtonType="Link" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=VehicleList]').footable();
        });
    </script>
</asp:Content>
