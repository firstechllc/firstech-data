﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminVehicleInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("VehicleMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                LoadVehicleMake();
                //ShowModelList(true);
            }
        }

        private void LoadVehicleMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName from dbo.VehicleMake with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("Make", "-1"));

                MakeList.DataTextField = "VehicleMakeName";
                MakeList.DataValueField = "VehicleMakeId";
                MakeList.DataSource = ds2;
                MakeList.DataBind();

                MakeList.Items.Insert(0, new ListItem("Make", "-1"));


                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                SearchInstallationTypeList.DataTextField = "InstallationTypeName";
                SearchInstallationTypeList.DataValueField = "InstallationTypeId";
                SearchInstallationTypeList.DataSource = ds2;
                SearchInstallationTypeList.DataBind();

                SearchInstallationTypeList.Items.Insert(0, new ListItem("", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ShowInfo(string info)
        {
            InfoLabel.Text = info;
            InfoLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }
        protected void MakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowModelList(false);
        }

        protected void ShowModelList(bool showAll)
        {
            if (AddVehicleTitle.Text != "Link Vehicle" && !showAll)
                return;

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "";
                if (showAll)
                {
                    sql = "select distinct VehicleModelId, VehicleModelName ";
                    sql += "from dbo.VehicleModel WITH (NOLOCK)  ";
                    sql += " order by VehicleModelName ";
                }
                else
                {
                    sql = "select distinct a.VehicleModelId, VehicleModelName ";
                    sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                    sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                    if (AddVehicleTitle.Text == "Link Vehicle")
                        sql += "where a.VehicleMakeId = " + MakeList.SelectedValue;
                    sql += " order by VehicleModelName ";
                }

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ModelList.DataTextField = "VehicleModelName";
                ModelList.DataValueField = "VehicleModelId";
                ModelList.DataSource = ds;
                ModelList.DataBind();

                ModelList.Items.Insert(0, new ListItem("Model", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowHideResult(false);
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " order by VehicleModelName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("Model", "-1"));

                ShowHideResult(false);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeYearList(true);
        }

        private void ChangeYearList(bool refresh)
        {
            ClearError();
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                VehicleYearList.Items.Clear();
                ShowHideResult(false);
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " and a.VehicleModelId = " + VehicleModelList.SelectedValue;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("Year", "-1"));

                if (refresh)
                {
                    ShowHideResult(false);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchInfo(0);
            NewVehiclePanel.Visible = false;
        }

        private void ShowHideResult(bool show)
        {
            NoteTitleLabel.Visible = show;
            AddWirePanelButton.Visible = show;
            AddDisassemblyPanelButton.Visible = show;
            AddPrepPanelButton.Visible = show;
            AddRoutingPanelButton.Visible = show;
            AddProgrammingPanelButton.Visible = show;
            AddFBPanelButton.Visible = show;
            AddDocumentPanelButton.Visible = show;
            SearchInstallationTypePanel.Visible = show;
            InternalNotePanel.Visible = show;

            WireList.Visible = show;
            NoteList.Visible = show;
            CommentList.Visible = show;
            InternalNoteList.Visible = show;
            DisassemblyList.Visible = show;
            PrepList.Visible = show;
            RoutingList.Visible = show;
            ProgrammingList.Visible = show;

            if (!show)
            {
                WiringHeader.Text = "Vehicle Wiring";
                DisassemblyHeader.Text = "Disassembly";
                FBResultHeader.Text = "Facebook Result";
                PrepHeader.Text = "Prep";
                RoutingHeader.Text = "Routing/Placement";
                ProgrammingHeader.Text = "Programming";
                TitlePanel.Visible = false;
                UrgentMessagePanel.Visible = false;
                VehicleMakeModelYearIdHidden.Value = "";

                NewVehiclePanel.Visible = false;
                AddWirePanel.Visible = false;
                EditNotePanel.Visible = false;
                AddDisassemblyPanel.Visible = false;
                AddDocumentPanel.Visible = false;
                AddPrepPanel.Visible = false;
                AddRoutingPanel.Visible = false;
                AddProgrammingPanel.Visible = false;
                AddFBPanel.Visible = false;
            }
        }

        private void ChangeVehicleTitle()
        {
            CurrentVehicle.Text = VehicleMakeList.SelectedItem.Text + " " + VehicleModelList.SelectedItem.Text + " " + VehicleYearList.SelectedItem.Text + " (" + VehicleMakeModelYearIdHidden.Value + ")";
        }

        protected void SearchInfo(int tabIdx)
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                ShowHideResult(false);
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                ShowHideResult(false);
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                ShowHideResult(false);
                return;
            }

            if (tabIdx == 0)
            {
                ChangeVehicleTitle();
                TitlePanel.Visible = true;
                UrgentMessagePanel.Visible = true;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeModelYearId from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear";
                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMakeModelYearIdHidden.Value = reader["VehicleMakeModelYearId"].ToString();
                    ChangeVehicleTitle();
                }
                else
                {
                    reader.Close();
                    return;
                }
                reader.Close();

                string SaveFolder = SaveFolderHidden.Value;

                // Link Vehicle
                sql = "select m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, l.LinkVehicleMakeModelYearId, ";
                sql += "v.VehicleMakeId, v.VehicleModelId ";
                sql += "from dbo.VehicleLink l WITH (NOLOCK) ";
                sql += "join dbo.VehicleMakeModelYear v WITH (NOLOCK) on l.LinkVehicleMakeModelYearId = v.VehicleMakeModelYearId ";
                sql += "join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId ";
                sql += "join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId ";
                sql += "where l.VehicleMakeModelYearId = @VehicleMakeModelYearId";

                SqlCommand linkCmd = new SqlCommand(sql, Con);
                linkCmd.CommandType = CommandType.Text;

                linkCmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                reader = linkCmd.ExecuteReader();
                if (reader.Read())
                {
                    LinkVehicle.Text = "(Link: " + reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString() + ")";
                    LinkVehicleMakeModelYearIdHidden.Value = reader["LinkVehicleMakeModelYearId"].ToString();
                    LinkVehicleMakeIdHidden.Value = reader["VehicleMakeId"].ToString();
                    LinkVehicleModelIdHidden.Value = reader["VehicleModelId"].ToString();
                    LinkVehicleYearHidden.Value = reader["VehicleYear"].ToString();
                }
                else
                {
                    LinkVehicle.Text = "";
                    LinkVehicleMakeModelYearIdHidden.Value = "";
                    LinkVehicleMakeIdHidden.Value = "";
                    LinkVehicleModelIdHidden.Value = "";
                    LinkVehicleYearHidden.Value = "";
                }
                reader.Close();

                if (tabIdx == 0)
                {
                    sql = GetUrgentQuery();

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    reader = Cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        UrgentMessageLabel.Text = reader["Note"].ToString();
                        UrgentMessageEdit.Text = reader["Note"].ToString();
                    }
                    else
                    {
                        UrgentMessageLabel.Text = "";
                        UrgentMessageEdit.Text = "";
                    }
                    reader.Close();
                }

                if (tabIdx == 0 || tabIdx == 1)
                {
                    ShowWireList(Con, SaveFolder);

                    SearchInstallationTypePanel.Visible = true;
                    InternalNotePanel.Visible = true;

                    ShowCommentList();
                    ShowInternalNotes();

                    AddWirePanel.Visible = false;
                    EditNotePanel.Visible = false;

                    sql = GetWireNoteQuery();

                    cmd = new SqlCommand(sql, Con);
                    cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    NoteList.DataSource = ds;
                    NoteList.DataBind();

                    /*

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Note"] != null)
                        {
                            NoteLabelTitle.Text = "Note: ";
                            NoteLabel.Text = reader["Note"].ToString();
                            editor.Text = reader["Note"].ToString();
                        }
                        else
                        {
                            NoteLabelTitle.Text = "Note: ";
                            NoteLabel.Text = "";
                        }

                        VehicleWireNoteIdHidden.Value = reader["VehicleWireNoteId"].ToString();
                    }
                    else
                    {
                        VehicleWireNoteIdHidden.Value = "";
                        NoteLabelTitle.Text = "Note: ";
                        NoteLabel.Text = "";
                    }
                    reader.Close();
                    */
                }

                if (tabIdx == 0 || tabIdx == 2)
                {
                    sql = GetFacebookQuery(SaveFolder);

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    FBList.DataSource = ds;
                    FBList.DataBind();

                    if (FBList != null && FBList.HeaderRow != null && FBList.HeaderRow.Cells.Count > 0)
                    {
                        FBList.Attributes["data-page"] = "false";

                        FBList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        FBResultHeader.Text = "Facebook Result (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        FBResultHeader.Text = "Facebook Result (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 3)
                {
                    sql = GetDocumentQuery();

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    DocumentList.DataSource = ds;
                    DocumentList.DataBind();

                    if (DocumentList != null && DocumentList.HeaderRow != null && DocumentList.HeaderRow.Cells.Count > 0)
                    {
                        DocumentList.Attributes["data-page"] = "false";

                        DocumentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        DocumentHeader.Text = "Documents (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        DocumentHeader.Text = "Documents (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 4)
                {
                    sql = GetDisassemblyQuery(SaveFolder);

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    DisassemblyList.DataSource = ds;
                    DisassemblyList.DataBind();

                    if (DisassemblyList != null && DisassemblyList.HeaderRow != null && DisassemblyList.HeaderRow.Cells.Count > 0)
                    {
                        DisassemblyList.Attributes["data-page"] = "false";

                        /*
                        DisassemblyList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        DisassemblyList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                        DisassemblyList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";

                        DisassemblyList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                        DisassemblyList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                        */

                        DisassemblyList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //DisassemblyTitle.Visible = true;

                        DisassemblyHeader.Text = "Disassembly (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        DisassemblyHeader.Text = "Disassembly (0)";
                        //DisassemblyTitle.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 5)
                {
                    sql = GetPrepQuery(SaveFolder);

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    PrepList.DataSource = ds;
                    PrepList.DataBind();

                    if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
                    {
                        PrepList.Attributes["data-page"] = "false";

                        PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        PrepList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        PrepList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        PrepList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //PrepTitle.Visible = true;

                        PrepHeader.Text = "Prep (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        PrepHeader.Text = "Prep (0)";
                        //PrepTitle.Visible = false;
                    }
                }

                // Routing
                if (tabIdx == 0 || tabIdx == 6)
                {
                    sql = GetRoutingQuery(SaveFolder);

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    RoutingList.DataSource = ds;
                    RoutingList.DataBind();

                    if (RoutingList != null && RoutingList.HeaderRow != null && RoutingList.HeaderRow.Cells.Count > 0)
                    {
                        RoutingList.Attributes["data-page"] = "false";

                        RoutingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        RoutingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        RoutingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        RoutingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        RoutingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        RoutingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        RoutingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //PlacementTitle.Visible = true;

                        RoutingHeader.Text = "Routing/Placement (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        RoutingHeader.Text = "Routing/Placement (0)";
                        //PlacementTitle.Visible = false;
                    }
                }

                // Programming
                if (tabIdx == 0 || tabIdx == 7)
                {
                    sql = GetProgrammingQuery(SaveFolder);

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    ProgrammingList.DataSource = ds;
                    ProgrammingList.DataBind();

                    if (ProgrammingList != null && ProgrammingList.HeaderRow != null && ProgrammingList.HeaderRow.Cells.Count > 0)
                    {
                        ProgrammingList.Attributes["data-page"] = "false";

                        ProgrammingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        ProgrammingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        ProgrammingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        ProgrammingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        ProgrammingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        ProgrammingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        ProgrammingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                        //PlacementTitle.Visible = true;

                        ProgrammingHeader.Text = "Programming (" + ds.Tables[0].Rows.Count + ")";
                    }
                    else
                    {
                        ProgrammingHeader.Text = "Programming (0)";
                        //PlacementTitle.Visible = false;
                    }
                }

                ShowHideResult(true);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Prep?');";
                    }
                }
            }
        }

        protected void DisassemblyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Disassembly?');";
                    }
                }
            }
        }
        protected void RoutingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Routing/Plancement?');";
                    }
                }
            }
        }

        private void HideNoImageLink(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink a1 = ((HyperLink)(e.Row.FindControl("Image1Link")));
                HyperLink a2 = ((HyperLink)(e.Row.FindControl("Image2Link")));
                HyperLink a3 = ((HyperLink)(e.Row.FindControl("Image3Link")));
                HyperLink a4 = ((HyperLink)(e.Row.FindControl("Image4Link")));
                HyperLink a5 = ((HyperLink)(e.Row.FindControl("Image5Link")));

                if (a1.ImageUrl == "")
                {
                    a1.Visible = false;
                }
                if (a2.ImageUrl == "")
                {
                    a2.Visible = false;
                }
                if (a3.ImageUrl == "")
                {
                    a3.Visible = false;
                }
                if (a4.ImageUrl == "")
                {
                    a4.Visible = false;
                }
                if (a5.ImageUrl == "")
                {
                    a5.Visible = false;
                }
            }
        }

        protected void AddVehiclePanelButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Add Vehicle";
            AddVehicleButton.Text = "Add";

            UnlinkVehicleButton.Visible = false;

            MakeList.SelectedIndex = -1;
            ModelList.SelectedIndex = -1;

            YearList.Visible = false;
            YearList.SelectedIndex = -1;
            Year.Visible = true;
            Year.Text = "";

            ShowModelList(true);
            NewVehiclePanel.Visible = true;
        }

        protected void CancelAddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();
            NewVehiclePanel.Visible = false;
        }

        protected void AddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (MakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make");
                return;
            }
            if (ModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model");
                return;
            }
            if (YearList.Visible && YearList.SelectedValue == "-1")
            {
                ShowError("Please select Year");
                return;
            }

            int year;
            if (Year.Visible && !int.TryParse(Year.Text, out year))
            {
                ShowError("Please enter year");
                Year.Focus();
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (AddVehicleTitle.Text == "Add Vehicle")
                {
                    string sql = "insert into dbo.VehicleMakeModelYear (VehicleMakeId, VehicleModelId, VehicleYear, UpdatedDt, UpdatedBy) ";
                    sql += "select @VehicleMakeId, @VehicleModelId, @VehicleYear, getdate(), @UpdatedBy";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                    ShowInfo("Added vehicle successfully");
                    LoadVehicleMake();
                    VehicleModelList.SelectedIndex = -1;
                    VehicleYearList.SelectedIndex = -1;
                }
                else if (AddVehicleTitle.Text == "Edit Vehicle")
                {
                    string sql = "update dbo.VehicleMakeModelYear set VehicleMakeId = @VehicleMakeId, VehicleModelId=@VehicleModelId, ";
                    sql += "VehicleYear=@VehicleYear, UpdatedDt=getdate(), UpdatedBy= @UpdatedBy ";
                    sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.ExecuteNonQuery();
                    ShowInfo("Updated vehicle successfully");

                    VehicleMakeList.SelectedValue = MakeList.SelectedValue;
                    VehicleModelList.SelectedValue = ModelList.SelectedValue;
                    ChangeYearList(false);
                    VehicleYearList.SelectedValue = Year.Text;
                    ChangeVehicleTitle();
                }
                else if (AddVehicleTitle.Text == "Link Vehicle")
                {
                    string sql = "select VehicleMakeModelYearId from dbo.VehicleMakeModelYear WITH (NOLOCK) ";
                    sql += "where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(YearList.SelectedValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        ShowError("Failed to find Vehicle information");
                        return;
                    }
                    reader.Close();

                    if (VehicleMakeModelYearIdHidden.Value == VehicleMakeModelYearId)
                    {
                        ShowError("You cannot link this Vehicle.");
                        return;
                    }
                    LinkVehicleMakeModelYearIdHidden.Value = VehicleMakeModelYearId;

                    LinkVehicleDatabase(VehicleMakeModelYearIdHidden.Value, VehicleMakeModelYearId);
                    LinkVehicle.Text = MakeList.SelectedItem.Text + " " + ModelList.SelectedItem.Text + " " + YearList.SelectedValue;

                    ShowInfo("Linked vehicle successfully");
                }

                Year.Text = "";
                NewVehiclePanel.Visible = false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Vehicle already exists");
                }
                else
                {
                    ShowError(ex.ToString());
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void LinkVehicleDatabase(string VehicleMakeModelYearId, string LinkVehicleMakeModelYearId)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "if not exists(select * from VehicleLink where VehicleMakeModelYearId=@VehicleMakeModelYearId) ";
                sql += "insert into dbo.VehicleLink (VehicleMakeModelYearId, LinkVehicleMakeModelYearId, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @LinkVehicleMakeModelYearId, getdate(), @UpdatedBy ";
                sql += "else ";
                sql += "update dbo.VehicleLink set LinkVehicleMakeModelYearId = @LinkVehicleMakeModelYearId, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                cmd.Parameters.Add("@LinkVehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(LinkVehicleMakeModelYearId);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        protected void EditVehicleButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Edit Vehicle";
            AddVehicleButton.Text = "Save";

            UnlinkVehicleButton.Visible = false;

            MakeList.SelectedValue = VehicleMakeList.SelectedValue;
            ShowModelList(true);
            ModelList.SelectedValue = VehicleModelList.SelectedValue;

            YearList.Visible = false;
            Year.Visible = true;
            Year.Text = VehicleYearList.SelectedValue;

            NewVehiclePanel.Visible = true;
        }

        protected void DeleteVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Wiring
                string sql = "delete from dbo.VehicleWireFunction where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Note
                sql = "delete from dbo.VehicleWireNote where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Disassembly
                sql = "delete from dbo.VehicleWireDisassembly where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Prep
                sql = "delete from dbo.VehicleWirePrep where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Routing
                sql = "delete from dbo.VehicleWirePlacementRouting where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Programming
                sql = "delete from dbo.VehicleWireProgramming where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Vehicle
                sql = "delete from dbo.VehicleMakeModelYear where VehicleMakeModelYearId = @VehicleMakeModelYearId";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                tran.Commit();

                VehicleMakeList.SelectedIndex = 0;
                VehicleModelList.Items.Clear();
                VehicleYearList.Items.Clear();
                ShowHideResult(false);

                ShowInfo("Deleted vehicle successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void AddWirePanelButton_Click(object sender, EventArgs e)
        {
            LoadWireInstall();

            VehicleColor.Text = "";
            Color.Text = "";
            PinOut.Text = "";
            Location.Text = "";
            Polarity.Text = "";

            AttachWiringImageDelete1.Checked = false;
            AttachWiringImageDelete2.Checked = false;
            AttachWiringImageDelete3.Checked = false;
            AttachWiringImageDelete4.Checked = false;
            AttachWiringImageDelete5.Checked = false;

            ExistingWiringFilename1.Value = "";
            AttachWiringImage1.Visible = false;
            AttachWiringImageDelete1.Visible = false;
            ReplaceWiringImageLabel1.Visible = false;
            AttachWiringFileUrl1.Text = "";

            ExistingWiringFilename2.Value = "";
            AttachWiringImage2.Visible = false;
            AttachWiringImageDelete2.Visible = false;
            ReplaceWiringImageLabel2.Visible = false;
            AttachWiringFileUrl2.Text = "";

            ExistingWiringFilename3.Value = "";
            AttachWiringImage3.Visible = false;
            AttachWiringImageDelete3.Visible = false;
            ReplaceWiringImageLabel3.Visible = false;
            AttachWiringFileUrl3.Text = "";

            ExistingWiringFilename4.Value = "";
            AttachWiringImage4.Visible = false;
            AttachWiringImageDelete4.Visible = false;
            ReplaceWiringImageLabel4.Visible = false;
            AttachWiringFileUrl4.Text = "";

            ExistingWiringFilename5.Value = "";
            AttachWiringImage5.Visible = false;
            AttachWiringImageDelete5.Visible = false;
            ReplaceWiringImageLabel5.Visible = false;
            AttachWiringFileUrl5.Text = "";

            AddWiringTitle.Text = "Add Wiring";
            AddWireButton.Text = "Add";
            AddWirePanel.Visible = true;
        }

        private void LoadWireInstall()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId, WireFunctionName from dbo.WireFunction with (nolock) where Inactive=0  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                WireFunctionList.DataTextField = "WireFunctionName";
                WireFunctionList.DataValueField = "WireFunctionId";
                WireFunctionList.DataSource = ds2;
                WireFunctionList.DataBind();

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            if (VehicleModelList.Items.Count <= 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Vehicle");
                return;
            }
            if (VehicleYearList.Items.Count <= 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Vehicle");
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachWiringFile1.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachWiringFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachWiringFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachWiringFile2.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachWiringFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachWiringFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachWiringFile3.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachWiringFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachWiringFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachWiringFile4.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachWiringFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachWiringFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachWiringFile5.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachWiringFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachWiringFileUrl5.Text.Trim();
            }

            if (AddWireButton.Text == "Add")
            {
                AddVehicleWire(VehicleMakeModelYearIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                    PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), FileName1, FileName2, FileName3, FileName4, FileName5);
            }
            else
            {
                if ((AttachWiringImageDelete1.Checked && ExistingWiringFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename1.Value);
                    }
                }
                if ((AttachWiringImageDelete2.Checked && ExistingWiringFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename2.Value);
                    }
                }
                if ((AttachWiringImageDelete3.Checked && ExistingWiringFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename3.Value);
                    }
                }
                if ((AttachWiringImageDelete4.Checked && ExistingWiringFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename4.Value);
                    }
                }
                if ((AttachWiringImageDelete5.Checked && ExistingWiringFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename5.Value);
                    }
                }

                string Attach1 = "";
                if (AttachWiringImageDelete1.Visible && AttachWiringImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingWiringFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachWiringImageDelete2.Visible && AttachWiringImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingWiringFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachWiringImageDelete3.Visible && AttachWiringImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingWiringFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachWiringImageDelete4.Visible && AttachWiringImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingWiringFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachWiringImageDelete5.Visible && AttachWiringImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingWiringFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                UpdateVehicleWire(VehicleWireFunctionIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                    PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), Attach1, Attach2, Attach3, Attach4, Attach5);
            }

            VehicleColor.Text = "";
            Color.Text = "";
            PinOut.Text = "";
            Location.Text = "";
            Polarity.Text = "";

            SearchInfo(1);
            AddWirePanel.Visible = false;
            EditNotePanel.Visible = false;
        }

        private void AddVehicleWire(string VehicleMakeModelYearId, string WireFunctionId, string InstallationTypeId, string VehicleColor, string Colour, string PinOut,
            string Location, string Polarity, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string sql = "insert into dbo.VehicleWireFunction (VehicleMakeModelYearId, ";
                sql += "WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy, Inactive) ";
                sql += "select @VehicleMakeModelYearId, ";
                sql += "@WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy, 0";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateVehicleWire(string VehicleWireFunctionId, string WireFunctionId, string InstallationTypeId, string VehicleColor, string Colour, string PinOut,
            string Location, string Polarity, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string sql = "update dbo.VehicleWireFunction ";
                sql += "set WireFunctionId = @WireFunctionId, InstallationTypeId=@InstallationTypeId, VehicleColor=@VehicleColor, ";
                sql += "Colour=@Colour, PinOut=@PinOut, Location=@Location, Polarity=@Polarity, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            AddWirePanel.Visible = false;
        }

        protected void WireList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Wiring?');";
                    }
                }
            }
        }

        protected void WireList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWireInstall();
            VehicleWireFunctionIdHidden.Value = ((HiddenField)(WireList.SelectedRow.FindControl("VehicleWireFunctionId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "select WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFunction WITH (NOLOCK) ";
                sql += "where VehicleWireFunctionId = @VehicleWireFunctionId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionList.SelectedValue = reader["WireFunctionId"].ToString();
                    InstalltionTypeList.SelectedValue = reader["InstallationTypeId"].ToString();
                    VehicleColor.Text = reader["VehicleColor"].ToString();
                    Color.Text = reader["Colour"].ToString();
                    PinOut.Text = reader["PinOut"].ToString();
                    Location.Text = reader["Location"].ToString();
                    Polarity.Text = reader["Polarity"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage1.ImageUrl = attach;
                            AttachWiringFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage1.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl1.Text = "";
                        }
                        ExistingWiringFilename1.Value = attach;
                        AttachWiringImage1.Visible = true;

                        AttachWiringImageDelete1.Visible = true;
                        ReplaceWiringImageLabel1.Visible = true;

                        AttachWiringImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename1.Value = "";
                        AttachWiringFileUrl1.Text = "";
                        AttachWiringImage1.Visible = false;
                        AttachWiringImageDelete1.Visible = false;
                        ReplaceWiringImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage2.ImageUrl = attach;
                            AttachWiringFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage2.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl2.Text = "";
                        }
                        ExistingWiringFilename2.Value = attach;
                        AttachWiringImage2.Visible = true;

                        AttachWiringImageDelete2.Visible = true;
                        ReplaceWiringImageLabel2.Visible = true;

                        AttachWiringImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename2.Value = "";
                        AttachWiringFileUrl2.Text = "";
                        AttachWiringImage2.Visible = false;
                        AttachWiringImageDelete2.Visible = false;
                        ReplaceWiringImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage3.ImageUrl = attach;
                            AttachWiringFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage3.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl3.Text = "";
                        }
                        ExistingWiringFilename3.Value = attach;
                        AttachWiringImage3.Visible = true;

                        AttachWiringImageDelete3.Visible = true;
                        ReplaceWiringImageLabel3.Visible = true;

                        AttachWiringImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename3.Value = "";
                        AttachWiringFileUrl3.Text = "";
                        AttachWiringImage3.Visible = false;
                        AttachWiringImageDelete3.Visible = false;
                        ReplaceWiringImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage4.ImageUrl = attach;
                            AttachWiringFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage4.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl4.Text = "";
                        }
                        ExistingWiringFilename4.Value = attach;
                        AttachWiringImage4.Visible = true;

                        AttachWiringImageDelete4.Visible = true;
                        ReplaceWiringImageLabel4.Visible = true;

                        AttachWiringImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename4.Value = "";
                        AttachWiringFileUrl4.Text = "";
                        AttachWiringImage4.Visible = false;
                        AttachWiringImageDelete4.Visible = false;
                        ReplaceWiringImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage5.ImageUrl = attach;
                            AttachWiringFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage5.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl5.Text = "";
                        }
                        ExistingWiringFilename5.Value = attach;
                        AttachWiringImage5.Visible = true;

                        AttachWiringImageDelete5.Visible = true;
                        ReplaceWiringImageLabel5.Visible = true;

                        AttachWiringImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename5.Value = "";
                        AttachWiringFileUrl5.Text = "";
                        AttachWiringImage5.Visible = false;
                        AttachWiringImageDelete5.Visible = false;
                        ReplaceWiringImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddWiringTitle.Text = "Edit Wiring";
                AddWireButton.Text = "Save";
                AddWirePanel.Visible = true;
                EditNotePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void WireList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFunctionIdValue = ((HiddenField)(WireList.Rows[e.RowIndex].FindControl("VehicleWireFunctionId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireFunctionId, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFunction a ";
                sql += "where VehicleWireFunctionId=" + VehicleWireFunctionIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireFunction where VehicleWireFunctionId=" + VehicleWireFunctionIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                SearchInfo(1);

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }



        protected void AddDisassemblyPanelButton_Click(object sender, EventArgs e)
        {
            AddDisassemblyTitle.Text = "Add Disassembly";
            AddDisassemblyButton.Text = "Add";
            VehicleWireDisassemblyIdHidden.Value = "";
            AddDisassemblyPanel.Visible = true;

            DisassemblyStep.Text = "";
            Disassemblyeditor.Text = "";

            AttachDisassemblyImageDelete1.Checked = false;
            AttachDisassemblyImageDelete2.Checked = false;
            AttachDisassemblyImageDelete3.Checked = false;
            AttachDisassemblyImageDelete4.Checked = false;
            AttachDisassemblyImageDelete5.Checked = false;

            ExistingDisassemblyFilename1.Value = "";
            AttachDisassemblyImage1.Visible = false;
            AttachDisassemblyImageDelete1.Visible = false;
            ReplaceDisassemblyImageLabel1.Visible = false;
            AttachDisassemblyFileUrl1.Text = "";

            ExistingDisassemblyFilename2.Value = "";
            AttachDisassemblyImage2.Visible = false;
            AttachDisassemblyImageDelete2.Visible = false;
            ReplaceDisassemblyImageLabel2.Visible = false;
            AttachDisassemblyFileUrl2.Text = "";

            ExistingDisassemblyFilename3.Value = "";
            AttachDisassemblyImage3.Visible = false;
            AttachDisassemblyImageDelete3.Visible = false;
            ReplaceDisassemblyImageLabel3.Visible = false;
            AttachDisassemblyFileUrl3.Text = "";

            ExistingDisassemblyFilename4.Value = "";
            AttachDisassemblyImage4.Visible = false;
            AttachDisassemblyImageDelete4.Visible = false;
            ReplaceDisassemblyImageLabel4.Visible = false;
            AttachDisassemblyFileUrl4.Text = "";

            ExistingDisassemblyFilename5.Value = "";
            AttachDisassemblyImage5.Visible = false;
            AttachDisassemblyImageDelete5.Visible = false;
            ReplaceDisassemblyImageLabel5.Visible = false;
            AttachDisassemblyFileUrl5.Text = "";

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void ResetTab()
        {
            WiringPaneltab.Attributes.Add("Class", "tab-title");
            FBPaneltab.Attributes.Add("Class", "tab-title");
            DocumentPaneltab.Attributes.Add("Class", "tab-title");
            linksdisassemblytab.Attributes.Add("Class", "tab-title");
            linkspreptab.Attributes.Add("Class", "tab-title");
            linkroutingtab.Attributes.Add("Class", "tab-title");
            linkprogrammingtab.Attributes.Add("Class", "tab-title");
            TSBPaneltab.Attributes.Add("Class", "tab-title");

            WiringPanel.Attributes.Add("Class", "content");
            FBPanel.Attributes.Add("Class", "content");
            DocumentPanel.Attributes.Add("Class", "content");
            linksdisassembly.Attributes.Add("Class", "content");
            linksprep.Attributes.Add("Class", "content");
            linkrouting.Attributes.Add("Class", "content");
            linkprogramming.Attributes.Add("Class", "content");
            TSBPanel.Attributes.Add("Class", "content");
        }

        protected void CancelDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDisassemblyPanel.Visible = false;

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName)
        {
            string DocumentName;
            SaveFile(PFile, FullPath, Prefix, Make, Model, Year, out FileName, out DocumentName);
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName, out string DocumentName)
        {
            FileName = "";
            DocumentName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                DocumentName = filename;
                filename = Prefix + "_" + Make + "_" + Model + "_" + Year + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void AddDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (DisassemblyStep.Text == "")
            {
                ShowError("Please enter Step");
                DisassemblyStep.Focus();
                return;
            }
            if (Disassemblyeditor.Text == "")
            {
                ShowError("Please enter Note");
                Disassemblyeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachDisassemblyFile1.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachDisassemblyFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachDisassemblyFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachDisassemblyFile2.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachDisassemblyFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachDisassemblyFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachDisassemblyFile3.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachDisassemblyFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachDisassemblyFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachDisassemblyFile4.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachDisassemblyFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachDisassemblyFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachDisassemblyFile5.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachDisassemblyFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachDisassemblyFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWireDisassemblyIdHidden.Value == "")
                {
                    AddDisassembly(VehicleMakeModelYearIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else
                {
                    if ((AttachDisassemblyImageDelete1.Checked && ExistingDisassemblyFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename1.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete2.Checked && ExistingDisassemblyFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename2.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete3.Checked && ExistingDisassemblyFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename3.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete4.Checked && ExistingDisassemblyFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename4.Value);
                        }
                    }
                    if ((AttachDisassemblyImageDelete5.Checked && ExistingDisassemblyFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingDisassemblyFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingDisassemblyFilename5.Value);
                        }
                    }

                    string Attach1 = "";
                    if (AttachDisassemblyImageDelete1.Visible && AttachDisassemblyImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingDisassemblyFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachDisassemblyImageDelete2.Visible && AttachDisassemblyImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingDisassemblyFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachDisassemblyImageDelete3.Visible && AttachDisassemblyImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingDisassemblyFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachDisassemblyImageDelete4.Visible && AttachDisassemblyImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingDisassemblyFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachDisassemblyImageDelete5.Visible && AttachDisassemblyImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingDisassemblyFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    UpdateDisassembly(VehicleWireDisassemblyIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }

                SearchInfo(4);
                AddDisassemblyPanel.Visible = false;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        private void AddDisassembly(string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleWireDisassembly (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateDisassembly(string VehicleWireDisassemblyId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleWireDisassembly ";
                sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void DisassemblyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireDisassemblyIdHidden.Value = ((HiddenField)(DisassemblyList.SelectedRow.FindControl("VehicleWireDisassemblyId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireDisassemblyId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireDisassembly  ";
                sql += "where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    DisassemblyStep.Text = reader["Step"].ToString();
                    Disassemblyeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage1.ImageUrl = attach;
                            AttachDisassemblyFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage1.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl1.Text = "";
                        }

                        ExistingDisassemblyFilename1.Value = attach;
                        AttachDisassemblyImage1.Visible = true;

                        AttachDisassemblyImageDelete1.Visible = true;
                        ReplaceDisassemblyImageLabel1.Visible = true;

                        AttachDisassemblyImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename1.Value = "";
                        AttachDisassemblyFileUrl1.Text = "";
                        AttachDisassemblyImage1.Visible = false;
                        AttachDisassemblyImageDelete1.Visible = false;
                        ReplaceDisassemblyImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage2.ImageUrl = attach;
                            AttachDisassemblyFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage2.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl2.Text = "";
                        }

                        ExistingDisassemblyFilename2.Value = attach;
                        AttachDisassemblyImage2.Visible = true;

                        AttachDisassemblyImageDelete2.Visible = true;
                        ReplaceDisassemblyImageLabel2.Visible = true;

                        AttachDisassemblyImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename2.Value = "";
                        AttachDisassemblyFileUrl2.Text = "";
                        AttachDisassemblyImage2.Visible = false;
                        AttachDisassemblyImageDelete2.Visible = false;
                        ReplaceDisassemblyImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage3.ImageUrl = attach;
                            AttachDisassemblyFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage3.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl3.Text = "";
                        }

                        ExistingDisassemblyFilename3.Value = attach;
                        AttachDisassemblyImage3.Visible = true;

                        AttachDisassemblyImageDelete3.Visible = true;
                        ReplaceDisassemblyImageLabel3.Visible = true;

                        AttachDisassemblyImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename3.Value = "";
                        AttachDisassemblyFileUrl3.Text = "";
                        AttachDisassemblyImage3.Visible = false;
                        AttachDisassemblyImageDelete3.Visible = false;
                        ReplaceDisassemblyImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage4.ImageUrl = attach;
                            AttachDisassemblyFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage4.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl4.Text = "";
                        }

                        ExistingDisassemblyFilename4.Value = attach;
                        AttachDisassemblyImage4.Visible = true;

                        AttachDisassemblyImageDelete4.Visible = true;
                        ReplaceDisassemblyImageLabel4.Visible = true;

                        AttachDisassemblyImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename4.Value = "";
                        AttachDisassemblyFileUrl4.Text = "";
                        AttachDisassemblyImage4.Visible = false;
                        AttachDisassemblyImageDelete4.Visible = false;
                        ReplaceDisassemblyImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage5.ImageUrl = attach;
                            AttachDisassemblyFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage5.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl5.Text = "";
                        }

                        ExistingDisassemblyFilename5.Value = attach;
                        AttachDisassemblyImage5.Visible = true;

                        AttachDisassemblyImageDelete5.Visible = true;
                        ReplaceDisassemblyImageLabel5.Visible = true;

                        AttachDisassemblyImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename5.Value = "";
                        AttachDisassemblyFileUrl5.Text = "";
                        AttachDisassemblyImage5.Visible = false;
                        AttachDisassemblyImageDelete5.Visible = false;
                        ReplaceDisassemblyImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddDisassemblyTitle.Text = "Edit Disassembly";
                AddDisassemblyButton.Text = "Save";
                AddDisassemblyPanel.Visible = true;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void DisassemblyList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireDisassemblyIdValue = ((HiddenField)(DisassemblyList.Rows[e.RowIndex].FindControl("VehicleWireDisassemblyId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireDisassemblyId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireDisassembly a ";
                sql += "where VehicleWireDisassemblyId=" + VehicleWireDisassemblyIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireDisassembly where VehicleWireDisassemblyId=" + VehicleWireDisassemblyIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddDisassemblyPanel.Visible = false;
                SearchInfo(4);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddPrepPanelButton_Click(object sender, EventArgs e)
        {
            AddPrepTitle.Text = "Add Prep";
            AddPrepButton.Text = "Add";
            VehicleWirePrepIdHidden.Value = "";
            AddPrepPanel.Visible = true;

            PrepStep.Text = "";
            Prepeditor.Text = "";

            AttachPrepImageDelete1.Checked = false;
            AttachPrepImageDelete2.Checked = false;
            AttachPrepImageDelete3.Checked = false;
            AttachPrepImageDelete4.Checked = false;
            AttachPrepImageDelete5.Checked = false;

            ExistingPrepFilename1.Value = "";
            AttachPrepImage1.Visible = false;
            AttachPrepImageDelete1.Visible = false;
            ReplacePrepImageLabel1.Visible = false;
            AttachPrepFileUrl1.Text = "";

            ExistingPrepFilename2.Value = "";
            AttachPrepImage2.Visible = false;
            AttachPrepImageDelete2.Visible = false;
            ReplacePrepImageLabel2.Visible = false;
            AttachPrepFileUrl2.Text = "";

            ExistingPrepFilename3.Value = "";
            AttachPrepImage3.Visible = false;
            AttachPrepImageDelete3.Visible = false;
            ReplacePrepImageLabel3.Visible = false;
            AttachPrepFileUrl3.Text = "";

            ExistingPrepFilename4.Value = "";
            AttachPrepImage4.Visible = false;
            AttachPrepImageDelete4.Visible = false;
            ReplacePrepImageLabel4.Visible = false;
            AttachPrepFileUrl4.Text = "";

            ExistingPrepFilename5.Value = "";
            AttachPrepImage5.Visible = false;
            AttachPrepImageDelete5.Visible = false;
            ReplacePrepImageLabel5.Visible = false;
            AttachPrepFileUrl5.Text = "";

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");

        }

        protected void CancelPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddPrepPanel.Visible = false;

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");
        }

        protected void AddPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (PrepStep.Text == "")
            {
                ShowError("Please enter Step");
                PrepStep.Focus();
                return;
            }
            if (Prepeditor.Text == "")
            {
                ShowError("Please enter Note");
                Prepeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachPrepFile1.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachPrepFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachPrepFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachPrepFile2.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachPrepFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachPrepFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachPrepFile3.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachPrepFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachPrepFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachPrepFile4.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachPrepFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachPrepFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachPrepFile5.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachPrepFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachPrepFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWirePrepIdHidden.Value == "")
                {
                    AddPrep(VehicleMakeModelYearIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else
                {
                    if ((AttachPrepImageDelete1.Checked && ExistingPrepFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename1.Value);
                        }
                    }
                    if ((AttachPrepImageDelete2.Checked && ExistingPrepFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename2.Value);
                        }
                    }
                    if ((AttachPrepImageDelete3.Checked && ExistingPrepFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename3.Value);
                        }
                    }
                    if ((AttachPrepImageDelete4.Checked && ExistingPrepFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename4.Value);
                        }
                    }
                    if ((AttachPrepImageDelete5.Checked && ExistingPrepFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingPrepFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingPrepFilename5.Value);
                        }
                    }


                    string Attach1 = "";
                    if (AttachPrepImageDelete1.Visible && AttachPrepImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingPrepFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachPrepImageDelete2.Visible && AttachPrepImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingPrepFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachPrepImageDelete3.Visible && AttachPrepImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingPrepFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachPrepImageDelete4.Visible && AttachPrepImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingPrepFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachPrepImageDelete5.Visible && AttachPrepImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingPrepFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    UpdatePrep(VehicleWirePrepIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);

                }

                SearchInfo(5);
                AddPrepPanel.Visible = false;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void AddPrep(string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleWirePrep (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdatePrep(string VehicleWirePrepId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleWirePrep ";
                sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void PrepList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePrepIdHidden.Value = ((HiddenField)(PrepList.SelectedRow.FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePrepId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePrep  ";
                sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    PrepStep.Text = reader["Step"].ToString();
                    Prepeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage1.ImageUrl = attach;
                            AttachPrepFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage1.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl1.Text = "";
                        }
                        ExistingPrepFilename1.Value = attach;
                        AttachPrepImage1.Visible = true;

                        AttachPrepImageDelete1.Visible = true;
                        ReplacePrepImageLabel1.Visible = true;

                        AttachPrepImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename1.Value = "";
                        AttachPrepFileUrl1.Text = "";
                        AttachPrepImage1.Visible = false;
                        AttachPrepImageDelete1.Visible = false;
                        ReplacePrepImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage2.ImageUrl = attach;
                            AttachPrepFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage2.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl2.Text = "";
                        }
                        ExistingPrepFilename2.Value = attach;
                        AttachPrepImage2.Visible = true;

                        AttachPrepImageDelete2.Visible = true;
                        ReplacePrepImageLabel2.Visible = true;

                        AttachPrepImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename2.Value = "";
                        AttachPrepFileUrl2.Text = "";
                        AttachPrepImage2.Visible = false;
                        AttachPrepImageDelete2.Visible = false;
                        ReplacePrepImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage3.ImageUrl = attach;
                            AttachPrepFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage3.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl3.Text = "";
                        }
                        ExistingPrepFilename3.Value = attach;
                        AttachPrepImage3.Visible = true;

                        AttachPrepImageDelete3.Visible = true;
                        ReplacePrepImageLabel3.Visible = true;

                        AttachPrepImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename3.Value = "";
                        AttachPrepFileUrl3.Text = "";
                        AttachPrepImage3.Visible = false;
                        AttachPrepImageDelete3.Visible = false;
                        ReplacePrepImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage4.ImageUrl = attach;
                            AttachPrepFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage4.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl4.Text = "";
                        }
                        ExistingPrepFilename4.Value = attach;
                        AttachPrepImage4.Visible = true;

                        AttachPrepImageDelete4.Visible = true;
                        ReplacePrepImageLabel4.Visible = true;

                        AttachPrepImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename4.Value = "";
                        AttachPrepFileUrl4.Text = "";
                        AttachPrepImage4.Visible = false;
                        AttachPrepImageDelete4.Visible = false;
                        ReplacePrepImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage5.ImageUrl = attach;
                            AttachPrepFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage5.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl5.Text = "";
                        }
                        ExistingPrepFilename5.Value = attach;
                        AttachPrepImage5.Visible = true;

                        AttachPrepImageDelete5.Visible = true;
                        ReplacePrepImageLabel5.Visible = true;

                        AttachPrepImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename5.Value = "";
                        AttachPrepFileUrl5.Text = "";
                        AttachPrepImage5.Visible = false;
                        AttachPrepImageDelete5.Visible = false;
                        ReplacePrepImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddPrepTitle.Text = "Edit Prep";
                AddPrepButton.Text = "Save";
                AddPrepPanel.Visible = true;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void PrepList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePrepIdValue = ((HiddenField)(PrepList.Rows[e.RowIndex].FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWirePrepId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWirePrep a ";
                sql += "where VehicleWirePrepId=" + VehicleWirePrepIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWirePrep where VehicleWirePrepId=" + VehicleWirePrepIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddPrepPanel.Visible = false;
                SearchInfo(5);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void AddRoutingPanelButton_Click(object sender, EventArgs e)
        {
            AddRoutingTitle.Text = "Add Routing/Placement";
            AddRoutingButton.Text = "Add";
            VehicleWirePlacementRoutingIdHidden.Value = "";
            AddRoutingPanel.Visible = true;

            RoutingStep.Text = "";
            Routingeditor.Text = "";

            AttachRoutingImageDelete1.Checked = false;
            AttachRoutingImageDelete2.Checked = false;
            AttachRoutingImageDelete3.Checked = false;
            AttachRoutingImageDelete4.Checked = false;
            AttachRoutingImageDelete5.Checked = false;

            ExistingRoutingFilename1.Value = "";
            AttachRoutingImage1.Visible = false;
            AttachRoutingImageDelete1.Visible = false;
            ReplaceRoutingImageLabel1.Visible = false;
            AttachRoutingFileUrl1.Text = "";

            ExistingRoutingFilename2.Value = "";
            AttachRoutingImage2.Visible = false;
            AttachRoutingImageDelete2.Visible = false;
            ReplaceRoutingImageLabel2.Visible = false;
            AttachRoutingFileUrl2.Text = "";

            ExistingRoutingFilename3.Value = "";
            AttachRoutingImage3.Visible = false;
            AttachRoutingImageDelete3.Visible = false;
            ReplaceRoutingImageLabel3.Visible = false;
            AttachRoutingFileUrl3.Text = "";

            ExistingRoutingFilename4.Value = "";
            AttachRoutingImage4.Visible = false;
            AttachRoutingImageDelete4.Visible = false;
            ReplaceRoutingImageLabel4.Visible = false;
            AttachRoutingFileUrl4.Text = "";

            ExistingRoutingFilename5.Value = "";
            AttachRoutingImage5.Visible = false;
            AttachRoutingImageDelete5.Visible = false;
            ReplaceRoutingImageLabel5.Visible = false;
            AttachRoutingFileUrl5.Text = "";

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");

        }

        protected void CancelRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddRoutingPanel.Visible = false;

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");
        }

        protected void AddRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (RoutingStep.Text == "")
            {
                ShowError("Please enter Step");
                RoutingStep.Focus();
                return;
            }
            if (Routingeditor.Text == "")
            {
                ShowError("Please enter Note");
                Routingeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachRoutingFile1.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachRoutingFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachRoutingFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachRoutingFile2.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachRoutingFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachRoutingFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachRoutingFile3.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachRoutingFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachRoutingFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachRoutingFile4.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachRoutingFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachRoutingFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachRoutingFile5.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachRoutingFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachRoutingFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWirePlacementRoutingIdHidden.Value == "")
                {
                    AddRouting(VehicleMakeModelYearIdHidden.Value, RoutingStep.Text, Routingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else
                {
                    if ((AttachRoutingImageDelete1.Checked && ExistingRoutingFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename1.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete2.Checked && ExistingRoutingFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename2.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete3.Checked && ExistingRoutingFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename3.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete4.Checked && ExistingRoutingFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename4.Value);
                        }
                    }
                    if ((AttachRoutingImageDelete5.Checked && ExistingRoutingFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingRoutingFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingRoutingFilename5.Value);
                        }
                    }

                    string Attach1 = "";
                    if (AttachRoutingImageDelete1.Visible && AttachRoutingImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingRoutingFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachRoutingImageDelete2.Visible && AttachRoutingImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingRoutingFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachRoutingImageDelete3.Visible && AttachRoutingImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingRoutingFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachRoutingImageDelete4.Visible && AttachRoutingImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingRoutingFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachRoutingImageDelete5.Visible && AttachRoutingImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingRoutingFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    UpdateRouting(VehicleWirePlacementRoutingIdHidden.Value, RoutingStep.Text, Routingeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }

                SearchInfo(6);
                AddRoutingPanel.Visible = false;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        private void AddRouting(string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleWirePlacementRouting (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateRouting(string VehicleWirePlacementRoutingId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleWirePlacementRouting ";
                sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void RoutingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePlacementRoutingIdHidden.Value = ((HiddenField)(RoutingList.SelectedRow.FindControl("VehicleWirePlacementRoutingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePlacementRoutingId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePlacementRouting  ";
                sql += "where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    RoutingStep.Text = reader["Step"].ToString();
                    Routingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage1.ImageUrl = attach;
                            AttachRoutingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage1.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl1.Text = "";
                        }
                        ExistingRoutingFilename1.Value = attach;
                        AttachRoutingImage1.Visible = true;

                        AttachRoutingImageDelete1.Visible = true;
                        ReplaceRoutingImageLabel1.Visible = true;

                        AttachRoutingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename1.Value = "";
                        AttachRoutingFileUrl1.Text = "";
                        AttachRoutingImage1.Visible = false;
                        AttachRoutingImageDelete1.Visible = false;
                        ReplaceRoutingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage2.ImageUrl = attach;
                            AttachRoutingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage2.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl2.Text = "";
                        }
                        ExistingRoutingFilename2.Value = attach;
                        AttachRoutingImage2.Visible = true;

                        AttachRoutingImageDelete2.Visible = true;
                        ReplaceRoutingImageLabel2.Visible = true;

                        AttachRoutingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename2.Value = "";
                        AttachRoutingFileUrl2.Text = "";
                        AttachRoutingImage2.Visible = false;
                        AttachRoutingImageDelete2.Visible = false;
                        ReplaceRoutingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage3.ImageUrl = attach;
                            AttachRoutingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage3.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl3.Text = "";
                        }
                        ExistingRoutingFilename3.Value = attach;
                        AttachRoutingImage3.Visible = true;

                        AttachRoutingImageDelete3.Visible = true;
                        ReplaceRoutingImageLabel3.Visible = true;

                        AttachRoutingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename3.Value = "";
                        AttachRoutingFileUrl3.Text = "";
                        AttachRoutingImage3.Visible = false;
                        AttachRoutingImageDelete3.Visible = false;
                        ReplaceRoutingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage4.ImageUrl = attach;
                            AttachRoutingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage4.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl4.Text = "";
                        }
                        ExistingRoutingFilename4.Value = attach;
                        AttachRoutingImage4.Visible = true;

                        AttachRoutingImageDelete4.Visible = true;
                        ReplaceRoutingImageLabel4.Visible = true;

                        AttachRoutingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename4.Value = "";
                        AttachRoutingFileUrl4.Text = "";
                        AttachRoutingImage4.Visible = false;
                        AttachRoutingImageDelete4.Visible = false;
                        ReplaceRoutingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage5.ImageUrl = attach;
                            AttachRoutingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage5.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl5.Text = "";
                        }
                        ExistingRoutingFilename5.Value = attach;
                        AttachRoutingImage5.Visible = true;

                        AttachRoutingImageDelete5.Visible = true;
                        ReplaceRoutingImageLabel5.Visible = true;

                        AttachRoutingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename5.Value = "";
                        AttachRoutingFileUrl5.Text = "";
                        AttachRoutingImage5.Visible = false;
                        AttachRoutingImageDelete5.Visible = false;
                        ReplaceRoutingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddRoutingTitle.Text = "Edit Routing/Placement";
                AddRoutingButton.Text = "Save";
                AddRoutingPanel.Visible = true;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void RoutingList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePlacementRoutingIdValue = ((HiddenField)(RoutingList.Rows[e.RowIndex].FindControl("VehicleWirePlacementRoutingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWirePlacementRoutingId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWirePlacementRouting a ";
                sql += "where VehicleWirePlacementRoutingId=" + VehicleWirePlacementRoutingIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWirePlacementRouting where VehicleWirePlacementRoutingId=" + VehicleWirePlacementRoutingIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddRoutingPanel.Visible = false;
                SearchInfo(6);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddFBPanelButton_Click(object sender, EventArgs e)
        {
            AddFBTitle.Text = "Add Facebook";
            AddFBButton.Text = "Add";
            VehicleWireFacebookIdHidden.Value = "";
            AddFBPanel.Visible = true;

            FBeditor.Text = "";
            FBUrl.Text = "";

            AttachFBImageDelete1.Checked = false;
            AttachFBImageDelete2.Checked = false;
            AttachFBImageDelete3.Checked = false;
            AttachFBImageDelete4.Checked = false;
            AttachFBImageDelete5.Checked = false;

            ExistingFBFilename1.Value = "";
            AttachFBImage1.Visible = false;
            AttachFBImageDelete1.Visible = false;
            ReplaceFBImageLabel1.Visible = false;
            AttachFBFileUrl1.Text = "";

            ExistingFBFilename2.Value = "";
            AttachFBImage2.Visible = false;
            AttachFBImageDelete2.Visible = false;
            ReplaceFBImageLabel2.Visible = false;
            AttachFBFileUrl2.Text = "";

            ExistingFBFilename3.Value = "";
            AttachFBImage3.Visible = false;
            AttachFBImageDelete3.Visible = false;
            ReplaceFBImageLabel3.Visible = false;
            AttachFBFileUrl3.Text = "";

            ExistingFBFilename4.Value = "";
            AttachFBImage4.Visible = false;
            AttachFBImageDelete4.Visible = false;
            ReplaceFBImageLabel4.Visible = false;
            AttachFBFileUrl4.Text = "";

            ExistingFBFilename5.Value = "";
            AttachFBImage5.Visible = false;
            AttachFBImageDelete5.Visible = false;
            ReplaceFBImageLabel5.Visible = false;
            AttachFBFileUrl5.Text = "";

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void CancelFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddFBPanel.Visible = false;

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void AddFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (FBeditor.Text == "")
            {
                ShowError("Please enter Facebook Result");
                FBeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachFBFile1.PostedFile, FullPath, "FB", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachFBFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachFBFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachFBFile2.PostedFile, FullPath, "FB", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachFBFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachFBFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachFBFile3.PostedFile, FullPath, "FB", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachFBFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachFBFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachFBFile4.PostedFile, FullPath, "FB", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachFBFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachFBFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachFBFile5.PostedFile, FullPath, "FB", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachFBFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachFBFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWireFacebookIdHidden.Value == "")
                {
                    AddFacebook(VehicleMakeModelYearIdHidden.Value, FBeditor.Text, FBUrl.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else
                {
                    if ((AttachFBImageDelete1.Checked && ExistingFBFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename1.Value);
                        }
                    }
                    if ((AttachFBImageDelete2.Checked && ExistingFBFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename2.Value);
                        }
                    }
                    if ((AttachFBImageDelete3.Checked && ExistingFBFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename3.Value);
                        }
                    }
                    if ((AttachFBImageDelete4.Checked && ExistingFBFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename4.Value);
                        }
                    }
                    if ((AttachFBImageDelete5.Checked && ExistingFBFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename5.Value);
                        }
                    }

                    string Attach1 = "";
                    if (AttachFBImageDelete1.Visible && AttachFBImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingFBFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachFBImageDelete2.Visible && AttachFBImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingFBFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachFBImageDelete3.Visible && AttachFBImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingFBFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachFBImageDelete4.Visible && AttachFBImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingFBFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachFBImageDelete5.Visible && AttachFBImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingFBFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    UpdateFacebook(VehicleWireFacebookIdHidden.Value, FBeditor.Text, FBUrl.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);

                }

                SearchInfo(2);
                AddFBPanel.Visible = false;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void AddFacebook(string VehicleMakeModelYearId, string Note, string URL, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleWireFacebook (VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Note, @URL, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateFacebook(string VehicleWireFacebookId, string Note, string URL, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleWireFacebook ";
                sql += "set Note=@Note, URL=@URL, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWireFacebookId = @VehicleWireFacebookId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Facebook Result?');";
                    }
                }
            }
        }

        protected void FBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireFacebookIdHidden.Value = ((HiddenField)(FBList.SelectedRow.FindControl("VehicleWireFacebookId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireFacebookId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireFacebook  ";
                sql += "where VehicleWireFacebookId = @VehicleWireFacebookId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    FBeditor.Text = reader["Note"].ToString();
                    FBUrl.Text = reader["URL"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage1.ImageUrl = attach;
                            AttachFBFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachFBImage1.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl1.Text = "";
                        }
                        ExistingFBFilename1.Value = attach;
                        AttachFBImage1.Visible = true;
                        AttachFBImageDelete1.Visible = true;
                        ReplaceFBImageLabel1.Visible = true;

                        AttachFBImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename1.Value = "";
                        AttachFBFileUrl1.Text = "";
                        AttachFBImage1.Visible = false;
                        AttachFBImageDelete1.Visible = false;
                        ReplaceFBImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage2.ImageUrl = attach;
                            AttachFBFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachFBImage2.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl2.Text = "";
                        }
                        ExistingFBFilename2.Value = attach;
                        AttachFBImage2.Visible = true;
                        AttachFBImageDelete2.Visible = true;
                        ReplaceFBImageLabel2.Visible = true;

                        AttachFBImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename2.Value = "";
                        AttachFBFileUrl2.Text = "";
                        AttachFBImage2.Visible = false;
                        AttachFBImageDelete2.Visible = false;
                        ReplaceFBImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage3.ImageUrl = attach;
                            AttachFBFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachFBImage3.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl3.Text = "";
                        }
                        ExistingFBFilename3.Value = attach;
                        AttachFBImage3.Visible = true;
                        AttachFBImageDelete3.Visible = true;
                        ReplaceFBImageLabel3.Visible = true;

                        AttachFBImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename3.Value = "";
                        AttachFBFileUrl3.Text = "";
                        AttachFBImage3.Visible = false;
                        AttachFBImageDelete3.Visible = false;
                        ReplaceFBImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage4.ImageUrl = attach;
                            AttachFBFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachFBImage4.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl4.Text = "";
                        }
                        ExistingFBFilename4.Value = attach;
                        AttachFBImage4.Visible = true;
                        AttachFBImageDelete4.Visible = true;
                        ReplaceFBImageLabel4.Visible = true;

                        AttachFBImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename4.Value = "";
                        AttachFBFileUrl4.Text = "";
                        AttachFBImage4.Visible = false;
                        AttachFBImageDelete4.Visible = false;
                        ReplaceFBImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage5.ImageUrl = attach;
                            AttachFBFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachFBImage5.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl5.Text = "";
                        }
                        ExistingFBFilename5.Value = attach;
                        AttachFBImage5.Visible = true;
                        AttachFBImageDelete5.Visible = true;
                        ReplaceFBImageLabel5.Visible = true;

                        AttachFBImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename5.Value = "";
                        AttachFBFileUrl5.Text = "";
                        AttachFBImage5.Visible = false;
                        AttachFBImageDelete5.Visible = false;
                        ReplaceFBImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddFBTitle.Text = "Edit Facebook";
                AddFBButton.Text = "Save";
                AddFBPanel.Visible = true;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFacebookIdValue = ((HiddenField)(FBList.SelectedRow.FindControl("VehicleWireFacebookId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireFacebookId, a.URL, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFacebook a ";
                sql += "where VehicleWireFacebookId=" + VehicleWireFacebookIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireFacebook where VehicleWireFacebookId=" + VehicleWireFacebookIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddFBPanel.Visible = false;
                SearchInfo(2);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddDocumentPanelButton_Click(object sender, EventArgs e)
        {
            AddDocumentTitle.Text = "Add Document";
            AddDocumentButton.Text = "Add";
            VehicleDocumentIdHidden.Value = "";
            AddDocumentPanel.Visible = true;

            ExistingDocument.Text = "";
            CurrentDocumentAttachFile.Value = "";
            AttachFileUrl.Text = "";

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void DocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[2].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this document?');";
                    }
                }

                HiddenField VehicleDocumentId = e.Row.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Row.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Row.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Row.FindControl("DocumentLabel") as Label;
                    if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
                    {
                        DocumentLabel.Text = "<a href='" + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }
            }
        }

        protected void DocumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleDocumentIdHidden.Value = ((HiddenField)(DocumentList.SelectedRow.FindControl("VehicleDocumentId"))).Value;

            HiddenField AttachFile = DocumentList.SelectedRow.FindControl("AttachFile") as HiddenField;
            Label DocumentLabel = DocumentList.SelectedRow.FindControl("DocumentLabel") as Label;

            ExistingDocument.Text = DocumentLabel.Text;
            CurrentDocumentAttachFile.Value = AttachFile.Value;

            AddDocumentTitle.Text = "Edit Document";
            AddDocumentButton.Text = "Save";
            AddDocumentPanel.Visible = true;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void DocumentList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleDocumentIdValue = ((HiddenField)(DocumentList.Rows[e.RowIndex].FindControl("VehicleDocumentId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select AttachFile ";
                sql += "from dbo.VehicleDocument a ";
                sql += "where VehicleDocumentId=@VehicleDocumentId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["AttachFile"].ToString()))
                        {
                            File.Delete(FullPath + reader["AttachFile"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleDocument where VehicleDocumentId=@VehicleDocumentId";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                cmd.ExecuteNonQuery();

                AddDocumentPanel.Visible = false;
                SearchInfo(3);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDocumentPanel.Visible = false;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void AddDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            string DocumentName;
            SaveFile(AttachFile.PostedFile, FullPath, "Document", Make, Model, Year, out FileName1, out DocumentName);
            if (FileName1 == "" && AttachFileUrl.Text.Trim() != "")
            {
                FileName1 = AttachFileUrl.Text.Trim();
                DocumentName = "URL";
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleDocumentIdHidden.Value == "")
                {
                    AddDocument(VehicleMakeModelYearIdHidden.Value, DocumentName, FileName1);
                }
                else
                {
                    if (File.Exists(FullPath + CurrentDocumentAttachFile.Value))
                    {
                        File.Delete(FullPath + CurrentDocumentAttachFile.Value);
                    }

                    UpdateDocument(VehicleDocumentIdHidden.Value, DocumentName, FileName1);
                }

                SearchInfo(3);
                AddDocumentPanel.Visible = false;

                ResetTab();
                DocumentPaneltab.Attributes.Add("Class", "tab-title active");
                DocumentPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void AddDocument(string VehicleMakeModelYearId, string DocumentName, string AttachFile)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleDocument (VehicleMakeModelYearId, DocumentName, AttachFile, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @DocumentName, @AttachFile, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateDocument(string VehicleDocumentId, string DocumentName, string AttachFile)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleDocument ";
                sql += "set DocumentName=@DocumentName, AttachFile=@AttachFile, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleDocumentId = @VehicleDocumentId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void EditUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessagePanel.Visible = true;
        }

        protected void CancelUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessagePanel.Visible = false;
        }

        protected void SaveUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessage(VehicleMakeModelYearIdHidden.Value, UrgentMessageEdit.Text);

            UpdateUrgentMessagePanel.Visible = false;
        }

        private void UpdateUrgentMessage(string VehicleMakeModelYearId, string msg)
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "if not exists (select * from dbo.VehicleUrgentMessage where VehicleMakeModelYearId=@VehicleMakeModelYearId) begin ";
                sql += "insert into dbo.VehicleUrgentMessage (VehicleMakeModelYearId, Note, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Note, getdate(), @UpdatedBy; end ";
                sql += "else begin ";
                sql += "update dbo.VehicleUrgentMessage set Note=@Note, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId; end";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar, 300).Value = msg;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                UrgentMessageLabel.Text = UrgentMessageEdit.Text;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ShowCommentList()
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select a.VehicleCommentId, a.Comment, b.FirstName + ' ' + b.LastName as Name, a.UpdatedDt ";
                sql += "from dbo.VehicleComment a ";
                sql += "left join dbo.AspNetUsers b on a.UpdatedBy = b.Id ";
                sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by a.UpdatedDt desc ";

                SqlCommand Cmd = new SqlCommand(sql, conn);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CommentList.DataSource = ds;
                CommentList.DataBind();

                if (CommentList != null && CommentList.HeaderRow != null && CommentList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    CommentList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    CommentList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    CommentList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    CommentList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    CommentList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    CommentList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    CommentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CommentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommentList.PageIndex = e.NewPageIndex;
            ShowCommentList();
        }

        protected void SaveInternalNoteButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (InternalNoteTxt.Text.Trim() == "")
            {
                ShowError("Please enter InternalNote.");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleInternalNote (VehicleMakeModelYearId, InternalNote, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @InternalNote, getdate(), @UpdatedBy; ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@InternalNote", SqlDbType.NVarChar).Value = InternalNoteTxt.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                InternalNoteTxt.Text = "";

                ShowInternalNotes();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ShowInternalNotes()
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select a.VehicleInternalNoteId, a.InternalNote, b.FirstName + ' ' + b.LastName as Name, a.UpdatedDt ";
                sql += "from dbo.VehicleInternalNote a ";
                sql += "left join dbo.AspNetUsers b on a.UpdatedBy = b.Id ";
                sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by a.UpdatedDt desc ";

                SqlCommand Cmd = new SqlCommand(sql, conn);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                InternalNoteList.DataSource = ds;
                InternalNoteList.DataBind();

                if (InternalNoteList != null && InternalNoteList.HeaderRow != null && InternalNoteList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    InternalNoteList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    InternalNoteList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    InternalNoteList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    InternalNoteList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    InternalNoteList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    InternalNoteList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    InternalNoteList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void InternalNoteList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            InternalNoteList.PageIndex = e.NewPageIndex;
            ShowInternalNotes();
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                StringBuilder excel = new StringBuilder();

                excel.AppendLine("Vehicle ID\t" + VehicleMakeModelYearIdHidden.Value);
                excel.AppendLine("Vehicle Make\t" + VehicleMakeList.SelectedItem.Text);
                excel.AppendLine("Vehicle Model\t" + VehicleModelList.SelectedItem.Text);
                excel.AppendLine("Vehicle Year\t" + VehicleYearList.SelectedItem.Text);
                excel.AppendLine("Link Vehicle ID\t" + LinkVehicleMakeModelYearIdHidden.Value);


                // Urgent Message
                string sql = GetUrgentQuery();

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                string umsg = "";
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    umsg = reader["Note"].ToString();
                }
                reader.Close();

                excel.AppendLine("");
                excel.AppendLine("[Urgent Message]");
                excel.AppendLine("Message:\t" + umsg.ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));

                // Wiring
                sql = GetWireQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Wiring]");
                excel.AppendLine("Id\tWire Function\tInstallation Type\tVehicle Color\tCM7X00/ADS Color\tLocation\tPin Out\tPolarity\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireFunctionId"].ToString());
                    excel.Append("\t"); excel.Append(reader["WireFunctionName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["InstallationTypeName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Colour"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["VehicleColor"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Location"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["PinOut"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Polarity"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Wire Note
                sql = GetWireNoteQuery();

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Wiring Note]");
                excel.AppendLine("Id\tInstallationTypeId\tNote");
                while (reader.Read())
                {
                    if (reader["VehicleWireNoteId"] != null && reader["VehicleWireNoteId"].ToString() != "")
                    {
                        excel.Append(reader["VehicleWireNoteId"].ToString());
                        excel.Append("\t"); excel.Append(reader["InstallationTypeId"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                        excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                        excel.AppendLine("");
                    }
                }
                reader.Close();

                // Disassembly
                sql = GetDisassemblyQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Disassembly]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireDisassemblyId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Prep
                sql = GetPrepQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Prep]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWirePrepId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Routing
                sql = GetRoutingQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Routing]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWirePlacementRoutingId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Programming
                sql = GetProgrammingQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Programming]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireProgrammingId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Facebook
                sql = GetFacebookQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Facebook]");
                excel.AppendLine("Id\tNote\tURL\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireFacebookId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["URL"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Document
                sql = GetDocumentQuery();

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Document]");
                excel.AppendLine("Id\tDocument Name\tFile");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleDocumentId"].ToString());
                    excel.Append("\t"); excel.Append(reader["DocumentName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["AttachFile"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                string VehicleName = VehicleMakeList.SelectedItem.Text.Trim() + "_" + VehicleModelList.SelectedItem.Text.Trim() + "_" + VehicleYearList.SelectedItem.Text.Trim();
                string filename = VehicleName + ".xls";
                string content_type = "text/csv";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = content_type;
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.Charset = "";
                this.EnableViewState = false;
                Response.Write(excel.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private string GetUrgentQuery()
        {
            return "select VehicleUrgentMessageId, Note from dbo.VehicleUrgentMessage where VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
        }

        private string GetWireQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireFunctionId, c.WireFunctionName, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, ";
            sql += "VehicleColor, PinOut, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireFunction a ";
            sql += "join dbo.VehicleMakeModelYear b on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId ";
            sql += "join dbo.WireFunction c on a.WireFunctionId = c.WireFunctionId ";
            sql += "left join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            if (SearchInstallationTypeList.SelectedValue != "-1")
            {
                sql += " and a.InstallationTypeId = " + SearchInstallationTypeList.SelectedValue + " ";
            }
            sql += " order by c.SortOrder ";

            return sql;
        }

        private string GetWireNoteQuery()
        {
            string sql = "select a.VehicleWireNoteId, a.Note, d.InstallationTypeId, d.InstallationTypeName ";
            sql += "from dbo.InstallationType d ";
            sql += "left join dbo.VehicleWireNote a on a.InstallationTypeId = d.InstallationTypeId ";
            sql += "and a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        private string GetDisassemblyQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireDisassemblyId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireDisassembly a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetPrepQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWirePrepId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWirePrep a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetRoutingQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWirePlacementRoutingId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWirePlacementRouting a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetProgrammingQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireProgrammingId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireProgramming a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetFacebookQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireFacebookId, a.URL, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireFacebook a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        private string GetDocumentQuery()
        {
            string sql = "select a.VehicleDocumentId, a.DocumentName, a.AttachFile ";
            sql += "from dbo.VehicleDocument a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        protected void LinkVehicleButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Link Vehicle";
            AddVehicleButton.Text = "Save";

            if (LinkVehicleMakeIdHidden.Value != "")
            {
                MakeList.SelectedValue = LinkVehicleMakeIdHidden.Value;
                ShowModelList(false);
                ModelList.SelectedValue = LinkVehicleModelIdHidden.Value;
                YearList.Visible = true;
                ShowYearList();
                YearList.SelectedValue = LinkVehicleYearHidden.Value;

                Year.Text = LinkVehicleYearHidden.Value;
                Year.Visible = false;

                UnlinkVehicleButton.Visible = true;
            }
            else
            {
                MakeList.SelectedIndex = -1;
                ModelList.SelectedIndex = -1;
                YearList.SelectedIndex = -1;
                YearList.Visible = true;

                Year.Text = "";
                Year.Visible = false;

                UnlinkVehicleButton.Visible = false;
            }

            NewVehiclePanel.Visible = true;
        }

        protected void ImportButton_Click(object sender, EventArgs e)
        {
            ImportPanel.Visible = true;
        }

        protected void ImportFileButton_Click(object sender, EventArgs e)
        {
            if (ImportFile.PostedFile != null && ImportFile.PostedFile.FileName != "")
            {
                string ImportVehicleMakeModelYearId = "";
                string LinkVehicleMakeModelYearId = "";
                var importFileString = new StreamReader(ImportFile.PostedFile.InputStream).ReadToEnd();
                string[] newlinesep = { Environment.NewLine };
                char[] tabsep = { '\t' };

                bool IsUrgentMessage = false;
                bool IsWiring = false;
                bool IsWiringNote = false;
                bool IsDisassembly = false;
                bool IsPrep = false;
                bool IsRouting = false;
                bool IsProgramming = false;
                bool IsFacebook = false;
                bool IsDocument = false;

                string[] lines = importFileString.Split(newlinesep, StringSplitOptions.None);
                foreach (string line in lines)
                {
                    if (line.Trim() != "")
                    {
                        string[] fields = line.Split(tabsep);
                        if (fields.Length > 0)
                        {
                            string field1 = fields[0].Trim().ToUpper();

                            if (fields[0].Trim().ToUpper() == "VEHICLE ID" && fields.Length > 1)
                            {
                                ImportVehicleMakeModelYearId = fields[1].Trim();
                            }
                            else if (fields[0].Trim().ToUpper() == "LINK VEHICLE ID" && fields.Length > 1)
                            {
                                LinkVehicleMakeModelYearId = fields[1].Trim();
                                LinkVehicleDatabase(ImportVehicleMakeModelYearId, LinkVehicleMakeModelYearId);
                            }
                            else if (field1 == "[URGENT MESSAGE]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsUrgentMessage = true;
                            }
                            else if (field1 == "[WIRING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsWiring = true;
                            }
                            else if (field1 == "[WIRING NOTE]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsWiringNote = true;
                            }
                            else if (field1 == "[DISASSEMBLY]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsDisassembly = true;
                            }
                            else if (field1 == "[PREP]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsPrep = true;
                            }
                            else if (field1 == "[ROUTING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsRouting = true;
                            }
                            else if (field1 == "[PROGRAMMING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsProgramming = true;
                            }
                            else if (field1 == "[FACEBOOK]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsFacebook = true;
                            }
                            else if (field1 == "[DOCUMENT]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsDocument = true;
                            }
                            else if (IsUrgentMessage && field1 == "MESSAGE:" && fields.Length > 1)
                            {
                                string msg = fields[1].Trim();
                                if (ImportVehicleMakeModelYearId != "" && msg != "")
                                {
                                    UpdateUrgentMessage(ImportVehicleMakeModelYearId, msg.Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                }
                            }
                            else if (IsWiring)
                            {
                                if (field1 != "ID" && fields.Length > 12)
                                {
                                    string FieldWireFunctionName = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldInstalltionTypeName = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldColor = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldVehicleColor = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldLocation = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldPinOut = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldPolarity = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[8].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[9].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[10].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[11].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[12].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    string FieldWireFunctionId = FindWireFunctionId(FieldWireFunctionName);
                                    string FieldInstalltionTypeId = FindInstalltionTypeId(FieldInstalltionTypeName);

                                    int VehicleWireFunctionId = 0;
                                    if (int.TryParse(field1, out VehicleWireFunctionId))
                                    {
                                        if (FieldWireFunctionId != "" && FieldInstalltionTypeId != "")
                                        {
                                            UpdateVehicleWire(VehicleWireFunctionId.ToString(), FieldWireFunctionId, FieldInstalltionTypeId,
                                                FieldVehicleColor, FieldColor,
                                                FieldPinOut, FieldLocation, FieldPolarity, FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                        }
                                    }
                                    else
                                    {
                                        if (ImportVehicleMakeModelYearId != "" && FieldWireFunctionId != "" && FieldInstalltionTypeId != "")
                                        {
                                            AddVehicleWire(ImportVehicleMakeModelYearId, FieldWireFunctionId, FieldInstalltionTypeId, FieldVehicleColor, FieldColor,
                                                FieldPinOut, FieldLocation, FieldPolarity, FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                        }
                                    }
                                }
                            }
                            else if (IsWiringNote)
                            {
                                if (field1 != "ID" && fields.Length > 2)
                                {
                                    string FieldInstallationTypeId = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldNote = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleWireNoteId = 0;
                                    if (int.TryParse(field1, out VehicleWireNoteId))
                                    {
                                        UpdateWireNote(ImportVehicleMakeModelYearId, VehicleWireNoteId.ToString(), FieldInstallationTypeId, FieldNote);
                                    }
                                    else
                                    {
                                        UpdateWireNote(ImportVehicleMakeModelYearId, "", FieldInstallationTypeId, FieldNote);
                                    }
                                }
                            }
                            else if (IsDisassembly)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWireDisassemblyId = 0;
                                    if (int.TryParse(field1, out VehicleWireDisassemblyId))
                                    {
                                        UpdateDisassembly(VehicleWireDisassemblyId.ToString(), FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddDisassembly(ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsPrep)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWirePrepId = 0;
                                    if (int.TryParse(field1, out VehicleWirePrepId))
                                    {
                                        UpdatePrep(VehicleWirePrepId.ToString(), FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddPrep(ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsRouting)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWirePlacementRoutingId = 0;
                                    if (int.TryParse(field1, out VehicleWirePlacementRoutingId))
                                    {
                                        UpdateRouting(VehicleWirePlacementRoutingId.ToString(), FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddRouting(ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsProgramming)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWireProgrammingId = 0;
                                    if (int.TryParse(field1, out VehicleWireProgrammingId))
                                    {
                                        UpdateProgramming(VehicleWireProgrammingId.ToString(), FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddProgramming(ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsFacebook)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldtNote = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldURL = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleWireFacebookId = 0;
                                    if (int.TryParse(field1, out VehicleWireFacebookId))
                                    {
                                        UpdateFacebook(VehicleWireFacebookId.ToString(), FieldtNote, FieldURL,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddFacebook(ImportVehicleMakeModelYearId, FieldtNote, FieldURL,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsDocument)
                            {
                                if (field1 != "ID" && fields.Length > 2)
                                {
                                    string FieldName = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldFile = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleDocumentId = 0;
                                    if (int.TryParse(field1, out VehicleDocumentId))
                                    {
                                        UpdateDocument(VehicleDocumentId.ToString(), FieldName, FieldFile);
                                    }
                                    else
                                    {
                                        AddDocument(ImportVehicleMakeModelYearId, FieldName, FieldFile);
                                    }
                                }
                            }
                        }
                    }
                }

                ImportPanel.Visible = false;
                ShowInfo("Imported file successfully");
                SearchInfo(0);
            }
            else
            {
                ShowError("Please specify file to import");
                return;
            }
        }

        private string RemoveQuote(string msg)
        {
            if (msg == null) return "";

            string msg2 = msg.Trim();
            if (msg2.StartsWith("\""))
            {
                msg2 = msg2.Substring(1);
            }
            if (msg2.EndsWith("\""))
            {
                msg2 = msg2.Substring(0, msg2.Length - 1);
            }
            return msg2;
        }

        protected void CancelImportFileButton_Click(object sender, EventArgs e)
        {
            ImportPanel.Visible = false;
        }

        private string FindWireFunctionId(string FieldWireFunctionName)
        {
            string WireFunctionId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId from dbo.WireFunction with (nolock) where WireFunctionName=@WireFunctionName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@WireFunctionName", SqlDbType.NVarChar, 100).Value = FieldWireFunctionName;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionId = reader["WireFunctionId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return WireFunctionId;
        }

        private string FindInstalltionTypeId(string FieldInstalltionTypeName)
        {
            string InstallationTypeId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select InstallationTypeId from dbo.InstallationType with (nolock) where InstallationTypeName=@InstallationTypeName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@InstallationTypeName", SqlDbType.NVarChar, 100).Value = FieldInstalltionTypeName;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    InstallationTypeId = reader["InstallationTypeId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return InstallationTypeId;
        }

        private string FindVehicleWireNoteId(string VehicleMakeModelYearId)
        {
            string VehicleWireNoteId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select a.VehicleWireNoteId, a.Note ";
                sql += "from dbo.VehicleWireNote a ";
                sql += "where a.VehicleMakeModelYearId = @VehicleMakeModelYearId";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleWireNoteId = reader["VehicleWireNoteId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return VehicleWireNoteId;
        }

        protected void UnlinkVehicleButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();


                string sql = "delete dbo.VehicleLink ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId and LinkVehicleMakeModelYearId = @LinkVehicleMakeModelYearId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@LinkVehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(LinkVehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                LinkVehicle.Text = "";
                LinkVehicleMakeModelYearIdHidden.Value = "";
                LinkVehicleMakeIdHidden.Value = "";
                LinkVehicleModelIdHidden.Value = "";
                LinkVehicleYearHidden.Value = "";

                ShowInfo("Un-linked vehicle successfully");

                NewVehiclePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());

            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddVehicleTitle.Text == "Link Vehicle")
                ShowYearList();
        }

        private void ShowYearList()
        {
            ClearError();
            if (ModelList.SelectedIndex < 0 || ModelList.SelectedValue == "-1")
            {
                YearList.Items.Clear();
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + MakeList.SelectedValue;
                sql += " and a.VehicleModelId = " + ModelList.SelectedValue;
                if (VehicleMakeModelYearIdHidden.Value != "")
                    sql += " and a.VehicleMakeModelYearId <> " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                YearList.DataTextField = "VehicleYear";
                YearList.DataValueField = "VehicleYear";
                YearList.DataSource = ds;
                YearList.DataBind();

                YearList.Items.Insert(0, new ListItem("Year", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowWireList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetWireQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            WireList.DataSource = ds;
            WireList.DataBind();

            if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
            {
                WireList.Attributes["data-page"] = "false";

                WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                WireList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                WireList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                //WiringTitle.Visible = true;
                WiringHeader.Text = "Vehicle Wiring (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                WiringHeader.Text = "Vehicle Wiring (0)";
                //ShowError("No wiring data available");
                //WiringTitle.Visible = false;
            }
        }

        protected void SearchInstallationTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string SaveFolder = SaveFolderHidden.Value;
                ShowWireList(Con, SaveFolder);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddProgrammingPanelButton_Click(object sender, EventArgs e)
        {
            AddProgrammingTitle.Text = "Add Programming";
            AddProgrammingButton.Text = "Add";
            VehicleWireProgrammingIdHidden.Value = "";
            AddProgrammingPanel.Visible = true;

            ProgrammingStep.Text = "";
            Programmingeditor.Text = "";

            AttachProgrammingImageDelete1.Checked = false;
            AttachProgrammingImageDelete2.Checked = false;
            AttachProgrammingImageDelete3.Checked = false;
            AttachProgrammingImageDelete4.Checked = false;
            AttachProgrammingImageDelete5.Checked = false;

            ExistingProgrammingFilename1.Value = "";
            AttachProgrammingImage1.Visible = false;
            AttachProgrammingImageDelete1.Visible = false;
            ReplaceProgrammingImageLabel1.Visible = false;
            AttachProgrammingFileUrl1.Text = "";

            ExistingProgrammingFilename2.Value = "";
            AttachProgrammingImage2.Visible = false;
            AttachProgrammingImageDelete2.Visible = false;
            ReplaceProgrammingImageLabel2.Visible = false;
            AttachProgrammingFileUrl2.Text = "";

            ExistingProgrammingFilename3.Value = "";
            AttachProgrammingImage3.Visible = false;
            AttachProgrammingImageDelete3.Visible = false;
            ReplaceProgrammingImageLabel3.Visible = false;
            AttachProgrammingFileUrl3.Text = "";

            ExistingProgrammingFilename4.Value = "";
            AttachProgrammingImage4.Visible = false;
            AttachProgrammingImageDelete4.Visible = false;
            ReplaceProgrammingImageLabel4.Visible = false;
            AttachProgrammingFileUrl4.Text = "";

            ExistingProgrammingFilename5.Value = "";
            AttachProgrammingImage5.Visible = false;
            AttachProgrammingImageDelete5.Visible = false;
            ReplaceProgrammingImageLabel5.Visible = false;
            AttachProgrammingFileUrl5.Text = "";

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");
        }

        protected void CancelProgrammingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddProgrammingPanel.Visible = false;

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");
        }

        protected void ProgrammingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Programming?');";
                    }
                }
            }
        }

        protected void AddProgrammingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (ProgrammingStep.Text == "")
            {
                ShowError("Please enter Step");
                ProgrammingStep.Focus();
                return;
            }
            if (Programmingeditor.Text == "")
            {
                ShowError("Please enter Note");
                Programmingeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachProgrammingFile1.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachProgrammingFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachProgrammingFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachProgrammingFile2.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachProgrammingFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachProgrammingFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachProgrammingFile3.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachProgrammingFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachProgrammingFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachProgrammingFile4.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachProgrammingFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachProgrammingFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachProgrammingFile5.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachProgrammingFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachProgrammingFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (VehicleWireProgrammingIdHidden.Value == "")
                {
                    AddProgramming(VehicleMakeModelYearIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else
                {
                    if ((AttachProgrammingImageDelete1.Checked && ExistingProgrammingFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingProgrammingFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingProgrammingFilename1.Value);
                        }
                    }
                    if ((AttachProgrammingImageDelete2.Checked && ExistingProgrammingFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingProgrammingFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingProgrammingFilename2.Value);
                        }
                    }
                    if ((AttachProgrammingImageDelete3.Checked && ExistingProgrammingFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingProgrammingFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingProgrammingFilename3.Value);
                        }
                    }
                    if ((AttachProgrammingImageDelete4.Checked && ExistingProgrammingFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingProgrammingFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingProgrammingFilename4.Value);
                        }
                    }
                    if ((AttachProgrammingImageDelete5.Checked && ExistingProgrammingFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingProgrammingFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingProgrammingFilename5.Value);
                        }
                    }

                    string Attach1 = "";
                    if (AttachProgrammingImageDelete1.Visible && AttachProgrammingImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingProgrammingFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachProgrammingImageDelete2.Visible && AttachProgrammingImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingProgrammingFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachProgrammingImageDelete3.Visible && AttachProgrammingImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingProgrammingFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachProgrammingImageDelete4.Visible && AttachProgrammingImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingProgrammingFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachProgrammingImageDelete5.Visible && AttachProgrammingImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingProgrammingFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    UpdateProgramming(VehicleWireProgrammingIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }

                SearchInfo(7);
                AddProgrammingPanel.Visible = false;

                ResetTab();
                linkprogrammingtab.Attributes.Add("Class", "tab-title active");
                linkprogramming.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void AddProgramming(string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleWireProgramming (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, ";
                sql += "UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, ";
                sql += "getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateProgramming(string VehicleWireProgrammingId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleWireProgramming ";
                sql += "set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                sql += "where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProgrammingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireProgrammingIdHidden.Value = ((HiddenField)(ProgrammingList.SelectedRow.FindControl("VehicleWireProgrammingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireProgrammingId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireProgramming  ";
                sql += "where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ProgrammingStep.Text = reader["Step"].ToString();
                    Programmingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage1.ImageUrl = attach;
                            AttachProgrammingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage1.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl1.Text = "";
                        }
                        ExistingProgrammingFilename1.Value = attach;
                        AttachProgrammingImage1.Visible = true;

                        AttachProgrammingImageDelete1.Visible = true;
                        ReplaceProgrammingImageLabel1.Visible = true;

                        AttachProgrammingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename1.Value = "";
                        AttachProgrammingFileUrl1.Text = "";
                        AttachProgrammingImage1.Visible = false;
                        AttachProgrammingImageDelete1.Visible = false;
                        ReplaceProgrammingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage2.ImageUrl = attach;
                            AttachProgrammingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage2.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl2.Text = "";
                        }
                        ExistingProgrammingFilename2.Value = attach;
                        AttachProgrammingImage2.Visible = true;

                        AttachProgrammingImageDelete2.Visible = true;
                        ReplaceProgrammingImageLabel2.Visible = true;

                        AttachProgrammingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename2.Value = "";
                        AttachProgrammingFileUrl2.Text = "";
                        AttachProgrammingImage2.Visible = false;
                        AttachProgrammingImageDelete2.Visible = false;
                        ReplaceProgrammingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage3.ImageUrl = attach;
                            AttachProgrammingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage3.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl3.Text = "";
                        }
                        ExistingProgrammingFilename3.Value = attach;
                        AttachProgrammingImage3.Visible = true;

                        AttachProgrammingImageDelete3.Visible = true;
                        ReplaceProgrammingImageLabel3.Visible = true;

                        AttachProgrammingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename3.Value = "";
                        AttachProgrammingFileUrl3.Text = "";
                        AttachProgrammingImage3.Visible = false;
                        AttachProgrammingImageDelete3.Visible = false;
                        ReplaceProgrammingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage4.ImageUrl = attach;
                            AttachProgrammingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage4.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl4.Text = "";
                        }
                        ExistingProgrammingFilename4.Value = attach;
                        AttachProgrammingImage4.Visible = true;

                        AttachProgrammingImageDelete4.Visible = true;
                        ReplaceProgrammingImageLabel4.Visible = true;

                        AttachProgrammingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename4.Value = "";
                        AttachProgrammingFileUrl4.Text = "";
                        AttachProgrammingImage4.Visible = false;
                        AttachProgrammingImageDelete4.Visible = false;
                        ReplaceProgrammingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage5.ImageUrl = attach;
                            AttachProgrammingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage5.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl5.Text = "";
                        }
                        ExistingProgrammingFilename5.Value = attach;
                        AttachProgrammingImage5.Visible = true;

                        AttachProgrammingImageDelete5.Visible = true;
                        ReplaceProgrammingImageLabel5.Visible = true;

                        AttachProgrammingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename5.Value = "";
                        AttachProgrammingFileUrl5.Text = "";
                        AttachProgrammingImage5.Visible = false;
                        AttachProgrammingImageDelete5.Visible = false;
                        ReplaceProgrammingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddProgrammingTitle.Text = "Edit Programming";
                AddProgrammingButton.Text = "Save";
                AddProgrammingPanel.Visible = true;

                ResetTab();
                linkprogrammingtab.Attributes.Add("Class", "tab-title active");
                linkprogramming.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProgrammingList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireProgrammingIdValue = ((HiddenField)(ProgrammingList.Rows[e.RowIndex].FindControl("VehicleWireProgrammingId"))).Value;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "select a.VehicleWireProgrammingId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireProgramming a ";
                sql += "where VehicleWireProgrammingId=" + VehicleWireProgrammingIdValue;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach1"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach1"].ToString());
                        }
                    }
                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach2"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach2"].ToString());
                        }
                    }
                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach3"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach3"].ToString());
                        }
                    }
                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach4"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach4"].ToString());
                        }
                    }
                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        if (File.Exists(FullPath + reader["Attach5"].ToString()))
                        {
                            File.Delete(FullPath + reader["Attach5"].ToString());
                        }
                    }
                }
                reader.Close();

                sql = "delete from dbo.VehicleWireProgramming where VehicleWireProgrammingId=" + VehicleWireProgrammingIdValue;
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                AddRoutingPanel.Visible = false;
                SearchInfo(7);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void NoteList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireNoteIdHidden.Value = ((HiddenField)(NoteList.SelectedRow.FindControl("VehicleWireNoteId"))).Value;
            

            SqlConnection conn = null;
            try
            {
                if (VehicleWireNoteIdHidden.Value != "")
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                    string sql = "SELECT Note  ";
                    sql += "from dbo.VehicleWireNote a ";
                    sql += "where VehicleWireNoteId = @VehicleWireNoteId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteIdHidden.Value);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        editor.Text = reader["Note"].ToString();
                    }
                    else
                    {
                        editor.Text = "";
                    }
                    reader.Close();
                }
                else
                {
                    editor.Text = "";
                }

                NoteInstallationTypeIdHidden.Value = ((HiddenField)(NoteList.SelectedRow.FindControl("InstallationTypeId"))).Value;
                InstallationTypeEditLabl.Text = ((Label)(NoteList.SelectedRow.FindControl("InstallationTypeNameLabel"))).Text;
                EditNotePanel.Visible = true;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelNoteButton_Click(object sender, EventArgs e)
        {
            EditNotePanel.Visible = false;
        }

        protected void SaveNoteButton_Click(object sender, EventArgs e)
        {
            UpdateWireNote(VehicleMakeModelYearIdHidden.Value, VehicleWireNoteIdHidden.Value, NoteInstallationTypeIdHidden.Value, editor.Text);

            EditNotePanel.Visible = false;
        }

        private void UpdateWireNote(string VehicleMakeModelYearId, string VehicleWireNoteId, string InstallationTypeId, string Note)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (VehicleWireNoteId != "")
                {
                    string sql = "update dbo.VehicleWireNote set Note = @Note, UpdatedDt = getdate(), UpdatedBy = @UpdatedBy where VehicleWireNoteId=@VehicleWireNoteId ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    Cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteId);

                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    string sql = "insert into dbo.VehicleWireNote (VehicleMakeModelYearId, InstallationTypeId, Note, UpdatedBy, UpdatedDt) ";
                    sql += "select @VehicleMakeModelYearId, @InstallationTypeId,  @Note, @UpdatedBy, getdate()";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                    Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);
                    Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    Cmd.ExecuteNonQuery();
                }

                SearchInfo(1);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }
    }
}