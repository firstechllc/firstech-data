﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminWiringAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MakeId.Value = Request["Make"];
                ModelId.Value = Request["Model"];
                YearId.Value = Request["Year"];
                Id.Value = Request["ID"];

                if (MakeId.Value == "" || ModelId.Value == "" || YearId.Value == "")
                {
                    return;
                }
                else
                {
                    LoadWireInstall();

                    if (Id.Value != "")
                    {
                        LoadVehicleWireFunction(Id.Value);
                        TitleLabel.Text = "Edit Wiring";
                    }
                    else
                    {
                        TitleLabel.Text = "Add Wiring";
                    }
                }
            }
        }

        private void LoadWireInstall()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId, WireFunctionName from dbo.WireFunction with (nolock) where Inactive=0  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                WireFunctionList.DataTextField = "WireFunctionName";
                WireFunctionList.DataValueField = "WireFunctionId";
                WireFunctionList.DataSource = ds2;
                WireFunctionList.DataBind();

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();

                sql = "select VehicleMakeName from dbo.VehicleMake with (nolock) where VehicleMakeId=" + MakeId.Value;

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    MakeLabel.Text = reader["VehicleMakeName"].ToString();
                }
                reader.Close();

                sql = "select VehicleModelName from dbo.VehicleModel with (nolock) where VehicleModelId=" + MakeId.Value;

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    ModelLabel.Text = reader["VehicleModelName"].ToString();
                }
                reader.Close();

                YearLabel.Text = YearId.Value;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void LoadVehicleWireFunction(string VehicleWireFunctionId)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "SELECT WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, ";
                sql += "UpdatedDt, UpdatedBy, Inactive ";
                sql += "from dbo.VehicleWireFunction  ";
                sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionId);

                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    WireFunctionList.SelectedValue = reader["WireFunctionId"].ToString();
                    InstalltionTypeList.SelectedValue = reader["InstallationTypeId"].ToString();

                    VehicleColor.Text = reader["VehicleColor"].ToString();
                    Color.Text = reader["Colour"].ToString();
                    PinOut.Text = reader["PinOut"].ToString();
                    Location.Text = reader["Location"].ToString();
                    Polarity.Text = reader["Polarity"].ToString();
                }
                reader.Close();

                AddButton.Text = "Update";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (Id.Value == "")
                {
                    string sql = "insert into dbo.VehicleWireFunction (VehicleMakeModelYearId, ";
                    sql += "WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, ";
                    sql += "UpdatedDt, UpdatedBy, Inactive) ";
                    sql += "select (select VehicleMakeModelYearId from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear), ";
                    sql += "@WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, ";
                    sql += "getdate(), @UpdatedBy, 0";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeId.Value);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelId.Value);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(YearId.Value);

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionList.SelectedValue);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor.Text.Trim();
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Color.Text.Trim();
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut.Text.Trim();
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location.Text.Trim();
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity.Text.Trim();

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    string sql = "update dbo.VehicleWireFunction ";
                    sql += "set WireFunctionId = @WireFunctionId, InstallationTypeId=@InstallationTypeId, VehicleColor=@VehicleColor, ";
                    sql += "Colour=@Colour, PinOut=@PinOut, Location=@Location, Polarity=@Polarity, ";
                    sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";
                    sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionList.SelectedValue);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor.Text.Trim();
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Color.Text.Trim();
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut.Text.Trim();
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location.Text.Trim();
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity.Text.Trim();

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(Id.Value);

                    cmd.ExecuteNonQuery();
                }

                Response.Redirect("/Admin/AdminWiring?Make=" + MakeId.Value + "&Model=" + ModelId.Value + "&Year=" + YearId.Value);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Admin/AdminWiring?Make=" + MakeId.Value + "&Model=" + ModelId.Value + "&Year=" + YearId.Value);
        }
    }
}