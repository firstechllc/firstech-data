﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="SearchCounter.aspx.cs" Inherits="FirstechData.Admin.SearchCounter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Search Counter</b></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-sm-1" style="white-space: nowrap; ">
                Start Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="StartDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                End Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="EndDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchButton_Click"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label><asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>

    <div class="row">
        <asp:GridView ID="CounterList" runat="server" AutoGenerateColumns="false" CssClass="demo" >
            <Columns>
                <asp:BoundField DataField="Num" HeaderText="No" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="Counter" HeaderText="Counter" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
