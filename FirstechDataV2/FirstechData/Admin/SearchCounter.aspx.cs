﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class SearchCounter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StartDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today.AddDays(-7));
                EndDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today);
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                SqlCommand Cmd = new SqlCommand("proc_SearchCounterSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@StartDate", SqlDbType.NVarChar, 2000).Value = DateTime.Parse(StartDate.Value);
                Cmd.Parameters.Add("@EndDate", SqlDbType.NVarChar, 2000).Value = DateTime.Parse(EndDate.Value);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CounterList.DataSource = ds;
                CounterList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }
    }
}