﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="FirstechData.Contact" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="entypo-phone"></i>
                <span style="color: black; font-size:medium"><b>CONTACT</b></span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <address>
                <b style="color:black">Phone</b><br />
                888-820-3690<br />
                Monday – Friday <br />
                8am – 5pm PST
            </address>

            <address>
                <b style="color:black">Web</b><br />
                    <a href="https://www.compustar.com" target="_blank">https://www.compustar.com</a><br/>
                    <a href="http://www.arcticstart.com" target="_blank">http://www.arcticstart.com</a><br/>
                    <a href="https://www.dronemobile.com" target="_blank">https://www.dronemobile.com</a><br/>
                    <a href="http://www.oemremotestart.com" target="_blank">http://www.oemremotestart.com</a><br/>
            </address>

            <address>
                <b style="color:black">Email</b><br />
                    <a href="mailto:cstocklin@compustar.com" target="_blank">cstocklin@compustar.com</a><br/>
            </address>
        </div>
    </div>
</asp:Content>
