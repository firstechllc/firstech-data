﻿<%@ Page Title="Firstech Data" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstechData._Default" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .carousel-inner > .item > a > img{
           width:100%;
        }
    </style>
    <div class="row">
        <div class="large-12 columns" style="max-height:704px; max-width:1280px;">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <asp:Repeater runat="server" ID="CarouselIndicatorList" OnItemDataBound="CarouselList_ItemDataBound">
                        <ItemTemplate>
                            <li data-target="#carousel-example-generic" data-slide-to="<%# Container.ItemIndex %>" <%# (Container.ItemIndex == 0 ? "class=\"active\"" : "") %>></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <asp:Repeater runat="server" ID="CarouselList" OnItemDataBound="CarouselList_ItemDataBound">
                        <ItemTemplate>
                            <div class="item <%# (Container.ItemIndex == 0 ? "active" : "") %>" style="max-height:704px; max-width:1280px;">
                                <asp:HyperLink runat="server" ID="MainBannerLink"><asp:Image runat="server" ID="MainBannerImage" Visible="false" CssClass="CarouselCss" /></asp:HyperLink>
                                <div class="carousel-caption">
                                    <asp:Label runat="server" ID="MainBannerCaption"></asp:Label>
                                    <asp:HiddenField runat="server" ID="BannerCaption" Value='<%# DataBinder.Eval(Container.DataItem, "BannerCaption") %>' />
                                    <asp:HiddenField runat="server" ID="BannerURL" Value='<%# DataBinder.Eval(Container.DataItem, "BannerURL") %>' />
                                    <asp:HiddenField runat="server" ID="BannerImage" Value='<%# DataBinder.Eval(Container.DataItem, "BannerImage") %>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="large-4 columns">
            <a href="/Events.aspx"><img src="Content/Images/events.jpg" /></a>
        </div>
        <div class="large-4 columns">
            <a href="/Newsfeed.aspx"><img src="Content/Images/newsfeed.jpg" /></a>
        </div>
        <div class="large-4 columns">
            <a href="https://www.facebook.com/groups/compustaridatalinksupportgroup/" target="_blank"><img src="Content/Images/techfeed.jpg" /></a>
        </div>
        <!-- /.box -->
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorLabel" />
        </p>
    </div>
    <asp:HiddenField runat="server" ID="SaveFolderHidden" />
</asp:Content>
