﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="FirstechData.Events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href='/Scripts/js/fullcalendar-2.4.0/fullcalendar.css' rel='stylesheet' />
    <link href='/Scripts/js/fullcalendar-2.4.0/fullcalendar.print.css' rel='stylesheet' media='print' />
    <br />
    <div class="row" style="margin-top: -20px">
        <div class="large-12 columns">
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                    </div>
                    <h3 class="box-title"><i class="icon-calendar"></i>
                        <span style="color: black; font-size:medium"><b>EVENTS</b></span>
                    </h3>
                </div>
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-12 columns">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end .timeline -->
            </div>
            <!-- box -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
    <script src='/Scripts/js/fullcalendar-2.4.0/lib/moment.min.js'></script>
    <script src='/Scripts/js/fullcalendar-2.4.0/fullcalendar.min.js'></script>
    <script src="/Scripts/js/fullcalendar-2.4.0/gcal.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#calendar').fullCalendar({
                googleCalendarApiKey: 'AIzaSyDPexxBjtvx6zY0CguYpdtEThHY-ScGiS8',
                events: {
                    googleCalendarId: 'firstechdata@gmail.com'
                },

                eventClick: function (event) {
                    window.open(event.url, 'gcalevent', 'width=700,height=600');
                    return false;
                },
            });
        });

    </script>
</asp:Content>
