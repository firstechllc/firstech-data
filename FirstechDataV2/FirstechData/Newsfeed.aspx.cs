﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class Newsfeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                FullPathHidden.Value = HttpContext.Current.Server.MapPath(SaveFolderHidden.Value);
                ShowNewsFeed();
            }
        }

        private void ShowNewsFeed()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select NewsfeedId, NewsfeedTitle, NewsfeedUrl, NewsfeedContent, NewsfeedDt, NewsfeedImage ";
                sql += "from dbo.Newsfeed ";
                sql += "order by NewsfeedDt desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                NewsFeedList.DataSource = ds;
                NewsFeedList.DataBind();
            }
            catch (Exception ex)
            {
                //ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void NewsFeedListPager_PreRender(object sender, EventArgs e)
        {
            ShowNewsFeed();
        }

        protected void NewsFeedList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField NFUrl = e.Item.FindControl("NFUrl") as HiddenField;
                HiddenField NFTitle = e.Item.FindControl("NFTitle") as HiddenField;
                HiddenField NFImage = e.Item.FindControl("NFImage") as HiddenField;
                HyperLink NFLink = e.Item.FindControl("NFLink") as HyperLink;
                Label NFLabel = e.Item.FindControl("NFLabel") as Label;
                Image ContentImage = e.Item.FindControl("ContentImage") as Image;

                if (NFLink != null)
                {
                    if (NFUrl.Value.Trim() != "")
                    {
                        NFLink.NavigateUrl = NFUrl.Value;
                        NFLink.Text = NFTitle.Value;

                        NFLabel.Visible = false;
                        NFLink.Visible = true;
                    }
                    else
                    {
                        NFLabel.Text = NFTitle.Value;

                        NFLabel.Visible = true;
                        NFLink.Visible = false;
                    }
                }

                if (NFImage.Value != "")
                {
                    ContentImage.ImageUrl = SaveFolderHidden.Value + NFImage.Value;
                }
            }
        }

    }
}