﻿<%@ Page Title="Search By Vehicle" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="FirstechData.Search" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-search"></i>
                <span style="color: black; font-size: medium"><b>SEARCH BY VEHICLE</b></span><asp:HiddenField runat="server" ID="SaveFolderHidden" />
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <div class="row">
                <div class="form-group form-horizontal">
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                        <asp:HiddenField runat="server" ID="VehicleMakeModelYearIdHidden" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row bg-white" runat="server" id="TitlePanel" visible="false">
        <div class="large-12 columns" style="padding-top: 5px;">
            <h4>
                <asp:Literal runat="server" ID="CurrentVehicle"></asp:Literal></h4>
        </div>
    </div>
    <div class="row bg-white" runat="server" id="UrgentMessagePanel" visible="false">
        <div class="large-12 columns" style="padding-top: 5px; padding-right:30px;">
            <table border="0" style="border-width:0px; width:100%; margin-bottom:0px;">
                <tr>
                    <td style="width:150px; text-align:left; padding-left:0px;"><h5>Urgent Message:</h5></td>
                    <td style="background-color:orange; color:white;"><asp:Literal runat="server" ID="UrgentMessageLabel"></asp:Literal></td>
                </tr>
            </table>
        </div>
    </div>
    <asp:Panel runat="server" ID="NodeWithSameSystemPanelMobile" ScrollBars="Auto" Width="100%" Height="70px">
    <ul class="tabs row" data-tab style="width: 980px; white-space: nowrap;">
        <li class="tab-title active" runat="server" id="WiringPaneltab">
            <a href="#<%=WiringPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="WiringHeader" Text="Wiring"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linksdisassemblytab">
            <a href="#<%=linksdisassembly.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DisassemblyHeader" Text="Disassembly"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkspreptab">
            <a href="#<%=linksprep.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="PrepHeader" Text="Prep"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkroutingtab">
            <a href="#<%=linkrouting.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="RoutingHeader" Text="Routing/Placement"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkprogrammingtab">
            <a href="#<%=linkprogramming.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="ProgrammingHeader" Text="Programming"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="FBPaneltab">
            <a href="#<%=FBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="FBResultHeader" Text="Facebook Result"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="DocumentPaneltab">
            <a href="#<%=DocumentPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DocumentHeader" Text="Documents"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="TSBPaneltab">
            <a href="#<%=TSBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="TSBHeader" Text="TSB"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="Videostab">
            <a href="#<%=VideosPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="VideosHeader" Text="Videos"></asp:Literal></a>
        </li>
    </ul>
    </asp:Panel>
    <div class="tabs-content edumix-tab-horz">
        <div class="content active" runat="server" id="WiringPanel">
            <div class="row">
                <div class="large-10 columns">
                    
                </div>
                <div class="large-2 columns right">
                    <span class="entypo-print"></span>
                    <asp:HyperLink runat="server" ID="PrintVehicleWiringButton" Target="_blank" Text="Print Wiring"></asp:HyperLink>
                </div>
            </div>
            <div class="row">
                <div class="large-3 middle-6 small-12 columns">
                    <asp:DropDownList runat="server" ID="InstalltionTypeList" Visible="false" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="InstalltionTypeList_SelectedIndexChanged" />
                    <asp:HiddenField runat="server" ID="DefaultInstallationTypeId" /><asp:HiddenField runat="server" ID="DefaultInstallationTypeId2" />
                </div>
            </div>
            <div class="row">
                <asp:Label runat="server" ID="NoteLabel"></asp:Label>
            </div>
            <div runat="server" id="linkwire">
                <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="WireList_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="WireFunctionName" HeaderText="Wire Function" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="InstallationTypeName" HeaderText="Installation Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="Polarity" HeaderText="Polarity" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="table-header" />
                </asp:GridView>
            </div>
            <div class="row" runat="server" id="CommentPanel" visible="false">
                <div class="large-1 columns">
                    Comment:
                </div>
                <div class="large-10 columns">
                    <asp:TextBox runat="server" ID="CommentTxt" Width="100%" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    <asp:Label runat="server" ID="CommentInfoLabel" Visible="false"></asp:Label>
                </div>
                <div class="large-1 columns">
                    <asp:Button runat="server" ID="SaveCommentButton" CssClass="button tiny bg-black radius no-margin" Text="Save" OnClick="SaveCommentButton_Click"></asp:Button>
                </div>
            </div>
        </div>
        <div class="content" runat="server" id="linksdisassembly">
            <div runat="server" id="disassemblypanel">
                <asp:GridView ID="DisassemblyList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="DisassemblyList_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                        <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="content" runat="server" id="linksprep">
            <div runat="server" id="preppanel">
                <asp:GridView ID="PrepList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="PrepList_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                        <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="content" runat="server" id="linkrouting">
            <div runat="server" id="routingpanel">
                <asp:GridView ID="RoutingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="RoutingList_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                        <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="content" runat="server" id="linkprogramming">
            <div runat="server" id="programmingpanel">
                <asp:GridView ID="ProgrammingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="ProgrammingList_RowDataBound" >
                    <Columns>
                        <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                        <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="content" runat="server" id="FBPanel">
            <div runat="server" id="facebookpanel">
                <asp:GridView ID="FBList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="FBList_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Facebook" HeaderStyle-Wrap="false" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Note") %>
                                <br />
                                <asp:HyperLink runat="server" ID="Link" NavigateUrl='<%# Bind("URL") %>' Target="_blank">[Link]</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' Width="30" Height="30"></asp:HyperLink>
                                <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' Width="30" Height="30"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="content" runat="server" id="DocumentPanel">
            <asp:DataList runat="server" ID="DocumentList" BorderWidth="0px" RepeatDirection="Vertical" RepeatColumns="1" OnItemDataBound="DocumentList_ItemDataBound">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:HiddenField ID="VehicleDocumentId" runat="server" Value='<%# Bind("VehicleDocumentId") %>' />
                    <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                    <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                    <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                </ItemTemplate>
                <ItemStyle BackColor="White" />
                <AlternatingItemStyle BackColor="White" />
            </asp:DataList>
        </div>
        <div class="content" runat="server" id="TSBPanel">
            <asp:GridView ID="TSBList" runat="server" AutoGenerateColumns="false" CssClass="demo" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ReportReceivedDate" HeaderText="Date" ItemStyle-Width="100px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="NHTSACampaignNumber" HeaderText="Campaign #" ItemStyle-Width="120px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HeaderText="Recall">
                        <ItemTemplate>
                            <b>Component</b>: <%# DataBinder.Eval(Container.DataItem, "Component") %><br />
                            <b>Summary</b>: <%# DataBinder.Eval(Container.DataItem, "Summary") %><br />
                            <b>Conequence</b>: <%# DataBinder.Eval(Container.DataItem, "Conequence") %><br />
                            <b>Remedy</b>: <%# DataBinder.Eval(Container.DataItem, "Remedy") %><br />
                            <b>Notes</b>: <%# DataBinder.Eval(Container.DataItem, "Notes") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="VideosPanel">
            <asp:GridView ID="VideosList" runat="server" AutoGenerateColumns="false" CssClass="demo" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ReportReceivedDate" HeaderText="Date" ItemStyle-Width="100px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="NHTSACampaignNumber" HeaderText="Campaign #" ItemStyle-Width="120px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HeaderText="Recall">
                        <ItemTemplate>
                            <b>Component</b>: <%# DataBinder.Eval(Container.DataItem, "Component") %><br />
                            <b>Summary</b>: <%# DataBinder.Eval(Container.DataItem, "Summary") %><br />
                            <b>Conequence</b>: <%# DataBinder.Eval(Container.DataItem, "Conequence") %><br />
                            <b>Remedy</b>: <%# DataBinder.Eval(Container.DataItem, "Remedy") %><br />
                            <b>Notes</b>: <%# DataBinder.Eval(Container.DataItem, "Notes") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <script>
        var linkwire_var = document.getElementById('<%=linkwire.ClientID%>');
        if (linkwire_var != null) {
            linkwire_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linkwire = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(linkwire, options);
                }
            };
        }

        var programmingpanel_var = document.getElementById('<%=programmingpanel.ClientID%>');
        if (programmingpanel_var != null) {
            programmingpanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    programmingpanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(programmingpanel, options);
                }
            };
        }

        var preppanel_var = document.getElementById('<%=preppanel.ClientID%>');
        if (preppanel_var != null) {
            preppanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    preppanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(preppanel, options);
                }
            };
        }

        var disassemblypanel_var = document.getElementById('<%=disassemblypanel.ClientID%>');
        if (disassemblypanel_var != null) {
            disassemblypanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    disassemblypanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(disassemblypanel, options);
                }
            };
        }

        var routingpanel_var = document.getElementById('<%=routingpanel.ClientID%>');
        if (routingpanel_var != null) {
            routingpanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    routingpanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(routingpanel, options);
                }
            };
        }

        var facebookpanel_var = document.getElementById('<%=facebookpanel.ClientID%>');
        if (facebookpanel_var != null) {
            facebookpanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    facebookpanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined" && link.id.indexOf("ImageLink") >= 0) {
                    blueimp.Gallery(facebookpanel, options);
                }
            };
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $('[id*=WireList]').footable();
            $('[id*=PrepList]').footable();
            $('[id*=FBList]').footable();
            $('[id*=DisassemblyList]').footable();
            $('[id*=RoutingList]').footable();
            $('[id*=ProgrammingList]').footable();
        });
    </script>

</asp:Content>
