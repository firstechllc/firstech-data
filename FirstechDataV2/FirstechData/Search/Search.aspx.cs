﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace FirstechData
{
    public class IPAddressRange
    {
        readonly AddressFamily addressFamily;
        readonly byte[] lowerBytes;
        readonly byte[] upperBytes;

        public IPAddressRange(IPAddress lower, IPAddress upper)
        {
            // Assert that lower.AddressFamily == upper.AddressFamily

            this.addressFamily = lower.AddressFamily;
            this.lowerBytes = lower.GetAddressBytes();
            this.upperBytes = upper.GetAddressBytes();
        }

        public bool IsInRange(IPAddress address)
        {
            if (address.AddressFamily != addressFamily)
            {
                return false;
            }

            byte[] addressBytes = address.GetAddressBytes();

            bool lowerBoundary = true, upperBoundary = true;

            for (int i = 0; i < this.lowerBytes.Length && 
                (lowerBoundary || upperBoundary); i++)
            {
                if ((lowerBoundary && addressBytes[i] < lowerBytes[i]) ||
                    (upperBoundary && addressBytes[i] > upperBytes[i]))
                {
                    return false;
                }

                lowerBoundary &= (addressBytes[i] == lowerBytes[i]);
                upperBoundary &= (addressBytes[i] == upperBytes[i]);
            }

            return true;
        }
        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

    }

    public partial class Search : System.Web.UI.Page
    {
        HashSet<string> AllowedIPs = new HashSet<string>()
        {
            "50.149.87.214",
            "198.22.123.14",
            "198.22.123.79",
            "198.22.123.103",
            "198.22.123.104",
            "198.22.123.105",
            "198.22.123.108",
            "198.22.123.109",
            "198.22.122.4",
            "70.60.1.174",
            "199.60.113.30",
        };

        IPAddressRange IPRange1 = new IPAddressRange(IPAddress.Parse("198.22.122.0"), IPAddress.Parse("198.22.122.24"));
        IPAddressRange IPRange2 = new IPAddressRange(IPAddress.Parse("168.94.245.0"), IPAddress.Parse("168.94.245.24"));
        IPAddressRange IPRange3 = new IPAddressRange(IPAddress.Parse("168.94.239.0"), IPAddress.Parse("168.94.239.24"));

        protected void Page_Load(object sender, EventArgs e)
        {
            string ipAddress = IPAddressRange.GetIPAddress();
            IPAddress ipaddr = IPAddress.Parse(ipAddress);

            if (!AllowedIPs.Contains(ipAddress) && !IPRange1.IsInRange(ipaddr) && !IPRange2.IsInRange(ipaddr) && !IPRange3.IsInRange(ipaddr) && !User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FSearch");
            }

            if (!IsPostBack)
            {
                Master.ChangeMenuCss("SearchByVehicleMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                LoadVehicleMake();
            }
        }

        private void LoadVehicleMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct VehicleMakeId, VehicleMakeName "; 
                sql += "from ( ";
                sql += "   select b.VehicleMakeId, b.VehicleMakeName "; 
                sql += "   from dbo.VehicleMake b with (nolock)   ";
                sql += "   join dbo.VehicleMakeModelYear a WITH (NOLOCK) on a.VehicleMakeId = b.VehicleMakeId  ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=a.VehicleMakeModelYearId  ";
                sql += "   union all ";
                sql += "   select b.VehicleMakeId, b.VehicleMakeName ";
                sql += "   from dbo.VehicleMake b with (nolock)   ";
                sql += "   join dbo.VehicleMakeModelYear a WITH (NOLOCK) on a.VehicleMakeId = b.VehicleMakeId  ";
                sql += "   join dbo.VehicleLink l with (NOLOCK) on l.VehicleMakeModelYearId = a.VehicleMakeModelYearId ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=l.LinkVehicleMakeModelYearId  ";
                sql += ") tbl ";
                sql += "order by VehicleMakeName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("Make", "-1"));

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();

                foreach(ListItem item in InstalltionTypeList.Items)
                {
                    if (item.Text.Trim() == "Remote Start w/ Blade AL")
                    {
                        DefaultInstallationTypeId.Value = item.Value;
                        break;
                    }
                }

                foreach (ListItem item in InstalltionTypeList.Items)
                {
                    if (item.Text.Trim() == "Remote Start")
                    {
                        DefaultInstallationTypeId2.Value = item.Value;
                        break;
                    }
                }

                if (DefaultInstallationTypeId.Value != "")
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId.Value;
                }
                else if (DefaultInstallationTypeId2.Value != "")
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId2.Value;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            ShowHideResult(false);
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct VehicleModelId, VehicleModelName  ";
                sql += "from ( ";
                sql += "   select a.VehicleModelId, VehicleModelName  ";
                sql += "   from dbo.VehicleMakeModelYear a WITH (NOLOCK) "; 
                sql += "   join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=a.VehicleMakeModelYearId  ";
                sql += "   where a.VehicleMakeId = @VehicleMakeId ";
                sql += "   union all ";
                sql += "   select a.VehicleModelId, VehicleModelName  ";
                sql += "   from dbo.VehicleMakeModelYear a WITH (NOLOCK)  ";
                sql += "   join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  ";
                sql += "   join dbo.VehicleLink l with (NOLOCK) on l.VehicleMakeModelYearId = a.VehicleMakeModelYearId ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=l.LinkVehicleMakeModelYearId  ";
                sql += "   where a.VehicleMakeId = @VehicleMakeId ";
                sql += ") tbl ";
                sql += "order by VehicleModelName  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("Model", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();
            ShowHideResult(false);
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct VehicleYear ";
                sql += "from ( ";
                sql += "   select a.VehicleYear  ";
                sql += "   from dbo.VehicleMakeModelYear a WITH (NOLOCK)  ";
                sql += "   join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=a.VehicleMakeModelYearId  ";
                sql += "   where a.VehicleMakeId = @VehicleMakeId ";
                sql += "    and a.VehicleModelId = @VehicleModelId ";
                sql += "   union all ";
                sql += "   select a.VehicleYear  ";
                sql += "   from dbo.VehicleMakeModelYear a WITH (NOLOCK)  ";
                sql += "   join dbo.VehicleLink l with (NOLOCK) on l.VehicleMakeModelYearId = a.VehicleMakeModelYearId ";
                sql += "   join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=l.LinkVehicleMakeModelYearId  ";
                sql += "   where a.VehicleMakeId = @VehicleMakeId ";
                sql += "    and a.VehicleModelId = @VehicleModelId ";
                sql += ") tbl ";
                sql += "order by VehicleYear desc ";


                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                Cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("Year", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchInfo();
        }

        protected void SearchInfo()
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                ShowHideResult(false);
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                ShowHideResult(false);
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                ShowHideResult(false);
                return;
            }

            ChangeVehicleTitle();
            TitlePanel.Visible = true;
            CommentInfoLabel.Visible = false;

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            bool isAdmin = false;
            var user = manager.FindByEmail(Context.User.Identity.GetUserName());
            if (user != null)
            {
                var roles = manager.GetRoles(user.Id);
                if (roles.Contains("Administrator"))
                {
                    isAdmin = true;
                }
            }

            //if (User.Identity.IsAuthenticated)
            if (isAdmin)
            {
                CommentPanel.Visible = true;
            }
            else
            {
                CommentPanel.Visible = false;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeModelYearId from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear";
                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMakeModelYearIdHidden.Value = reader["VehicleMakeModelYearId"].ToString();
                }
                else
                {
                    reader.Close();
                    return;
                }
                reader.Close();


                cmd = new SqlCommand("proc_VehicleUrgentMessageSearch", Con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    UrgentMessageLabel.Text = reader["Note"].ToString();
                    if (UrgentMessageLabel.Text != "")
                    {
                        UrgentMessagePanel.Visible = true;
                    }
                    else
                    {
                        UrgentMessagePanel.Visible = false;
                    }
                }
                else
                {
                    UrgentMessageLabel.Text = "";
                    UrgentMessagePanel.Visible = false;
                }
                reader.Close();


                string SaveFolder = SaveFolderHidden.Value;
                WireList.Columns[3].Visible = true;

                SqlCommand Cmd = new SqlCommand("proc_VehicleWireSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(DefaultInstallationTypeId.Value);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireList.DataSource = ds;
                WireList.DataBind();
                WireList.Visible = true;

                if (ds.Tables[0].Rows.Count == 0)
                {
                    Cmd = new SqlCommand("proc_VehicleWireSearch", Con);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                    Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(DefaultInstallationTypeId2.Value);

                    adp = new SqlDataAdapter(Cmd);
                    ds = new DataSet();

                    adp.Fill(ds, "List");

                    WireList.DataSource = ds;
                    WireList.DataBind();
                    WireList.Visible = true;

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        InstalltionTypeList.SelectedValue = DefaultInstallationTypeId2.Value;

                        WireList.Columns[3].Visible = false;
                    }
                }
                else 
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId.Value;
                }

                if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[2].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";

                    WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";

                    WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //WiringTitle.Visible = true;
                    WiringHeader.Text = "Wiring (" + ds.Tables[0].Rows.Count + ")";
                    PrintVehicleWiringButton.NavigateUrl = "PrintVehicleWiring.aspx?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue;

                    WiringPaneltab.Visible = true;
                    WiringPanel.Visible = true;
                    InstalltionTypeList.Visible = true;
                }
                else
                {
                    WiringHeader.Text = "Wiring (0)";

                    InstalltionTypeList.Visible = false;
                    //ShowError("No wiring data available");
                    //WiringTitle.Visible = false;
                }

                cmd = new SqlCommand("proc_VehicleWireNoteSearch", Con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Note"] != null)
                    {
                        NoteLabel.Text = reader["Note"].ToString();
                        NoteLabel.Visible = true;
                    }
                    else
                    {
                        NoteLabel.Text = "";
                    }
                }
                else
                {
                    NoteLabel.Text = "";
                }
                reader.Close();

                if (!(WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0) && NoteLabel.Text == "")
                {
                    WiringPaneltab.Visible = false;
                    WiringPanel.Visible = false;
                }


                Cmd = new SqlCommand("proc_VehiclePrepSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                PrepList.DataSource = ds;
                PrepList.DataBind();
                PrepList.Visible = true;

                if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
                {
                    PrepList.Attributes["data-page"] = "false";

                    /*
                    PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    */

                    PrepList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    PrepList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    PrepList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PrepTitle.Visible = true;

                    PrepHeader.Text = "Prep (" + ds.Tables[0].Rows.Count + ")";

                    linkspreptab.Visible = true;
                    linksprep.Visible = true;
                }
                else
                {
                    //PrepHeader.Text = "Prep (0)";
                    //PrepTitle.Visible = false;

                    linkspreptab.Visible = false;
                    linksprep.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleFacebookSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                FBList.DataSource = ds;
                FBList.DataBind();
                FBList.Visible = true;

                if (FBList != null && FBList.HeaderRow != null && FBList.HeaderRow.Cells.Count > 0)
                {
                    FBList.Attributes["data-page"] = "false";

                    FBList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    FBList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";

                    FBList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    FBList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";

                    FBList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    FBResultHeader.Text = "Facebook Result (" + ds.Tables[0].Rows.Count + ")";

                    FBPaneltab.Visible = true;
                    FBPanel.Visible = true;
                }
                else
                {
                    FBResultHeader.Text = "Facebook Result (0)";

                    FBPaneltab.Visible = false;
                    FBPanel.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleDocumentSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                DocumentList.DataSource = ds;
                DocumentList.DataBind();
                DocumentList.Visible = true;

                if (DocumentList != null && DocumentList.Items != null && DocumentList.Items.Count > 0)
                {
                    DocumentHeader.Text = "Documents (" + ds.Tables[0].Rows.Count + ")";

                    DocumentPanel.Visible = true;
                    DocumentPaneltab.Visible = true;
                }
                else
                {
                    //DocumentHeader.Text = "Prep (0)";
                    DocumentPanel.Visible = false;
                    DocumentPaneltab.Visible = false;
                }
                

                Cmd = new SqlCommand("proc_VehicleDisassemblySearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                DisassemblyList.DataSource = ds;
                DisassemblyList.DataBind();
                DisassemblyList.Visible = true;

                if (DisassemblyList != null && DisassemblyList.HeaderRow != null && DisassemblyList.HeaderRow.Cells.Count > 0)
                {
                    DisassemblyList.Attributes["data-page"] = "false";

                    /*
                    DisassemblyList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    DisassemblyList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    DisassemblyList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    */

                    DisassemblyList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    DisassemblyList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    DisassemblyList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    DisassemblyList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //DisassemblyTitle.Visible = true;

                    DisassemblyHeader.Text = "Disassembly (" + ds.Tables[0].Rows.Count + ")";

                    linksdisassemblytab.Visible = true;
                    linksdisassembly.Visible = true;
                }
                else
                {
                    DisassemblyHeader.Text = "Disassembly (0)";
                    //DisassemblyTitle.Visible = false;

                    linksdisassemblytab.Visible = false;
                    linksdisassembly.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleRoutingSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                RoutingList.DataSource = ds;
                RoutingList.DataBind();
                RoutingList.Visible = true;

                if (RoutingList != null && RoutingList.HeaderRow != null && RoutingList.HeaderRow.Cells.Count > 0)
                {
                    RoutingList.Attributes["data-page"] = "false";

                    RoutingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    RoutingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    RoutingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    RoutingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    RoutingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    RoutingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    RoutingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PlacementTitle.Visible = true;

                    RoutingHeader.Text = "Routing/Placement (" + ds.Tables[0].Rows.Count + ")";

                    linkroutingtab.Visible = true;
                    linkrouting.Visible = true;
                }
                else
                {
                    RoutingHeader.Text = "Routing/Placement (0)";
                    //PlacementTitle.Visible = false;

                    linkroutingtab.Visible = false;
                    linkrouting.Visible = false;
                }

                // Programming
                Cmd = new SqlCommand("proc_VehicleProgrammingSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                ProgrammingList.DataSource = ds;
                ProgrammingList.DataBind();
                ProgrammingList.Visible = true;

                if (ProgrammingList != null && ProgrammingList.HeaderRow != null && ProgrammingList.HeaderRow.Cells.Count > 0)
                {
                    ProgrammingList.Attributes["data-page"] = "false";

                    ProgrammingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    ProgrammingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    ProgrammingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    ProgrammingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    ProgrammingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    ProgrammingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    ProgrammingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PlacementTitle.Visible = true;

                    ProgrammingHeader.Text = "Programming (" + ds.Tables[0].Rows.Count + ")";

                    linkprogrammingtab.Visible = true;
                    linkprogramming.Visible = true;
                }
                else
                {
                    ProgrammingHeader.Text = "Programming (0)";
                    //PlacementTitle.Visible = false;

                    linkprogrammingtab.Visible = false;
                    linkprogramming.Visible = false;
                }

                // TSB
                try
                {
                    string url = "http://www.nhtsa.gov/webapi/api/Recalls/vehicle/modelyear/" + VehicleYearList.SelectedItem.Text + "/make/" + VehicleMakeList.SelectedItem.Text.ToLower() + "/model/" + VehicleModelList.SelectedItem.Text + "?format=xml";

                    WebClient wc = new WebClient();
                    Stream st = wc.OpenRead(url);
                    StreamReader sreader = new StreamReader(st);
                    string recallstr = sreader.ReadToEnd();

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(recallstr);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("ReportReceivedDate");
                    dt.Columns.Add("Component");
                    dt.Columns.Add("NHTSACampaignNumber");
                    dt.Columns.Add("Summary");
                    dt.Columns.Add("Conequence");
                    dt.Columns.Add("Remedy");
                    dt.Columns.Add("Notes");

                    if (xDoc.ChildNodes.Count > 0)
                    {
                        if (xDoc.ChildNodes[0].ChildNodes.Count >= 3)
                        {
                            foreach (XmlNode xNode in xDoc.ChildNodes[0].ChildNodes[2].ChildNodes)
                            {
                                DataRow row = dt.NewRow();
                                foreach (XmlNode xNode2 in xNode.ChildNodes)
                                {
                                    if (xNode2.Name == "ReportReceivedDate")
                                    {
                                        row[xNode2.Name] = xNode2.InnerText.Replace("T00:00:00", "");
                                    }
                                    else if (xNode2.Name == "Component"
                                        || xNode2.Name == "NHTSACampaignNumber"
                                        || xNode2.Name == "Summary"
                                        || xNode2.Name == "Conequence"
                                        || xNode2.Name == "Remedy"
                                        || xNode2.Name == "Notes")
                                    {
                                        row[xNode2.Name] = xNode2.InnerText;
                                    }
                                }
                                dt.Rows.Add(row);
                            }
                        }
                    }

                    TSBList.DataSource = dt;
                    TSBList.DataBind();

                    if (TSBList != null && TSBList.HeaderRow != null && TSBList.HeaderRow.Cells.Count > 0)
                    {
                        TSBList.Attributes["data-page"] = "false";

                        TSBList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        TSBList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        TSBList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        TSBList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        TSBList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        TSBList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        TSBList.HeaderRow.TableSection = TableRowSection.TableHeader;

                        TSBHeader.Text = "TSB (" + dt.Rows.Count + ")";

                        TSBPaneltab.Visible = true;
                        TSBPanel.Visible = true;
                    }
                    else
                    {
                        TSBHeader.Text = "TSB (0)";

                        TSBPaneltab.Visible = false;
                        TSBPanel.Visible = false;
                    }
                }
                catch (Exception) { }


                Videostab.Visible = false;
                VideosPanel.Visible = false;


                ShowHideResult(true);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        
        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        protected void DisassemblyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }
        protected void RoutingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        private void HideNoImageLink(GridViewRowEventArgs e)
        { 
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink a1 = ((HyperLink)(e.Row.FindControl("ImageLink1")));
                HyperLink a2 = ((HyperLink)(e.Row.FindControl("ImageLink2")));
                HyperLink a3 = ((HyperLink)(e.Row.FindControl("ImageLink3")));
                HyperLink a4 = ((HyperLink)(e.Row.FindControl("ImageLink4")));
                HyperLink a5 = ((HyperLink)(e.Row.FindControl("ImageLink5")));

                if (a1.NavigateUrl == "")
                {
                    a1.Visible = false;
                }
                if (a2.NavigateUrl == "")
                {
                    a2.Visible = false;
                }
                if (a3.NavigateUrl == "")
                {
                    a3.Visible = false;
                }
                if (a4.NavigateUrl == "")
                {
                    a4.Visible = false;
                }
                if (a5.NavigateUrl == "")
                {
                    a5.Visible = false;
                }
            }
        }

        protected void FBList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        private void ShowHideResult(bool show)
        {
            WireList.Visible = show;
            NoteLabel.Visible = show;
            DisassemblyList.Visible = show;
            PrepList.Visible = show;
            RoutingList.Visible = show;
            ProgrammingList.Visible = show;

            if (!show)
            {
                WiringHeader.Text = "Wiring";
                DisassemblyHeader.Text = "Disassembly";
                FBResultHeader.Text = "Facebook Result";
                PrepHeader.Text = "Prep";
                RoutingHeader.Text = "Routing/Placement";
                ProgrammingHeader.Text = "Programming";
                DocumentHeader.Text = "Documents";
                VehicleMakeModelYearIdHidden.Value = "";

                TitlePanel.Visible = false;
                UrgentMessagePanel.Visible = false;
                CommentPanel.Visible = false;
            }
        }

        protected void DocumentList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField VehicleDocumentId = e.Item.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Item.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Item.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Item.FindControl("DocumentLabel") as Label;
                    if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
                    {
                        DocumentLabel.Text = "<a href='" + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }
            }
        }

        protected void WireList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        private void ChangeVehicleTitle()
        {
            CurrentVehicle.Text = VehicleMakeList.SelectedItem.Text + " " + VehicleModelList.SelectedItem.Text + " " + VehicleYearList.SelectedItem.Text;
        }

        protected void SaveCommentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (CommentTxt.Text.Trim() == "")
            {
                ShowError("Please enter Comment.");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleComment (VehicleMakeModelYearId, Comment, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Comment, getdate(), @UpdatedBy; ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = CommentTxt.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                CommentTxt.Text = "";
                CommentInfoLabel.Visible = true;
                CommentInfoLabel.Text = "Your comment has been saved. Thanks.";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void InstalltionTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!WiringPanel.Visible)
                return;
            if (InstalltionTypeList.Items.Count <= 0)
                return;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand Cmd = new SqlCommand("proc_VehicleWireSearch", conn);
                Cmd.CommandType = CommandType.StoredProcedure;

                string SaveFolder = SaveFolderHidden.Value;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireList.DataSource = ds;
                WireList.DataBind();
                WireList.Visible = true;

                if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[2].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";

                    WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";

                    WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //WiringTitle.Visible = true;
                    WiringHeader.Text = "Wiring (" + ds.Tables[0].Rows.Count + ")";
                    PrintVehicleWiringButton.NavigateUrl = "PrintVehicleWiring.aspx?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue;

                    WiringPaneltab.Visible = true;
                    WiringPanel.Visible = true;

                    if (InstalltionTypeList.SelectedValue == DefaultInstallationTypeId2.Value)
                    {
                        WireList.Columns[3].Visible = false;
                    }
                    else
                    {
                        WireList.Columns[3].Visible = true;
                    }
                }
                else
                {
                    WiringHeader.Text = "Wiring (0)";
                    //ShowError("No wiring data available");
                    //WiringTitle.Visible = false;
                }

                SqlCommand cmd = new SqlCommand("proc_VehicleWireNoteSearch", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Note"] != null)
                    {
                        NoteLabel.Text = reader["Note"].ToString();
                        NoteLabel.Visible = true;
                    }
                    else
                    {
                        NoteLabel.Text = "";
                    }
                }
                else
                {
                    NoteLabel.Text = "";
                }
                reader.Close();


            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ProgrammingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }
    }
}