﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FirstechData.Startup))]
namespace FirstechData
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
