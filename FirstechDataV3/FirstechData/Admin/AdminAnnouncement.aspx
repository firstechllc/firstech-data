﻿<%@ Page Title="Announcement" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminAnnouncement.aspx.cs" Inherits="FirstechData.Admin.AdminAnnouncement" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Announcement</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i><span>Announcements</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:TextBox runat="server" ID="SearchTxt" Width="200px" CssClass="col-sm-2" placeholder="Announcement to search"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchButton_Click" />
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add Announcement" OnClick="AddButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="AnnouncementList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowSorting="false" AllowPaging="true" PageSize="20"
                                OnPageIndexChanging="AnnouncementList_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="AnnouncementId" ItemStyle-Width="50px" />
                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <a href='AdminAnnouncementAdd.aspx?ID=<%# DataBinder.Eval(Container, "DataItem.AnnouncementId") %>'><%# DataBinder.Eval(Container, "DataItem.AnnouncementTitle") %></b>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Start Date" DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="110px" />
                                    <asp:BoundField HeaderText="End Date" DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="80px" />
                                    <asp:BoundField HeaderText="Active" DataField="Active" ItemStyle-Width="60px" />
                                    <asp:BoundField HeaderText="UPDATE" DataField="UpdatedDt" ItemStyle-Width="180px" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=AnnouncementList]').footable();
        });
    </script>
</asp:Content>
