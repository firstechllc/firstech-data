﻿<%@ Page Title="Disclaimer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminDisclaimer.aspx.cs" Inherits="FirstechData.Admin.AdminDisclaimer" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Disclaimer</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span><asp:Label runat="server" ID="TitleLabel"></asp:Label></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="ErrorLabel" />
                    </p>
                    <div class="form-horizontal">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="editor" CssClass="col-md-2 control-label">Disclaimer</asp:Label>
                            <div class="col-md-10">
                                <CKEditor:CKEditorControl ID="editor" runat="server" CssClass="form-control row" Width="100%" Height="300px"  BasePath="/Scripts/js/ckeditor">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <asp:Button runat="server" ID="SaveButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end .timeline -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
