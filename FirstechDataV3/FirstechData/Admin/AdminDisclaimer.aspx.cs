﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminDisclaimer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                Master.ChangeMenuCss("DisclaimerMenu");
                ShowDisclaimer();
                SaveButton.Text = "Update";
                TitleLabel.Text = "Edit Disclaimer";
            }
        }

        private void ShowDisclaimer()
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select Disclaimer, UpdatedBy, UpdatedDt ";
                sql += "from dbo.Disclaimer ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    editor.Text = reader["Disclaimer"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "if exists(select * from dbo.Disclaimer) begin update dbo.Disclaimer set Disclaimer=@Disclaimer, UpdatedBy=@UpdatedBy, UpdatedDt=getdate() end ";
                sql += "else begin insert into dbo.Disclaimer (Disclaimer, UpdatedBy, UpdatedDt) select @Disclaimer, @UpdatedBy, getdate() end";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Disclaimer", SqlDbType.NText).Value = editor.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminDisclaimer");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

    }
}