﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminEditorApproval.aspx.cs" Inherits="FirstechData.Admin.AdminEditorApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Editor Activity</b></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-sm-1" style="white-space: nowrap; ">
                Start Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="StartDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                End Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="EndDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                Status: 
            </div>
            <div class="col-sm-2">
                <asp:DropDownList runat="server" ID="StatusList">
                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Draft" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Approved" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Denied" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-2">
                <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchButton_Click"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>

    <div class="row">
        <asp:GridView ID="EditorLogList" runat="server" AutoGenerateColumns="False" CssClass="footable" Width="100%" 
            AllowPaging="true" PageSize="50" OnPageIndexChanging="EditorLogList_PageIndexChanging"  >
            <Columns>
                <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" ItemStyle-Font-Size="12px" ItemStyle-Width="150px" />
                <asp:BoundField DataField="StatusName" HeaderText="Status" ItemStyle-Font-Size="12px" ItemStyle-Width="100px" />
                <asp:BoundField DataField="ProfileName" HeaderText="Editor" ItemStyle-Font-Size="12px" ItemStyle-Width="200px" />
                <asp:BoundField DataField="EditorLog" HeaderText="Feed" ReadOnly="true" HtmlEncode="false"/>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
