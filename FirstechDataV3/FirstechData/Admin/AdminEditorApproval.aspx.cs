﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminEditorApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                StartDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today.AddDays(-7));
                EndDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today);
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowList();
        }

        private void ShowList()
        { 
            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                SqlCommand Cmd = new SqlCommand("proc_SearchEditorApproval", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                DateTime dt;
                if (DateTime.TryParse(StartDate.Value.Trim(), out dt))
                    Cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = dt;
                if (DateTime.TryParse(EndDate.Value.Trim(), out dt))
                    Cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = dt.AddDays(1);
                if (StatusList.SelectedValue != "-1")
                    Cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = int.Parse(StatusList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                EditorLogList.DataSource = ds;
                EditorLogList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }
        
        protected void EditorLogList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            EditorLogList.PageIndex = e.NewPageIndex;
            ShowList();
        }
    }
}