﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminHomeBanner.aspx.cs" Inherits="FirstechData.Admin.AdminHomeBanner" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <div class="row">
                <div class="large-6 columns">
                    <h4 class="box-title"><i class="fontello-doc"></i><span>Homepage Banner</span></h4>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
        </div>
        <div class="box-body " style="display: block;">
            <asp:Image runat="server" ID="aa" />
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddButton" Text="(+) Add Banner" OnClick="AddButton_Click"></asp:LinkButton>
                </div>
            </div>
            <asp:Panel runat="server" ID="AddBannerPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddBannerTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="BannerCaption" CssClass="col-md-2 control-label">Banner Caption</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:TextBox runat="server" ID="BannerCaption"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="BannerURL" CssClass="col-md-2 control-label">Banner URL</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:TextBox runat="server" ID="BannerURL"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="AttachFile" CssClass="col-md-2 control-label">Banner Image(Desktop)</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:Label runat="server" ID="ExistingBanner"></asp:Label>
                                        <asp:FileUpload runat="server" ID="AttachFile" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="AttachFile" CssClass="col-md-2 control-label">Banner Image(Mobile)</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:Label runat="server" ID="ExistingBannerMobile"></asp:Label>
                                        <asp:FileUpload runat="server" ID="AttachFileMobile" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="BannerOrder" CssClass="col-md-2 control-label">Order</asp:Label>
                                    <div class="large-6 columns">
                                        <asp:TextBox runat="server" ID="BannerOrder" Text="1"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="ThisBannerIdHidden" />
                                        <asp:Button runat="server" ID="AddBannerButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddBannerButton_Click" />
                                        <asp:Button runat="server" ID="CancelBannerButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelBannerButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="MainPanel" ScrollBars="Horizontal">
                <div class="row">
                    <div class="large-12 columns">
                        <asp:GridView ID="BannerList" runat="server" AutoGenerateColumns="false" CssClass="footable" OnRowDataBound="BannerList_RowDataBound"
                            OnRowDeleting="BannerList_RowDeleting" OnSelectedIndexChanged="BannerList_SelectedIndexChanged" OnRowCommand="BannerList_RowCommand"
                            RowStyle-VerticalAlign="Top">
                            <Columns>
                                <asp:BoundField DataField="BannerId" HeaderText="ID" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" ItemStyle-Width="50px" />
                                <asp:BoundField DataField="BannerCaption" HeaderText="Banner Caption" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                <asp:BoundField DataField="BannerURL" HeaderText="Banner URL" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                <asp:TemplateField HeaderText="Banner Image" HeaderStyle-Font-Size="11px" ItemStyle-Font-Size="11px" >
                                    <ItemTemplate>
                                        <asp:HiddenField ID="BannerIdHidden2" runat="server" Value='<%# Bind("BannerId") %>' />
                                        <asp:HiddenField ID="BannerImageFilename" runat="server" Value='<%# Bind("BannerImage") %>' />
                                        <asp:HiddenField ID="BannerImageMobileFilename" runat="server" Value='<%# Bind("BannerImageMobile") %>' />
                                        <asp:HiddenField ID="SortOrder" runat="server" Value='<%# Bind("SortOrder") %>' />
                                        Desktop:<asp:Image runat="server" ID="BannerImage" />
                                        Mobile:<asp:Image runat="server" ID="BannerImageMobile" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:Label ID="SortOrderLabel" runat="server" BorderWidth="0" Text='<%# Bind("SortOrder") %>'></asp:Label>
                                        <asp:LinkButton runat="server" ID="UpButton" Text="UP" CommandName="UP" CommandArgument='<%# Bind("BannerId") %>'></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="DnButton" Text="DN" CommandName="DN" CommandArgument='<%# Bind("BannerId") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                                    ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                </asp:CommandField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField runat="server" ID="SaveFolderHidden" />
                        <asp:HiddenField runat="server" ID="FullPathHidden" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[id*=BannerList]').footable();
        });
    </script>
</asp:Content>
