﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminHomepage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("HomepageMenu");
                ShowInfo();
            }
        }

        private void ShowInfo()
        {
            SqlConnection Con = null;
            try
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select MainBannerCaption1, MainBannerImage1, MainBannerURL1, MainBannerCaption2, MainBannerImage2, MainBannerURL2, ";
                sql += "MainBannerCaption3, MainBannerImage3, MainBannerURL3, LeftBannerCaption, LeftBannerImage, LeftBannerURL, ";
                sql += "MiddleBannerCaption, MiddleBannerImage, MiddleBannerURL, RightBannerCaption, RightBannerImage, RightBannerURL ";
                sql += "from dbo.Homepage ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataReader reader = Cmd.ExecuteReader();
                if(reader.Read())
                {
                    if (reader["MainBannerCaption1"] != null)
                        MainBannerCaption1.Text = reader["MainBannerCaption1"].ToString();
                    if (reader["MainBannerURL1"] != null)
                        MainBannerURL1.Text = reader["MainBannerURL1"].ToString();
                    if (reader["MainBannerCaption2"] != null)
                        MainBannerCaption2.Text = reader["MainBannerCaption2"].ToString();
                    if (reader["MainBannerURL2"] != null)
                        MainBannerURL2.Text = reader["MainBannerURL2"].ToString();
                    if (reader["MainBannerCaption3"] != null)
                        MainBannerCaption3.Text = reader["MainBannerCaption3"].ToString();
                    if (reader["MainBannerURL3"] != null)
                        MainBannerURL3.Text = reader["MainBannerURL3"].ToString();
                    if (reader["LeftBannerCaption"] != null)
                        LeftBannerCaption.Text = reader["LeftBannerCaption"].ToString();
                    if (reader["LeftBannerURL"] != null)
                        LeftBannerURL.Text = reader["LeftBannerURL"].ToString();
                    if (reader["MiddleBannerCaption"] != null)
                        MiddleBannerCaption.Text = reader["MiddleBannerCaption"].ToString();
                    if (reader["MiddleBannerURL"] != null)
                        MiddleBannerURL.Text = reader["MiddleBannerURL"].ToString();
                    if (reader["RightBannerCaption"] != null)
                        RightBannerCaption.Text = reader["RightBannerCaption"].ToString();
                    if (reader["RightBannerURL"] != null)
                        RightBannerURL.Text = reader["RightBannerURL"].ToString();

                    if (reader["MainBannerImage1"] != null && reader["MainBannerImage1"].ToString() != "")
                    {
                        AttachImage1.ImageUrl = SaveFolder + reader["MainBannerImage1"].ToString();
                        AttachImage1.Visible = true;
                        ReplaceImageLabel1.Visible = true;
                    }
                    if (reader["MainBannerImage2"] != null && reader["MainBannerImage2"].ToString() != "")
                    {
                        AttachImage2.ImageUrl = SaveFolder + reader["MainBannerImage2"].ToString();
                        AttachImage2.Visible = true;
                        ReplaceImageLabel2.Visible = true;
                    }
                    if (reader["MainBannerImage3"] != null && reader["MainBannerImage3"].ToString() != "")
                    {
                        AttachImage3.ImageUrl = SaveFolder + reader["MainBannerImage3"].ToString();
                        AttachImage3.Visible = true;
                        ReplaceImageLabel3.Visible = true;
                    }
                    if (reader["LeftBannerImage"] != null && reader["LeftBannerImage"].ToString() != "")
                    {
                        LeftAttachImage.ImageUrl = SaveFolder + reader["LeftBannerImage"].ToString();
                        LeftAttachImage.Visible = true;
                        ReplaceLeftImageLabel.Visible = true;
                    }
                    if (reader["MiddleBannerImage"] != null && reader["MiddleBannerImage"].ToString() != "")
                    {
                        MiddleAttachImage.ImageUrl = SaveFolder + reader["MiddleBannerImage"].ToString();
                        MiddleAttachImage.Visible = true;
                        ReplaceMiddleImageLabel.Visible = true;
                    }
                    if (reader["RightBannerImage"] != null && reader["RightBannerImage"].ToString() != "")
                    {
                        RightAttachImage.ImageUrl = SaveFolder + reader["RightBannerImage"].ToString();
                        RightAttachImage.Visible = true;
                        ReplaceRightImageLabel.Visible = true;
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, out string FileName)
        {
            FileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                filename = "Banner_" + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            ClearError();
            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string FileName1;
            SaveFile(AttachFile1.PostedFile, FullPath, out FileName1);
            string FileName2;
            SaveFile(AttachFile2.PostedFile, FullPath, out FileName2);
            string FileName3;
            SaveFile(AttachFile3.PostedFile, FullPath, out FileName3);
            string LeftFileName;
            SaveFile(LeftAttachFile.PostedFile, FullPath, out LeftFileName);
            string MiddleFileName;
            SaveFile(MiddleAttachFile.PostedFile, FullPath, out MiddleFileName);
            string RightFileName;
            SaveFile(RightAttachFile.PostedFile, FullPath, out RightFileName);

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();


                string sql = "update dbo.Homepage set ";
                sql += "  MainBannerCaption1 = @MainBannerCaption1, MainBannerURL1 = @MainBannerURL1, ";
                sql += "  MainBannerCaption2 = @MainBannerCaption2, MainBannerURL2 = @MainBannerURL2, ";
                sql += "  MainBannerCaption3 = @MainBannerCaption3, MainBannerURL3 = @MainBannerURL3, ";

                sql += "  LeftBannerCaption = @LeftBannerCaption, LeftBannerURL = @LeftBannerURL, ";
                sql += "  MiddleBannerCaption = @MiddleBannerCaption, MiddleBannerURL = @MiddleBannerURL, ";
                sql += "  RightBannerCaption = @RightBannerCaption, RightBannerURL = @RightBannerURL, ";

                if (FileName1 != "")
                {
                    sql += " MainBannerImage1 = @MainBannerImage1, ";
                }
                if (FileName2 != "")
                {
                    sql += " MainBannerImage2 = @MainBannerImage2, ";
                }
                if (FileName3 != "")
                {
                    sql += " MainBannerImage3 = @MainBannerImage3, ";
                }
                if (LeftFileName != "")
                {
                    sql += " LeftBannerImage = @LeftBannerImage, ";
                }
                if (MiddleFileName != "")
                {
                    sql += " MiddleBannerImage = @MiddleBannerImage, ";
                }
                if (RightFileName != "")
                {
                    sql += " RightBannerImage = @RightBannerImage, ";
                }

                sql += "  UpdatedDt = getdate(), UpdatedBy = @UpdatedBy ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@MainBannerCaption1", SqlDbType.NVarChar, 500).Value = MainBannerCaption1.Text.Trim();
                cmd.Parameters.Add("@MainBannerURL1", SqlDbType.NVarChar, 500).Value = MainBannerURL1.Text.Trim();

                cmd.Parameters.Add("@MainBannerCaption2", SqlDbType.NVarChar, 500).Value = MainBannerCaption2.Text.Trim();
                cmd.Parameters.Add("@MainBannerURL2", SqlDbType.NVarChar, 500).Value = MainBannerURL2.Text.Trim();

                cmd.Parameters.Add("@MainBannerCaption3", SqlDbType.NVarChar, 500).Value = MainBannerCaption3.Text.Trim();
                cmd.Parameters.Add("@MainBannerURL3", SqlDbType.NVarChar, 500).Value = MainBannerURL3.Text.Trim();

                cmd.Parameters.Add("@LeftBannerCaption", SqlDbType.NVarChar, 500).Value = LeftBannerCaption.Text.Trim();
                cmd.Parameters.Add("@LeftBannerURL", SqlDbType.NVarChar, 500).Value = LeftBannerURL.Text.Trim();

                cmd.Parameters.Add("@MiddleBannerCaption", SqlDbType.NVarChar, 500).Value = MiddleBannerCaption.Text.Trim();
                cmd.Parameters.Add("@MiddleBannerURL", SqlDbType.NVarChar, 500).Value = MiddleBannerURL.Text.Trim();

                cmd.Parameters.Add("@RightBannerCaption", SqlDbType.NVarChar, 500).Value = RightBannerCaption.Text.Trim();
                cmd.Parameters.Add("@RightBannerURL", SqlDbType.NVarChar, 500).Value = RightBannerURL.Text.Trim();

                if (FileName1 != "")
                {
                    cmd.Parameters.Add("@MainBannerImage1", SqlDbType.NVarChar, 1000).Value = FileName1;
                }
                if (FileName2 != "")
                {
                    cmd.Parameters.Add("@MainBannerImage2", SqlDbType.NVarChar, 1000).Value = FileName2;
                }
                if (FileName3 != "")
                {
                    cmd.Parameters.Add("@MainBannerImage3", SqlDbType.NVarChar, 1000).Value = FileName3;
                }
                if (LeftFileName != "")
                {
                    cmd.Parameters.Add("@LeftBannerImage", SqlDbType.NVarChar, 1000).Value = LeftFileName;
                }
                if (MiddleFileName != "")
                {
                    cmd.Parameters.Add("@MiddleBannerImage", SqlDbType.NVarChar, 1000).Value = MiddleFileName;
                }
                if (RightFileName != "")
                {
                    cmd.Parameters.Add("@RightBannerImage", SqlDbType.NVarChar, 1000).Value = RightFileName;
                }

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminHomepage");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void DeleteBanner(int idx)
        {
            SqlConnection conn = null;
            try
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select MainBannerImage1, MainBannerImage2, MainBannerImage3 from dbo.Homepage";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    if (idx == 1 && reader["MainBannerImage1"] != null)
                    {
                        if (File.Exists(FullPath + reader["MainBannerImage1"].ToString()))
                        {
                            File.Delete(FullPath + reader["MainBannerImage1"].ToString());
                        }
                    }
                    else if (idx == 2 && reader["MainBannerImage2"] != null)
                    {
                        if (File.Exists(FullPath + reader["MainBannerImage2"].ToString()))
                        {
                            File.Delete(FullPath + reader["MainBannerImage2"].ToString());
                        }
                    }
                    else if (idx == 3 && reader["MainBannerImage3"] != null)
                    {
                        if (File.Exists(FullPath + reader["MainBannerImage3"].ToString()))
                        {
                            File.Delete(FullPath + reader["MainBannerImage3"].ToString());
                        }
                    }
                }
                reader.Close();


                sql = "update dbo.Homepage set ";
                if (idx == 1)
                {
                    sql += "  MainBannerCaption1 = null, MainBannerURL1 = null, MainBannerImage1 = null ";
                }
                else if (idx == 2)
                {
                    sql += "  MainBannerCaption2 = null, MainBannerURL2 = null, MainBannerImage2 = null ";
                }
                else if (idx == 3)
                {
                    sql += "  MainBannerCaption3 = null, MainBannerURL3 = null, MainBannerImage3 = null ";
                }

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminHomepage");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        protected void DeleteBanner1Button_Click(object sender, EventArgs e)
        {
            DeleteBanner(1);
        }

        protected void DeleteBanner2Button_Click(object sender, EventArgs e)
        {
            DeleteBanner(2);
        }

        protected void DeleteBanner3Button_Click(object sender, EventArgs e)
        {
            DeleteBanner(3);
        }
    }
}