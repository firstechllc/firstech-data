﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminInstallationType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                sortCriteria = "InstallationTypeName";
                sortDir = "asc";
                ShowInstallationType();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowInstallationType()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select InstallationTypeId, InstallationTypeName, SortOrder, UpdatedBy, UpdatedDt ";
                sql += "from dbo.InstallationType ";
                if (SearchInstallationTypeTxt.Text.Trim() != "")
                {
                    sql += " where InstallationTypeName like '%' + @InstallationTypeName + '%' ";
                }
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                if (SearchInstallationTypeTxt.Text.Trim() != "")
                {
                    Cmd.Parameters.Add("@InstallationTypeName", SqlDbType.NVarChar, 100).Value = SearchInstallationTypeTxt.Text;
                }

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                InstallationTypeList.DataSource = ds;
                InstallationTypeList.DataBind();

                if (InstallationTypeList != null && InstallationTypeList.HeaderRow != null && InstallationTypeList.HeaderRow.Cells.Count > 0)
                {
                    InstallationTypeList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    InstallationTypeList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    InstallationTypeList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    InstallationTypeList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";

                    InstallationTypeList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void InstallationTypeList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            ClearError();
            InstallationTypeList.EditIndex = -1;
            ShowInstallationType();
        }

        protected void InstallationTypeList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ClearError();

            string InstallationTypeId = ((HiddenField)(InstallationTypeList.Rows[e.RowIndex].FindControl("InstallationTypeIdHidden"))).Value; ;
            string InstallationType = ((TextBox)(InstallationTypeList.Rows[e.RowIndex].FindControl("InstallationTypeTxt"))).Text;
            string SortOrder = ((TextBox)(InstallationTypeList.Rows[e.RowIndex].FindControl("SortOrderTxt"))).Text;

            if (InstallationType == "")
            {
                ShowError("Please enter Installation Type");
                return;
            }
            int SortOrderInt;
            if (!int.TryParse(SortOrder, out SortOrderInt))
            {
                ShowError("Please enter Sort Order");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.InstallationType set InstallationTypeName = @InstallationTypeName, SortOrder=@SortOrder, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where InstallationTypeId=" + InstallationTypeId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@InstallationTypeName", SqlDbType.NVarChar, 100).Value = InstallationType;
                cmd.Parameters.Add("@SortOrder", SqlDbType.Int).Value = SortOrderInt;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                InstallationTypeList.EditIndex = -1;
                ShowInstallationType();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void InstallationTypeList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ClearError();
            InstallationTypeList.EditIndex = e.NewEditIndex;
            ShowInstallationType();
        }

        protected void InstallationTypeList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string InstallationTypeId = ((HiddenField)(InstallationTypeList.Rows[e.RowIndex].FindControl("InstallationTypeIdHidden2"))).Value; ;
            string InstallationType = ((Label)(InstallationTypeList.Rows[e.RowIndex].FindControl("InstallationTypeLabel"))).Text;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "delete from dbo.InstallationType where InstallationTypeId=" + InstallationTypeId;
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                ShowInstallationType();
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("The DELETE statement conflicted with the REFERENCE constraint") > 0)
                    ShowError("There are Installationing, Prep, or Disassembly data of " + InstallationType + ". You cannot delete it.");
                else
                    ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void InstallationTypeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this InstallationType?');";
                    }
                }
            }

        }

        protected void InstallationTypeList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }
            ShowInstallationType();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminInstallationTypeAdd");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowInstallationType();
            InstallationTypeList.PageIndex = 0;
        }
    }
}