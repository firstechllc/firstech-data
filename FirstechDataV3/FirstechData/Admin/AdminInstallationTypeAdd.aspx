﻿<%@ Page Title="Add Installation Type" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminInstallationTypeAdd.aspx.cs" Inherits="FirstechData.Admin.AdminInstallationTypeAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Installation Type</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-box"></i><span>Add Installation Type</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>

            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="InstallationType" CssClass="col-md-2 control-label">Installation Type</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="InstallationType" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="InstallationType" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Installation Type field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="SortOrder" CssClass="col-md-2 control-label">Sort Order</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="SortOrder" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="SortOrder" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Sort Order field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Add" CssClass="button tiny bg-black radius" />
                        <asp:Button runat="server" OnClick="CancelButton_Click" ID="CancelButton" Text="Cancel" CssClass="button tiny bg-black radius" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
