﻿<%@ Page Title="Add Make" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminMakeAdd.aspx.cs" Inherits="FirstechData.Admin.AdminMakeAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Make</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-warehouse"></i><span>Add Make</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>

            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Make" CssClass="col-md-2 control-label">Make</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Make" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Make" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Make field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-2 control-label">Country</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Country" CssClass="form-control" MaxLength="2" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Country" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Country field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" CssClass="button tiny bg-black radius" ID="AddButton" Text="Add" />
                        <asp:Button runat="server" OnClick="CancelButton_Click" CssClass="button tiny bg-black radius" ID="CancelButton" Text="Cancel" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
