﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminModel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                sortCriteria = "VehicleModelName";
                sortDir = "asc";
                ShowModel();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowModel()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleModelId, VehicleModelName, UpdatedBy, UpdatedDt ";
                sql += "from dbo.VehicleModel ";
                if (SearchModelTxt.Text.Trim() != "")
                {
                    sql += " where VehicleModelName like '%' + @VehicleModelName + '%' ";
                }
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                if (SearchModelTxt.Text.Trim() != "")
                {
                    Cmd.Parameters.Add("@VehicleModelName", SqlDbType.NVarChar, 100).Value = SearchModelTxt.Text;
                }

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ModelList.DataSource = ds;
                ModelList.DataBind();

                if (ModelList != null && ModelList.HeaderRow != null && ModelList.HeaderRow.Cells.Count > 0)
                {
                    ModelList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    ModelList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    ModelList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    ModelList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void ModelList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            ClearError();
            ModelList.EditIndex = -1;
            ShowModel();
        }

        protected void ModelList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ClearError();

            string VehicleModelId = ((HiddenField)(ModelList.Rows[e.RowIndex].FindControl("VehicleModelIdHidden"))).Value; ;
            string Model = ((TextBox)(ModelList.Rows[e.RowIndex].FindControl("ModelTxt"))).Text;

            if (Model == "")
            {
                ShowError("Please enter Model");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleModel set VehicleModelName = @VehicleModelName, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleModelId=" + VehicleModelId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleModelName", SqlDbType.NVarChar, 100).Value = Model;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                ModelList.EditIndex = -1;
                ShowModel();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ModelList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ClearError();
            ModelList.EditIndex = e.NewEditIndex;
            ShowModel();
        }

        protected void ModelList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string VehicleModelId = ((HiddenField)(ModelList.Rows[e.RowIndex].FindControl("VehicleModelIdHidden2"))).Value; ;
            string Model = ((Label)(ModelList.Rows[e.RowIndex].FindControl("ModelLabel"))).Text;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "delete from dbo.VehicleModel where VehicleModelId=" + VehicleModelId;
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                ShowModel();
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("The DELETE statement conflicted with the REFERENCE constraint") > 0)
                    ShowError("There are Wireing, Prep, or Disassembly data of " + Model + ". You cannot delete it.");
                else
                    ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ModelList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[2].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Model?');";
                    }
                }
            }

        }

        protected void ModelList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }
            ShowModel();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminModelAdd");
        }

        protected void ModelList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ModelList.PageIndex = e.NewPageIndex;
            ShowModel();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowModel();
            ModelList.PageIndex = 0;
        }
    }
}