﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminNewsfeed.aspx.cs" Inherits="FirstechData.Admin.AdminNewsfeed" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="margin-top: -20px">
        <div class="large-12 columns">
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                    </div>
                    <h3 class="box-title"><i class="icon-document"></i>
                        <span style="color: black; font-size: medium"><b>NEWSFEED</b></span>
                    </h3>
                </div>
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="large-12 columns">
                            <asp:LinkButton runat="server" ID="AddNewsButton" Text="(+) Add News" OnClick="AddNewsButton_Click"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <asp:Panel runat="server" ID="NewPanel" Visible="false">
                                <div class="box border-gray">
                                    <div class="box-header bg-transparent bor">
                                        <h3 class="box-title"><i></i>
                                            <span style="color: black; font-size: medium">
                                                <asp:Literal runat="server" ID="AddNewsTitle"></asp:Literal></span></h3>
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="form-horizontal">
                                                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                                                <div class="form-group">
                                                    <asp:Label runat="server" AssociatedControlID="TitleTxt" CssClass="col-md-2 control-label">Title</asp:Label>
                                                    <div class="col-md-10">
                                                        <asp:TextBox runat="server" ID="TitleTxt" CssClass="form-control row" Width="100%" />
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTxt" Display="Dynamic"
                                                            CssClass="text-danger" ErrorMessage="The Title field is required." />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label runat="server" AssociatedControlID="UrlTxt" CssClass="col-md-2 control-label">URL</asp:Label>
                                                    <div class="col-md-10">
                                                        <asp:TextBox runat="server" ID="UrlTxt" CssClass="form-control row" Width="100%" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label runat="server" AssociatedControlID="NewsfeedDate" CssClass="col-md-2 control-label">Date (MM/DD/YYYY)</asp:Label>
                                                    <div class="col-md-10">
                                                        <asp:TextBox runat="server" ID="NewsfeedDate" ClientIDMode="Static" CssClass="form-control row date form_datetime" />
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="NewsfeedDate" Display="Dynamic"
                                                            CssClass="text-danger" ErrorMessage="The Date field is required." />
                                                        <asp:CompareValidator ID="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="NewsfeedDate"
                                                            ErrorMessage="Invalid date"> </asp:CompareValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label runat="server" AssociatedControlID="NewsfeedImage" CssClass="col-md-2 control-label">Image</asp:Label>
                                                    <div class="col-md-10">
                                                        <asp:FileUpload runat="server" ID="NewsfeedImage" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label runat="server" AssociatedControlID="SummaryTxt" CssClass="col-md-2 control-label">Summary</asp:Label>
                                                    <div class="col-md-10">
                                                        <CKEditor:CKEditorControl ID="SummaryTxt" runat="server" CssClass="form-control row" Width="100%" Height="80px" BasePath="/Scripts/js/ckeditor">
                                                        </CKEditor:CKEditorControl>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="SummaryTxt" Display="Dynamic"
                                                            CssClass="text-danger" ErrorMessage="The Summary field is required." />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <asp:HiddenField runat="server" ID="ThisNewsIdHidden" />
                                                        <asp:Button runat="server" ID="SaveButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveButton_Click"></asp:Button>
                                                        <asp:Button runat="server" ID="CancelButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelButton_Click" CausesValidation="false"></asp:Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <br />

                            <asp:GridView ID="NewsList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowPaging="true" PageSize="5"
                                OnPageIndexChanging="NewsList_PageIndexChanging"
                                OnRowDataBound="NewsList_RowDataBound"
                                OnRowDeleting="NewsList_RowDeleting" 
                                OnSelectedIndexChanged="NewsList_SelectedIndexChanged" 
                                RowStyle-VerticalAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="News" HeaderStyle-Font-Size="11px">
                                        <ItemTemplate>
                                            <h4>
                                                <asp:HyperLink runat="server" Target="_blank" ID="NFLink"></asp:HyperLink>
                                                <asp:Label runat="server" ID="NFLabel"></asp:Label>
                                                <asp:HiddenField runat="server" ID="NFUrl" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedUrl") %>' />
                                                <asp:HiddenField runat="server" ID="NFTitle" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedTitle") %>' />
                                                <asp:HiddenField runat="server" ID="NFImage" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedImage") %>' />
                                                <asp:HiddenField runat="server" ID="NFDt" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %>' />
                                                <asp:HiddenField runat="server" ID="NFContent" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                            </h4>
                                            <h6><%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %></h6>
                                            <p>
                                                <div class="row">
                                                    <div class="large-8 columns">
                                                        <asp:Literal runat="server" ID="ContentLabel" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                                    </div>
                                                    <div class="large-4 columns right">
                                                        <asp:Image runat="server" ID="ContentImage" />
                                                    </div>
                                                </div>
                                            </p>
                                            <asp:HiddenField runat="server" ID="NewsfeedId" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <asp:HiddenField runat="server" ID="SaveFolderHidden" />
    <asp:HiddenField runat="server" ID="FullPathHidden" />
</asp:Content>
