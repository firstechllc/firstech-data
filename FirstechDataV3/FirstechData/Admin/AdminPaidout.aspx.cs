﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminPaidout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                StartDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DateTime.Today.Year + "-" + DateTime.Today.Month + "-01"));
                EndDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DateTime.Today.Year + "-" + DateTime.Today.Month + "-01").AddMonths(1).AddDays(-1));
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowList();
        }

        private void ShowList()
        { 
            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                SqlCommand Cmd = new SqlCommand("proc_SearchPaidout", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                DateTime dt;
                if (DateTime.TryParse(StartDate.Value.Trim(), out dt))
                    Cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = dt;
                if (DateTime.TryParse(EndDate.Value.Trim(), out dt))
                    Cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = dt.AddDays(1);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                PaidOutList.DataSource = ds;
                PaidOutList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand cmd = new SqlCommand("proc_SearchPaidout", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                DateTime dt;
                if (DateTime.TryParse(StartDate.Value.Trim(), out dt))
                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = dt;
                if (DateTime.TryParse(EndDate.Value.Trim(), out dt))
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = dt.AddDays(1);

                SqlDataReader reader = cmd.ExecuteReader();
                StringBuilder excel = new StringBuilder();

                excel.AppendLine("ID,MTCRepcode,InvoiceNumber,Email,AccountType,SubscriptionPlan,RewardType,RewardAmount,AddedDate");
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                while (reader.Read())
                {
                    excel.Append(reader["ID"].ToString());
                    excel.Append(",");
                    excel.Append(reader["EditorMTCRepcode"].ToString());
                    excel.Append(",");
                    excel.Append(reader["InvoiceNumber"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Email"].ToString());
                    excel.Append(",");
                    excel.Append(reader["AccountType"].ToString());
                    excel.Append(",");
                    excel.Append(reader["SubscriptionPlan"].ToString());
                    excel.Append(",");
                    excel.Append(reader["RewardType"].ToString());
                    excel.Append(",");
                    excel.Append(reader["RewardAmount"].ToString());
                    excel.Append(",");
                    excel.Append(string.Format("{0:MM/dd/yyyy}", reader["AddedDate"]));
                    excel.AppendLine("");
                }

                reader.Close();
                conn.Close();

                string filename = "PaidOut.csv";
                string content_type = "text/csv";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = content_type;
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.Charset = "";
                this.EnableViewState = false;
                Response.Write(excel.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}