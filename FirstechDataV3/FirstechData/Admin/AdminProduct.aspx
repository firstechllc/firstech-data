﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminProduct.aspx.cs" Inherits="FirstechData.Admin.AdminProduct" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .form-group
        {
            margin-bottom:0px;
        }
        .form-control
        {
            font-size:12px;
        }
        .nomax
        {
            max-width: none;
        }
    </style>
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                    </div>
                    <h3 class="box-title"><i class="icon-gear"></i>
                        <span style="color: black; font-size: medium"><b>Product Information</b></span>
                    </h3>
                </div>
                <div>
                    <div class="row">
                        <div class="form-group form-horizontal">
                            <div class="col-sm-3">
                                <asp:DropDownList runat="server" ID="ProductList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="ProductList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3 text-right">
                                <asp:Button runat="server" ID="AddProductPanelButton" CssClass="button tiny bg-black radius" Text="Add Product" OnClick="AddProductPanelButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label><asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
                    <asp:HiddenField runat="server" ID="ProductIdHidden" />
                    <asp:HiddenField runat="server" ID="ProductTypeIdHidden" />
                </div>
            </div>
            <asp:Panel runat="server" ID="NewProductPanel" Visible="false">
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddProductTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="ProductTypeList" CssClass="col-md-2 control-label">Product Type</asp:Label>
                                    <div class="col-md-10">
                                        <asp:DropDownList runat="server" ID="ProductTypeList" CssClass="form-control" Width="250px" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="ModelNumber" CssClass="col-md-2 control-label">Model Number</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="ModelNumber" CssClass="form-control" Width="250px" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="PartNumber" CssClass="col-md-2 control-label">Part Number</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="PartNumber" CssClass="form-control" Width="250px" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="ProductPicture" CssClass="col-md-2 control-label">Picture</asp:Label>
                                    <div class="col-md-10">
                                        <asp:FileUpload runat="server" ID="ProductPicture" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:Button runat="server" ID="AddProductButton" CssClass="button tiny bg-black radius" Text="Add" OnClick="AddProductButton_Click"></asp:Button>
                                        <asp:Button runat="server" ID="CancelAddProductButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelAddProductButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="EditBrainPanel" Visible="false">
                <div class="row bg-white" runat="server" id="TitlePanel">
                    <div class="large-4 columns" style="padding-top: 5px;">
                        <h4><asp:Literal runat="server" ID="CurrentBrainProduct"></asp:Literal></h4>
                    </div>
                    <div class="large-2 columns" style="padding-top: 5px;">
                    </div>
                    <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
                        <asp:Button runat="server" ID="DeleteBrainButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Product" OnClick="DeleteBrainButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Product?');"></asp:Button>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Search Fields:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditBrainModelNumber" CssClass="col-md-3 control-label">Model Number:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:TextBox runat="server" ID="EditBrainModelNumber" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditBrainPartNumber" CssClass="col-md-3 control-label">Part Number:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:TextBox runat="server" ID="EditBrainPartNumber" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="AlternateNamesList" CssClass="col-md-3 control-label">Alternate Names:</asp:Label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:ListBox runat="server" ID="AlternateNamesList" CssClass="form-control" Height="60px" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                                <div class="col-md-1" style="text-align: center; ">
                                                    <asp:Button runat="server" ID="AlternateNamesAddButton" Width="20px" Text="+" OnClick="AlternateNamesAddButton_Click" />
                                                    <asp:Button runat="server" ID="AlternateNamesDelButton" Width="20px" Text="-" OnClick="AlternateNamesDelButton_Click" />
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:TextBox runat="server" ID="AlternateNames" CssClass="form-control" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditBrainProductPicture" CssClass="col-md-3 control-label">Picture:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:FileUpload runat="server" ID="EditBrainProductPicture" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-9 right">
                                            <asp:Button runat="server" ID="SaveControlBrainButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveControlBrainButton_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <asp:Image runat="server" ID="ProductPictureView" />
                                <asp:HiddenField runat="server" ID="ProductPictureFilepath" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Product Specifications:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="BrainDocumentURL" CssClass="col-md-2 control-label">Document URL:</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="BrainDocumentURL" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainTwoWayAntennaList" CssClass="col-md-2 control-label">2 Way Antenna:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainTwoWayAntennaList" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainOneWayAntennaList" CssClass="col-md-2 control-label">1 Way Antenna:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainOneWayAntennaList" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainDroneCompatible" CssClass="col-md-2 control-label">Drone Compatible:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainDroneCompatible" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainStatusList" CssClass="col-md-2 control-label">Status:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainStatusList" CssClass="form-control">
                                            <asp:ListItem Text="Current" Value="Current"></asp:ListItem>
                                            <asp:ListItem Text="Discontinued" Value="Discontinued"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainOperatingVoltages" CssClass="col-md-2 control-label">Operating Voltages:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditBrainOperatingVoltages" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainBladeCompatible" CssClass="col-md-2 control-label">Blade Compatible:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainBladeCompatible" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            <asp:ListItem Text="Built In" Value="Built In"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainAvailableForWarranty" CssClass="col-md-2 control-label">Available for Warranty:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainAvailableForWarranty" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="Limited" Value="Limited"></asp:ListItem>
                                            <asp:ListItem Text="Very Limited" Value="Very Limited"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainIdleCurrent" CssClass="col-md-2 control-label">Idle Current(mA):</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditBrainIdleCurrent" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainDataPort" CssClass="col-md-2 control-label">Data Port(s):</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainDataPort" CssClass="form-control">
                                            <asp:ListItem Text="2x2 Way" Value="2x2 Way"></asp:ListItem>
                                            <asp:ListItem Text="1x2 Way" Value="1x2 Way"></asp:ListItem>
                                            <asp:ListItem Text="1x1 Way" Value="1x1 Way"></asp:ListItem>
                                            <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainReplacePartNoListSelected" CssClass="col-md-2 control-label">Replacement Part Number:</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainReplacePartNoList" CssClass="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainReplacePartNoAdd" Width="20px" Text="+" OnClick="EditBrainReplacePartNoAdd_Click" />
                                                <asp:Button runat="server" ID="EditBrainReplacePartNoDel" Width="20px" Text="-" OnClick="EditBrainReplacePartNoDel_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainReplacePartNoListSelected" CssClass="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainOperatingTemperatureC" CssClass="col-md-2 control-label">Operating Temperature Celsius:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditBrainOperatingTemperatureC" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainOperatingTemperatureF" CssClass="col-md-2 control-label">Operating Temperature Fahrenheit:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditBrainOperatingTemperatureF" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainAntennaPort4Pin" CssClass="col-md-2 control-label">4 Pin Antenna Port:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainAntennaPort4Pin" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            <asp:ListItem Text="FT-D100" Value="FT-D100"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainAntennaPort6Pin" CssClass="col-md-2 control-label">6 Pin Antenna Port:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainAntennaPort6Pin" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            <asp:ListItem Text="FT-D100" Value="FT-D100"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainWaterResistantList" CssClass="col-md-2 control-label">Water Resistant:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditBrainWaterResistantList" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveBrainProductSpecButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveBrainProductSpecButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Features:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainConvenienceListSelected" CssClass="col-md-1 control-label">Convenience:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainConvenienceList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainConvenienceAddButton" Text="+" Width="20px" OnClick="EditBrainConvenienceAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainConvenienceDelButton" Text="-" Width="20px" OnClick="EditBrainConvenienceDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainConvenienceListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainRemoteStartListSelected" CssClass="col-md-1 control-label">Remote Start:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainRemoteStartList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainRemoteStartAddButton" Text="+" Width="20px" OnClick="EditBrainRemoteStartAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainRemoteStartDelButton" Text="-" Width="20px" OnClick="EditBrainRemoteStartDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainRemoteStartListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainAlarmListSelected" CssClass="col-md-1 control-label">Alarm:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainAlarmList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainAlarmAddButton" Text="+" Width="20px" OnClick="EditBrainAlarmAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainAlarmDelButton" Text="-" Width="20px" OnClick="EditBrainAlarmDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainAlarmListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainInstallationListSelected" CssClass="col-md-1 control-label">Installation Flexibility:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainInstallationList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainInstallationAddButton" Text="+" Width="20px" OnClick="EditBrainInstallationAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainInstallationDelButton" Text="-" Width="20px" OnClick="EditBrainInstallationDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainInstallationListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveFeatureButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveFeatureButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Remote:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainCompRemotesCurrentListSelected" CssClass="col-md-1 control-label">Current:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainCompRemotesCurrentList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainCompRemotesCurrentAddButton" Text="+" Width="20px" OnClick="EditBrainCompRemotesCurrentAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainCompRemotesCurrentDelButton" Text="-" Width="20px" OnClick="EditBrainCompRemotesCurrentDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainCompRemotesCurrentListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditBrainCompRemotesDisListSelected" CssClass="col-md-1 control-label">Discontinued:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainCompRemotesDisList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainCompRemotesDisAddButton" Text="+" Width="20px" OnClick="EditBrainCompRemotesDisAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainCompRemotesDisDelButton" Text="-" Width="20px" OnClick="EditBrainCompRemotesDisDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainCompRemotesDisListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveCompRemoteButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveCompRemoteButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Accessories:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditBrainAccessoriesListSelected" CssClass="col-md-1 control-label">Model Number:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainAccessoriesList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditBrainAccessoriesAddButton" Text="+" Width="20px" OnClick="EditBrainAccessoriesAddButton_Click" />
                                                <asp:Button runat="server" ID="EditBrainAccessoriesDelButton" Text="-" Width="20px" OnClick="EditBrainAccessoriesDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditBrainAccessoriesListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAccessoriesButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAccessoriesButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="EditRemotePanel" Visible="false">
                <div class="row bg-white" runat="server" id="Div1">
                    <div class="large-4 columns" style="padding-top: 5px;">
                        <h4><asp:Literal runat="server" ID="CurrentRemoteProduct"></asp:Literal></h4>
                    </div>
                    <div class="large-2 columns" style="padding-top: 5px;">
                    </div>
                    <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
                        <asp:Button runat="server" ID="DeleteRemoteButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Product" OnClick="DeleteRemoteButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Product?');"></asp:Button>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Search Fields:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditRemoteModelNumber" CssClass="col-md-3 control-label">Model Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditRemoteModelNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditRemoteFccId" CssClass="col-md-3 control-label">FCC ID(US):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditRemoteFccId" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditRemotePartNumber" CssClass="col-md-3 control-label">Part Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditRemotePartNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditRemoteICID" CssClass="col-md-3 control-label">IC ID (Canada):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditRemoteICID" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="AlternateNamesRemoteList" CssClass="col-md-3 control-label">Alternate Names:</asp:Label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:ListBox runat="server" ID="AlternateNamesRemoteList" CssClass="form-control" Height="60px" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                                <div class="col-md-1" style="text-align: center; ">
                                                    <asp:Button runat="server" ID="AlternateNamesRemoteAddButton" Width="20px" Text="+" OnClick="AlternateNamesRemoteAddButton_Click" />
                                                    <asp:Button runat="server" ID="AlternateNamesRemoteDelButton" Width="20px" Text="-" OnClick="AlternateNamesRemoteDelButton_Click" />
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:TextBox runat="server" ID="AlternateNamesRemtoe" CssClass="form-control" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditRemoteProductPicture" CssClass="col-md-3 control-label">Picture:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:FileUpload runat="server" ID="EditRemoteProductPicture" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-9 right">
                                            <asp:Button runat="server" ID="SaveRemoteButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveRemoteButton_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <asp:Image runat="server" ID="RemotePictureView" />
                                <asp:HiddenField runat="server" ID="ProductRemoteFilepath" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Product Specifications:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="RemoteDocumentURL" CssClass="col-md-2 control-label">Document URL:</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="RemoteDocumentURL" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="WayRemoteList" CssClass="col-md-2 control-label">2 Way/2 Way:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="WayRemoteList" CssClass="form-control">
                                            <asp:ListItem Text="2 Way Remote" Value="2Way"></asp:ListItem>
                                            <asp:ListItem Text="1 Way Remote" Value="1Way"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteTwoWayAntenna" CssClass="col-md-2 control-label">2 Way Antenna:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteTwoWayAntenna" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteOneWayAntenna" CssClass="col-md-2 control-label">1 Way Antenna:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteOneWayAntenna" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteStatusList" CssClass="col-md-2 control-label">Status:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditRemoteStatusList" CssClass="form-control">
                                            <asp:ListItem Text="Current" Value="Current"></asp:ListItem>
                                            <asp:ListItem Text="Discontinued" Value="Discontinued"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteOperatingVoltages" CssClass="col-md-2 control-label">Operating Voltages:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteOperatingVoltages" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteRangeInFeet" CssClass="col-md-2 control-label">Range In Feet:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteRangeInFeet" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteAvailableForWarranty" CssClass="col-md-2 control-label">Available for Warranty:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditRemoteAvailableForWarranty" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="Limited" Value="Limited"></asp:ListItem>
                                            <asp:ListItem Text="Very Limited" Value="Very Limited"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteIdleCurrent" CssClass="col-md-2 control-label">Idle Current(mA):</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteIdleCurrent" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteBatteryType" CssClass="col-md-2 control-label">Battery Type:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteBatteryType" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteReplacePartNoListSelected" CssClass="col-md-2 control-label">Replacement Part Number:</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteReplacePartNoList" CssClass="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteReplacePartNoAddButton" Width="20px" Text="+" OnClick="EditRemoteReplacePartNoAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteReplacePartNoDelButton" Width="20px" Text="-" OnClick="EditRemoteReplacePartNoDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteReplacePartNoListSelected" CssClass="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteOperatingTemperatureC" CssClass="col-md-2 control-label">Operating Temperature Celsius:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteOperatingTemperatureC" CssClass="form-control" ></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteOperatingTemperatureF" CssClass="col-md-2 control-label">Operating Temperature Fahrenheit:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteOperatingTemperatureF" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteEstimatedBatteryLife" CssClass="col-md-2 control-label">Estimated Battery Life:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditRemoteEstimatedBatteryLife" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteWaterResistantList" CssClass="col-md-2 control-label">Water Resistant:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditRemoteWaterResistantList" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveRemoteProductSpecButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveRemoteProductSpecButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Antenna:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteComp1AntennaListSelected" CssClass="col-md-1 control-label">Compatible 1 Way Antenna:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteComp1AntennaList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteComp1AntennaAddButton" Text="+" Width="20px" OnClick="EditRemoteComp1AntennaAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteComp1AntennaDelButton" Text="-" Width="20px" OnClick="EditRemoteComp1AntennaDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteComp1AntennaListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteComp2AntennaListSelected" CssClass="col-md-1 control-label">Compatible 2 Way Antenna:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteComp2AntennaList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteComp2AntennaAddButton" Text="+" Width="20px" OnClick="EditRemoteComp2AntennaAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteComp2AntennaDelButton" Text="-" Width="20px" OnClick="EditRemoteComp2AntennaDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteComp2AntennaListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveRemoteCompAntennaButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveRemoteCompAntennaButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Features:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteConvenienceListSelected" CssClass="col-md-1 control-label">Convenience:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteConvenienceList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteConvenienceAddButton" Text="+" Width="20px" OnClick="EditRemoteConvenienceAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteConvenienceDelButton" Text="-" Width="20px" OnClick="EditRemoteConvenienceDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteConvenienceListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteAuxiliaryListSelected" CssClass="col-md-1 control-label">Auxiliary:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteAuxiliaryList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteAuxiliaryAddButton" Text="+" Width="20px" OnClick="EditRemoteAuxiliaryAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteAuxiliaryDelButton" Text="-" Width="20px" OnClick="EditRemoteAuxiliaryDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteAuxiliaryListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteProgrammableListSelected" CssClass="col-md-1 control-label">Programmable:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteProgrammableList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteProgrammableAddButton" Text="+" Width="20px" OnClick="EditRemoteProgrammableAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteProgrammableDelButton" Text="-" Width="20px" OnClick="EditRemoteProgrammableDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteProgrammableListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveRemoteFeatureButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveRemoteFeatureButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Brains:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteCompBrainCurrentListSelected" CssClass="col-md-1 control-label">Current:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteCompBrainCurrentList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteCompBrainAddButton" Text="+" Width="20px" OnClick="EditRemoteCompBrainAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteCompBrainDelButton" Text="-" Width="20px" OnClick="EditRemoteCompBrainDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteCompBrainCurrentListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditRemoteCompBrainDisListSelected" CssClass="col-md-1 control-label">Discontinued:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteCompBrainDisList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditRemoteCompBrainDisAddButton" Text="+" Width="20px" OnClick="EditRemoteCompBrainDisAddButton_Click" />
                                                <asp:Button runat="server" ID="EditRemoteCompBrainDisDelButton" Text="-" Width="20px" OnClick="EditRemoteCompBrainDisDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditRemoteCompBrainDisListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveRemoteCompBrainButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveRemoteCompBrainButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="EditAntennaPanel" Visible="false">
                <div class="row bg-white" runat="server" id="Div2">
                    <div class="large-4 columns" style="padding-top: 5px;">
                        <h4><asp:Literal runat="server" ID="CurrentAntennaProduct"></asp:Literal></h4>
                    </div>
                    <div class="large-2 columns" style="padding-top: 5px;">
                    </div>
                    <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
                        <asp:Button runat="server" ID="DeleteAntennaButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Product" OnClick="DeleteAntennaButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Product?');"></asp:Button>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Search Fields:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAntennaModelNumber" CssClass="col-md-3 control-label">Model Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAntennaModelNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditAntennaFccId" CssClass="col-md-3 control-label">FCC ID(US):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAntennaFccId" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAntennaPartNumber" CssClass="col-md-3 control-label">Part Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAntennaPartNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditAntennaICID" CssClass="col-md-3 control-label">IC ID (Canada):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAntennaICID" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="AlternateNamesAntennaList" CssClass="col-md-3 control-label">Alternate Names:</asp:Label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:ListBox runat="server" ID="AlternateNamesAntennaList" CssClass="form-control" Height="60px" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                                <div class="col-md-1" style="text-align: center; ">
                                                    <asp:Button runat="server" ID="AlternateNamesAntennaAddButton" Width="20px" Text="+" OnClick="AlternateNamesAntennaAddButton_Click" />
                                                    <asp:Button runat="server" ID="AlternateNamesAntennaDelButton" Width="20px" Text="-" OnClick="AlternateNamesAntennaDelButton_Click" />
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:TextBox runat="server" ID="AlternateNamesAntenna" CssClass="form-control" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAntennaProductPicture" CssClass="col-md-3 control-label">Picture:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:FileUpload runat="server" ID="EditAntennaProductPicture" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-9 right">
                                            <asp:Button runat="server" ID="SaveAntennaButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAntennaButton_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <asp:Image runat="server" ID="AntennaPictureView" />
                                <asp:HiddenField runat="server" ID="ProductAntennaFilepath" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Product Specifications:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="AntennaDocumentURL" CssClass="col-md-2 control-label">Document URL:</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="AntennaDocumentURL" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="WayAntennaList" CssClass="col-md-2 control-label">2 Way/2 Way:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="WayAntennaList" CssClass="form-control">
                                            <asp:ListItem Text="2 Way Remote" Value="2Way"></asp:ListItem>
                                            <asp:ListItem Text="1 Way Remote" Value="1Way"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaStatusList" CssClass="col-md-2 control-label">Status:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditAntennaStatusList" CssClass="form-control">
                                            <asp:ListItem Text="Current" Value="Current"></asp:ListItem>
                                            <asp:ListItem Text="Discontinued" Value="Discontinued"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaOperatingVoltages" CssClass="col-md-2 control-label">Operating Voltages:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAntennaOperatingVoltages" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaRangeInFeet" CssClass="col-md-2 control-label">Range In Feet:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAntennaRangeInFeet" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaAvailableForWarranty" CssClass="col-md-2 control-label">Available for Warranty:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditAntennaAvailableForWarranty" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="Limited" Value="Limited"></asp:ListItem>
                                            <asp:ListItem Text="Very Limited" Value="Very Limited"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaIdleCurrent" CssClass="col-md-2 control-label">Idle Current(mA):</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAntennaIdleCurrent" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaReplacePartNoListSelected" CssClass="col-md-2 control-label">Replacement Part Number:</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaReplacePartNoList" CssClass="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAntennaReplacePartNoAddButton" Width="20px" Text="+" OnClick="EditAntennaReplacePartNoAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAntennaReplacePartNoDelButton" Width="20px" Text="-" OnClick="EditAntennaReplacePartNoDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaReplacePartNoListSelected" CssClass="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaOperatingTemperatureC" CssClass="col-md-2 control-label">Operating Temperature Celsius:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAntennaOperatingTemperatureC" CssClass="form-control" ></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaOperatingTemperatureF" CssClass="col-md-2 control-label">Operating Temperature Fahrenheit:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAntennaOperatingTemperatureF" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaRequiredCableList" CssClass="col-md-2 control-label">Required Antenna Cable:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditAntennaRequiredCableList" CssClass="form-control">
                                            <asp:ListItem Text="4 pin to 4 pin" Value="4 pin to 4 pin"></asp:ListItem>
                                            <asp:ListItem Text="4 pin to 6 pin" Value="4 pin to 6 pin"></asp:ListItem>
                                            <asp:ListItem Text="6 pin to 6 pin" Value="6 pin to 6 pin"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAntennaProductSpecButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAntennaProductSpecButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Remotes:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaCompRemoteCurrentListSelected" CssClass="col-md-1 control-label">Current:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompRemoteCurrentList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAntennaCompRemoteCurrentAddButton" Text="+" Width="20px" OnClick="EditAntennaCompRemoteCurrentAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAntennaCompRemoteCurrentDelButton" Text="-" Width="20px" OnClick="EditAntennaCompRemoteCurrentDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompRemoteCurrentListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaCompRemoteDisListSelected" CssClass="col-md-1 control-label">Discontinued:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompRemoteDisList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAntennaCompRemoteDisAddButton" Text="+" Width="20px" OnClick="EditAntennaCompRemoteDisAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAntennaCompRemoteDisDelButton" Text="-" Width="20px" OnClick="EditAntennaCompRemoteDisDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompRemoteDisListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAntennaCompRemoteButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAntennaCompRemoteButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Brains:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaCompBrainCurrentListSelected" CssClass="col-md-1 control-label">Current:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompBrainCurrentList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAntennaCompBrainCurrentAddButton" Text="+" Width="20px" OnClick="EditAntennaCompBrainCurrentAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAntennaCompBrainCurrentDelButton" Text="-" Width="20px" OnClick="EditAntennaCompBrainCurrentDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompBrainCurrentListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAntennaCompBrainDisListSelected" CssClass="col-md-1 control-label">Discontinued:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompBrainDisList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAntennaCompBrainDisAddButton" Text="+" Width="20px" OnClick="EditAntennaCompBrainDisAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAntennaCompBrainDisDelButton" Text="-" Width="20px" OnClick="EditAntennaCompBrainDisDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAntennaCompBrainDisListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAntennaCompBrainButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAntennaCompBrainButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="EditAccessoriesPanel" Visible="false">
                <div class="row bg-white" runat="server" id="Div3">
                    <div class="large-4 columns" style="padding-top: 5px;">
                        <h4><asp:Literal runat="server" ID="CurrentAccessoriesProduct"></asp:Literal></h4>
                    </div>
                    <div class="large-2 columns" style="padding-top: 5px;">
                    </div>
                    <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
                        <asp:Button runat="server" ID="DeleteAccessoriesButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Product" OnClick="DeleteAccessoriesButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Product?');"></asp:Button>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Search Fields:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAccessoriesModelNumber" CssClass="col-md-3 control-label">Model Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAccessoriesModelNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditAccessoriesFccId" CssClass="col-md-3 control-label">FCC ID(US):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAccessoriesFccId" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAccessoriesPartNumber" CssClass="col-md-3 control-label">Part Number:</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAccessoriesPartNumber" CssClass="form-control" />
                                        </div>
                                        <asp:Label runat="server" AssociatedControlID="EditAccessoriesICID" CssClass="col-md-3 control-label">IC ID (Canada):</asp:Label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="EditAccessoriesICID" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="AlternateNamesAccessoriesList" CssClass="col-md-3 control-label">Alternate Names:</asp:Label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:ListBox runat="server" ID="AlternateNamesAccessoriesList" CssClass="form-control" Height="60px" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                                <div class="col-md-1" style="text-align: center; ">
                                                    <asp:Button runat="server" ID="AlternateNamesAccessoriesAddButton" Width="20px" Text="+" OnClick="AlternateNamesAccessoriesAddButton_Click" />
                                                    <asp:Button runat="server" ID="AlternateNamesAccessoriesDelButton" Width="20px" Text="-" OnClick="AlternateNamesAccessoriesDelButton_Click" />
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:TextBox runat="server" ID="AlternateNamesAccessories" CssClass="form-control" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="EditAccessoriesProductPicture" CssClass="col-md-3 control-label">Picture:</asp:Label>
                                        <div class="col-md-9">
                                            <asp:FileUpload runat="server" ID="EditAccessoriesProductPicture" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-9 right">
                                            <asp:Button runat="server" ID="SaveAccessoriesInfoButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAccessoriesInfoButton_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <asp:Image runat="server" ID="AccessoriesPictureView" />
                                <asp:HiddenField runat="server" ID="ProductAccessoriesFilepath" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Product Specifications:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="AccessoriesDocumentURL" CssClass="col-md-2 control-label">Document URL:</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="AccessoriesDocumentURL" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesStatusList" CssClass="col-md-2 control-label">Status:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditAccessoriesStatusList" CssClass="form-control">
                                            <asp:ListItem Text="Current" Value="Current"></asp:ListItem>
                                            <asp:ListItem Text="Discontinued" Value="Discontinued"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesOperatingVoltages" CssClass="col-md-2 control-label">Operating Voltages:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAccessoriesOperatingVoltages" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesAvailableForWarranty" CssClass="col-md-2 control-label">Available for Warranty:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="EditAccessoriesAvailableForWarranty" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                            <asp:ListItem Text="Limited" Value="Limited"></asp:ListItem>
                                            <asp:ListItem Text="Very Limited" Value="Very Limited"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesIdleCurrent" CssClass="col-md-2 control-label">Idle Current(mA):</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAccessoriesIdleCurrent" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesOperatingTemperatureC" CssClass="col-md-2 control-label">Operating Temperature Celsius:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAccessoriesOperatingTemperatureC" CssClass="form-control" ></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesOperatingTemperatureF" CssClass="col-md-2 control-label">Operating Temperature Fahrenheit:</asp:Label>
                                    <div class="col-md-2">
                                        <asp:TextBox runat="server" ID="EditAccessoriesOperatingTemperatureF" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesReplacePartNoListSelected" CssClass="col-md-2 control-label">Replacement Part Number:</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesReplacePartNoList" CssClass="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAccessoriesReplacePartNoAddButton" Width="20px" Text="+" OnClick="EditAccessoriesReplacePartNoAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAccessoriesReplacePartNoDelButton" Width="20px" Text="-" OnClick="EditAccessoriesReplacePartNoDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesReplacePartNoListSelected" CssClass="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAccessoriesProductSpecButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAccessoriesProductSpecButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">Compatible Brains:</span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesCompBrainCurrentListSelected" CssClass="col-md-1 control-label">Current:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesCompBrainCurrentList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAccessoriesCompBrainCurrentAddButton" Text="+" Width="20px" OnClick="EditAccessoriesCompBrainCurrentAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAccessoriesCompBrainCurrentDelButton" Text="-" Width="20px" OnClick="EditAccessoriesCompBrainCurrentDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesCompBrainCurrentListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" AssociatedControlID="EditAccessoriesCompBrainDisListSelected" CssClass="col-md-1 control-label">Discontinued:</asp:Label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesCompBrainDisList" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: center; ">
                                                <asp:Button runat="server" ID="EditAccessoriesCompBrainDisAddButton" Text="+" Width="20px" OnClick="EditAccessoriesCompBrainDisAddButton_Click" />
                                                <asp:Button runat="server" ID="EditAccessoriesCompBrainDisDelButton" Text="-" Width="20px" OnClick="EditAccessoriesCompBrainDisDelButton_Click" />
                                            </div>
                                            <div class="col-md-5">
                                                <asp:ListBox runat="server" ID="EditAccessoriesCompBrainDisListSelected" CssClass="form-control" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10 right">
                                        <asp:Button runat="server" ID="SaveAccessoriesCompBrainButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveAccessoriesCompBrainButton_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="AddProductButton" />
            <asp:PostBackTrigger ControlID="SaveControlBrainButton" />
            <asp:PostBackTrigger ControlID="SaveRemoteButton" />
            <asp:PostBackTrigger ControlID="SaveAntennaButton" />
            <asp:PostBackTrigger ControlID="SaveAccessoriesInfoButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
