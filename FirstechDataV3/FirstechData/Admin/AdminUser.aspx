﻿<%@ Page Title="Edit User" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminUser.aspx.cs" Inherits="FirstechData.Admin.User" Async="true" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-user"></i>
                <span>User</span>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <div class="row">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </div>
            <div>
                <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                    <p class="text-success"><%: SuccessMessage %></p>
                </asp:PlaceHolder>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-horizontal">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 control-label">Email</asp:Label>
                            <div class="col-md-10">
                                <asp:Label runat="server" ID="Email" CssClass="form-control" Width="280" />
                                <asp:HiddenField runat="server" ID="UserId" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The First Name field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"  Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Last Name field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 control-label">Store Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="StoreName" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Address field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="City" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The City field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State / Province</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="State" CssClass="form-control" MaxLength="20" Width="100"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="State" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The State field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Zip" CssClass="col-md-2 control-label">Zip</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Zip" CssClass="form-control" MaxLength="20" Width="100"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Zip" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Zip field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-2 control-label">Country</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Country" CssClass="form-control" MaxLength="50" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Country" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Country field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="RegistrationDate" CssClass="col-md-2 control-label">Registration Date</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="RegistrationDate" CssClass="form-control"  />
                                <asp:HiddenField runat="server" ID="RegistrationDateHidden" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="EmailConfirmed" CssClass="col-md-2 control-label">Email Confirmed</asp:Label>
                            <div class="col-md-5" style="height:35px;">
                                <asp:Label runat="server" ID="EmailConfirmed" CssClass="form-control" Width="280" />
                            </div>
                            <div class="col-md-5" style="height:35px;">
                                <asp:Button runat="server" ID="ResendEmailValidButton" Text="Resend Email Validation" OnClick="ResendEmailValidButton_Click" CssClass="btn" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Approved" CssClass="col-md-2 control-label">Approved</asp:Label>
                            <div class="col-md-5" style="height:35px;">
                                <asp:Label runat="server" ID="Approved" CssClass="form-control" Width="280" />
                            </div>
                            <div class="col-md-5" style="height:35px;">
                                <asp:Button runat="server" ID="ApproveButton" Text="Approve" OnClick="ApproveButton_Click" CssClass="btn"/>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="RepPanel">
                            <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">Rep Auto Filter</asp:Label>
                            <div class="col-md-10">
                                Zip Codes:<asp:TextBox runat="server" ID="RepZipCodes" CssClass="form-control" />
                                Cities:<asp:TextBox runat="server" ID="RepCities" CssClass="form-control" />
                                States:<asp:TextBox runat="server" ID="RepStates" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="EditorPanel">
                            <asp:Label runat="server" AssociatedControlID="EditorMTCRepcode" CssClass="col-md-2 control-label">MTCRepcode</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="EditorMTCRepcode" CssClass="form-control" />
                            </div>
                        </div>

                        <dl class="dl-horizontal">
                        </dl>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-5">
                                <asp:Button runat="server" OnClick="UpdateUser_Click" Text="Update" CssClass="btn btn-primary" />
                                <asp:Button runat="server" ID="CloseButton" OnClick="CloseButton_Click" Text="Close" CssClass="btn btn-default" />
                            </div>
                            <div class="col-md-5" style="height:35px;">
                                <asp:Button runat="server" ID="ResetPasswdButton" Text="Reset Pwd" OnClick="ResetPasswdButton_Click" CssClass="btn"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
