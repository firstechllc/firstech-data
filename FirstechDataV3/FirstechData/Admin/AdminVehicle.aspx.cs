﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminVehicle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sortCriteria = "VehicleMakeName";
                sortDir = "asc";
                LoadData();
                ShowVehicle();
            }
        }

        private void LoadData()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName ";
                sql += "from dbo.VehicleMake ";
                sql += "order by VehicleMakeName";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                SampleMakeList.DataSource = ds;
                SampleMakeList.DataValueField = "VehicleMakeId";
                SampleMakeList.DataTextField = "VehicleMakeName";
                SampleMakeList.DataBind();

                SampleMakeList.Items.Insert(0, new ListItem("", ""));

                sql = "select VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleModel ";
                sql += "order by VehicleModelName";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                SampleModelList.DataSource = ds;
                SampleModelList.DataValueField = "VehicleModelId";
                SampleModelList.DataTextField = "VehicleModelName";
                SampleModelList.DataBind();

                SampleModelList.Items.Insert(0, new ListItem("", ""));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowVehicle()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select a.VehicleMakeModelYearId, a.VehicleMakeId, b.VehicleMakeName, a.VehicleModelId, c.VehicleModelName, a.VehicleYear, a.UpdatedDt, a.UpdatedBy ";
                sql += "from dbo.VehicleMakeModelYear a ";
                sql += "join dbo.VehicleMake b on a.VehicleMakeId = b.VehicleMakeId ";
                sql += "join dbo.VehicleModel c on a.VehicleModelId = c.VehicleModelId ";
                sql += "where 1=1 ";
                if (SampleMakeList.SelectedValue != "")
                {
                    sql += " and a.VehicleMakeId = @VehicleMakeId ";
                }
                if (SampleModelList.SelectedValue != "")
                {
                    sql += " and a.VehicleModelId = @VehicleModelId ";
                }
                if (SearchYearTxt.Text.Trim() != "")
                {
                    sql += " and VehicleYear = @Year ";
                }
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                if (SampleMakeList.SelectedValue != "")
                {
                    Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(SampleMakeList.SelectedValue);
                }
                if (SampleModelList.SelectedValue != "")
                {
                    Cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(SampleModelList.SelectedValue);
                }
                if (SearchYearTxt.Text.Trim() != "")
                {
                    Cmd.Parameters.Add("@Year", SqlDbType.Int).Value = int.Parse(SearchYearTxt.Text);
                }

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleList.DataSource = ds;
                VehicleList.DataBind();

                if (VehicleList != null && VehicleList.HeaderRow != null && VehicleList.HeaderRow.Cells.Count > 0)
                {
                    VehicleList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    VehicleList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    VehicleList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    VehicleList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    VehicleList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";

                    VehicleList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void VehicleList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            ClearError();
            VehicleList.EditIndex = -1;
            ShowVehicle();
        }

        protected void VehicleList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ClearError();

            string VehicleMakeModelYearId = ((HiddenField)(VehicleList.Rows[e.RowIndex].FindControl("VehicleMakeModelYearIdHidden"))).Value; ;
            DropDownList MakeList = (DropDownList)(VehicleList.Rows[e.RowIndex].FindControl("MakeList"));
            DropDownList ModelList = (DropDownList)(VehicleList.Rows[e.RowIndex].FindControl("ModelList"));
            string Year = ((TextBox)(VehicleList.Rows[e.RowIndex].FindControl("YearTxt"))).Text;

            if (Year == "")
            {
                ShowError("Please enter Year");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleMakeModelYear set VehicleMakeId = @VehicleMakeId, VehicleModelId=@VehicleModelId, VehicleYear=@VehicleYear, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeModelYearId=" + VehicleMakeModelYearId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                VehicleList.EditIndex = -1;
                ShowVehicle();
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                    ShowError("Same vehicle already exists.");
                else
                    ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void VehicleList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ClearError();
            VehicleList.EditIndex = e.NewEditIndex;
            ShowVehicle();
        }

        protected void VehicleList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string VehicleMakeModelYearId = ((HiddenField)(VehicleList.Rows[e.RowIndex].FindControl("VehicleMakeModelYearIdHidden2"))).Value; ;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "delete from dbo.VehicleMakeModelYear where VehicleMakeModelYearId=" + VehicleMakeModelYearId;
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                ShowVehicle();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void VehicleList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList MakeList = (DropDownList)(e.Row.FindControl("MakeList"));
                DropDownList ModelList = (DropDownList)(e.Row.FindControl("ModelList"));

                HiddenField VehicleMakeId = (HiddenField)(e.Row.FindControl("VehicleMakeIdHidden"));
                HiddenField VehicleModelId = (HiddenField)(e.Row.FindControl("VehicleModelIdHidden"));

                if (MakeList != null)
                {
                    MakeList.Items.Clear();
                    foreach (ListItem item in SampleMakeList.Items)
                    {
                        if (item.Value != "")
                        {
                            MakeList.Items.Add(new ListItem(item.Text, item.Value));
                        }
                    }
                    MakeList.SelectedValue = VehicleMakeId.Value;
                }

                if (ModelList != null)
                {
                    ModelList.Items.Clear();
                    foreach (ListItem item in SampleModelList.Items)
                    {
                        if (item.Value != "")
                        {
                            ModelList.Items.Add(new ListItem(item.Text, item.Value));
                        }
                    }
                    ModelList.SelectedValue = VehicleModelId.Value;
                }

                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Vehicle?');";
                    }
                }
            }

        }

        protected void VehicleList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }
            ShowVehicle();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminVehicleAdd");
        }

        protected void VehicleList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            VehicleList.PageIndex = e.NewPageIndex;
            ShowVehicle();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowVehicle();
            VehicleList.PageIndex = 0;
        }

    }
}