﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminVehicleInfo.aspx.cs" Inherits="FirstechData.Admin.AdminVehicleInfo" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        td input[type="text"] {
            margin-bottom: 0px;
        }

        td select {
            margin-bottom: 0px;
            padding-top: 1px;
            padding-bottom: 1px;
        }
    </style>
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Vehicle Info</b></span><asp:HiddenField runat="server" ID="SaveFolderHidden" /><asp:HiddenField runat="server" ID="RoleNameHidden" />
            </h3>
        </div>
        <div>
            <div class="row">
                <div class="form-group form-horizontal">
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3 text-right">
                        <asp:Button runat="server" ID="AddVehiclePanelButton" CssClass="button tiny bg-black radius" Text="Add Vehicle" OnClick="AddVehiclePanelButton_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label><asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>
    <asp:Panel runat="server" ID="NewVehiclePanel" Visible="false">
        <div class="box">
            <div class="box-header bg-transparent bor">
                <h3 class="box-title"><i></i>
                    <span style="color: black; font-size: medium">
                        <asp:Literal runat="server" ID="AddVehicleTitle"></asp:Literal></span></h3>
            </div>
            <div>
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="MakeList" CssClass="col-md-2 control-label">Make</asp:Label>
                            <div class="col-md-10">
                                <asp:DropDownList runat="server" ID="MakeList" CssClass="form-control" Width="250px" OnSelectedIndexChanged="MakeList_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="ModelList" CssClass="col-md-2 control-label">Model</asp:Label>
                            <div class="col-md-10">
                                <asp:DropDownList runat="server" ID="ModelList" CssClass="form-control" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="ModelList_SelectedIndexChanged"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Year" CssClass="col-md-2 control-label">Year</asp:Label>
                            <div class="col-md-10">
                                <asp:DropDownList runat="server" ID="YearList" CssClass="form-control" Width="250px" ></asp:DropDownList>
                                <asp:TextBox runat="server" ID="Year" CssClass="form-control" MaxLength="4" Width="250px" />
                            </div>
                        </div>
                        <asp:Panel runat="server" ID="VehiclePicturePanel">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="VehiclePicture" CssClass="col-md-2 control-label">Picture</asp:Label>
                            <div class="col-md-10">
                                <asp:FileUpload runat="server" ID="VehiclePicture" />
                            </div>
                        </div>
                        </asp:Panel>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="SearchCountTxt" CssClass="col-md-2 control-label">Search Count</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="SearchCountTxt" CssClass="form-control" MaxLength="4" Width="50px" ReadOnly="true" />
                                Counter Adjust
                                <asp:TextBox runat="server" ID="AdjustSearchCountTxt" CssClass="form-control" MaxLength="4" Width="50px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" ID="AddVehicleButton" CssClass="button tiny bg-black radius" Text="Add" OnClick="AddVehicleButton_Click"></asp:Button>
                                <asp:Button runat="server" ID="CancelAddVehicleButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelAddVehicleButton_Click"></asp:Button>
                                &nbsp;&nbsp;&nbsp;<asp:Button runat="server" ID="UnlinkVehicleButton" CssClass="button tiny bg-black radius" Text="Unlink" OnClick="UnlinkVehicleButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="ImportPanel" Visible="false">
        <div class="box">
            <div class="box-header bg-transparent bor">
                <h3 class="box-title"><i></i>
                    <span style="color: black; font-size: medium">Import</span></h3>
            </div>
            <div>
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="ImportFile" CssClass="col-md-2 control-label">File</asp:Label>
                            <div class="col-md-10">
                                <asp:FileUpload runat="server" ID="ImportFile" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" ID="ImportFileButton" CssClass="button tiny bg-black radius" Text="Start" OnClick="ImportFileButton_Click"></asp:Button>
                                <asp:Button runat="server" ID="CancelImportFileButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelImportFileButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="row bg-white" runat="server" id="TitlePanel" visible="false">
        <div class="large-4 columns" style="padding-top: 5px;">
            <h4><asp:Literal runat="server" ID="CurrentVehicle"></asp:Literal></h4>
        </div>
        <div class="large-2 columns" style="padding-top: 5px;">
            <h5><asp:Literal runat="server" ID="LinkVehicle"></asp:Literal></h5>
            <asp:HiddenField runat="server" ID="LinkVehicleMakeModelYearIdHidden" />
            <asp:HiddenField runat="server" ID="LinkVehicleMakeIdHidden" />
            <asp:HiddenField runat="server" ID="LinkVehicleModelIdHidden" />
            <asp:HiddenField runat="server" ID="LinkVehicleYearHidden" />
        </div>
        <div class="large-6 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
            <asp:HiddenField runat="server" ID="VehicleMakeModelYearIdHidden" />
            <asp:HiddenField runat="server" ID="Approved" />
            <asp:Button runat="server" ID="LinkVehicleButton" CssClass="button tiny bg-black radius no-margin" Text="Link Vehicle" OnClick="LinkVehicleButton_Click"></asp:Button>
            <asp:Button runat="server" ID="EditVehicleButton" CssClass="button tiny bg-black radius no-margin" Text="Edit Vehicle" OnClick="EditVehicleButton_Click"></asp:Button>
            <asp:Button runat="server" ID="DeleteVehicleButton" CssClass="button tiny bg-black radius no-margin" Text="Delete Vehicle" OnClick="DeleteVehicleButton_Click" OnClientClick="return confirm('Are you sure you want to delete this Vehicle?');"></asp:Button>
            &nbsp;&nbsp;
            <asp:Button runat="server" ID="ExportButton" CssClass="button tiny bg-black radius no-margin" Text="Export" OnClick="ExportButton_Click"></asp:Button>
            <asp:Button runat="server" ID="ImportButton" CssClass="button tiny bg-black radius no-margin" Text="Import" OnClick="ImportButton_Click"></asp:Button>&nbsp;
            <asp:Button runat="server" ID="ApproveButton" CssClass="button tiny bg-black radius no-margin" Text="Approve" OnClick="ApproveButton_Click"></asp:Button>
            <asp:Button runat="server" ID="ReserveButton" CssClass="button tiny bg-black radius no-margin" Text="Reserve" OnClick="ReserveButton_Click"></asp:Button>
            <asp:Button runat="server" ID="CancelReserveButton" Text="Cancel" OnClick="CancelReserveButton_Click" CssClass="btn btn-primary btn-sm" Visible="false" OnClientClick="return confirm('Are you sure you want to cancel it?');" />
            <asp:Label runat="server" ID="ReservedByLabel" ForeColor="Red" Font-Bold="true" Font-Size="12"></asp:Label>
        </div>
    </div>
    <div class="row bg-white" runat="server" id="UrgentMessagePanel" visible="false">
        <div class="large-2 columns" style="padding-top: 5px;">
            <h5>Urgent Message</h5>
        </div>
        <div class="large-10 columns text-right" style="padding-top: 10px; padding-bottom: 10px;">
            <div class="row">
                <div class="large-10 columns text-left">
                    <asp:Label runat="server" ID="UrgentMessageLabel"></asp:Label>
                </div>
                <div class="large-2 columns">
                    <asp:Button runat="server" ID="EditUrgentMessageButton" CssClass="button tiny bg-black radius no-margin" Text="Edit" OnClick="EditUrgentMessageButton_Click"></asp:Button>
                </div>
            </div>
            <div class="row">
                <asp:Panel runat="server" ID="UpdateUrgentMessagePanel" Visible="false">
                    <br />
                    <asp:TextBox runat="server" ID="UrgentMessageEdit" Height="30px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                    <asp:Button runat="server" ID="SaveUrgentMessageButton" CssClass="button tiny bg-black radius no-margin" Text="Save" OnClick="SaveUrgentMessageButton_Click"></asp:Button>
                    <asp:Button runat="server" ID="CancelUrgentMessageButton" CssClass="button tiny bg-black radius no-margin" Text="Cancel" OnClick="CancelUrgentMessageButton_Click"></asp:Button>
                </asp:Panel>
            </div>
        </div>
    </div>
    <ul class="tabs row" data-tab>
        <li class="tab-title active" runat="server" id="WiringPaneltab">
            <a href="#<%=WiringPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="WiringHeader" Text="Vehicle Wiring"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linksdisassemblytab">
            <a href="#<%=linksdisassembly.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DisassemblyHeader" Text="Disassembly"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkspreptab">
            <a href="#<%=linksprep.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="PrepHeader" Text="Prep"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkroutingtab">
            <a href="#<%=linkrouting.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="RoutingHeader" Text="Routing/Placement"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkprogrammingtab">
            <a href="#<%=linkprogramming.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="ProgrammingHeader" Text="Programming"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="FBPaneltab">
            <a href="#<%=FBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="FBResultHeader" Text="Facebook Result"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="DocumentPaneltab">
            <a href="#<%=DocumentPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DocumentHeader" Text="Documents"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="TSBPaneltab">
            <a href="#<%=TSBPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="TSBHeader" Text="TSB"></asp:Literal></a>
        </li>
    </ul>
    <div class="tabs-content edumix-tab-horz">
        <div class="content active" runat="server" id="WiringPanel">
            <asp:UpdatePanel ID="WireListPanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row"><div class="col-sm-12 text-center"><asp:Label runat="server" ID="WireErrorLabel" ForeColor="Red" Visible="false"></asp:Label><asp:Label runat="server" ID="Label2" ForeColor="Blue" Visible="false"></asp:Label></div></div>
                    <div class="row">
                        <div class="large-6 columns">
                            <asp:Panel runat="server" ID="ShowNotePanel" >
                                <asp:Label runat="server" ID="NoteTitleLabel" Text="Note" Font-Bold="true"></asp:Label>
                                <asp:Panel runat="server" ID="EditNotePanel" Visible="false">
                                    <div class="box border-gray">
                                        <div class="box-header bg-transparent bor">
                                            <h3 class="box-title"><i></i>
                                                <span style="color: black; font-size: medium">Edit Note</span></h3>
                                        </div>
                                        <div>
                                            <div class="row">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="InstallationTypeEditLabl" CssClass="col-md-2 control-label">Note</asp:Label>
                                                        <div class="col-md-8" style="padding-left: 23px;">
                                                            <asp:Label runat="server" ID="InstallationTypeEditLabl"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label runat="server" AssociatedControlID="editor" CssClass="col-md-2 control-label">Note</asp:Label>
                                                        <div class="col-md-8" style="padding-left: 23px;">
                                                            <CKEditor:CKEditorControl ID="editor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor"></CKEditor:CKEditorControl>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-offset-2 col-md-10">
                                                            <asp:HiddenField runat="server" ID="VehicleWireNoteIdHidden" />
                                                            <asp:HiddenField runat="server" ID="NoteInstallationTypeIdHidden" />
                                                            <asp:HiddenField runat="server" ID="VehicleWireNoteEditorWorkIdHidden" />
                                                            <asp:HiddenField runat="server" ID="VehicleWireNoteEditorWorkActionHidden" />
                                                            <asp:Button runat="server" ID="SaveNoteButton" Text="Save" CssClass="button tiny bg-black radius" OnClick="SaveNoteButton_Click" />
                                                            <asp:Button runat="server" ID="CancelNoteButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelNoteButton_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:GridView ID="NoteList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" OnSelectedIndexChanged="NoteList_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Installation Type" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="InstallationTypeNameLabel" runat="server" Text='<%# Bind("InstallationTypeName") %>' Font-Size="11px"></asp:Label>
                                                <asp:HiddenField ID="VehicleWireNoteId" runat="server" Value='<%# Bind("VehicleWireNoteId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="InstallationTypeId" runat="server" Value='<%# Bind("InstallationTypeId") %>'></asp:HiddenField>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Note" HeaderText="Note" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HtmlEncode="false" />
                                        <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                                            ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                    </Columns>
                                    <HeaderStyle CssClass="table-header" />
                                </asp:GridView>
                                <asp:GridView ID="NoteListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                                    OnRowDataBound="NoteListEditor_RowDataBound" OnRowCommand="NoteListEditor_RowCommand" 
                                    Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                                    <Columns>
                                        <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                        <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                        <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                                        <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                        <asp:TemplateField HeaderText="Installation Type" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="InstallationTypeNameLabel" runat="server" Text='<%# Bind("InstallationTypeName") %>' Font-Size="11px"></asp:Label>
                                                <asp:HiddenField ID="VehicleWireNoteId" runat="server" Value='<%# Bind("VehicleWireNoteId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="InstallationTypeId" runat="server" Value='<%# Bind("InstallationTypeId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="VehicleWireNoteEditorId" runat="server" Value='<%# Bind("VehicleWireNoteEditorId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Note" HeaderText="Note" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HtmlEncode="false" />
                                        <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                                        <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                                    </Columns>
                                    <HeaderStyle CssClass="table-header" />
                                </asp:GridView>

                            </asp:Panel>
                        </div>
                        <div class="large-6 columns">
                            <asp:Label runat="server" ID="PictureLabel" Text="Picture" Font-Bold="true" Visible="false"></asp:Label>
                            <asp:Image runat="server" ID="VehiclePicture1" />
                            <asp:HiddenField runat="server" ID="VehiclePictureFilepath1" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <asp:Label runat="server" ID="LastUpdatedDate" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <br />
                            <asp:Label runat="server" ID="WireLabel" Text="Wire" Font-Bold="true" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <asp:LinkButton runat="server" ID="AddWirePanelButton" Text="(+) Add Row (Vehicle Wiring)" OnClick="AddWirePanelButton_Click" Visible="false"></asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="AddWireBatchButton" Text="(+) Add Rows" OnClick="AddWireBatchButton_Click" Visible="false"></asp:LinkButton>
                            <asp:HiddenField runat="server" ID="ExistingWiringFilename1" />
                            <asp:HiddenField runat="server" ID="ExistingWiringFilename2" />
                            <asp:HiddenField runat="server" ID="ExistingWiringFilename3" />
                            <asp:HiddenField runat="server" ID="ExistingWiringFilename4" />
                            <asp:HiddenField runat="server" ID="ExistingWiringFilename5" />
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="AddWirePanel" Visible="false">
                        <div class="box border-gray">
                            <div class="box-header bg-transparent bor">
                                <h3 class="box-title"><i></i>
                                    <span style="color: black; font-size: medium">
                                        <asp:Literal runat="server" ID="AddWiringTitle"></asp:Literal></span></h3>
                            </div>
                            <div>
                                <div class="row">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="WireFunctionList" CssClass="col-md-2 control-label">Wire Function</asp:Label>
                                            <div class="col-md-4 text-left">
                                                <asp:DropDownList runat="server" ID="WireFunctionList" CssClass="form-control no-margin no-padding" />
                                            </div>
                                            <asp:Label runat="server" AssociatedControlID="InstalltionTypeList" CssClass="col-md-2 control-label">Installtion Type</asp:Label>
                                            <div class="col-md-4 text-left">
                                                <asp:DropDownList runat="server" ID="InstalltionTypeList" CssClass="form-control no-margin no-padding" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="Color" CssClass="col-md-2 control-label">Vehicle Color</asp:Label>
                                            <div class="col-md-4" style="padding-left: 23px;">
                                                <asp:TextBox runat="server" ID="Color" CssClass="form-control row" />
                                            </div>
                                            <asp:Label runat="server" AssociatedControlID="VehicleColor" CssClass="col-md-2 control-label">CM7X00/ADS Color</asp:Label>
                                            <div class="col-md-4" style="padding-left: 23px;">
                                                <asp:TextBox runat="server" ID="VehicleColor" CssClass="form-control row" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="Location" CssClass="col-md-2 control-label">Location</asp:Label>
                                            <div class="col-md-4" style="padding-left: 23px;">
                                                <asp:TextBox runat="server" ID="Location" CssClass="form-control row" />
                                            </div>
                                            <asp:Label runat="server" AssociatedControlID="PinOut" CssClass="col-md-2 control-label">Pin Out</asp:Label>
                                            <div class="col-md-4" style="padding-left: 23px;">
                                                <asp:TextBox runat="server" ID="PinOut" CssClass="form-control row" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="Polarity" CssClass="col-md-2 control-label">Polarity</asp:Label>
                                            <div class="col-md-4" style="padding-left: 23px;">
                                                <asp:TextBox runat="server" ID="Polarity" CssClass="form-control row" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:Image runat="server" ID="AttachWiringImage1" Visible="false" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete1" Text="Delete?" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:FileUpload runat="server" ID="AttachWiringFile1" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:Label runat="server" ID="ReplaceWiringImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-2 columns" style="padding-left: 0">
                                                        Or URL:
                                                    </div>
                                                    <div class="small-10 columns">
                                                        <asp:TextBox runat="server" ID="AttachWiringFileUrl1" Width="300px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <br />

                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:Image runat="server" ID="AttachWiringImage2" Visible="false" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete2" Text="Delete?" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:FileUpload runat="server" ID="AttachWiringFile2" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:Label runat="server" ID="ReplaceWiringImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-2 columns" style="padding-left: 0">
                                                        Or URL:
                                                    </div>
                                                    <div class="small-10 columns">
                                                        <asp:TextBox runat="server" ID="AttachWiringFileUrl2" Width="300px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <br />

                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:Image runat="server" ID="AttachWiringImage3" Visible="false" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete3" Text="Delete?" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:FileUpload runat="server" ID="AttachWiringFile3" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:Label runat="server" ID="ReplaceWiringImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-2 columns" style="padding-left: 0">
                                                        Or URL:
                                                    </div>
                                                    <div class="small-10 columns">
                                                        <asp:TextBox runat="server" ID="AttachWiringFileUrl3" Width="300px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <br />

                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:Image runat="server" ID="AttachWiringImage4" Visible="false" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete4" Text="Delete?" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:FileUpload runat="server" ID="AttachWiringFile4" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:Label runat="server" ID="ReplaceWiringImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-2 columns" style="padding-left: 0">
                                                        Or URL:
                                                    </div>
                                                    <div class="small-10 columns">
                                                        <asp:TextBox runat="server" ID="AttachWiringFileUrl4" Width="300px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <br />

                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:Image runat="server" ID="AttachWiringImage5" Visible="false" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:CheckBox runat="server" ID="AttachWiringImageDelete5" Text="Delete?" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-9 columns" style="padding-left: 0">
                                                        <asp:FileUpload runat="server" ID="AttachWiringFile5" />
                                                    </div>
                                                    <div class="small-3 columns">
                                                        <asp:Label runat="server" ID="ReplaceWiringImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="small-2 columns" style="padding-left: 0">
                                                        Or URL:
                                                    </div>
                                                    <div class="small-10 columns">
                                                        <asp:TextBox runat="server" ID="AttachWiringFileUrl5" Width="300px"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-2 col-md-10">
                                                <asp:HiddenField runat="server" ID="VehicleWireFunctionIdHidden" />
                                                <asp:HiddenField runat="server" ID="VehicleWireEditorWorkIdHidden" />
                                                <asp:HiddenField runat="server" ID="VehicleWireEditorWorkActionHidden" />
                                                <asp:Button runat="server" ID="AddWireButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddWireButton_Click" />
                                                <asp:Button runat="server" ID="CancelWireButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelWireButton_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <div class="row" runat="server" id="SearchInstallationTypePanel" visible="false">
                        <div class="large-2 columns">
                            Installation Type: 
                        </div>
                        <div class="large-5 columns">
                            <asp:DropDownList runat="server" ID="SearchInstallationTypeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="SearchInstallationTypeList_SelectedIndexChanged" />
                        </div>
                        <div class="large-5 columns">
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="WireListBatchPanel" Visible="false">
                        <asp:GridView ID="WireListBatch" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" OnRowDataBound="WireListBatch_RowDataBound" >
                            <Columns>
                                <asp:TemplateField HeaderText="Wire Function" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="WireFunctionList" Font-Size="11px" Height="25px"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Installation Type" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="InstallationTypeName" runat="server" Text='<%# Bind("InstallationTypeName") %>' Font-Size="11px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vehicle Color" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="ColorTxt" runat="server" Font-Size="11px" Height="25px" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CM7X00/ADS Color" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="VehicleColorTxt" runat="server" Font-Size="11px" Height="25px" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="LocationTxt" runat="server" Font-Size="11px" Height="25px" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pin Out" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="PinOutTxt" runat="server" Font-Size="11px" Height="25px" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Polarity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="PolarityTxt" runat="server" Font-Size="11px" Height="25px" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Polarity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:FileUpload runat="server" ID="AttachWiringFile1" />
                                        <asp:FileUpload runat="server" ID="AttachWiringFile2" />
                                        <asp:FileUpload runat="server" ID="AttachWiringFile3" />
                                        <asp:FileUpload runat="server" ID="AttachWiringFile4" />
                                        <asp:FileUpload runat="server" ID="AttachWiringFile5" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="table-header" />
                            <RowStyle Height="50px" />
                            <AlternatingRowStyle Height="50px" />
                        </asp:GridView>
                        <asp:Button runat="server" ID="SaveWireBatchButton" Text="Save" CssClass="button tiny bg-black radius" OnClick="SaveWireBatchButton_Click" />
                        <asp:Button runat="server" ID="CancelWireBatchButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelWireBatchButton_Click" />
                    </asp:Panel>
                    <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%"
                        OnRowDataBound="WireList_RowDataBound" OnSelectedIndexChanged="WireList_SelectedIndexChanged" OnRowDeleting="WireList_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Wire Function" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="WireFunctionNameLabel" runat="server" Text='<%# Bind("WireFunctionName") %>' Font-Size="11px"></asp:Label>
                                    <asp:HiddenField ID="VehicleWireFunctionId" runat="server" Value='<%# Bind("VehicleWireFunctionId") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="InstallationTypeName" HeaderText="Installation Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Polarity" HeaderText="Polarity" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                                ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:CommandField>
                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:CommandField>
                        </Columns>
                        <HeaderStyle CssClass="table-header" />
                    </asp:GridView>
                    <asp:GridView ID="WireListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                        OnRowDataBound="WireListEditor_RowDataBound" OnRowCommand="WireListEditor_RowCommand" 
                        Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                        <Columns>
                            <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                            <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:TemplateField HeaderText="Wire Function" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="WireFunctionNameLabel" runat="server" Text='<%# Bind("WireFunctionName") %>' Font-Size="11px"></asp:Label>
                                    <asp:HiddenField ID="VehicleWireFunctionId" runat="server" Value='<%# Bind("VehicleWireFunctionId") %>'></asp:HiddenField>
                                    <asp:HiddenField ID="VehicleWireFunctionEditorId" runat="server" Value='<%# Bind("VehicleWireFunctionEditorId") %>'></asp:HiddenField>
                                    <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                                    <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                                    <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="InstallationTypeName" HeaderText="Installation Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:BoundField DataField="Polarity" HeaderText="Polarity" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                            <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                    <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                            <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                            <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                        </Columns>
                        <HeaderStyle CssClass="table-header" />
                    </asp:GridView>
                    <asp:GridView ID="CommentList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%"
                        AllowPaging="true" OnPageIndexChanging="CommentList_PageIndexChanging" PageSize="5" 
                        Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>Comments</td></tr></table>'>
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="User" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="UpdatedDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="Comment" HeaderText="User Comment" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HtmlEncode="false" />
                        </Columns>
                        <HeaderStyle CssClass="table-header" />
                    </asp:GridView>
                    <div class="row" runat="server" id="InternalNotePanel" visible="false">
                        <div class="large-2 columns">
                            Internal Note:
                        </div>
                        <div class="large-9 columns">
                            <asp:TextBox runat="server" ID="InternalNoteTxt" Width="100%" Height="50px" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div class="large-1 columns">
                            <asp:Button runat="server" ID="SaveInternalNoteButton" CssClass="button tiny bg-black radius no-margin" Text="Save" OnClick="SaveInternalNoteButton_Click"></asp:Button>
                        </div>
                    </div>
                    <asp:GridView ID="InternalNoteList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%"
                        AllowPaging="true" OnPageIndexChanging="InternalNoteList_PageIndexChanging" PageSize="5" 
                        Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>Internal Note</td></tr></table>'>
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Admin" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="UpdatedDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="InternalNote" HeaderText="Internal Notes" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        </Columns>
                        <HeaderStyle CssClass="table-header" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="AddWireButton" />
                    <asp:PostBackTrigger ControlID="SaveWireBatchButton" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="content" runat="server" id="linksdisassembly">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddDisassemblyPanelButton" Text="(+) Add Row" OnClick="AddDisassemblyPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingDisassemblyFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddDisassemblyPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddDisassemblyTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="DisassemblyStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="DisassemblyStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Disassemblyeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Disassemblyeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachDisassemblyImage1" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete1" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachDisassemblyFileUrl1" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachDisassemblyImage2" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete2" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachDisassemblyFileUrl2" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachDisassemblyImage3" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete3" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachDisassemblyFileUrl3" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachDisassemblyImage4" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete4" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachDisassemblyFileUrl4" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachDisassemblyImage5" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachDisassemblyImageDelete5" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachDisassemblyFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceDisassemblyImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachDisassemblyFileUrl5" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireDisassemblyIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireDisassemblyEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireDisassemblyEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddDisassemblyButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddDisassemblyButton_Click" />
                                        <asp:Button runat="server" ID="CancelDisassemblyButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelDisassemblyButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="DisassemblyList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="DisassemblyList_RowDataBound" Width="100%" OnSelectedIndexChanged="DisassemblyList_SelectedIndexChanged" OnRowDeleting="DisassemblyList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireDisassemblyId" runat="server" Value='<%# Bind("VehicleWireDisassemblyId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="DisassemblyListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="DisassemblyListEditor_RowDataBound" OnRowCommand="DisassemblyListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireDisassemblyId" runat="server" Value='<%# Bind("VehicleWireDisassemblyId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="VehicleWireDisassemblyEditorId" runat="server" Value='<%# Bind("VehicleWireDisassemblyEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linksprep">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddPrepPanelButton" Text="(+) Add Row" OnClick="AddPrepPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingPrepFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddPrepPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddPrepTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="PrepStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="PrepStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Prepeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Prepeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachPrepImage1" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachPrepImageDelete1" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachPrepFileUrl1" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachPrepImage2" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachPrepImageDelete2" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachPrepFileUrl2" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachPrepImage3" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachPrepImageDelete3" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachPrepFileUrl3" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachPrepImage4" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachPrepImageDelete4" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachPrepFileUrl4" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachPrepImage5" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachPrepImageDelete5" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachPrepFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplacePrepImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachPrepFileUrl5" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWirePrepIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWirePrepEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWirePrepEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddPrepButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddPrepButton_Click" />
                                        <asp:Button runat="server" ID="CancelPrepButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelPrepButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="PrepList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="PrepList_RowDataBound" Width="100%"
                OnSelectedIndexChanged="PrepList_SelectedIndexChanged" OnRowDeleting="PrepList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePrepId" runat="server" Value='<%# Bind("VehicleWirePrepId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="PrepListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="PrepListEditor_RowDataBound" OnRowCommand="PrepListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePrepId" runat="server" Value='<%# Bind("VehicleWirePrepId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="VehicleWirePrepEditorId" runat="server" Value='<%# Bind("VehicleWirePrepEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linkrouting">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddRoutingPanelButton" Text="(+) Add Row" OnClick="AddRoutingPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingRoutingFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddRoutingPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddRoutingTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="RoutingStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="RoutingStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Routingeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Routingeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachRoutingImage1" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachRoutingImageDelete1" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachRoutingFileUrl1" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachRoutingImage2" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachRoutingImageDelete2" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachRoutingFileUrl2" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachRoutingImage3" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachRoutingImageDelete3" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachRoutingFileUrl3" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachRoutingImage4" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachRoutingImageDelete4" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachRoutingFileUrl4" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachRoutingImage5" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachRoutingImageDelete5" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachRoutingFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceRoutingImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachRoutingFileUrl5" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWirePlacementRoutingIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWirePlacementRoutingEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWirePlacementRoutingEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddRoutingButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddRoutingButton_Click" />
                                        <asp:Button runat="server" ID="CancelRoutingButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelRoutingButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="RoutingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="RoutingList_RowDataBound" Width="100%"
                OnSelectedIndexChanged="RoutingList_SelectedIndexChanged" OnRowDeleting="RoutingList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePlacementRoutingId" runat="server" Value='<%# Bind("VehicleWirePlacementRoutingId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="RoutingListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="RoutingListEditor_RowDataBound" OnRowCommand="RoutingListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWirePlacementRoutingId" runat="server" Value='<%# Bind("VehicleWirePlacementRoutingId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="VehicleWirePlacementRoutingEditorId" runat="server" Value='<%# Bind("VehicleWirePlacementRoutingEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linkprogramming">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddProgrammingPanelButton" Text="(+) Add Row" OnClick="AddProgrammingPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingProgrammingFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingProgrammingFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingProgrammingFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingProgrammingFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingProgrammingFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddProgrammingPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddProgrammingTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="ProgrammingStep" CssClass="col-md-2 control-label">Step</asp:Label>
                                    <div class="col-md-1">
                                        <asp:TextBox runat="server" ID="ProgrammingStep" CssClass="form-control" TextMode="Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Programmingeditor" CssClass="col-md-2 control-label">Note</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="Programmingeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachProgrammingImage1" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachProgrammingImageDelete1" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachProgrammingFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceProgrammingImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachProgrammingFileUrl1" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachProgrammingImage2" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachProgrammingImageDelete2" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachProgrammingFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceProgrammingImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachProgrammingFileUrl2" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachProgrammingImage3" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachProgrammingImageDelete3" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachProgrammingFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceProgrammingImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachProgrammingFileUrl3" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachProgrammingImage4" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachProgrammingImageDelete4" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachProgrammingFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceProgrammingImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachProgrammingFileUrl4" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachProgrammingImage5" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachProgrammingImageDelete5" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachProgrammingFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceProgrammingImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachProgrammingFileUrl5" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireProgrammingIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireProgrammingEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireProgrammingEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddProgrammingButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddProgrammingButton_Click" />
                                        <asp:Button runat="server" ID="CancelProgrammingButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelProgrammingButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="ProgrammingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="ProgrammingList_RowDataBound" Width="100%"
                OnSelectedIndexChanged="ProgrammingList_SelectedIndexChanged" OnRowDeleting="ProgrammingList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireProgrammingId" runat="server" Value='<%# Bind("VehicleWireProgrammingId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="ProgrammingListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="ProgrammingListEditor_RowDataBound" OnRowCommand="ProgrammingListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Step" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="StepLabel" runat="server" Text='<%# Bind("Step") %>' Font-Size="11px"></asp:Label>
                            <asp:HiddenField ID="VehicleWireProgrammingId" runat="server" Value='<%# Bind("VehicleWireProgrammingId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="VehicleWireProgrammingEditorId" runat="server" Value='<%# Bind("VehicleWireProgrammingEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="FBPanel">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddFBPanelButton" Text="(+) Add Row" OnClick="AddFBPanelButton_Click" Visible="false"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="ExistingFBFilename1" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename2" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename3" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename4" />
                    <asp:HiddenField runat="server" ID="ExistingFBFilename5" />
                </div>
            </div>
            <asp:Panel runat="server" ID="AddFBPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddFBTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="FBeditor" CssClass="col-md-2 control-label">Facebook</asp:Label>
                                    <div class="col-md-8">
                                        <CKEditor:CKEditorControl ID="FBeditor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="FBUrl" CssClass="col-md-2 control-label">URL</asp:Label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" ID="FBUrl" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachFBImage1" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachFBImageDelete1" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile1" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachFBFileUrl1" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachFBImage2" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachFBImageDelete2" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile2" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachFBFileUrl2" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachFBImage3" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachFBImageDelete3" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile3" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachFBFileUrl3" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachFBImage4" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachFBImageDelete4" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile4" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachFBFileUrl4" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Image runat="server" ID="AttachFBImage5" Visible="false" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:CheckBox runat="server" ID="AttachFBImageDelete5" Text="Delete?" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:FileUpload runat="server" ID="AttachFBFile5" />
                                            </div>
                                            <div class="small-3 columns">
                                                <asp:Label runat="server" ID="ReplaceFBImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-2 columns" style="padding-left: 0">
                                                Or URL:
                                            </div>
                                            <div class="small-10 columns">
                                                <asp:TextBox runat="server" ID="AttachFBFileUrl5" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleWireFacebookIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireFacebookEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleWireFacebookEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddFBButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddFBButton_Click" />
                                        <asp:Button runat="server" ID="CancelFBButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelFBButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="FBList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="FBList_RowDataBound" Width="100%" OnSelectedIndexChanged="FBList_SelectedIndexChanged" OnRowDeleting="FBList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Facebook" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Note") %>
                            <br />URL: <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("URL") %>'></asp:Literal>
                            <asp:HiddenField ID="VehicleWireFacebookId" runat="server" Value='<%# Bind("VehicleWireFacebookId") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="FBListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="FBListEditor_RowDataBound" OnRowCommand="FBListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Facebook" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Note") %>
                            <br />URL: <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("URL") %>'></asp:Literal>
                            <asp:HiddenField ID="VehicleWireFacebookId" runat="server" Value='<%# Bind("VehicleWireFacebookId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="VehicleWireFacebookEditorId" runat="server" Value='<%# Bind("VehicleWireFacebookEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="Image1Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach1")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image2Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach2")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image3Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach3")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image4Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach4")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="Image5Link" BorderWidth="0" NavigateUrl='#' ImageUrl='<%# Eval("Attach5")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="DocumentPanel">
            <div class="row">
                <div class="large-12 columns">
                    <asp:LinkButton runat="server" ID="AddDocumentPanelButton" Text="(+) Add Row" OnClick="AddDocumentPanelButton_Click" Visible="false"></asp:LinkButton>
                </div>
            </div>
            <asp:Panel runat="server" ID="AddDocumentPanel" Visible="false">
                <div class="box border-gray">
                    <div class="box-header bg-transparent bor">
                        <h3 class="box-title"><i></i>
                            <span style="color: black; font-size: medium">
                                <asp:Literal runat="server" ID="AddDocumentTitle"></asp:Literal></span></h3>
                    </div>
                    <div>
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Document</asp:Label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="small-9 columns" style="padding-left: 0">
                                                <asp:Label runat="server" ID="ExistingDocument"></asp:Label><asp:HiddenField runat="server" ID="CurrentDocumentAttachFile" />
                                                <asp:FileUpload runat="server" ID="AttachFile" />
                                                OR URL: <asp:TextBox runat="server" ID="AttachFileUrl" Width="300px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:HiddenField runat="server" ID="VehicleDocumentIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleDocumentEditorWorkIdHidden" />
                                        <asp:HiddenField runat="server" ID="VehicleDocumentEditorWorkActionHidden" />
                                        <asp:Button runat="server" ID="AddDocumentButton" Text="Add" CssClass="button tiny bg-black radius" OnClick="AddDocumentButton_Click" />
                                        <asp:Button runat="server" ID="CancelDocumentButton" Text="Cancel" CssClass="button tiny bg-black radius" OnClick="CancelDocumentButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:GridView ID="DocumentList" runat="server" AutoGenerateColumns="false" CssClass="demo"
                OnRowDataBound="DocumentList_RowDataBound" Width="100%" OnSelectedIndexChanged="DocumentList_SelectedIndexChanged" OnRowDeleting="DocumentList_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Document" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="VehicleDocumentId" runat="server" Value='<%# Bind("VehicleDocumentId") %>' />
                            <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                            <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                            <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT"
                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="50px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="DocumentListEditor" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" 
                OnRowDataBound="DocumentListEditor_RowDataBound" OnRowCommand="DocumentListEditor_RowCommand" 
                Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0" bgcolor="yellow"><tr><td>From Editor</td></tr></table>'>
                <Columns>
                    <asp:BoundField DataField="EditorWorkActionName" HeaderText="Type" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="ProfileName" HeaderText="Editor" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:BoundField DataField="EditorWorkDt" HeaderText="Date" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField DataField="StatusName" HeaderText="Status" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderText="Document" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                            <asp:HiddenField ID="VehicleDocumentId" runat="server" Value='<%# Bind("VehicleDocumentId") %>' />
                            <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                            <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                            <asp:HiddenField ID="VehicleDocumentEditorId" runat="server" Value='<%# Bind("VehicleDocumentEditorId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="StatusId" runat="server" Value='<%# Bind("StatusId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkId" runat="server" Value='<%# Bind("EditorWorkId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="EditorWorkAction" runat="server" Value='<%# Bind("EditorWorkAction") %>'></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="EditApprove" HeaderText="Edit & Approve" Text="Edit & Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="Approve" HeaderText="Approve" Text="Approve" />
                    <asp:ButtonField ButtonType="Link" CommandName="DeleteDeny" HeaderText="Delete" Text="Delete" />
                </Columns>
                <HeaderStyle CssClass="table-header" />
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="TSBPanel">
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <script type="text/javascript">
        $(function () {
        });
    </script>

</asp:Content>
