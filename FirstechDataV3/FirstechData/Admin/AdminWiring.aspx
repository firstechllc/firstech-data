﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminWiring.aspx.cs" Inherits="FirstechData.Admin.AdminWiring" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Information</li>
        <li>Wiring</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-flow-merge"></i>
                        <span>Wiring</span>
                        <asp:HiddenField runat="server" ID="MakeId" />
                        <asp:HiddenField runat="server" ID="ModelId" />
                        <asp:HiddenField runat="server" ID="YearId" />
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="form-group form-horizontal">
                            <label for="VehicleMakeList" class="col-sm-1 control-label">Make:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <label for="VehicleModelList" class="col-sm-1 control-label">Model:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <label for="VehicleYearList" class="col-sm-1 control-label">Year:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add New" OnClick="AddButton_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <asp:Label runat="server" id="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false" CssClass="footable"
                                OnSelectedIndexChanged="WireList_SelectedIndexChanged" OnRowDataBound="WireList_RowDataBound"  OnRowDeleting="WireList_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Wire Function" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" >
                                        <ItemTemplate>
                                            <asp:Label ID="WireFunctionNameLabel" runat="server" Text='<%# Bind("WireFunctionName") %>' Font-Size="11px"></asp:Label>
                                            <asp:HiddenField ID="VehicleWireFunctionId" runat="server" Value='<%# Bind("VehicleWireFunctionId") %>'></asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="InstallationTypeName" HeaderText="Installation Type" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:BoundField DataField="VehicleColor" HeaderText="Vehicle Color" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:BoundField DataField="Colour" HeaderText="CM7X00/ADS Color" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:BoundField DataField="Location" HeaderText="Location" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:BoundField DataField="PinOut" HeaderText="Pin Out" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:BoundField DataField="Polarity" HeaderText="Polarity" ItemStyle-Font-Size="11px" HeaderStyle-Font-Size="11px" />
                                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="EDIT" 
                                        ButtonType="Link" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"  ItemStyle-Width="50px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=WireList]').footable();
        });
    </script>
</asp:Content>
