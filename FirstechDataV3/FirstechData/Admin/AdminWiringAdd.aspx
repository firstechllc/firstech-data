﻿<%@ Page Title="Add Wiring" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminWiringAdd.aspx.cs" Inherits="FirstechData.Admin.AdminWiringAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Information</li>
        <li>Wiring</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-flow-merge"></i>
                <span><asp:Label runat="server" ID="TitleLabel"></asp:Label></span>
                <asp:HiddenField runat="server" ID="MakeId" />
                <asp:HiddenField runat="server" ID="ModelId" />
                <asp:HiddenField runat="server" ID="YearId" />
                <asp:HiddenField runat="server" ID="Id" />
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MakeLabel" CssClass="col-md-2 control-label">Make</asp:Label>
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="MakeLabel" CssClass="form-control no-border no-shadow" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="ModelLabel" CssClass="col-md-2 control-label">Model</asp:Label>
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="ModelLabel" CssClass="form-control no-border no-shadow" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="YearLabel" CssClass="col-md-2 control-label">Year</asp:Label>
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="YearLabel" CssClass="form-control no-border no-shadow" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="WireFunctionList" CssClass="col-md-2 control-label">Wire Function</asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="WireFunctionList" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="InstalltionTypeList" CssClass="col-md-2 control-label">Installtion Type</asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="InstalltionTypeList" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="VehicleColor" CssClass="col-md-2 control-label">Vehicle Color</asp:Label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="VehicleColor" CssClass="form-control row" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Color" CssClass="col-md-2 control-label">CM7X00/ADS Color</asp:Label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="Color" CssClass="form-control row" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Color" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The CM7X00/ADS Color field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Location" CssClass="col-md-2 control-label">Location</asp:Label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="Location" CssClass="form-control row" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Location" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Location field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="PinOut" CssClass="col-md-2 control-label">Pin Out</asp:Label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="PinOut" CssClass="form-control row" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Polarity" CssClass="col-md-2 control-label">Polarity</asp:Label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="Polarity" CssClass="form-control row" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Add" CssClass="button tiny bg-black radius" />
                        <asp:Button runat="server" OnClick="CancelButton_Click" ID="CancelButton" Text="Cancel" CssClass="button tiny bg-black radius" CausesValidation="false" />
                    </div>
                </div>
            </div>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>
        </div>
    </div>
</asp:Content>
