﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="SearchCounter.aspx.cs" Inherits="FirstechData.Admin.SearchCounter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Search Counter</b></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-sm-1" style="white-space: nowrap; ">
                Start Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="StartDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                End Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="EndDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                Top : 
            </div>
            <div class="col-sm-2">
                <asp:DropDownList runat="server" ID="TopList">
                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                    <asp:ListItem Text="100" Value="100"></asp:ListItem>
                    <asp:ListItem Text="500" Value="500"></asp:ListItem>
                    <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
                    <asp:ListItem Text="2000" Value="2000"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="col-sm-2">
                <asp:HiddenField runat="server" ID="RoleNameHidden" />
                <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchButton_Click"></asp:Button>
                <asp:Button runat="server" ID="ExportButton" CssClass="button tiny bg-gray radius" Text="Export" OnClick="ExportButton_Click"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>

    <div class="row">
        <asp:GridView ID="CounterList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="CounterList_RowDataBound"
            OnSelectedIndexChanged="CounterList_SelectedIndexChanged" OnRowCommand="CounterList_RowCommand">
            <Columns>
                <asp:BoundField DataField="Num" HeaderText="No" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:TemplateField HeaderText="Vehicle" SortExpression="Vehicle">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="VehicleLink" Text='<%# Bind("Vehicle") %>' ></asp:HyperLink>
                        <asp:HiddenField runat="server" ID="VehicleMakeModelYearId" Value='<%# Bind("VehicleMakeModelYearId") %>' />
                        <asp:HiddenField runat="server" ID="ReservedBy" Value='<%# Bind("ReservedBy") %>' />
                        <asp:HiddenField runat="server" ID="Reserved" Value='<%# Bind("Reserved") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Counter" HeaderText="Counter" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="UpdatedDt" HeaderText="Last Updated Date" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="Reserved" HeaderText="Reserved By" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                <asp:CommandField ShowSelectButton="true" HeaderText="Reserve" SelectText="Reserve" ButtonType="Link" ItemStyle-Width="50px" />
                <asp:TemplateField HeaderText="Counter" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <asp:TextBox runat="server" ID="SearchCountTxt" Width="50px" Text='<%# Bind("SearchCount") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField ButtonType="Link" CommandName="ChangeSearchCount" HeaderText="Update Counter" Text="Update" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
