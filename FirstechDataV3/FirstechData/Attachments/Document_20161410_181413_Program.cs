﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EPS.Model;
using System.Diagnostics;
using cm = System.Configuration.ConfigurationManager;

namespace EPS.Service.CheckEmailConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string bmk = "";
            try
            {
                bool debug = false;

                if (args.Length > 0 && args[0].Trim().ToUpper() == "-DEBUG")
                    debug = true;

                bmk = "EPS.Data.DALFacade df = new EPS.Data.DALFacade();";
                EPS.Data.DALFacade df = new EPS.Data.DALFacade();

                string LogFolderPath = cm.AppSettings["LogFolderPath"];
                System.IO.File.AppendAllText(@"D:\Temp\mail_service.log", "[CheckEmail]OnStart:" + DateTime.Now + Environment.NewLine);
                string logFileNm = "CheckEmail_" + EPS.Utils.Utility.GetMonthlyFileNm();

                string log = "";
                DataSet PracticeSet = df.eps_sel_Practice_Simple_ByEPS(1, true);
                foreach (DataRow dr in PracticeSet.Tables[0].Rows)
                {
                    int PracticeId = int.Parse(dr["PracticeID"].ToString());
                    PracticeInfo practice = df.eps_sel_Practice_Scalar(1, PracticeId);

                    DataSet MailSettings = df.eps_sel_EmailServerSetting_PracticeID(PracticeId);
                    string UserName = "";
                    string Password = "";
                    string ServerName = "";
                    int Port = 0;
                    foreach (DataRow maildr in MailSettings.Tables[0].Rows)
                    {
                        UserName = maildr["EmailEmail"].ToString();
                        Password = maildr["EmailPasswd"].ToString();
                        ServerName = maildr["PopServer"].ToString();
                        Port = int.Parse(maildr["PopPortNo"].ToString());
                    }

                    if (CheckPop(practice.PopServer, practice.EmailEmail, practice.EmailPasswd, practice.PopPortNo)
                        || CheckPop(ServerName, UserName, Password, Port))
                    {
                        System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "PracticeId:" + PracticeId + Environment.NewLine);
                        System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "practice.PopServer:" + practice.PopServer + Environment.NewLine);
                        System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "ServerName:" + ServerName + Environment.NewLine);
                        try
                        {
                            log += "[" + PracticeId + "] [Start: " + DateTime.Now.ToString() + "] ";
                            string result = df.eps_isrt_GetEmail(PracticeId, debug);
                            if (result.Trim() != "")
                            {
                                SendEmail("lmw21c@hotmail.com", "CheckEmailConsole Error", "Failed to run eps_isrt_GetEmail - PracticeId:" + PracticeId.ToString() + "<br>Error: " + result);
                                SendEmail("eps@epsdr.com", "CheckEmailConsole Error", "Failed to run eps_isrt_GetEmail - PracticeId:" + PracticeId.ToString() + "<br>Error: " + result);
                            }
                            log += " [End: " + DateTime.Now.ToString() + "] ";
                        }
                        catch (Exception ex)
                        {
                            System.IO.File.AppendAllText(@"D:\Temp\error.txt", "ERROR on " + DateTime.Now.ToString() + ": " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                        }
                    }
                }

                DataSet BillingCompanySet = df.eps_sel_BillingCompanyList(1);
                foreach (DataRow dr in BillingCompanySet.Tables[0].Rows)
                {
                    int BillingCompanyID = int.Parse(dr["BillingCompanyID"].ToString());
                    BillingCompanyInfo billingcompany = df.eps_sel_BillingCompany_Scalar(1, BillingCompanyID);

                    DataSet MailSettings = df.eps_sel_EmailServerSetting_BillingCompanyID(BillingCompanyID);
                    string UserName = "";
                    string Password = "";
                    string ServerName = "";
                    int Port = 0;
                    foreach (DataRow maildr in MailSettings.Tables[0].Rows)
                    {
                        string EmailServerSettingID = maildr["EmailServerSettingID"].ToString();
                        UserName = maildr["EmailEmail"].ToString();
                        Password = maildr["EmailPasswd"].ToString();
                        ServerName = maildr["PopServer"].ToString();
                        Port = int.Parse(maildr["PopPortNo"].ToString());
                        int RetryCount = int.Parse(maildr["RetryCount"].ToString());

                        if (RetryCount < 5)
                        {
                            if (CheckPop(billingcompany.PopServer, billingcompany.EmailEmail, billingcompany.EmailPasswd, billingcompany.PopPortNo)
                                || CheckPop(ServerName, UserName, Password, Port))
                            {
                                System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "BillingCompanyID:" + BillingCompanyID + Environment.NewLine);
                                System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "billingcompany.PopServer:" + billingcompany.PopServer + Environment.NewLine);
                                System.IO.File.AppendAllText(@"D:\Temp\email_debug.log", DateTime.Now + "ServerName:" + ServerName + Environment.NewLine);
                                try
                                {
                                    log += "[" + BillingCompanyID + "] [Start: " + DateTime.Now.ToString() + "] ";
                                    string result = df.eps_isrt_GetEmail_BillingCompany(BillingCompanyID, EmailServerSettingID, debug);
                                    if (result.Trim() != "")
                                    {
                                        if (result == "Failed to authenticate")
                                        {
                                            SendEmail("lmw21c@hotmail.com", "CheckEmailConsole Failed to authenticate", "Failed to authenticate - BillingDocumentID:" + BillingCompanyID.ToString() + ", EmailServerSettingID:" + EmailServerSettingID + "<br>Error: " + result);
                                            SendEmail("eps@epsdr.com", "CheckEmailConsole Failed to authenticate", "Failed to authenticate - BillingDocumentID:" + BillingCompanyID.ToString() + ", EmailServerSettingID:" + EmailServerSettingID + "<br>Error: " + result);

                                            RetryCount++;
                                            df.eps_upd_EmailServerSetting_RetryCount(int.Parse(EmailServerSettingID), RetryCount);
                                        }
                                        else
                                        {
                                            SendEmail("lmw21c@hotmail.com", "CheckEmailConsole Error", "Failed to run eps_isrt_GetEmail_BillingCompany - BillingDocumentID:" + BillingCompanyID.ToString() + "<br>Error: " + result);
                                            SendEmail("eps@epsdr.com", "CheckEmailConsole Error", "Failed to run eps_isrt_GetEmail_BillingCompany - BillingDocumentID:" + BillingCompanyID.ToString() + "<br>Error: " + result);
                                        }
                                    }
                                    log += " [End: " + DateTime.Now.ToString() + "] ";
                                }
                                catch (Exception ex)
                                {
                                    System.IO.File.AppendAllText(@"D:\Temp\error.txt", "ERROR on " + DateTime.Now.ToString() + ": " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                                }
                            }
                        }
                    }
                }

                EPS.Utils.Utility.ValidateDirectory(LogFolderPath);
                System.IO.File.AppendAllText(System.IO.Path.Combine(LogFolderPath, logFileNm), log + Environment.NewLine);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(@"D:\Temp\error.txt", "ERROR on " + DateTime.Now.ToString() + ": " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                throw ex;
                //string inputParam = this.ToString();
                //DLS3.Error.ExeptionHandler.LogAppError("", this.GetType().Namespace, this.GetType().Name, ex, DLS3.Diagnostics.Utils.GetMethodName(), bmk, "", inputParam, false, true);
            }
        }

        private static bool CheckPop(string PopServer, string EmailEmail, string EmailPassword, int PopPort)
        {
            bool ret = true;

            if (PopServer == null || PopServer == "")
                ret = false;
            else if (EmailEmail == null || EmailEmail == "")
                ret = false;
            else if (EmailPassword == null || EmailPassword == "")
                ret = false;
            else if (PopPort <= 0)
                ret = false;

            return ret;
        }

        protected static void SendEmail(string ToEmail, string Subject, string MailBody)
        {
            try
            {
                if (ToEmail != "")
                {
                    string Name = "Alert";
                    string Email = "test@hansolmed.com";
                    string EmailPasswd = "hansol";
                    string SMTPServer = "smtp.gmail.com";
                    int PortNo = 587;
                    int SSLNeed = 1;
                    int AuthNeed = 1;
                    if (Email != "")
                    {

                        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(SMTPServer.Trim(), PortNo);
                        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

                        System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(Email, Name);
                        message.From = fromAddress;
                        message.To.Add(ToEmail); //parm.Email);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = MailBody;

                        //smtpClient.Host = SMTPServer;
                        //smtpClient.Port = 465;
                        if (SSLNeed > 0)
                            smtpClient.EnableSsl = true;
                        smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Timeout = 20000;
                        if (AuthNeed > 0)
                            smtpClient.Credentials = new System.Net.NetworkCredential(Email.Trim(), EmailPasswd.Trim()); //Email, EmailPasswd);
                        smtpClient.Send(message);
                    }
                }
            }
            catch (Exception ex)
            {
                //ret[0] = "Error";
                //ret[1] = "Failed to send email. please check email address again:" + ex.ToString() + ":" + SMTPServer + ":" + Email + ":" + EmailPasswd;
            }
        }

    }
}
