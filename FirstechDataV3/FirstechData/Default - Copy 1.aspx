﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default - Copy 1.aspx.cs" Inherits="FirstechData._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span>Home</a></li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="row">
        <div class="large-12 columns">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <asp:ImageButton runat="server" ID="MainBannerImage1" Visible="false" />
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption1"></asp:Label>
                        </div>
                    </div>
                    <div class="item">
                        <asp:ImageButton runat="server" ID="MainBannerImage2" Visible="false" />
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption2"></asp:Label>
                        </div>
                    </div>
                    <div class="item">
                        <asp:ImageButton runat="server" ID="MainBannerImage3" Visible="false" />
                        <div class="carousel-caption">
                            <asp:Label runat="server" ID="MainBannerCaption3"></asp:Label>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="large-4 columns">

            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title">
                        <span><asp:Label runat="server" ID="LeftBannerCaption"></asp:Label></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <asp:ImageButton runat="server" ID="LeftAttachImage" Visible="false" />
                </div>
                <!-- end .timeline -->

            </div>
            <!-- /.box-body -->
        </div>


        <div class="large-4 columns">
            <!-- timeline item -->
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title">
                        <asp:Label runat="server" ID="MiddleBannerCaption"></asp:Label>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                    <asp:ImageButton runat="server" ID="MiddleAttachImage" Visible="false" />
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="large-4 columns">
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title">
                        <asp:Label runat="server" ID="RightBannerCaption"></asp:Label>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                    <asp:ImageButton runat="server" ID="RightAttachImage" Visible="false" />
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.box -->
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorLabel" />
        </p>
    </div>


</asp:Content>
