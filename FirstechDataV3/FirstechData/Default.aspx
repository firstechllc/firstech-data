﻿<%@ Page Title="Firstech Data" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstechData._Default" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .mymarginbottom {
            margin-bottom:5px;
        }

        .carousel-inner > .item > a > img {
            width: 100%;
        }

        .carouselwide {
            width: 100%;
            display: block;
        }

        .carouselnarrow {
            display: none;
        }

        @media (max-width: 1000px) {
            .carouselwide {
                display: none;
            }

            .carouselnarrow {
                width: 100%;
                display: block;
            }
        }

        .simple-marquee-container * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        }

        .simple-marquee-container {
            width: 100%;
            background: white;
            float: left;
            display: inline-block;
            overflow: hidden;
            box-sizing: border-box;
            height: 55px;
            position: relative;
            cursor: pointer;
            border-width: 1px;
            border-color: #dfdfdf;
            border-bottom-style: solid;
        }

        .marquee-sibling {
            padding: 0;
            background: white;
            width: 100px;
            height: 55px;
            line-height: 42px;
            font-size: 12px;
            font-weight: normal;
            color: black;
            text-align: center;
            float: left;
            left: 0;
            z-index: 1000;
            padding-top: 10px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .inner-box {
            height: 35px;
            background-color: black;
            color: white;
            line-height: 35px;
        }

        .marquee, *[class^="marquee"] {
            display: inline-block;
            white-space: nowrap;
            position: absolute;
        }

        .marquee {
            margin-left: 25%;
        }

        .marquee-content-items {
            display: inline-block;
            padding: 5px;
            margin: 0;
            height: 55px;
            position: relative;
        }

            .marquee-content-items li {
                display: inline-block;
                line-height: 35px;
                color: black;
                font-size:11px;
                margin-right: 20px;
                padding-right: 20px;
                border-right-color: #dfdfdf;
                border-right-width:1px;
                border-right-style: solid;
            }

                .marquee-content-items li:after {
                    margin-right: 15em;
                }

                .marquee-content-items li .title {
                    border:0; 
                    margin:0; 
                    padding:0;
                }
                .marquee-content-items li .detail {
                    border:0; 
                    margin:0; 
                    padding:0;
                    font-size:11px;
                }    
    </style>
    <div class="row">
        <div class="large-12 columns" style="max-height: 704px; max-width: 1280px;">
            <div id="carousel-example-generic" class="carousel slide carouselwide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <asp:Repeater runat="server" ID="CarouselIndicatorList" OnItemDataBound="CarouselList_ItemDataBound">
                        <ItemTemplate>
                            <li data-target="#carousel-example-generic" data-slide-to="<%# Container.ItemIndex %>" <%# (Container.ItemIndex == 0 ? "class=\"active\"" : "") %>></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <asp:Repeater runat="server" ID="CarouselList" OnItemDataBound="CarouselList_ItemDataBound">
                        <ItemTemplate>
                            <div class="item <%# (Container.ItemIndex == 0 ? "active" : "") %>" style="max-height: 704px; max-width: 1280px;">
                                <asp:HyperLink runat="server" ID="MainBannerLink">
                                    <asp:Image runat="server" ID="MainBannerImage" Visible="false" CssClass="CarouselCss" />
                                </asp:HyperLink>
                                <div class="carousel-caption">
                                    <asp:Label runat="server" ID="MainBannerCaption"></asp:Label>
                                    <asp:HiddenField runat="server" ID="BannerCaption" Value='<%# DataBinder.Eval(Container.DataItem, "BannerCaption") %>' />
                                    <asp:HiddenField runat="server" ID="BannerURL" Value='<%# DataBinder.Eval(Container.DataItem, "BannerURL") %>' />
                                    <asp:HiddenField runat="server" ID="BannerImage" Value='<%# DataBinder.Eval(Container.DataItem, "BannerImage") %>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div id="carousel-example-generic2" class="carousel slide carouselnarrow" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <asp:Repeater runat="server" ID="CarouselIndicatorList2" OnItemDataBound="CarouselList2_ItemDataBound">
                        <ItemTemplate>
                            <li data-target="#carousel-example-generic2" data-slide-to="<%# Container.ItemIndex %>" <%# (Container.ItemIndex == 0 ? "class=\"active\"" : "") %>></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <asp:Repeater runat="server" ID="CarouselList2" OnItemDataBound="CarouselList2_ItemDataBound">
                        <ItemTemplate>
                            <div class="item <%# (Container.ItemIndex == 0 ? "active" : "") %>" style="max-height: 704px; max-width: 1280px;">
                                <asp:HyperLink runat="server" ID="MainBannerLink">
                                    <asp:Image runat="server" ID="MainBannerImage" Visible="false" CssClass="CarouselCss" />
                                </asp:HyperLink>
                                <div class="carousel-caption">
                                    <asp:Label runat="server" ID="MainBannerCaption"></asp:Label>
                                    <asp:HiddenField runat="server" ID="BannerCaption" Value='<%# DataBinder.Eval(Container.DataItem, "BannerCaption") %>' />
                                    <asp:HiddenField runat="server" ID="BannerURL" Value='<%# DataBinder.Eval(Container.DataItem, "BannerURL") %>' />
                                    <asp:HiddenField runat="server" ID="BannerImageMobile" Value='<%# DataBinder.Eval(Container.DataItem, "BannerImageMobile") %>' />
                                    <asp:HiddenField runat="server" ID="BannerImage" Value='<%# DataBinder.Eval(Container.DataItem, "BannerImage") %>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns" style="max-height: 704px; max-width: 1280px;">
            <div class="simple-marquee-container">
                <div class="marquee-sibling">
                    <div class="inner-box">
                        <b><a style="color:white;" href="javascript:openNewFeedPopup()">News</a></b>
                    </div>
                </div>
                <div class="marquee">
                    <ul class="marquee-content-items">
                        <asp:Repeater runat="server" ID="NewsFeedList" OnItemDataBound="NewsFeedList_ItemDataBound">
                            <ItemTemplate>
                                <li><p class="title"><b><asp:HyperLink runat="server" ID="NewsLink" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ></asp:HyperLink></b><asp:HiddenField runat="server" ID="NewsfeedId" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' ></asp:HiddenField></p>
                                    <p class="detail"><asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UpdateDt") %>' ></asp:Label><asp:HiddenField ID="Loc" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Loc") %>' ></asp:HiddenField></p></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row" >
        <div class="large-12 columns" style="max-height: 704px; max-width: 1280px;">
    <div class="row" >
        <div class="large-4 medium-12 small-12 columns">
            <asp:Panel runat="server" ID="SubmitTipPanelAuth" Visible="false"><img src="Content/Images/submittip.jpg" data-toggle="modal" data-target="#myModal" style="cursor: pointer;" /></asp:Panel>
            <asp:Panel runat="server" ID="SubmitTipPanelNoAuth" ><a href="/Account/Login?ReturnUrl=%2F"><img src="Content/Images/submittip.jpg" /></a></asp:Panel>
        </div>
        <div class="large-4 medium-12 small-12 columns">
            <a href="https://www.youtube.com/channel/UCvLp0NC-DQnoPJVly7do5Kg" target="_blank"><img src="Content/Images/watchvideo.jpg" /></a>
        </div>
        <div class="large-4 medium-12 small-12 columns">
            <a href="https://www.facebook.com/groups/compustaridatalinksupportgroup/" target="_blank"><img src="Content/Images/techfeed.jpg" /></a>
        </div>
        </div>
        </div>
    </div>
    <div class="row">
        <!-- /.box -->
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorLabel" />
        </p>
    </div>
    <br />
    <asp:HiddenField runat="server" ID="SaveFolderHidden" />

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">&nbsp;&nbsp;&nbsp;Submit a Tip</h3>
                </div>
                <div class="modal-body" style="color:black;">
                    <div class="row">
                        <div class="small-12 columns">Send us vehicle information, images, or anything that can help us provide more data to your fellow installers.</div>
                    </div>
                    <hr style="margin-top:10px; margin-bottom:10px;" />
                    <div class="row">
                        <div class="small-12 columns"><b>Type of Tip</b></div>
                    </div>
                    <asp:RadioButtonList runat="server" ID="TypeOfTips" RepeatDirection="Horizontal" BorderWidth="0" Width="90%" CssClass="mymarginbottom">
                        <asp:ListItem Text="Image" Value="Image"></asp:ListItem>
                        <asp:ListItem Text="Document" Value="Document"></asp:ListItem>
                        <asp:ListItem Text="General" Value="General"></asp:ListItem>
                    </asp:RadioButtonList>
                    <hr style="margin-top:0px; margin-bottom:10px;" />
                    <div class="row">
                        <div class="small-12 columns"><b>Vehicle</b></div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <asp:TextBox runat="server" Width="100%" ID="TipYear" placeholder="Year" CssClass="mymarginbottom"></asp:TextBox>
                        </div>
                        <div class="small-6 columns">
                            <asp:TextBox runat="server" Width="100%" ID="TipMake" placeholder="Make" CssClass="mymarginbottom"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <asp:TextBox runat="server" Width="100%" ID="TipModel" placeholder="Model" CssClass="mymarginbottom"></asp:TextBox>
                        </div>
                        <div class="small-6 columns">
                            Key Style:
                            <asp:DropDownList runat="server" ID="KeyStyleList" CssClass="mymarginbottom" Width="140px">
                                <asp:ListItem Text="Standard Key" Value="Standard Key"></asp:ListItem>
                                <asp:ListItem Text="Push to Start" Value="Push to Start"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns"><b>File Upload</b>
                            <br />(Supported Formats .pdf, .jpg, .png, .gif, .zip, .doc, .xlsx, .csv)
                            <br /><asp:FileUpload runat="server" ID="TipFile" />
                        </div>
                    </div>
                    <hr style="margin-top:0px; margin-bottom:10px;" />
                    <div class="row">
                        <div class="small-12 columns"><b>Description</b>
                            <br /><asp:TextBox runat="server" ID="TipDesc" Width="100%" Rows="5" TextMode="MultiLine" CssClass="mymarginbottom"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button runat="server" ID="TipSaveButton" Text="Submit Tip" CssClass="btn btn-primary" OnClick="TipSaveButton_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="DefaultJSContent" ContentPlaceHolderID="JSContent" runat="server">
    <script>
        $(document).ready(function () {
            $('.simple-marquee-container').SimpleMarquee({
                // controls the speed at which the marquee moves
                duration: 2000000,
                // right margin between consecutive marquees
                padding: 0
               
            });
            $(".marquee-content-items").trigger('mouseenter');
            $(".marquee-content-items").trigger('mouseleave');
        });

        function openNewFeedPopup() {
            win = window.open("NewsfeedPopup", "Newsfeed", "width=800,height=800,menubar=no,location=no,toolbar=no,scrollbars=yes,resizable=yes")
            win.focus();
        }
    </script>
</asp:Content>
