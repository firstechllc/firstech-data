﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Newsfeed.aspx.cs" Inherits="FirstechData.Newsfeed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="row" style="margin-top: -20px">
                <div class="large-12 columns">
                    <div class="box">
                        <div class="box-header bg-transparent">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                            </div>
                            <h3 class="box-title"><i class="icon-document"></i>
                                <span style="color: black; font-size: medium"><b>NEWSFEED</b></span>
                            </h3>
                        </div>
                        <div class="box-body " style="display: block;">
                            <div class="row">
                                <div class="large-12 columns">
                                    <asp:ListView runat="server" ID="NewsFeedList" OnItemDataBound="NewsFeedList_ItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="large-12 columns">
                                                    <article class="reading-nest">
                                                        <div class="row">
                                                            <h4>
                                                                <asp:HyperLink runat="server" Target="_blank" ID="NFLink"></asp:HyperLink>
                                                                <asp:Label runat="server" ID="NFLabel"></asp:Label>
                                                                <asp:HiddenField runat="server" ID="NFUrl" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedUrl") %>' />
                                                                <asp:HiddenField runat="server" ID="NFTitle" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedTitle") %>' />
                                                                <asp:HiddenField runat="server" ID="NFImage" Value='<%# DataBinder.Eval(Container.DataItem, "NewsfeedImage") %>' />
                                                            </h4>
                                                        </div>
                                                        <div class="row">
                                                            <h6><%# DataBinder.Eval(Container.DataItem, "NewsfeedDt", "{0:MM/dd/yyyy}") %></h6>
                                                        </div>
                                                        <div class="row">
                                                            <div class="large-8 columns">
                                                                    <asp:Literal runat="server" ID="ContentLabel" Text='<%# DataBinder.Eval(Container.DataItem, "NewsfeedContent") %>' />
                                                            </div>
                                                            <div class="large-4 columns right">
                                                                <asp:Image runat="server" ID="ContentImage" />
                                                            </div>
                                                        </div>
                                                        <div class="row">&nbsp;&nbsp;&nbsp;</div>
                                                    </article>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <asp:DataPager ID="NewsFeedListPager" runat="server" PagedControlID="NewsFeedList" PageSize="10" OnPreRender="NewsFeedListPager_PreRender">
                                        <Fields>
                                            <asp:NumericPagerField ButtonType="Link" ButtonCount="10" />
                                        </Fields>
                                    </asp:DataPager>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="SaveFolderHidden" />
            <asp:HiddenField runat="server" ID="FullPathHidden" />
            <asp:HiddenField runat="server" ID="NewsfeedId" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
