﻿<%@ Page Title="Document" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="FirstechData.Document" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .FolderClass {
            width: 100%;
            background-color: rgb(59, 181, 222);
            padding: 6px 2px 6px 6px;
            /*
            padding-top: 15px;
            padding-bottom: 15px;
            */
            margin-bottom:10px;
        }
        .FolderClass a {
            background-color: rgb(59, 181, 222);
            color: white;
            padding: 0px 2px 2px 2px;
            font-size: 15px;
            vertical-align:middle;
        }
        .doccell td {
            margin: 1px;
            padding: 1px;
            vertical-align: middle;
            text-align: left;
        }
        .doccell td a img {
            display: inline-block;
            height:100%;
            vertical-align: middle;
            text-align: left;
        }
        .FileClass {
            padding-top: 15px;
        }
        .vertical-align {
            display: flex;
            align-items: center;
        }
        .imageSize img {
            max-width: 80px;
            max-height: 80px;
        }
    </style>
    <div class="box" style="margin-top:0px;">
        <div class="box-header bg-transparent">
            <h3 class="box-title"><i class="icon-document"></i>
                <span style="color: black; font-size: medium"><b>DOCUMENTS</b></span>
                <asp:HiddenField runat="server" ID="SaveFolderHidden" />
                <asp:HiddenField runat="server" ID="DocumentId" />
            </h3>
        </div>
        <div class="row">
            <div class="large-12 columns" style="padding-left:40px;"><asp:Label runat="server" ID="CurrentFolder" Visible="false"></asp:Label></div>
        </div>
        <div class="box-body " style="display: block; padding-top:5px;">
            <div class="row">
                <div class="large-4 medim-7 small-7 columns" style="height:40px;">
                    <asp:TextBox runat="server" ID="DocumentSearch" placeholder="Search Document"></asp:TextBox>
                </div>
                <div class="large-2 medim-3 small-3 columns" style="height:40px;">
                    <asp:Button runat="server" ID="SearchDocumentButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchDocumentButton_Click" />
                </div>
                <div class="large-6 medim-2 small-2 columns" style="height:40px;">
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns"><h5><asp:Literal runat="server" ID="CurrentFolderName"></asp:Literal></h5></div>
            </div>
            <div class="row" runat="server" id="linkdocument">
                <div class="large-5 columns">
                    <asp:ListView runat="server" ID="DocumentList" OnItemDataBound="DocumentList_ItemDataBound" >
                        <ItemTemplate>
                            <div class="row vertical-align">
                                <div class="large-3 medim-5 small-5 columns">
                                    <asp:HyperLink runat="server" ID="ThumbnailImage" BorderWidth="0" ToolTip='Wiring' CssClass="imageSize" ></asp:HyperLink>
                                    &nbsp;
                                </div>
                                <div class="large-7 medim-7 small-7 columns">
                                    <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0" ></asp:Label>
                                    <asp:HiddenField ID="DocumentIdHidden2" runat="server" Value='<%# Bind("DocumentId") %>' />
                                    <asp:HiddenField ID="DocumentType" runat="server" Value='<%# Bind("DocumentType") %>' />
                                    <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                                    <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                                </div>
                                <div class="large-2 medim-2 small-2 columns">&nbsp;</div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <a class="close">×</a>
        <ol class="indicator"></ol>
    </div>
    <script>
        document.getElementById('<%=linkdocument.ClientID%>').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {
                    index: link,
                    event: event,
                    onslide: function (index, slide) {
                        var link = this.list[index].getAttribute('data-link');
                        $(slide).children().attr('href', link)
                    }
                },
                linkdocument = this.getElementsByTagName('a');

            if (link.href.indexOf("/Document?") < 0 && link.href.indexOf("/Document_") < 0) {
                blueimp.Gallery(linkdocument, options);
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
