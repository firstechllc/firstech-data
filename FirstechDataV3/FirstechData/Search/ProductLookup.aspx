﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductLookup.aspx.cs" Inherits="FirstechData.ProductLookup" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media (min-width: 1080px) {
            .productwin {
                width:1080px;
            }
        }

        @media (min-width: 500px) {
            .titleleft {
                text-align:right;
            }
        }

    </style>
    <script>
        function OpenDocument(url)
        {
            window.open(url, '_newtab');
        }
    </script>
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box productwin" style="margin-top: 0px;">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                    </div>
                    <h3 class="box-title"><i class="icon-search"></i>
                        <span style="color: black; font-size: medium"><b>Product Lookup</b></span>
                    </h3>
                </div>
                <div class="box-body " style="display: block; padding-top: 5px; color: black;">
                    <div class="row">
                        <div class="large-4 medim-7 small-7 columns" style="height: 40px;">
                            <asp:TextBox runat="server" ID="ProductSearch" placeholder="Model, Part, FCC, or IC ID Number"></asp:TextBox>
                        </div>
                        <div class="large-2 medim-3 small-3 columns" style="height: 40px;">
                            <asp:Button runat="server" ID="ProductSearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="ProductSearchButton_Click" />
                        </div>
                        <div class="large-6 medim-2 small-2 columns" style="height: 40px;">
                            <asp:HiddenField runat="server" ID="ProductIdHidden" />
                            <asp:HiddenField runat="server" ID="ProductTypeIdHidden" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <asp:Label runat="server" ID="ErrorMsg" ForeColor="Blue" Visible="false"></asp:Label>
                            <br /><br />
                        </div>
                    </div>
                    <div class="row" runat="server" id="SearchListPanel" visible="false">
                        <div class="large-6 columns">
                            <asp:GridView ID="SearchList" runat="server" AutoGenerateColumns="false" CssClass="small-font" AllowPaging="false" OnRowDataBound="SearchListList_RowDataBound" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Model Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="Link"></asp:HyperLink>
                                            <asp:HiddenField ID="ModelNumber" runat="server" Value='<%# Bind("ModelNumber") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="ProductId" runat="server" Value='<%# Bind("ProductId") %>'></asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PartNumber" HeaderText="Part Number" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HtmlEncode="false" />
                                </Columns>
                                <HeaderStyle CssClass="table-header" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="row" runat="server" id="MainBrainPanel" visible="false">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-5 columns">
                                    <asp:Image runat="server" ID="ProductPictureView" />
                                </div>
                                <div class="large-7 columns">
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="BrainModelNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Model Number
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="BrainPartNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Part Number
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray; ">
                                            Status:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainStatus" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray; ">
                                            Available for Warranty:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainAvailableForWarranty" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Replacement Part Number:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:ListView runat="server" ID="BrainReplacementPartList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div>
                                                        <%# Eval("PartNumber") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Blade Compatible:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainBladeCompatible" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Drone Compatible:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainDroneCompatible" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Data Port(s):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainDataport" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Operating Voltage:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainOperatingVoltage" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Idle Current (mA):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainIdleCurrent" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            OperatingTemperature:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainOperatingTempF" ForeColor="Black"></asp:Label><br />
                                            <asp:Label runat="server" ID="BrainOperatingTempC" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Water Resistant:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="BrainWaterResistant" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns" style="text-align:right">
                                    <asp:Button runat="server" ID="ProductDocumentButton" CssClass="btn bg-black" Text="Product Documents" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">&nbsp;<br /><br /></div>
                            </div>
                            <div class="row">
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;" >
                                            Features
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Convenience:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="BrainConvenienceList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductBrainConvenienceName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Remote Start:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="BrainRemoteStartList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductBrainRemoteStartName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Alarm:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="BrainAlarmList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductBrainAlarmName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Installation:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="BrainInstallationList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductBrainInstallationName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;">
                                            Compatible Remotes
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" >
                                            Current:
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">2 Way:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="TwoWayRemoteList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">1 Way:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="OneWayRemoteList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" >
                                            Discontinued:
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">2 Way:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="TwoWayRemoteDisList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">1 Way:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="OneWayRemoteDisList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;">
                                            Accessories
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 columns">
                                            <asp:ListView runat="server" ID="BrainAccessoriesList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div style="width:95%; ">
                                                        <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="MainRemotePanel" visible="false">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-5 columns">
                                    <asp:Image runat="server" ID="ProductRemotePictureView" />
                                </div>
                                <div class="large-7 columns">
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="RemoteModelNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Model Number
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="RemotePartNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                           </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Part Number
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%">
                                                    <asp:Label runat="server" ID="RemoteTwoWayAntenna" Font-Size="20px" ForeColor="Black"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    2 Way Antenna
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%">
                                                    <asp:Label runat="server" ID="RemoteOneWayAntenna" Font-Size="20px" ForeColor="Black"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%">
                                                    1 Way Antenna
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>

                                    <asp:Panel runat="server" ID="AdminFccPanel1">
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            FCC ID (USA):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteFccIdUSA" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            IC ID (Canada):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteFccIdCanada" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    </asp:Panel>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Status:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteStatus" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Available for Warranty:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteAvailableForWarranty" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Replacement Part Number:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:ListView runat="server" ID="RemoteReplacementPartList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div>
                                                        <%# Eval("PartNumber") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Estimated Range (ft.):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteEstimatedRange" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Battery:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteBattery" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Operating Voltage:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteOperatingVoltage" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Estimated Battery Life (Days):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteEstimatedLife" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            OperatingTemperature:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteOperatingTempC" ForeColor="Black"></asp:Label><br />
                                            <asp:Label runat="server" ID="RemoteOperatingTempF" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Water Resistant:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="RemoteWaterResistant" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns" style="text-align:right">
                                    <asp:Button runat="server" ID="RemoteDocumentButton" CssClass="btn bg-black" Text="Product Documents" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">&nbsp;<br /><br /></div>
                            </div>
                            <div class="row">
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;" >
                                            Features
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Convenience:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteConvenienceList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductRemoteConvenienceName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Programming:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteProgrammingList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <%# Eval("ProductRemoteProgrammableName") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Installation:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                            <asp:ListView runat="server" ID="RemoteAuxiliaryList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div style="width:95%; ">
                                                        <%# Eval("ProductRemoteAuxiliaryName") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;">
                                            Compatible Brains
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Current:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteCompBrainCurrentList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Discontinued:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteCompBrainDisList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;">
                                            Compatible Antenna
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Current:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteAntennaCurrentList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Discontinued:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="RemoteAntennaDisList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="MainAntennaPanel" visible="false">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-5 columns">
                                    <asp:Image runat="server" ID="ProductAntennaPictureView" />
                                </div>
                                <div class="large-7 columns">
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%">
                                                    <asp:Label runat="server" ID="AntennaModelNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Model Number
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="AntennaPartNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Part Number
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%">
                                                    <asp:Label runat="server" ID="AntennaRequiredCable" Font-Size="20px" ForeColor="Black"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Required Antenna Cable
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                            </div>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>
                                    <asp:Panel runat="server" ID="AdminFccPanel2">
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            FCC ID (USA):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaFccIdUSA" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            IC ID (Canada):
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaFccIdCanada" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    </asp:Panel>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Status:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaStatus" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Available for Warranty:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaAvailableForWarranty" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Replacement Part Number:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:ListView runat="server" ID="AntennaReplacementPartList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div>
                                                        <%# Eval("PartNumber") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Operating Voltage:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaOperatingVoltage" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            OperatingTemperature:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AntennaOperatingTempF" ForeColor="Black"></asp:Label><br />
                                            <asp:Label runat="server" ID="AntennaOperatingTempC" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns" style="text-align:right">
                                    <asp:Button runat="server" ID="AntennaDocumentButton" CssClass="btn bg-black" Text="Product Documents" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">&nbsp;<br /><br /></div>
                            </div>
                            <div class="row">
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;" >
                                            Compatible Remotes
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Current:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AntennaCompRemoteCurrentList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Discontinued:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AntennaCompRemoteDisList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;">
                                            Compatible Brains
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Current:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AntennaCompBrainCurrentList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Discontinued:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AntennaCompBrainDispList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="MainAccessoriesPanel" visible="false">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-5 columns">
                                    <asp:Image runat="server" ID="ProductAccessoriesPictureView" />
                                </div>
                                <div class="large-7 columns">
                                    <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="AccessoriesModelNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Model Number
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; width:95%;">
                                                    <asp:Label runat="server" ID="AccessoriesPartNumber" Font-Bold="true" Font-Size="25px"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-12 columns" style="text-align: center; width:95%;">
                                                    Part Number
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">&nbsp;<br /><br /></div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Status:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AccessoriesStatus" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Available for Warranty:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AccessoriesAvailableForWarranty" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Replacement Part Number:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:ListView runat="server" ID="AccessoriesReplacementPartList">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div>
                                                        <%# Eval("PartNumber") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            Operating Voltage:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AccessoriesOperatingVoltage" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="large-4 small-12 columns titleleft" style="color: gray;">
                                            OperatingTemperature:
                                        </div>
                                        <div class="large-8 small-12 columns">
                                            <asp:Label runat="server" ID="AccessoriesOperatingTempF" ForeColor="Black"></asp:Label><br />
                                            <asp:Label runat="server" ID="AccessoriesOperatingTempC" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns" style="text-align:right">
                                    <asp:Button runat="server" ID="AccessoriesDocumentButton" CssClass="btn bg-black" Text="Product Documents" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">&nbsp;<br /><br /></div>
                            </div>
                            <div class="row">
                                <div class="large-6 small-12 columns">&nbsp;
                                </div>
                                <div class="large-6 small-12 columns">
                                    <div class="row">
                                        <div class="large-12 columns" style="border-bottom: 1px solid gray; text-align: center; color:gray;width:95%;">
                                            Compatible Brains
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Current:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AccessoriesCompBrainCurrentList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-6 small-12 columns">
                                            <div class="row">
                                                <div class="large-12 columns" style="color:gray;">Discontinued:</div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 small-1 columns">&nbsp;</div>
                                                <div class="large-11 small-8 columns">
                                                    <asp:ListView runat="server" ID="AccessoriesCompBrainDispList">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div style="width:95%; ">
                                                                <a href='ProductLookup?Id=<%# Eval("ProductId") %>'><%# Eval("PartNumber") %></a>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
