﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace FirstechData
{
    public class IPAddressRange
    {
        readonly AddressFamily addressFamily;
        readonly byte[] lowerBytes;
        readonly byte[] upperBytes;

        public IPAddressRange(IPAddress lower, IPAddress upper)
        {
            // Assert that lower.AddressFamily == upper.AddressFamily

            this.addressFamily = lower.AddressFamily;
            this.lowerBytes = lower.GetAddressBytes();
            this.upperBytes = upper.GetAddressBytes();
        }

        public bool IsInRange(IPAddress address)
        {
            if (address.AddressFamily != addressFamily)
            {
                return false;
            }

            byte[] addressBytes = address.GetAddressBytes();

            bool lowerBoundary = true, upperBoundary = true;

            for (int i = 0; i < this.lowerBytes.Length && 
                (lowerBoundary || upperBoundary); i++)
            {
                if ((lowerBoundary && addressBytes[i] < lowerBytes[i]) ||
                    (upperBoundary && addressBytes[i] > upperBytes[i]))
                {
                    return false;
                }

                lowerBoundary &= (addressBytes[i] == lowerBytes[i]);
                upperBoundary &= (addressBytes[i] == upperBytes[i]);
            }

            return true;
        }
        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

    }

    public partial class Search : System.Web.UI.Page
    {
        HashSet<string> AllowedIPs = new HashSet<string>()
        {
            "50.149.87.214",
            "198.22.123.14",
            "198.22.123.79",
            "198.22.123.103",
            "198.22.123.104",
            "198.22.123.105",
            "198.22.123.108",
            "198.22.123.109",
            "198.22.122.4",
            "70.60.1.174",
            "199.60.113.30",
        };

        IPAddressRange IPRange1 = new IPAddressRange(IPAddress.Parse("198.22.122.0"), IPAddress.Parse("198.22.122.24"));
        IPAddressRange IPRange2 = new IPAddressRange(IPAddress.Parse("168.94.245.0"), IPAddress.Parse("168.94.245.24"));
        IPAddressRange IPRange3 = new IPAddressRange(IPAddress.Parse("168.94.239.0"), IPAddress.Parse("168.94.239.24"));

        protected void Page_Load(object sender, EventArgs e)
        {
            string ipAddress = IPAddressRange.GetIPAddress();
            IPAddress ipaddr = IPAddress.Parse(ipAddress);

            if (!AllowedIPs.Contains(ipAddress) && !IPRange1.IsInRange(ipaddr) && !IPRange2.IsInRange(ipaddr) && !IPRange3.IsInRange(ipaddr) && !User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FSearch");
            }

            if (!IsPostBack)
            {
                try
                {
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    IList<string> roleNames = manager.GetRoles(User.Identity.GetUserId());
                    RoleNameHidden.Value = string.Join(",", roleNames);
                }
                catch(Exception)
                {
                    RoleNameHidden.Value = "";
                }

                Master.ChangeMenuCss("SearchByVehicleMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                LoadVehicleMake();
                int id;
                if (Request["Id"] != null && int.TryParse(Request["Id"].ToString(), out id))
                {
                    FindVehicle(id);
                }

                CommentImageUpload.Attributes["onchange"] = "imagepreview(this);";
            }

            registerpostback();
        }

        private void LoadVehicleMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct VehicleMakeId, VehicleMakeName "; 
                sql += "from ( ";
                sql += "   select b.VehicleMakeId, b.VehicleMakeName "; 
                sql += "   from dbo.VehicleMake b with (nolock)   ";
                sql += "   join dbo.VehicleMakeModelYear a WITH (NOLOCK) on a.VehicleMakeId = b.VehicleMakeId  ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=a.VehicleMakeModelYearId  ";
                sql += "   union all ";
                sql += "   select b.VehicleMakeId, b.VehicleMakeName ";
                sql += "   from dbo.VehicleMake b with (nolock)   ";
                sql += "   join dbo.VehicleMakeModelYear a WITH (NOLOCK) on a.VehicleMakeId = b.VehicleMakeId  ";
                sql += "   join dbo.VehicleLink l with (NOLOCK) on l.VehicleMakeModelYearId = a.VehicleMakeModelYearId ";
                sql += "   join (select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK)) w on w.VehicleMakeModelYearId=l.LinkVehicleMakeModelYearId  ";
                sql += ") tbl ";
                sql += "order by VehicleMakeName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("Make", "-1"));

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0 order by SortOrder ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();

                foreach(ListItem item in InstalltionTypeList.Items)
                {
                    if (item.Text.Trim() == "CM-7X00 w/ Blade AL")
                    {
                        DefaultInstallationTypeId.Value = item.Value;
                        break;
                    }
                }

                foreach (ListItem item in InstalltionTypeList.Items)
                {
                    if (item.Text.Trim() == "Vehicle Wiring")
                    {
                        DefaultInstallationTypeId2.Value = item.Value;
                        break;
                    }
                }

                if (DefaultInstallationTypeId.Value != "")
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId.Value;
                }
                else if (DefaultInstallationTypeId2.Value != "")
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId2.Value;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        private void FindVehicle(int id)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleModelId, VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = id;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    int VehicleMakeId, VehicleModelId, VehicleYear;

                    if (reader["VehicleMakeId"] != null && int.TryParse(reader["VehicleMakeId"].ToString(), out VehicleMakeId) &&
                        reader["VehicleModelId"] != null && int.TryParse(reader["VehicleModelId"].ToString(), out VehicleModelId) &&
                        reader["VehicleYear"] != null && int.TryParse(reader["VehicleYear"].ToString(), out VehicleYear))
                    {
                        VehicleMakeList.SelectedValue = VehicleMakeId.ToString();
                        ChangeVehicleMakeGetYear(VehicleYear);
                        ChangeVehicleYearGetModel(VehicleModelId);
                        SearchInfo();
                        ShowWireList();
                        ShowCommentList();
                    }
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeVehicleMakeGetYear(0);
        }

        private void ChangeVehicleMakeGetYear(int VehicleYear)
        {
            ClearError();
            ShowHideResult(false);
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                VehicleYearList.Items.Clear();
                VehicleModelList.Items.Clear();
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "proc_VehicleYearAvailableLoad";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("Year", "-1"));

                if (VehicleYear > 0)
                {
                    VehicleYearList.SelectedValue = VehicleYear.ToString();
                }

                VehicleModelList.Items.Clear();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowHideResult(false);
                return;
            }
            SearchInfo();
            ShowWireList();
            ShowCommentList();
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeVehicleYearGetModel(0);
        }

        private void ChangeVehicleYearGetModel(int VehicleModelId)
        { 
            ClearError();
            ShowHideResult(false);
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                VehicleModelList.Items.Clear();
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "proc_VehicleModelAvailableLoad";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                Cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("Model", "-1"));

                if (VehicleModelId > 0)
                {
                    VehicleModelList.SelectedValue = VehicleModelId.ToString();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void SearchInfo()
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                ShowHideResult(false);
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                ShowHideResult(false);
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                ShowHideResult(false);
                return;
            }

            ChangeVehicleTitle();
            TitlePanel.Visible = true;
            CommentInfoLabel.Visible = false;

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            bool isAdmin = false;
            var user = manager.FindByEmail(Context.User.Identity.GetUserName());
            if (user != null)
            {
                var roles = manager.GetRoles(user.Id);
                if (roles.Contains("Administrator"))
                {
                    isAdmin = true;
                }
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string SaveFolder = SaveFolderHidden.Value;


                string sql = "select VehicleMakeModelYearId, Picture1 ";
                sql += "from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear";
                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMakeModelYearIdHidden.Value = reader["VehicleMakeModelYearId"].ToString();
                    if (reader["Picture1"] != null && reader["Picture1"].ToString() != "")
                    {
                        VehiclePicture1.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                        VehiclePicture1.Visible = true;
                    }
                    else
                    {
                        VehiclePicture1.ImageUrl = "/Content/Images/empty.png";
                        VehiclePicture1.Visible = true;
                    }
                }
                else
                {
                    reader.Close();
                    VehiclePicture1.ImageUrl = "/Content/Images/empty.png";
                    VehiclePicture1.Visible = true;
                    return;
                }
                reader.Close();


                cmd = new SqlCommand("proc_VehicleUrgentMessageSearch", Con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    UrgentMessageLabel.Text = reader["Note"].ToString();
                    if (UrgentMessageLabel.Text != "")
                    {
                        UrgentMessagePanel.Visible = true;
                    }
                    else
                    {
                        UrgentMessagePanel.Visible = false;
                    }
                }
                else
                {
                    UrgentMessageLabel.Text = "";
                    UrgentMessagePanel.Visible = false;
                }
                reader.Close();

                // Profile Picture
                if (User.Identity.IsAuthenticated)
                {
                    cmd = new SqlCommand("proc_UserProfileLoad", Con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    reader = cmd.ExecuteReader();
                    if (reader.Read() && reader["ProfilePicture"].ToString() != "")
                    {
                        ProfilePicture.ImageUrl = SaveFolder + reader["ProfilePicture"].ToString();
                        UserProfilePictureFilepath.Value = SaveFolder + reader["ProfilePicture"].ToString();
                    }
                    else
                    {
                        ProfilePicture.ImageUrl = SaveFolder + "noprofile.png";
                        UserProfilePictureFilepath.Value = SaveFolder + "noprofile.png";
                    }
                    reader.Close();
                }




                SqlCommand Cmd = new SqlCommand("proc_VehiclePrepSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                PrepList.DataSource = ds;
                PrepList.DataBind();
                PrepList.Visible = true;

                if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
                {
                    PrepList.Attributes["data-page"] = "false";

                    /*
                    PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    */

                    PrepList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    PrepList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    PrepList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PrepTitle.Visible = true;

                    PrepHeader.Text = "Prep (" + ds.Tables[0].Rows.Count + ")";

                    linkspreptab.Visible = true;
                    linksprep.Visible = true;
                }
                else
                {
                    //PrepHeader.Text = "Prep (0)";
                    //PrepTitle.Visible = false;

                    linkspreptab.Visible = false;
                    linksprep.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleFacebookSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                FBList.DataSource = ds;
                FBList.DataBind();
                FBList.Visible = true;

                if (FBList != null && FBList.HeaderRow != null && FBList.HeaderRow.Cells.Count > 0)
                {
                    FBList.Attributes["data-page"] = "false";

                    FBList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    FBList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";

                    FBList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    FBList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";

                    FBList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    FBResultHeader.Text = "Facebook Result (" + ds.Tables[0].Rows.Count + ")";

                    FBPaneltab.Visible = true;
                    FBPanel.Visible = true;
                }
                else
                {
                    FBResultHeader.Text = "Facebook Result (0)";

                    FBPaneltab.Visible = false;
                    FBPanel.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleDocumentSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                DocumentList.DataSource = ds;
                DocumentList.DataBind();
                DocumentList.Visible = true;

                if (DocumentList != null && DocumentList.Items != null && DocumentList.Items.Count > 0)
                {
                    DocumentHeader.Text = "Documents (" + ds.Tables[0].Rows.Count + ")";

                    DocumentPanel.Visible = true;
                    DocumentPaneltab.Visible = true;
                }
                else
                {
                    //DocumentHeader.Text = "Prep (0)";
                    DocumentPanel.Visible = false;
                    DocumentPaneltab.Visible = false;
                }
                

                Cmd = new SqlCommand("proc_VehicleDisassemblySearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                DisassemblyList.DataSource = ds;
                DisassemblyList.DataBind();
                DisassemblyList.Visible = true;

                if (DisassemblyList != null && DisassemblyList.HeaderRow != null && DisassemblyList.HeaderRow.Cells.Count > 0)
                {
                    DisassemblyList.Attributes["data-page"] = "false";

                    /*
                    DisassemblyList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    DisassemblyList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    DisassemblyList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    */

                    DisassemblyList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    DisassemblyList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    DisassemblyList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    DisassemblyList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //DisassemblyTitle.Visible = true;

                    DisassemblyHeader.Text = "Disassembly (" + ds.Tables[0].Rows.Count + ")";

                    linksdisassemblytab.Visible = true;
                    linksdisassembly.Visible = true;
                }
                else
                {
                    DisassemblyHeader.Text = "Disassembly (0)";
                    //DisassemblyTitle.Visible = false;

                    linksdisassemblytab.Visible = false;
                    linksdisassembly.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleRoutingSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                RoutingList.DataSource = ds;
                RoutingList.DataBind();
                RoutingList.Visible = true;

                if (RoutingList != null && RoutingList.HeaderRow != null && RoutingList.HeaderRow.Cells.Count > 0)
                {
                    RoutingList.Attributes["data-page"] = "false";

                    RoutingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    RoutingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    RoutingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    RoutingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    RoutingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    RoutingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    RoutingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PlacementTitle.Visible = true;

                    RoutingHeader.Text = "Routing/Placement (" + ds.Tables[0].Rows.Count + ")";

                    linkroutingtab.Visible = true;
                    linkrouting.Visible = true;
                }
                else
                {
                    RoutingHeader.Text = "Routing/Placement (0)";
                    //PlacementTitle.Visible = false;

                    linkroutingtab.Visible = false;
                    linkrouting.Visible = false;
                }

                // Programming
                Cmd = new SqlCommand("proc_VehicleProgrammingSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                ProgrammingList.DataSource = ds;
                ProgrammingList.DataBind();
                ProgrammingList.Visible = true;

                if (ProgrammingList != null && ProgrammingList.HeaderRow != null && ProgrammingList.HeaderRow.Cells.Count > 0)
                {
                    ProgrammingList.Attributes["data-page"] = "false";

                    ProgrammingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    ProgrammingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    ProgrammingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    ProgrammingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    ProgrammingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    ProgrammingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    ProgrammingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    //PlacementTitle.Visible = true;

                    ProgrammingHeader.Text = "Programming (" + ds.Tables[0].Rows.Count + ")";

                    linkprogrammingtab.Visible = true;
                    linkprogramming.Visible = true;
                }
                else
                {
                    ProgrammingHeader.Text = "Programming (0)";
                    //PlacementTitle.Visible = false;

                    linkprogrammingtab.Visible = false;
                    linkprogramming.Visible = false;
                }

                // TSB
                try
                {
                    string url = "http://www.nhtsa.gov/webapi/api/Recalls/vehicle/modelyear/" + VehicleYearList.SelectedItem.Text + "/make/" + VehicleMakeList.SelectedItem.Text.ToLower() + "/model/" + VehicleModelList.SelectedItem.Text + "?format=xml";

                    WebClient wc = new WebClient();
                    Stream st = wc.OpenRead(url);
                    StreamReader sreader = new StreamReader(st);
                    string recallstr = sreader.ReadToEnd();

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(recallstr);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("ReportReceivedDate");
                    dt.Columns.Add("Component");
                    dt.Columns.Add("NHTSACampaignNumber");
                    dt.Columns.Add("Summary");
                    dt.Columns.Add("Conequence");
                    dt.Columns.Add("Remedy");
                    dt.Columns.Add("Notes");

                    if (xDoc.ChildNodes.Count > 0)
                    {
                        if (xDoc.ChildNodes[0].ChildNodes.Count >= 3)
                        {
                            foreach (XmlNode xNode in xDoc.ChildNodes[0].ChildNodes[2].ChildNodes)
                            {
                                DataRow row = dt.NewRow();
                                foreach (XmlNode xNode2 in xNode.ChildNodes)
                                {
                                    if (xNode2.Name == "ReportReceivedDate")
                                    {
                                        row[xNode2.Name] = xNode2.InnerText.Replace("T00:00:00", "");
                                    }
                                    else if (xNode2.Name == "Component"
                                        || xNode2.Name == "NHTSACampaignNumber"
                                        || xNode2.Name == "Summary"
                                        || xNode2.Name == "Conequence"
                                        || xNode2.Name == "Remedy"
                                        || xNode2.Name == "Notes")
                                    {
                                        row[xNode2.Name] = xNode2.InnerText;
                                    }
                                }
                                dt.Rows.Add(row);
                            }
                        }
                    }

                    TSBList.DataSource = dt;
                    TSBList.DataBind();

                    if (TSBList != null && TSBList.HeaderRow != null && TSBList.HeaderRow.Cells.Count > 0)
                    {
                        TSBList.Attributes["data-page"] = "false";

                        TSBList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                        TSBList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                        TSBList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                        TSBList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                        TSBList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                        TSBList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                        TSBList.HeaderRow.TableSection = TableRowSection.TableHeader;

                        TSBHeader.Text = "TSB (" + dt.Rows.Count + ")";

                        TSBPaneltab.Visible = true;
                        TSBPanel.Visible = true;
                    }
                    else
                    {
                        TSBHeader.Text = "TSB (0)";

                        TSBPaneltab.Visible = false;
                        TSBPanel.Visible = false;
                    }
                }
                catch (Exception) { }


                Videostab.Visible = false;
                VideosPanel.Visible = false;


                ShowHideResult(true);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowCommentList()
        {
            ClearError();

            //if (isAdmin)
            if (User.Identity.IsAuthenticated)
            {
                SqlConnection Con = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    Con = new SqlConnection(connStr);
                    Con.Open();

                    SqlCommand Cmd = new SqlCommand("proc_VehicleCommentLoad", Con);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                    Cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");

                    CommentList.DataSource = ds;
                    CommentList.DataBind();

                    if (CommentList != null && CommentList.HeaderRow != null && CommentList.HeaderRow.Cells.Count > 0)
                    {
                        CommentList.Attributes["data-page"] = "false";

                        CommentList.HeaderRow.Cells[0].Attributes["data-class"] = "phone";

                        CommentList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";

                        CommentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (Con != null)
                        Con.Close();
                }

                CommentPanel.Visible = true;
                CommentPanel2.Visible = true;
                CommentListPanel.Visible = true;
            }
            else
            {
                CommentPanel.Visible = false;
                CommentPanel2.Visible = false;
            }
        }

        protected void registerpostback()
        {
            foreach (GridViewRow row in CommentList.Rows)
            {
                Button SaveReplyCommentButton = ((Button)(row.FindControl("SaveReplyCommentButton")));
                if (SaveReplyCommentButton != null)
                {
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(SaveReplyCommentButton);
                    /*
                    PostBackTrigger trig = new PostBackTrigger();
                    trig.ControlID = SaveReplyCommentButton.UniqueID;
                    CommentListPanel.Triggers.Add(trig);
                    */
                }
            }
        }

        private void ShowReplyCommentList(int ParentVehicleCommentId, GridView List)
        {
            ClearError();

            //if (isAdmin)
            if (User.Identity.IsAuthenticated)
            {
                SqlConnection Con = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    Con = new SqlConnection(connStr);
                    Con.Open();

                    SqlCommand Cmd = new SqlCommand("proc_VehicleCommentLoad", Con);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                    Cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    Cmd.Parameters.Add("@ParentVehicleCommentId", SqlDbType.Int).Value = ParentVehicleCommentId;

                    SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                    DataSet ds = new DataSet();

                    adp.Fill(ds, "List");
                    DataView dv = new DataView(ds.Tables[0]);
                    dv.Sort = "VehicleCommentId asc";

                    List.DataSource = dv;
                    List.DataBind();

                    if (List != null && List.HeaderRow != null && List.HeaderRow.Cells.Count > 0)
                    {
                        List.Attributes["data-page"] = "false";

                        List.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                        List.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";

                        List.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (Con != null)
                        Con.Close();
                }
            }
        }


        private void ShowWireList()
        {
            ClearError();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string SaveFolder = SaveFolderHidden.Value;
                WireList.Columns[3].Visible = true;

                SqlCommand Cmd = new SqlCommand("proc_VehicleWireSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(DefaultInstallationTypeId.Value);
                Cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireList.DataSource = ds;
                WireList.DataBind();
                WireList.Visible = true;
                PrintVehicleWiringButton.Visible = true;

                if (ds.Tables[0].Rows.Count == 0)
                {
                    Cmd = new SqlCommand("proc_VehicleWireSearch", Con);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                    Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                    Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(DefaultInstallationTypeId2.Value);
                    Cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    adp = new SqlDataAdapter(Cmd);
                    ds = new DataSet();

                    adp.Fill(ds, "List");

                    WireList.DataSource = ds;
                    WireList.DataBind();
                    WireList.Visible = true;
                    PrintVehicleWiringButton.Visible = true;

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        InstalltionTypeList.SelectedValue = DefaultInstallationTypeId2.Value;

                        WireList.Columns[2].Visible = false;
                    }
                }
                else
                {
                    InstalltionTypeList.SelectedValue = DefaultInstallationTypeId.Value;
                }

                if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";

                    WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";

                    WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //WiringTitle.Visible = true;
                    WiringHeader.Text = "Wiring (" + ds.Tables[0].Rows.Count + ")";
                    PrintVehicleWiringButton.NavigateUrl = "PrintVehicleWiring.aspx?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue + "&InstId=" + InstalltionTypeList.SelectedValue;

                    WiringPaneltab.Visible = true;
                    WiringPanel.Visible = true;
                    InstalltionTypeList.Visible = true;
                }
                else
                {
                    WiringHeader.Text = "Wiring (0)";

                    InstalltionTypeList.Visible = false;
                }

                if (!(WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0) && NoteLabel.Text == "")
                {
                    WiringPaneltab.Visible = false;
                    WiringPanel.Visible = false;
                }

                Cmd = new SqlCommand("proc_VehicleWireNoteSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Note"] != null)
                    {
                        NoteLabel.Text = reader["Note"].ToString();
                        NoteLabel.Visible = true;
                        NotesPanel.Visible = true;
                    }
                    else
                    {
                        NoteLabel.Text = "";
                        NotesPanel.Visible = false;
                    }
                }
                else
                {
                    NoteLabel.Text = "";
                    NotesPanel.Visible = false;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        protected void DisassemblyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }
        protected void RoutingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        private void HideNoImageLink(GridViewRowEventArgs e)
        { 
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink a1 = ((HyperLink)(e.Row.FindControl("ImageLink1")));
                HyperLink a2 = ((HyperLink)(e.Row.FindControl("ImageLink2")));
                HyperLink a3 = ((HyperLink)(e.Row.FindControl("ImageLink3")));
                HyperLink a4 = ((HyperLink)(e.Row.FindControl("ImageLink4")));
                HyperLink a5 = ((HyperLink)(e.Row.FindControl("ImageLink5")));

                if (a1.NavigateUrl == "")
                {
                    a1.Visible = false;
                }
                if (a2.NavigateUrl == "")
                {
                    a2.Visible = false;
                }
                if (a3.NavigateUrl == "")
                {
                    a3.Visible = false;
                }
                if (a4.NavigateUrl == "")
                {
                    a4.Visible = false;
                }
                if (a5.NavigateUrl == "")
                {
                    a5.Visible = false;
                }
            }
        }

        protected void FBList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        private void ShowHideResult(bool show)
        {
            WireList.Visible = show;
            PrintVehicleWiringButton.Visible = show;
            NotesPanel.Visible = show;
            VehiclePicture1.Visible = show;
            NoteLabel.Visible = show;
            DisassemblyList.Visible = show;
            PrepList.Visible = show;
            RoutingList.Visible = show;
            ProgrammingList.Visible = show;
            FBList.Visible = show;
            DocumentList.Visible = show;
            CommentListPanel.Visible = show;
            WiringPanel.Visible = show;

            if (!show)
            {
                WiringHeader.Text = "Wiring";
                DisassemblyHeader.Text = "Disassembly";
                FBResultHeader.Text = "Facebook Result";
                PrepHeader.Text = "Prep";
                RoutingHeader.Text = "Routing/Placement";
                ProgrammingHeader.Text = "Programming";
                DocumentHeader.Text = "Documents";
                VehicleMakeModelYearIdHidden.Value = "";

                TitlePanel.Visible = false;
                UrgentMessagePanel.Visible = false;
                CommentPanel.Visible = false;
                CommentPanel2.Visible = false;
            }
        }

        protected void DocumentList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField VehicleDocumentId = e.Item.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Item.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Item.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Item.FindControl("DocumentLabel") as Label;
                    if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
                    {
                        DocumentLabel.Text = "<a href='" + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }
            }
        }

        protected void WireList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField LikeDisLike = ((HiddenField)(e.Row.FindControl("LikeDisLike")));
                LinkButton WireThumbUpButton = ((LinkButton)(e.Row.FindControl("WireThumbUpButton")));
                LinkButton WireThumbDownButton = ((LinkButton)(e.Row.FindControl("WireThumbDownButton")));

                if (LikeDisLike.Value == "1")
                {
                    WireThumbUpButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }
                else if (LikeDisLike.Value == "0")
                {
                    WireThumbDownButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }
            }
        }

        private void ChangeVehicleTitle()
        {
            CurrentVehicle.Text = VehicleMakeList.SelectedItem.Text + " " + VehicleModelList.SelectedItem.Text + " " + VehicleYearList.SelectedItem.Text;
        }

        protected void SaveCommentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (SaveComment(int.Parse(VehicleMakeModelYearIdHidden.Value), CommentTxt.Text.Trim(), 0, CommentImageUpload.PostedFile))
            {
                ShowCommentList();
            }
        }

        private void UpdateComment(int VehicleCommentId, string Comment)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand cmd = new SqlCommand("proc_VehicleCommentUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleCommentId", SqlDbType.Int).Value = VehicleCommentId;
                cmd.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = Comment;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        private bool SaveComment(int VehicleMakeModelYearId, string Comment, int ParentVehicleCommentId, HttpPostedFile postedFile)
        { 
            SqlConnection conn = null;
            try
            {
                string OriginalFilename = "";
                string Filename = "";

                if (postedFile != null)
                {
                    SaveFile(postedFile, "CommentImage", out OriginalFilename, out Filename);
                }

                if (Comment.Trim() == "" && Filename == "")
                {
                    if (postedFile != null)
                    {
                        ShowError("Please enter Comment or upload picture");
                    }
                    else
                    {
                        ShowError("Please enter Comment");
                    }
                    return false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand cmd = new SqlCommand("proc_VehicleCommentSave", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = VehicleMakeModelYearId;
                cmd.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = Comment;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ParentVehicleCommentId", SqlDbType.Int).Value = ParentVehicleCommentId;
                cmd.Parameters.Add("@OriginalImgFilename", SqlDbType.NVarChar, 500).Value = OriginalFilename;
                cmd.Parameters.Add("@ImgFilename", SqlDbType.NVarChar, 500).Value = Filename;

                cmd.ExecuteNonQuery();

                CommentTxt.Text = "";
                CommentInfoLabel.Visible = true;
                CommentInfoLabel.Text = "Your comment has been saved. Thanks.";
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());

                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
            return true;
        }

        protected void InstalltionTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!WiringPanel.Visible)
                return;
            if (InstalltionTypeList.Items.Count <= 0)
                return;
            if (VehicleMakeModelYearIdHidden.Value == "")
                return;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand Cmd = new SqlCommand("proc_VehicleWireSearch", conn);
                Cmd.CommandType = CommandType.StoredProcedure;

                string SaveFolder = SaveFolderHidden.Value;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = SaveFolder;
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireList.DataSource = ds;
                WireList.DataBind();
                WireList.Visible = true;
                PrintVehicleWiringButton.Visible = true;

                if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
                {
                    WireList.Attributes["data-page"] = "false";

                    WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    WireList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                    WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";

                    WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                    WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";

                    WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //WiringTitle.Visible = true;
                    WiringHeader.Text = "Wiring (" + ds.Tables[0].Rows.Count + ")";
                    PrintVehicleWiringButton.NavigateUrl = "PrintVehicleWiring.aspx?Make=" + VehicleMakeList.SelectedValue + "&Model=" + VehicleModelList.SelectedValue + "&Year=" + VehicleYearList.SelectedValue + "&InstId=" + InstalltionTypeList.SelectedValue;

                    WiringPaneltab.Visible = true;
                    WiringPanel.Visible = true;

                    if (InstalltionTypeList.SelectedValue == DefaultInstallationTypeId2.Value)
                    {
                        WireList.Columns[2].Visible = false;
                    }
                    else
                    {
                        WireList.Columns[2].Visible = true;
                    }
                }
                else
                {
                    WiringHeader.Text = "Wiring (0)";
                    //ShowError("No wiring data available");
                    //WiringTitle.Visible = false;
                }

                SqlCommand cmd = new SqlCommand("proc_VehicleWireNoteSearch", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstalltionTypeList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Note"] != null)
                    {
                        NoteLabel.Text = reader["Note"].ToString();
                        NoteLabel.Visible = true;
                    }
                    else
                    {
                        NoteLabel.Text = "";
                    }
                }
                else
                {
                    NoteLabel.Text = "";
                }
                reader.Close();


            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ProgrammingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
        }

        protected void WireList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            string VehicleWireFunctionId = (string)e.CommandArgument;
            if (e.CommandName != "ThumbUp" && e.CommandName != "ThumbDown")
            {
                return;
            }

            try
            {
                if (e.CommandName == "ThumbUp")
                {
                    ThumbWireFunction(int.Parse(VehicleWireFunctionId), 1);
                    ShowWireList();
                }
                else if (e.CommandName == "ThumbDown")
                {
                    ThumbWireFunction(int.Parse(VehicleWireFunctionId), 0);
                    ShowWireList();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
            }
        }

        private void ThumbWireFunction(int VehicleWireFunctionId, int LikeDisLike)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "proc_VehicleWireThumb";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = VehicleWireFunctionId;
                cmd.Parameters.Add("@LikeDisLike", SqlDbType.Int).Value = LikeDisLike;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void CommentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommentList.PageIndex = e.NewPageIndex;
            ShowCommentList();
        }

        protected void CommentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ProfilePictureFilepath = ((HiddenField)(e.Row.FindControl("ProfilePictureFilepath"))).Value;
                Image ProfilePicture = ((Image)(e.Row.FindControl("ProfilePicture")));

                if (ProfilePictureFilepath == "")
                    ProfilePictureFilepath = "noprofile.png";


                ProfilePicture.ImageUrl = SaveFolderHidden.Value + ProfilePictureFilepath;

                HiddenField LikeDisLike = ((HiddenField)(e.Row.FindControl("LikeDisLike")));
                LinkButton CommentThumbUpButton = ((LinkButton)(e.Row.FindControl("CommentThumbUpButton")));
                LinkButton CommentThumbDownButton = ((LinkButton)(e.Row.FindControl("CommentThumbDownButton")));

                if (LikeDisLike.Value == "1")
                {
                    CommentThumbUpButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }
                else if (LikeDisLike.Value == "0")
                {
                    CommentThumbDownButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }
                
                LinkButton ShowReplyButton = ((LinkButton)(e.Row.FindControl("ShowReplyButton")));
                Literal ShowReplySep = ((Literal)(e.Row.FindControl("ShowReplySep")));
                HiddenField NoOfReplies = ((HiddenField)(e.Row.FindControl("NoOfReplies")));
                Label ReplyCount = ((Label)(e.Row.FindControl("ReplyCount")));
                int NoOfRepliesInt;
                if (int.TryParse(NoOfReplies.Value, out NoOfRepliesInt))
                {
                    if (NoOfRepliesInt > 0)
                    {
                        ShowReplyButton.Visible = true;
                        ShowReplySep.Visible = true;
                        ReplyCount.Visible = true;
                    }
                    else
                    {
                        ShowReplyButton.Visible = false;
                        ShowReplySep.Visible = false;
                        ReplyCount.Visible = false;
                    }
                }
                else
                {
                    ShowReplyButton.Visible = false;
                    ShowReplySep.Visible = false;
                    ReplyCount.Visible = false;
                }

                Image UserProfilePicture = ((Image)(e.Row.FindControl("UserProfilePicture")));
                UserProfilePicture.ImageUrl = UserProfilePictureFilepath.Value;


                HiddenField VehicleCommentId = ((HiddenField)(e.Row.FindControl("VehicleCommentId")));
                if (ExpandReplyVehicleCommentId.Value == VehicleCommentId.Value)
                {
                    Panel ReplyCommentListPanel = (Panel)(e.Row.FindControl("ReplyCommentListPanel"));
                    GridView ReplyCommentList = (GridView)(e.Row.FindControl("ReplyCommentList"));

                    ShowReplyCommentList(int.Parse(VehicleCommentId.Value), ReplyCommentList);

                    ReplyCommentListPanel.Visible = true;
                    ShowReplyButton.Text = "Hide Replies";
                }

                
                HiddenField UpdatedBy = ((HiddenField)(e.Row.FindControl("UpdatedBy")));
                LinkButton EditButton = ((LinkButton)(e.Row.FindControl("EditButton")));
                if (UpdatedBy.Value != User.Identity.GetUserId())
                {
                    EditButton.Visible = false;
                }

                HiddenField IsReported = ((HiddenField)(e.Row.FindControl("IsReported")));
                Label Comment = ((Label)(e.Row.FindControl("Comment")));
                if (RoleNameHidden.Value.Contains("Administrator") && IsReported.Value == "1")
                {
                    Comment.ForeColor = System.Drawing.Color.Red;
                }

                HiddenField ImgFilename = ((HiddenField)(e.Row.FindControl("ImgFilename")));
                Image CommentImage = ((Image)(e.Row.FindControl("CommentImage")));
                if (ImgFilename.Value != "")
                {
                    CommentImage.ImageUrl = SaveFolderHidden.Value + ImgFilename.Value;
                    CommentImage.Visible = true;
                }
                else
                {
                    CommentImage.Visible = false;
                }

                FileUpload ReplyCommentImageUpload = ((FileUpload)(e.Row.FindControl("ReplyCommentImageUpload")));
                ReplyCommentImageUpload.Attributes["onchange"] = "replyimagepreview(this);";
            }
        }

        protected void CommentList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            try
            {
                if (e.CommandName == "ThumbUp")
                {
                    string VehicleCommentId = (string)e.CommandArgument;
                    ThumbComment(int.Parse(VehicleCommentId), 1);
                    ShowCommentList();
                }
                else if (e.CommandName == "ThumbDown")
                {
                    string VehicleCommentId = (string)e.CommandArgument;
                    ThumbComment(int.Parse(VehicleCommentId), 0);
                    ShowCommentList();
                }
                else if (e.CommandName == "EditComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    Panel EditCommentPanel = (Panel)(CommentList.Rows[index].FindControl("EditCommentPanel"));
                    Label CommentLabel = (Label)(CommentList.Rows[index].FindControl("Comment"));
                    TextBox EditCommentTxt = (TextBox)(CommentList.Rows[index].FindControl("EditCommentTxt"));

                    EditCommentTxt.Text = CommentLabel.Text;
                    EditCommentPanel.Visible = true;
                }
                else if (e.CommandName == "CancelEditComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    Panel EditCommentPanel = (Panel)(CommentList.Rows[index].FindControl("EditCommentPanel"));

                    EditCommentPanel.Visible = false;
                }
                else if (e.CommandName == "SaveEditComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    HiddenField EditVehicleCommentId = (HiddenField)(CommentList.Rows[index].FindControl("EditVehicleCommentId"));
                    TextBox EditCommentTxt = (TextBox)(CommentList.Rows[index].FindControl("EditCommentTxt"));
                    if (EditCommentTxt.Text.Trim() == "")
                    {
                        ShowError("Please enter comment.");
                        return;
                    }

                    UpdateComment(int.Parse(EditVehicleCommentId.Value), EditCommentTxt.Text.Trim());
                    ShowCommentList();
                    Panel EditCommentPanel = (Panel)(CommentList.Rows[index].FindControl("EditCommentPanel"));
                    EditCommentPanel.Visible = false;
                }
                else if (e.CommandName == "Reply")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    Panel ReplyCommentPanel = (Panel)(CommentList.Rows[index].FindControl("ReplyCommentPanel"));

                    ReplyCommentPanel.Visible = true;
                }
                
                else if (e.CommandName == "CancelReply")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    Panel ReplyCommentPanel = (Panel)(CommentList.Rows[index].FindControl("ReplyCommentPanel"));

                    ReplyCommentPanel.Visible = false;
                }
                else if (e.CommandName == "SaveReply")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    HiddenField ParentVehicleCommentId = (HiddenField)(CommentList.Rows[index].FindControl("VehicleCommentId"));
                    TextBox ReplyCommentTxt = (TextBox)(CommentList.Rows[index].FindControl("ReplyCommentTxt"));
                    FileUpload ReplyCommentImageUpload = (FileUpload)(CommentList.Rows[index].FindControl("ReplyCommentImageUpload"));

                    if (SaveComment(int.Parse(VehicleMakeModelYearIdHidden.Value), ReplyCommentTxt.Text.Trim(), int.Parse(ParentVehicleCommentId.Value), ReplyCommentImageUpload.PostedFile))
                    {
                        ExpandReplyVehicleCommentId.Value = ParentVehicleCommentId.Value;
                        ShowCommentList();
                    }
                }
                else if (e.CommandName == "ShowReplyList")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    LinkButton ShowReplyButton = (LinkButton)(CommentList.Rows[index].FindControl("ShowReplyButton"));
                    Panel ReplyCommentListPanel = (Panel)(CommentList.Rows[index].FindControl("ReplyCommentListPanel"));
                    HiddenField ParentVehicleCommentId = (HiddenField)(CommentList.Rows[index].FindControl("VehicleCommentId"));
                    GridView ReplyCommentList = (GridView)(CommentList.Rows[index].FindControl("ReplyCommentList"));

                    if (ShowReplyButton.Text == "Show Replies")
                    {
                        ShowReplyCommentList(int.Parse(ParentVehicleCommentId.Value), ReplyCommentList);

                        ReplyCommentListPanel.Visible = true;
                        ShowReplyButton.Text = "Hide Replies";
                    }
                    else
                    {
                        ReplyCommentListPanel.Visible = false;
                        ShowReplyButton.Text = "Show Replies";
                    }

                }
                else if (e.CommandName == "ReportComment")
                {
                    string VehicleCommentId = (string)e.CommandArgument;
                    ReportComment(int.Parse(VehicleCommentId));
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
            }
        }

        private void ThumbComment(int VehicleCommentId, int LikeDisLike)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "proc_VehicleCommentThumb";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@VehicleCommentId", SqlDbType.Int).Value = VehicleCommentId;
                cmd.Parameters.Add("@LikeDisLike", SqlDbType.Int).Value = LikeDisLike;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }
        private void ReportComment(int VehicleCommentId)
        {
            string VehicleMake = "", VehicleModel = "", VehicleYear = "", Comment = "", Name="", Email="";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "proc_VehicleCommentReport";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@VehicleCommentId", SqlDbType.Int).Value = VehicleCommentId;

                cmd.ExecuteNonQuery();

                sql = @"select vm.VehicleMakeName, vo.VehicleModelName, b.VehicleYear, a.Comment, u.FirstName + ' ' + u.LastName as Name, u.Email
                        from dbo.VehicleComment a WITH (NOLOCK) 
                        join dbo.VehicleMakeModelYear b WITH (NOLOCK) on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId 
                        join dbo.VehicleMake vm with (NOLOCK) on vm.VehicleMakeId = b.VehicleMakeId
                        join dbo.VehicleModel vo with (NOLOCK) on vo.VehicleModelId = b.VehicleModelId
                        join dbo.AspNetUsers u on a.UpdatedBy = u.Id
                        where VehicleCommentId = @VehicleCommentId
                     ";

                SqlCommand Cmd = new SqlCommand(sql, conn);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleCommentId", SqlDbType.Int).Value = VehicleCommentId;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMake = reader["VehicleMakeName"].ToString();
                    VehicleModel = reader["VehicleModelName"].ToString();
                    VehicleYear = reader["VehicleYear"].ToString();
                    Comment = reader["Comment"].ToString();
                    Name = reader["Name"].ToString();
                    Email = reader["Email"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }


            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            string ReportCommentEmail = System.Configuration.ConfigurationManager.AppSettings["ReportCommentEmail"];
            var ReportCommentUser = manager.FindByEmail(ReportCommentEmail);

            string subject = "Report Comment: " + VehicleMake + " " + VehicleModel + " " + VehicleYear;

            StringBuilder mailContent = new StringBuilder();
            mailContent.AppendLine("Make: " + VehicleMake + "<br/>");
            mailContent.AppendLine("Model: " + VehicleModel + "<br/>");
            mailContent.AppendLine("Year: " + VehicleYear + "<br/>");
            mailContent.AppendLine("Email: " + Email + "<br/>");
            mailContent.AppendLine("Name: " + Name + "<br/>");
            mailContent.AppendLine("Comment: " + Comment + "<br/>");
            mailContent.AppendLine("Reported by: " + User.Identity.GetUserName() + "<br/>");

            manager.SendEmail(ReportCommentUser.Id, subject, mailContent.ToString());
        }

        protected void ReplyCommentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ProfilePictureFilepath = ((HiddenField)(e.Row.FindControl("ProfilePictureFilepath"))).Value;
                Image ProfilePicture = ((Image)(e.Row.FindControl("ProfilePicture")));

                if (ProfilePictureFilepath == "")
                    ProfilePictureFilepath = "noprofile.png";

                ProfilePicture.ImageUrl = SaveFolderHidden.Value + ProfilePictureFilepath;

                HiddenField LikeDisLike = ((HiddenField)(e.Row.FindControl("LikeDisLike")));
                LinkButton CommentThumbUpButton = ((LinkButton)(e.Row.FindControl("CommentThumbUpButton")));
                LinkButton CommentThumbDownButton = ((LinkButton)(e.Row.FindControl("CommentThumbDownButton")));

                if (LikeDisLike.Value == "1")
                {
                    CommentThumbUpButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }
                else if (LikeDisLike.Value == "0")
                {
                    CommentThumbDownButton.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3bb5de");
                }

                HiddenField UpdatedBy = ((HiddenField)(e.Row.FindControl("UpdatedBy")));
                LinkButton EditReplyButton = ((LinkButton)(e.Row.FindControl("EditReplyButton")));
                if (UpdatedBy.Value != User.Identity.GetUserId() && EditReplyButton != null)
                {
                    EditReplyButton.Visible = false;
                }

                HiddenField IsReported = ((HiddenField)(e.Row.FindControl("IsReported")));
                Label Comment = ((Label)(e.Row.FindControl("Comment")));
                if (RoleNameHidden.Value.Contains("Administrator") && IsReported.Value == "1")
                {
                    Comment.ForeColor = System.Drawing.Color.Red;
                }

                HiddenField ImgFilename = ((HiddenField)(e.Row.FindControl("ImgFilename")));
                Image CommentImage = ((Image)(e.Row.FindControl("CommentImage")));
                if (ImgFilename.Value != "")
                {
                    CommentImage.ImageUrl = SaveFolderHidden.Value + ImgFilename.Value;
                    CommentImage.Visible = true;
                }
                else
                {
                    CommentImage.Visible = false;
                }
            }
        }

        protected void ReplyCommentList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            try
            {
                if (e.CommandName == "ThumbUp")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    HiddenField VehicleCommentId = (HiddenField)(List.Rows[index].FindControl("VehicleCommentId"));
                    HiddenField ParentVehicleCommentId = (HiddenField)(List.Rows[index].FindControl("ParentVehicleCommentId"));

                    ThumbComment(int.Parse(VehicleCommentId.Value), 1);
                    ExpandReplyVehicleCommentId.Value = ParentVehicleCommentId.Value;
                    ShowReplyCommentList(int.Parse(ParentVehicleCommentId.Value), (GridView)sender);
                }
                else if (e.CommandName == "ThumbDown")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    HiddenField VehicleCommentId = (HiddenField)(List.Rows[index].FindControl("VehicleCommentId"));
                    HiddenField ParentVehicleCommentId = (HiddenField)(List.Rows[index].FindControl("ParentVehicleCommentId"));
                    
                    ThumbComment(int.Parse(VehicleCommentId.Value), 0);
                    ExpandReplyVehicleCommentId.Value = ParentVehicleCommentId.Value;
                    ShowReplyCommentList(int.Parse(ParentVehicleCommentId.Value), (GridView)sender);
                }
                else if (e.CommandName == "EditReplyComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    Panel EditReplyCommentPanel = (Panel)(List.Rows[index].FindControl("EditReplyCommentPanel"));
                    Label CommentLabel = (Label)(List.Rows[index].FindControl("Comment"));
                    TextBox EditReplyCommentTxt = (TextBox)(List.Rows[index].FindControl("EditReplyCommentTxt"));

                    EditReplyCommentTxt.Text = CommentLabel.Text;
                    EditReplyCommentPanel.Visible = true;
                }
                else if (e.CommandName == "CancelEditReplyComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    Panel EditReplyCommentPanel = (Panel)(List.Rows[index].FindControl("EditReplyCommentPanel"));

                    EditReplyCommentPanel.Visible = false;
                }
                else if (e.CommandName == "SaveEditReplyComment")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    HiddenField EditReplyVehicleCommentId = (HiddenField)(List.Rows[index].FindControl("EditReplyVehicleCommentId"));
                    HiddenField ParentVehicleCommentId = (HiddenField)(List.Rows[index].FindControl("ParentVehicleCommentId"));
                    
                    TextBox EditReplyCommentTxt = (TextBox)(List.Rows[index].FindControl("EditReplyCommentTxt"));
                    if (EditReplyCommentTxt.Text.Trim() == "")
                    {
                        ShowError("Please enter comment.");
                        return;
                    }

                    UpdateComment(int.Parse(EditReplyVehicleCommentId.Value), EditReplyCommentTxt.Text.Trim());
                    ShowReplyCommentList(int.Parse(ParentVehicleCommentId.Value), List);

                    Panel EditReplyCommentPanel = (Panel)(List.Rows[index].FindControl("EditReplyCommentPanel"));
                    EditReplyCommentPanel.Visible = false;
                }
                else if (e.CommandName == "ReportComment")
                {
                    string VehicleCommentId = (string)e.CommandArgument;
                    ReportComment(int.Parse(VehicleCommentId));
                }

                e.Handled = true;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
            }
        }

        protected void SubmitCorrectionButton_Click(object sender, EventArgs e)
        {
            if (CorrectionDesc.Text.Trim() == "")
            {
                ShowError("Please enter correction");
                return;
            }
            else
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                string CorrectionEmail = System.Configuration.ConfigurationManager.AppSettings["CorrectionEmail"];
                var CorrectionUser = manager.FindByEmail(CorrectionEmail);

                string VehicleMake = "", VehicleModel = "", VehicleYear = "", WireFunctionName = "", InstallationTypeName = "";

                SqlConnection Con = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    Con = new SqlConnection(connStr);
                    Con.Open();

                    string sql = @"select vm.VehicleMakeName, vo.VehicleModelName, b.VehicleYear, c.WireFunctionName, InstallationTypeName = isnull(d.InstallationTypeName, '')
                        from dbo.VehicleWireFunction a WITH (NOLOCK) 
                        join dbo.VehicleMakeModelYear b WITH (NOLOCK) on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId 
                        join dbo.VehicleMake vm with (NOLOCK) on vm.VehicleMakeId = b.VehicleMakeId
                        join dbo.VehicleModel vo with (NOLOCK) on vo.VehicleModelId = b.VehicleModelId
                        join dbo.WireFunction c WITH (NOLOCK) on a.WireFunctionId = c.WireFunctionId 
                        left join dbo.InstallationType d WITH (NOLOCK) on a.InstallationTypeId = d.InstallationTypeId 
                        where VehicleWireFunctionId = @VehicleWireFunctionId
                     ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdHidden.Value);

                    SqlDataReader reader = Cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMake = reader["VehicleMakeName"].ToString();
                        VehicleModel = reader["VehicleModelName"].ToString();
                        VehicleYear = reader["VehicleYear"].ToString();
                        WireFunctionName = reader["WireFunctionName"].ToString();
                        InstallationTypeName = reader["InstallationTypeName"].ToString();
                    }
                    reader.Close();

                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (Con != null)
                        Con.Close();
                }


                string subject = "Wire Correction: " + WireFunctionName + " - " + VehicleMake + " " + VehicleModel + " " + VehicleYear;

                var user = manager.FindById(User.Identity.GetUserId());

                StringBuilder mailContent = new StringBuilder();
                if (user != null)
                {
                    mailContent.AppendLine("User Name: " + user.FirstName + " " + user.LastName + "<br/>");
                    mailContent.AppendLine("User Email: " + user.Email + "<br/>");
                }
                mailContent.AppendLine("Make: " + VehicleMake + "<br/>");
                mailContent.AppendLine("Model: " + VehicleModel + "<br/>");
                mailContent.AppendLine("Year: " + VehicleYear + "<br/>");
                mailContent.AppendLine("Wire: " + WireFunctionName + "<br/>");
                mailContent.AppendLine("Installation Type: " + InstallationTypeName + "<br/>");
                mailContent.AppendLine("Correction: " + CorrectionDesc.Text.Trim() + "<br/>");

                manager.SendEmail(CorrectionUser.Id, subject, mailContent.ToString());

                ThumbWireFunction(int.Parse(VehicleWireFunctionIdHidden.Value), 0);
                ShowWireList();
            }
        }

        private void SaveFile(HttpPostedFile PFile, string Prefix, out string OriginalFilename, out string FileName)
        {
            FileName = "";
            OriginalFilename = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                OriginalFilename = filename;
                filename = Prefix + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

    }
}