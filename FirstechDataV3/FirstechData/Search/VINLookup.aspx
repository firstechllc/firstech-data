﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VINLookup.aspx.cs" Inherits="FirstechData.VINLookup" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box" style="margin-top:0px;">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-search"></i>
                <span style="color: black; font-size: medium"><b>VIN Lookup</b></span>
            </h3>
        </div>
        <div class="box-body " style="display: block; padding-top:5px; color: black;">
            <div class="row">
                <div class="large-4 medim-7 small-7 columns" style="height:40px;">
                    <asp:TextBox runat="server" ID="VINSearch" placeholder="Search VIN"></asp:TextBox>
                </div>
                <div class="large-2 medim-3 small-3 columns" style="height:40px;">
                    <asp:Button runat="server" ID="VINSearchButton" CssClass="button tiny bg-black radius" Text="Decode" OnClick="VINSearchButton_Click" />
                </div>
                <div class="large-6 medim-2 small-2 columns" style="height:40px;">
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Label runat="server" ID="PhotoInfo"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="large-2 columns">Year:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="YearLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Make:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="MakeLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Model:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="ModelLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Trim:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="TrimLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Body:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="BodyLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Number of Doors:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="NoOfDoorsLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Transmission Type:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="TransmissionTypeLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Driven Wheels:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns"><asp:Label runat="server" ID="DrivenWheelsLabel"></asp:Label></div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Engine:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns">
                    <asp:Label runat="server" ID="EngineInfo"></asp:Label>
                </div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">TSB:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns">
                    <asp:Label runat="server" ID="TSBInfo"></asp:Label>
                </div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-2 columns">Recalls:</div>
                <div class="large-1 columns"></div>
                <div class="large-5 columns">
                    <asp:Label runat="server" ID="RecallInfo"></asp:Label>
                </div>
                <div class="large-4 columns"></div>
            </div>
            <div class="row">
                <div class="large-12 columns"><asp:Label runat="server" ID="ErrorMsg" ForeColor="Blue"></asp:Label></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
