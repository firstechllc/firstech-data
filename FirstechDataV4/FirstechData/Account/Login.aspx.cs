﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using FirstechData.Models;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;

namespace FirstechData.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];

            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (User.Identity.IsAuthenticated)
            {
                Response.Redirect("/");
            }
           
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                switch (result)
                {
                    case SignInStatus.Success:
                        var user = manager.FindByEmail(Email.Text);
                        var roles = manager.GetRoles(user.Id);
                        if (user != null && user.Approved)
                        {
                            /*
                            if (roles.Contains("Administrator"))
                            {
                                IdentityHelper.RedirectToReturnUrl("~/Admin/AdminUsers", Response);
                            }
                            {
                             */
                            if (CheckField(user.Id))
                            {
                                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                            }
                            else
                            {
                                IdentityHelper.RedirectToReturnUrl("/Account/Manage?required=1", Response);
                            }
                            //}
                        }
                        else
                        {
                            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                            FailureText.Text = "Your membership is still pending.";
                            ErrorMessage.Visible = true;
                        }
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }

        private bool CheckField(string Id)
        {
            SqlConnection conn = null;
            try
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "proc_UserProfileLoad";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = Id;

                string WhereBuyProduct = "";

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    WhereBuyProduct = reader["WhereBuyProduct"].ToString();
                }
                reader.Close();

                if (WhereBuyProduct != "")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

            return false;
        }
    }
}