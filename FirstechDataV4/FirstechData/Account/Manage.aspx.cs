﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using FirstechData.Models;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using System.IO;

namespace FirstechData.Account
{
    public partial class Manage : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        private bool HasPassword(ApplicationUserManager manager)
        {
            return manager.HasPassword(User.Identity.GetUserId());
        }

        public bool HasPhoneNumber { get; private set; }

        public bool TwoFactorEnabled { get; private set; }

        public bool TwoFactorBrowserRemembered { get; private set; }

        public int LoginsCount { get; set; }

        protected void Page_Load()
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            HasPhoneNumber = String.IsNullOrEmpty(manager.GetPhoneNumber(User.Identity.GetUserId()));

            // Enable this after setting up two-factor authentientication
            //PhoneNumber.Text = manager.GetPhoneNumber(User.Identity.GetUserId()) ?? String.Empty;

            TwoFactorEnabled = manager.GetTwoFactorEnabled(User.Identity.GetUserId());

            LoginsCount = manager.GetLogins(User.Identity.GetUserId()).Count;

            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;

            if (!IsPostBack)
            {
                var user = manager.FindById(User.Identity.GetUserId());
                Email.Text = user.Email;
                FirstName.Text = user.FirstName;
                LastName.Text = user.LastName;
                StoreName.Text = user.StoreName;
                Address.Text = user.Address;
                City.Text = user.City;
                State.Text = user.State;
                Zip.Text = user.ZipCode;

                // Determine the sections to render
                if (HasPassword(manager))
                {
                    ChangePassword.Visible = true;
                }
                else
                {
                    CreatePassword.Visible = true;
                    ChangePassword.Visible = false;
                }

                // Render success message
                var message = Request.QueryString["m"];
                if (message != null)
                {
                    // Strip the query string from action
                    Form.Action = ResolveUrl("~/Account/Manage");

                    SuccessMessage =
                        message == "ChangePwdSuccess" ? "Your password has been changed."
                        : message == "SetPwdSuccess" ? "Your password has been set."
                        : message == "RemoveLoginSuccess" ? "The account was removed."
                        : message == "AddPhoneNumberSuccess" ? "Phone number has been added"
                        : message == "RemovePhoneNumberSuccess" ? "Phone number was removed"
                        : String.Empty;
                    successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                }

                SqlConnection conn = null;
                try
                {
                    string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "proc_UserProfileLoad";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ProfileName.Text = reader["ProfileName"].ToString();
                        Country.Text = reader["Country"].ToString();
                        CurrentProfileName.Value = reader["ProfileName"].ToString();
                        ProfilePictureFilepath1.Value = reader["ProfilePicture"].ToString();

                        if (ProfilePictureFilepath1.Value != "")
                        {
                            ProfilePicture1.ImageUrl = SaveFolder + ProfilePictureFilepath1.Value;
                        }
                        else
                        {
                            ProfilePicture1.ImageUrl = SaveFolder + "noprofile.png";
                        }

                        WhereBuyProduct.Text = reader["WhereBuyProduct"].ToString();
                        //EditorMTCRepcode.Text = reader["EditorMTCRepcode"].ToString();
                    }
                    else
                    {
                        ProfilePictureFilepath1.Value = "";
                        ProfilePicture1.ImageUrl = SaveFolder + "noprofile.png";
                    }
                    reader.Close();

                    if (Request["required"] != null && Request["required"].ToString() == "1")
                    {
                        RequiredMessage.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Where do you buy your products from' field is missing. Please update.";
                        RequiredMessage.Visible = true;
                    }
                    else
                    {
                        RequiredMessage.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }

                /*
                IList<string> roleNames = manager.GetRoles(User.Identity.GetUserId());

                if (roleNames.Contains("Editor"))
                {
                    EditorPanel.Visible = true;
                }
                else
                {
                    EditorPanel.Visible = false;
                }
                */

            }
            else
            {
                RequiredMessage.Visible = false;
            }
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected void RemovePhone_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var result = manager.SetPhoneNumber(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return;
            }
            var user = manager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                Response.Redirect("/Account/Manage?m=RemovePhoneNumberSuccess");
            }
        }

        // DisableTwoFactorAuthentication
        protected void TwoFactorDisable_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            manager.SetTwoFactorEnabled(User.Identity.GetUserId(), false);

            Response.Redirect("/Account/Manage");
        }

        //EnableTwoFactorAuthentication 
        protected void TwoFactorEnable_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            manager.SetTwoFactorEnabled(User.Identity.GetUserId(), true);

            Response.Redirect("/Account/Manage");
        }

        protected void UpdateUser_Click(object sender, EventArgs e)
        {
            ClearError();

            if (CurrentProfileName.Value.Trim() != ProfileName.Text.Trim())
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "select * from dbo.UserProfile where ProfileName=@ProfileName";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProfileName", SqlDbType.NVarChar, 200).Value = ProfileName.Text.Trim();

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ShowError("Someone else has already chosen " + ProfileName.Text.Trim() + ". Please try another Profile Name");
                        reader.Close();
                        return;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(User.Identity.GetUserId());

            user.FirstName = FirstName.Text;
            user.LastName = LastName.Text;
            user.StoreName = StoreName.Text;
            user.Address = Address.Text;
            user.City = City.Text;
            user.State = State.Text;
            user.ZipCode = Zip.Text;

            IdentityResult result = manager.Update(user);
            if (result.Succeeded)
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string FileName1;
                SaveFile(ProfilePicture.PostedFile, FullPath, "Profile", out FileName1);

                if (FileName1 != "" && ProfilePictureFilepath1.Value != "")
                {
                    if (File.Exists(FullPath + ProfilePictureFilepath1.Value))
                    {
                        File.Delete(FullPath + ProfilePictureFilepath1.Value);
                    }
                }

                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "proc_UserProfileUpdate";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;
                    cmd.Parameters.Add("@ProfileName", SqlDbType.NVarChar, 200).Value = ProfileName.Text.Trim();
                    cmd.Parameters.Add("@ProfilePicture", SqlDbType.NVarChar, 500).Value = FileName1;
                    cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 50).Value = Country.Text.Trim();
                    cmd.Parameters.Add("@WhereBuyProduct", SqlDbType.NVarChar, 100).Value = WhereBuyProduct.Text.Trim();
                    /*
                    if (EditorPanel.Visible)
                    {
                        cmd.Parameters.Add("@EditorMTCRepcode", SqlDbType.NVarChar, 100).Value = EditorMTCRepcode.Text.Trim();
                    }
                    */
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }

                IdentityHelper.RedirectToReturnUrl("/", Response);
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, out string FileName)
        {
            FileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                filename = Prefix + "_" + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }
        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

    }
}