﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminAnnouncementAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    AnnouncementIdHidden.Value = Request["ID"];
                    ShowAnnouncement();
                    SaveButton.Text = "Update";
                    TitleLabel.Text = "Edit Announcement";
                    DeleteButton.Visible = true;
                }
                else
                {
                    SaveButton.Text = "Save";
                    TitleLabel.Text = "Add Announcement";
                    DeleteButton.Visible = false;
                }
            }
        }

        private void ShowAnnouncement()
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select AnnouncementId, AnnouncementTitle, AnnouncementContent, StartDate, EndDate, ";
                sql += "IsActive, UpdatedBy, UpdatedDt ";
                sql += "from dbo.Announcement ";
                sql += "where AnnouncementId = @AnnouncementId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@AnnouncementId", SqlDbType.Int).Value = int.Parse(AnnouncementIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    TitleTxt.Text = reader["AnnouncementTitle"].ToString();
                    editor.Text = reader["AnnouncementContent"].ToString();
                    StartDate.Text = string.Format("{0:MM/dd/yyyy}", reader["StartDate"]);
                    EndDate.Text = string.Format("{0:MM/dd/yyyy}", reader["EndDate"]);
                    if (reader["IsActive"].ToString() == "1")
                        ActiveChk.Checked = true;
                    else
                        ActiveChk.Checked = false;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "";
                if (AnnouncementIdHidden.Value == "")
                {
                    sql = "insert into dbo.Announcement (AnnouncementTitle, StartDate, EndDate, IsActive, AnnouncementContent, UpdatedBy, UpdatedDt) ";
                    sql += "select @AnnouncementTitle, @StartDate, @EndDate, @IsActive, @AnnouncementContent, @UpdatedBy, getdate()";
                }
                else
                {
                    sql = "update dbo.Announcement set AnnouncementTitle=@AnnouncementTitle, StartDate=@StartDate, EndDate=@EndDate, ";
                    sql += "IsActive=@IsActive, AnnouncementContent=@AnnouncementContent, UpdatedBy=@UpdatedBy, UpdatedDt=getdate() ";
                    sql += "where AnnouncementId = @AnnouncementId ";
                }

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@AnnouncementTitle", SqlDbType.NVarChar, 200).Value = TitleTxt.Text.Trim();
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DateTime.Parse(StartDate.Text.Trim());
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = DateTime.Parse(EndDate.Text.Trim());
                if (ActiveChk.Checked)
                    cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@AnnouncementContent", SqlDbType.NText).Value = editor.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                if (AnnouncementIdHidden.Value != "")
                {
                    cmd.Parameters.Add("@AnnouncementId", SqlDbType.Int).Value = int.Parse(AnnouncementIdHidden.Value);
                }

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminAnnouncement");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminAnnouncement");
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                if (AnnouncementIdHidden.Value != "")
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "delete dbo.Announcement ";
                    sql += "where AnnouncementId = @AnnouncementId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@AnnouncementId", SqlDbType.Int).Value = int.Parse(AnnouncementIdHidden.Value);

                    cmd.ExecuteNonQuery();

                    Response.Redirect("~/Admin/AdminAnnouncement");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }
    }
}