﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminHomeBanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                Master.ChangeMenuCss("HomepageMenu");

                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                FullPathHidden.Value = HttpContext.Current.Server.MapPath(SaveFolderHidden.Value);
                ShowHomeBanner();
            }
        }

        private void ShowHomeBanner()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select BannerId, BannerCaption, BannerImage, BannerImageMobile, BannerURL, SortOrder ";
                sql += "from dbo.Banner ";
                sql += "where BannerLocation = @BannerLocation ";
                sql += "order by SortOrder";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@BannerLocation", SqlDbType.Int).Value = 1;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                BannerList.DataSource = ds;
                BannerList.DataBind();

                if (BannerList != null && BannerList.HeaderRow != null && BannerList.HeaderRow.Cells.Count > 0)
                {
                    BannerList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    BannerList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    BannerList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    BannerList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                    BannerList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                    BannerList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                    BannerList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";

                    BannerList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            AddBannerTitle.Text = "Add Banner";
            AddBannerButton.Text = "Add";
            ThisBannerIdHidden.Value = "";
            //ExistingBanner.Text = "";
            BannerCaption.Text = "";
            BannerURL.Text = "";

            AddBannerPanel.Visible = true;
        }

        protected void CancelBannerButton_Click(object sender, EventArgs e)
        {
            AddBannerPanel.Visible = false;
        }

        protected void AddBannerButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (BannerCaption.Text.Trim() == "")
            {
                ShowError("Please enter Banner Caption");
                return;
            }
            if (BannerURL.Text.Trim() == "")
            {
                ShowError("Please enter Banner URL");
                return;
            }

            int order = 1;
            string ImageFileName = "";
            string FileName1 = "";
            SaveFile(AttachFile.PostedFile, "Banner", out ImageFileName, out FileName1);

            string ImageFileNameMobile = "";
            string FileNameMobile = "";
            SaveFile(AttachFileMobile.PostedFile, "BannerMobile", out ImageFileNameMobile, out FileNameMobile);

            if (ThisBannerIdHidden.Value == "" && FileName1 == "")
            {
                ShowError("Please upload Banner Image");
                return;
            }
            if (!int.TryParse(BannerOrder.Text.Trim(), out order))
            {
                ShowError("Please enter valid Order");
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (ThisBannerIdHidden.Value == "")
                {
                    string sql = "insert into dbo.Banner ";
                    sql += "(BannerLocation, BannerCaption, BannerURL, BannerImage, BannerImageMobile, UpdatedBy, UpdatedDt, SortOrder) ";
                    sql += "select @BannerLocation, @BannerCaption, @BannerURL, @BannerImage, @BannerImageMobile, @UpdatedBy, getdate(), @SortOrder ";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@BannerLocation", SqlDbType.Int).Value = 1;
                    Cmd.Parameters.Add("@BannerCaption", SqlDbType.NVarChar, 500).Value = BannerCaption.Text.Trim();
                    Cmd.Parameters.Add("@BannerURL", SqlDbType.NVarChar, 1000).Value = BannerURL.Text.Trim();
                    Cmd.Parameters.Add("@BannerImage", SqlDbType.NVarChar, 500).Value = FileName1;
                    Cmd.Parameters.Add("@BannerImageMobile", SqlDbType.NVarChar, 500).Value = FileNameMobile;
                    Cmd.Parameters.Add("@SortOrder", SqlDbType.Int).Value = order;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    if (FileName1 != "")
                    {
                        string query = "select BannerImage from dbo.Banner where BannerId = @BannerId";
                        SqlCommand Cmd2 = new SqlCommand(query, Con);
                        Cmd2.CommandType = CommandType.Text;

                        Cmd2.Parameters.Add("@BannerId", SqlDbType.Int).Value = int.Parse(ThisBannerIdHidden.Value);

                        SqlDataReader reader = Cmd2.ExecuteReader();
                        if(reader.Read())
                        {
                            if (reader["BannerImage"] != null && reader["BannerImage"].ToString() != "")
                            {
                                string FilePath = FullPathHidden.Value + reader["BannerImage"].ToString();
                                if (File.Exists(FilePath))
                                {
                                    File.Delete(FilePath);
                                }
                            }
                        }
                        reader.Close();
                    }
                    if (FileNameMobile != "")
                    {
                        string query = "select BannerImageMobile from dbo.Banner where BannerId = @BannerId";
                        SqlCommand Cmd2 = new SqlCommand(query, Con);
                        Cmd2.CommandType = CommandType.Text;

                        Cmd2.Parameters.Add("@BannerId", SqlDbType.Int).Value = int.Parse(ThisBannerIdHidden.Value);

                        SqlDataReader reader = Cmd2.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["BannerImageMobile"] != null && reader["BannerImageMobile"].ToString() != "")
                            {
                                string FilePath = FullPathHidden.Value + reader["BannerImageMobile"].ToString();
                                if (File.Exists(FilePath))
                                {
                                    File.Delete(FilePath);
                                }
                            }
                        }
                        reader.Close();
                    }

                    string sql = "update dbo.Banner ";
                    if (FileName1 != "" && FileNameMobile != "")
                    {
                        sql += "set BannerCaption=@BannerCaption, BannerURL = @BannerURL, BannerImage=@BannerImage, BannerImageMobile=@BannerImageMobile, UpdatedBy=@UpdatedBy, SortOrder=@SortOrder, UpdatedDt=getdate() ";
                    }
                    else if (FileName1 != "")
                    {
                        sql += "set BannerCaption=@BannerCaption, BannerURL = @BannerURL, BannerImage=@BannerImage, UpdatedBy=@UpdatedBy, SortOrder=@SortOrder, UpdatedDt=getdate() ";
                    }
                    else if (FileNameMobile != "")
                    {
                        sql += "set BannerCaption=@BannerCaption, BannerURL = @BannerURL, BannerImageMobile=@BannerImageMobile, UpdatedBy=@UpdatedBy, SortOrder=@SortOrder, UpdatedDt=getdate() ";
                    }
                    else
                    {
                        sql += "set BannerCaption=@BannerCaption, BannerURL = @BannerURL, UpdatedBy=@UpdatedBy, SortOrder=@SortOrder, UpdatedDt=getdate() ";
                    }
                    sql += "where BannerId = @BannerId";

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.Parameters.Add("@BannerCaption", SqlDbType.NVarChar, 500).Value = BannerCaption.Text.Trim();
                    Cmd.Parameters.Add("@BannerURL", SqlDbType.NVarChar, 1000).Value = BannerURL.Text.Trim();
                    if (FileName1 != "" && FileNameMobile != "")
                    {
                        Cmd.Parameters.Add("@BannerImage", SqlDbType.NVarChar, 500).Value = FileName1;
                        Cmd.Parameters.Add("@BannerImageMobile", SqlDbType.NVarChar, 500).Value = FileNameMobile;
                    }
                    else if (FileName1 != "")
                    {
                        Cmd.Parameters.Add("@BannerImage", SqlDbType.NVarChar, 500).Value = FileName1;
                    }
                    else if (FileNameMobile != "")
                    {
                        Cmd.Parameters.Add("@BannerImageMobile", SqlDbType.NVarChar, 500).Value = FileNameMobile;
                    }
                    Cmd.Parameters.Add("@SortOrder", SqlDbType.Int).Value = order;
                    Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    Cmd.Parameters.Add("@BannerId", SqlDbType.Int).Value = int.Parse(ThisBannerIdHidden.Value);

                    Cmd.ExecuteNonQuery();
                }

                BannerOrder.Text = "1";
                AddBannerPanel.Visible = false;
                ShowHomeBanner();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void SaveFile(HttpPostedFile PFile, string Prefix, out string ImageFileName, out string FileName)
        {
            FileName = "";
            ImageFileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                ImageFileName = filename;
                filename = Prefix + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPathHidden.Value + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void BannerList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[6].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[6].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Banner?');";
                    }
                }

                HiddenField BannerImageFilename = e.Row.FindControl("BannerImageFilename") as HiddenField;
                Image BannerImage = e.Row.FindControl("BannerImage") as Image;
                BannerImage.ImageUrl = SaveFolderHidden.Value + BannerImageFilename.Value;

                HiddenField BannerImageMobileFilename = e.Row.FindControl("BannerImageMobileFilename") as HiddenField;
                Image BannerImageMobile = e.Row.FindControl("BannerImageMobile") as Image;
                if (BannerImageMobileFilename.Value != "")
                    BannerImageMobile.ImageUrl = SaveFolderHidden.Value + BannerImageMobileFilename.Value;
            }
        }

        protected void BannerList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField BannerIdHidden2 = BannerList.Rows[e.RowIndex].FindControl("BannerIdHidden2") as HiddenField;
            if (BannerIdHidden2 != null)
            {
                SqlConnection Con = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    Con = new SqlConnection(connStr);
                    Con.Open();

                    DeleteBanner(Con, int.Parse(BannerIdHidden2.Value));

                    ShowHomeBanner();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (Con != null)
                        Con.Close();
                }
            }
        }

        private void DeleteBanner(SqlConnection Con, int BannerId)
        {
            string sql = "select BannerImage, BannerImageMobile from dbo.Banner where BannerId = @BannerId";
            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@BannerId", SqlDbType.Int).Value = BannerId;

            SqlDataReader reader = Cmd.ExecuteReader();
            if (reader.Read())
            {
                if (reader["BannerImage"] != null && reader["BannerImage"].ToString() != "")
                {
                    string FilePath = FullPathHidden.Value + reader["BannerImage"].ToString();
                    if (File.Exists(FilePath))
                    {
                        File.Delete(FilePath);
                    }
                }
                if (reader["BannerImageMobile"] != null && reader["BannerImageMobile"].ToString() != "")
                {
                    string FilePath = FullPathHidden.Value + reader["BannerImageMobile"].ToString();
                    if (File.Exists(FilePath))
                    {
                        File.Delete(FilePath);
                    }
                }
            }
            reader.Close();

            sql = "delete from dbo.Banner where BannerId = @BannerId";
            Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.Parameters.Add("@BannerId", SqlDbType.Int).Value = BannerId;

            Cmd.ExecuteNonQuery();
        }

        protected void BannerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HiddenField BannerIdHidden2 = BannerList.SelectedRow.FindControl("BannerIdHidden2") as HiddenField;
            if (BannerIdHidden2 != null)
            {
                AddBannerTitle.Text = "Edit Banner";
                AddBannerButton.Text = "Save";
                ThisBannerIdHidden.Value = BannerIdHidden2.Value;

                HiddenField BannerImageFilename = BannerList.SelectedRow.FindControl("BannerImageFilename") as HiddenField;
                HiddenField BannerImageMobileFilename = BannerList.SelectedRow.FindControl("BannerImageMobileFilename") as HiddenField;
                HiddenField SortOrder = BannerList.SelectedRow.FindControl("SortOrder") as HiddenField;

                //ExistingBanner.Text = BannerImageFilename.Value;
                BannerOrder.Text = SortOrder.Value;
                BannerCaption.Text = BannerList.SelectedRow.Cells[1].Text;
                BannerURL.Text = BannerList.SelectedRow.Cells[2].Text;

                AddBannerPanel.Visible = true;
            }

        }

        protected void BannerList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string BannerId = (string)e.CommandArgument;
            if (e.CommandName != "UP" && e.CommandName != "DN")
            {
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                StringBuilder sql = new StringBuilder();
                if (e.CommandName == "UP")
                {
                    sql.AppendLine("declare @SortOrder int; declare @UpBannerId int; declare @BannerLocation int; ");
                    sql.AppendLine("select @BannerLocation = BannerLocation, @SortOrder = SortOrder from dbo.Banner where BannerId=@BannerId; ");
                    sql.AppendLine("select @UpBannerId = BannerId from Banner (nolock) where SortOrder = (select max(SortOrder) from dbo.Banner where BannerLocation = @BannerLocation and SortOrder < @SortOrder) ; ");
                    sql.AppendLine("if exists (select * from dbo.Banner where BannerLocation = @BannerLocation and SortOrder = @SortOrder and BannerId <> @BannerId)");
                    sql.AppendLine("begin ");
                    sql.AppendLine("    update dbo.Banner set SortOrder = @SortOrder - 1 where BannerId=@BannerId;");
                    sql.AppendLine("end");
                    sql.AppendLine("else if @UpBannerId is not null ");
                    sql.AppendLine("begin ");
                    sql.AppendLine("    update dbo.Banner set SortOrder = (select SortOrder from dbo.Banner where BannerId=@UpBannerId) where  BannerId=@BannerId;");
                    sql.AppendLine("    update dbo.Banner set SortOrder = @SortOrder where  BannerId=@UpBannerId;");
                    sql.AppendLine("end");
                }
                else if (e.CommandName == "DN")
                {
                    sql.AppendLine("declare @SortOrder int; declare @DnBannerId int; declare @BannerLocation int; ");
                    sql.AppendLine("select @BannerLocation = BannerLocation, @SortOrder = SortOrder from dbo.Banner where BannerId=@BannerId; ");
                    sql.AppendLine("select @DnBannerId = BannerId from Banner (nolock) where SortOrder = (select min(SortOrder) from dbo.Banner where BannerLocation = @BannerLocation and SortOrder > @SortOrder) ; ");
                    sql.AppendLine("if exists (select * from dbo.Banner where BannerLocation = @BannerLocation and SortOrder = @SortOrder and BannerId <> @BannerId)");
                    sql.AppendLine("begin ");
                    sql.AppendLine("    update dbo.Banner set SortOrder = @SortOrder + 1 where BannerId=@BannerId;");
                    sql.AppendLine("end");
                    sql.AppendLine("else if @DnBannerId is not null ");
                    sql.AppendLine("begin ");
                    sql.AppendLine("    update dbo.Banner set SortOrder = (select SortOrder from dbo.Banner where BannerId=@DnBannerId) where BannerId=@BannerId;");
                    sql.AppendLine("    update dbo.Banner set SortOrder = @SortOrder where  BannerId=@DnBannerId;");
                    sql.AppendLine("end");
                }
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@BannerId", SqlDbType.Int).Value = int.Parse(BannerId);

                cmd.ExecuteNonQuery();

                ShowHomeBanner();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}