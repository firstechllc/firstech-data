﻿<%@ Page Title="Homepage" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminHomepage.aspx.cs" Inherits="FirstechData.Admin.AdminHomepage" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-wrench"></i>
                <span>
                    Homepage
                </span>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerCaption1" CssClass="col-md-3 control-label">Rotaing Main Banner #1 Caption</asp:Label>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" ID="MainBannerCaption1" CssClass="form-control"/>
                    </div>
                    <div class="col-md-3 text-right">
                        <asp:Button runat="server" ID="DeleteBanner1Button" Text="Delete Banner #1" CssClass="button tiny bg-black radius" OnClick="DeleteBanner1Button_Click"
                            OnClientClick="return confirm('Are you sure you want to delete this banner #1?');" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerURL1" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="MainBannerURL1" CssClass="form-control"  TextMode="Url"/>
                    </div>
                </div>
                <div class="form-group" style="vertical-align:top">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="AttachFile1">Image</asp:Label>
                    <div class="col-md-9">
                        <asp:Image runat="server" ID="AttachImage1" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="AttachFile1" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerCaption2" CssClass="col-md-3 control-label">Rotaing Main Banner #2 Caption</asp:Label>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" ID="MainBannerCaption2" CssClass="form-control"/>
                    </div>
                    <div class="col-md-3 text-right">
                        <asp:Button runat="server" ID="DeleteBanner2Button" Text="Delete Banner #2" CssClass="button tiny bg-black radius" OnClick="DeleteBanner2Button_Click"
                            OnClientClick="return confirm('Are you sure you want to delete this banner #2?');" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerURL2" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="MainBannerURL2" CssClass="form-control" TextMode="Url" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="AttachFile2">Image</asp:Label>
                    <div class="col-md-9" style="vertical-align:top">
                        <asp:Image runat="server" ID="AttachImage2" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="AttachFile2" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerCaption3" CssClass="col-md-3 control-label">Rotaing Main Banner #3 Caption</asp:Label>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" ID="MainBannerCaption3" CssClass="form-control"/>
                    </div>
                    <div class="col-md-3 text-right">
                        <asp:Button runat="server" ID="DeleteBanner3Button" Text="Delete Banner #3" CssClass="button tiny bg-black radius" OnClick="DeleteBanner3Button_Click"
                            OnClientClick="return confirm('Are you sure you want to delete this banner #3?');" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MainBannerURL3" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="MainBannerURL3" CssClass="form-control" TextMode="Url" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="AttachFile3">Image</asp:Label>
                    <div class="col-md-9" style="vertical-align:top">
                        <asp:Image runat="server" ID="AttachImage3" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="AttachFile3" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="LeftBannerCaption" CssClass="col-md-3 control-label">Left Banner Caption</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="LeftBannerCaption" CssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="LeftBannerURL" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="LeftBannerURL" CssClass="form-control"  TextMode="Url"/>
                    </div>
                </div>
                <div class="form-group" style="vertical-align:top">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="LeftAttachFile">Image</asp:Label>
                    <div class="col-md-9">
                        <asp:Image runat="server" ID="LeftAttachImage" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="LeftAttachFile" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceLeftImageLabel" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MiddleBannerCaption" CssClass="col-md-3 control-label">Middle Banner Caption</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="MiddleBannerCaption" CssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MiddleBannerURL" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="MiddleBannerURL" CssClass="form-control" TextMode="Url" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="MiddleAttachFile">Image</asp:Label>
                    <div class="col-md-9" style="vertical-align:top">
                        <asp:Image runat="server" ID="MiddleAttachImage" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="MiddleAttachFile" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceMiddleImageLabel" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="RightBannerCaption" CssClass="col-md-3 control-label">Right Banner Caption</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="RightBannerCaption" CssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="RightBannerURL" CssClass="col-md-3 control-label">URL</asp:Label>
                    <div class="col-md-9">
                        <asp:TextBox runat="server" ID="RightBannerURL" CssClass="form-control" TextMode="Url" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-3 control-label" AssociatedControlID="RightAttachFile">Image</asp:Label>
                    <div class="col-md-9" style="vertical-align:top">
                        <asp:Image runat="server" ID="RightAttachImage" Visible="false" />
                        <div class="row">
                            <div class="small-9 columns" style="padding-left: 0">
                                <asp:FileUpload runat="server" ID="RightAttachFile" />
                            </div>
                            <div class="small-3 columns">
                                <asp:Label runat="server" ID="ReplaceRightImageLabel" Text="(Replace)" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Update" CssClass="button tiny bg-black radius" />
                    </div>
                </div>
            </div>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>
        </div>
    </div>
</asp:Content>
