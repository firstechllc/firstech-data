﻿<%@ Page Title="Admin Installation Type" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminInstallationType.aspx.cs" Inherits="FirstechData.Admin.AdminInstallationType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Installation Type</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-box"></i><span>Installation Type</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:TextBox runat="server" ID="SearchInstallationTypeTxt" Width="150px" CssClass="col-sm-2" placeholder="Installation Type to search"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search Installation Type" OnClick="SearchButton_Click" />&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" ID="AddButton" CssClass="button tiny bg-black radius pull-right" Text="Add Installation Type" OnClick="AddButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="InstallationTypeList" runat="server" AutoGenerateColumns="false" CssClass="footable" AllowSorting="true"
                                OnRowCancelingEdit="InstallationTypeList_RowCancelingEdit" OnRowUpdating="InstallationTypeList_RowUpdating"
                                OnRowDeleting="InstallationTypeList_RowDeleting" OnRowEditing="InstallationTypeList_RowEditing" OnRowDataBound="InstallationTypeList_RowDataBound" OnSorting="InstallationTypeList_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Installation Type" SortExpression="InstallationTypeName">
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="InstallationTypeIdHidden" runat="server" Value='<%# Bind("InstallationTypeId") %>' />
                                            <asp:TextBox ID="InstallationTypeTxt" runat="server" Text='<%# Bind("InstallationTypeName") %>' Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="InstallationTypeIdHidden2" runat="server" Value='<%# Bind("InstallationTypeId") %>' />
                                            <asp:Label ID="InstallationTypeLabel" runat="server" Text='<%# Bind("InstallationTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sort Order" SortExpression="SortOrder">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="SortOrderTxt" runat="server" Text='<%# Bind("SortOrder") %>' Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="SortOrderLabel" runat="server" Text='<%# Bind("SortOrder") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField CancelText="Cancel" EditText="Edit" HeaderText="Edit" ShowEditButton="True" UpdateText="Save" ButtonType="Link" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" DeleteText="Delete" ItemStyle-Width="80px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('[id*=InstallationTypeList]').footable();
        });
    </script>
</asp:Content>
