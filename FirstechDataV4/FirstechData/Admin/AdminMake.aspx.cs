﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminMake : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (!roleNames.Contains("Administrator"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                sortCriteria = "VehicleMakeName";
                sortDir = "asc";
                ShowMake();
            }
        }

        public string sortCriteria
        {
            get
            {
                return ViewState["sortCriteria"].ToString();
            }
            set
            {
                ViewState["sortCriteria"] = value;
            }
        }

        public string sortDir
        {
            get
            {
                return ViewState["sortDir"].ToString();
            }
            set
            {
                ViewState["sortDir"] = value;
            }
        }

        private void ShowMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName, CountryCode, UpdatedBy, UpdatedDt ";
                sql += "from dbo.VehicleMake ";
                sql += "order by " + sortCriteria + " " + sortDir;

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                MakeList.DataSource = ds;
                MakeList.DataBind();

                if (MakeList != null && MakeList.HeaderRow != null && MakeList.HeaderRow.Cells.Count > 0)
                {
                    MakeList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                    MakeList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                    MakeList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                    MakeList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";

                    MakeList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void MakeList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            ClearError();
            MakeList.EditIndex = -1;
            ShowMake();
        }

        protected void MakeList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ClearError();

            string VehicleMakeId = ((HiddenField)(MakeList.Rows[e.RowIndex].FindControl("VehicleMakeIdHidden"))).Value; ;
            string Make = ((TextBox)(MakeList.Rows[e.RowIndex].FindControl("MakeTxt"))).Text;
            string CountryCode = ((TextBox)(MakeList.Rows[e.RowIndex].FindControl("CountryCodeTxt"))).Text;

            if (Make == "")
            {
                ShowError("Please enter Make");
                return;
            }
            if (CountryCode == "")
            {
                ShowError("Please enter Country");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "update dbo.VehicleMake set VehicleMakeName = @VehicleMakeName, CountryCode = @CountryCode, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeId=" + VehicleMakeId;

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeName", SqlDbType.NVarChar, 100).Value = Make;
                cmd.Parameters.Add("@CountryCode", SqlDbType.VarChar, 10).Value = CountryCode;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                MakeList.EditIndex = -1;
                ShowMake();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void MakeList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ClearError();
            MakeList.EditIndex = e.NewEditIndex;
            ShowMake();
        }

        protected void MakeList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string VehicleMakeId = ((HiddenField)(MakeList.Rows[e.RowIndex].FindControl("VehicleMakeIdHidden2"))).Value; ;
            string Make = ((Label)(MakeList.Rows[e.RowIndex].FindControl("MakeLabel"))).Text;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                string sql = "delete from dbo.VehicleMake where VehicleMakeId=" + VehicleMakeId;
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                ShowMake();
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("The DELETE statement conflicted with the REFERENCE constraint") > 0)
                    ShowError("There are Wireing, Prep, or Disassembly data of " + Make + ". You cannot delete it.");
                else
                    ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void MakeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Make?');";
                    }
                }
            }

        }

        protected void MakeList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (sortCriteria != e.SortExpression)
            {
                sortCriteria = e.SortExpression;
                sortDir = "asc";
            }
            else
            {
                if (sortDir == "desc")
                    sortDir = "asc";
                else
                    sortDir = "desc";
            }
            ShowMake();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminMakeAdd");
        }

    }
}