﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminNote.aspx.cs" Inherits="FirstechData.Admin.AdminNote" ValidateRequest="false" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Information</li>
        <li>Note</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Note</span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <div class="form-group form-horizontal">
                            <label for="VehicleMakeList" class="col-sm-1 control-label">Make:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <label for="VehicleModelList" class="col-sm-1 control-label">Model:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <label for="VehicleYearList" class="col-sm-1 control-label">Year:</label>
                            <div class="col-sm-2">
                                <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Note</span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <asp:Panel runat="server" ID="ViewPanel">
                        <asp:Label runat="server" ID="NoteLabel"></asp:Label><br /><br />
                        <asp:Button runat="server" ID="EditButton" CssClass="button tiny bg-black radius" Text="Edit" OnClick="EditButton_Click"></asp:Button>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="EditPanel" Visible="false">
                        <CKEditor:CKEditorControl ID="editor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                        </CKEditor:CKEditorControl>
                        <br />
                        <asp:Button runat="server" ID="SaveButton" CssClass="button tiny bg-black radius" Text="Save" OnClick="SaveButton_Click"></asp:Button>
                        <asp:Button runat="server" ID="CancelButton" CssClass="button tiny bg-black radius" Text="Cancel" OnClick="CancelButton_Click"></asp:Button>
                    </asp:Panel>
                </div>
                <!-- end .timeline -->
            </div>
            <asp:HiddenField runat="server" ID="VehicleWireNoteIdHidden" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
