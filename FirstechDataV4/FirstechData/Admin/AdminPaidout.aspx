﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminPaidout.aspx.cs" Inherits="FirstechData.Admin.AdminPaidout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-gear"></i>
                <span style="color: black; font-size: medium"><b>Paid Out</b></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-sm-1" style="white-space: nowrap; ">
                Start Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="StartDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                End Date: 
            </div>
            <div class="col-sm-2">
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control" runat="server" id="EndDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchButton_Click"></asp:Button>
                <asp:Button runat="server" ID="ExportButton" CssClass="button tiny bg-gray radius" Text="Export" OnClick="ExportButton_Click"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="InfoLabel" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </div>

    <div class="row">
        <asp:GridView ID="PaidOutList" runat="server" AutoGenerateColumns="False" CssClass="footable" Width="100%" 
            AllowPaging="false" >
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="EditorMTCRepcode" HeaderText="MTCRepcode" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="InvoiceNumber" HeaderText="InvoiceNumber" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="AccountType" HeaderText="AccountType" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="SubscriptionPlan" HeaderText="SubscriptionPlan" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="RewardType" HeaderText="RewardType" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="RewardAmount" HeaderText="RewardAmount" ItemStyle-Font-Size="12px" />
                <asp:BoundField DataField="AddedDate" HeaderText="AddedDate" ItemStyle-Font-Size="12px" DataFormatString="{0:MM/dd/yyyy}" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
