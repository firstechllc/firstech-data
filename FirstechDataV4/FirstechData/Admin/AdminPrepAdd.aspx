﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminPrepAdd.aspx.cs" Inherits="FirstechData.Admin.AdminPrepAdd" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Information</li>
        <li>Prep</li>
    </ul>
    <!-- end of breadcrumbs -->

    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-wrench"></i>
                        <span>
                            <asp:Label runat="server" ID="TitleLabel"></asp:Label>
                        </span>
                        <asp:HiddenField runat="server" ID="MakeId" />
                        <asp:HiddenField runat="server" ID="ModelId" />
                        <asp:HiddenField runat="server" ID="YearId" />
                        <asp:HiddenField runat="server" ID="Id" />
                        <asp:HiddenField runat="server" ID="ExistingFilename1" />
                        <asp:HiddenField runat="server" ID="ExistingFilename2" />
                        <asp:HiddenField runat="server" ID="ExistingFilename3" />
                        <asp:HiddenField runat="server" ID="ExistingFilename4" />
                        <asp:HiddenField runat="server" ID="ExistingFilename5" />
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="form-horizontal">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="MakeLabel" CssClass="col-md-2 control-label">Make</asp:Label>
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="MakeLabel" CssClass="form-control no-border no-shadow" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="ModelLabel" CssClass="col-md-2 control-label">Model</asp:Label>
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="ModelLabel" CssClass="form-control no-border no-shadow" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="YearLabel" CssClass="col-md-2 control-label">Year</asp:Label>
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="YearLabel" CssClass="form-control no-border no-shadow" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Step" CssClass="col-md-2 control-label">Step</asp:Label>
                            <div class="col-md-1">
                                <asp:TextBox runat="server" ID="Step" CssClass="form-control" TextMode="Number" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Step" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="The Step field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="editor" CssClass="col-md-2 control-label">Note</asp:Label>
                            <div class="col-md-8">
                                <CKEditor:CKEditorControl ID="editor" runat="server" Height="200" BasePath="/Scripts/js/ckeditor">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 control-label">Image Files</asp:Label>
                            <div class="col-md-6">
                                <asp:Image runat="server" ID="AttachImage1" Visible="false" />
                                <asp:CheckBox runat="server" ID="AttachIamgeDelete1" Text="Delete?" Visible="false" />
                                <div class="row">
                                    <div class="small-9 columns" style="padding-left: 0">
                                        <asp:FileUpload runat="server" ID="AttachFile1" />
                                    </div>
                                    <div class="small-3 columns">
                                        <asp:Label runat="server" ID="ReplaceImageLabel1" Text="(Replace)" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <br />

                                <asp:Image runat="server" ID="AttachImage2" Visible="false" />
                                <asp:CheckBox runat="server" ID="AttachIamgeDelete2" Text="Delete?" Visible="false" />
                                <div class="row">
                                    <div class="small-9 columns" style="padding-left: 0">
                                        <asp:FileUpload runat="server" ID="AttachFile2" />
                                    </div>
                                    <div class="small-3 columns">
                                        <asp:Label runat="server" ID="ReplaceImageLabel2" Text="(Replace)" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <br />

                                <asp:Image runat="server" ID="AttachImage3" Visible="false" />
                                <asp:CheckBox runat="server" ID="AttachIamgeDelete3" Text="Delete?" Visible="false" />
                                <div class="row">
                                    <div class="small-9 columns" style="padding-left: 0">
                                        <asp:FileUpload runat="server" ID="AttachFile3" />
                                    </div>
                                    <div class="small-3 columns">
                                        <asp:Label runat="server" ID="ReplaceImageLabel3" Text="(Replace)" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <br />

                                <asp:Image runat="server" ID="AttachImage4" Visible="false" />
                                <asp:CheckBox runat="server" ID="AttachIamgeDelete4" Text="Delete?" Visible="false" />
                                <div class="row">
                                    <div class="small-9 columns" style="padding-left: 0">
                                        <asp:FileUpload runat="server" ID="AttachFile4" />
                                    </div>
                                    <div class="small-3 columns">
                                        <asp:Label runat="server" ID="ReplaceImageLabel4" Text="(Replace)" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <br />

                                <asp:Image runat="server" ID="AttachImage5" Visible="false" />
                                <asp:CheckBox runat="server" ID="AttachIamgeDelete5" Text="Delete?" Visible="false" />
                                <div class="row">
                                    <div class="small-9 columns" style="padding-left: 0">
                                        <asp:FileUpload runat="server" ID="AttachFile5" />
                                    </div>
                                    <div class="small-3 columns">
                                        <asp:Label runat="server" ID="ReplaceImageLabel5" Text="(Replace)" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Add" CssClass="button tiny bg-black radius" />
                                <asp:Button runat="server" OnClick="CancelButton_Click" ID="CancelButton" Text="Cancel" CssClass="button tiny bg-black radius" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="ErrorLabel" />
                    </p>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="AddButton" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
