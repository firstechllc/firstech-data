﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("ProductMenu");
                LoadDim();
                LoadProductList();

                BrainDocumentURL.Attributes.CssStyle.Add("max-width", "none");
                RemoteDocumentURL.Attributes.CssStyle.Add("max-width", "none");
                AntennaDocumentURL.Attributes.CssStyle.Add("max-width", "none");
                AccessoriesDocumentURL.Attributes.CssStyle.Add("max-width", "none");

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());
                RoleNameHidden.Value = string.Join(",", roleNames);
            }
        }

        private void LoadDim()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select ProductTypeId, ProductTypeName from dbo.ProductType with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ProductTypeList.DataTextField = "ProductTypeName";
                ProductTypeList.DataValueField = "ProductTypeId";
                ProductTypeList.DataSource = ds;
                ProductTypeList.DataBind();

                ProductTypeList.Items.Insert(0, new ListItem("", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }
        private void LoadProductList()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select ProductId, PartNumber as Name from dbo.Product with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ProductList.DataTextField = "Name";
                ProductList.DataValueField = "ProductId";
                ProductList.DataSource = ds;
                ProductList.DataBind();

                ProductList.Items.Insert(0, new ListItem("", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ShowInfo(string info)
        {
            InfoLabel.Text = info;
            InfoLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }

        protected void ProductList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();

            string ProductIdStr = ProductList.SelectedValue;

            if (ProductIdStr == "-1")
            {
                ShowError("Please select Product.");

                CloseWindows();

                return;
            }

            ProductIdHidden.Value = ProductIdStr;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select ProductTypeId, ModelNumber, PartNumber, Picture1=isnull(Picture1, ''), UpdatedDt, UpdatedBy
                    from dbo.Product where ProductId = @ProductId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();

                string PartNumber = "";
                string ModelNumber = "";
                string Picture1 = "";

                if (reader.Read())
                {
                    ProductTypeIdHidden.Value = reader["ProductTypeId"].ToString();
                    PartNumber = reader["PartNumber"].ToString();
                    ModelNumber = reader["ModelNumber"].ToString();
                    Picture1 = reader["Picture1"].ToString();
                }
                reader.Close();

                CloseWindows();

                if (ProductTypeIdHidden.Value == "1") // Control Brain
                {
                    ShowEditControlBrain(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditBrainPanel.Visible = true;
                }
                else if (ProductTypeIdHidden.Value == "2") // Remote
                {
                    ShowEditRemote(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditRemotePanel.Visible = true;
                }
                else if (ProductTypeIdHidden.Value == "3") // Antenna
                {
                    ShowEditAntenna(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditAntennaPanel.Visible = true;
                }
                else if (ProductTypeIdHidden.Value == "4") // Accessories
                {
                    ShowEditAccessories(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditAccessoriesPanel.Visible = true;
                }
                else if (ProductTypeIdHidden.Value == "5") // Package
                {
                    ShowEditPackage(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditPackagePanel.Visible = true;
                }
                else if (ProductTypeIdHidden.Value == "6") // Drone
                {
                    ShowEditDrone(ProductIdStr, PartNumber, ModelNumber, Picture1);
                    EditDronePanel.Visible = true;
                }

                NewProductPanel.Visible = false;


                if (RoleNameHidden.Value.Contains("Administrator") || RoleNameHidden.Value.Contains("Office"))
                {
                    WikiPanel.Visible = true;
                    ShowProductWikiSectionList();
                }
                else
                {
                    WikiPanel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowEditControlBrain(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentBrainProduct.Text = PartNumber + " (Control Brain)";
                EditBrainModelNumber.Text = ModelNumber;
                EditBrainPartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    ProductPictureView.ImageUrl = SaveFolder + Picture1;
                    ProductPictureFilepath.Value = Picture1;
                    ProductPictureView.Visible = true;
                }
                else
                {
                    ProductPictureView.ImageUrl = "";
                    ProductPictureFilepath.Value = "";
                    ProductPictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select TwoWayAntenna, OneWayAntenna,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport,
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, DocumentURL
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        BrainDocumentURL.Text = reader["DocumentURL"].ToString();
                    else
                        BrainDocumentURL.Text = "";

                    if (reader["TwoWayAntenna"] != null && reader["TwoWayAntenna"].ToString() != "")
                        EditBrainTwoWayAntennaList.SelectedValue = reader["TwoWayAntenna"].ToString();

                    if (reader["OneWayAntenna"] != null && reader["OneWayAntenna"].ToString() != "")
                        EditBrainOneWayAntennaList.SelectedValue = reader["OneWayAntenna"].ToString();

                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditBrainStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditBrainOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditBrainOperatingVoltages.Text = "";

                    if (reader["BladeCompatible"] != null && reader["BladeCompatible"].ToString() != "")
                        EditBrainBladeCompatible.SelectedValue = reader["BladeCompatible"].ToString();

                    if (reader["DroneCompatible"] != null && reader["DroneCompatible"].ToString() != "")
                        EditBrainDroneCompatible.SelectedValue = reader["DroneCompatible"].ToString();

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditBrainAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["IdleCurrent"] != null)
                        EditBrainIdleCurrent.Text = reader["IdleCurrent"].ToString();
                    else
                        EditBrainIdleCurrent.Text = "";

                    if (reader["Dataport"] != null && reader["Dataport"].ToString() != "")
                        EditBrainDataPort.SelectedValue = reader["Dataport"].ToString();

                    if (reader["OperatingTempF"] != null)
                        EditBrainOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditBrainOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditBrainOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditBrainOperatingTemperatureC.Text = "";

                    if (reader["AntennaPort4Pin"] != null && reader["AntennaPort4Pin"].ToString() != "")
                        EditBrainAntennaPort4Pin.SelectedValue = reader["AntennaPort4Pin"].ToString();

                    if (reader["AntennaPort6Pin"] != null && reader["AntennaPort6Pin"].ToString() != "")
                        EditBrainAntennaPort6Pin.SelectedValue = reader["AntennaPort6Pin"].ToString();

                    if (reader["WaterResistant"] != null && reader["WaterResistant"].ToString() != "")
                        EditBrainWaterResistantList.SelectedValue = reader["WaterResistant"].ToString();
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesList.DataTextField = "ProductAlternateName";
                AlternateNamesList.DataValueField = "ProductAlternateName";
                AlternateNamesList.DataSource = ds;
                AlternateNamesList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 1 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainReplacePartNoList.DataTextField = "PartNumber";
                EditBrainReplacePartNoList.DataValueField = "ProductId";
                EditBrainReplacePartNoList.DataSource = ds;
                EditBrainReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainReplacePartNoListSelected.DataTextField = "PartNumber";
                EditBrainReplacePartNoListSelected.DataValueField = "ProductId";
                EditBrainReplacePartNoListSelected.DataSource = ds;
                EditBrainReplacePartNoListSelected.DataBind();


                // Convenience
                sql = "select ProductBrainConvenienceId, ProductBrainConvenienceName from dbo.ProductBrainConvenience with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainConvenienceList.DataTextField = "ProductBrainConvenienceName";
                EditBrainConvenienceList.DataValueField = "ProductBrainConvenienceId";
                EditBrainConvenienceList.DataSource = ds;
                EditBrainConvenienceList.DataBind();


                sql = @"select a.ProductBrainConvenienceId, a.ProductBrainConvenienceName 
                    from dbo.ProductBrainConvenience a with (nolock) 
                    join dbo.ProductProductBrainConvenience b with (nolock) on a.ProductBrainConvenienceId=b.ProductBrainConvenienceId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainConvenienceListSelected.DataTextField = "ProductBrainConvenienceName";
                EditBrainConvenienceListSelected.DataValueField = "ProductBrainConvenienceId";
                EditBrainConvenienceListSelected.DataSource = ds;
                EditBrainConvenienceListSelected.DataBind();


                // Remote Start
                sql = "select ProductBrainRemoteStartId, ProductBrainRemoteStartName from dbo.ProductBrainRemoteStart with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainRemoteStartList.DataTextField = "ProductBrainRemoteStartName";
                EditBrainRemoteStartList.DataValueField = "ProductBrainRemoteStartId";
                EditBrainRemoteStartList.DataSource = ds;
                EditBrainRemoteStartList.DataBind();

                sql = @"select a.ProductBrainRemoteStartId, a.ProductBrainRemoteStartName 
                    from dbo.ProductBrainRemoteStart a with (nolock) 
                    join dbo.ProductProductBrainRemoteStart b with (nolock) on a.ProductBrainRemoteStartId=b.ProductBrainRemoteStartId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainRemoteStartListSelected.DataTextField = "ProductBrainRemoteStartName";
                EditBrainRemoteStartListSelected.DataValueField = "ProductBrainRemoteStartId";
                EditBrainRemoteStartListSelected.DataSource = ds;
                EditBrainRemoteStartListSelected.DataBind();



                // Alarm
                sql = "select ProductBrainAlarmId, ProductBrainAlarmName from dbo.ProductBrainAlarm with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainAlarmList.DataTextField = "ProductBrainAlarmName";
                EditBrainAlarmList.DataValueField = "ProductBrainAlarmId";
                EditBrainAlarmList.DataSource = ds;
                EditBrainAlarmList.DataBind();

                sql = @"select a.ProductBrainAlarmId, a.ProductBrainAlarmName 
                    from dbo.ProductBrainAlarm a with (nolock) 
                    join dbo.ProductProductBrainAlarm b with (nolock) on a.ProductBrainAlarmId=b.ProductBrainAlarmId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainAlarmListSelected.DataTextField = "ProductBrainAlarmName";
                EditBrainAlarmListSelected.DataValueField = "ProductBrainAlarmId";
                EditBrainAlarmListSelected.DataSource = ds;
                EditBrainAlarmListSelected.DataBind();



                // Installation Flexibility
                sql = "select ProductBrainInstallationId, ProductBrainInstallationName from dbo.ProductBrainInstallation with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainInstallationList.DataTextField = "ProductBrainInstallationName";
                EditBrainInstallationList.DataValueField = "ProductBrainInstallationId";
                EditBrainInstallationList.DataSource = ds;
                EditBrainInstallationList.DataBind();


                sql = @"select a.ProductBrainInstallationId, a.ProductBrainInstallationName 
                    from dbo.ProductBrainInstallation a with (nolock) 
                    join dbo.ProductProductBrainInstallation b with (nolock) on a.ProductBrainInstallationId=b.ProductBrainInstallationId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainInstallationListSelected.DataTextField = "ProductBrainInstallationName";
                EditBrainInstallationListSelected.DataValueField = "ProductBrainInstallationId";
                EditBrainInstallationListSelected.DataSource = ds;
                EditBrainInstallationListSelected.DataBind();


                // Compatible Remotes Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompRemotesCurrentList.DataTextField = "PartNumber";
                EditBrainCompRemotesCurrentList.DataValueField = "ProductId";
                EditBrainCompRemotesCurrentList.DataSource = ds;
                EditBrainCompRemotesCurrentList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                    where b.ProductId_Brain = @ProductId
                        and a.ProductTypeId = 2 
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompRemotesCurrentListSelected.DataTextField = "PartNumber";
                EditBrainCompRemotesCurrentListSelected.DataValueField = "ProductId";
                EditBrainCompRemotesCurrentListSelected.DataSource = ds;
                EditBrainCompRemotesCurrentListSelected.DataBind();


                // Compatible Remotes Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompRemotesDisList.DataTextField = "PartNumber";
                EditBrainCompRemotesDisList.DataValueField = "ProductId";
                EditBrainCompRemotesDisList.DataSource = ds;
                EditBrainCompRemotesDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                    where b.ProductId_Brain = @ProductId
                        and a.ProductTypeId = 2 
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompRemotesDisListSelected.DataTextField = "PartNumber";
                EditBrainCompRemotesDisListSelected.DataValueField = "ProductId";
                EditBrainCompRemotesDisListSelected.DataSource = ds;
                EditBrainCompRemotesDisListSelected.DataBind();

                // Compatible Drones Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 6 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompDronesCurrentList.DataTextField = "PartNumber";
                EditBrainCompDronesCurrentList.DataValueField = "ProductId";
                EditBrainCompDronesCurrentList.DataSource = ds;
                EditBrainCompDronesCurrentList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Drone
                    where b.ProductId_Brain = @ProductId
                        and a.ProductTypeId = 6 
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompDronesCurrentListSelected.DataTextField = "PartNumber";
                EditBrainCompDronesCurrentListSelected.DataValueField = "ProductId";
                EditBrainCompDronesCurrentListSelected.DataSource = ds;
                EditBrainCompDronesCurrentListSelected.DataBind();


                // Compatible Drones Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 6 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompDronesDisList.DataTextField = "PartNumber";
                EditBrainCompDronesDisList.DataValueField = "ProductId";
                EditBrainCompDronesDisList.DataSource = ds;
                EditBrainCompDronesDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Drone
                    where b.ProductId_Brain = @ProductId
                        and a.ProductTypeId = 6
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainCompDronesDisListSelected.DataTextField = "PartNumber";
                EditBrainCompDronesDisListSelected.DataValueField = "ProductId";
                EditBrainCompDronesDisListSelected.DataSource = ds;
                EditBrainCompDronesDisListSelected.DataBind();

                // Accessories
                sql = @"select ProductId, ModelNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 4 order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainAccessoriesList.DataTextField = "ModelNumber";
                EditBrainAccessoriesList.DataValueField = "ProductId";
                EditBrainAccessoriesList.DataSource = ds;
                EditBrainAccessoriesList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Accessories
                    where b.ProductId_Brain = @ProductId
                        and a.ProductTypeId = 4
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditBrainAccessoriesListSelected.DataTextField = "PartNumber";
                EditBrainAccessoriesListSelected.DataValueField = "ProductId";
                EditBrainAccessoriesListSelected.DataSource = ds;
                EditBrainAccessoriesListSelected.DataBind();

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowEditRemote(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentRemoteProduct.Text = PartNumber + " (Remote)";
                EditRemoteModelNumber.Text = ModelNumber;
                EditRemotePartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    RemotePictureView.ImageUrl = SaveFolder + Picture1;
                    ProductRemoteFilepath.Value = Picture1;
                    RemotePictureView.Visible = true;
                }
                else
                {
                    RemotePictureView.ImageUrl = "";
                    ProductRemoteFilepath.Value = "";
                    RemotePictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select WayRemote, TwoWayAntenna, OneWayAntenna, EstimatedRangeFt, FccIdUSA, FccIdCanada, DocumentURL,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport, 
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, Battery, EstimatedBatteryLifeDays
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["FccIdUSA"] != null && reader["FccIdUSA"].ToString() != "")
                        EditRemoteFccId.Text = reader["FccIdUSA"].ToString();
                    else
                        EditRemoteFccId.Text = "";

                    if (reader["FccIdCanada"] != null && reader["FccIdCanada"].ToString() != "")
                        EditRemoteICID.Text = reader["FccIdCanada"].ToString();
                    else
                        EditRemoteICID.Text = "";

                    if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        RemoteDocumentURL.Text = reader["DocumentURL"].ToString();
                    else
                        RemoteDocumentURL.Text = "";

                    if (reader["WayRemote"] != null && reader["WayRemote"].ToString() != "")
                        WayRemoteList.SelectedValue = reader["WayRemote"].ToString();

                    if (reader["TwoWayAntenna"] != null && reader["TwoWayAntenna"].ToString() != "")
                        EditRemoteTwoWayAntenna.Text = reader["TwoWayAntenna"].ToString();
                    else
                        EditRemoteTwoWayAntenna.Text = "";

                    if (reader["OneWayAntenna"] != null && reader["OneWayAntenna"].ToString() != "")
                        EditRemoteOneWayAntenna.Text = reader["OneWayAntenna"].ToString();
                    else
                        EditRemoteOneWayAntenna.Text = "";

                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditRemoteStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditRemoteOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditRemoteOperatingVoltages.Text = "";

                    if (reader["EstimatedRangeFt"] != null && reader["EstimatedRangeFt"].ToString() != "")
                        EditRemoteRangeInFeet.Text = reader["EstimatedRangeFt"].ToString();
                    else
                        EditRemoteRangeInFeet.Text = "";

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditRemoteAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["IdleCurrent"] != null)
                        EditRemoteIdleCurrent.Text = reader["IdleCurrent"].ToString();
                    else
                        EditRemoteIdleCurrent.Text = "";

                    if (reader["Battery"] != null && reader["Battery"].ToString() != "")
                        EditRemoteBatteryType.Text = reader["Battery"].ToString();
                    else
                        EditRemoteBatteryType.Text = "";

                    if (reader["OperatingTempF"] != null)
                        EditRemoteOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditRemoteOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditRemoteOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditRemoteOperatingTemperatureC.Text = "";

                    if (reader["EstimatedBatteryLifeDays"] != null && reader["EstimatedBatteryLifeDays"].ToString() != "")
                        EditRemoteEstimatedBatteryLife.Text = reader["EstimatedBatteryLifeDays"].ToString();
                    else
                        EditRemoteEstimatedBatteryLife.Text = "";

                    if (reader["WaterResistant"] != null && reader["WaterResistant"].ToString() != "")
                        EditRemoteWaterResistantList.SelectedValue = reader["WaterResistant"].ToString();
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesRemoteList.DataTextField = "ProductAlternateName";
                AlternateNamesRemoteList.DataValueField = "ProductAlternateName";
                AlternateNamesRemoteList.DataSource = ds;
                AlternateNamesRemoteList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 2 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteReplacePartNoList.DataTextField = "PartNumber";
                EditRemoteReplacePartNoList.DataValueField = "ProductId";
                EditRemoteReplacePartNoList.DataSource = ds;
                EditRemoteReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteReplacePartNoListSelected.DataTextField = "PartNumber";
                EditRemoteReplacePartNoListSelected.DataValueField = "ProductId";
                EditRemoteReplacePartNoListSelected.DataSource = ds;
                EditRemoteReplacePartNoListSelected.DataBind();


                // Convenience
                sql = "select ProductRemoteConvenienceId, ProductRemoteConvenienceName from dbo.ProductRemoteConvenience with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteConvenienceList.DataTextField = "ProductRemoteConvenienceName";
                EditRemoteConvenienceList.DataValueField = "ProductRemoteConvenienceId";
                EditRemoteConvenienceList.DataSource = ds;
                EditRemoteConvenienceList.DataBind();


                sql = @"select a.ProductRemoteConvenienceId, a.ProductRemoteConvenienceName 
                    from dbo.ProductRemoteConvenience a with (nolock) 
                    join dbo.ProductProductRemoteConvenience b with (nolock) on a.ProductRemoteConvenienceId=b.ProductRemoteConvenienceId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteConvenienceListSelected.DataTextField = "ProductRemoteConvenienceName";
                EditRemoteConvenienceListSelected.DataValueField = "ProductRemoteConvenienceId";
                EditRemoteConvenienceListSelected.DataSource = ds;
                EditRemoteConvenienceListSelected.DataBind();


                // Auxiliary
                sql = "select ProductRemoteAuxiliaryId, ProductRemoteAuxiliaryName from dbo.ProductRemoteAuxiliary with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteAuxiliaryList.DataTextField = "ProductRemoteAuxiliaryName";
                EditRemoteAuxiliaryList.DataValueField = "ProductRemoteAuxiliaryId";
                EditRemoteAuxiliaryList.DataSource = ds;
                EditRemoteAuxiliaryList.DataBind();

                sql = @"select a.ProductRemoteAuxiliaryId, a.ProductRemoteAuxiliaryName 
                    from dbo.ProductRemoteAuxiliary a with (nolock) 
                    join dbo.ProductProductRemoteAuxiliary b with (nolock) on a.ProductRemoteAuxiliaryId=b.ProductRemoteAuxiliaryId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteAuxiliaryListSelected.DataTextField = "ProductRemoteAuxiliaryName";
                EditRemoteAuxiliaryListSelected.DataValueField = "ProductRemoteAuxiliaryId";
                EditRemoteAuxiliaryListSelected.DataSource = ds;
                EditRemoteAuxiliaryListSelected.DataBind();



                // Programmable
                sql = "select ProductRemoteProgrammableId, ProductRemoteProgrammableName from dbo.ProductRemoteProgrammable with (nolock) order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteProgrammableList.DataTextField = "ProductRemoteProgrammableName";
                EditRemoteProgrammableList.DataValueField = "ProductRemoteProgrammableId";
                EditRemoteProgrammableList.DataSource = ds;
                EditRemoteProgrammableList.DataBind();

                sql = @"select a.ProductRemoteProgrammableId, a.ProductRemoteProgrammableName 
                    from dbo.ProductRemoteProgrammable a with (nolock) 
                    join dbo.ProductProductRemoteProgrammable b with (nolock) on a.ProductRemoteProgrammableId=b.ProductRemoteProgrammableId
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteProgrammableListSelected.DataTextField = "ProductRemoteProgrammableName";
                EditRemoteProgrammableListSelected.DataValueField = "ProductRemoteProgrammableId";
                EditRemoteProgrammableListSelected.DataSource = ds;
                EditRemoteProgrammableListSelected.DataBind();

                


                // Compatible Brain Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompBrainCurrentList.DataTextField = "PartNumber";
                EditRemoteCompBrainCurrentList.DataValueField = "ProductId";
                EditRemoteCompBrainCurrentList.DataSource = ds;
                EditRemoteCompBrainCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Remote = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompBrainCurrentListSelected.DataTextField = "PartNumber";
                EditRemoteCompBrainCurrentListSelected.DataValueField = "ProductId";
                EditRemoteCompBrainCurrentListSelected.DataSource = ds;
                EditRemoteCompBrainCurrentListSelected.DataBind();

                // Compatible Companion
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompanionList.DataTextField = "PartNumber";
                EditRemoteCompanionList.DataValueField = "ProductId";
                EditRemoteCompanionList.DataSource = ds;
                EditRemoteCompanionList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductRemoteCompanion b with (nolock) on a.ProductId=b.ProductId_Companion
                    where b.ProductId_Remote = @ProductId
                        and a.ProductTypeId = 2
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompanionListSelected.DataTextField = "PartNumber";
                EditRemoteCompanionListSelected.DataValueField = "ProductId";
                EditRemoteCompanionListSelected.DataSource = ds;
                EditRemoteCompanionListSelected.DataBind();


                // Compatible Brain Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompBrainDisList.DataTextField = "PartNumber";
                EditRemoteCompBrainDisList.DataValueField = "ProductId";
                EditRemoteCompBrainDisList.DataSource = ds;
                EditRemoteCompBrainDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Remote = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteCompBrainDisListSelected.DataTextField = "PartNumber";
                EditRemoteCompBrainDisListSelected.DataValueField = "ProductId";
                EditRemoteCompBrainDisListSelected.DataSource = ds;
                EditRemoteCompBrainDisListSelected.DataBind();

                // Compatible 1 Way Antenna
                sql = @"select ProductId, ModelNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 3 
                    and WayRemote = '1Way' 
                    order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteComp1AntennaList.DataTextField = "ModelNumber";
                EditRemoteComp1AntennaList.DataValueField = "ProductId";
                EditRemoteComp1AntennaList.DataSource = ds;
                EditRemoteComp1AntennaList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Antenna
                    where b.ProductId_Remote = @ProductId
                        and a.ProductTypeId = 3
                        and a.WayRemote = '1Way' 
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteComp1AntennaListSelected.DataTextField = "PartNumber";
                EditRemoteComp1AntennaListSelected.DataValueField = "ProductId";
                EditRemoteComp1AntennaListSelected.DataSource = ds;
                EditRemoteComp1AntennaListSelected.DataBind();


                // Compatible 2 Way Antenna
                sql = @"select ProductId, ModelNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 3 
                    and WayRemote = '2Way' 
                    order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteComp2AntennaList.DataTextField = "ModelNumber";
                EditRemoteComp2AntennaList.DataValueField = "ProductId";
                EditRemoteComp2AntennaList.DataSource = ds;
                EditRemoteComp2AntennaList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Antenna
                    where b.ProductId_Remote = @ProductId
                        and a.ProductTypeId = 3
                        and a.WayRemote = '2Way' 
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditRemoteComp2AntennaListSelected.DataTextField = "PartNumber";
                EditRemoteComp2AntennaListSelected.DataValueField = "ProductId";
                EditRemoteComp2AntennaListSelected.DataSource = ds;
                EditRemoteComp2AntennaListSelected.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowEditAntenna(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentAntennaProduct.Text = PartNumber + " (Antenna)";
                EditAntennaModelNumber.Text = ModelNumber;
                EditAntennaPartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    AntennaPictureView.ImageUrl = SaveFolder + Picture1;
                    ProductAntennaFilepath.Value = Picture1;
                    AntennaPictureView.Visible = true;
                }
                else
                {
                    AntennaPictureView.ImageUrl = "";
                    ProductAntennaFilepath.Value = "";
                    AntennaPictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select WayRemote, TwoWayAntenna, OneWayAntenna, EstimatedRangeFt, FccIdUSA, FccIdCanada, RequiredAntennaCable,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport, DocumentURL,
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, Battery, EstimatedBatteryLifeDays
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["FccIdUSA"] != null && reader["FccIdUSA"].ToString() != "")
                        EditAntennaFccId.Text = reader["FccIdUSA"].ToString();
                    else
                        EditAntennaFccId.Text = "";

                    if (reader["FccIdCanada"] != null && reader["FccIdCanada"].ToString() != "")
                        EditAntennaICID.Text = reader["FccIdCanada"].ToString();
                    else
                        EditAntennaICID.Text = "";

                    if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        AntennaDocumentURL.Text = reader["DocumentURL"].ToString();
                    else
                        AntennaDocumentURL.Text = "";

                    if (reader["WayRemote"] != null && reader["WayRemote"].ToString() != "")
                        WayAntennaList.SelectedValue = reader["WayRemote"].ToString();

                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditAntennaStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditAntennaOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditAntennaOperatingVoltages.Text = "";

                    if (reader["EstimatedRangeFt"] != null && reader["EstimatedRangeFt"].ToString() != "")
                        EditAntennaRangeInFeet.Text = reader["EstimatedRangeFt"].ToString();
                    else
                        EditAntennaRangeInFeet.Text = "";

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditAntennaAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["IdleCurrent"] != null)
                        EditAntennaIdleCurrent.Text = reader["IdleCurrent"].ToString();
                    else
                        EditAntennaIdleCurrent.Text = "";

                    if (reader["OperatingTempF"] != null)
                        EditAntennaOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditAntennaOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditAntennaOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditAntennaOperatingTemperatureC.Text = "";

                    if (reader["RequiredAntennaCable"] != null && reader["RequiredAntennaCable"].ToString() != "")
                        EditAntennaRequiredCableList.Text = reader["RequiredAntennaCable"].ToString();
                    else
                        EditAntennaRequiredCableList.Text = "";
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesAntennaList.DataTextField = "ProductAlternateName";
                AlternateNamesAntennaList.DataValueField = "ProductAlternateName";
                AlternateNamesAntennaList.DataSource = ds;
                AlternateNamesAntennaList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 3 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaReplacePartNoList.DataTextField = "PartNumber";
                EditAntennaReplacePartNoList.DataValueField = "ProductId";
                EditAntennaReplacePartNoList.DataSource = ds;
                EditAntennaReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaReplacePartNoListSelected.DataTextField = "PartNumber";
                EditAntennaReplacePartNoListSelected.DataValueField = "ProductId";
                EditAntennaReplacePartNoListSelected.DataSource = ds;
                EditAntennaReplacePartNoListSelected.DataBind();


                // Compatible Remote Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompRemoteCurrentList.DataTextField = "PartNumber";
                EditAntennaCompRemoteCurrentList.DataValueField = "ProductId";
                EditAntennaCompRemoteCurrentList.DataSource = ds;
                EditAntennaCompRemoteCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Remote
                    where b.ProductId_Antenna = @ProductId
                        and a.ProductTypeId = 2
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompRemoteCurrentListSelected.DataTextField = "PartNumber";
                EditAntennaCompRemoteCurrentListSelected.DataValueField = "ProductId";
                EditAntennaCompRemoteCurrentListSelected.DataSource = ds;
                EditAntennaCompRemoteCurrentListSelected.DataBind();


                // Compatible Remote Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompRemoteDisList.DataTextField = "PartNumber";
                EditAntennaCompRemoteDisList.DataValueField = "ProductId";
                EditAntennaCompRemoteDisList.DataSource = ds;
                EditAntennaCompRemoteDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Remote
                    where b.ProductId_Antenna = @ProductId
                        and a.ProductTypeId = 2
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompRemoteDisListSelected.DataTextField = "PartNumber";
                EditAntennaCompRemoteDisListSelected.DataValueField = "ProductId";
                EditAntennaCompRemoteDisListSelected.DataSource = ds;
                EditAntennaCompRemoteDisListSelected.DataBind();


                // Compatible Brain Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompBrainCurrentList.DataTextField = "PartNumber";
                EditAntennaCompBrainCurrentList.DataValueField = "ProductId";
                EditAntennaCompBrainCurrentList.DataSource = ds;
                EditAntennaCompBrainCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Antenna = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompBrainCurrentListSelected.DataTextField = "PartNumber";
                EditAntennaCompBrainCurrentListSelected.DataValueField = "ProductId";
                EditAntennaCompBrainCurrentListSelected.DataSource = ds;
                EditAntennaCompBrainCurrentListSelected.DataBind();


                // Compatible Brain Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompBrainDisList.DataTextField = "PartNumber";
                EditAntennaCompBrainDisList.DataValueField = "ProductId";
                EditAntennaCompBrainDisList.DataSource = ds;
                EditAntennaCompBrainDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Antenna = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAntennaCompBrainDisListSelected.DataTextField = "PartNumber";
                EditAntennaCompBrainDisListSelected.DataValueField = "ProductId";
                EditAntennaCompBrainDisListSelected.DataSource = ds;
                EditAntennaCompBrainDisListSelected.DataBind();

                
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowEditAccessories(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentAccessoriesProduct.Text = PartNumber + " (Accessories)";
                EditAccessoriesModelNumber.Text = ModelNumber;
                EditAccessoriesPartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    AccessoriesPictureView.ImageUrl = SaveFolder + Picture1;
                    ProductAccessoriesFilepath.Value = Picture1;
                    AccessoriesPictureView.Visible = true;
                }
                else
                {
                    AccessoriesPictureView.ImageUrl = "";
                    ProductAccessoriesFilepath.Value = "";
                    AccessoriesPictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select WayRemote, TwoWayAntenna, OneWayAntenna, EstimatedRangeFt, FccIdUSA, FccIdCanada, RequiredAntennaCable,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport, DocumentURL,
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, Battery, EstimatedBatteryLifeDays
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["FccIdUSA"] != null && reader["FccIdUSA"].ToString() != "")
                        EditAccessoriesFccId.Text = reader["FccIdUSA"].ToString();
                    else
                        EditAccessoriesFccId.Text = "";

                    if (reader["FccIdCanada"] != null && reader["FccIdCanada"].ToString() != "")
                        EditAccessoriesICID.Text = reader["FccIdCanada"].ToString();
                    else
                        EditAccessoriesICID.Text = "";

                    if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        AccessoriesDocumentURL.Text = reader["DocumentURL"].ToString();
                    else
                        AccessoriesDocumentURL.Text = "";

                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditAccessoriesStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditAccessoriesOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditAccessoriesOperatingVoltages.Text = "";

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditAccessoriesAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["IdleCurrent"] != null)
                        EditAccessoriesIdleCurrent.Text = reader["IdleCurrent"].ToString();
                    else
                        EditAccessoriesIdleCurrent.Text = "";

                    if (reader["OperatingTempF"] != null)
                        EditAccessoriesOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditAccessoriesOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditAccessoriesOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditAccessoriesOperatingTemperatureC.Text = "";
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesAccessoriesList.DataTextField = "ProductAlternateName";
                AlternateNamesAccessoriesList.DataValueField = "ProductAlternateName";
                AlternateNamesAccessoriesList.DataSource = ds;
                AlternateNamesAccessoriesList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 4 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesReplacePartNoList.DataTextField = "PartNumber";
                EditAccessoriesReplacePartNoList.DataValueField = "ProductId";
                EditAccessoriesReplacePartNoList.DataSource = ds;
                EditAccessoriesReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesReplacePartNoListSelected.DataTextField = "PartNumber";
                EditAccessoriesReplacePartNoListSelected.DataValueField = "ProductId";
                EditAccessoriesReplacePartNoListSelected.DataSource = ds;
                EditAccessoriesReplacePartNoListSelected.DataBind();


                // Compatible Brain Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesCompBrainCurrentList.DataTextField = "PartNumber";
                EditAccessoriesCompBrainCurrentList.DataValueField = "ProductId";
                EditAccessoriesCompBrainCurrentList.DataSource = ds;
                EditAccessoriesCompBrainCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Accessories = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesCompBrainCurrentListSelected.DataTextField = "PartNumber";
                EditAccessoriesCompBrainCurrentListSelected.DataValueField = "ProductId";
                EditAccessoriesCompBrainCurrentListSelected.DataSource = ds;
                EditAccessoriesCompBrainCurrentListSelected.DataBind();


                // Compatible Brain Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesCompBrainDisList.DataTextField = "PartNumber";
                EditAccessoriesCompBrainDisList.DataValueField = "ProductId";
                EditAccessoriesCompBrainDisList.DataSource = ds;
                EditAccessoriesCompBrainDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Accessories = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditAccessoriesCompBrainDisListSelected.DataTextField = "PartNumber";
                EditAccessoriesCompBrainDisListSelected.DataValueField = "ProductId";
                EditAccessoriesCompBrainDisListSelected.DataSource = ds;
                EditAccessoriesCompBrainDisListSelected.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        protected void AddProductPanelButton_Click(object sender, EventArgs e)
        {
            AddProductTitle.Text = "Add Product";
            AddProductButton.Text = "Add";

            CloseWindows();
            NewProductPanel.Visible = true;
        }

        protected void AddProductButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (ProductTypeList.SelectedValue == "-1")
            {
                ShowError("Please select Product Type");
                ProductTypeList.Focus();
                return;
            }
            if (ModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                ModelNumber.Focus();
                return;
            }
            if (PartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                PartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);


            string PictureFileName1;
            string Prefix = "ProductBrain";

            if (ProductTypeList.SelectedValue == "1") // Control Brain
            {
                Prefix = "ProductBrain";
            }
            else if (ProductTypeList.SelectedValue == "2") // Remote
            {
                Prefix = "ProducRemote";
            }
            else if (ProductTypeList.SelectedValue == "3") // Antenna
            {
                Prefix = "ProducAntenna";
            }
            else if (ProductTypeList.SelectedValue == "4") // Accessories
            {
                Prefix = "ProducAccessories";
            }
            else if (ProductTypeList.SelectedValue == "6") // Drone
            {
                Prefix = "ProductDrone";
            }

            SaveFile(ProductPicture.PostedFile, FullPath, Prefix, ModelNumber.Text.Trim(), PartNumber.Text.Trim(), out PictureFileName1);

            /*
            if (AddProductTitle.Text == "Edit Product")
            {
                if (PictureFileName1 != "" && ProductPictureFilepath1.Value != "")
                {
                    if (File.Exists(FullPath + ProductPictureFilepath1.Value))
                    {
                        File.Delete(FullPath + ProductPictureFilepath1.Value);
                    }
                }
            }
            */

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (AddProductTitle.Text == "Add Product")
                {
                    string sql = @"insert into dbo.Product (ProductTypeId, ModelNumber, PartNumber, Picture1, UpdatedDt, UpdatedBy, [Status]) 
                        select @ProductTypeId, @ModelNumber, @PartNumber, @Picture1, getdate(), @UpdatedBy, 'Current'";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductTypeId", SqlDbType.Int).Value = int.Parse(ProductTypeList.SelectedValue);
                    cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = ModelNumber.Text.Trim();
                    cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = PartNumber.Text.Trim();
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    cmd.ExecuteNonQuery();

                    ShowInfo("Added Product successfully");

                    LoadProductList();

                    ProductTypeList.SelectedIndex = -1;
                    ModelNumber.Text = "";
                    PartNumber.Text = "";
                }
                /*
                else if (AddProductTitle.Text == "Edit Product")
                {
                    string sql = "update dbo.ProductMakeModelYear set ProductMakeId = @ProductMakeId, ProductModelId=@ProductModelId, ";
                    sql += "ProductYear=@ProductYear, UpdatedDt=getdate(), UpdatedBy= @UpdatedBy, Picture1=@Picture1, SearchCount=@SearchCount ";
                    sql += "where ProductMakeModelYearId = @ProductMakeModelYearId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    int SearchCount = 0;
                    if (AdjustSearchCountTxt.Text != "" && int.TryParse(AdjustSearchCountTxt.Text, out SearchCount))
                    {
                        SearchCount = int.Parse(AdjustSearchCountTxt.Text);
                    }

                    cmd.Parameters.Add("@ProductMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@ProductModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@ProductYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                    cmd.Parameters.Add("@SearchCount", SqlDbType.Int).Value = SearchCount;
                    cmd.Parameters.Add("@ProductMakeModelYearId", SqlDbType.Int).Value = int.Parse(ProductMakeModelYearIdHidden.Value);

                    cmd.ExecuteNonQuery();
                    ShowInfo("Updated Product successfully");

                    ProductMakeList.SelectedValue = MakeList.SelectedValue;
                    ChangeProductMakeGetYear(false, int.Parse(Year.Text));
                    //ProductYearList.SelectedValue = Year.Text;
                    ChangeProductYearGetModel(false, int.Parse(ModelList.SelectedValue));
                    //ProductModelList.SelectedValue = ModelList.SelectedValue;
                    SearchInfo(0);
                }
                */

                NewProductPanel.Visible = false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string ModelNumber, string PartNumber, out string FileName)
        {
            FileName = "";
            if (PFile != null && PFile.FileName != "")
            {
                ModelNumber = ModelNumber.Replace(" ", "").Replace("/", "").Replace(".", "").Replace("+", "").Replace("'", "").Replace("\'", "").Replace(",", "");
                PartNumber = PartNumber.Replace(" ", "").Replace("/", "").Replace(".", "").Replace("+", "").Replace("'", "").Replace("\'", "").Replace(",", "");

                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                filename = Prefix + "_" + ModelNumber + "_" + PartNumber + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }


        protected void CancelAddProductButton_Click(object sender, EventArgs e)
        {
            NewProductPanel.Visible = false;
        }

        private void SelectFromList(ListBox sourceList, ListBox destList)
        {
            int size1 = sourceList.Items.Count;
            int size2 = destList.Items.Count;
            bool found = false;
            for (int j = 0; j < size1; j++)
            {
                if (sourceList.Items[j].Selected)
                {
                    string id = sourceList.Items[j].Value;
                    found = false;
                    for (int i = 0; i < size2; i++)
                    {
                        if (id == destList.Items[i].Value)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        destList.Items.Add(new ListItem(sourceList.Items[j].Text, sourceList.Items[j].Value));
                }
            }
        }

        private void DeselectFromList(ListBox destList)
        {
            int size = destList.Items.Count;
            for (int i = size - 1; i >= 0; i--)
                if (destList.Items[i].Selected)
                    destList.Items.RemoveAt(i);
        }

        protected void EditBrainReplacePartNoAdd_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainReplacePartNoList, EditBrainReplacePartNoListSelected);
        }

        protected void EditBrainReplacePartNoDel_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainReplacePartNoListSelected);
        }

        protected void EditBrainConvenienceAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainConvenienceList, EditBrainConvenienceListSelected);
        }

        protected void EditBrainConvenienceDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainConvenienceListSelected);
        }

        protected void EditBrainRemoteStartAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainRemoteStartList, EditBrainRemoteStartListSelected);
        }

        protected void EditBrainRemoteStartDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainRemoteStartListSelected);
        }

        protected void EditBrainAlarmAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainAlarmList, EditBrainAlarmListSelected);
        }

        protected void EditBrainAlarmDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainAlarmListSelected);
        }

        protected void EditBrainInstallationAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainInstallationList, EditBrainInstallationListSelected);
        }

        protected void EditBrainInstallationDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainInstallationListSelected);
        }

        protected void EditBrainCompRemotesCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainCompRemotesCurrentList, EditBrainCompRemotesCurrentListSelected);
        }

        protected void EditBrainCompRemotesCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainCompRemotesCurrentListSelected);
        }

        protected void EditBrainCompRemotesDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainCompRemotesDisList, EditBrainCompRemotesDisListSelected);
        }

        protected void EditBrainCompRemotesDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainCompRemotesDisListSelected);
        }

        protected void EditBrainAccessoriesAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainAccessoriesList, EditBrainAccessoriesListSelected);
        }

        protected void EditBrainAccessoriesDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainAccessoriesListSelected);
        }

        protected void SaveFeatureButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string ProductBrainConvenienceIds = "";
                foreach (ListItem item in EditBrainConvenienceListSelected.Items)
                {
                    ProductBrainConvenienceIds += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductProductBrainConvenienceUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductBrainConvenienceIds != "")
                    ProductBrainConvenienceIds = ProductBrainConvenienceIds.Substring(0, ProductBrainConvenienceIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductBrainConvenienceIds", SqlDbType.VarChar, 4000).Value = ProductBrainConvenienceIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Remote Start
                string ProductBrainRemoteStartIds = "";
                foreach (ListItem item in EditBrainRemoteStartListSelected.Items)
                {
                    ProductBrainRemoteStartIds += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductProductBrainRemoteStartUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductBrainRemoteStartIds != "")
                    ProductBrainRemoteStartIds = ProductBrainRemoteStartIds.Substring(0, ProductBrainRemoteStartIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductBrainRemoteStartIds", SqlDbType.VarChar, 4000).Value = ProductBrainRemoteStartIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Alarm
                string ProductBrainAlarmIds = "";
                foreach (ListItem item in EditBrainAlarmListSelected.Items)
                {
                    ProductBrainAlarmIds += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductProductBrainAlarmUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductBrainAlarmIds != "")
                    ProductBrainAlarmIds = ProductBrainAlarmIds.Substring(0, ProductBrainAlarmIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductBrainAlarmIds", SqlDbType.VarChar, 4000).Value = ProductBrainAlarmIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Installation Flexibility
                string ProductBrainInstallationIds = "";
                foreach (ListItem item in EditBrainInstallationListSelected.Items)
                {
                    ProductBrainInstallationIds += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductProductBrainInstallationUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductBrainInstallationIds != "")
                    ProductBrainInstallationIds = ProductBrainInstallationIds.Substring(0, ProductBrainInstallationIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductBrainInstallationIds", SqlDbType.VarChar, 4000).Value = ProductBrainInstallationIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Features successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void SaveCompRemoteButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Remotes = "";
                foreach (ListItem item in EditBrainCompRemotesCurrentListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }
                foreach (ListItem item in EditBrainCompRemotesDisListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductBrainCompatibleRemoteUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Remotes != "")
                    Remotes = Remotes.Substring(0, Remotes.Length - 1);

                cmd.Parameters.Add("@ProductId_Brain", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Remotes", SqlDbType.VarChar, 4000).Value = Remotes;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Remotes successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void SaveAccessoriesButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Accessories = "";
                foreach (ListItem item in EditBrainAccessoriesListSelected.Items)
                {
                    Accessories += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductBrainCompatibleAccessoriesUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Accessories != "")
                    Accessories = Accessories.Substring(0, Accessories.Length - 1);

                cmd.Parameters.Add("@ProductId_Brain", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Accessories", SqlDbType.VarChar, 4000).Value = Accessories;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Accessories successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void SaveBrainProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@DocumentURL", SqlDbType.NVarChar, 3000).Value = BrainDocumentURL.Text.Trim();
                cmd.Parameters.Add("@TwoWayAntenna", SqlDbType.NVarChar, 20).Value = EditBrainTwoWayAntennaList.SelectedValue;
                cmd.Parameters.Add("@OneWayAntenna", SqlDbType.NVarChar, 20).Value = EditBrainOneWayAntennaList.SelectedValue;
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditBrainStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditBrainOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@BladeCompatible", SqlDbType.NVarChar, 20).Value = EditBrainBladeCompatible.SelectedValue;
                cmd.Parameters.Add("@DroneCompatible", SqlDbType.NVarChar, 20).Value = EditBrainDroneCompatible.SelectedValue;
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditBrainAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@IdleCurrent", SqlDbType.NVarChar, 20).Value = EditBrainIdleCurrent.Text.Trim();
                cmd.Parameters.Add("@Dataport", SqlDbType.NVarChar, 20).Value = EditBrainDataPort.SelectedValue;
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditBrainOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditBrainOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@AntennaPort4Pin", SqlDbType.NVarChar, 20).Value = EditBrainAntennaPort4Pin.SelectedValue;
                cmd.Parameters.Add("@AntennaPort6Pin", SqlDbType.NVarChar, 20).Value = EditBrainAntennaPort6Pin.SelectedValue;
                cmd.Parameters.Add("@WaterResistant", SqlDbType.NVarChar, 20).Value = EditBrainWaterResistantList.SelectedValue;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditBrainReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void SaveControlBrainButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditBrainModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditBrainModelNumber.Focus();
                return;
            }
            if (EditBrainPartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditBrainPartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditBrainProductPicture.PostedFile, FullPath, "ProductBrain", EditBrainModelNumber.Text.Trim(), EditBrainPartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += "UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditBrainModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditBrainPartNumber.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    ProductPictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductPictureFilepath.Value = PictureFileName1;
                    ProductPictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AlternateNamesAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNames.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNames.Focus();
                return;
            }

            AlternateNamesList.Items.Add(new ListItem(name, name));
            AlternateNames.Text = "";
        }

        protected void AlternateNamesDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesList);
        }

        protected void DeleteBrainButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        private void DeleteProduct()
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductDelete", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                tran.Commit();

                CloseWindows();
                LoadProductList();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void CloseWindows()
        {
            NewProductPanel.Visible = false;
            EditBrainPanel.Visible = false;
            EditRemotePanel.Visible = false;
            EditAntennaPanel.Visible = false;
            EditAccessoriesPanel.Visible = false;
            EditPackagePanel.Visible = false;
            WikiPanel.Visible = false;
        }

        protected void DeleteRemoteButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void DeleteAntennaButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void DeleteAccessoriesButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void SaveRemoteButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditRemoteModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditRemoteModelNumber.Focus();
                return;
            }
            if (EditRemotePartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditRemotePartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditRemoteProductPicture.PostedFile, FullPath, "ProducRemote", EditRemoteModelNumber.Text.Trim(), EditRemotePartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, FccIdUSA=@FccIdUSA, FccIdCanada=@FccIdCanada, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += " UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditRemoteModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditRemotePartNumber.Text.Trim();
                cmd.Parameters.Add("@FccIdUSA", SqlDbType.NVarChar, 30).Value = EditRemoteFccId.Text.Trim();
                cmd.Parameters.Add("@FccIdCanada", SqlDbType.NVarChar, 30).Value = EditRemoteICID.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    RemotePictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductRemoteFilepath.Value = PictureFileName1;
                    RemotePictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesRemoteList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AlternateNamesRemoteAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNamesRemtoe.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNamesRemtoe.Focus();
                return;
            }

            AlternateNamesRemoteList.Items.Add(new ListItem(name, name));
            AlternateNamesRemtoe.Text = "";
        }

        protected void AlternateNamesRemoteDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesRemoteList);
        }

        protected void EditRemoteReplacePartNoAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteReplacePartNoList, EditRemoteReplacePartNoListSelected);
        }

        protected void EditRemoteReplacePartNoDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteReplacePartNoListSelected);
        }

        protected void SaveRemoteProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@DocumentURL", SqlDbType.NVarChar, 3000).Value = RemoteDocumentURL.Text.Trim();
                cmd.Parameters.Add("@TwoWayAntenna", SqlDbType.NVarChar, 20).Value = EditRemoteTwoWayAntenna.Text.Trim();
                cmd.Parameters.Add("@OneWayAntenna", SqlDbType.NVarChar, 20).Value = EditRemoteOneWayAntenna.Text.Trim();
                cmd.Parameters.Add("@WayRemote", SqlDbType.NVarChar, 20).Value = WayRemoteList.SelectedValue;
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditRemoteStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditRemoteOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@EstimatedRangeFt", SqlDbType.NVarChar, 20).Value = EditRemoteRangeInFeet.Text.Trim();
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditRemoteAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@IdleCurrent", SqlDbType.NVarChar, 20).Value = EditRemoteIdleCurrent.Text.Trim();
                cmd.Parameters.Add("@Battery", SqlDbType.NVarChar, 20).Value = EditRemoteBatteryType.Text.Trim();
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditRemoteOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditRemoteOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@EstimatedBatteryLifeDays", SqlDbType.NVarChar, 50).Value = EditRemoteEstimatedBatteryLife.Text.Trim();
                cmd.Parameters.Add("@WaterResistant", SqlDbType.NVarChar, 20).Value = EditRemoteWaterResistantList.SelectedValue;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditRemoteReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }
                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditRemoteComp1AntennaAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteComp1AntennaList, EditRemoteComp1AntennaListSelected);
        }

        protected void EditRemoteComp1AntennaDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteComp1AntennaListSelected);
        }

        protected void EditRemoteComp2AntennaAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteComp2AntennaList, EditRemoteComp2AntennaListSelected);
        }

        protected void EditRemoteComp2AntennaDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteComp2AntennaListSelected);
        }

        protected void SaveRemoteCompAntennaButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Antennas = "";
                foreach (ListItem item in EditRemoteComp1AntennaListSelected.Items)
                {
                    Antennas += item.Value + ",";
                }
                foreach (ListItem item in EditRemoteComp2AntennaListSelected.Items)
                {
                    Antennas += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductRemoteCompatibleAntennaUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Antennas != "")
                    Antennas = Antennas.Substring(0, Antennas.Length - 1);

                cmd.Parameters.Add("@ProductId_Remote", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Antennas", SqlDbType.VarChar, 4000).Value = Antennas;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Antennas successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditRemoteConvenienceAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteConvenienceList, EditRemoteConvenienceListSelected);
        }

        protected void EditRemoteConvenienceDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteConvenienceListSelected);
        }

        protected void EditRemoteAuxiliaryAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteAuxiliaryList, EditRemoteAuxiliaryListSelected);
        }

        protected void EditRemoteAuxiliaryDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteAuxiliaryListSelected);
        }

        protected void EditRemoteProgrammableAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteProgrammableList, EditRemoteProgrammableListSelected);
        }

        protected void EditRemoteProgrammableDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteProgrammableListSelected);
        }

        protected void SaveRemoteFeatureButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string ProductRemoteConvenienceIds = "";
                foreach (ListItem item in EditRemoteConvenienceListSelected.Items)
                {
                    ProductRemoteConvenienceIds += item.Value + ",";
                }

                if (ProductRemoteConvenienceIds != "")
                    ProductRemoteConvenienceIds = ProductRemoteConvenienceIds.Substring(0, ProductRemoteConvenienceIds.Length - 1);

                SqlCommand cmd = new SqlCommand("proc_ProductProductRemoteConvenienceUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductRemoteConvenienceIds", SqlDbType.VarChar, 4000).Value = ProductRemoteConvenienceIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Auxiliary
                string ProductRemoteAuxiliaryIds = "";
                foreach (ListItem item in EditRemoteAuxiliaryListSelected.Items)
                {
                    ProductRemoteAuxiliaryIds += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductProductRemoteAuxiliaryUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductRemoteAuxiliaryIds != "")
                    ProductRemoteAuxiliaryIds = ProductRemoteAuxiliaryIds.Substring(0, ProductRemoteAuxiliaryIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductRemoteAuxiliaryIds", SqlDbType.VarChar, 4000).Value = ProductRemoteAuxiliaryIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Programming
                string ProductRemoteProgrammableIds = "";
                foreach (ListItem item in EditRemoteProgrammableListSelected.Items)
                {
                    ProductRemoteProgrammableIds += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductProductRemoteProgrammableUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductRemoteProgrammableIds != "")
                    ProductRemoteProgrammableIds = ProductRemoteProgrammableIds.Substring(0, ProductRemoteProgrammableIds.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductRemoteProgrammableIds", SqlDbType.VarChar, 4000).Value = ProductRemoteProgrammableIds;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Features successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditRemoteCompBrainAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteCompBrainCurrentList, EditRemoteCompBrainCurrentListSelected);
        }

        protected void EditRemoteCompBrainDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteCompBrainCurrentListSelected);
        }

        protected void EditRemoteCompBrainDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteCompBrainDisList, EditRemoteCompBrainDisListSelected);
        }

        protected void EditRemoteCompBrainDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteCompBrainDisListSelected);
        }

        protected void SaveRemoteCompBrainButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Brains = "";
                foreach (ListItem item in EditRemoteCompBrainCurrentListSelected.Items)
                {
                    Brains += item.Value + ",";
                }
                foreach (ListItem item in EditRemoteCompBrainDisListSelected.Items)
                {
                    Brains += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductRemoteCompatibleBrainUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Brains != "")
                    Brains = Brains.Substring(0, Brains.Length - 1);

                cmd.Parameters.Add("@ProductId_Remote", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Brains", SqlDbType.VarChar, 4000).Value = Brains;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Brains successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AlternateNamesAntennaAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNamesAntenna.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNamesAntenna.Focus();
                return;
            }

            AlternateNamesAntennaList.Items.Add(new ListItem(name, name));
            AlternateNamesAntenna.Text = "";

        }

        protected void AlternateNamesAntennaDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesAntennaList);
        }

        protected void SaveAntennaButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditAntennaModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditAntennaModelNumber.Focus();
                return;
            }
            if (EditAntennaPartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditAntennaPartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditAntennaProductPicture.PostedFile, FullPath, "ProducAntenna", EditAntennaModelNumber.Text.Trim(), EditAntennaPartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, FccIdUSA=@FccIdUSA, FccIdCanada=@FccIdCanada, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += " UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditAntennaModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditAntennaPartNumber.Text.Trim();
                cmd.Parameters.Add("@FccIdUSA", SqlDbType.NVarChar, 30).Value = EditAntennaFccId.Text.Trim();
                cmd.Parameters.Add("@FccIdCanada", SqlDbType.NVarChar, 30).Value = EditAntennaICID.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    AntennaPictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductAntennaFilepath.Value = PictureFileName1;
                    AntennaPictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesAntennaList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditAntennaReplacePartNoAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAntennaReplacePartNoList, EditAntennaReplacePartNoListSelected);
        }

        protected void EditAntennaReplacePartNoDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAntennaReplacePartNoListSelected);
        }

        protected void SaveAntennaProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@DocumentURL", SqlDbType.NVarChar, 3000).Value = AntennaDocumentURL.Text.Trim();
                cmd.Parameters.Add("@WayRemote", SqlDbType.NVarChar, 20).Value = WayAntennaList.SelectedValue;
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditAntennaStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditAntennaOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@EstimatedRangeFt", SqlDbType.NVarChar, 20).Value = EditAntennaRangeInFeet.Text.Trim();
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditAntennaAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@IdleCurrent", SqlDbType.NVarChar, 20).Value = EditAntennaIdleCurrent.Text.Trim();
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditAntennaOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditAntennaOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@RequiredAntennaCable", SqlDbType.NVarChar, 30).Value = EditAntennaRequiredCableList.SelectedValue;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditAntennaReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditAntennaCompRemoteCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAntennaCompRemoteCurrentList, EditAntennaCompRemoteCurrentListSelected);
        }

        protected void EditAntennaCompRemoteCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAntennaCompRemoteCurrentListSelected);
        }

        protected void EditAntennaCompRemoteDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAntennaCompRemoteDisList, EditAntennaCompRemoteDisListSelected);
        }

        protected void EditAntennaCompRemoteDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAntennaCompRemoteDisListSelected);
        }

        protected void SaveAntennaCompRemoteButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Remotes = "";
                foreach (ListItem item in EditAntennaCompRemoteCurrentListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }
                foreach (ListItem item in EditAntennaCompRemoteDisListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductAntennaCompatibleRemoteUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Remotes != "")
                    Remotes = Remotes.Substring(0, Remotes.Length - 1);

                cmd.Parameters.Add("@ProductId_Antenna", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Remotes", SqlDbType.VarChar, 4000).Value = Remotes;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Remotes successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditAntennaCompBrainCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAntennaCompBrainCurrentList, EditAntennaCompBrainCurrentListSelected);
        }

        protected void EditAntennaCompBrainCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAntennaCompBrainCurrentListSelected);
        }

        protected void EditAntennaCompBrainDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAntennaCompBrainDisList, EditAntennaCompBrainDisListSelected);
        }

        protected void EditAntennaCompBrainDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAntennaCompBrainDisListSelected);
        }

        protected void SaveAntennaCompBrainButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Brains = "";
                foreach (ListItem item in EditAntennaCompBrainCurrentListSelected.Items)
                {
                    Brains += item.Value + ",";
                }
                foreach (ListItem item in EditAntennaCompBrainDisListSelected.Items)
                {
                    Brains += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductAntennaCompatibleBrainUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Brains != "")
                    Brains = Brains.Substring(0, Brains.Length - 1);

                cmd.Parameters.Add("@ProductId_Antenna", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Brains", SqlDbType.VarChar, 4000).Value = Brains;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Brains successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AlternateNamesAccessoriesAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNamesAccessories.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNamesAccessories.Focus();
                return;
            }

            AlternateNamesAccessoriesList.Items.Add(new ListItem(name, name));
            AlternateNamesAccessories.Text = "";

        }

        protected void AlternateNamesAccessoriesDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesAccessoriesList);
        }

        protected void SaveAccessoriesInfoButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditAccessoriesModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditAccessoriesModelNumber.Focus();
                return;
            }
            if (EditAccessoriesPartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditAccessoriesPartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditAccessoriesProductPicture.PostedFile, FullPath, "ProducAccessories", EditAccessoriesModelNumber.Text.Trim(), EditAccessoriesPartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, FccIdUSA=@FccIdUSA, FccIdCanada=@FccIdCanada, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += " UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditAccessoriesModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditAccessoriesPartNumber.Text.Trim();
                cmd.Parameters.Add("@FccIdUSA", SqlDbType.NVarChar, 30).Value = EditAccessoriesFccId.Text.Trim();
                cmd.Parameters.Add("@FccIdCanada", SqlDbType.NVarChar, 30).Value = EditAccessoriesICID.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    AccessoriesPictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductAccessoriesFilepath.Value = PictureFileName1;
                    AccessoriesPictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesAccessoriesList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditAccessoriesReplacePartNoAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAccessoriesReplacePartNoList, EditAccessoriesReplacePartNoListSelected);
        }

        protected void EditAccessoriesReplacePartNoDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAccessoriesReplacePartNoListSelected);
        }

        protected void SaveAccessoriesProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@DocumentURL", SqlDbType.NVarChar, 3000).Value = AccessoriesDocumentURL.Text.Trim();
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditAccessoriesStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditAccessoriesOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditAccessoriesAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@IdleCurrent", SqlDbType.NVarChar, 20).Value = EditAccessoriesIdleCurrent.Text.Trim();
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditAccessoriesOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditAccessoriesOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditAccessoriesReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditAccessoriesCompBrainCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAccessoriesCompBrainCurrentList, EditAccessoriesCompBrainCurrentListSelected);
        }

        protected void EditAccessoriesCompBrainCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAccessoriesCompBrainCurrentListSelected);
        }

        protected void EditAccessoriesCompBrainDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditAccessoriesCompBrainDisList, EditAccessoriesCompBrainDisListSelected);
        }

        protected void EditAccessoriesCompBrainDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditAccessoriesCompBrainDisListSelected);
        }

        protected void SaveAccessoriesCompBrainButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Brains = "";
                foreach (ListItem item in EditAccessoriesCompBrainCurrentListSelected.Items)
                {
                    Brains += item.Value + ",";
                }
                foreach (ListItem item in EditAccessoriesCompBrainDisListSelected.Items)
                {
                    Brains += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductAccessoriesCompatibleBrainUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Brains != "")
                    Brains = Brains.Substring(0, Brains.Length - 1);

                cmd.Parameters.Add("@ProductId_Accessories", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Brains", SqlDbType.VarChar, 4000).Value = Brains;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Brains successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowEditPackage(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentPackageProduct.Text = PartNumber + " (Package)";
                EditPackageModelNumber.Text = ModelNumber;
                EditPackagePartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    PackagePictureView.ImageUrl = SaveFolder + Picture1;
                    ProductPackageFilepath.Value = Picture1;
                    PackagePictureView.Visible = true;
                }
                else
                {
                    PackagePictureView.ImageUrl = "";
                    ProductPackageFilepath.Value = "";
                    PackagePictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select WayRemote, TwoWayAntenna, OneWayAntenna, EstimatedRangeFt, FccIdUSA, FccIdCanada, RequiredAntennaCable,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport, DocumentURL,
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, Battery, EstimatedBatteryLifeDays
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditPackageStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditPackageOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditPackageOperatingVoltages.Text = "";

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditPackageAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["OperatingTempF"] != null)
                        EditPackageOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditPackageOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditPackageOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditPackageOperatingTemperatureC.Text = "";
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesPackageList.DataTextField = "ProductAlternateName";
                AlternateNamesPackageList.DataValueField = "ProductAlternateName";
                AlternateNamesPackageList.DataSource = ds;
                AlternateNamesPackageList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 4 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageReplacePartNoList.DataTextField = "PartNumber";
                EditPackageReplacePartNoList.DataValueField = "ProductId";
                EditPackageReplacePartNoList.DataSource = ds;
                EditPackageReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageReplacePartNoListSelected.DataTextField = "PartNumber";
                EditPackageReplacePartNoListSelected.DataValueField = "ProductId";
                EditPackageReplacePartNoListSelected.DataSource = ds;
                EditPackageReplacePartNoListSelected.DataBind();


                // Remote
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 2 order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageRemoteCurrentList.DataTextField = "PartNumber";
                EditPackageRemoteCurrentList.DataValueField = "ProductId";
                EditPackageRemoteCurrentList.DataSource = ds;
                EditPackageRemoteCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductPackageIncludeRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                    where b.ProductId_Package = @ProductId
                        and a.ProductTypeId = 2
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageRemoteCurrentListSelected.DataTextField = "PartNumber";
                EditPackageRemoteCurrentListSelected.DataValueField = "ProductId";
                EditPackageRemoteCurrentListSelected.DataSource = ds;
                EditPackageRemoteCurrentListSelected.DataBind();


                // Brain
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageBrainCurrentList.DataTextField = "PartNumber";
                EditPackageBrainCurrentList.DataValueField = "ProductId";
                EditPackageBrainCurrentList.DataSource = ds;
                EditPackageBrainCurrentList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductPackageIncludeBrain b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Package = @ProductId
                        and a.ProductTypeId = 1
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditPackageBrainCurrentListSelected.DataTextField = "PartNumber";
                EditPackageBrainCurrentListSelected.DataValueField = "ProductId";
                EditPackageBrainCurrentListSelected.DataSource = ds;
                EditPackageBrainCurrentListSelected.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void DeletePackageButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void SavePackageInfoButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditPackageModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditPackageModelNumber.Focus();
                return;
            }
            if (EditPackagePartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditPackagePartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditPackageProductPicture.PostedFile, FullPath, "ProducPackage", EditPackageModelNumber.Text.Trim(), EditPackagePartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += " UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditPackageModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditPackagePartNumber.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    PackagePictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductPackageFilepath.Value = PictureFileName1;
                    PackagePictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesPackageList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AlternateNamesPackageAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNamesPackage.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNamesPackage.Focus();
                return;
            }

            AlternateNamesPackageList.Items.Add(new ListItem(name, name));
            AlternateNamesPackage.Text = "";
        }

        protected void AlternateNamesPackageDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesPackageList);
        }

        protected void EditPackageReplacePartNoAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditPackageReplacePartNoList, EditPackageReplacePartNoListSelected);
        }

        protected void EditPackageReplacePartNoDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditPackageReplacePartNoListSelected);
        }

        protected void SavePackageProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditPackageStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditPackageOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditPackageAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditPackageOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditPackageOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditPackageReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void EditPackageRemoteCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditPackageRemoteCurrentList, EditPackageRemoteCurrentListSelected);
        }

        protected void EditPackageRemoteCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditPackageRemoteCurrentListSelected);
        }

        protected void EditPackageBrainCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditPackageBrainCurrentList, EditPackageBrainCurrentListSelected);
        }

        protected void EditPackageBrainCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditPackageBrainCurrentListSelected);
        }

        protected void SavePackageIncludedButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Remote
                string Remote = "";
                foreach (ListItem item in EditPackageRemoteCurrentListSelected.Items)
                {
                    Remote += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductPackageRemoteUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Remote != "")
                    Remote = Remote.Substring(0, Remote.Length - 1);

                cmd.Parameters.Add("@ProductId_Package", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Remote", SqlDbType.VarChar, 4000).Value = Remote;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                string Brains = "";
                foreach (ListItem item in EditPackageBrainCurrentListSelected.Items)
                {
                    Brains += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductPackageBrainUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Brains != "")
                    Brains = Brains.Substring(0, Brains.Length - 1);

                cmd.Parameters.Add("@ProductId_Package", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Brain", SqlDbType.VarChar, 4000).Value = Brains;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();


                tran.Commit();
                ShowInfo("Saved Included Remotes and Brain successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        /*
        private void ShowProductInfo(string ProductIdStr)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select ProductInfo from dbo.ProductWiki where ProductId=@ProductId and IsCurrent=1";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ProductInfoLabel.Text = reader["ProductInfo"].ToString();
                }
                else
                {
                    ProductInfoLabel.Text = "";
                }
                reader.Close();

                sql = @"select a.ProductWikiId, a.ProductInfo, a.UpdatedDt, a.Approved, ApprovedStr = case when a.Approved=1 then 'Yes' else 'No' end,
                        a.UpdatedBy, UpdatedUser = b.FirstName + ' ' + b.LastName
                    from dbo.ProductWiki a
                    join dbo.AspNetUsers b on a.UpdatedBy = b.Id 
                    where a.ProductId=@ProductId ";
                if (RoleNameHidden.Value.Contains("Office"))
                {
                    sql += "and a.UpdatedBy = @Updatedby ";

                }
                sql += "order by a.UpdatedDt desc";

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                if (RoleNameHidden.Value.Contains("Office"))
                {
                    cmd.Parameters.Add("@Updatedby", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                }

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ProductInfoList.DataSource = ds;
                ProductInfoList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditProductInfoButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select ProductInfo from dbo.ProductWiki where ProductId=@ProductId and IsCurrent=1";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ProductInfoEditor.Text = reader["ProductInfo"].ToString();
                }
                else
                {
                    ProductInfoEditor.Text = "";
                }
                reader.Close();

                ProductInfoEditor.Visible = true;
                ProductInfoSaveButton.Visible = true;
                ProductInfoCancelButton.Visible = true;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProductInfoSaveButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                tran = conn.BeginTransaction();

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    string sql1 = @"update dbo.ProductWiki set IsCurrent = 0 where ProductId = @ProductId and IsCurrent = 1";

                    SqlCommand cmd2 = new SqlCommand(sql1, conn);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Transaction = tran;

                    cmd2.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                    cmd2.ExecuteNonQuery();
                }

                string sql = @"insert into dbo.ProductWiki (ProductId, ProductInfo, UpdatedBy, UpdatedDt, Approved, IsCurrent) 
                        select @ProductId, @ProductInfo, @UpdatedBy, getdate(), @Approved, @IsCurrent";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductInfo", SqlDbType.NText).Value = ProductInfoEditor.Text;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    cmd.Parameters.Add("@Approved", SqlDbType.Int).Value = 1;
                    cmd.Parameters.Add("@IsCurrent", SqlDbType.Int).Value = 1;
                }
                else
                {
                    cmd.Parameters.Add("@Approved", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@IsCurrent", SqlDbType.Int).Value = 0;
                }

                cmd.ExecuteNonQuery();

                ShowInfo("Added Product Info successfully");

                ProductInfoEditor.Visible = false;
                ProductInfoSaveButton.Visible = false;
                ProductInfoCancelButton.Visible = false;

                tran.Commit();

                //ShowProductInfo(ProductIdHidden.Value);
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProductInfoCancelButton_Click(object sender, EventArgs e)
        {
            ProductInfoEditor.Visible = false;
            ProductInfoSaveButton.Visible = false;
            ProductInfoCancelButton.Visible = false;
        }

        protected void ProductInfoList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField Approved = e.Row.FindControl("Approved") as HiddenField;

                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (Approved.Value == "1" || RoleNameHidden.Value.Contains("Office"))
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this?');";
                        }
                    }
                }

                if (e.Row.Cells[5].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[5].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (Approved.Value == "1") // Approved 
                        {
                            deleteButton.Visible = false;
                        }
                        else 
                        {
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Note?');";
                        }
                    }
                }
            }
        }

        protected void ProductInfoList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    tran = conn.BeginTransaction();

                    string sql = @"update dbo.ProductWiki set IsCurrent = 0 where ProductId = @ProductId and IsCurrent = 1";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                    cmd.ExecuteNonQuery();

                    sql = @"update dbo.ProductWiki set IsCurrent = 1, Approved=1 where ProductWikiId = @ProductWikiId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@ProductWikiId", SqlDbType.Int).Value = int.Parse(ProductWikiId);

                    cmd.ExecuteNonQuery();

                    tran.Commit();

                    //ShowProductInfo(ProductIdHidden.Value);
                }
                catch (Exception ex)
                {
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "DeleteRow")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"delete from dbo.ProductWiki where ProductWikiId=@ProductWikiId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductWikiId", SqlDbType.Int).Value = int.Parse(ProductWikiId);

                    cmd.ExecuteNonQuery();

                    //ShowProductInfo(ProductIdHidden.Value);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "View")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"select UpdatedDt, ProductInfo from dbo.ProductWiki where ProductWikiId=@ProductWikiId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductWikiId", SqlDbType.Int).Value = int.Parse(ProductWikiId);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ProductInfoDateLabel.Text = reader["UpdatedDt"].ToString();
                        ProductInfoView2.Text = reader["ProductInfo"].ToString();
                    }
                    reader.Close();

                    sql = @"select UpdatedDt, ProductInfo from dbo.ProductWiki where ProductId=@ProductId and IsCurrent=1";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ProductInfoView1.Text = reader["ProductInfo"].ToString();
                    }
                    reader.Close();

                    ProductInfoComparePanel.Visible = true;
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }
        */

        protected void EditRemoteCompanionAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditRemoteCompanionList, EditRemoteCompanionListSelected);
        }

        protected void EditRemoteCompanionDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditRemoteCompanionListSelected);
        }

        protected void SaveRemoteCompanionButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Remotes = "";
                foreach (ListItem item in EditRemoteCompanionListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductRemoteCompanionUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Remotes != "")
                    Remotes = Remotes.Substring(0, Remotes.Length - 1);

                cmd.Parameters.Add("@ProductId_Remote", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Companions", SqlDbType.VarChar, 4000).Value = Remotes;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Companions successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddTextBoxButton_Click(object sender, EventArgs e)
        {
            SectionTitleTxt.Text = "";
            SectionDetailTxt.Text = "";
            EditProductWikiSectionDetailIdHidden.Value = "";
            EditProductWikiSectionIdHidden.Value = "";

            NewTextBoxPanel.Visible = true;
        }
        
        protected void CancelSectionButton_Click(object sender, EventArgs e)
        {
            NewTextBoxPanel.Visible = false;
        }

        protected void SaveSectionButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                if (SectionTitleTxt.Text.Trim() == "")
                {
                    ShowError("Please enter Section Title.");
                    SectionTitleTxt.Focus();
                    return;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();

                // Convenience
                string Remotes = "";
                foreach (ListItem item in EditRemoteCompanionListSelected.Items)
                {
                    Remotes += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductWikiSectionUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductWikiSectionTitle", SqlDbType.NVarChar, 200).Value = SectionTitleTxt.Text.Trim();
                cmd.Parameters.Add("@ProductWikiSectionInfo", SqlDbType.NText).Value = SectionDetailTxt.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                if (EditProductWikiSectionDetailIdHidden.Value != "")
                {
                    cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(EditProductWikiSectionDetailIdHidden.Value);
                }
                if (EditProductWikiSectionIdHidden.Value != "")
                {
                    cmd.Parameters.Add("@ProductWikiSectionId", SqlDbType.Int).Value = int.Parse(EditProductWikiSectionIdHidden.Value);
                }

                cmd.ExecuteNonQuery();

                NewTextBoxPanel.Visible = false;
                ShowProductWikiSectionList();
                ShowInfo("Saved Section successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ShowProductWikiSectionList()
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select a.ProductWikiSectionId, a.ProductWikiSectionDetailId, c.ProductWikiSectionTitle, a.UpdatedDt, 
                        a.Approved, ApprovedStr = case when a.Approved=1 then 'Yes' else 'No' end,
                        a.UpdatedBy, UpdatedUser = b.FirstName + ' ' + b.LastName
                    from dbo.ProductWikiSectionDetail a
                    join dbo.ProductWikiSection c on a.ProductWikiSectionId = c.ProductWikiSectionId
                    join dbo.AspNetUsers b on a.UpdatedBy = b.Id 
                    where c.ProductId=@ProductId ";
                /*
                if (RoleNameHidden.Value.Contains("Office"))
                {
                    sql += "and a.UpdatedBy = @Updatedby ";

                }
                */
                sql += "order by c.SortOrder, a.UpdatedDt";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                if (RoleNameHidden.Value.Contains("Office"))
                {
                    cmd.Parameters.Add("@Updatedby", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                }

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ProductWikiSectionList.DataSource = ds;
                ProductWikiSectionList.DataBind();

                ProductInfoComparePanel.Visible = false;

                if (RoleNameHidden.Value.Contains("Office"))
                {
                    ProductWikiSectionList.Columns[5].Visible = false;
                    ProductWikiSectionList.Columns[6].Visible = false;
                    ProductWikiSectionList.Columns[7].Visible = false;
                    ProductWikiSectionList.Columns[8].Visible = false;
                    ProductWikiSectionList.Columns[9].Visible = false;
                }
                else
                {
                    ProductWikiSectionList.Columns[5].Visible = true;
                    ProductWikiSectionList.Columns[6].Visible = true;
                    ProductWikiSectionList.Columns[7].Visible = true;
                    ProductWikiSectionList.Columns[8].Visible = true;
                    ProductWikiSectionList.Columns[9].Visible = true;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        string CurrentProductWikiSectionId = "-1";
        protected void ProductWikiSectionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ProductWikiSectionId = ((HiddenField)(e.Row.FindControl("ProductWikiSectionId"))).Value;
                HiddenField Approved = e.Row.FindControl("Approved") as HiddenField;

                if (ProductWikiSectionId == CurrentProductWikiSectionId)
                {
                    LinkButton upButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    LinkButton dnButton = e.Row.Cells[9].Controls[0] as LinkButton;

                    upButton.Visible = false;
                    dnButton.Visible = false;
                }
                CurrentProductWikiSectionId = ProductWikiSectionId;

                if (e.Row.Cells[6].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[6].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (Approved.Value == "1" && RoleNameHidden.Value.Contains("Office"))
                        {
                            approveButton.Visible = false;
                        }
                        else if (Approved.Value == "1" && RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.Visible = true;
                            approveButton.Text = "Deny";
                            approveButton.OnClientClick = "return confirm('Are you sure you want to deny section?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.Text = "Approve";
                            approveButton.Visible = true;
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve section?');";
                        }
                    }
                }

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        /*
                        if (Approved.Value == "1") // Approved 
                        {
                            deleteButton.Visible = false;
                        }
                        else
                        {
                        */
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this section?');";
                        /*
                        }
                        */
                    }
                }
            }
        }

        protected void ProductWikiSectionList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditSection")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiSectionDetailId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionDetailId"))).Value;
                    string ProductWikiSectionId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"select a.ProductWikiSectionInfo, c.ProductWikiSectionTitle
                    from dbo.ProductWikiSectionDetail a
                    join dbo.ProductWikiSection c on a.ProductWikiSectionId = c.ProductWikiSectionId
                    where a.ProductWikiSectionDetailId = @ProductWikiSectionDetailId ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionDetailId);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        SectionTitleTxt.Text = reader["ProductWikiSectionTitle"].ToString();
                        SectionDetailTxt.Text = reader["ProductWikiSectionInfo"].ToString();
                        EditProductWikiSectionDetailIdHidden.Value = ProductWikiSectionDetailId;
                        EditProductWikiSectionIdHidden.Value = ProductWikiSectionId;

                        NewTextBoxPanel.Visible = true;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string Approved = ((HiddenField)(List.Rows[idx].FindControl("Approved"))).Value;
                    string ProductWikiSectionId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionId"))).Value;
                    string ProductWikiSectionDetailId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionDetailId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    tran = conn.BeginTransaction();

                    if (Approved == "0")
                    {
                        string sql = @"update dbo.ProductWikiSectionDetail set Approved = 0 where ProductWikiSectionId = @ProductWikiSectionId and Approved = 1";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ProductWikiSectionId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionId);

                        cmd.ExecuteNonQuery();

                        sql = @"update dbo.ProductWikiSectionDetail set Approved=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where ProductWikiSectionDetailId = @ProductWikiSectionDetailId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionDetailId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        string sql = @"update dbo.ProductWikiSectionDetail set Approved=0 where ProductWikiSectionDetailId = @ProductWikiSectionDetailId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionDetailId);

                        cmd.ExecuteNonQuery();
                    }

                    tran.Commit();

                    ShowProductWikiSectionList();
                }
                catch (Exception ex)
                {
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "DeleteRow")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiSectionId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionId"))).Value;
                    string ProductWikiSectionDetailId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionDetailId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("proc_ProductWikiSectionDelete", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ProductWikiSectionId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionId);
                    cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionDetailId);

                    cmd.ExecuteNonQuery();

                    ShowProductWikiSectionList();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "View")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiSectionId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionId"))).Value;
                    string ProductWikiSectionDetailId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionDetailId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"select UpdatedDt, ProductWikiSectionInfo from dbo.ProductWikiSectionDetail where ProductWikiSectionDetailId=@ProductWikiSectionDetailId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductWikiSectionDetailId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionDetailId);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ProductInfoDateLabel.Text = reader["UpdatedDt"].ToString();
                        ProductInfoView2.Text = reader["ProductWikiSectionInfo"].ToString();
                    }
                    reader.Close();

                    sql = @"select a.UpdatedDt, ProductWikiSectionInfo from dbo.ProductWikiSectionDetail a
                        where ProductWikiSectionId=@ProductWikiSectionId and Approved=1";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@ProductWikiSectionId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionId);

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ProductInfoView1.Text = reader["ProductWikiSectionInfo"].ToString();
                    }
                    reader.Close();

                    ProductInfoComparePanel.Visible = true;
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Up" || e.CommandName == "Down")
            {
                SqlConnection conn = null;
                try
                {
                    int idx = Convert.ToInt32(e.CommandArgument);
                    GridView List = (GridView)sender;

                    string ProductWikiSectionId = ((HiddenField)(List.Rows[idx].FindControl("ProductWikiSectionId"))).Value;

                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("proc_ProductWikiSectionUpDn", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (e.CommandName == "Up")
                    {
                        cmd.Parameters.Add("@UpDn", SqlDbType.Int).Value = 1;
                    }
                    else
                    {
                        cmd.Parameters.Add("@UpDn", SqlDbType.Int).Value = 2;
                    }
                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                    cmd.Parameters.Add("@ProductWikiSectionId", SqlDbType.Int).Value = int.Parse(ProductWikiSectionId);

                    cmd.ExecuteNonQuery();

                    ShowProductWikiSectionList();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }

        private void ShowEditDrone(string ProductIdStr, string PartNumber, string ModelNumber, string Picture1)
        {
            SqlConnection Con = null;
            try
            {
                CurrentDroneProduct.Text = PartNumber + " (Drone)";
                EditDroneModelNumber.Text = ModelNumber;
                EditDronePartNumber.Text = PartNumber;

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                if (Picture1 != "")
                {
                    DronePictureView.ImageUrl = SaveFolder + Picture1;
                    ProductDroneFilepath.Value = Picture1;
                    DronePictureView.Visible = true;
                }
                else
                {
                    DronePictureView.ImageUrl = "";
                    ProductDroneFilepath.Value = "";
                    DronePictureView.Visible = false;
                }

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                // Specifications
                string sql = @"select 
                        [Status], OperatingVoltage, AvailableForWarranty,  
                        OperatingTempF, OperatingTempC, WaterResistant
                    from dbo.Product 
                    where ProductId=@ProductId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Status"] != null && reader["Status"].ToString() != "")
                        EditDroneStatusList.SelectedValue = reader["Status"].ToString();

                    if (reader["OperatingVoltage"] != null)
                        EditDroneOperatingVoltages.Text = reader["OperatingVoltage"].ToString();
                    else
                        EditDroneOperatingVoltages.Text = "";

                    if (reader["AvailableForWarranty"] != null && reader["AvailableForWarranty"].ToString() != "")
                        EditDroneAvailableForWarranty.SelectedValue = reader["AvailableForWarranty"].ToString();

                    if (reader["OperatingTempF"] != null)
                        EditDroneOperatingTemperatureF.Text = reader["OperatingTempF"].ToString();
                    else
                        EditDroneOperatingTemperatureF.Text = "";

                    if (reader["OperatingTempC"] != null)
                        EditDroneOperatingTemperatureC.Text = reader["OperatingTempC"].ToString();
                    else
                        EditDroneOperatingTemperatureC.Text = "";

                    if (reader["WaterResistant"] != null && reader["WaterResistant"].ToString() != "")
                        EditDroneWaterResistantList.SelectedValue = reader["WaterResistant"].ToString();
                }
                reader.Close();

                // Alternate Name
                sql = @"select b.ProductAlternateName
                    from dbo.ProductAlternateName b with (nolock) 
                    where b.ProductId = @ProductId
                    order by 1 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                AlternateNamesDroneList.DataTextField = "ProductAlternateName";
                AlternateNamesDroneList.DataValueField = "ProductAlternateName";
                AlternateNamesDroneList.DataSource = ds;
                AlternateNamesDroneList.DataBind();



                // Replacemenet Part Number
                sql = "select ProductId, PartNumber from dbo.Product with (nolock) where ProductTypeId = 6 and ProductId <> @ProductId order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneReplacePartNoList.DataTextField = "PartNumber";
                EditDroneReplacePartNoList.DataValueField = "ProductId";
                EditDroneReplacePartNoList.DataSource = ds;
                EditDroneReplacePartNoList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                    where b.ProductId = @ProductId
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneReplacePartNoListSelected.DataTextField = "PartNumber";
                EditDroneReplacePartNoListSelected.DataValueField = "ProductId";
                EditDroneReplacePartNoListSelected.DataSource = ds;
                EditDroneReplacePartNoListSelected.DataBind();

                // Compatible Brain Current
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Current' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneCompBrainCurrentList.DataTextField = "PartNumber";
                EditDroneCompBrainCurrentList.DataValueField = "ProductId";
                EditDroneCompBrainCurrentList.DataSource = ds;
                EditDroneCompBrainCurrentList.DataBind();

                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Drone = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Current'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneCompBrainCurrentListSelected.DataTextField = "PartNumber";
                EditDroneCompBrainCurrentListSelected.DataValueField = "ProductId";
                EditDroneCompBrainCurrentListSelected.DataSource = ds;
                EditDroneCompBrainCurrentListSelected.DataBind();

                // Compatible Brain Discontinued
                sql = @"select ProductId, PartNumber from dbo.Product with (nolock) 
                    where ProductTypeId = 1 and [Status] = 'Discontinued' order by 2";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneCompBrainDisList.DataTextField = "PartNumber";
                EditDroneCompBrainDisList.DataValueField = "ProductId";
                EditDroneCompBrainDisList.DataSource = ds;
                EditDroneCompBrainDisList.DataBind();


                sql = @"select a.ProductId, a.PartNumber 
                    from dbo.Product a with (nolock) 
                    join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Brain
                    where b.ProductId_Drone = @ProductId
                        and a.ProductTypeId = 1
                        and a.[Status] = 'Discontinued'
                    order by 2 ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdStr);

                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                EditDroneCompBrainDisListSelected.DataTextField = "PartNumber";
                EditDroneCompBrainDisListSelected.DataValueField = "ProductId";
                EditDroneCompBrainDisListSelected.DataSource = ds;
                EditDroneCompBrainDisListSelected.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        protected void DeleteDronButton_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void AlternateNamesDroneAddButton_Click(object sender, EventArgs e)
        {
            string name = AlternateNamesDrone.Text.Trim();
            if (name == "")
            {
                ShowError("Please enter name");
                AlternateNamesDrone.Focus();
                return;
            }

            AlternateNamesDroneList.Items.Add(new ListItem(name, name));
            AlternateNamesDrone.Text = "";
        }

        protected void AlternateNamesDroneDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(AlternateNamesDroneList);
        }

        protected void SaveDroneButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (EditDroneModelNumber.Text.Trim() == "")
            {
                ShowError("Please enter Model Number");
                EditDroneModelNumber.Focus();
                return;
            }
            if (EditDronePartNumber.Text.Trim() == "")
            {
                ShowError("Please enter Part Number");
                EditDronePartNumber.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string PictureFileName1;
            SaveFile(EditDroneProductPicture.PostedFile, FullPath, "ProducDrone", EditDroneModelNumber.Text.Trim(), EditDronePartNumber.Text.Trim(), out PictureFileName1);

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                string sql = @"update dbo.Product set ModelNumber=@ModelNumber, PartNumber=@PartNumber, ";
                if (PictureFileName1 != "")
                {
                    sql += " Picture1 = @Picture1, ";
                }
                sql += " UpdatedDt=getdate(), UpdatedBy=@UpdatedBy where ProductId = @ProductId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ModelNumber", SqlDbType.NVarChar, 100).Value = EditDroneModelNumber.Text.Trim();
                cmd.Parameters.Add("@PartNumber", SqlDbType.NVarChar, 100).Value = EditDronePartNumber.Text.Trim();
                if (PictureFileName1 != "")
                {
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                }
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                cmd.ExecuteNonQuery();

                LoadProductList();
                ProductList.SelectedValue = ProductIdHidden.Value;

                if (PictureFileName1 != "")
                {
                    DronePictureView.ImageUrl = SaveFolder + PictureFileName1;
                    ProductDroneFilepath.Value = PictureFileName1;
                    DronePictureView.Visible = true;
                }

                // Replacement Part Number
                string names = "";
                foreach (ListItem item in AlternateNamesDroneList.Items)
                {
                    names += item.Value + ",";
                }

                cmd = new SqlCommand("proc_ProductAlternateNameUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (names != "")
                    names = names.Substring(0, names.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Names", SqlDbType.VarChar, 4000).Value = names;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Search Field successfully");
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Product already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void EditDroneReplacePartNoAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditDroneReplacePartNoList, EditDroneReplacePartNoListSelected);
        }

        protected void EditDroneReplacePartNoDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditDroneReplacePartNoListSelected);
        }

        protected void SaveDroneProductSpecButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();
                tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("proc_ProductUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = EditDroneStatusList.SelectedValue;
                cmd.Parameters.Add("@OperatingVoltage", SqlDbType.NVarChar, 20).Value = EditDroneOperatingVoltages.Text.Trim();
                cmd.Parameters.Add("@AvailableForWarranty", SqlDbType.NVarChar, 20).Value = EditDroneAvailableForWarranty.SelectedValue;
                cmd.Parameters.Add("@OperatingTempF", SqlDbType.NVarChar, 20).Value = EditDroneOperatingTemperatureF.Text.Trim();
                cmd.Parameters.Add("@OperatingTempC", SqlDbType.NVarChar, 20).Value = EditDroneOperatingTemperatureC.Text.Trim();
                cmd.Parameters.Add("@WaterResistant", SqlDbType.NVarChar, 20).Value = EditDroneWaterResistantList.SelectedValue;

                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                // Replacement Part Number
                string ProductId_Replacements = "";
                foreach (ListItem item in EditDroneReplacePartNoListSelected.Items)
                {
                    ProductId_Replacements += item.Value + ",";
                }
                cmd = new SqlCommand("proc_ProductReplacementPartNumberUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (ProductId_Replacements != "")
                    ProductId_Replacements = ProductId_Replacements.Substring(0, ProductId_Replacements.Length - 1);

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Replacements", SqlDbType.VarChar, 4000).Value = ProductId_Replacements;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Updated Product Specifications successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void EditDroneCompBrainAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditDroneCompBrainCurrentList, EditDroneCompBrainCurrentListSelected);
        }

        protected void EditDroneCompBrainDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditDroneCompBrainCurrentListSelected);
        }

        protected void EditDroneCompBrainDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditDroneCompBrainDisList, EditDroneCompBrainDisListSelected);
        }

        protected void EditDroneCompBrainDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditDroneCompBrainDisListSelected);
        }

        protected void SaveDroneCompBrainButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Brains = "";
                foreach (ListItem item in EditDroneCompBrainCurrentListSelected.Items)
                {
                    Brains += item.Value + ",";
                }
                foreach (ListItem item in EditDroneCompBrainDisListSelected.Items)
                {
                    Brains += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductDroneCompatibleBrainUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Brains != "")
                    Brains = Brains.Substring(0, Brains.Length - 1);

                cmd.Parameters.Add("@ProductId_Drone", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Brains", SqlDbType.VarChar, 4000).Value = Brains;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Brains successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void EditBrainCompDronesCurrentAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainCompDronesCurrentList, EditBrainCompDronesCurrentListSelected);
        }

        protected void EditBrainCompDronesCurrentDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainCompDronesCurrentListSelected);
        }

        protected void EditBrainCompDronesDisAddButton_Click(object sender, EventArgs e)
        {
            SelectFromList(EditBrainCompDronesDisList, EditBrainCompDronesDisListSelected);
        }

        protected void EditBrainCompDronesDisDelButton_Click(object sender, EventArgs e)
        {
            DeselectFromList(EditBrainCompDronesDisListSelected);
        }

        protected void SaveCompDroneButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Convenience
                string Drones = "";
                foreach (ListItem item in EditBrainCompDronesCurrentListSelected.Items)
                {
                    Drones += item.Value + ",";
                }
                foreach (ListItem item in EditBrainCompDronesDisListSelected.Items)
                {
                    Drones += item.Value + ",";
                }

                SqlCommand cmd = new SqlCommand("proc_ProductBrainCompatibleDroneUpdate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tran;

                if (Drones != "")
                    Drones = Drones.Substring(0, Drones.Length - 1);

                cmd.Parameters.Add("@ProductId_Brain", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);
                cmd.Parameters.Add("@ProductId_Drones", SqlDbType.VarChar, 4000).Value = Drones;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                tran.Commit();
                ShowInfo("Saved Compatible Drones successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }
    }
}