﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using FirstechData.Models;
using System.Text;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;

namespace FirstechData.Admin
{
    public partial class User : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected void Page_Load()
        {
            if (!IsPostBack)
            {
                Master.ChangeMenuCss("UsersMenu");

                if (Request["Id"] == null || Request["Id"] == "")
                    return;

                UserId.Value = Request["Id"];
                LoadUser();
            }
        }

        private void LoadUser()
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(UserId.Value);

            Email.Text = user.Email;
            FirstName.Text = user.FirstName;
            LastName.Text = user.LastName;
            StoreName.Text = user.StoreName;
            Address.Text = user.Address;
            City.Text = user.City;
            State.Text = user.State;
            Zip.Text = user.ZipCode;
            EmailConfirmed.Text = user.EmailConfirmed.ToString();
            if (EmailConfirmed.Text == "False")
                ResendEmailValidButton.Visible = true;
            else
                ResendEmailValidButton.Visible = false;
            Approved.Text = user.Approved.ToString();
            if (Approved.Text == "False")
                ApproveButton.Text = "Approve";
            else
                ApproveButton.Text = "Disapprove";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select RegistrationDate, ApproveDate, RepZipCodes=isnull(RepZipCodes, ''),
                        RepCities = isnull(RepCities, ''), RepStates = isnull(RepStates, ''), EditorMTCRepcode = isnull(EditorMTCRepcode, ''),
                        Country = isnull(Country, '')
                    from dbo.UserProfile where UserId=@UserId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Country.Text = reader["Country"].ToString();

                    /*
                    ApprovedDate.Text = string.Format("{0:MM/dd/yyyy}", reader["ApproveDate"]);
                    ApproveDateHidden.Value = string.Format("{0:MM/dd/yyyy}", reader["ApproveDate"]);
                    */

                    RegistrationDate.Text = string.Format("{0:MM/dd/yyyy}", reader["RegistrationDate"]);
                    RegistrationDateHidden.Value = string.Format("{0:MM/dd/yyyy}", reader["RegistrationDate"]);
                    

                    RepZipCodes.Text = reader["RepZipCodes"].ToString();
                    RepCities.Text = reader["RepCities"].ToString();
                    RepStates.Text = reader["RepStates"].ToString();

                    EditorMTCRepcode.Text = reader["EditorMTCRepcode"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

            IList<string> roleNames = manager.GetRoles(UserId.Value);
            if (roleNames.Contains("Rep"))
            {
                RepPanel.Visible = true;
            }
            else
            {
                RepPanel.Visible = false;
            }

            if (roleNames.Contains("Editor"))
            {
                EditorPanel.Visible = true;
            }
            else
            {
                EditorPanel.Visible = false;
            }

        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }


        protected void UpdateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(UserId.Value);


            DateTime Registration;
            if (RegistrationDate.Text != RegistrationDateHidden.Value && DateTime.TryParse(RegistrationDate.Text, out Registration))
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = "update dbo.UserProfile set RegistrationDate=@RegistrationDate where UserId=@UserId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@RegistrationDate", SqlDbType.DateTime).Value = Registration;
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            if (RepPanel.Visible == true)
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"update dbo.UserProfile set RepZipCodes=@RepZipCodes,
                            RepCities=@RepCities, RepStates=@RepStates
                        where UserId=@UserId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@RepZipCodes", SqlDbType.NVarChar, 500).Value = RepZipCodes.Text.Trim();
                    cmd.Parameters.Add("@RepCities", SqlDbType.NVarChar, 500).Value = RepCities.Text.Trim();
                    cmd.Parameters.Add("@RepStates", SqlDbType.NVarChar, 500).Value = RepStates.Text.Trim();
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }

            }

            if (EditorPanel.Visible == true)
            {
                SqlConnection conn = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string sql = @"update dbo.UserProfile set EditorMTCRepcode=@EditorMTCRepcode
                        where UserId=@UserId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@EditorMTCRepcode", SqlDbType.NVarChar, 100).Value = EditorMTCRepcode.Text.Trim();
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }

            }

            SqlConnection conn2 = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn2 = new SqlConnection(connStr);
                conn2.Open();

                string sql = @"update dbo.UserProfile set Country=@Country
                        where UserId=@UserId";

                SqlCommand cmd = new SqlCommand(sql, conn2);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 50).Value = Country.Text.Trim();
                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 128).Value = user.Id;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn2 != null)
                    conn2.Close();
            }


            user.FirstName = FirstName.Text;
            user.LastName = LastName.Text;
            user.StoreName = StoreName.Text;
            user.Address = Address.Text;
            user.City = City.Text;
            user.State = State.Text;
            user.ZipCode = Zip.Text;

            IdentityResult result = manager.Update(user);
            if (result.Succeeded)
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/AdminUsers", Response);
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        protected async void ResendEmailValidButton_Click(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var user = await userStore.FindByIdAsync(UserId.Value);
            SendEmailValidation(user);

            ShowError("Sent validation email successfully.");
        }

        protected void ApproveButton_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(UserId.Value);
            user.Approved = !user.Approved;

            IdentityResult result = manager.Update(user);
            if (result.Succeeded)
            {
                if (user.Approved)
                {
                    SendApprovalEmail(user.Id, user.Email);
                }
                LoadUser();
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        protected async void ResetPasswdButton_Click(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            string newPasswd = "123456";
            string hashedNewPasswd = userManager.PasswordHasher.HashPassword(newPasswd);
            var user = await userStore.FindByIdAsync(UserId.Value);
            await userStore.SetPasswordHashAsync(user, hashedNewPasswd);
            await userStore.UpdateAsync(user);

            ShowError("Reset password successfully.");
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        private async void SendEmailValidation(ApplicationUser user)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            string code = manager.GenerateEmailConfirmationToken(user.Id);
            string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);

            StringBuilder mailContent = new StringBuilder();
            mailContent.AppendLine("<!doctype html><html><head><title>Thank you for applying for Firstech Data</title></head>");
            mailContent.AppendLine("<body style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: arial, helvetica, sans-serif; color: #222222; font-size: 15px; background: #F5F5F5; line-height: 125%;\">");
            mailContent.AppendLine("	<table class=\"container\" width=\"100%\" bgcolor=\"#F5F5F5\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse;\">");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\" class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("				<a href=\"http://www.firstechdata.com\" target=\"_blank\">");
            mailContent.AppendLine("					<img src=\"https://mlsvc01-prod.s3.amazonaws.com/b86ab128001/2e689763-98c4-4fd2-a038-facddf1e3b9c.png?ver=1466454321000\" alt=\"firstech data logo\" width=\"250\" height=\"87\">");
            mailContent.AppendLine("				</a></td></tr>");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\">");
            mailContent.AppendLine("				<table class=\"content\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"table-layout:fixed; box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC;\">");
            mailContent.AppendLine("					<tr><td class=\"content-cell\" align=\"left\" style=\"word-break:break-all; padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("							<p><b>Thank you registering for Firstech Data!</b></p>");
            mailContent.AppendLine("							<p><a class=\"confirmation-link\" href=\"" + callbackUrl + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Confirm My E-mail</a></p>");
            mailContent.AppendLine("							<p><font size='-1'>If you can't click on the link, just copy and paste it in your browser - " + callbackUrl + "</font></p>");
            mailContent.AppendLine("							<p>Please click the link above to confirm your e-mail so that we may begin reviewing your registration. Upon approval, you will receive confirmation that your account is active.</p>");
            mailContent.AppendLine("							<p>Please allow up to 2 business days for activation.</p>");
            mailContent.AppendLine("						</td></tr></table></td></tr>");
            mailContent.AppendLine("		<tr><td>&nbsp;</td></tr>");
            mailContent.AppendLine("		<tr><td align=\"center\">");
            mailContent.AppendLine("				<table class=\"content secondary\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC; color: #888888; font-size: 12px;\">");
            mailContent.AppendLine("					<tr><td class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            mailContent.AppendLine("							<p><a class=\"footer-link\" href=\"http://www.firstechdata.com\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">FirstechData.com</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Log In</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            mailContent.AppendLine("								<a class=\"footer-link\" href=\"http://firstechdata.com/Account/Register\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Register</a></p>");
            mailContent.AppendLine("							<p>Technical Support: (888)820-3690</p>");
            mailContent.AppendLine("							<p>Hours: Mon-Fri: 7AM-5PM PST</p>");
            mailContent.AppendLine("							<p>&copy; 2016 Firstech, LLC. All rights reserved. </p>");
            mailContent.AppendLine("						</td></tr></table></td></tr>");
            mailContent.AppendLine("	</table></body></html>");

            //await manager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");
            try
            { 
                await manager.SendEmailAsync(user.Id, "Confirm your E-mail", mailContent.ToString());
            }
            catch (Exception)
            {
                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                var admin = manager.FindByEmail(AdminEmail);

                string title = "Sending Failure - " + "Confirm your E-mail";
                string content = "Sending Failure - " + user.Email + "<br/>" + mailContent.ToString();
                await manager.SendEmailAsync(admin.Id, title, content);
            }
        }

        private void SendApprovalEmail(string userid, string email)
        {
            StringBuilder emailContent = new StringBuilder();

            emailContent.AppendLine("<!doctype html><html><head><title>Thank you for applying for Firstech Data</title></head>");
            emailContent.AppendLine("<body style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: arial, helvetica, sans-serif; color: #222222; font-size: 15px; background: #F5F5F5; line-height: 125%;\">");
            emailContent.AppendLine("    <table class=\"container\" width=\"100%\" bgcolor=\"#F5F5F5\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse;\">");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\" class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                <a href=\"http://www.firstechdata.com\" target=\"_blank\">");
            emailContent.AppendLine("                    <img src=\"https://mlsvc01-prod.s3.amazonaws.com/b86ab128001/2e689763-98c4-4fd2-a038-facddf1e3b9c.png?ver=1466454321000\" alt=\"firstech data logo\" width=\"250\" height=\"87\">");
            emailContent.AppendLine("                </a>");
            emailContent.AppendLine("            </td></tr>");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\">");
            emailContent.AppendLine("                <table class=\"content\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC;\">");
            emailContent.AppendLine("                    <tr><td class=\"content-cell\" align=\"left\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                            <p><b>Your account for Firstech Data has been approved!</b></p>");
            emailContent.AppendLine("                            <p><a class=\"confirmation-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Log Into Your Account</a></p>");
            emailContent.AppendLine("                            <p>Username: " + email + "</p>");
            emailContent.AppendLine("                            <p>Log into your account today and get connected to one of the most powerful tools in the 12-volt industry: <b>Firstech Data</b>!</p>");
            emailContent.AppendLine("                        </td></tr>");
            emailContent.AppendLine("                </table></td>");
            emailContent.AppendLine("        </tr>");
            emailContent.AppendLine("        <tr><td>&nbsp;</td></tr>");
            emailContent.AppendLine("        <tr><td align=\"center\">");
            emailContent.AppendLine("                <table class=\"content secondary\" width=\"600\" bgcolor=\"#FFFFFF\" style=\"box-shadow: 0px 0px 10px #CCCCCC; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; border: solid 1px #CCCCCC; color: #888888; font-size: 12px;\">");
            emailContent.AppendLine("                    <tr><td class=\"content-cell\" style=\"padding: 15px 30px 15px 30px;\">");
            emailContent.AppendLine("                            <p><a class=\"footer-link\" href=\"http://www.firstechdata.com\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">FirstechData.com</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            emailContent.AppendLine("                                <a class=\"footer-link\" href=\"http://firstechdata.com/Account/Login\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Log In</a>&nbsp;&nbsp;|&nbsp;&nbsp;");
            emailContent.AppendLine("                                <a class=\"footer-link\" href=\"http://firstechdata.com/Account/Register\" target=\"_blank\" style=\"text-decoration: underline; color: #888888; font-weight: bold;\">Register</a></p>");
            emailContent.AppendLine("                            <p>Technical Support: (888)820-3690</p>");
            emailContent.AppendLine("                            <p>Hours: Mon-Fri: 7AM-5PM PST</p>");
            emailContent.AppendLine("                            <p>&copy; 2016 Firstech, LLC. All rights reserved. </p>");
            emailContent.AppendLine("                        </td>");
            emailContent.AppendLine("                    </tr>");
            emailContent.AppendLine("</table></td></tr></table></body></html>");

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            try
            { 
                manager.SendEmailAsync(userid, "Your account for Firstech Data has been approved!", emailContent.ToString());
            }
            catch (Exception)
            {
                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                var admin = manager.FindByEmail(AdminEmail);

                string title = "Sending Failure - " + "Your account for Firstech Data has been approved!";
                string content = "Sending Failure - " + email + "<br/>" + emailContent.ToString();
                manager.SendEmailAsync(admin.Id, title, content);
            }
        }

        protected void CloseButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminUsers");
        }
    }
}