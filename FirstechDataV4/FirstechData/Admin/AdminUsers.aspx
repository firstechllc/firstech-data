﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminUsers.aspx.cs" Inherits="FirstechData.Admin.AdminUsers" Async="true" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="tabs row" data-tab style="width: 980px; white-space: nowrap;">
        <li class="tab-title active" runat="server" id="WiringPaneltab">
            <a href="#<%=AccountPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="AccountPanelHeader" Text="Account/Manage"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linksdisassemblytab">
            <a href="#<%=FeedPanel.ClientID%>" style="font-size: small; padding: 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="FeedPanelHeader" Text="Feed"></asp:Literal></a>
        </li>
    </ul>
    <div class="tabs-content edumix-tab-horz">
        <div class="content active" runat="server" id="AccountPanel">
            <div class="box">
                <div class="box-header bg-transparent">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-user"></i>
                        <span>Users</span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body " style="display: block;">
                    <div class="row">
                        <asp:Literal runat="server" ID="ErrorMessage" /><asp:HiddenField runat="server" ID="RoleHidden" />
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="FirstNameSearch" placeholder="First Name"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="LastNameSearch" placeholder="Last Name"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="StoreNameSearch" placeholder="Store Name"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="ProfileNameSearch" placeholder="Profile Name"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="EmailSearch" placeholder="Email"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="AddressSearch" placeholder="Address"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="CitySearch" placeholder="City"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="StateSearch" placeholder="State / Province"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="CountrySearch" placeholder="Country"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:TextBox runat="server" ID="WhereBuyProductSearch" placeholder="Where to Buy"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 col-xs-2"></div>
                        <div class="col-sm-2 col-xs-2">
                            <asp:Button runat="server" ID="SearchButton" CssClass="button tiny bg-black radius" Text="Search User" OnClick="SearchButton_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group form-horizontal" style="height: 40px">
                            <label for="ApprovedList" class="col-sm-1 col-xs-6 control-label">Approved:</label>
                            <div class="col-sm-2 col-xs-6">
                                <asp:DropDownList runat="server" ID="ApprovedList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ApprovedList_SelectedIndexChanged">
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="True" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label for="RoleList" class="col-sm-1 col-xs-6 control-label"><asp:Label runat="server" ID="UserTypeLabel" Text="User Type:"></asp:Label></label>
                            <div class="col-sm-2 col-xs-6">
                                <asp:DropDownList runat="server" ID="RoleList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="RoleList_SelectedIndexChanged">
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="User" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Rep" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Editor" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Office" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="Administrator" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label for="RoleList" class="col-sm-2 col-xs-6 control-label">Email Confirmed:</label>
                            <div class="col-sm-2 col-xs-6">
                                <asp:DropDownList runat="server" ID="EmailConfirmedList" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="EmailConfirmedList_SelectedIndexChanged">
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="True" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2 col-xs-6">
                                <asp:Button runat="server" ID="ExportButton" CssClass="button tiny bg-black radius" Text="Export" OnClick="ExportButton_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Panel runat="server" ID="ListPanel" ScrollBars="Horizontal">
                                <asp:GridView ID="UserList" runat="server" AutoGenerateColumns="False" CssClass="footable"
                                    AllowPaging="true" PageSize="20" OnPageIndexChanging="UserList_PageIndexChanging" AllowSorting="true" OnSorting="UserList_Sorting"
                                    OnRowDataBound="UserList_RowDataBound" OnRowDeleting="UserList_RowDeleting" OnSelectedIndexChanged="UserList_SelectedIndexChanged"
                                    OnRowCommand="UserList_RowCommand" OnRowEditing="UserList_RowEditing" OnRowCancelingEdit="UserList_RowCancelingEdit" OnRowUpdating="UserList_RowUpdating">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                                            <ItemTemplate>
                                                <asp:HyperLink runat="server" ID="EmailLink" Text='<%# Bind("Email") %>'></asp:HyperLink>
                                                <asp:HiddenField ID="UserId" runat="server" Value='<%# Bind("Id") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EmailEdit" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                                <asp:HiddenField ID="UserId2" runat="server" Value='<%# Bind("Id") %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                                            <ItemTemplate>
                                                <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="FirstNameEdit" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                            <ItemTemplate>
                                                <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="LastNameEdit" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Name" SortExpression="StoreName">
                                            <ItemTemplate>
                                                <asp:Label ID="StoreNameLabel" runat="server" Text='<%# Bind("StoreName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="StoreNameEdit" runat="server" Text='<%# Bind("StoreName") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="AddressLabel" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="AddressEdit" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City" SortExpression="City">
                                            <ItemTemplate>
                                                <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="CityEdit" runat="server" Text='<%# Bind("City") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State / Province" SortExpression="State">
                                            <ItemTemplate>
                                                <asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="StateEdit" runat="server" Text='<%# Bind("State") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Zip Code" SortExpression="ZipCode">
                                            <ItemTemplate>
                                                <asp:Label ID="ZipCodeLabel" runat="server" Text='<%# Bind("ZipCode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="ZipCodeEdit" runat="server" Text='<%# Bind("ZipCode") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Registration Date" >
                                            <ItemTemplate>
                                                <asp:Label ID="RegistrationDateLabel" runat="server" ></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="RegistrationDateText" runat="server" ></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="RegistrationDateHidden" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MTC Code" >
                                            <ItemTemplate>
                                                <asp:Label ID="EditorMTCRepcodeLabel" runat="server" ></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EditorMTCRepcodeText" runat="server" ></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="EditorMTCRepcodeHidden" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Where" >
                                            <ItemTemplate>
                                                <asp:Label ID="WhereBuyProductLabel" runat="server" ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="EmailConfirmed" HeaderText="Email Confirmed" SortExpression="EmailConfirmed" ReadOnly="true" />
                                        <asp:BoundField DataField="Approved" HeaderText="Approved" ItemStyle-Width="50px" SortExpression="Approved" ReadOnly="true" />
                                        <asp:CommandField ShowSelectButton="true" SelectText="Approve" ItemStyle-Width="50px" />
                                        <asp:CommandField ShowEditButton="true" EditText="Edit" ItemStyle-Width="50px" />
                                        <asp:CommandField ShowDeleteButton="true" DeleteText="Delete" ItemStyle-Width="50px" />
                                        <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Change" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="ChangeRoleButton1" Text="Change" CommandName="ChangeRole" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>&nbsp;
                                                <asp:LinkButton runat="server" ID="ChangeRoleButton2" Text="Change" CommandName="ChangeRole" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>&nbsp;
                                                <asp:LinkButton runat="server" ID="ChangeRoleButton3" Text="Change" CommandName="ChangeRole" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>&nbsp;
                                                <asp:LinkButton runat="server" ID="ChangeRoleButton4" Text="Change" CommandName="ChangeRole" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reset Pwd" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="ResetPasswordButton" Text="Reset Pwd" CommandName="ResetPassword" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="ResendValidationEmailButton" Text="Resend Email Validation" CommandName="SendValidationEmail" CommandArgument='<%# Bind("Id") %>' Font-Underline="true"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content" runat="server" id="FeedPanel">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <div class="row">
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="StartDateFeedSearch" placeholder="Start Date"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">~</div>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="EndDateFeedSearch" placeholder="End Date"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <asp:TextBox runat="server" ID="EmailFeedSearch" placeholder="User Email"></asp:TextBox>
                    <asp:TextBox runat="server" ID="WhereBuySearch" placeholder="Where You Buy"></asp:TextBox>
                </div>
                <div class="col-sm-5 col-xs-5">
                    <div class="row">
                        <div class="col-sm-4">
                            <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control" placeholder="Maker" AutoPostBack="true" OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-sm-4">
                            <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control" placeholder="Year" AutoPostBack="true" OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-sm-4">
                            <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control" placeholder="Model" AutoPostBack="true" ></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    <asp:Button runat="server" ID="SearchFeedButton" CssClass="button tiny bg-black radius" Text="Search" OnClick="SearchFeedButton_Click" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <asp:GridView ID="FeedList" runat="server" AutoGenerateColumns="False" CssClass="footable" Width="100%"
                        AllowPaging="true" PageSize="50" OnPageIndexChanging="FeedList_PageIndexChanging" OnRowDataBound="FeedList_RowDataBound" OnRowDeleting="FeedList_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Date" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" ItemStyle-Width="60px">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="DtLabel" Text='<%# Bind("dt") %>' />
                                    <asp:HiddenField runat="server" ID="FeedType" Value='<%# Bind("FeedType") %>' />
                                    <asp:HiddenField runat="server" ID="Id" Value='<%# Bind("Id") %>' />
                                    <asp:HiddenField runat="server" ID="IsReported" Value='<%# Bind("IsReported") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Feed" HeaderText="Feed" ReadOnly="true" HtmlEncode="false"/>
                            <asp:CommandField ShowDeleteButton="true" HeaderText="Delete" DeleteText="Delete" ItemStyle-Width="50px" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <div class="row">
        <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[id*=UserList]').footable();
        });
    </script>
</asp:Content>
