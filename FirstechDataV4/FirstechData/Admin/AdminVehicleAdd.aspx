﻿<%@ Page Title="Add Vehicle" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AdminVehicleAdd.aspx.cs" Inherits="FirstechData.Admin.AdminVehicleAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span></a></li>
        <li>Admin</li>
        <li>Code</li>
        <li>Vehicle</li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="fontello-bus"></i><span>Add Vehicle</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body " style="display: block;">
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorLabel" />
            </p>

            <div class="form-horizontal">
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="MakeList" CssClass="col-md-2 control-label">Make</asp:Label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="MakeList" CssClass="form-control" Width="250px" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="ModelList" CssClass="col-md-2 control-label">Model</asp:Label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ModelList" CssClass="form-control" Width="250px" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Year" CssClass="col-md-2 control-label">Year</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Year" CssClass="form-control" MaxLength="4" Width="250px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Year" Display="Dynamic"
                            CssClass="text-danger" ErrorMessage="The Year field is required." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="AddButton_Click" ID="AddButton" Text="Add" CssClass="button tiny bg-black radius" />
                        <asp:Button runat="server" OnClick="CancelButton_Click" ID="CancelButton" Text="Cancel" CssClass="button tiny bg-black radius" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
