﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminVehicleAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName ";
                sql += "from dbo.VehicleMake ";
                sql += "order by VehicleMakeName";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                MakeList.DataSource = ds;
                MakeList.DataValueField = "VehicleMakeId";
                MakeList.DataTextField = "VehicleMakeName";
                MakeList.DataBind();

                sql = "select VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleModel ";
                sql += "order by VehicleModelName";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                ModelList.DataSource = ds;
                ModelList.DataValueField = "VehicleModelId";
                ModelList.DataTextField = "VehicleModelName";
                ModelList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            ClearError();

            int Y;
            if (!int.TryParse(Year.Text, out Y))
            {
                ShowError("Please enter valid Year");
                Year.Focus();
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleMakeModelYear (VehicleMakeId, VehicleModelId, VehicleYear, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeId, @VehicleModelId, @VehicleYear, getdate(), @UpdatedBy";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                Response.Redirect("~/Admin/AdminVehicle");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AdminVehicle");
       }
    }
}