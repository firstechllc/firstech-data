﻿using FirstechData.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class AdminVehicleInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (roleNames.Contains("Rep"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                Master.ChangeMenuCss("VehicleMenu");
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                RoleNameHidden.Value = string.Join(",", roleNames);

                LoadVehicleMake();
                //ShowModelList(true);

                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                var user = manager.FindByEmail(Context.User.Identity.GetUserName());
                if (user != null)
                {
                    var roles = manager.GetRoles(user.Id);
                    if (roles.Contains("Editor"))
                    {
                        ImportButton.Visible = false;
                        ExportButton.Visible = false;
                        ApproveButton.Visible = false;
                    }
                }

                int id;
                if (Request["Id"] != null && int.TryParse(Request["Id"].ToString(), out id))
                {
                    FindVehicle(id);
                }
            }
        }

        private void FindVehicle(int id)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleModelId, VehicleYear, Picture1 ";
                sql += "from dbo.VehicleMakeModelYear WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = id;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    int VehicleMakeId, VehicleModelId, VehicleYear;

                    if (reader["VehicleMakeId"] != null && int.TryParse(reader["VehicleMakeId"].ToString(), out VehicleMakeId) &&
                        reader["VehicleModelId"] != null && int.TryParse(reader["VehicleModelId"].ToString(), out VehicleModelId) &&
                        reader["VehicleYear"] != null && int.TryParse(reader["VehicleYear"].ToString(), out VehicleYear))
                    {
                        VehicleMakeList.SelectedValue = VehicleMakeId.ToString();
                        ChangeVehicleMakeGetYear(false, VehicleYear);
                        ChangeVehicleYearGetModel(true, VehicleModelId);
                        SearchInfo(0);
                        NewVehiclePanel.Visible = false;
                    }
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void LoadVehicleMake()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select VehicleMakeId, VehicleMakeName from dbo.VehicleMake with (nolock)  ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                VehicleMakeList.DataTextField = "VehicleMakeName";
                VehicleMakeList.DataValueField = "VehicleMakeId";
                VehicleMakeList.DataSource = ds2;
                VehicleMakeList.DataBind();

                VehicleMakeList.Items.Insert(0, new ListItem("Make", "-1"));

                MakeList.DataTextField = "VehicleMakeName";
                MakeList.DataValueField = "VehicleMakeId";
                MakeList.DataSource = ds2;
                MakeList.DataBind();

                MakeList.Items.Insert(0, new ListItem("Make", "-1"));


                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0 order by SortOrder  ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                SearchInstallationTypeList.DataTextField = "InstallationTypeName";
                SearchInstallationTypeList.DataValueField = "InstallationTypeId";
                SearchInstallationTypeList.DataSource = ds2;
                SearchInstallationTypeList.DataBind();

                SearchInstallationTypeList.Items.Insert(0, new ListItem("", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowWireError(string error)
        {
            WireErrorLabel.Text = error;
            WireErrorLabel.Visible = true;
        }


        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ShowInfo(string info)
        {
            InfoLabel.Text = info;
            InfoLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
            WireErrorLabel.Visible = false;
        }
        protected void MakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowModelList(false);
        }

        protected void ShowModelList(bool showAll)
        {
            if (AddVehicleTitle.Text != "Link Vehicle" && !showAll)
                return;

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "";
                if (showAll)
                {
                    sql = "select distinct VehicleModelId, VehicleModelName ";
                    sql += "from dbo.VehicleModel WITH (NOLOCK)  ";
                    sql += " order by VehicleModelName ";
                }
                else
                {
                    sql = "select distinct a.VehicleModelId, VehicleModelName ";
                    sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                    sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                    if (AddVehicleTitle.Text == "Link Vehicle")
                        sql += "where a.VehicleMakeId = " + MakeList.SelectedValue;
                    sql += " order by VehicleModelName ";
                }

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                ModelList.DataTextField = "VehicleModelName";
                ModelList.DataValueField = "VehicleModelId";
                ModelList.DataSource = ds;
                ModelList.DataBind();

                ModelList.Items.Insert(0, new ListItem("Model", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        protected void VehicleMakeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeVehicleMakeGetYear(true, 0);
        }

        protected void VehicleModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchInfo(0);
            NewVehiclePanel.Visible = false;
        }

        private void ChangeVehicleMakeGetYear(bool refresh, int VehicleYear)
        {
            ClearError();
            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                VehicleYearList.Items.Clear();
                ShowHideResult(false);
                return;
            }

            VehicleModelList.Items.Clear();

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleYearList.DataTextField = "VehicleYear";
                VehicleYearList.DataValueField = "VehicleYear";
                VehicleYearList.DataSource = ds;
                VehicleYearList.DataBind();

                VehicleYearList.Items.Insert(0, new ListItem("Year", "-1"));

                if (VehicleYear > 0)
                {
                    VehicleYearList.SelectedValue = VehicleYear.ToString();
                }

                VehicleModelList.Items.Clear();

                if (refresh)
                {
                    ShowHideResult(false);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void VehicleYearList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeVehicleYearGetModel(true, 0);
        }

        private void ChangeVehicleYearGetModel(bool refresh, int VehicleModelId)
        { 
            ClearError();
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowHideResult(false);
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleModelId, VehicleModelName ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + VehicleMakeList.SelectedValue;
                sql += " and a.VehicleYear = " + VehicleYearList.SelectedValue;
                sql += " order by VehicleModelName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                VehicleModelList.DataTextField = "VehicleModelName";
                VehicleModelList.DataValueField = "VehicleModelId";
                VehicleModelList.DataSource = ds;
                VehicleModelList.DataBind();

                VehicleModelList.Items.Insert(0, new ListItem("Model", "-1"));

                if (VehicleModelId > 0)
                {
                    VehicleModelList.SelectedValue = VehicleModelId.ToString();
                }

                if (refresh)
                {
                    ShowHideResult(false);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowHideResult(bool show)
        {
            NoteTitleLabel.Visible = show;
            AddWirePanelButton.Visible = show;
            AddWireBatchButton.Visible = show;
            AddDisassemblyPanelButton.Visible = show;
            AddPrepPanelButton.Visible = show;
            AddRoutingPanelButton.Visible = show;
            AddProgrammingPanelButton.Visible = show;
            AddFBPanelButton.Visible = show;
            AddDocumentPanelButton.Visible = show;
            SearchInstallationTypePanel.Visible = show;
            InternalNotePanel.Visible = show;
            WireLabel.Visible = show;
            PictureLabel.Visible = show;
            VehiclePicture1.Visible = show;

            LastUpdatedDate.Visible = show;
            ReservedByLabel.Visible = show;
            CancelReserveButton.Visible = show;
            ReserveButton.Visible = show;

            WireList.Visible = show;
            WireListEditor.Visible = show;
            DisassemblyList.Visible = show;
            DisassemblyListEditor.Visible = show;
            FBList.Visible = show;
            FBListEditor.Visible = show;
            DocumentList.Visible = show;
            DocumentListEditor.Visible = show;
            NoteList.Visible = show;
            NoteListEditor.Visible = show;
            CommentList.Visible = show;
            InternalNoteList.Visible = show;
            PrepList.Visible = show;
            PrepListEditor.Visible = show;
            RoutingList.Visible = show;
            RoutingListEditor.Visible = show;
            ProgrammingList.Visible = show;
            ProgrammingListEditor.Visible = show;

            if (!show)
            {
                WiringHeader.Text = "Vehicle Wiring";
                DisassemblyHeader.Text = "Disassembly";
                FBResultHeader.Text = "Facebook Result";
                PrepHeader.Text = "Prep";
                RoutingHeader.Text = "Routing/Placement";
                ProgrammingHeader.Text = "Programming";
                TitlePanel.Visible = false;
                UrgentMessagePanel.Visible = false;
                VehicleMakeModelYearIdHidden.Value = "";

                NewVehiclePanel.Visible = false;
                AddWirePanel.Visible = false;
                EditNotePanel.Visible = false;
                AddDisassemblyPanel.Visible = false;
                AddDocumentPanel.Visible = false;
                AddPrepPanel.Visible = false;
                AddRoutingPanel.Visible = false;
                AddProgrammingPanel.Visible = false;
                AddFBPanel.Visible = false;
            }
        }

        private void ChangeVehicleTitle()
        {
            string title = VehicleMakeList.SelectedItem.Text + " " + VehicleModelList.SelectedItem.Text + " " + VehicleYearList.SelectedItem.Text + " (" + VehicleMakeModelYearIdHidden.Value + ") ";
            if (Approved.Value == "1")
            {
                title += "- Approved";
                ApproveButton.Text = "Change Draft";
            }
            else
            {
                title += "- Draft";
                ApproveButton.Text = "Approve";
            }
            CurrentVehicle.Text = title;
        }

        protected void SearchInfo(int tabIdx)
        {
            ClearError();

            if (VehicleMakeList.SelectedIndex < 0 || VehicleMakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make.");
                ShowHideResult(false);
                return;
            }
            if (VehicleModelList.SelectedIndex < 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model.");
                ShowHideResult(false);
                return;
            }
            if (VehicleYearList.SelectedIndex < 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowError("Please select Year.");
                ShowHideResult(false);
                return;
            }

            if (tabIdx == 0)
            {
                ChangeVehicleTitle();
                TitlePanel.Visible = true;
                UrgentMessagePanel.Visible = true;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string SaveFolder = SaveFolderHidden.Value;

                string sql = "select VehicleMakeModelYearId, Approved, Picture1, SearchCount=isnull(SearchCount, 0) ";
                sql += "from dbo.VehicleMakeModelYear where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear";
                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(VehicleMakeList.SelectedValue);
                cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(VehicleModelList.SelectedValue);
                cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(VehicleYearList.SelectedValue);

                VehiclePictureFilepath1.Value = "";
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleMakeModelYearIdHidden.Value = reader["VehicleMakeModelYearId"].ToString();
                    Approved.Value = reader["Approved"].ToString();
                    if (reader["Picture1"] != null && reader["Picture1"].ToString() != "")
                    {
                        VehiclePicture1.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                        VehiclePictureFilepath1.Value = reader["Picture1"].ToString();
                        VehiclePicture1.Visible = true;
                    }
                    else
                    {
                        VehiclePicture1.ImageUrl = "";
                        VehiclePicture1.Visible = false;
                    }

                    ChangeVehicleTitle();
                    AdjustSearchCountTxt.Text = reader["SearchCount"].ToString();
                }
                else
                {
                    reader.Close();
                    return;
                }
                reader.Close();

                sql = "proc_SearchCounterSearch";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@Number", SqlDbType.Int).Value = 100;
                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    SearchCountTxt.Text = reader["Counter"].ToString();
                }
                reader.Close();


                PictureLabel.Visible = true;

                ShowHideResult(true);

                // Check Reserved
                sql = @"select ReservedAt=dateadd(day, 3, v.ReservedAt), ReservedBy = isnull(v.ReservedBy, ''), ProfileName=isnull(au.ProfileName, '')
                    from dbo.VehicleMakeModelYear v
                    left join dbo.UserProfile au with (nolock) on v.ReservedBy = au.UserId
                    where v.VehicleMakeModelYearId = @VehicleMakeModelYearId
                        and dateadd(day, 3, v.ReservedAt) > getdate() ";

                cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                string ReservedBy = "";
                string ReservedProfileName = "";
                DateTime ReservedAt = DateTime.MinValue;
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ReservedBy = reader["ReservedBy"].ToString().Trim();
                    ReservedProfileName = reader["ProfileName"].ToString().Trim();
                    if (ReservedBy != "")
                    {
                        ReservedAt = DateTime.Parse(reader["ReservedAt"].ToString());
                    }
                }
                reader.Close();

                bool ReservedAndCanEdit = false;
                if (ReservedBy != "")
                {
                    ReservedByLabel.Text = "Reserved by " + ReservedProfileName + " (Expires at " + ReservedAt.ToString() + ")";
                    if (RoleNameHidden.Value.Contains("Administrator") || (RoleNameHidden.Value.Contains("Editor") && ReservedBy == User.Identity.GetUserId()))
                    {
                        ReservedAndCanEdit = true;
                        CancelReserveButton.Visible = true;
                        ReserveButton.Visible = false;
                    }
                    else
                    {
                        CancelReserveButton.Visible = false;
                        ReserveButton.Visible = false;
                    }
                }
                else
                {
                    ReservedAndCanEdit = true;
                    ReservedByLabel.Text = "";
                    CancelReserveButton.Visible = false;
                    ReserveButton.Visible = true;
                }


                // Link Vehicle
                sql = "select m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, l.LinkVehicleMakeModelYearId, ";
                sql += "v.VehicleMakeId, v.VehicleModelId ";
                sql += "from dbo.VehicleLink l WITH (NOLOCK) ";
                sql += "join dbo.VehicleMakeModelYear v WITH (NOLOCK) on l.LinkVehicleMakeModelYearId = v.VehicleMakeModelYearId ";
                sql += "join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId ";
                sql += "join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId ";
                sql += "where l.VehicleMakeModelYearId = @VehicleMakeModelYearId";

                SqlCommand linkCmd = new SqlCommand(sql, Con);
                linkCmd.CommandType = CommandType.Text;

                linkCmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                reader = linkCmd.ExecuteReader();
                if (reader.Read())
                {
                    LinkVehicle.Text = "(Link: " + reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString() + ")";
                    LinkVehicleMakeModelYearIdHidden.Value = reader["LinkVehicleMakeModelYearId"].ToString();
                    LinkVehicleMakeIdHidden.Value = reader["VehicleMakeId"].ToString();
                    LinkVehicleModelIdHidden.Value = reader["VehicleModelId"].ToString();
                    LinkVehicleYearHidden.Value = reader["VehicleYear"].ToString();
                }
                else
                {
                    LinkVehicle.Text = "";
                    LinkVehicleMakeModelYearIdHidden.Value = "";
                    LinkVehicleMakeIdHidden.Value = "";
                    LinkVehicleModelIdHidden.Value = "";
                    LinkVehicleYearHidden.Value = "";
                }
                reader.Close();

                if (!ReservedAndCanEdit)
                {
                    LinkVehicleButton.Visible = false;
                    EditVehicleButton.Visible = false;
                }

                if (!RoleNameHidden.Value.Contains("Administrator"))
                {
                    DeleteVehicleButton.Visible = false;
                }

                if (tabIdx == 0)
                {
                    sql = GetUrgentQuery();

                    SqlCommand Cmd = new SqlCommand(sql, Con);
                    Cmd.CommandType = CommandType.Text;

                    reader = Cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        UrgentMessageLabel.Text = reader["Note"].ToString();
                        UrgentMessageEdit.Text = reader["Note"].ToString();
                    }
                    else
                    {
                        UrgentMessageLabel.Text = "";
                        UrgentMessageEdit.Text = "";
                    }
                    reader.Close();

                    if (!ReservedAndCanEdit)
                    {
                        EditUrgentMessageButton.Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 1)
                {
                    ShowWireList(Con, SaveFolder);
                    ShowWireListEditor(Con, SaveFolder);

                    WireLabel.Visible = true;
                    SearchInstallationTypePanel.Visible = true;
                    InternalNotePanel.Visible = true;

                    ShowCommentList();
                    ShowInternalNotes();

                    AddWirePanel.Visible = false;
                    EditNotePanel.Visible = false;

                    ShowNoteList(Con);
                    ShowNoteListEditor(Con);



                    cmd = new SqlCommand("proc_VehicleUpdateDate", Con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        LastUpdatedDate.Text = "Last Updated: " + reader["UpdatedDt"].ToString();
                        LastUpdatedDate.Visible = true;
                    }
                    else
                    {
                        LastUpdatedDate.Visible = false;
                    }
                    reader.Close();

                    /*

                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Note"] != null)
                        {
                            NoteLabelTitle.Text = "Note: ";
                            NoteLabel.Text = reader["Note"].ToString();
                            editor.Text = reader["Note"].ToString();
                        }
                        else
                        {
                            NoteLabelTitle.Text = "Note: ";
                            NoteLabel.Text = "";
                        }

                        VehicleWireNoteIdHidden.Value = reader["VehicleWireNoteId"].ToString();
                    }
                    else
                    {
                        VehicleWireNoteIdHidden.Value = "";
                        NoteLabelTitle.Text = "Note: ";
                        NoteLabel.Text = "";
                    }
                    reader.Close();
                    */

                    if (!ReservedAndCanEdit)
                    {
                        AddWirePanelButton.Visible = false;
                        AddWireBatchButton.Visible = false;
                        InternalNotePanel.Visible = false;
                        WireList.Columns[8].Visible = false;
                        WireList.Columns[9].Visible = false;
                    }
                    else
                    {
                        WireList.Columns[8].Visible = true;
                        WireList.Columns[9].Visible = true;
                    }
                }

                if (tabIdx == 0 || tabIdx == 2)
                {
                    ShowFBList(Con, SaveFolder);
                    ShowFBListEditor(Con, SaveFolder);

                    if (!ReservedAndCanEdit)
                    {
                        AddFBPanelButton.Visible = false;
                        FBList.Columns[2].Visible = false;
                        FBList.Columns[3].Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 3)
                {
                    ShowDocumentList(Con);
                    ShowDocumentListEditor(Con);

                    if (!ReservedAndCanEdit)
                    {
                        AddDocumentPanelButton.Visible = false;
                        DocumentList.Columns[1].Visible = false;
                        DocumentList.Columns[2].Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 4)
                {
                    ShowDisassemblyList(Con, SaveFolder);
                    ShowDisassemblyListEditor(Con, SaveFolder);

                    if (!ReservedAndCanEdit)
                    {
                        AddDisassemblyPanelButton.Visible = false;
                        DisassemblyList.Columns[3].Visible = false;
                        DisassemblyList.Columns[4].Visible = false;
                    }
                }

                if (tabIdx == 0 || tabIdx == 5)
                {
                    ShowPrepList(Con, SaveFolder);
                    ShowPrepListEditor(Con, SaveFolder);

                    if (!ReservedAndCanEdit)
                    {
                        AddPrepPanelButton.Visible = false;
                        PrepList.Columns[3].Visible = false;
                        PrepList.Columns[4].Visible = false;
                    }
                }

                // Routing
                if (tabIdx == 0 || tabIdx == 6)
                {
                    ShowRoutingList(Con, SaveFolder);
                    ShowRoutingListEditor(Con, SaveFolder);

                    if (!ReservedAndCanEdit)
                    {
                        AddRoutingPanelButton.Visible = false;
                        RoutingList.Columns[3].Visible = false;
                        RoutingList.Columns[4].Visible = false;
                    }
                }

                // Programming
                if (tabIdx == 0 || tabIdx == 7)
                {
                    ShowProgrammingList(Con, SaveFolder);
                    ShowProgrammingListEditor(Con, SaveFolder);

                    if (!ReservedAndCanEdit)
                    {
                        AddProgrammingPanelButton.Visible = false;
                        ProgrammingList.Columns[3].Visible = false;
                        ProgrammingList.Columns[4].Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void PrepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Prep?');";
                    }
                }
            }
        }

        protected void DisassemblyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Disassembly?');";
                    }
                }
            }
        }
        protected void RoutingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Routing/Plancement?');";
                    }
                }
            }
        }

        private void HideNoImageLink(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink a1 = ((HyperLink)(e.Row.FindControl("Image1Link")));
                HyperLink a2 = ((HyperLink)(e.Row.FindControl("Image2Link")));
                HyperLink a3 = ((HyperLink)(e.Row.FindControl("Image3Link")));
                HyperLink a4 = ((HyperLink)(e.Row.FindControl("Image4Link")));
                HyperLink a5 = ((HyperLink)(e.Row.FindControl("Image5Link")));

                if (a1.ImageUrl == "")
                {
                    a1.Visible = false;
                }
                if (a2.ImageUrl == "")
                {
                    a2.Visible = false;
                }
                if (a3.ImageUrl == "")
                {
                    a3.Visible = false;
                }
                if (a4.ImageUrl == "")
                {
                    a4.Visible = false;
                }
                if (a5.ImageUrl == "")
                {
                    a5.Visible = false;
                }
            }
        }

        protected void AddVehiclePanelButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Add Vehicle";
            AddVehicleButton.Text = "Add";

            UnlinkVehicleButton.Visible = false;

            MakeList.SelectedIndex = -1;
            ModelList.SelectedIndex = -1;

            YearList.Visible = false;
            YearList.SelectedIndex = -1;
            Year.Visible = true;
            Year.Text = "";

            ShowModelList(true);
            NewVehiclePanel.Visible = true;
            VehiclePicturePanel.Visible = true;
        }

        protected void CancelAddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();
            NewVehiclePanel.Visible = false;
        }

        protected void AddVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (MakeList.SelectedValue == "-1")
            {
                ShowError("Please select Make");
                return;
            }
            if (ModelList.SelectedValue == "-1")
            {
                ShowError("Please select Model");
                return;
            }
            if (YearList.Visible && YearList.SelectedValue == "-1")
            {
                ShowError("Please select Year");
                return;
            }

            int year = 0;
            if (Year.Visible && !int.TryParse(Year.Text, out year))
            {
                ShowError("Please enter year");
                Year.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleMake = MakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string VehicleModel = ModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string VehicleYear = "";
            if (YearList.Visible)
            {
                VehicleYear = YearList.SelectedValue;
            }
            else
            {
                VehicleYear = Year.Text;
            }

            string PictureFileName1;
            SaveFile(VehiclePicture.PostedFile, FullPath, "Picture", VehicleMake, VehicleModel, VehicleYear, out PictureFileName1);

            if (AddVehicleTitle.Text == "Edit Vehicle")
            {
                if (PictureFileName1 != "" && VehiclePictureFilepath1.Value != "")
                {
                    if (File.Exists(FullPath + VehiclePictureFilepath1.Value))
                    {
                        File.Delete(FullPath + VehiclePictureFilepath1.Value);
                    }
                }
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (AddVehicleTitle.Text == "Add Vehicle")
                {
                    string sql = "insert into dbo.VehicleMakeModelYear (VehicleMakeId, VehicleModelId, VehicleYear, UpdatedDt, UpdatedBy, Picture1, SearchCount) ";
                    sql += "select @VehicleMakeId, @VehicleModelId, @VehicleYear, getdate(), @UpdatedBy, @Picture1, 0";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;

                    cmd.ExecuteNonQuery();
                    ShowInfo("Added vehicle successfully");
                    LoadVehicleMake();
                    VehicleModelList.SelectedIndex = -1;
                    VehicleYearList.SelectedIndex = -1;
                }
                else if (AddVehicleTitle.Text == "Edit Vehicle")
                {
                    string sql = "update dbo.VehicleMakeModelYear set VehicleMakeId = @VehicleMakeId, VehicleModelId=@VehicleModelId, ";
                    sql += "VehicleYear=@VehicleYear, UpdatedDt=getdate(), UpdatedBy= @UpdatedBy, Picture1=@Picture1, SearchCount=@SearchCount ";
                    sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    int SearchCount = 0;
                    if (AdjustSearchCountTxt.Text != "" && int.TryParse(AdjustSearchCountTxt.Text, out SearchCount))
                    {
                        SearchCount = int.Parse(AdjustSearchCountTxt.Text);
                    }

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Year.Text);
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@Picture1", SqlDbType.NVarChar, 2000).Value = PictureFileName1;
                    cmd.Parameters.Add("@SearchCount", SqlDbType.Int).Value = SearchCount;
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                    cmd.ExecuteNonQuery();
                    ShowInfo("Updated vehicle successfully");

                    VehicleMakeList.SelectedValue = MakeList.SelectedValue;
                    ChangeVehicleMakeGetYear(false, int.Parse(Year.Text));
                    //VehicleYearList.SelectedValue = Year.Text;
                    ChangeVehicleYearGetModel(false, int.Parse(ModelList.SelectedValue));
                    //VehicleModelList.SelectedValue = ModelList.SelectedValue;
                    SearchInfo(0);
                }
                else if (AddVehicleTitle.Text == "Link Vehicle")
                {
                    string sql = "select VehicleMakeModelYearId from dbo.VehicleMakeModelYear WITH (NOLOCK) ";
                    sql += "where VehicleMakeId=@VehicleMakeId and VehicleModelId=@VehicleModelId and VehicleYear=@VehicleYear ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(MakeList.SelectedValue);
                    cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(ModelList.SelectedValue);
                    cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(YearList.SelectedValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        ShowError("Failed to find Vehicle information");
                        return;
                    }
                    reader.Close();

                    if (VehicleMakeModelYearIdHidden.Value == VehicleMakeModelYearId)
                    {
                        ShowError("You cannot link this Vehicle.");
                        return;
                    }
                    LinkVehicleMakeModelYearIdHidden.Value = VehicleMakeModelYearId;

                    LinkVehicleDatabase(VehicleMakeModelYearIdHidden.Value, VehicleMakeModelYearId);
                    LinkVehicle.Text = MakeList.SelectedItem.Text + " " + ModelList.SelectedItem.Text + " " + YearList.SelectedValue;

                    ShowInfo("Linked vehicle successfully");
                }

                Year.Text = "";
                NewVehiclePanel.Visible = false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("Cannot insert duplicate key row") > 0)
                {
                    ShowError("Vehicle already exists");

                    if (PictureFileName1 != "" && File.Exists(FullPath + PictureFileName1))
                    {
                        File.Delete(FullPath + PictureFileName1);
                    }
                }
                else
                {
                    ShowError(ex.ToString());
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void LinkVehicleDatabase(string VehicleMakeModelYearId, string LinkVehicleMakeModelYearId)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "if not exists(select * from VehicleLink where VehicleMakeModelYearId=@VehicleMakeModelYearId) ";
                sql += "insert into dbo.VehicleLink (VehicleMakeModelYearId, LinkVehicleMakeModelYearId, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @LinkVehicleMakeModelYearId, getdate(), @UpdatedBy ";
                sql += "else ";
                sql += "update dbo.VehicleLink set LinkVehicleMakeModelYearId = @LinkVehicleMakeModelYearId, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                cmd.Parameters.Add("@LinkVehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(LinkVehicleMakeModelYearId);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        protected void EditVehicleButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Edit Vehicle";
            AddVehicleButton.Text = "Save";

            UnlinkVehicleButton.Visible = false;

            MakeList.SelectedValue = VehicleMakeList.SelectedValue;
            ShowModelList(true);
            ModelList.SelectedValue = VehicleModelList.SelectedValue;

            YearList.Visible = false;
            Year.Visible = true;
            Year.Text = VehicleYearList.SelectedValue;

            NewVehiclePanel.Visible = true;
            VehiclePicturePanel.Visible = true;
        }

        protected void DeleteVehicleButton_Click(object sender, EventArgs e)
        {
            ClearError();

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                // Wiring
                string sql = "delete from dbo.VehicleWireFunction where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Note
                sql = "delete from dbo.VehicleWireNote where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Disassembly
                sql = "delete from dbo.VehicleWireDisassembly where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Prep
                sql = "delete from dbo.VehicleWirePrep where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Routing
                sql = "delete from dbo.VehicleWirePlacementRouting where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Programming
                sql = "delete from dbo.VehicleWireProgramming where VehicleMakeModelYearId = @VehicleMakeModelYearId ";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                // Vehicle
                sql = "delete from dbo.VehicleMakeModelYear where VehicleMakeModelYearId = @VehicleMakeModelYearId";
                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                tran.Commit();

                VehicleMakeList.SelectedIndex = 0;
                VehicleModelList.Items.Clear();
                VehicleYearList.Items.Clear();
                ShowHideResult(false);

                ShowInfo("Deleted vehicle successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void AddWirePanelButton_Click(object sender, EventArgs e)
        {
            LoadWireInstall();

            VehicleColor.Text = "";
            Color.Text = "";
            PinOut.Text = "";
            Location.Text = "";
            Polarity.Text = "";

            AttachWiringImageDelete1.Checked = false;
            AttachWiringImageDelete2.Checked = false;
            AttachWiringImageDelete3.Checked = false;
            AttachWiringImageDelete4.Checked = false;
            AttachWiringImageDelete5.Checked = false;

            ExistingWiringFilename1.Value = "";
            AttachWiringImage1.Visible = false;
            AttachWiringImageDelete1.Visible = false;
            ReplaceWiringImageLabel1.Visible = false;
            AttachWiringFileUrl1.Text = "";

            ExistingWiringFilename2.Value = "";
            AttachWiringImage2.Visible = false;
            AttachWiringImageDelete2.Visible = false;
            ReplaceWiringImageLabel2.Visible = false;
            AttachWiringFileUrl2.Text = "";

            ExistingWiringFilename3.Value = "";
            AttachWiringImage3.Visible = false;
            AttachWiringImageDelete3.Visible = false;
            ReplaceWiringImageLabel3.Visible = false;
            AttachWiringFileUrl3.Text = "";

            ExistingWiringFilename4.Value = "";
            AttachWiringImage4.Visible = false;
            AttachWiringImageDelete4.Visible = false;
            ReplaceWiringImageLabel4.Visible = false;
            AttachWiringFileUrl4.Text = "";

            ExistingWiringFilename5.Value = "";
            AttachWiringImage5.Visible = false;
            AttachWiringImageDelete5.Visible = false;
            ReplaceWiringImageLabel5.Visible = false;
            AttachWiringFileUrl5.Text = "";

            AddWiringTitle.Text = "Add Wiring";
            AddWireButton.Text = "Add";
            AddWirePanel.Visible = true;
        }

        private void LoadWireInstall()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId, WireFunctionName from dbo.WireFunction with (nolock) where Inactive=0 order by WireFunctionName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp2 = new SqlDataAdapter(Cmd);
                DataSet ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                WireFunctionList.DataTextField = "WireFunctionName";
                WireFunctionList.DataValueField = "WireFunctionId";
                WireFunctionList.DataSource = ds2;
                WireFunctionList.DataBind();

                sql = "select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0 order by InstallationTypeName ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                adp2 = new SqlDataAdapter(Cmd);
                ds2 = new DataSet();

                adp2.Fill(ds2, "List");

                InstalltionTypeList.DataTextField = "InstallationTypeName";
                InstalltionTypeList.DataValueField = "InstallationTypeId";
                InstalltionTypeList.DataSource = ds2;
                InstalltionTypeList.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            if (VehicleModelList.Items.Count <= 0 || VehicleModelList.SelectedValue == "-1")
            {
                ShowWireError("Please select Vehicle");
                return;
            }
            if (VehicleYearList.Items.Count <= 0 || VehicleYearList.SelectedValue == "-1")
            {
                ShowWireError("Please select Vehicle");
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachWiringFile1.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachWiringFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachWiringFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachWiringFile2.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachWiringFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachWiringFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachWiringFile3.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachWiringFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachWiringFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachWiringFile4.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachWiringFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachWiringFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachWiringFile5.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachWiringFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachWiringFileUrl5.Text.Trim();
            }

            if (AddWireButton.Text == "Add" || VehicleWireEditorWorkActionHidden.Value == "0")
            {
                if (VehicleWireEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    if (AttachWiringImageDelete1.Visible && AttachWiringImageDelete1.Checked)
                    {
                        FileName1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingWiringFilename1.Value;
                        }
                    }
                    if (AttachWiringImageDelete2.Visible && AttachWiringImageDelete2.Checked)
                    {
                        FileName2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingWiringFilename2.Value;
                        }
                    }
                    if (AttachWiringImageDelete3.Visible && AttachWiringImageDelete3.Checked)
                    {
                        FileName3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingWiringFilename3.Value;
                        }
                    }
                    if (AttachWiringImageDelete4.Visible && AttachWiringImageDelete4.Checked)
                    {
                        FileName4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingWiringFilename4.Value;
                        }
                    }
                    if (AttachWiringImageDelete5.Visible && AttachWiringImageDelete5.Checked)
                    {
                        FileName5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingWiringFilename5.Value;
                        }
                    }

                    AddVehicleWire(VehicleWireEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                        PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else if (AddWireButton.Text == "Add")
                {
                    AddVehicleWire("", VehicleMakeModelYearIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                        PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }
            else
            {
                if ((AttachWiringImageDelete1.Checked && ExistingWiringFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename1.Value);
                    }
                }
                if ((AttachWiringImageDelete2.Checked && ExistingWiringFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename2.Value);
                    }
                }
                if ((AttachWiringImageDelete3.Checked && ExistingWiringFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename3.Value);
                    }
                }
                if ((AttachWiringImageDelete4.Checked && ExistingWiringFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename4.Value);
                    }
                }
                if ((AttachWiringImageDelete5.Checked && ExistingWiringFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingWiringFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingWiringFilename5.Value);
                    }
                }

                string Attach1 = "";
                if (AttachWiringImageDelete1.Visible && AttachWiringImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingWiringFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachWiringImageDelete2.Visible && AttachWiringImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingWiringFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachWiringImageDelete3.Visible && AttachWiringImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingWiringFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachWiringImageDelete4.Visible && AttachWiringImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingWiringFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachWiringImageDelete5.Visible && AttachWiringImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingWiringFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                if (VehicleWireEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdateVehicleWire(VehicleWireEditorWorkIdHidden.Value, VehicleWireFunctionIdHidden.Value, VehicleMakeModelYearIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                        PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), Attach1, Attach2, Attach3, Attach4, Attach5);
                }
                else
                {
                    UpdateVehicleWire("", VehicleWireFunctionIdHidden.Value, VehicleMakeModelYearIdHidden.Value, WireFunctionList.SelectedValue, InstalltionTypeList.SelectedValue, VehicleColor.Text.Trim(), Color.Text.Trim(),
                        PinOut.Text.Trim(), Location.Text.Trim(), Polarity.Text.Trim(), Attach1, Attach2, Attach3, Attach4, Attach5);
                }
            }

            VehicleColor.Text = "";
            Color.Text = "";
            PinOut.Text = "";
            Location.Text = "";
            Polarity.Text = "";

            SearchInfo(1);
            AddWirePanel.Visible = false;
            EditNotePanel.Visible = false;
        }

        private void AddVehicleWire(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string WireFunctionId, string InstallationTypeId, string VehicleColor, string Colour, string PinOut,
            string Location, string Polarity, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                { 
                    sql = @"insert into dbo.VehicleWireFunction (VehicleMakeModelYearId, 
                        WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, Inactive, EditorWorkId) 
                        select @VehicleMakeModelYearId, 
                        @WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, 0, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();


                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFunctionEditor (
                        EditorWorkId, VehicleMakeModelYearId, 
                        WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, 
                        @WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateVehicleWire(string ApprovalEditorWorkId, string VehicleWireFunctionId, string VehicleMakeModelYearId, string WireFunctionId, string InstallationTypeId, string VehicleColor, string Colour, string PinOut,
            string Location, string Polarity, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWireFunctionId
                            from dbo.VehicleWireFunctionEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWireFunctionId = reader["VehicleWireFunctionId"].ToString();
                        }
                        reader.Close();

                        sql = "update dbo.VehicleWireFunction ";
                        sql += "set WireFunctionId = @WireFunctionId, InstallationTypeId=@InstallationTypeId, VehicleColor=@VehicleColor, ";
                        sql += "Colour=@Colour, PinOut=@PinOut, Location=@Location, Polarity=@Polarity, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                        sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId ";
                        sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                        cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                        cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                        cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                        cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                        cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                        cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionId);

                        cmd.ExecuteNonQuery();

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = "update dbo.VehicleWireFunction ";
                        sql += "set WireFunctionId = @WireFunctionId, InstallationTypeId=@InstallationTypeId, VehicleColor=@VehicleColor, ";
                        sql += "Colour=@Colour, PinOut=@PinOut, Location=@Location, Polarity=@Polarity, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, ";
                        sql += "UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId ";
                        sql += "where VehicleWireFunctionId = @VehicleWireFunctionId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                        cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                        cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                        cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                        cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                        cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                        cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFunctionEditor (
                        EditorWorkId, VehicleWireFunctionId, VehicleMakeModelYearId,
                        WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWireFunctionId, @VehicleMakeModelYearId,
                        @WireFunctionId, @InstallationTypeId, @VehicleColor, @Colour, @PinOut, @Location, @Polarity, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@WireFunctionId", SqlDbType.Int).Value = int.Parse(WireFunctionId);
                    cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar, 100).Value = VehicleColor;
                    cmd.Parameters.Add("@Colour", SqlDbType.NVarChar, 100).Value = Colour;
                    cmd.Parameters.Add("@PinOut", SqlDbType.NVarChar, 100).Value = PinOut;
                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar, 100).Value = Location;
                    cmd.Parameters.Add("@Polarity", SqlDbType.NVarChar, 100).Value = Polarity;
                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelWireButton_Click(object sender, EventArgs e)
        {
            ClearError();
            AddWirePanel.Visible = false;
        }

        protected void WireList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Wiring?');";
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void WireList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWireInstall();
            VehicleWireFunctionIdHidden.Value = ((HiddenField)(WireList.SelectedRow.FindControl("VehicleWireFunctionId"))).Value;
            VehicleWireEditorWorkIdHidden.Value = "";
            VehicleWireEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "select WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                sql += "from dbo.VehicleWireFunction WITH (NOLOCK) ";
                sql += "where VehicleWireFunctionId = @VehicleWireFunctionId ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionList.SelectedValue = reader["WireFunctionId"].ToString();
                    InstalltionTypeList.SelectedValue = reader["InstallationTypeId"].ToString();
                    VehicleColor.Text = reader["VehicleColor"].ToString();
                    Color.Text = reader["Colour"].ToString();
                    PinOut.Text = reader["PinOut"].ToString();
                    Location.Text = reader["Location"].ToString();
                    Polarity.Text = reader["Polarity"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage1.ImageUrl = attach;
                            AttachWiringFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage1.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl1.Text = "";
                        }
                        ExistingWiringFilename1.Value = attach;
                        AttachWiringImage1.Visible = true;

                        AttachWiringImageDelete1.Visible = true;
                        ReplaceWiringImageLabel1.Visible = true;

                        AttachWiringImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename1.Value = "";
                        AttachWiringFileUrl1.Text = "";
                        AttachWiringImage1.Visible = false;
                        AttachWiringImageDelete1.Visible = false;
                        ReplaceWiringImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage2.ImageUrl = attach;
                            AttachWiringFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage2.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl2.Text = "";
                        }
                        ExistingWiringFilename2.Value = attach;
                        AttachWiringImage2.Visible = true;

                        AttachWiringImageDelete2.Visible = true;
                        ReplaceWiringImageLabel2.Visible = true;

                        AttachWiringImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename2.Value = "";
                        AttachWiringFileUrl2.Text = "";
                        AttachWiringImage2.Visible = false;
                        AttachWiringImageDelete2.Visible = false;
                        ReplaceWiringImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage3.ImageUrl = attach;
                            AttachWiringFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage3.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl3.Text = "";
                        }
                        ExistingWiringFilename3.Value = attach;
                        AttachWiringImage3.Visible = true;

                        AttachWiringImageDelete3.Visible = true;
                        ReplaceWiringImageLabel3.Visible = true;

                        AttachWiringImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename3.Value = "";
                        AttachWiringFileUrl3.Text = "";
                        AttachWiringImage3.Visible = false;
                        AttachWiringImageDelete3.Visible = false;
                        ReplaceWiringImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage4.ImageUrl = attach;
                            AttachWiringFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage4.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl4.Text = "";
                        }
                        ExistingWiringFilename4.Value = attach;
                        AttachWiringImage4.Visible = true;

                        AttachWiringImageDelete4.Visible = true;
                        ReplaceWiringImageLabel4.Visible = true;

                        AttachWiringImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename4.Value = "";
                        AttachWiringFileUrl4.Text = "";
                        AttachWiringImage4.Visible = false;
                        AttachWiringImageDelete4.Visible = false;
                        ReplaceWiringImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage5.ImageUrl = attach;
                            AttachWiringFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage5.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl5.Text = "";
                        }
                        ExistingWiringFilename5.Value = attach;
                        AttachWiringImage5.Visible = true;

                        AttachWiringImageDelete5.Visible = true;
                        ReplaceWiringImageLabel5.Visible = true;

                        AttachWiringImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename5.Value = "";
                        AttachWiringFileUrl5.Text = "";
                        AttachWiringImage5.Visible = false;
                        AttachWiringImageDelete5.Visible = false;
                        ReplaceWiringImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddWiringTitle.Text = "Edit Wiring";
                AddWireButton.Text = "Save";
                AddWirePanel.Visible = true;
                EditNotePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void WireList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFunctionIdValue = ((HiddenField)(WireList.Rows[e.RowIndex].FindControl("VehicleWireFunctionId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = "select a.VehicleWireFunctionId, Attach1, Attach2, Attach3, Attach4, Attach5 ";
                    sql += "from dbo.VehicleWireFunction a ";
                    sql += "where VehicleWireFunctionId=@VehicleWireFunctionId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWireFunction where VehicleWireFunctionId=@VehicleWireFunctionId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = "select VehicleWireFunctionId, VehicleMakeModelYearId ";
                    sql += "from dbo.VehicleWireFunction with (NOLOCK) ";
                    sql += "where VehicleWireFunctionId=@VehicleWireFunctionId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFunctionEditor(
                            EditorWorkId, VehicleWireFunctionId, VehicleMakeModelYearId,
                            WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWireFunctionId, VehicleMakeModelYearId,
                            WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWireFunction where VehicleWireFunctionId = @VehicleWireFunctionId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireFunctionId", SqlDbType.Int).Value = int.Parse(VehicleWireFunctionIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                SearchInfo(1);
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddDisassemblyPanelButton_Click(object sender, EventArgs e)
        {
            AddDisassemblyTitle.Text = "Add Disassembly";
            AddDisassemblyButton.Text = "Add";
            VehicleWireDisassemblyIdHidden.Value = "";
            AddDisassemblyPanel.Visible = true;

            DisassemblyStep.Text = "";
            Disassemblyeditor.Text = "";

            AttachDisassemblyImageDelete1.Checked = false;
            AttachDisassemblyImageDelete2.Checked = false;
            AttachDisassemblyImageDelete3.Checked = false;
            AttachDisassemblyImageDelete4.Checked = false;
            AttachDisassemblyImageDelete5.Checked = false;

            ExistingDisassemblyFilename1.Value = "";
            AttachDisassemblyImage1.Visible = false;
            AttachDisassemblyImageDelete1.Visible = false;
            ReplaceDisassemblyImageLabel1.Visible = false;
            AttachDisassemblyFileUrl1.Text = "";

            ExistingDisassemblyFilename2.Value = "";
            AttachDisassemblyImage2.Visible = false;
            AttachDisassemblyImageDelete2.Visible = false;
            ReplaceDisassemblyImageLabel2.Visible = false;
            AttachDisassemblyFileUrl2.Text = "";

            ExistingDisassemblyFilename3.Value = "";
            AttachDisassemblyImage3.Visible = false;
            AttachDisassemblyImageDelete3.Visible = false;
            ReplaceDisassemblyImageLabel3.Visible = false;
            AttachDisassemblyFileUrl3.Text = "";

            ExistingDisassemblyFilename4.Value = "";
            AttachDisassemblyImage4.Visible = false;
            AttachDisassemblyImageDelete4.Visible = false;
            ReplaceDisassemblyImageLabel4.Visible = false;
            AttachDisassemblyFileUrl4.Text = "";

            ExistingDisassemblyFilename5.Value = "";
            AttachDisassemblyImage5.Visible = false;
            AttachDisassemblyImageDelete5.Visible = false;
            ReplaceDisassemblyImageLabel5.Visible = false;
            AttachDisassemblyFileUrl5.Text = "";

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void ResetTab()
        {
            WiringPaneltab.Attributes.Add("Class", "tab-title");
            FBPaneltab.Attributes.Add("Class", "tab-title");
            DocumentPaneltab.Attributes.Add("Class", "tab-title");
            linksdisassemblytab.Attributes.Add("Class", "tab-title");
            linkspreptab.Attributes.Add("Class", "tab-title");
            linkroutingtab.Attributes.Add("Class", "tab-title");
            linkprogrammingtab.Attributes.Add("Class", "tab-title");
            TSBPaneltab.Attributes.Add("Class", "tab-title");

            WiringPanel.Attributes.Add("Class", "content");
            FBPanel.Attributes.Add("Class", "content");
            DocumentPanel.Attributes.Add("Class", "content");
            linksdisassembly.Attributes.Add("Class", "content");
            linksprep.Attributes.Add("Class", "content");
            linkrouting.Attributes.Add("Class", "content");
            linkprogramming.Attributes.Add("Class", "content");
            TSBPanel.Attributes.Add("Class", "content");
        }

        protected void CancelDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDisassemblyPanel.Visible = false;

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName)
        {
            string DocumentName;
            SaveFile(PFile, FullPath, Prefix, Make, Model, Year, out FileName, out DocumentName);
        }

        private void SaveFile(HttpPostedFile PFile, string FullPath, string Prefix, string Make, string Model, string Year, out string FileName, out string DocumentName)
        {
            FileName = "";
            DocumentName = "";
            if (PFile != null && PFile.FileName != "")
            {
                string filepath = PFile.FileName;
                string filename = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                DocumentName = filename;
                filename = Prefix + "_" + Make + "_" + Model + "_" + Year + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + filename;
                filename = filename.Replace(" ", "").Replace(@"\", "_");

                int IntFileSize = PFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                PFile.InputStream.Read(myData, 0, IntFileSize);

                FileName = filename;
                FileStream newFile = new FileStream(FullPath + filename, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }
        }

        protected void AddDisassemblyButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (DisassemblyStep.Text == "")
            {
                ShowError("Please enter Step");
                DisassemblyStep.Focus();
                return;
            }
            if (Disassemblyeditor.Text == "")
            {
                ShowError("Please enter Note");
                Disassemblyeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachDisassemblyFile1.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachDisassemblyFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachDisassemblyFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachDisassemblyFile2.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachDisassemblyFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachDisassemblyFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachDisassemblyFile3.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachDisassemblyFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachDisassemblyFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachDisassemblyFile4.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachDisassemblyFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachDisassemblyFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachDisassemblyFile5.PostedFile, FullPath, "Disassembly", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachDisassemblyFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachDisassemblyFileUrl5.Text.Trim();
            }

            if (AddDisassemblyButton.Text == "Add" || VehicleWireDisassemblyEditorWorkActionHidden.Value == "0")
            {
                if (VehicleWireDisassemblyEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    AddDisassembly(VehicleWireDisassemblyEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else if (AddDisassemblyButton.Text == "Add")
                {
                    AddDisassembly("", VehicleMakeModelYearIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }
            else
            {
                if ((AttachDisassemblyImageDelete1.Checked && ExistingDisassemblyFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingDisassemblyFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingDisassemblyFilename1.Value);
                    }
                }
                if ((AttachDisassemblyImageDelete2.Checked && ExistingDisassemblyFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingDisassemblyFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingDisassemblyFilename2.Value);
                    }
                }
                if ((AttachDisassemblyImageDelete3.Checked && ExistingDisassemblyFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingDisassemblyFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingDisassemblyFilename3.Value);
                    }
                }
                if ((AttachDisassemblyImageDelete4.Checked && ExistingDisassemblyFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingDisassemblyFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingDisassemblyFilename4.Value);
                    }
                }
                if ((AttachDisassemblyImageDelete5.Checked && ExistingDisassemblyFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingDisassemblyFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingDisassemblyFilename5.Value);
                    }
                }

                string Attach1 = "";
                if (AttachDisassemblyImageDelete1.Visible && AttachDisassemblyImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingDisassemblyFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachDisassemblyImageDelete2.Visible && AttachDisassemblyImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingDisassemblyFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachDisassemblyImageDelete3.Visible && AttachDisassemblyImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingDisassemblyFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachDisassemblyImageDelete4.Visible && AttachDisassemblyImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingDisassemblyFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachDisassemblyImageDelete5.Visible && AttachDisassemblyImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingDisassemblyFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                if (VehicleWireDisassemblyEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdateDisassembly(VehicleWireDisassemblyEditorWorkIdHidden.Value, VehicleWireDisassemblyIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                    Attach1, Attach2, Attach3, Attach4, Attach5);
                }
                else
                {
                    UpdateDisassembly("", VehicleWireDisassemblyIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DisassemblyStep.Text, Disassemblyeditor.Text,
                    Attach1, Attach2, Attach3, Attach4, Attach5);
                }
            }

            SearchInfo(4);
            AddDisassemblyPanel.Visible = false;

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");
        }

        private void AddDisassembly(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleWireDisassembly (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();

                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireDisassemblyEditor (
                        EditorWorkId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateDisassembly(string ApprovalEditorWorkId, string VehicleWireDisassemblyId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWireDisassemblyId
                            from dbo.VehicleWireDisassemblyEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWireDisassemblyId = reader["VehicleWireDisassemblyId"].ToString();
                        }
                        reader.Close();


                        sql = @"update dbo.VehicleWireDisassembly 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyId);

                        cmd.ExecuteNonQuery();

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleWireDisassembly 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireDisassemblyEditor (
                        EditorWorkId, VehicleWireDisassemblyId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWireDisassemblyId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowDisassemblyList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetDisassemblyQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            DisassemblyList.DataSource = ds;
            DisassemblyList.DataBind();

            if (DisassemblyList != null && DisassemblyList.HeaderRow != null && DisassemblyList.HeaderRow.Cells.Count > 0)
            {
                DisassemblyList.Attributes["data-page"] = "false";

                /*
                DisassemblyList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                DisassemblyList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                DisassemblyList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                DisassemblyList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                DisassemblyList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";

                DisassemblyList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                DisassemblyList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                DisassemblyList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                DisassemblyList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                DisassemblyList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                */

                DisassemblyList.HeaderRow.TableSection = TableRowSection.TableHeader;
                //DisassemblyTitle.Visible = true;

                DisassemblyHeader.Text = "Disassembly (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                DisassemblyHeader.Text = "Disassembly (0)";
                //DisassemblyTitle.Visible = false;
            }
        }


        private void ShowDisassemblyListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetDisassemblyQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            DisassemblyListEditor.DataSource = ds;
            DisassemblyListEditor.DataBind();

            if (DisassemblyListEditor != null && DisassemblyListEditor.HeaderRow != null && DisassemblyListEditor.HeaderRow.Cells.Count > 0)
            {
                DisassemblyListEditor.Attributes["data-page"] = "false";

                DisassemblyListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                DisassemblyListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                DisassemblyListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                DisassemblyListEditor.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                DisassemblyListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                DisassemblyListEditor.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                DisassemblyListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    DisassemblyListEditor.HeaderRow.Cells[9].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    DisassemblyListEditor.Columns[7].Visible = false;
                    DisassemblyListEditor.Columns[8].Visible = false;
                    DisassemblyListEditor.HeaderRow.Cells[9].Text = "Delete";
                }
            }
        }

        protected void DisassemblyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireDisassemblyIdHidden.Value = ((HiddenField)(DisassemblyList.SelectedRow.FindControl("VehicleWireDisassemblyId"))).Value;
            VehicleWireDisassemblyEditorWorkIdHidden.Value = "";
            VehicleWireDisassemblyEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireDisassemblyId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireDisassembly  ";
                sql += "where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    DisassemblyStep.Text = reader["Step"].ToString();
                    Disassemblyeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage1.ImageUrl = attach;
                            AttachDisassemblyFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage1.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl1.Text = "";
                        }

                        ExistingDisassemblyFilename1.Value = attach;
                        AttachDisassemblyImage1.Visible = true;

                        AttachDisassemblyImageDelete1.Visible = true;
                        ReplaceDisassemblyImageLabel1.Visible = true;

                        AttachDisassemblyImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename1.Value = "";
                        AttachDisassemblyFileUrl1.Text = "";
                        AttachDisassemblyImage1.Visible = false;
                        AttachDisassemblyImageDelete1.Visible = false;
                        ReplaceDisassemblyImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage2.ImageUrl = attach;
                            AttachDisassemblyFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage2.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl2.Text = "";
                        }

                        ExistingDisassemblyFilename2.Value = attach;
                        AttachDisassemblyImage2.Visible = true;

                        AttachDisassemblyImageDelete2.Visible = true;
                        ReplaceDisassemblyImageLabel2.Visible = true;

                        AttachDisassemblyImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename2.Value = "";
                        AttachDisassemblyFileUrl2.Text = "";
                        AttachDisassemblyImage2.Visible = false;
                        AttachDisassemblyImageDelete2.Visible = false;
                        ReplaceDisassemblyImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage3.ImageUrl = attach;
                            AttachDisassemblyFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage3.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl3.Text = "";
                        }

                        ExistingDisassemblyFilename3.Value = attach;
                        AttachDisassemblyImage3.Visible = true;

                        AttachDisassemblyImageDelete3.Visible = true;
                        ReplaceDisassemblyImageLabel3.Visible = true;

                        AttachDisassemblyImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename3.Value = "";
                        AttachDisassemblyFileUrl3.Text = "";
                        AttachDisassemblyImage3.Visible = false;
                        AttachDisassemblyImageDelete3.Visible = false;
                        ReplaceDisassemblyImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage4.ImageUrl = attach;
                            AttachDisassemblyFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage4.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl4.Text = "";
                        }

                        ExistingDisassemblyFilename4.Value = attach;
                        AttachDisassemblyImage4.Visible = true;

                        AttachDisassemblyImageDelete4.Visible = true;
                        ReplaceDisassemblyImageLabel4.Visible = true;

                        AttachDisassemblyImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename4.Value = "";
                        AttachDisassemblyFileUrl4.Text = "";
                        AttachDisassemblyImage4.Visible = false;
                        AttachDisassemblyImageDelete4.Visible = false;
                        ReplaceDisassemblyImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage5.ImageUrl = attach;
                            AttachDisassemblyFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage5.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl5.Text = "";
                        }

                        ExistingDisassemblyFilename5.Value = attach;
                        AttachDisassemblyImage5.Visible = true;

                        AttachDisassemblyImageDelete5.Visible = true;
                        ReplaceDisassemblyImageLabel5.Visible = true;

                        AttachDisassemblyImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename5.Value = "";
                        AttachDisassemblyFileUrl5.Text = "";
                        AttachDisassemblyImage5.Visible = false;
                        AttachDisassemblyImageDelete5.Visible = false;
                        ReplaceDisassemblyImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddDisassemblyTitle.Text = "Edit Disassembly";
                AddDisassemblyButton.Text = "Save";
                AddDisassemblyPanel.Visible = true;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void DisassemblyList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireDisassemblyIdValue = ((HiddenField)(DisassemblyList.Rows[e.RowIndex].FindControl("VehicleWireDisassemblyId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select a.VehicleWireDisassemblyId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                        from dbo.VehicleWireDisassembly a 
                        where VehicleWireDisassemblyId=@VehicleWireDisassemblyId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWireDisassembly where VehicleWireDisassemblyId=@VehicleWireDisassemblyId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleWireDisassemblyId, VehicleMakeModelYearId 
                        from dbo.VehicleWireDisassembly with (NOLOCK) 
                        where VehicleWireDisassemblyId=@VehicleWireDisassemblyId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireDisassemblyEditor(
                            EditorWorkId, VehicleWireDisassemblyId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWireDisassemblyId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWireDisassembly where VehicleWireDisassemblyId = @VehicleWireDisassemblyId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireDisassemblyId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddDisassemblyPanel.Visible = false;
                SearchInfo(4);

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddPrepPanelButton_Click(object sender, EventArgs e)
        {
            AddPrepTitle.Text = "Add Prep";
            AddPrepButton.Text = "Add";
            VehicleWirePrepIdHidden.Value = "";
            AddPrepPanel.Visible = true;

            PrepStep.Text = "";
            Prepeditor.Text = "";

            AttachPrepImageDelete1.Checked = false;
            AttachPrepImageDelete2.Checked = false;
            AttachPrepImageDelete3.Checked = false;
            AttachPrepImageDelete4.Checked = false;
            AttachPrepImageDelete5.Checked = false;

            ExistingPrepFilename1.Value = "";
            AttachPrepImage1.Visible = false;
            AttachPrepImageDelete1.Visible = false;
            ReplacePrepImageLabel1.Visible = false;
            AttachPrepFileUrl1.Text = "";

            ExistingPrepFilename2.Value = "";
            AttachPrepImage2.Visible = false;
            AttachPrepImageDelete2.Visible = false;
            ReplacePrepImageLabel2.Visible = false;
            AttachPrepFileUrl2.Text = "";

            ExistingPrepFilename3.Value = "";
            AttachPrepImage3.Visible = false;
            AttachPrepImageDelete3.Visible = false;
            ReplacePrepImageLabel3.Visible = false;
            AttachPrepFileUrl3.Text = "";

            ExistingPrepFilename4.Value = "";
            AttachPrepImage4.Visible = false;
            AttachPrepImageDelete4.Visible = false;
            ReplacePrepImageLabel4.Visible = false;
            AttachPrepFileUrl4.Text = "";

            ExistingPrepFilename5.Value = "";
            AttachPrepImage5.Visible = false;
            AttachPrepImageDelete5.Visible = false;
            ReplacePrepImageLabel5.Visible = false;
            AttachPrepFileUrl5.Text = "";

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");

        }

        protected void CancelPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddPrepPanel.Visible = false;

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");
        }

        protected void AddPrepButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (PrepStep.Text == "")
            {
                ShowError("Please enter Step");
                PrepStep.Focus();
                return;
            }
            if (Prepeditor.Text == "")
            {
                ShowError("Please enter Note");
                Prepeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachPrepFile1.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachPrepFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachPrepFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachPrepFile2.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachPrepFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachPrepFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachPrepFile3.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachPrepFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachPrepFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachPrepFile4.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachPrepFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachPrepFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachPrepFile5.PostedFile, FullPath, "Prep", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachPrepFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachPrepFileUrl5.Text.Trim();
            }


            if (AddPrepButton.Text == "Add" || VehicleWirePrepEditorWorkActionHidden.Value == "0")
            {
                if (VehicleWirePrepEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    AddPrep(VehicleWirePrepEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);

                }
                else if (AddPrepButton.Text == "Add")
                {
                    AddPrep("", VehicleMakeModelYearIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }
            else
            {
                if ((AttachPrepImageDelete1.Checked && ExistingPrepFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingPrepFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingPrepFilename1.Value);
                    }
                }
                if ((AttachPrepImageDelete2.Checked && ExistingPrepFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingPrepFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingPrepFilename2.Value);
                    }
                }
                if ((AttachPrepImageDelete3.Checked && ExistingPrepFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingPrepFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingPrepFilename3.Value);
                    }
                }
                if ((AttachPrepImageDelete4.Checked && ExistingPrepFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingPrepFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingPrepFilename4.Value);
                    }
                }
                if ((AttachPrepImageDelete5.Checked && ExistingPrepFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingPrepFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingPrepFilename5.Value);
                    }
                }


                string Attach1 = "";
                if (AttachPrepImageDelete1.Visible && AttachPrepImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingPrepFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachPrepImageDelete2.Visible && AttachPrepImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingPrepFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachPrepImageDelete3.Visible && AttachPrepImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingPrepFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachPrepImageDelete4.Visible && AttachPrepImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingPrepFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachPrepImageDelete5.Visible && AttachPrepImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingPrepFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                if (VehicleWirePrepEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdatePrep(VehicleWirePrepEditorWorkIdHidden.Value, VehicleWirePrepIdHidden.Value, VehicleMakeModelYearIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }
                else
                {
                    UpdatePrep("", VehicleWirePrepIdHidden.Value, VehicleMakeModelYearIdHidden.Value, PrepStep.Text, Prepeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }
            }

            SearchInfo(5);
            AddPrepPanel.Visible = false;

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");
        }

        private void AddPrep(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleWirePrep (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePrepEditor (
                        EditorWorkId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdatePrep(string ApprovalEditorWorkId, string VehicleWirePrepId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWirePrepId
                            from dbo.VehicleWirePrepEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWirePrepId = reader["VehicleWirePrepId"].ToString();
                        }
                        reader.Close();

                        sql = @"update dbo.VehicleWirePrep 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5,
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWirePrepId = @VehicleWirePrepId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepId);

                        cmd.ExecuteNonQuery();

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleWirePrep 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWirePrepId = @VehicleWirePrepId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePrepEditor (
                        EditorWorkId, VehicleWirePrepId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWirePrepId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowPrepList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetPrepQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            PrepList.DataSource = ds;
            PrepList.DataBind();

            if (PrepList != null && PrepList.HeaderRow != null && PrepList.HeaderRow.Cells.Count > 0)
            {
                PrepList.Attributes["data-page"] = "false";

                PrepList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                PrepList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                PrepList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                PrepList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                PrepList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                PrepList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                PrepList.HeaderRow.TableSection = TableRowSection.TableHeader;
                //PrepTitle.Visible = true;

                PrepHeader.Text = "Prep (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                PrepHeader.Text = "Prep (0)";
                //PrepTitle.Visible = false;
            }
        }


        private void ShowPrepListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetPrepQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            PrepListEditor.DataSource = ds;
            PrepListEditor.DataBind();

            if (PrepListEditor != null && PrepListEditor.HeaderRow != null && PrepListEditor.HeaderRow.Cells.Count > 0)
            {
                PrepListEditor.Attributes["data-page"] = "false";

                PrepListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                PrepListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                PrepListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                PrepListEditor.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                PrepListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                PrepListEditor.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                PrepListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    PrepListEditor.HeaderRow.Cells[9].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    PrepListEditor.Columns[7].Visible = false;
                    PrepListEditor.Columns[8].Visible = false;
                    PrepListEditor.HeaderRow.Cells[9].Text = "Delete";
                }
            }
        }


        protected void PrepList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePrepIdHidden.Value = ((HiddenField)(PrepList.SelectedRow.FindControl("VehicleWirePrepId"))).Value;
            VehicleWirePrepEditorWorkIdHidden.Value = "";
            VehicleWirePrepEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePrepId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePrep  ";
                sql += "where VehicleWirePrepId = @VehicleWirePrepId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    PrepStep.Text = reader["Step"].ToString();
                    Prepeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage1.ImageUrl = attach;
                            AttachPrepFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage1.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl1.Text = "";
                        }
                        ExistingPrepFilename1.Value = attach;
                        AttachPrepImage1.Visible = true;

                        AttachPrepImageDelete1.Visible = true;
                        ReplacePrepImageLabel1.Visible = true;

                        AttachPrepImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename1.Value = "";
                        AttachPrepFileUrl1.Text = "";
                        AttachPrepImage1.Visible = false;
                        AttachPrepImageDelete1.Visible = false;
                        ReplacePrepImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage2.ImageUrl = attach;
                            AttachPrepFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage2.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl2.Text = "";
                        }
                        ExistingPrepFilename2.Value = attach;
                        AttachPrepImage2.Visible = true;

                        AttachPrepImageDelete2.Visible = true;
                        ReplacePrepImageLabel2.Visible = true;

                        AttachPrepImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename2.Value = "";
                        AttachPrepFileUrl2.Text = "";
                        AttachPrepImage2.Visible = false;
                        AttachPrepImageDelete2.Visible = false;
                        ReplacePrepImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage3.ImageUrl = attach;
                            AttachPrepFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage3.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl3.Text = "";
                        }
                        ExistingPrepFilename3.Value = attach;
                        AttachPrepImage3.Visible = true;

                        AttachPrepImageDelete3.Visible = true;
                        ReplacePrepImageLabel3.Visible = true;

                        AttachPrepImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename3.Value = "";
                        AttachPrepFileUrl3.Text = "";
                        AttachPrepImage3.Visible = false;
                        AttachPrepImageDelete3.Visible = false;
                        ReplacePrepImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage4.ImageUrl = attach;
                            AttachPrepFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage4.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl4.Text = "";
                        }
                        ExistingPrepFilename4.Value = attach;
                        AttachPrepImage4.Visible = true;

                        AttachPrepImageDelete4.Visible = true;
                        ReplacePrepImageLabel4.Visible = true;

                        AttachPrepImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename4.Value = "";
                        AttachPrepFileUrl4.Text = "";
                        AttachPrepImage4.Visible = false;
                        AttachPrepImageDelete4.Visible = false;
                        ReplacePrepImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage5.ImageUrl = attach;
                            AttachPrepFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage5.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl5.Text = "";
                        }
                        ExistingPrepFilename5.Value = attach;
                        AttachPrepImage5.Visible = true;

                        AttachPrepImageDelete5.Visible = true;
                        ReplacePrepImageLabel5.Visible = true;

                        AttachPrepImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename5.Value = "";
                        AttachPrepFileUrl5.Text = "";
                        AttachPrepImage5.Visible = false;
                        AttachPrepImageDelete5.Visible = false;
                        ReplacePrepImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddPrepTitle.Text = "Edit Prep";
                AddPrepButton.Text = "Save";
                AddPrepPanel.Visible = true;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void PrepList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePrepIdValue = ((HiddenField)(PrepList.Rows[e.RowIndex].FindControl("VehicleWirePrepId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select a.VehicleWirePrepId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                        from dbo.VehicleWirePrep a 
                        where VehicleWirePrepId=@VehicleWirePrepId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWirePrep where VehicleWirePrepId=@VehicleWirePrepId";
                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleWirePrepId, VehicleMakeModelYearId 
                        from dbo.VehicleWirePrep with (NOLOCK) 
                        where VehicleWirePrepId=@VehicleWirePrepId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePrepEditor(
                            EditorWorkId, VehicleWirePrepId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWirePrepId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWirePrep where VehicleWirePrepId = @VehicleWirePrepId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWirePrepId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddPrepPanel.Visible = false;
                SearchInfo(5);

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void AddRoutingPanelButton_Click(object sender, EventArgs e)
        {
            AddRoutingTitle.Text = "Add Routing/Placement";
            AddRoutingButton.Text = "Add";
            VehicleWirePlacementRoutingIdHidden.Value = "";
            AddRoutingPanel.Visible = true;

            RoutingStep.Text = "";
            Routingeditor.Text = "";

            AttachRoutingImageDelete1.Checked = false;
            AttachRoutingImageDelete2.Checked = false;
            AttachRoutingImageDelete3.Checked = false;
            AttachRoutingImageDelete4.Checked = false;
            AttachRoutingImageDelete5.Checked = false;

            ExistingRoutingFilename1.Value = "";
            AttachRoutingImage1.Visible = false;
            AttachRoutingImageDelete1.Visible = false;
            ReplaceRoutingImageLabel1.Visible = false;
            AttachRoutingFileUrl1.Text = "";

            ExistingRoutingFilename2.Value = "";
            AttachRoutingImage2.Visible = false;
            AttachRoutingImageDelete2.Visible = false;
            ReplaceRoutingImageLabel2.Visible = false;
            AttachRoutingFileUrl2.Text = "";

            ExistingRoutingFilename3.Value = "";
            AttachRoutingImage3.Visible = false;
            AttachRoutingImageDelete3.Visible = false;
            ReplaceRoutingImageLabel3.Visible = false;
            AttachRoutingFileUrl3.Text = "";

            ExistingRoutingFilename4.Value = "";
            AttachRoutingImage4.Visible = false;
            AttachRoutingImageDelete4.Visible = false;
            ReplaceRoutingImageLabel4.Visible = false;
            AttachRoutingFileUrl4.Text = "";

            ExistingRoutingFilename5.Value = "";
            AttachRoutingImage5.Visible = false;
            AttachRoutingImageDelete5.Visible = false;
            ReplaceRoutingImageLabel5.Visible = false;
            AttachRoutingFileUrl5.Text = "";

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");

        }

        protected void CancelRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddRoutingPanel.Visible = false;

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");
        }

        protected void AddRoutingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (RoutingStep.Text == "")
            {
                ShowError("Please enter Step");
                RoutingStep.Focus();
                return;
            }
            if (Routingeditor.Text == "")
            {
                ShowError("Please enter Note");
                Routingeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachRoutingFile1.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachRoutingFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachRoutingFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachRoutingFile2.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachRoutingFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachRoutingFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachRoutingFile3.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachRoutingFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachRoutingFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachRoutingFile4.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachRoutingFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachRoutingFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachRoutingFile5.PostedFile, FullPath, "Routing", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachRoutingFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachRoutingFileUrl5.Text.Trim();
            }

            if (AddRoutingButton.Text == "Add" || VehicleWirePlacementRoutingEditorWorkActionHidden.Value == "0")
            {
                if (VehicleWirePlacementRoutingEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    AddRouting(VehicleWirePlacementRoutingEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, RoutingStep.Text, Routingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else if (AddRoutingButton.Text == "Add")
                {
                    AddRouting("", VehicleMakeModelYearIdHidden.Value, RoutingStep.Text, Routingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }
            else
            {
                if ((AttachRoutingImageDelete1.Checked && ExistingRoutingFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingRoutingFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingRoutingFilename1.Value);
                    }
                }
                if ((AttachRoutingImageDelete2.Checked && ExistingRoutingFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingRoutingFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingRoutingFilename2.Value);
                    }
                }
                if ((AttachRoutingImageDelete3.Checked && ExistingRoutingFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingRoutingFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingRoutingFilename3.Value);
                    }
                }
                if ((AttachRoutingImageDelete4.Checked && ExistingRoutingFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingRoutingFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingRoutingFilename4.Value);
                    }
                }
                if ((AttachRoutingImageDelete5.Checked && ExistingRoutingFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingRoutingFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingRoutingFilename5.Value);
                    }
                }

                string Attach1 = "";
                if (AttachRoutingImageDelete1.Visible && AttachRoutingImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingRoutingFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachRoutingImageDelete2.Visible && AttachRoutingImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingRoutingFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachRoutingImageDelete3.Visible && AttachRoutingImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingRoutingFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachRoutingImageDelete4.Visible && AttachRoutingImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingRoutingFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachRoutingImageDelete5.Visible && AttachRoutingImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingRoutingFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                if (VehicleWirePlacementRoutingEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdateRouting(VehicleWirePlacementRoutingEditorWorkIdHidden.Value, VehicleWirePlacementRoutingIdHidden.Value, VehicleMakeModelYearIdHidden.Value, 
                        RoutingStep.Text, Routingeditor.Text, Attach1, Attach2, Attach3, Attach4, Attach5);
                }
                else
                {
                    UpdateRouting("", VehicleWirePlacementRoutingIdHidden.Value, VehicleMakeModelYearIdHidden.Value,
                        RoutingStep.Text, Routingeditor.Text, Attach1, Attach2, Attach3, Attach4, Attach5);
                }

            }

            SearchInfo(6);
            AddRoutingPanel.Visible = false;

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");
        }

        private void AddRouting(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleWirePlacementRouting (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePlacementRoutingEditor (
                        EditorWorkId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateRouting(string ApprovalEditorWorkId, string VehicleWirePlacementRoutingId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWirePlacementRoutingId
                            from dbo.VehicleWirePlacementRoutingEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWirePlacementRoutingId = reader["VehicleWirePlacementRoutingId"].ToString();
                        }
                        reader.Close();


                        sql = @"update dbo.VehicleWirePlacementRouting 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingId);

                        cmd.ExecuteNonQuery();

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleWirePlacementRouting 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePlacementRoutingEditor (
                        EditorWorkId, VehicleWirePlacementRoutingId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWirePlacementRoutingId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowRoutingList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetRoutingQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            RoutingList.DataSource = ds;
            RoutingList.DataBind();

            if (RoutingList != null && RoutingList.HeaderRow != null && RoutingList.HeaderRow.Cells.Count > 0)
            {
                RoutingList.Attributes["data-page"] = "false";

                RoutingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                RoutingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                RoutingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                RoutingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                RoutingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                RoutingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                RoutingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                //PlacementTitle.Visible = true;

                RoutingHeader.Text = "Routing/Placement (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                RoutingHeader.Text = "Routing/Placement (0)";
                //PlacementTitle.Visible = false;
            }
        }


        private void ShowRoutingListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetRoutingQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            RoutingListEditor.DataSource = ds;
            RoutingListEditor.DataBind();

            if (RoutingListEditor != null && RoutingListEditor.HeaderRow != null && RoutingListEditor.HeaderRow.Cells.Count > 0)
            {
                RoutingListEditor.Attributes["data-page"] = "false";

                RoutingListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                RoutingListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                RoutingListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                RoutingListEditor.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                RoutingListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                RoutingListEditor.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                RoutingListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    RoutingListEditor.HeaderRow.Cells[9].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    RoutingListEditor.Columns[7].Visible = false;
                    RoutingListEditor.Columns[8].Visible = false;
                    RoutingListEditor.HeaderRow.Cells[9].Text = "Delete";
                }
            }
        }

        protected void RoutingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWirePlacementRoutingIdHidden.Value = ((HiddenField)(RoutingList.SelectedRow.FindControl("VehicleWirePlacementRoutingId"))).Value;
            VehicleWirePlacementRoutingEditorWorkIdHidden.Value = "";
            VehicleWirePlacementRoutingEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWirePlacementRoutingId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWirePlacementRouting  ";
                sql += "where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    RoutingStep.Text = reader["Step"].ToString();
                    Routingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage1.ImageUrl = attach;
                            AttachRoutingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage1.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl1.Text = "";
                        }
                        ExistingRoutingFilename1.Value = attach;
                        AttachRoutingImage1.Visible = true;

                        AttachRoutingImageDelete1.Visible = true;
                        ReplaceRoutingImageLabel1.Visible = true;

                        AttachRoutingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename1.Value = "";
                        AttachRoutingFileUrl1.Text = "";
                        AttachRoutingImage1.Visible = false;
                        AttachRoutingImageDelete1.Visible = false;
                        ReplaceRoutingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage2.ImageUrl = attach;
                            AttachRoutingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage2.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl2.Text = "";
                        }
                        ExistingRoutingFilename2.Value = attach;
                        AttachRoutingImage2.Visible = true;

                        AttachRoutingImageDelete2.Visible = true;
                        ReplaceRoutingImageLabel2.Visible = true;

                        AttachRoutingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename2.Value = "";
                        AttachRoutingFileUrl2.Text = "";
                        AttachRoutingImage2.Visible = false;
                        AttachRoutingImageDelete2.Visible = false;
                        ReplaceRoutingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage3.ImageUrl = attach;
                            AttachRoutingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage3.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl3.Text = "";
                        }
                        ExistingRoutingFilename3.Value = attach;
                        AttachRoutingImage3.Visible = true;

                        AttachRoutingImageDelete3.Visible = true;
                        ReplaceRoutingImageLabel3.Visible = true;

                        AttachRoutingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename3.Value = "";
                        AttachRoutingFileUrl3.Text = "";
                        AttachRoutingImage3.Visible = false;
                        AttachRoutingImageDelete3.Visible = false;
                        ReplaceRoutingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage4.ImageUrl = attach;
                            AttachRoutingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage4.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl4.Text = "";
                        }
                        ExistingRoutingFilename4.Value = attach;
                        AttachRoutingImage4.Visible = true;

                        AttachRoutingImageDelete4.Visible = true;
                        ReplaceRoutingImageLabel4.Visible = true;

                        AttachRoutingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename4.Value = "";
                        AttachRoutingFileUrl4.Text = "";
                        AttachRoutingImage4.Visible = false;
                        AttachRoutingImageDelete4.Visible = false;
                        ReplaceRoutingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage5.ImageUrl = attach;
                            AttachRoutingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage5.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl5.Text = "";
                        }
                        ExistingRoutingFilename5.Value = attach;
                        AttachRoutingImage5.Visible = true;

                        AttachRoutingImageDelete5.Visible = true;
                        ReplaceRoutingImageLabel5.Visible = true;

                        AttachRoutingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename5.Value = "";
                        AttachRoutingFileUrl5.Text = "";
                        AttachRoutingImage5.Visible = false;
                        AttachRoutingImageDelete5.Visible = false;
                        ReplaceRoutingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddRoutingTitle.Text = "Edit Routing/Placement";
                AddRoutingButton.Text = "Save";
                AddRoutingPanel.Visible = true;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void RoutingList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWirePlacementRoutingIdValue = ((HiddenField)(RoutingList.Rows[e.RowIndex].FindControl("VehicleWirePlacementRoutingId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select a.VehicleWirePlacementRoutingId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWirePlacementRouting a 
                    where VehicleWirePlacementRoutingId=@VehicleWirePlacementRoutingId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWirePlacementRouting where VehicleWirePlacementRoutingId=@VehicleWirePlacementRoutingId";
                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleWirePlacementRoutingId, VehicleMakeModelYearId 
                        from dbo.VehicleWirePlacementRouting with (NOLOCK) 
                        where VehicleWirePlacementRoutingId=@VehicleWirePlacementRoutingId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWirePlacementRoutingEditor(
                            EditorWorkId, VehicleWirePlacementRoutingId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWirePlacementRoutingId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWirePlacementRouting where VehicleWirePlacementRoutingId = @VehicleWirePlacementRoutingId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWirePlacementRoutingId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddRoutingPanel.Visible = false;
                SearchInfo(6);

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddFBPanelButton_Click(object sender, EventArgs e)
        {
            AddFBTitle.Text = "Add Facebook";
            AddFBButton.Text = "Add";
            VehicleWireFacebookIdHidden.Value = "";
            AddFBPanel.Visible = true;

            FBeditor.Text = "";
            FBUrl.Text = "";

            AttachFBImageDelete1.Checked = false;
            AttachFBImageDelete2.Checked = false;
            AttachFBImageDelete3.Checked = false;
            AttachFBImageDelete4.Checked = false;
            AttachFBImageDelete5.Checked = false;

            ExistingFBFilename1.Value = "";
            AttachFBImage1.Visible = false;
            AttachFBImageDelete1.Visible = false;
            ReplaceFBImageLabel1.Visible = false;
            AttachFBFileUrl1.Text = "";

            ExistingFBFilename2.Value = "";
            AttachFBImage2.Visible = false;
            AttachFBImageDelete2.Visible = false;
            ReplaceFBImageLabel2.Visible = false;
            AttachFBFileUrl2.Text = "";

            ExistingFBFilename3.Value = "";
            AttachFBImage3.Visible = false;
            AttachFBImageDelete3.Visible = false;
            ReplaceFBImageLabel3.Visible = false;
            AttachFBFileUrl3.Text = "";

            ExistingFBFilename4.Value = "";
            AttachFBImage4.Visible = false;
            AttachFBImageDelete4.Visible = false;
            ReplaceFBImageLabel4.Visible = false;
            AttachFBFileUrl4.Text = "";

            ExistingFBFilename5.Value = "";
            AttachFBImage5.Visible = false;
            AttachFBImageDelete5.Visible = false;
            ReplaceFBImageLabel5.Visible = false;
            AttachFBFileUrl5.Text = "";

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void CancelFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddFBPanel.Visible = false;

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");
        }

        protected void AddFBButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (FBeditor.Text == "")
            {
                ShowError("Please enter Facebook Result");
                FBeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachFBFile1.PostedFile, FullPath, "FB", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachFBFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachFBFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachFBFile2.PostedFile, FullPath, "FB", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachFBFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachFBFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachFBFile3.PostedFile, FullPath, "FB", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachFBFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachFBFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachFBFile4.PostedFile, FullPath, "FB", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachFBFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachFBFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachFBFile5.PostedFile, FullPath, "FB", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachFBFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachFBFileUrl5.Text.Trim();
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                if (AddFBButton.Text == "Add" || VehicleWireFacebookEditorWorkActionHidden.Value == "0")
                {
                    if (VehicleWireFacebookEditorWorkActionHidden.Value == "0") // Edit & Approve
                    {
                        AddFacebook(VehicleWireFacebookEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, FBeditor.Text, FBUrl.Text,
                            FileName1, FileName2, FileName3, FileName4, FileName5);
                    }
                    else if (AddFBButton.Text == "Add")
                    {
                        AddFacebook("", VehicleMakeModelYearIdHidden.Value, FBeditor.Text, FBUrl.Text,
                            FileName1, FileName2, FileName3, FileName4, FileName5);
                    }
                }
                else
                {
                    if ((AttachFBImageDelete1.Checked && ExistingFBFilename1.Value != "") || FileName1 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename1.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename1.Value);
                        }
                    }
                    if ((AttachFBImageDelete2.Checked && ExistingFBFilename2.Value != "") || FileName2 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename2.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename2.Value);
                        }
                    }
                    if ((AttachFBImageDelete3.Checked && ExistingFBFilename3.Value != "") || FileName3 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename3.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename3.Value);
                        }
                    }
                    if ((AttachFBImageDelete4.Checked && ExistingFBFilename4.Value != "") || FileName4 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename4.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename4.Value);
                        }
                    }
                    if ((AttachFBImageDelete5.Checked && ExistingFBFilename5.Value != "") || FileName5 != "")
                    {
                        if (File.Exists(FullPath + ExistingFBFilename5.Value))
                        {
                            File.Delete(FullPath + ExistingFBFilename5.Value);
                        }
                    }

                    string Attach1 = "";
                    if (AttachFBImageDelete1.Visible && AttachFBImageDelete1.Checked)
                    {
                        Attach1 = "";
                    }
                    else
                    {
                        if (FileName1 == "")
                        {
                            FileName1 = ExistingFBFilename1.Value;
                        }
                        Attach1 = FileName1;
                    }

                    string Attach2 = "";
                    if (AttachFBImageDelete2.Visible && AttachFBImageDelete2.Checked)
                    {
                        Attach2 = "";
                    }
                    else
                    {
                        if (FileName2 == "")
                        {
                            FileName2 = ExistingFBFilename2.Value;
                        }
                        Attach2 = FileName2;
                    }

                    string Attach3 = "";
                    if (AttachFBImageDelete3.Visible && AttachFBImageDelete3.Checked)
                    {
                        Attach3 = "";
                    }
                    else
                    {
                        if (FileName3 == "")
                        {
                            FileName3 = ExistingFBFilename3.Value;
                        }
                        Attach3 = FileName3;
                    }

                    string Attach4 = "";
                    if (AttachFBImageDelete4.Visible && AttachFBImageDelete4.Checked)
                    {
                        Attach4 = "";
                    }
                    else
                    {
                        if (FileName4 == "")
                        {
                            FileName4 = ExistingFBFilename4.Value;
                        }
                        Attach4 = FileName4;
                    }

                    string Attach5 = "";
                    if (AttachFBImageDelete5.Visible && AttachFBImageDelete5.Checked)
                    {
                        Attach5 = "";
                    }
                    else
                    {
                        if (FileName5 == "")
                        {
                            FileName5 = ExistingFBFilename5.Value;
                        }
                        Attach5 = FileName5;
                    }

                    if (VehicleWireFacebookEditorWorkActionHidden.Value == "1") // Edit & Approve
                    {
                        UpdateFacebook(VehicleWireFacebookEditorWorkIdHidden.Value, VehicleWireFacebookIdHidden.Value, VehicleMakeModelYearIdHidden.Value, 
                            FBeditor.Text, FBUrl.Text,
                            Attach1, Attach2, Attach3, Attach4, Attach5);
                    }
                    else
                    {
                        UpdateFacebook("", VehicleWireFacebookIdHidden.Value, VehicleMakeModelYearIdHidden.Value,
                            FBeditor.Text, FBUrl.Text,
                            Attach1, Attach2, Attach3, Attach4, Attach5);
                    }
                }

                SearchInfo(2);
                AddFBPanel.Visible = false;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void AddFacebook(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string Note, string URL, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleWireFacebook (VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @Note, @URL, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                    cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();

                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFacebookEditor (
                        EditorWorkId, VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @Note, @URL, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                    cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateFacebook(string ApprovalEditorWorkId, string VehicleWireFacebookId, string VehicleMakeModelYearId, string Note, string URL, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWireFacebookId
                            from dbo.VehicleWireFacebookEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWireFacebookId = reader["VehicleWireFacebookId"].ToString();
                        }
                        reader.Close();


                        sql = @"update dbo.VehicleWireFacebook 
                            set Note=@Note, URL=@URL, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireFacebookId = @VehicleWireFacebookId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                        cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookId);

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleWireFacebook 
                            set Note=@Note, URL=@URL, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireFacebookId = @VehicleWireFacebookId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                        cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFacebookEditor (
                        EditorWorkId, VehicleWireFacebookId, VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWireFacebookId, @VehicleMakeModelYearId, @Note, @URL, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                    cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 500).Value = URL;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowFBList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetFacebookQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            FBList.DataSource = ds;
            FBList.DataBind();

            if (FBList != null && FBList.HeaderRow != null && FBList.HeaderRow.Cells.Count > 0)
            {
                FBList.Attributes["data-page"] = "false";

                FBList.HeaderRow.TableSection = TableRowSection.TableHeader;

                FBResultHeader.Text = "Facebook Result (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                FBResultHeader.Text = "Facebook Result (0)";
            }
        }


        private void ShowFBListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetFacebookQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            FBListEditor.DataSource = ds;
            FBListEditor.DataBind();

            if (FBListEditor != null && FBListEditor.HeaderRow != null && FBListEditor.HeaderRow.Cells.Count > 0)
            {
                FBListEditor.Attributes["data-page"] = "false";

                FBListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                FBListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                FBListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                FBListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";

                FBListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                FBListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";

                FBListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    FBListEditor.HeaderRow.Cells[8].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    FBListEditor.Columns[6].Visible = false;
                    FBListEditor.Columns[7].Visible = false;
                    FBListEditor.HeaderRow.Cells[8].Text = "Delete";
                }
            }
        }

        protected void FBList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[3].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Facebook Result?');";
                    }
                }
            }
        }

        protected void FBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireFacebookIdHidden.Value = ((HiddenField)(FBList.SelectedRow.FindControl("VehicleWireFacebookId"))).Value;
            VehicleWireFacebookEditorWorkIdHidden.Value = "";
            VehicleWireFacebookEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireFacebookId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireFacebook  ";
                sql += "where VehicleWireFacebookId = @VehicleWireFacebookId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    FBeditor.Text = reader["Note"].ToString();
                    FBUrl.Text = reader["URL"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage1.ImageUrl = attach;
                            AttachFBFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachFBImage1.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl1.Text = "";
                        }
                        ExistingFBFilename1.Value = attach;
                        AttachFBImage1.Visible = true;
                        AttachFBImageDelete1.Visible = true;
                        ReplaceFBImageLabel1.Visible = true;

                        AttachFBImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename1.Value = "";
                        AttachFBFileUrl1.Text = "";
                        AttachFBImage1.Visible = false;
                        AttachFBImageDelete1.Visible = false;
                        ReplaceFBImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage2.ImageUrl = attach;
                            AttachFBFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachFBImage2.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl2.Text = "";
                        }
                        ExistingFBFilename2.Value = attach;
                        AttachFBImage2.Visible = true;
                        AttachFBImageDelete2.Visible = true;
                        ReplaceFBImageLabel2.Visible = true;

                        AttachFBImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename2.Value = "";
                        AttachFBFileUrl2.Text = "";
                        AttachFBImage2.Visible = false;
                        AttachFBImageDelete2.Visible = false;
                        ReplaceFBImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage3.ImageUrl = attach;
                            AttachFBFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachFBImage3.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl3.Text = "";
                        }
                        ExistingFBFilename3.Value = attach;
                        AttachFBImage3.Visible = true;
                        AttachFBImageDelete3.Visible = true;
                        ReplaceFBImageLabel3.Visible = true;

                        AttachFBImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename3.Value = "";
                        AttachFBFileUrl3.Text = "";
                        AttachFBImage3.Visible = false;
                        AttachFBImageDelete3.Visible = false;
                        ReplaceFBImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage4.ImageUrl = attach;
                            AttachFBFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachFBImage4.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl4.Text = "";
                        }
                        ExistingFBFilename4.Value = attach;
                        AttachFBImage4.Visible = true;
                        AttachFBImageDelete4.Visible = true;
                        ReplaceFBImageLabel4.Visible = true;

                        AttachFBImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename4.Value = "";
                        AttachFBFileUrl4.Text = "";
                        AttachFBImage4.Visible = false;
                        AttachFBImageDelete4.Visible = false;
                        ReplaceFBImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage5.ImageUrl = attach;
                            AttachFBFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachFBImage5.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl5.Text = "";
                        }
                        ExistingFBFilename5.Value = attach;
                        AttachFBImage5.Visible = true;
                        AttachFBImageDelete5.Visible = true;
                        ReplaceFBImageLabel5.Visible = true;

                        AttachFBImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename5.Value = "";
                        AttachFBFileUrl5.Text = "";
                        AttachFBImage5.Visible = false;
                        AttachFBImageDelete5.Visible = false;
                        ReplaceFBImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddFBTitle.Text = "Edit Facebook";
                AddFBButton.Text = "Save";
                AddFBPanel.Visible = true;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireFacebookIdValue = ((HiddenField)(FBList.Rows[e.RowIndex].FindControl("VehicleWireFacebookId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select a.VehicleWireFacebookId, a.URL, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                        from dbo.VehicleWireFacebook a 
                        where VehicleWireFacebookId=@VehicleWireFacebookId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWireFacebook where VehicleWireFacebookId=@VehicleWireFacebookId";
                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleWireFacebookId, VehicleMakeModelYearId 
                        from dbo.VehicleWireFacebook with (NOLOCK) 
                        where VehicleWireFacebookId=@VehicleWireFacebookId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireFacebookEditor(
                            EditorWorkId, VehicleWireFacebookId, VehicleMakeModelYearId,
                            Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWireFacebookId, VehicleMakeModelYearId,
                            Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWireFacebook where VehicleWireFacebookId = @VehicleWireFacebookId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireFacebookId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddFBPanel.Visible = false;
                SearchInfo(2);

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddDocumentPanelButton_Click(object sender, EventArgs e)
        {
            AddDocumentTitle.Text = "Add Document";
            AddDocumentButton.Text = "Add";
            VehicleDocumentIdHidden.Value = "";
            AddDocumentPanel.Visible = true;

            ExistingDocument.Text = "";
            CurrentDocumentAttachFile.Value = "";
            AttachFileUrl.Text = "";

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        private void ShowDocumentList(SqlConnection Con)
        {
            string sql = GetDocumentQuery();

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            DocumentList.DataSource = ds;
            DocumentList.DataBind();

            if (DocumentList != null && DocumentList.HeaderRow != null && DocumentList.HeaderRow.Cells.Count > 0)
            {
                DocumentList.Attributes["data-page"] = "false";

                DocumentList.HeaderRow.TableSection = TableRowSection.TableHeader;

                DocumentHeader.Text = "Documents (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                DocumentHeader.Text = "Documents (0)";
            }
        }

        private void ShowDocumentListEditor(SqlConnection Con)
        {
            string sql = GetDocumentQueryEditor();

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            DocumentListEditor.DataSource = ds;
            DocumentListEditor.DataBind();

            if (DocumentListEditor != null && DocumentListEditor.HeaderRow != null && DocumentListEditor.HeaderRow.Cells.Count > 0)
            {
                DocumentListEditor.Attributes["data-page"] = "false";

                DocumentListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                DocumentListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                DocumentListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                DocumentListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                DocumentListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                DocumentListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                DocumentListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                DocumentListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";

                DocumentListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                DocumentListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";

                DocumentListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    DocumentListEditor.HeaderRow.Cells[7].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    DocumentListEditor.Columns[5].Visible = false;
                    DocumentListEditor.Columns[6].Visible = false;
                    DocumentListEditor.HeaderRow.Cells[7].Text = "Delete";
                }
            }
        }

        protected void DocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[2].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this document?');";
                    }
                }

                HiddenField VehicleDocumentId = e.Row.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Row.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Row.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Row.FindControl("DocumentLabel") as Label;
                    if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
                    {
                        DocumentLabel.Text = "<a href='" + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a> - " + AttachFile.Value;
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }
            }
        }

        protected void DocumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleDocumentIdHidden.Value = ((HiddenField)(DocumentList.SelectedRow.FindControl("VehicleDocumentId"))).Value;
            VehicleDocumentEditorWorkIdHidden.Value = "";
            VehicleDocumentEditorWorkActionHidden.Value = "";

            HiddenField AttachFile = DocumentList.SelectedRow.FindControl("AttachFile") as HiddenField;
            Label DocumentLabel = DocumentList.SelectedRow.FindControl("DocumentLabel") as Label;

            ExistingDocument.Text = DocumentLabel.Text;
            CurrentDocumentAttachFile.Value = AttachFile.Value;
            if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
            {
                AttachFileUrl.Text = AttachFile.Value;
            }

            AddDocumentTitle.Text = "Edit Document";
            AddDocumentButton.Text = "Save";
            AddDocumentPanel.Visible = true;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void DocumentList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleDocumentIdValue = ((HiddenField)(DocumentList.Rows[e.RowIndex].FindControl("VehicleDocumentId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select AttachFile 
                        from dbo.VehicleDocument a 
                        where VehicleDocumentId=@VehicleDocumentId";

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["AttachFile"].ToString()))
                            {
                                File.Delete(FullPath + reader["AttachFile"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleDocument where VehicleDocumentId=@VehicleDocumentId";
                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleDocumentId, VehicleMakeModelYearId 
                        from dbo.VehicleDocument with (NOLOCK) 
                        where VehicleDocumentId=@VehicleDocumentId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleDocumentEditor (
                            EditorWorkId, VehicleDocumentId, VehicleMakeModelYearId,
                            DocumentName, AttachFile)
                        select @EditorWorkId, VehicleDocumentId, VehicleMakeModelYearId,
                            DocumentName, AttachFile
                        from dbo.VehicleDocument where VehicleDocumentId = @VehicleDocumentId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddDocumentPanel.Visible = false;
                SearchInfo(3);

                ResetTab();
                DocumentPaneltab.Attributes.Add("Class", "tab-title active");
                DocumentPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddDocumentPanel.Visible = false;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        protected void AddDocumentButton_Click(object sender, EventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            string DocumentName;
            SaveFile(AttachFile.PostedFile, FullPath, "Document", Make, Model, Year, out FileName1, out DocumentName);
            if (FileName1 == "" && AttachFileUrl.Text.Trim() != "")
            {
                FileName1 = AttachFileUrl.Text.Trim();
                DocumentName = "URL";
            }

            if (AddDocumentButton.Text == "Add" || VehicleDocumentEditorWorkActionHidden.Value == "0")
            {
                if (VehicleDocumentEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    AddDocument(VehicleDocumentEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DocumentName, FileName1);
                }
                else if (AddDocumentButton.Text == "Add")
                {
                    AddDocument("", VehicleMakeModelYearIdHidden.Value, DocumentName, FileName1);
                }
            }
            else
            {
                if (File.Exists(FullPath + CurrentDocumentAttachFile.Value))
                {
                    File.Delete(FullPath + CurrentDocumentAttachFile.Value);
                }

                if (VehicleDocumentEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdateDocument(VehicleDocumentEditorWorkIdHidden.Value, VehicleDocumentIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DocumentName, FileName1);
                }
                else
                {
                    UpdateDocument("", VehicleDocumentIdHidden.Value, VehicleMakeModelYearIdHidden.Value, DocumentName, FileName1);
                }

            }

            SearchInfo(3);
            AddDocumentPanel.Visible = false;

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");
        }

        private void AddDocument(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string DocumentName, string AttachFile)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleDocument (VehicleMakeModelYearId, DocumentName, AttachFile, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @DocumentName, @AttachFile, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                    cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();

                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();


                    sql = @"insert into dbo.VehicleDocumentEditor (
                        EditorWorkId, VehicleMakeModelYearId, DocumentName, AttachFile) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @DocumentName, @AttachFile";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                    cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateDocument(string ApprovalEditorWorkId, string VehicleDocumentId, string VehicleMakeModelYearId, string DocumentName, string AttachFile)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleDocumentId
                            from dbo.VehicleDocumentEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleDocumentId = reader["VehicleDocumentId"].ToString();
                        }
                        reader.Close();

                        sql = @"update dbo.VehicleDocument 
                            set DocumentName=@DocumentName, AttachFile=@AttachFile, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleDocumentId = @VehicleDocumentId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                        cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentId);

                        cmd.ExecuteNonQuery();

                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleDocument 
                            set DocumentName=@DocumentName, AttachFile=@AttachFile, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleDocumentId = @VehicleDocumentId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                        cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleDocumentEditor (
                        EditorWorkId, VehicleDocumentId, VehicleMakeModelYearId, DocumentName, AttachFile) 
                        select @EditorWorkId, @VehicleDocumentId, @VehicleMakeModelYearId, @DocumentName, @AttachFile";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleDocumentId", SqlDbType.Int).Value = int.Parse(VehicleDocumentId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 200).Value = DocumentName;
                    cmd.Parameters.Add("@AttachFile", SqlDbType.NVarChar, 2000).Value = AttachFile;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }


        protected void EditUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessagePanel.Visible = true;
        }

        protected void CancelUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessagePanel.Visible = false;
        }

        protected void SaveUrgentMessageButton_Click(object sender, EventArgs e)
        {
            UpdateUrgentMessage(VehicleMakeModelYearIdHidden.Value, UrgentMessageEdit.Text);

            UpdateUrgentMessagePanel.Visible = false;
        }

        private void UpdateUrgentMessage(string VehicleMakeModelYearId, string msg)
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "if not exists (select * from dbo.VehicleUrgentMessage where VehicleMakeModelYearId=@VehicleMakeModelYearId) begin ";
                sql += "insert into dbo.VehicleUrgentMessage (VehicleMakeModelYearId, Note, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @Note, getdate(), @UpdatedBy; end ";
                sql += "else begin ";
                sql += "update dbo.VehicleUrgentMessage set Note=@Note, UpdatedDt=getdate(), UpdatedBy=@UpdatedBy ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId; end";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                cmd.Parameters.Add("@Note", SqlDbType.NVarChar, 300).Value = msg;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                UrgentMessageLabel.Text = UrgentMessageEdit.Text;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ShowCommentList()
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select a.VehicleCommentId, a.Comment, b.FirstName + ' ' + b.LastName as Name, a.UpdatedDt ";
                sql += "from dbo.VehicleComment a ";
                sql += "left join dbo.AspNetUsers b on a.UpdatedBy = b.Id ";
                sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by a.UpdatedDt desc ";

                SqlCommand Cmd = new SqlCommand(sql, conn);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CommentList.DataSource = ds;
                CommentList.DataBind();

                if (CommentList != null && CommentList.HeaderRow != null && CommentList.HeaderRow.Cells.Count > 0)
                {
                    CommentList.Attributes["data-page"] = "false";

                    CommentList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    CommentList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    CommentList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    CommentList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    CommentList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    CommentList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    CommentList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CommentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommentList.PageIndex = e.NewPageIndex;
            ShowCommentList();
        }

        protected void SaveInternalNoteButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (InternalNoteTxt.Text.Trim() == "")
            {
                ShowWireError("Please enter InternalNote.");
                return;
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.VehicleInternalNote (VehicleMakeModelYearId, InternalNote, UpdatedDt, UpdatedBy) ";
                sql += "select @VehicleMakeModelYearId, @InternalNote, getdate(), @UpdatedBy; ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@InternalNote", SqlDbType.NVarChar).Value = InternalNoteTxt.Text.Trim();
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                InternalNoteTxt.Text = "";

                ShowInternalNotes();
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }

        protected void ShowInternalNotes()
        {
            ClearError();

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "select a.VehicleInternalNoteId, a.InternalNote, b.FirstName + ' ' + b.LastName as Name, a.UpdatedDt ";
                sql += "from dbo.VehicleInternalNote a ";
                sql += "left join dbo.AspNetUsers b on a.UpdatedBy = b.Id ";
                sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by a.UpdatedDt desc ";

                SqlCommand Cmd = new SqlCommand(sql, conn);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                InternalNoteList.DataSource = ds;
                InternalNoteList.DataBind();

                if (InternalNoteList != null && InternalNoteList.HeaderRow != null && InternalNoteList.HeaderRow.Cells.Count > 0)
                {
                    InternalNoteList.Attributes["data-page"] = "false";

                    InternalNoteList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                    InternalNoteList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                    InternalNoteList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                    InternalNoteList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                    InternalNoteList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                    InternalNoteList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                    InternalNoteList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void InternalNoteList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            InternalNoteList.PageIndex = e.NewPageIndex;
            ShowInternalNotes();
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                StringBuilder excel = new StringBuilder();

                excel.AppendLine("Vehicle ID\t" + VehicleMakeModelYearIdHidden.Value);
                excel.AppendLine("Vehicle Make\t" + VehicleMakeList.SelectedItem.Text);
                excel.AppendLine("Vehicle Model\t" + VehicleModelList.SelectedItem.Text);
                excel.AppendLine("Vehicle Year\t" + VehicleYearList.SelectedItem.Text);
                excel.AppendLine("Link Vehicle ID\t" + LinkVehicleMakeModelYearIdHidden.Value);


                // Urgent Message
                string sql = GetUrgentQuery();

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                string umsg = "";
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    umsg = reader["Note"].ToString();
                }
                reader.Close();

                excel.AppendLine("");
                excel.AppendLine("[Urgent Message]");
                excel.AppendLine("Message:\t" + umsg.ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));

                // Wiring
                sql = GetWireQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Wiring]");
                excel.AppendLine("Id\tWire Function\tInstallation Type\tVehicle Color\tCM7X00/ADS Color\tLocation\tPin Out\tPolarity\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireFunctionId"].ToString());
                    excel.Append("\t"); excel.Append(reader["WireFunctionName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["InstallationTypeName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Colour"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["VehicleColor"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Location"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["PinOut"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Polarity"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Wire Note
                sql = GetWireNoteQuery();

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Wiring Note]");
                excel.AppendLine("Id\tInstallationTypeId\tNote");
                while (reader.Read())
                {
                    if (reader["VehicleWireNoteId"] != null && reader["VehicleWireNoteId"].ToString() != "")
                    {
                        excel.Append(reader["VehicleWireNoteId"].ToString());
                        excel.Append("\t"); excel.Append(reader["InstallationTypeId"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                        excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                        excel.AppendLine("");
                    }
                }
                reader.Close();

                // Disassembly
                sql = GetDisassemblyQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Disassembly]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireDisassemblyId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Prep
                sql = GetPrepQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Prep]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWirePrepId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Routing
                sql = GetRoutingQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Routing]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWirePlacementRoutingId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Programming
                sql = GetProgrammingQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Programming]");
                excel.AppendLine("Id\tStep\tDescription\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireProgrammingId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Step"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Facebook
                sql = GetFacebookQuery("");

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Facebook]");
                excel.AppendLine("Id\tNote\tURL\tFile 1\tFile 2\tFile 3\tFile 4\tFile 5");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleWireFacebookId"].ToString());
                    excel.Append("\t"); excel.Append(reader["Note"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["URL"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach1"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach2"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach3"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach4"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["Attach5"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                // Document
                sql = GetDocumentQuery();

                cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                reader = cmd.ExecuteReader();

                excel.AppendLine("");
                excel.AppendLine("[Document]");
                excel.AppendLine("Id\tDocument Name\tFile");
                while (reader.Read())
                {
                    excel.Append(reader["VehicleDocumentId"].ToString());
                    excel.Append("\t"); excel.Append(reader["DocumentName"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.Append("\t"); excel.Append(reader["AttachFile"].ToString().Replace(Environment.NewLine, "[NewLine]").Replace("\t", "[Tab]"));
                    excel.AppendLine("");
                }
                reader.Close();

                string VehicleName = VehicleMakeList.SelectedItem.Text.Trim() + "_" + VehicleModelList.SelectedItem.Text.Trim() + "_" + VehicleYearList.SelectedItem.Text.Trim();
                string filename = VehicleName + ".xls";
                string content_type = "text/csv";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = content_type;
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.Charset = "";
                this.EnableViewState = false;
                Response.Write(excel.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private string GetUrgentQuery()
        {
            return "select VehicleUrgentMessageId, Note from dbo.VehicleUrgentMessage where VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
        }

        private string GetWireQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireFunctionId, c.WireFunctionName, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, ";
            sql += "VehicleColor, PinOut, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireFunction a ";
            sql += "join dbo.VehicleMakeModelYear b on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId ";
            sql += "join dbo.WireFunction c on a.WireFunctionId = c.WireFunctionId ";
            sql += "left join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            if (SearchInstallationTypeList.SelectedValue != "-1")
            {
                sql += " and a.InstallationTypeId = " + SearchInstallationTypeList.SelectedValue + " ";
            }
            sql += " order by c.SortOrder ";

            return sql;
        }

        private string GetWireQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWireFunctionEditorId, a.EditorWorkId, StatusId,
                a.VehicleWireFunctionId, c.WireFunctionName, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, 
                VehicleColor, PinOut, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWireFunctionEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                left join dbo.WireFunction c on a.WireFunctionId = c.WireFunctionId 
                left join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId 
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }

        private string GetWireQueryBatchEdit(string InstallationTypeId, string SaveFolder)
        {
            string sql = @"select a.VehicleWireFunctionId, a.WireFunctionId, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, VehicleColor, PinOut
            from dbo.WireFunction c 
            left join dbo.VehicleWireFunction a on a.WireFunctionId = c.WireFunctionId 
            left join dbo.VehicleMakeModelYear b on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId 
            left join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId 
            where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value + @"
                 and a.InstallationTypeId = " + SearchInstallationTypeList.SelectedValue + @"  
             order by c.SortOrder";

            return sql;
        }


        private string GetWireNoteQuery()
        {
            string sql = "select a.VehicleWireNoteId, a.Note, d.InstallationTypeId, d.InstallationTypeName ";
            sql += "from dbo.InstallationType d ";
            sql += "left join dbo.VehicleWireNote a on a.InstallationTypeId = d.InstallationTypeId ";
            sql += "and a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        private string GetWireNoteQueryEditor()
        {
            string sql = @"select a.VehicleWireNoteEditorId, a.EditorWorkId, StatusId,
                a.VehicleWireNoteId, a.Note, d.InstallationTypeId, d.InstallationTypeName, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end
                from dbo.VehicleWireNoteEditor a 
                join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }


        private string GetDisassemblyQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireDisassemblyId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireDisassembly a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetDisassemblyQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWireDisassemblyEditorId, a.EditorWorkId, StatusId,
                a.VehicleWireDisassemblyId, a.Step, a.Note, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWireDisassemblyEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }

        private string GetPrepQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWirePrepId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWirePrep a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetPrepQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWirePrepEditorId, a.EditorWorkId, StatusId,
                a.VehicleWirePrepId, a.Step, a.Note, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWirePrepEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }


        private string GetRoutingQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWirePlacementRoutingId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWirePlacementRouting a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetRoutingQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWirePlacementRoutingEditorId, a.EditorWorkId, StatusId,
                a.VehicleWirePlacementRoutingId, a.Step, a.Note, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWirePlacementRoutingEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }


        private string GetProgrammingQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireProgrammingId, a.Step, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireProgramming a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;
            sql += " order by a.Step ";

            return sql;
        }

        private string GetProgrammingQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWireProgrammingEditorId, a.EditorWorkId, StatusId,
                a.VehicleWireProgrammingId, a.Step, a.Note, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWireProgrammingEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }


        private string GetFacebookQuery(string SaveFolder)
        {
            string sql = "select a.VehicleWireFacebookId, a.URL, a.Note, ";
            sql += "case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + "' + a.Attach1 else '' end else '' end as Attach1, ";
            sql += "case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + "' + a.Attach2 else '' end else '' end as Attach2, ";
            sql += "case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + "' + a.Attach3 else '' end else '' end as Attach3, ";
            sql += "case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + "' + a.Attach4 else '' end else '' end as Attach4, ";
            sql += "case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + "' + a.Attach5 else '' end else '' end as Attach5 ";
            sql += "from dbo.VehicleWireFacebook a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        private string GetFacebookQueryEditor(string SaveFolder)
        {
            string sql = @"select a.VehicleWireFacebookEditorId, a.EditorWorkId, StatusId,
                a.VehicleWireFacebookId, a.URL, a.Note, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
                case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then '" + SaveFolder + @"' + a.Attach1 else '' end else '' end as Attach1, 
                case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then '" + SaveFolder + @"' + a.Attach2 else '' end else '' end as Attach2, 
                case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then '" + SaveFolder + @"' + a.Attach3 else '' end else '' end as Attach3, 
                case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then '" + SaveFolder + @"' + a.Attach4 else '' end else '' end as Attach4, 
                case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then '" + SaveFolder + @"' + a.Attach5 else '' end else '' end as Attach5 
                from dbo.VehicleWireFacebookEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }



        private string GetDocumentQuery()
        {
            string sql = "select a.VehicleDocumentId, a.DocumentName, a.AttachFile ";
            sql += "from dbo.VehicleDocument a ";
            sql += "where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            return sql;
        }

        private string GetDocumentQueryEditor()
        {
            string sql = @"select a.VehicleDocumentEditorId, a.EditorWorkId, StatusId,
                a.VehicleDocumentId, a.DocumentName, a.AttachFile, EditorWorkDt, au.ProfileName, 
                EditorWorkActionName = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end, EditorWorkAction,
                StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end
                from dbo.VehicleDocumentEditor a 
                join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
                join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
                where a.VehicleMakeModelYearId = " + VehicleMakeModelYearIdHidden.Value;

            if (RoleNameHidden.Value.Contains("Editor"))
            {
                sql += @" and ew.EditorId = '" + User.Identity.GetUserId() + @"' ";
            }
            else
            {
                sql += @" and ew.StatusId = 0 ";
            }

            sql += @" order by EditorWorkDt desc";

            return sql;
        }


        protected void LinkVehicleButton_Click(object sender, EventArgs e)
        {
            AddVehicleTitle.Text = "Link Vehicle";
            AddVehicleButton.Text = "Save";

            if (LinkVehicleMakeIdHidden.Value != "")
            {
                MakeList.SelectedValue = LinkVehicleMakeIdHidden.Value;
                ShowModelList(false);
                ModelList.SelectedValue = LinkVehicleModelIdHidden.Value;
                YearList.Visible = true;
                ShowYearList();
                YearList.SelectedValue = LinkVehicleYearHidden.Value;

                Year.Text = LinkVehicleYearHidden.Value;
                Year.Visible = false;

                UnlinkVehicleButton.Visible = true;
            }
            else
            {
                MakeList.SelectedIndex = -1;
                ModelList.SelectedIndex = -1;
                YearList.SelectedIndex = -1;
                YearList.Visible = true;

                Year.Text = "";
                Year.Visible = false;

                UnlinkVehicleButton.Visible = false;
            }

            NewVehiclePanel.Visible = true;
            VehiclePicturePanel.Visible = false;

        }

        protected void ImportButton_Click(object sender, EventArgs e)
        {
            ImportPanel.Visible = true;
        }

        protected void ImportFileButton_Click(object sender, EventArgs e)
        {
            if (ImportFile.PostedFile != null && ImportFile.PostedFile.FileName != "")
            {
                string ImportVehicleMakeModelYearId = "";
                string LinkVehicleMakeModelYearId = "";
                var importFileString = new StreamReader(ImportFile.PostedFile.InputStream).ReadToEnd();
                string[] newlinesep = { Environment.NewLine };
                char[] tabsep = { '\t' };

                bool IsUrgentMessage = false;
                bool IsWiring = false;
                bool IsWiringNote = false;
                bool IsDisassembly = false;
                bool IsPrep = false;
                bool IsRouting = false;
                bool IsProgramming = false;
                bool IsFacebook = false;
                bool IsDocument = false;

                string[] lines = importFileString.Split(newlinesep, StringSplitOptions.None);
                foreach (string line in lines)
                {
                    if (line.Trim() != "")
                    {
                        string[] fields = line.Split(tabsep);
                        if (fields.Length > 0)
                        {
                            string field1 = fields[0].Trim().ToUpper();

                            if (fields[0].Trim().ToUpper() == "VEHICLE ID" && fields.Length > 1)
                            {
                                ImportVehicleMakeModelYearId = fields[1].Trim();
                            }
                            else if (fields[0].Trim().ToUpper() == "LINK VEHICLE ID" && fields.Length > 1)
                            {
                                LinkVehicleMakeModelYearId = fields[1].Trim();
                                LinkVehicleDatabase(ImportVehicleMakeModelYearId, LinkVehicleMakeModelYearId);
                            }
                            else if (field1 == "[URGENT MESSAGE]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsUrgentMessage = true;
                            }
                            else if (field1 == "[WIRING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsWiring = true;
                            }
                            else if (field1 == "[WIRING NOTE]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsWiringNote = true;
                            }
                            else if (field1 == "[DISASSEMBLY]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsDisassembly = true;
                            }
                            else if (field1 == "[PREP]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsPrep = true;
                            }
                            else if (field1 == "[ROUTING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsRouting = true;
                            }
                            else if (field1 == "[PROGRAMMING]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsProgramming = true;
                            }
                            else if (field1 == "[FACEBOOK]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsFacebook = true;
                            }
                            else if (field1 == "[DOCUMENT]")
                            {
                                IsUrgentMessage = false; IsWiring = false; IsWiringNote = false; IsDisassembly = false; IsPrep = false;
                                IsRouting = false; IsProgramming = false; IsFacebook = false; IsDocument = false;

                                IsDocument = true;
                            }
                            else if (IsUrgentMessage && field1 == "MESSAGE:" && fields.Length > 1)
                            {
                                string msg = fields[1].Trim();
                                if (ImportVehicleMakeModelYearId != "" && msg != "")
                                {
                                    UpdateUrgentMessage(ImportVehicleMakeModelYearId, msg.Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                }
                            }
                            else if (IsWiring)
                            {
                                if (field1 != "ID" && fields.Length > 12)
                                {
                                    string FieldWireFunctionName = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldInstalltionTypeName = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldColor = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldVehicleColor = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldLocation = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldPinOut = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldPolarity = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[8].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[9].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[10].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[11].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[12].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    string FieldWireFunctionId = FindWireFunctionId(FieldWireFunctionName);
                                    string FieldInstalltionTypeId = FindInstalltionTypeId(FieldInstalltionTypeName);

                                    int VehicleWireFunctionId = 0;
                                    if (int.TryParse(field1, out VehicleWireFunctionId))
                                    {
                                        if (FieldWireFunctionId != "" && FieldInstalltionTypeId != "")
                                        {
                                            UpdateVehicleWire("", VehicleWireFunctionId.ToString(), ImportVehicleMakeModelYearId, FieldWireFunctionId, FieldInstalltionTypeId,
                                                FieldVehicleColor, FieldColor,
                                                FieldPinOut, FieldLocation, FieldPolarity, FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                        }
                                    }
                                    else
                                    {
                                        if (ImportVehicleMakeModelYearId != "" && FieldWireFunctionId != "" && FieldInstalltionTypeId != "")
                                        {
                                            AddVehicleWire("", ImportVehicleMakeModelYearId, FieldWireFunctionId, FieldInstalltionTypeId, FieldVehicleColor, FieldColor,
                                                FieldPinOut, FieldLocation, FieldPolarity, FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                        }
                                    }
                                }
                            }
                            else if (IsWiringNote)
                            {
                                if (field1 != "ID" && fields.Length > 2)
                                {
                                    string FieldInstallationTypeId = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldNote = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleWireNoteId = 0;
                                    if (int.TryParse(field1, out VehicleWireNoteId))
                                    {
                                        UpdateWireNote(ImportVehicleMakeModelYearId, VehicleWireNoteId.ToString(), FieldInstallationTypeId, FieldNote);
                                    }
                                    else
                                    {
                                        UpdateWireNote(ImportVehicleMakeModelYearId, "", FieldInstallationTypeId, FieldNote);
                                    }
                                }
                            }
                            else if (IsDisassembly)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWireDisassemblyId = 0;
                                    if (int.TryParse(field1, out VehicleWireDisassemblyId))
                                    {
                                        UpdateDisassembly("", VehicleWireDisassemblyId.ToString(), ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddDisassembly("", ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsPrep)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWirePrepId = 0;
                                    if (int.TryParse(field1, out VehicleWirePrepId))
                                    {
                                        UpdatePrep("", VehicleWirePrepId.ToString(), ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddPrep("", ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsRouting)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleWirePlacementRoutingId = 0;
                                    if (int.TryParse(field1, out VehicleWirePlacementRoutingId))
                                    {
                                        UpdateRouting("", VehicleWirePlacementRoutingId.ToString(), ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddRouting("", ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsProgramming)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldStep = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldDesc = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));


                                    int VehicleWireProgrammingId = 0;
                                    if (int.TryParse(field1, out VehicleWireProgrammingId))
                                    {
                                        UpdateProgramming("", VehicleWireProgrammingId.ToString(), ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddProgramming("", ImportVehicleMakeModelYearId, FieldStep, FieldDesc,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsFacebook)
                            {
                                if (field1 != "ID" && fields.Length > 7)
                                {
                                    string FieldtNote = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldURL = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach1 = RemoveQuote(fields[3].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach2 = RemoveQuote(fields[4].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach3 = RemoveQuote(fields[5].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach4 = RemoveQuote(fields[6].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldAttach5 = RemoveQuote(fields[7].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleWireFacebookId = 0;
                                    if (int.TryParse(field1, out VehicleWireFacebookId))
                                    {
                                        UpdateFacebook("", VehicleWireFacebookId.ToString(), ImportVehicleMakeModelYearId, FieldtNote, FieldURL,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                    else
                                    {
                                        AddFacebook("", ImportVehicleMakeModelYearId, FieldtNote, FieldURL,
                                            FieldAttach1, FieldAttach2, FieldAttach3, FieldAttach4, FieldAttach5);
                                    }
                                }
                            }
                            else if (IsDocument)
                            {
                                if (field1 != "ID" && fields.Length > 2)
                                {
                                    string FieldName = RemoveQuote(fields[1].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));
                                    string FieldFile = RemoveQuote(fields[2].Trim().Replace("[NewLine]", Environment.NewLine).Replace("[Tab]", "\t"));

                                    int VehicleDocumentId = 0;
                                    if (int.TryParse(field1, out VehicleDocumentId))
                                    {
                                        UpdateDocument("", VehicleDocumentId.ToString(), ImportVehicleMakeModelYearId, FieldName, FieldFile);
                                    }
                                    else
                                    {
                                        AddDocument("", ImportVehicleMakeModelYearId, FieldName, FieldFile);
                                    }
                                }
                            }
                        }
                    }
                }

                ImportPanel.Visible = false;
                ShowInfo("Imported file successfully");
                SearchInfo(0);
            }
            else
            {
                ShowError("Please specify file to import");
                return;
            }
        }

        private string RemoveQuote(string msg)
        {
            if (msg == null) return "";

            string msg2 = msg.Trim();
            if (msg2.StartsWith("\""))
            {
                msg2 = msg2.Substring(1);
            }
            if (msg2.EndsWith("\""))
            {
                msg2 = msg2.Substring(0, msg2.Length - 1);
            }
            return msg2;
        }

        protected void CancelImportFileButton_Click(object sender, EventArgs e)
        {
            ImportPanel.Visible = false;
        }

        private string FindWireFunctionId(string FieldWireFunctionName)
        {
            string WireFunctionId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select WireFunctionId from dbo.WireFunction with (nolock) where WireFunctionName=@WireFunctionName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@WireFunctionName", SqlDbType.NVarChar, 100).Value = FieldWireFunctionName;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionId = reader["WireFunctionId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return WireFunctionId;
        }

        private string FindInstalltionTypeId(string FieldInstalltionTypeName)
        {
            string InstallationTypeId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select InstallationTypeId from dbo.InstallationType with (nolock) where InstallationTypeName=@InstallationTypeName ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@InstallationTypeName", SqlDbType.NVarChar, 100).Value = FieldInstalltionTypeName;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    InstallationTypeId = reader["InstallationTypeId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return InstallationTypeId;
        }

        private string FindVehicleWireNoteId(string VehicleMakeModelYearId)
        {
            string VehicleWireNoteId = "";
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select a.VehicleWireNoteId, a.Note ";
                sql += "from dbo.VehicleWireNote a ";
                sql += "where a.VehicleMakeModelYearId = @VehicleMakeModelYearId";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleWireNoteId = reader["VehicleWireNoteId"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

            return VehicleWireNoteId;
        }

        protected void UnlinkVehicleButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();


                string sql = "delete dbo.VehicleLink ";
                sql += "where VehicleMakeModelYearId = @VehicleMakeModelYearId and LinkVehicleMakeModelYearId = @LinkVehicleMakeModelYearId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@LinkVehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(LinkVehicleMakeModelYearIdHidden.Value);
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                cmd.ExecuteNonQuery();

                LinkVehicle.Text = "";
                LinkVehicleMakeModelYearIdHidden.Value = "";
                LinkVehicleMakeIdHidden.Value = "";
                LinkVehicleModelIdHidden.Value = "";
                LinkVehicleYearHidden.Value = "";

                ShowInfo("Un-linked vehicle successfully");

                NewVehiclePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());

            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ModelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddVehicleTitle.Text == "Link Vehicle")
                ShowYearList();
        }

        private void ShowYearList()
        {
            ClearError();
            if (ModelList.SelectedIndex < 0 || ModelList.SelectedValue == "-1")
            {
                YearList.Items.Clear();
                return;
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select distinct a.VehicleYear ";
                sql += "from dbo.VehicleMakeModelYear a WITH (NOLOCK) ";
                sql += "join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId ";
                sql += "where a.VehicleMakeId = " + MakeList.SelectedValue;
                sql += " and a.VehicleModelId = " + ModelList.SelectedValue;
                if (VehicleMakeModelYearIdHidden.Value != "")
                    sql += " and a.VehicleMakeModelYearId <> " + VehicleMakeModelYearIdHidden.Value;
                sql += " order by VehicleYear desc";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                YearList.DataTextField = "VehicleYear";
                YearList.DataValueField = "VehicleYear";
                YearList.DataSource = ds;
                YearList.DataBind();

                YearList.Items.Insert(0, new ListItem("Year", "-1"));
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowWireList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetWireQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            WireList.DataSource = ds;
            WireList.DataBind();

            if (WireList != null && WireList.HeaderRow != null && WireList.HeaderRow.Cells.Count > 0)
            {
                WireList.Attributes["data-page"] = "false";

                WireList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                WireList.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                WireList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                WireList.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                WireList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                WireList.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                WireList.HeaderRow.TableSection = TableRowSection.TableHeader;

                //WiringTitle.Visible = true;
                WiringHeader.Text = "Vehicle Wiring (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                WiringHeader.Text = "Vehicle Wiring (0)";
                //ShowError("No wiring data available");
                //WiringTitle.Visible = false;
            }
        }

        private void ShowWireListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetWireQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            WireListEditor.DataSource = ds;
            WireListEditor.DataBind();

            if (WireListEditor != null && WireListEditor.HeaderRow != null && WireListEditor.HeaderRow.Cells.Count > 0)
            {
                WireListEditor.Attributes["data-page"] = "false";

                WireListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                WireListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                WireListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[10].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[11].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[12].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[13].Attributes["data-hide"] = "phone";
                WireListEditor.HeaderRow.Cells[14].Attributes["data-hide"] = "phone";

                WireListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[10].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[11].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[12].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[13].Attributes["data-sort-ignore"] = "true";
                WireListEditor.HeaderRow.Cells[14].Attributes["data-sort-ignore"] = "true";

                WireListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    WireListEditor.HeaderRow.Cells[14].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    WireListEditor.Columns[12].Visible = false;
                    WireListEditor.Columns[13].Visible = false;
                    WireListEditor.HeaderRow.Cells[14].Text = "Delete";
                }
            }
        }

        protected void SearchInstallationTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string SaveFolder = SaveFolderHidden.Value;
                ShowWireList(Con, SaveFolder);

                if (WireListBatchPanel.Visible)
                {
                    ShowWireBatch();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void AddProgrammingPanelButton_Click(object sender, EventArgs e)
        {
            AddProgrammingTitle.Text = "Add Programming";
            AddProgrammingButton.Text = "Add";
            VehicleWireProgrammingIdHidden.Value = "";
            AddProgrammingPanel.Visible = true;

            ProgrammingStep.Text = "";
            Programmingeditor.Text = "";

            AttachProgrammingImageDelete1.Checked = false;
            AttachProgrammingImageDelete2.Checked = false;
            AttachProgrammingImageDelete3.Checked = false;
            AttachProgrammingImageDelete4.Checked = false;
            AttachProgrammingImageDelete5.Checked = false;

            ExistingProgrammingFilename1.Value = "";
            AttachProgrammingImage1.Visible = false;
            AttachProgrammingImageDelete1.Visible = false;
            ReplaceProgrammingImageLabel1.Visible = false;
            AttachProgrammingFileUrl1.Text = "";

            ExistingProgrammingFilename2.Value = "";
            AttachProgrammingImage2.Visible = false;
            AttachProgrammingImageDelete2.Visible = false;
            ReplaceProgrammingImageLabel2.Visible = false;
            AttachProgrammingFileUrl2.Text = "";

            ExistingProgrammingFilename3.Value = "";
            AttachProgrammingImage3.Visible = false;
            AttachProgrammingImageDelete3.Visible = false;
            ReplaceProgrammingImageLabel3.Visible = false;
            AttachProgrammingFileUrl3.Text = "";

            ExistingProgrammingFilename4.Value = "";
            AttachProgrammingImage4.Visible = false;
            AttachProgrammingImageDelete4.Visible = false;
            ReplaceProgrammingImageLabel4.Visible = false;
            AttachProgrammingFileUrl4.Text = "";

            ExistingProgrammingFilename5.Value = "";
            AttachProgrammingImage5.Visible = false;
            AttachProgrammingImageDelete5.Visible = false;
            ReplaceProgrammingImageLabel5.Visible = false;
            AttachProgrammingFileUrl5.Text = "";

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");
        }

        protected void CancelProgrammingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            AddProgrammingPanel.Visible = false;

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");
        }

        protected void ProgrammingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HideNoImageLink(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[4].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Programming?');";
                    }
                }
            }
        }

        protected void AddProgrammingButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (ProgrammingStep.Text == "")
            {
                ShowError("Please enter Step");
                ProgrammingStep.Focus();
                return;
            }
            if (Programmingeditor.Text == "")
            {
                ShowError("Please enter Note");
                Programmingeditor.Focus();
                return;
            }

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            string FileName1;
            SaveFile(AttachProgrammingFile1.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName1);
            if (FileName1 == "" && AttachProgrammingFileUrl1.Text.Trim() != "")
            {
                FileName1 = AttachProgrammingFileUrl1.Text.Trim();
            }
            string FileName2;
            SaveFile(AttachProgrammingFile2.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName2);
            if (FileName2 == "" && AttachProgrammingFileUrl2.Text.Trim() != "")
            {
                FileName2 = AttachProgrammingFileUrl2.Text.Trim();
            }
            string FileName3;
            SaveFile(AttachProgrammingFile3.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName3);
            if (FileName3 == "" && AttachProgrammingFileUrl3.Text.Trim() != "")
            {
                FileName3 = AttachProgrammingFileUrl3.Text.Trim();
            }
            string FileName4;
            SaveFile(AttachProgrammingFile4.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName4);
            if (FileName4 == "" && AttachProgrammingFileUrl4.Text.Trim() != "")
            {
                FileName4 = AttachProgrammingFileUrl4.Text.Trim();
            }
            string FileName5;
            SaveFile(AttachProgrammingFile5.PostedFile, FullPath, "Programming", Make, Model, Year, out FileName5);
            if (FileName5 == "" && AttachProgrammingFileUrl5.Text.Trim() != "")
            {
                FileName5 = AttachProgrammingFileUrl5.Text.Trim();
            }

            if (AddProgrammingButton.Text == "Add" || VehicleWireProgrammingEditorWorkActionHidden.Value == "0")
            {
                if (VehicleWireProgrammingEditorWorkActionHidden.Value == "0") // Edit & Approve
                {
                    AddProgramming(VehicleWireProgrammingEditorWorkIdHidden.Value, VehicleMakeModelYearIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
                else if (AddProgrammingButton.Text == "Add")
                {
                    AddProgramming("", VehicleMakeModelYearIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }
            else
            {
                if ((AttachProgrammingImageDelete1.Checked && ExistingProgrammingFilename1.Value != "") || FileName1 != "")
                {
                    if (File.Exists(FullPath + ExistingProgrammingFilename1.Value))
                    {
                        File.Delete(FullPath + ExistingProgrammingFilename1.Value);
                    }
                }
                if ((AttachProgrammingImageDelete2.Checked && ExistingProgrammingFilename2.Value != "") || FileName2 != "")
                {
                    if (File.Exists(FullPath + ExistingProgrammingFilename2.Value))
                    {
                        File.Delete(FullPath + ExistingProgrammingFilename2.Value);
                    }
                }
                if ((AttachProgrammingImageDelete3.Checked && ExistingProgrammingFilename3.Value != "") || FileName3 != "")
                {
                    if (File.Exists(FullPath + ExistingProgrammingFilename3.Value))
                    {
                        File.Delete(FullPath + ExistingProgrammingFilename3.Value);
                    }
                }
                if ((AttachProgrammingImageDelete4.Checked && ExistingProgrammingFilename4.Value != "") || FileName4 != "")
                {
                    if (File.Exists(FullPath + ExistingProgrammingFilename4.Value))
                    {
                        File.Delete(FullPath + ExistingProgrammingFilename4.Value);
                    }
                }
                if ((AttachProgrammingImageDelete5.Checked && ExistingProgrammingFilename5.Value != "") || FileName5 != "")
                {
                    if (File.Exists(FullPath + ExistingProgrammingFilename5.Value))
                    {
                        File.Delete(FullPath + ExistingProgrammingFilename5.Value);
                    }
                }

                string Attach1 = "";
                if (AttachProgrammingImageDelete1.Visible && AttachProgrammingImageDelete1.Checked)
                {
                    Attach1 = "";
                }
                else
                {
                    if (FileName1 == "")
                    {
                        FileName1 = ExistingProgrammingFilename1.Value;
                    }
                    Attach1 = FileName1;
                }

                string Attach2 = "";
                if (AttachProgrammingImageDelete2.Visible && AttachProgrammingImageDelete2.Checked)
                {
                    Attach2 = "";
                }
                else
                {
                    if (FileName2 == "")
                    {
                        FileName2 = ExistingProgrammingFilename2.Value;
                    }
                    Attach2 = FileName2;
                }

                string Attach3 = "";
                if (AttachProgrammingImageDelete3.Visible && AttachProgrammingImageDelete3.Checked)
                {
                    Attach3 = "";
                }
                else
                {
                    if (FileName3 == "")
                    {
                        FileName3 = ExistingProgrammingFilename3.Value;
                    }
                    Attach3 = FileName3;
                }

                string Attach4 = "";
                if (AttachProgrammingImageDelete4.Visible && AttachProgrammingImageDelete4.Checked)
                {
                    Attach4 = "";
                }
                else
                {
                    if (FileName4 == "")
                    {
                        FileName4 = ExistingProgrammingFilename4.Value;
                    }
                    Attach4 = FileName4;
                }

                string Attach5 = "";
                if (AttachProgrammingImageDelete5.Visible && AttachProgrammingImageDelete5.Checked)
                {
                    Attach5 = "";
                }
                else
                {
                    if (FileName5 == "")
                    {
                        FileName5 = ExistingProgrammingFilename5.Value;
                    }
                    Attach5 = FileName5;
                }

                if (VehicleWireProgrammingEditorWorkActionHidden.Value == "1") // Edit & Approve
                {
                    UpdateProgramming(VehicleWireProgrammingEditorWorkIdHidden.Value, VehicleWireProgrammingIdHidden.Value, VehicleMakeModelYearIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }
                else
                {
                    UpdateProgramming("", VehicleWireProgrammingIdHidden.Value, VehicleMakeModelYearIdHidden.Value, ProgrammingStep.Text, Programmingeditor.Text,
                        Attach1, Attach2, Attach3, Attach4, Attach5);
                }

            }

            SearchInfo(7);
            AddProgrammingPanel.Visible = false;

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");
        }

        private void AddProgramming(string ApprovalEditorWorkId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"insert into dbo.VehicleWireProgramming (VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                        UpdatedDt, UpdatedBy, EditorWorkId) 
                        select @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5, 
                        getdate(), @UpdatedBy, @EditorWorkId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                    if (ApprovalEditorWorkId != "")
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();

                    if (ApprovalEditorWorkId != "")
                    {
                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 0; // Add
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireProgrammingEditor (
                        EditorWorkId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void UpdateProgramming(string ApprovalEditorWorkId, string VehicleWireProgrammingId, string VehicleMakeModelYearId, string Step, string Note, string Attach1, string Attach2, string Attach3, string Attach4, string Attach5)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    if (ApprovalEditorWorkId != "")
                    {
                        sql = @"select VehicleWireProgrammingId
                            from dbo.VehicleWireProgrammingEditor 
                            where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            VehicleWireProgrammingId = reader["VehicleWireProgrammingId"].ToString();
                        }
                        reader.Close();

                        sql = @"update dbo.VehicleWireProgramming 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);
                        cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingId);

                        cmd.ExecuteNonQuery();


                        sql = "update dbo.EditorWork set StatusId=2, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(ApprovalEditorWorkId);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = @"update dbo.VehicleWireProgramming 
                            set Step=@Step, Note=@Note, Attach1=@Attach1, Attach2=@Attach2, Attach3=@Attach3, Attach4=@Attach4, Attach5=@Attach5, 
                            UpdatedDt = getdate(), UpdatedBy = @UpdatedBy, EditorWorkId=@EditorWorkId
                            where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                        cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                        cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                        cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                        cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;
                        cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingId);

                        cmd.ExecuteNonQuery();
                    }
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireProgrammingEditor (
                        EditorWorkId, VehicleWireProgrammingId, VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5) 
                        select @EditorWorkId, @VehicleWireProgrammingId, @VehicleMakeModelYearId, @Step, @Note, @Attach1, @Attach2, @Attach3, @Attach4, @Attach5";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingId);
                    cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                    cmd.Parameters.Add("@Step", SqlDbType.Int).Value = int.Parse(Step);
                    cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                    cmd.Parameters.Add("@Attach1", SqlDbType.NVarChar, 2000).Value = Attach1;
                    cmd.Parameters.Add("@Attach2", SqlDbType.NVarChar, 2000).Value = Attach2;
                    cmd.Parameters.Add("@Attach3", SqlDbType.NVarChar, 2000).Value = Attach3;
                    cmd.Parameters.Add("@Attach4", SqlDbType.NVarChar, 2000).Value = Attach4;
                    cmd.Parameters.Add("@Attach5", SqlDbType.NVarChar, 2000).Value = Attach5;

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowProgrammingList(SqlConnection Con, string SaveFolder)
        {
            string sql = GetProgrammingQuery(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            ProgrammingList.DataSource = ds;
            ProgrammingList.DataBind();

            if (ProgrammingList != null && ProgrammingList.HeaderRow != null && ProgrammingList.HeaderRow.Cells.Count > 0)
            {
                ProgrammingList.Attributes["data-page"] = "false";

                ProgrammingList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                ProgrammingList.HeaderRow.Cells[1].Attributes["data-hide"] = "phone";
                ProgrammingList.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";

                ProgrammingList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                ProgrammingList.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                ProgrammingList.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";

                ProgrammingList.HeaderRow.TableSection = TableRowSection.TableHeader;
                //PlacementTitle.Visible = true;

                ProgrammingHeader.Text = "Programming (" + ds.Tables[0].Rows.Count + ")";
            }
            else
            {
                ProgrammingHeader.Text = "Programming (0)";
                //PlacementTitle.Visible = false;
            }
        }


        private void ShowProgrammingListEditor(SqlConnection Con, string SaveFolder)
        {
            string sql = GetProgrammingQueryEditor(SaveFolder);

            SqlCommand Cmd = new SqlCommand(sql, Con);
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            ProgrammingListEditor.DataSource = ds;
            ProgrammingListEditor.DataBind();

            if (ProgrammingListEditor != null && ProgrammingListEditor.HeaderRow != null && ProgrammingListEditor.HeaderRow.Cells.Count > 0)
            {
                ProgrammingListEditor.Attributes["data-page"] = "false";

                ProgrammingListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
                ProgrammingListEditor.HeaderRow.Cells[1].Attributes["data-class"] = "expand";
                ProgrammingListEditor.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[4].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[5].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[6].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[7].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[8].Attributes["data-hide"] = "phone";
                ProgrammingListEditor.HeaderRow.Cells[9].Attributes["data-hide"] = "phone";

                ProgrammingListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[1].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[2].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[3].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[4].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[5].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[6].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[7].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[8].Attributes["data-sort-ignore"] = "true";
                ProgrammingListEditor.HeaderRow.Cells[9].Attributes["data-sort-ignore"] = "true";

                ProgrammingListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    ProgrammingListEditor.HeaderRow.Cells[9].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    ProgrammingListEditor.Columns[7].Visible = false;
                    ProgrammingListEditor.Columns[8].Visible = false;
                    ProgrammingListEditor.HeaderRow.Cells[9].Text = "Delete";
                }
            }
        }

        protected void ProgrammingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireProgrammingIdHidden.Value = ((HiddenField)(ProgrammingList.SelectedRow.FindControl("VehicleWireProgrammingId"))).Value;
            VehicleWireProgrammingEditorWorkIdHidden.Value = "";
            VehicleWireProgrammingEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = "SELECT VehicleWireProgrammingId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5  ";
                sql += "from dbo.VehicleWireProgramming  ";
                sql += "where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ProgrammingStep.Text = reader["Step"].ToString();
                    Programmingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage1.ImageUrl = attach;
                            AttachProgrammingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage1.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl1.Text = "";
                        }
                        ExistingProgrammingFilename1.Value = attach;
                        AttachProgrammingImage1.Visible = true;

                        AttachProgrammingImageDelete1.Visible = true;
                        ReplaceProgrammingImageLabel1.Visible = true;

                        AttachProgrammingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename1.Value = "";
                        AttachProgrammingFileUrl1.Text = "";
                        AttachProgrammingImage1.Visible = false;
                        AttachProgrammingImageDelete1.Visible = false;
                        ReplaceProgrammingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage2.ImageUrl = attach;
                            AttachProgrammingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage2.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl2.Text = "";
                        }
                        ExistingProgrammingFilename2.Value = attach;
                        AttachProgrammingImage2.Visible = true;

                        AttachProgrammingImageDelete2.Visible = true;
                        ReplaceProgrammingImageLabel2.Visible = true;

                        AttachProgrammingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename2.Value = "";
                        AttachProgrammingFileUrl2.Text = "";
                        AttachProgrammingImage2.Visible = false;
                        AttachProgrammingImageDelete2.Visible = false;
                        ReplaceProgrammingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage3.ImageUrl = attach;
                            AttachProgrammingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage3.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl3.Text = "";
                        }
                        ExistingProgrammingFilename3.Value = attach;
                        AttachProgrammingImage3.Visible = true;

                        AttachProgrammingImageDelete3.Visible = true;
                        ReplaceProgrammingImageLabel3.Visible = true;

                        AttachProgrammingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename3.Value = "";
                        AttachProgrammingFileUrl3.Text = "";
                        AttachProgrammingImage3.Visible = false;
                        AttachProgrammingImageDelete3.Visible = false;
                        ReplaceProgrammingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage4.ImageUrl = attach;
                            AttachProgrammingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage4.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl4.Text = "";
                        }
                        ExistingProgrammingFilename4.Value = attach;
                        AttachProgrammingImage4.Visible = true;

                        AttachProgrammingImageDelete4.Visible = true;
                        ReplaceProgrammingImageLabel4.Visible = true;

                        AttachProgrammingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename4.Value = "";
                        AttachProgrammingFileUrl4.Text = "";
                        AttachProgrammingImage4.Visible = false;
                        AttachProgrammingImageDelete4.Visible = false;
                        ReplaceProgrammingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage5.ImageUrl = attach;
                            AttachProgrammingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage5.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl5.Text = "";
                        }
                        ExistingProgrammingFilename5.Value = attach;
                        AttachProgrammingImage5.Visible = true;

                        AttachProgrammingImageDelete5.Visible = true;
                        ReplaceProgrammingImageLabel5.Visible = true;

                        AttachProgrammingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename5.Value = "";
                        AttachProgrammingFileUrl5.Text = "";
                        AttachProgrammingImage5.Visible = false;
                        AttachProgrammingImageDelete5.Visible = false;
                        ReplaceProgrammingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddProgrammingTitle.Text = "Edit Programming";
                AddProgrammingButton.Text = "Save";
                AddProgrammingPanel.Visible = true;

                ResetTab();
                linkprogrammingtab.Attributes.Add("Class", "tab-title active");
                linkprogramming.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProgrammingList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ClearError();

            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

            string VehicleWireProgrammingIdValue = ((HiddenField)(ProgrammingList.Rows[e.RowIndex].FindControl("VehicleWireProgrammingId"))).Value;

            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql = "";
                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    sql = @"select a.VehicleWireProgrammingId, a.Step, a.Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                        from dbo.VehicleWireProgramming a 
                        where VehicleWireProgrammingId=@VehicleWireProgrammingId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdValue);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach1"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach1"].ToString());
                            }
                        }
                        if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach2"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach2"].ToString());
                            }
                        }
                        if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach3"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach3"].ToString());
                            }
                        }
                        if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach4"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach4"].ToString());
                            }
                        }
                        if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                        {
                            if (File.Exists(FullPath + reader["Attach5"].ToString()))
                            {
                                File.Delete(FullPath + reader["Attach5"].ToString());
                            }
                        }
                    }
                    reader.Close();

                    sql = "delete from dbo.VehicleWireProgramming where VehicleWireProgrammingId=@VehicleWireProgrammingId";
                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdValue);

                    cmd.ExecuteNonQuery();
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    sql = @"select VehicleWireProgrammingId, VehicleMakeModelYearId 
                        from dbo.VehicleWireProgramming with (NOLOCK) 
                        where VehicleWireProgrammingId=@VehicleWireProgrammingId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdValue);

                    string VehicleMakeModelYearId = "";
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        VehicleMakeModelYearId = reader["VehicleMakeModelYearId"].ToString();
                    }
                    else
                    {
                        reader.Close();
                        tran.Commit();
                        return;
                    }
                    reader.Close();

                    sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                    cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 2; // Delete
                    cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                    sql = @"insert into dbo.VehicleWireProgrammingEditor(
                            EditorWorkId, VehicleWireProgrammingId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5)
                        select @EditorWorkId, VehicleWireProgrammingId, VehicleMakeModelYearId,
                            Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5
                        from dbo.VehicleWireProgramming where VehicleWireProgrammingId = @VehicleWireProgrammingId";

                    cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = tran;

                    cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                    cmd.Parameters.Add("@VehicleWireProgrammingId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingIdValue);

                    cmd.ExecuteNonQuery();
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                AddRoutingPanel.Visible = false;
                SearchInfo(7);

                ResetTab();
                linkprogrammingtab.Attributes.Add("Class", "tab-title active");
                linkprogramming.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void ShowNoteList(SqlConnection Con)
        {
            string sql = GetWireNoteQuery();

            SqlCommand cmd = new SqlCommand(sql, Con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            NoteList.DataSource = ds;
            NoteList.DataBind();

            if (NoteList != null && NoteList.HeaderRow != null && NoteList.HeaderRow.Cells.Count > 0)
            {
                NoteList.Attributes["data-page"] = "false";

                NoteList.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                NoteList.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";

                NoteList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void ShowNoteListEditor(SqlConnection Con)
        {
            string sql = GetWireNoteQueryEditor();

            SqlCommand cmd = new SqlCommand(sql, Con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            adp.Fill(ds, "List");

            NoteListEditor.DataSource = ds;
            NoteListEditor.DataBind();

            if (NoteListEditor != null && NoteListEditor.HeaderRow != null && NoteListEditor.HeaderRow.Cells.Count > 0)
            {
                NoteListEditor.Attributes["data-page"] = "false";

                NoteListEditor.HeaderRow.Cells[0].Attributes["data-class"] = "expand";

                NoteListEditor.HeaderRow.Cells[0].Attributes["data-sort-ignore"] = "true";

                NoteListEditor.HeaderRow.TableSection = TableRowSection.TableHeader;

                if (RoleNameHidden.Value.Contains("Administrator"))
                {
                    NoteListEditor.HeaderRow.Cells[8].Text = "Deny";
                }
                else if (RoleNameHidden.Value.Contains("Editor"))
                {
                    NoteListEditor.Columns[6].Visible = false;
                    NoteListEditor.Columns[7].Visible = false;
                    NoteListEditor.HeaderRow.Cells[8].Text = "Delete";
                }
            }
        }

        protected void NoteList_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehicleWireNoteIdHidden.Value = ((HiddenField)(NoteList.SelectedRow.FindControl("VehicleWireNoteId"))).Value;
            VehicleWireNoteEditorWorkIdHidden.Value = "";
            VehicleWireNoteEditorWorkActionHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                if (VehicleWireNoteIdHidden.Value != "")
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);
                    conn.Open();

                    string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                    string sql = "SELECT Note  ";
                    sql += "from dbo.VehicleWireNote a ";
                    sql += "where VehicleWireNoteId = @VehicleWireNoteId";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteIdHidden.Value);

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        editor.Text = reader["Note"].ToString();
                    }
                    else
                    {
                        editor.Text = "";
                    }
                    reader.Close();
                }
                else
                {
                    editor.Text = "";
                }

                NoteInstallationTypeIdHidden.Value = ((HiddenField)(NoteList.SelectedRow.FindControl("InstallationTypeId"))).Value;
                InstallationTypeEditLabl.Text = ((Label)(NoteList.SelectedRow.FindControl("InstallationTypeNameLabel"))).Text;
                EditNotePanel.Visible = true;
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void CancelNoteButton_Click(object sender, EventArgs e)
        {
            EditNotePanel.Visible = false;
        }

        protected void SaveNoteButton_Click(object sender, EventArgs e)
        {
            UpdateWireNote(VehicleMakeModelYearIdHidden.Value, VehicleWireNoteIdHidden.Value, NoteInstallationTypeIdHidden.Value, editor.Text);

            EditNotePanel.Visible = false;
        }

        private void UpdateWireNote(string VehicleMakeModelYearId, string VehicleWireNoteId, string InstallationTypeId, string Note)
        {
            SqlConnection Con = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);

                Con.Open();
                tran = Con.BeginTransaction();

                string sql = "";

                if (VehicleWireNoteId != "")
                {
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (VehicleWireNoteEditorWorkActionHidden.Value == "1") // Edit & Approve
                        {
                            ApproveEditedNote(VehicleWireNoteEditorWorkIdHidden.Value, Note);
                        }
                        else
                        {
                            sql = "update dbo.VehicleWireNote set Note = @Note, UpdatedDt = getdate(), UpdatedBy = @UpdatedBy where VehicleWireNoteId=@VehicleWireNoteId ";

                            SqlCommand Cmd = new SqlCommand(sql, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Transaction = tran;

                            Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                            Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                            Cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteId);

                            Cmd.ExecuteNonQuery();
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                        SqlCommand cmd = new SqlCommand(sql, Con);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                        cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                        sql = @"insert into dbo.VehicleWireNoteEditor (
                            EditorWorkId, VehicleWireNoteId, VehicleMakeModelYearId, InstallationTypeId, Note) 
                        select @EditorWorkId, @VehicleWireNoteId, @VehicleMakeModelYearId, @InstallationTypeId, @Note";

                        cmd = new SqlCommand(sql, Con);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                        cmd.Parameters.Add("@VehicleWireNoteId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteId);
                        cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                        cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (VehicleWireNoteEditorWorkActionHidden.Value == "1") // Edit & Approve
                        {
                            ApproveEditedNote(VehicleWireNoteEditorWorkIdHidden.Value, Note);
                        }
                        else
                        {
                            sql = "insert into dbo.VehicleWireNote (VehicleMakeModelYearId, InstallationTypeId, Note, UpdatedBy, UpdatedDt) ";
                            sql += "select @VehicleMakeModelYearId, @InstallationTypeId,  @Note, @UpdatedBy, getdate()";

                            SqlCommand Cmd = new SqlCommand(sql, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Transaction = tran;

                            Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                            Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);
                            Cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                            Cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                            Cmd.ExecuteNonQuery();
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"insert into dbo.EditorWork (EditorId, EditorWorkDt, EditorWorkAction, StatusId) 
                        select @EditorId, getdate(), @EditorWorkAction, @StatusId;
                        select @EditorWorkId = SCOPE_IDENTITY(); ";

                        SqlCommand cmd = new SqlCommand(sql, Con);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorId", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkAction", SqlDbType.Int).Value = 1; // Edit
                        cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = 0; // Draft
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        string EditorWorkId = cmd.Parameters["@EditorWorkId"].Value.ToString();

                        sql = @"insert into dbo.VehicleWireNoteEditor (
                            EditorWorkId, VehicleMakeModelYearId, InstallationTypeId, Note) 
                        select @EditorWorkId, @VehicleMakeModelYearId, @InstallationTypeId, @Note";

                        cmd = new SqlCommand(sql, Con);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);
                        cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);
                        cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(InstallationTypeId);
                        cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;

                        cmd.ExecuteNonQuery();
                    }
                }

                if (sql != "")
                {
                    tran.Commit();
                }

                SearchInfo(1);
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
                if (Con.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }


        private void ApproveEditedNote(string EditorWorkId, string Note)
        {
            SqlConnection conn = null;
            SqlTransaction tran = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);

                conn.Open();
                tran = conn.BeginTransaction();

                string sql2 = @"
                        if exists (select * from dbo.VehicleWireNoteEditor where EditorWorkId=@EditorWorkId and VehicleWireNoteId is not null)
                                begin
                                    update a
                                    set a.Note=@Note, 
                                        a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                    from dbo.VehicleWireNote a
                                    join dbo.VehicleWireNoteEditor b on a.VehicleWireNoteId = b.VehicleWireNoteId
                                    join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                    where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1;
                                end
                                else if exists (select * from dbo.VehicleWireNoteEditor where EditorWorkId=@EditorWorkId )
                                begin
                                    insert into dbo.VehicleWireNote(VehicleMakeModelYearId, InstallationTypeId, Note, UpdatedBy, UpdatedDt, EditorWorkId) 
                                    select b.VehicleMakeModelYearId, b.InstallationTypeId, @Note, c.EditorId, c.EditorWorkDt,  b.EditorWorkId
                                    from dbo.VehicleWireNoteEditor b 
                                    join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                    where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1;
                                end
                ";

                SqlCommand cmd2 = new SqlCommand(sql2, conn);
                cmd2.CommandType = CommandType.Text;
                cmd2.Transaction = tran;

                cmd2.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Note;
                cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                cmd2.ExecuteNonQuery();

                string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;

                cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                cmd.ExecuteNonQuery();

                tran.Commit();


            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
                if (conn.State == ConnectionState.Open && tran != null)
                    tran.Rollback();
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ApproveButton_Click(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "update dbo.VehicleMakeModelYear set Approved = @Approved ";
                sql += "where VehicleMakeModelYearId=@VehicleMakeModelYearId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                if (ApproveButton.Text == "Approve")
                    cmd.Parameters.Add("@Approved", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@Approved", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                SearchInfo(0);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void CancelReserveButton_Click(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "update dbo.VehicleMakeModelYear set ReservedAt = null, ReservedBy=null ";
                sql += "where VehicleMakeModelYearId=@VehicleMakeModelYearId";

                SqlCommand cmd = new SqlCommand(sql, Con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                cmd.ExecuteNonQuery();

                SearchInfo(0);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void EditApproveWire(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWireEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWireEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWireFunctionIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWireFunctionEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWireEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    WireFunctionList.SelectedValue = reader["WireFunctionId"].ToString();
                    InstalltionTypeList.SelectedValue = reader["InstallationTypeId"].ToString();
                    VehicleColor.Text = reader["VehicleColor"].ToString();
                    Color.Text = reader["Colour"].ToString();
                    PinOut.Text = reader["PinOut"].ToString();
                    Location.Text = reader["Location"].ToString();
                    Polarity.Text = reader["Polarity"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage1.ImageUrl = attach;
                            AttachWiringFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage1.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl1.Text = "";
                        }
                        ExistingWiringFilename1.Value = attach;
                        AttachWiringImage1.Visible = true;

                        AttachWiringImageDelete1.Visible = true;
                        ReplaceWiringImageLabel1.Visible = true;

                        AttachWiringImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename1.Value = "";
                        AttachWiringFileUrl1.Text = "";
                        AttachWiringImage1.Visible = false;
                        AttachWiringImageDelete1.Visible = false;
                        ReplaceWiringImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage2.ImageUrl = attach;
                            AttachWiringFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage2.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl2.Text = "";
                        }
                        ExistingWiringFilename2.Value = attach;
                        AttachWiringImage2.Visible = true;

                        AttachWiringImageDelete2.Visible = true;
                        ReplaceWiringImageLabel2.Visible = true;

                        AttachWiringImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename2.Value = "";
                        AttachWiringFileUrl2.Text = "";
                        AttachWiringImage2.Visible = false;
                        AttachWiringImageDelete2.Visible = false;
                        ReplaceWiringImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage3.ImageUrl = attach;
                            AttachWiringFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage3.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl3.Text = "";
                        }
                        ExistingWiringFilename3.Value = attach;
                        AttachWiringImage3.Visible = true;

                        AttachWiringImageDelete3.Visible = true;
                        ReplaceWiringImageLabel3.Visible = true;

                        AttachWiringImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename3.Value = "";
                        AttachWiringFileUrl3.Text = "";
                        AttachWiringImage3.Visible = false;
                        AttachWiringImageDelete3.Visible = false;
                        ReplaceWiringImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage4.ImageUrl = attach;
                            AttachWiringFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage4.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl4.Text = "";
                        }
                        ExistingWiringFilename4.Value = attach;
                        AttachWiringImage4.Visible = true;

                        AttachWiringImageDelete4.Visible = true;
                        ReplaceWiringImageLabel4.Visible = true;

                        AttachWiringImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename4.Value = "";
                        AttachWiringFileUrl4.Text = "";
                        AttachWiringImage4.Visible = false;
                        AttachWiringImageDelete4.Visible = false;
                        ReplaceWiringImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachWiringImage5.ImageUrl = attach;
                            AttachWiringFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachWiringImage5.ImageUrl = SaveFolder + attach;
                            AttachWiringFileUrl5.Text = "";
                        }
                        ExistingWiringFilename5.Value = attach;
                        AttachWiringImage5.Visible = true;

                        AttachWiringImageDelete5.Visible = true;
                        ReplaceWiringImageLabel5.Visible = true;

                        AttachWiringImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingWiringFilename5.Value = "";
                        AttachWiringFileUrl5.Text = "";
                        AttachWiringImage5.Visible = false;
                        AttachWiringImageDelete5.Visible = false;
                        ReplaceWiringImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddWiringTitle.Text = "Edit & Approve Wiring";
                AddWireButton.Text = "Approve";
                AddWirePanel.Visible = true;
                EditNotePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void WireListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[12].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[12].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[13].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[13].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Wiring?');";
                        }
                    }
                }

                if (e.Row.Cells[14].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[14].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Wiring?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Wiring?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void WireListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveWire(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, c.WireFunctionName, d.InstallationTypeName,
                                b.Colour, b.VehicleColor, b.PinOut, b.Location, b.Polarity, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWireFunctionEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            left join dbo.WireFunction c on b.WireFunctionId = c.WireFunctionId 
                            left join dbo.InstallationType d on b.InstallationTypeId = d.InstallationTypeId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Wire ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["WireFunctionName"].ToString() + " " + reader["InstallationTypeName"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Wire Function: " + reader["WireFunctionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Installtion Type: " + reader["InstallationTypeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Vehicle Color: " + reader["Colour"].ToString() + "<br/>");
                            mailContent.AppendLine("CM7X00/ADS Color: " + reader["VehicleColor"].ToString() + "<br/>");
                            mailContent.AppendLine("Pin Out: " + reader["PinOut"].ToString() + "<br/>");
                            mailContent.AppendLine("Location: " + reader["Location"].ToString() + "<br/>");
                            mailContent.AppendLine("Polarity: " + reader["Polarity"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            {
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWireFunctionEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWireFunctionEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(1);
                }
                catch (Exception ex)
                {
                    ShowWireError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWireFunction (
                                VehicleMakeModelYearId, 
                                WireFunctionId, InstallationTypeId, VehicleColor, Colour, PinOut, Location, Polarity, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, Inactive, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, 
                                a.WireFunctionId, a.InstallationTypeId, a.VehicleColor, a.Colour, a.PinOut, a.Location, a.Polarity, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, 0, a.EditorWorkId
                                from dbo.VehicleWireFunctionEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.WireFunctionId=b.WireFunctionId, a.InstallationTypeId=b.InstallationTypeId, a.VehicleColor=b.VehicleColor, 
                                    a.Colour=b.Colour, a.PinOut=b.PinOut, a.Location=b.Location, a.Polarity=b.Polarity, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWireFunction a
                                join dbo.VehicleWireFunctionEditor b on a.VehicleWireFunctionId = b.VehicleWireFunctionId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWireFunction a
                                join dbo.VehicleWireFunctionEditor b on a.VehicleWireFunctionId = b.VehicleWireFunctionId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(1);
                    }
                }
                catch (Exception ex)
                {
                    ShowWireError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            e.Handled = true;
        }

        protected void DisassemblyListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Disassembly?');";
                        }
                    }
                }

                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Disassembly?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Disassembly?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void DisassemblyListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveDisassembly(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Step, b.Note, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWireDisassemblyEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Disassembly ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["Step"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Step: " + reader["Step"].ToString() + "<br/>");
                            mailContent.AppendLine("Description: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWireDisassemblyEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWireDisassemblyEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(4);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWireDisassembly (
                                VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.Step, a.Note, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleWireDisassemblyEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.Step=b.Step, a.Note=b.Note, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWireDisassembly a
                                join dbo.VehicleWireDisassemblyEditor b on a.VehicleWireDisassemblyId = b.VehicleWireDisassemblyId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWireDisassembly a
                                join dbo.VehicleWireDisassemblyEditor b on a.VehicleWireDisassemblyId = b.VehicleWireDisassemblyId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(4);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            linksdisassemblytab.Attributes.Add("Class", "tab-title active");
            linksdisassembly.Attributes.Add("Class", "content active");

            e.Handled = true;
        }

        protected void EditApproveDisassembly(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWireDisassemblyEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWireDisassemblyEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWireDisassemblyIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWireDisassemblyEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWireDisassemblyEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    DisassemblyStep.Text = reader["Step"].ToString();
                    Disassemblyeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage1.ImageUrl = attach;
                            AttachDisassemblyFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage1.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl1.Text = "";
                        }

                        ExistingDisassemblyFilename1.Value = attach;
                        AttachDisassemblyImage1.Visible = true;

                        AttachDisassemblyImageDelete1.Visible = true;
                        ReplaceDisassemblyImageLabel1.Visible = true;

                        AttachDisassemblyImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename1.Value = "";
                        AttachDisassemblyFileUrl1.Text = "";
                        AttachDisassemblyImage1.Visible = false;
                        AttachDisassemblyImageDelete1.Visible = false;
                        ReplaceDisassemblyImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage2.ImageUrl = attach;
                            AttachDisassemblyFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage2.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl2.Text = "";
                        }

                        ExistingDisassemblyFilename2.Value = attach;
                        AttachDisassemblyImage2.Visible = true;

                        AttachDisassemblyImageDelete2.Visible = true;
                        ReplaceDisassemblyImageLabel2.Visible = true;

                        AttachDisassemblyImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename2.Value = "";
                        AttachDisassemblyFileUrl2.Text = "";
                        AttachDisassemblyImage2.Visible = false;
                        AttachDisassemblyImageDelete2.Visible = false;
                        ReplaceDisassemblyImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage3.ImageUrl = attach;
                            AttachDisassemblyFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage3.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl3.Text = "";
                        }

                        ExistingDisassemblyFilename3.Value = attach;
                        AttachDisassemblyImage3.Visible = true;

                        AttachDisassemblyImageDelete3.Visible = true;
                        ReplaceDisassemblyImageLabel3.Visible = true;

                        AttachDisassemblyImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename3.Value = "";
                        AttachDisassemblyFileUrl3.Text = "";
                        AttachDisassemblyImage3.Visible = false;
                        AttachDisassemblyImageDelete3.Visible = false;
                        ReplaceDisassemblyImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage4.ImageUrl = attach;
                            AttachDisassemblyFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage4.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl4.Text = "";
                        }

                        ExistingDisassemblyFilename4.Value = attach;
                        AttachDisassemblyImage4.Visible = true;

                        AttachDisassemblyImageDelete4.Visible = true;
                        ReplaceDisassemblyImageLabel4.Visible = true;

                        AttachDisassemblyImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename4.Value = "";
                        AttachDisassemblyFileUrl4.Text = "";
                        AttachDisassemblyImage4.Visible = false;
                        AttachDisassemblyImageDelete4.Visible = false;
                        ReplaceDisassemblyImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachDisassemblyImage5.ImageUrl = attach;
                            AttachDisassemblyFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachDisassemblyImage5.ImageUrl = SaveFolder + attach;
                            AttachDisassemblyFileUrl5.Text = "";
                        }

                        ExistingDisassemblyFilename5.Value = attach;
                        AttachDisassemblyImage5.Visible = true;

                        AttachDisassemblyImageDelete5.Visible = true;
                        ReplaceDisassemblyImageLabel5.Visible = true;

                        AttachDisassemblyImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingDisassemblyFilename5.Value = "";
                        AttachDisassemblyFileUrl5.Text = "";
                        AttachDisassemblyImage5.Visible = false;
                        AttachDisassemblyImageDelete5.Visible = false;
                        ReplaceDisassemblyImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddDisassemblyTitle.Text = "Edit & Approve Disassembly";
                AddDisassemblyButton.Text = "Approve";
                AddDisassemblyPanel.Visible = true;

                ResetTab();
                linksdisassemblytab.Attributes.Add("Class", "tab-title active");
                linksdisassembly.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void FBListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[6].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[6].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Facebook?');";
                        }
                    }
                }

                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Facebook?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Facebook?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void FBListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveFacebook(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Note, b.URL, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWireFacebookEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Facebook ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Description: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("URL: " + reader["URL"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWireFacebookEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWireFacebookEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(2);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWireFacebook (
                                VehicleMakeModelYearId, Note, URL, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.Note, a.URL, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleWireFacebookEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.URL=b.URL, a.Note=b.Note, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWireFacebook a
                                join dbo.VehicleWireFacebookEditor b on a.VehicleWireFacebookId = b.VehicleWireFacebookId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWireFacebook a
                                join dbo.VehicleWireFacebookEditor b on a.VehicleWireFacebookId = b.VehicleWireFacebookId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(2);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            FBPaneltab.Attributes.Add("Class", "tab-title active");
            FBPanel.Attributes.Add("Class", "content active");

            e.Handled = true;
        }

        protected void EditApproveFacebook(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWireFacebookEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWireFacebookEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWireFacebookIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select URL, Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWireFacebookEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWireFacebookEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    FBeditor.Text = reader["Note"].ToString();
                    FBUrl.Text = reader["URL"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage1.ImageUrl = attach;
                            AttachFBFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachFBImage1.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl1.Text = "";
                        }
                        ExistingFBFilename1.Value = attach;
                        AttachFBImage1.Visible = true;
                        AttachFBImageDelete1.Visible = true;
                        ReplaceFBImageLabel1.Visible = true;

                        AttachFBImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename1.Value = "";
                        AttachFBFileUrl1.Text = "";
                        AttachFBImage1.Visible = false;
                        AttachFBImageDelete1.Visible = false;
                        ReplaceFBImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage2.ImageUrl = attach;
                            AttachFBFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachFBImage2.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl2.Text = "";
                        }
                        ExistingFBFilename2.Value = attach;
                        AttachFBImage2.Visible = true;
                        AttachFBImageDelete2.Visible = true;
                        ReplaceFBImageLabel2.Visible = true;

                        AttachFBImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename2.Value = "";
                        AttachFBFileUrl2.Text = "";
                        AttachFBImage2.Visible = false;
                        AttachFBImageDelete2.Visible = false;
                        ReplaceFBImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage3.ImageUrl = attach;
                            AttachFBFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachFBImage3.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl3.Text = "";
                        }
                        ExistingFBFilename3.Value = attach;
                        AttachFBImage3.Visible = true;
                        AttachFBImageDelete3.Visible = true;
                        ReplaceFBImageLabel3.Visible = true;

                        AttachFBImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename3.Value = "";
                        AttachFBFileUrl3.Text = "";
                        AttachFBImage3.Visible = false;
                        AttachFBImageDelete3.Visible = false;
                        ReplaceFBImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage4.ImageUrl = attach;
                            AttachFBFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachFBImage4.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl4.Text = "";
                        }
                        ExistingFBFilename4.Value = attach;
                        AttachFBImage4.Visible = true;
                        AttachFBImageDelete4.Visible = true;
                        ReplaceFBImageLabel4.Visible = true;

                        AttachFBImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename4.Value = "";
                        AttachFBFileUrl4.Text = "";
                        AttachFBImage4.Visible = false;
                        AttachFBImageDelete4.Visible = false;
                        ReplaceFBImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachFBImage5.ImageUrl = attach;
                            AttachFBFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachFBImage5.ImageUrl = SaveFolder + attach;
                            AttachFBFileUrl5.Text = "";
                        }
                        ExistingFBFilename5.Value = attach;
                        AttachFBImage5.Visible = true;
                        AttachFBImageDelete5.Visible = true;
                        ReplaceFBImageLabel5.Visible = true;

                        AttachFBImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingFBFilename5.Value = "";
                        AttachFBFileUrl5.Text = "";
                        AttachFBImage5.Visible = false;
                        AttachFBImageDelete5.Visible = false;
                        ReplaceFBImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddFBTitle.Text = "Edit & Approve Facebook";
                AddFBButton.Text = "Approve";
                AddFBPanel.Visible = true;

                ResetTab();
                FBPaneltab.Attributes.Add("Class", "tab-title active");
                FBPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void DocumentListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[5].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[5].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[6].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[6].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Document?');";
                        }
                    }
                }

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Document?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Document?');";
                        }
                    }
                }

                HiddenField VehicleDocumentId = e.Row.FindControl("VehicleDocumentId") as HiddenField;
                if (VehicleDocumentId != null)
                {
                    HiddenField DocumentName = e.Row.FindControl("DocumentName") as HiddenField;
                    HiddenField AttachFile = e.Row.FindControl("AttachFile") as HiddenField;

                    Label DocumentLabel = e.Row.FindControl("DocumentLabel") as Label;
                    if (AttachFile.Value.StartsWith("http://") || AttachFile.Value.StartsWith("https://"))
                    {
                        DocumentLabel.Text = "<a href='" + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a> - " + AttachFile.Value;
                    }
                    else
                    {
                        DocumentLabel.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile.Value + "' target='_blank'>" + DocumentName.Value + "</a>";
                    }
                }
            }
        }

        protected void DocumentListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveDocument(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.DocumentName, b.AttachFile, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleDocumentEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Document ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["DocumentName"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Document: " + reader["DocumentName"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, AttachFile 
                            from dbo.VehicleDocumentEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["AttachFile"] != null && reader["AttachFile"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["AttachFile"].ToString()))
                                {
                                    File.Delete(FullPath + reader["AttachFile"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleDocumentEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(3);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleDocument (
                                VehicleMakeModelYearId, DocumentName, AttachFile, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.DocumentName, a.AttachFile,
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleDocumentEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.DocumentName=b.DocumentName, a.AttachFile=b.AttachFile, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleDocument a
                                join dbo.VehicleDocumentEditor b on a.VehicleDocumentId = b.VehicleDocumentId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleDocument a
                                join dbo.VehicleDocumentEditor b on a.VehicleDocumentId = b.VehicleDocumentId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(3);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            DocumentPaneltab.Attributes.Add("Class", "tab-title active");
            DocumentPanel.Attributes.Add("Class", "content active");

            e.Handled = true;
        }

        protected void EditApproveDocument(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleDocumentEditorWorkIdHidden.Value = EditorWorkId;
            VehicleDocumentEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleDocumentIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select DocumentName, AttachFile
                    from dbo.VehicleDocumentEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleDocumentEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string DocumentName = reader["DocumentName"].ToString();
                    string AttachFile = reader["AttachFile"].ToString();
                    if (AttachFile.StartsWith("http://") || AttachFile.StartsWith("https://"))
                    {
                        ExistingDocument.Text = "<a href='" + AttachFile + "' target='_blank'>" + DocumentName + "</a> - " + AttachFile;
                        AttachFileUrl.Text = AttachFile;
                    }
                    else
                    {
                        ExistingDocument.Text = "<a href='../" + SaveFolderHidden.Value.Replace("~/", "") + AttachFile + "' target='_blank'>" + DocumentName + "</a>";
                    }

                    CurrentDocumentAttachFile.Value = AttachFile;
                }
                reader.Close();

                AddDocumentTitle.Text = "Edit & Approve Document";
                AddDocumentButton.Text = "Approve";
                AddDocumentPanel.Visible = true;

                ResetTab();
                DocumentPaneltab.Attributes.Add("Class", "tab-title active");
                DocumentPanel.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void PrepListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Prep?');";
                        }
                    }
                }

                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Prep?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Prep?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void PrepListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApprovePrep(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Step, b.Note, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWirePrepEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Prep ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["Step"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Step: " + reader["Step"].ToString() + "<br/>");
                            mailContent.AppendLine("Description: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWirePrepEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWirePrepEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(5);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWirePrep (
                                VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.Step, a.Note, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleWirePrepEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.Step=b.Step, a.Note=b.Note, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWirePrep a
                                join dbo.VehicleWirePrepEditor b on a.VehicleWirePrepId = b.VehicleWirePrepId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWirePrep a
                                join dbo.VehicleWirePrepEditor b on a.VehicleWirePrepId = b.VehicleWirePrepId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(5);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            linkspreptab.Attributes.Add("Class", "tab-title active");
            linksprep.Attributes.Add("Class", "content active");

            e.Handled = true;
        }

        protected void EditApprovePrep(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWirePrepEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWirePrepEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWirePrepIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWirePrepEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWirePrepEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    PrepStep.Text = reader["Step"].ToString();
                    Prepeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage1.ImageUrl = attach;
                            AttachPrepFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage1.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl1.Text = "";
                        }
                        ExistingPrepFilename1.Value = attach;
                        AttachPrepImage1.Visible = true;

                        AttachPrepImageDelete1.Visible = true;
                        ReplacePrepImageLabel1.Visible = true;

                        AttachPrepImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename1.Value = "";
                        AttachPrepFileUrl1.Text = "";
                        AttachPrepImage1.Visible = false;
                        AttachPrepImageDelete1.Visible = false;
                        ReplacePrepImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage2.ImageUrl = attach;
                            AttachPrepFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage2.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl2.Text = "";
                        }
                        ExistingPrepFilename2.Value = attach;
                        AttachPrepImage2.Visible = true;

                        AttachPrepImageDelete2.Visible = true;
                        ReplacePrepImageLabel2.Visible = true;

                        AttachPrepImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename2.Value = "";
                        AttachPrepFileUrl2.Text = "";
                        AttachPrepImage2.Visible = false;
                        AttachPrepImageDelete2.Visible = false;
                        ReplacePrepImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage3.ImageUrl = attach;
                            AttachPrepFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage3.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl3.Text = "";
                        }
                        ExistingPrepFilename3.Value = attach;
                        AttachPrepImage3.Visible = true;

                        AttachPrepImageDelete3.Visible = true;
                        ReplacePrepImageLabel3.Visible = true;

                        AttachPrepImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename3.Value = "";
                        AttachPrepFileUrl3.Text = "";
                        AttachPrepImage3.Visible = false;
                        AttachPrepImageDelete3.Visible = false;
                        ReplacePrepImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage4.ImageUrl = attach;
                            AttachPrepFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage4.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl4.Text = "";
                        }
                        ExistingPrepFilename4.Value = attach;
                        AttachPrepImage4.Visible = true;

                        AttachPrepImageDelete4.Visible = true;
                        ReplacePrepImageLabel4.Visible = true;

                        AttachPrepImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename4.Value = "";
                        AttachPrepFileUrl4.Text = "";
                        AttachPrepImage4.Visible = false;
                        AttachPrepImageDelete4.Visible = false;
                        ReplacePrepImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachPrepImage5.ImageUrl = attach;
                            AttachPrepFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachPrepImage5.ImageUrl = SaveFolder + attach;
                            AttachPrepFileUrl5.Text = "";
                        }
                        ExistingPrepFilename5.Value = attach;
                        AttachPrepImage5.Visible = true;

                        AttachPrepImageDelete5.Visible = true;
                        ReplacePrepImageLabel5.Visible = true;

                        AttachPrepImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingPrepFilename5.Value = "";
                        AttachPrepFileUrl5.Text = "";
                        AttachPrepImage5.Visible = false;
                        AttachPrepImageDelete5.Visible = false;
                        ReplacePrepImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddPrepTitle.Text = "Edit & Approve Prep";
                AddPrepButton.Text = "Approve";
                AddPrepPanel.Visible = true;

                ResetTab();
                linkspreptab.Attributes.Add("Class", "tab-title active");
                linksprep.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void RoutingListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Routing/Plancement?');";
                        }
                    }
                }

                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Routing/Placement?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Routing/Placement?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void RoutingListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveRouting(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Step, b.Note, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWirePlacementRoutingEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Routing/Placement ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["Step"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Step: " + reader["Step"].ToString() + "<br/>");
                            mailContent.AppendLine("Description: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWirePlacementRoutingEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWirePlacementRoutingEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(6);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWirePlacementRouting (
                                VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.Step, a.Note, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleWirePlacementRoutingEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.Step=b.Step, a.Note=b.Note, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWirePlacementRouting a
                                join dbo.VehicleWirePlacementRoutingEditor b on a.VehicleWirePlacementRoutingId = b.VehicleWirePlacementRoutingId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWirePlacementRouting a
                                join dbo.VehicleWirePlacementRoutingEditor b on a.VehicleWirePlacementRoutingId = b.VehicleWirePlacementRoutingId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(6);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            linkroutingtab.Attributes.Add("Class", "tab-title active");
            linkrouting.Attributes.Add("Class", "content active");

            e.Handled = true;
        }

        protected void EditApproveRouting(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWirePlacementRoutingEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWirePlacementRoutingEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWirePlacementRoutingIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWirePlacementRoutingEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWirePlacementRoutingEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    RoutingStep.Text = reader["Step"].ToString();
                    Routingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage1.ImageUrl = attach;
                            AttachRoutingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage1.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl1.Text = "";
                        }
                        ExistingRoutingFilename1.Value = attach;
                        AttachRoutingImage1.Visible = true;

                        AttachRoutingImageDelete1.Visible = true;
                        ReplaceRoutingImageLabel1.Visible = true;

                        AttachRoutingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename1.Value = "";
                        AttachRoutingFileUrl1.Text = "";
                        AttachRoutingImage1.Visible = false;
                        AttachRoutingImageDelete1.Visible = false;
                        ReplaceRoutingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage2.ImageUrl = attach;
                            AttachRoutingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage2.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl2.Text = "";
                        }
                        ExistingRoutingFilename2.Value = attach;
                        AttachRoutingImage2.Visible = true;

                        AttachRoutingImageDelete2.Visible = true;
                        ReplaceRoutingImageLabel2.Visible = true;

                        AttachRoutingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename2.Value = "";
                        AttachRoutingFileUrl2.Text = "";
                        AttachRoutingImage2.Visible = false;
                        AttachRoutingImageDelete2.Visible = false;
                        ReplaceRoutingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage3.ImageUrl = attach;
                            AttachRoutingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage3.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl3.Text = "";
                        }
                        ExistingRoutingFilename3.Value = attach;
                        AttachRoutingImage3.Visible = true;

                        AttachRoutingImageDelete3.Visible = true;
                        ReplaceRoutingImageLabel3.Visible = true;

                        AttachRoutingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename3.Value = "";
                        AttachRoutingFileUrl3.Text = "";
                        AttachRoutingImage3.Visible = false;
                        AttachRoutingImageDelete3.Visible = false;
                        ReplaceRoutingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage4.ImageUrl = attach;
                            AttachRoutingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage4.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl4.Text = "";
                        }
                        ExistingRoutingFilename4.Value = attach;
                        AttachRoutingImage4.Visible = true;

                        AttachRoutingImageDelete4.Visible = true;
                        ReplaceRoutingImageLabel4.Visible = true;

                        AttachRoutingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename4.Value = "";
                        AttachRoutingFileUrl4.Text = "";
                        AttachRoutingImage4.Visible = false;
                        AttachRoutingImageDelete4.Visible = false;
                        ReplaceRoutingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachRoutingImage5.ImageUrl = attach;
                            AttachRoutingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachRoutingImage5.ImageUrl = SaveFolder + attach;
                            AttachRoutingFileUrl5.Text = "";
                        }
                        ExistingRoutingFilename5.Value = attach;
                        AttachRoutingImage5.Visible = true;

                        AttachRoutingImageDelete5.Visible = true;
                        ReplaceRoutingImageLabel5.Visible = true;

                        AttachRoutingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingRoutingFilename5.Value = "";
                        AttachRoutingFileUrl5.Text = "";
                        AttachRoutingImage5.Visible = false;
                        AttachRoutingImageDelete5.Visible = false;
                        ReplaceRoutingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddRoutingTitle.Text = "Edit & Approve Routing/Placement";
                AddRoutingButton.Text = "Approve";
                AddRoutingPanel.Visible = true;

                ResetTab();
                linkroutingtab.Attributes.Add("Class", "tab-title active");
                linkrouting.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void ProgrammingListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Programming?');";
                        }
                    }
                }

                if (e.Row.Cells[9].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[9].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Programming?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Programming?');";
                        }
                    }
                }

                HyperLink Image1Link = e.Row.FindControl("Image1Link") as HyperLink;
                if (Image1Link != null && Image1Link.ImageUrl == "")
                {
                    Image1Link.Visible = false;
                }
                HyperLink Image2Link = e.Row.FindControl("Image2Link") as HyperLink;
                if (Image2Link != null && Image2Link.ImageUrl == "")
                {
                    Image2Link.Visible = false;
                }
                HyperLink Image3Link = e.Row.FindControl("Image3Link") as HyperLink;
                if (Image3Link != null && Image3Link.ImageUrl == "")
                {
                    Image3Link.Visible = false;
                }
                HyperLink Image4Link = e.Row.FindControl("Image4Link") as HyperLink;
                if (Image4Link != null && Image4Link.ImageUrl == "")
                {
                    Image4Link.Visible = false;
                }
                HyperLink Image5Link = e.Row.FindControl("Image5Link") as HyperLink;
                if (Image5Link != null && Image5Link.ImageUrl == "")
                {
                    Image5Link.Visible = false;
                }
            }
        }

        protected void ProgrammingListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveProgramming(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Step, b.Note, anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWireProgrammingEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Programming ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["Step"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Step: " + reader["Step"].ToString() + "<br/>");
                            mailContent.AppendLine("Description: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"select b.EditorWorkId, Attach1, Attach2, Attach3, Attach4, Attach5 
                            from dbo.VehicleWireProgrammingEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach1"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach1"].ToString());
                                }
                            }
                            if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach2"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach2"].ToString());
                                }
                            }
                            if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach3"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach3"].ToString());
                                }
                            }
                            if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach4"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach4"].ToString());
                                }
                            }
                            if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                            {
                                if (File.Exists(FullPath + reader["Attach5"].ToString()))
                                {
                                    File.Delete(FullPath + reader["Attach5"].ToString());
                                }
                            }
                        }
                        reader.Close();

                        sql = @"delete a from dbo.VehicleWireProgrammingEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(7);
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "0") // Add
                        {
                            string sql2 = @"insert into dbo.VehicleWireProgramming (
                                VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, 
                                UpdatedDt, UpdatedBy, EditorWorkId) 
                                select 
                                a.VehicleMakeModelYearId, a.Step, a.Note, a.Attach1, a.Attach2, a.Attach3, a.Attach4, a.Attach5, 
                                b.EditorWorkDt, b.EditorId, a.EditorWorkId
                                from dbo.VehicleWireProgrammingEditor a
                                join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                                where b.EditorWorkId = @EditorWorkId and b.EditorWorkAction = 0";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"update a
                                set a.Step=b.Step, a.Note=b.Note, 
                                    a.Attach1=b.Attach1, a.Attach2=b.Attach2, a.Attach3=b.Attach3, a.Attach4=b.Attach4, a.Attach5=b.Attach5, 
                                    a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                from dbo.VehicleWireProgramming a
                                join dbo.VehicleWireProgrammingEditor b on a.VehicleWireProgrammingId = b.VehicleWireProgrammingId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1 ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        else if (EditorWorkAction == "2") // Delete
                        {
                            string sql2 = @"delete a
                                from dbo.VehicleWireProgramming a
                                join dbo.VehicleWireProgrammingEditor b on a.VehicleWireProgrammingId = b.VehicleWireProgrammingId
                                join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 2";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(7);
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            ResetTab();
            linkprogrammingtab.Attributes.Add("Class", "tab-title active");
            linkprogramming.Attributes.Add("Class", "content active");

            e.Handled = true;

        }

        protected void EditApproveProgramming(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWireProgrammingEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWireProgrammingEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWireProgrammingIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5 
                    from dbo.VehicleWireProgrammingEditor a 
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWireProgrammingEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ProgrammingStep.Text = reader["Step"].ToString();
                    Programmingeditor.Text = reader["Note"].ToString();

                    if (reader["Attach1"] != null && reader["Attach1"].ToString() != "")
                    {
                        string attach = reader["Attach1"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage1.ImageUrl = attach;
                            AttachProgrammingFileUrl1.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage1.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl1.Text = "";
                        }
                        ExistingProgrammingFilename1.Value = attach;
                        AttachProgrammingImage1.Visible = true;

                        AttachProgrammingImageDelete1.Visible = true;
                        ReplaceProgrammingImageLabel1.Visible = true;

                        AttachProgrammingImageDelete1.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename1.Value = "";
                        AttachProgrammingFileUrl1.Text = "";
                        AttachProgrammingImage1.Visible = false;
                        AttachProgrammingImageDelete1.Visible = false;
                        ReplaceProgrammingImageLabel1.Visible = false;
                    }

                    if (reader["Attach2"] != null && reader["Attach2"].ToString() != "")
                    {
                        string attach = reader["Attach2"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage2.ImageUrl = attach;
                            AttachProgrammingFileUrl2.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage2.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl2.Text = "";
                        }
                        ExistingProgrammingFilename2.Value = attach;
                        AttachProgrammingImage2.Visible = true;

                        AttachProgrammingImageDelete2.Visible = true;
                        ReplaceProgrammingImageLabel2.Visible = true;

                        AttachProgrammingImageDelete2.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename2.Value = "";
                        AttachProgrammingFileUrl2.Text = "";
                        AttachProgrammingImage2.Visible = false;
                        AttachProgrammingImageDelete2.Visible = false;
                        ReplaceProgrammingImageLabel2.Visible = false;
                    }

                    if (reader["Attach3"] != null && reader["Attach3"].ToString() != "")
                    {
                        string attach = reader["Attach3"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage3.ImageUrl = attach;
                            AttachProgrammingFileUrl3.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage3.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl3.Text = "";
                        }
                        ExistingProgrammingFilename3.Value = attach;
                        AttachProgrammingImage3.Visible = true;

                        AttachProgrammingImageDelete3.Visible = true;
                        ReplaceProgrammingImageLabel3.Visible = true;

                        AttachProgrammingImageDelete3.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename3.Value = "";
                        AttachProgrammingFileUrl3.Text = "";
                        AttachProgrammingImage3.Visible = false;
                        AttachProgrammingImageDelete3.Visible = false;
                        ReplaceProgrammingImageLabel3.Visible = false;
                    }

                    if (reader["Attach4"] != null && reader["Attach4"].ToString() != "")
                    {
                        string attach = reader["Attach4"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage4.ImageUrl = attach;
                            AttachProgrammingFileUrl4.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage4.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl4.Text = "";
                        }
                        ExistingProgrammingFilename4.Value = attach;
                        AttachProgrammingImage4.Visible = true;

                        AttachProgrammingImageDelete4.Visible = true;
                        ReplaceProgrammingImageLabel4.Visible = true;

                        AttachProgrammingImageDelete4.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename4.Value = "";
                        AttachProgrammingFileUrl4.Text = "";
                        AttachProgrammingImage4.Visible = false;
                        AttachProgrammingImageDelete4.Visible = false;
                        ReplaceProgrammingImageLabel4.Visible = false;
                    }

                    if (reader["Attach5"] != null && reader["Attach5"].ToString() != "")
                    {
                        string attach = reader["Attach5"].ToString();

                        if (attach.StartsWith("http://") || attach.StartsWith("https://"))
                        {
                            AttachProgrammingImage5.ImageUrl = attach;
                            AttachProgrammingFileUrl5.Text = attach;
                        }
                        else
                        {
                            AttachProgrammingImage5.ImageUrl = SaveFolder + attach;
                            AttachProgrammingFileUrl5.Text = "";
                        }
                        ExistingProgrammingFilename5.Value = attach;
                        AttachProgrammingImage5.Visible = true;

                        AttachProgrammingImageDelete5.Visible = true;
                        ReplaceProgrammingImageLabel5.Visible = true;

                        AttachProgrammingImageDelete5.Checked = false;
                    }
                    else
                    {
                        ExistingProgrammingFilename5.Value = "";
                        AttachProgrammingFileUrl5.Text = "";
                        AttachProgrammingImage5.Visible = false;
                        AttachProgrammingImageDelete5.Visible = false;
                        ReplaceProgrammingImageLabel5.Visible = false;
                    }
                }
                reader.Close();

                AddProgrammingTitle.Text = "Edit & Approve Programming";
                AddProgrammingButton.Text = "Approve";
                AddProgrammingPanel.Visible = true;

                ResetTab();
                linkprogrammingtab.Attributes.Add("Class", "tab-title active");
                linkprogramming.Attributes.Add("Class", "content active");
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void NoteListEditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField StatusId = e.Row.FindControl("StatusId") as HiddenField;
                string EditorWorkAction = ((HiddenField)(e.Row.FindControl("EditorWorkAction"))).Value;

                if (e.Row.Cells[6].Controls.Count > 0)
                {
                    LinkButton editApproveButton = e.Row.Cells[6].Controls[0] as LinkButton;
                    if (EditorWorkAction == "2") // Delete
                    {
                        editApproveButton.Visible = false;
                    }
                    else if (editApproveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            editApproveButton.Visible = false;
                        }
                    }
                }


                if (e.Row.Cells[7].Controls.Count > 0)
                {
                    LinkButton approveButton = e.Row.Cells[7].Controls[0] as LinkButton;
                    if (approveButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            approveButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            approveButton.OnClientClick = "return confirm('Are you sure you want to approve this Note?');";
                        }
                    }
                }

                if (e.Row.Cells[8].Controls.Count > 0)
                {
                    LinkButton deleteButton = e.Row.Cells[8].Controls[0] as LinkButton;
                    if (deleteButton != null)
                    {
                        if (StatusId.Value == "1" || StatusId.Value == "2" || StatusId.Value == "3") // Approved or EditApproved or Denied
                        {
                            deleteButton.Visible = false;
                        }
                        else if (RoleNameHidden.Value.Contains("Administrator"))
                        {
                            deleteButton.Text = "Deny";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to deny this Note?');";
                        }
                        else if (RoleNameHidden.Value.Contains("Editor"))
                        {
                            deleteButton.Text = "Delete";
                            deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Note?');";
                        }
                    }
                }
            }
        }

        protected void NoteListEditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearError();

            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string EditorWorkId = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkId"))).Value;
            string EditorWorkAction = ((HiddenField)(List.Rows[idx].FindControl("EditorWorkAction"))).Value;

            if (e.CommandName == "EditApprove")
            {
                EditApproveNote(EditorWorkId, EditorWorkAction);
            }
            else if (e.CommandName == "DeleteDeny")
            {
                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    string sql = "";
                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        // Denied
                        sql = "update dbo.EditorWork set StatusId=3, DeniedBy=@DeniedBy, DeniedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@DeniedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();


                        sql = @"select a.EditorId, m.VehicleMakeName, o.VehicleModelName, v.VehicleYear, b.Note, d.InstallationTypeId, d.InstallationTypeName, 
                                anu.Email as EditorEmail,
                                EditorWorkActionName = case a.EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end
                            from dbo.EditorWork a 
                            join dbo.VehicleWireNoteEditor b on a.EditorWorkId = b.EditorWorkId
                            join dbo.InstallationType d on b.InstallationTypeId = d.InstallationTypeId
                            join dbo.VehicleMakeModelYear v WITH (NOLOCK) on b.VehicleMakeModelYearId = v.VehicleMakeModelYearId 
                            join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
                            join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId 
                            join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = a.EditorId
                        where a.EditorWorkId = @EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        string EditorEmail = "";
                        string subject = "Denied - Wire Note ";

                        StringBuilder mailContent = new StringBuilder();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            EditorEmail = reader["EditorEmail"].ToString();

                            subject += reader["VehicleMakeName"].ToString() + " " + reader["VehicleModelName"].ToString() + " " + reader["VehicleYear"].ToString()
                                + reader["InstallationTypeName"].ToString();

                            mailContent.AppendLine("Type: " + reader["EditorWorkActionName"].ToString() + "<br/>");
                            mailContent.AppendLine("Make: " + reader["VehicleMakeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Model: " + reader["VehicleModelName"].ToString() + "<br/>");
                            mailContent.AppendLine("Year: " + reader["VehicleYear"].ToString() + "<br/>");
                            mailContent.AppendLine("Installation Type: " + reader["InstallationTypeName"].ToString() + "<br/>");
                            mailContent.AppendLine("Note: " + reader["Note"].ToString() + "<br/>");
                            mailContent.AppendLine("<a href=\"" + Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearIdHidden.Value + "\" target=\"_blank\" style=\"text-decoration: underline; color: #00B8E4; font-weight: bold;\">Edit Vehicle</a><br/>");
                        }
                        reader.Close();

                        if (EditorEmail != "")
                        {
                            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            var EditorUser = manager.FindByEmail(EditorEmail);

                            try
                            { 
                                manager.SendEmail(EditorUser.Id, subject, mailContent.ToString());
                            }
                            catch (Exception)
                            {
                                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                                var admin = manager.FindByEmail(AdminEmail);

                                string title = "Sending Failure - " + subject;
                                string content = "Sending Failure - " + EditorUser.Email + "<br/>" + mailContent.ToString();
                                manager.SendEmail(admin.Id, title, content);
                            }
                        }
                    }
                    else if (RoleNameHidden.Value.Contains("Editor"))
                    {
                        sql = @"delete a from dbo.VehicleWireNoteEditor a 
                            join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                            where b.EditorWorkId = @EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();


                        sql = "delete from dbo.EditorWork where EditorWorkId=@EditorWorkId";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = EditorWorkId;

                        cmd.ExecuteNonQuery();
                    }

                    if (sql != "")
                    {
                        tran.Commit();
                    }

                    SearchInfo(1);
                }
                catch (Exception ex)
                {
                    ShowWireError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
            else if (e.CommandName == "Approve")
            {
                SqlConnection conn = null;
                SqlTransaction tran = null;
                try
                {
                    string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    conn = new SqlConnection(connStr);

                    conn.Open();
                    tran = conn.BeginTransaction();

                    if (RoleNameHidden.Value.Contains("Administrator"))
                    {
                        if (EditorWorkAction == "1") // Edit
                        {
                            string sql2 = @"
                                if exists (select * from dbo.VehicleWireNoteEditor where EditorWorkId=@EditorWorkId and VehicleWireNoteId is not null)
                                begin
                                    update a
                                    set a.Note=b.Note, 
                                        a.UpdatedDt=c.EditorWorkDt, a.UpdatedBy=c.EditorId, a.EditorWorkId = c.EditorWorkId
                                    from dbo.VehicleWireNote a
                                    join dbo.VehicleWireNoteEditor b on a.VehicleWireNoteId = b.VehicleWireNoteId
                                    join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                    where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1;
                                end
                                else if exists (select * from dbo.VehicleWireNoteEditor where EditorWorkId=@EditorWorkId )
                                begin
                                    insert into dbo.VehicleWireNote(VehicleMakeModelYearId, InstallationTypeId, Note, UpdatedBy, UpdatedDt, EditorWorkId) 
                                    select b.VehicleMakeModelYearId, b.InstallationTypeId, b.Note, c.EditorId, c.EditorWorkDt,  b.EditorWorkId
                                    from dbo.VehicleWireNoteEditor b 
                                    join dbo.EditorWork c on b.EditorWorkId = c.EditorWorkId
                                    where c.EditorWorkId = @EditorWorkId and c.EditorWorkAction = 1;
                                end
                            ";

                            SqlCommand cmd2 = new SqlCommand(sql2, conn);
                            cmd2.CommandType = CommandType.Text;
                            cmd2.Transaction = tran;

                            cmd2.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                            cmd2.ExecuteNonQuery();
                        }
                        
                        string sql = "update dbo.EditorWork set StatusId=1, ApprovedBy=@ApprovedBy, ApprovedDt=getdate() where EditorWorkId=@EditorWorkId";

                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = tran;

                        cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                        cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(EditorWorkId);

                        cmd.ExecuteNonQuery();

                        tran.Commit();

                        SearchInfo(1);
                    }
                }
                catch (Exception ex)
                {
                    ShowWireError(ex.ToString());
                    if (conn.State == ConnectionState.Open && tran != null)
                        tran.Rollback();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }

            e.Handled = true;
        }

        protected void EditApproveNote(string EditorWorkId, string EditorWorkAction)
        {
            LoadWireInstall();

            VehicleWireNoteEditorWorkIdHidden.Value = EditorWorkId;
            VehicleWireNoteEditorWorkActionHidden.Value = EditorWorkAction;
            VehicleWireNoteIdHidden.Value = "";

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string sql = @"select a.Note, a.InstallationTypeId, d.InstallationTypeName
                    from dbo.VehicleWireNoteEditor a 
                    join dbo.InstallationType d on a.InstallationTypeId = d.InstallationTypeId
                    join dbo.EditorWork b on a.EditorWorkId = b.EditorWorkId
                    where a.EditorWorkId = @EditorWorkId";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@EditorWorkId", SqlDbType.Int).Value = int.Parse(VehicleWireNoteEditorWorkIdHidden.Value);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    editor.Text = reader["Note"].ToString();
                    NoteInstallationTypeIdHidden.Value = reader["InstallationTypeId"].ToString();
                    InstallationTypeEditLabl.Text = reader["InstallationTypeName"].ToString();
                }
                else
                {
                    editor.Text = "";
                }
                reader.Close();

                SaveNoteButton.Text = "Approve";
                EditNotePanel.Visible = true;
            }
            catch (Exception ex)
            {
                ShowWireError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        protected void AddWireBatchButton_Click(object sender, EventArgs e)
        {
            ShowWireBatch();
        }

        private void ShowWireBatch()
        { 
            if (SearchInstallationTypeList.SelectedValue == "-1")
            {
                foreach (ListItem item in SearchInstallationTypeList.Items)
                {
                    if (item.Text.Trim() == "CM-7X00 w/ Blade AL")
                    {
                        SearchInstallationTypeList.SelectedValue = item.Value;
                        break;
                    }
                }
            }

            LoadWireInstall();

            DataTable tbl = new DataTable("TopTable");
            tbl.Columns.Add(new DataColumn("No", System.Type.GetType("System.String")));
            tbl.Columns.Add(new DataColumn("InstallationTypeName", System.Type.GetType("System.String")));

            for (int i=0; i<20; i++)
            {
                DataRow row = tbl.NewRow();
                row[0] = i.ToString();
                row[1] = SearchInstallationTypeList.SelectedItem.Text;

                tbl.Rows.Add(row);
            }

            WireListBatch.DataSource = tbl;
            WireListBatch.DataBind();

            if (WireListBatch != null && WireListBatch.HeaderRow != null && WireListBatch.HeaderRow.Cells.Count > 0)
            {
                WireListBatch.Attributes["data-page"] = "false";
                WireListBatch.HeaderRow.TableSection = TableRowSection.TableHeader;
            }


            WireListBatchPanel.Visible = true;
        }

        protected void WireListBatch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList WireFunctionList1 = (DropDownList)(e.Row.FindControl("WireFunctionList"));
                foreach(ListItem item in WireFunctionList.Items)
                {
                    WireFunctionList1.Items.Add(new ListItem(item.Text, item.Value));
                }
            }
        }

        protected void CancelWireBatchButton_Click(object sender, EventArgs e)
        {
            WireListBatchPanel.Visible = false;
        }

        protected void SaveWireBatchButton_Click(object sender, EventArgs e)
        {
            string SaveFolder1 = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            string FullPath = HttpContext.Current.Server.MapPath(SaveFolder1);
            string Make = VehicleMakeList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Model = VehicleModelList.SelectedItem.Text.Replace(" ", "").Replace("'", "").Replace("\'", "").Replace(",", "").Replace(".", "");
            string Year = VehicleYearList.SelectedItem.Text;

            bool IsUpdated = false;
            foreach (GridViewRow row in WireListBatch.Rows)
            {
                TextBox ColorTxtBatch = (TextBox)(row.FindControl("ColorTxt"));
                TextBox VehicleColorTxtBatch = (TextBox)(row.FindControl("VehicleColorTxt"));
                TextBox LocationTxtBatch = (TextBox)(row.FindControl("LocationTxt"));
                TextBox PinOutTxtBatch = (TextBox)(row.FindControl("PinOutTxt"));
                TextBox PolarityTxtBatch = (TextBox)(row.FindControl("PolarityTxt"));
                FileUpload AttachWiringFile1 = (FileUpload)(row.FindControl("AttachWiringFile1"));
                FileUpload AttachWiringFile2 = (FileUpload)(row.FindControl("AttachWiringFile2"));
                FileUpload AttachWiringFile3 = (FileUpload)(row.FindControl("AttachWiringFile3"));
                FileUpload AttachWiringFile4 = (FileUpload)(row.FindControl("AttachWiringFile4"));
                FileUpload AttachWiringFile5 = (FileUpload)(row.FindControl("AttachWiringFile5"));

                if (ColorTxtBatch.Text.Trim() != "" || VehicleColorTxtBatch.Text.Trim() != "" || LocationTxtBatch.Text.Trim() != "" ||
                    PinOutTxtBatch.Text.Trim() != "" || PolarityTxtBatch.Text.Trim() != "")
                {
                    IsUpdated = true;

                    string FileName1 = "";
                    SaveFile(AttachWiringFile1.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName1);
                    string FileName2 = "";
                    SaveFile(AttachWiringFile2.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName2);
                    string FileName3 = "";
                    SaveFile(AttachWiringFile3.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName3);
                    string FileName4 = "";
                    SaveFile(AttachWiringFile4.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName4);
                    string FileName5 = "";
                    SaveFile(AttachWiringFile5.PostedFile, FullPath, "Wiring", Make, Model, Year, out FileName5);

                    DropDownList WireFunctionListBatch = (DropDownList)(row.FindControl("WireFunctionList"));

                    AddVehicleWire("", VehicleMakeModelYearIdHidden.Value, WireFunctionListBatch.SelectedValue, SearchInstallationTypeList.SelectedValue,
                        VehicleColorTxtBatch.Text.Trim(), ColorTxtBatch.Text.Trim(),
                        PinOutTxtBatch.Text.Trim(), LocationTxtBatch.Text.Trim(), PolarityTxtBatch.Text.Trim(), FileName1, FileName2, FileName3, FileName4, FileName5);
                }
            }

            WireListBatchPanel.Visible = false;

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (IsUpdated)
                {
                    string SaveFolder = SaveFolderHidden.Value;
                    ShowWireList(Con, SaveFolder);
                    ShowWireListEditor(Con, SaveFolder);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void ReserveButton_Click(object sender, EventArgs e)
        {
            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = @"select isnull(count(*), 0) as Cnt
                        from dbo.VehicleMakeModelYear 
                        where dateadd(day, 3, ReservedAt) > getdate() and ReservedBy=@ReservedBy";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@ReservedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                int ReserveCnt = 0;
                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    ReserveCnt = int.Parse(reader["Cnt"].ToString());
                }
                reader.Close();

                if (ReserveCnt >= 5)
                {
                    ShowError("You already reserved 5 vehicles.");
                    return;
                }

                sql = @"select *
                        from dbo.VehicleMakeModelYear 
                        where VehicleMakeModelYearId = @VehicleMakeModelYearId
                        and dateadd(day, 3, ReservedAt) > getdate() ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    reader.Close();
                    ShowError("This vehicle is already reserved.");

                    return;
                }
                reader.Close();

                sql = @"update dbo.VehicleMakeModelYear 
                        set ReservedBy=@ReservedBy, ReservedAt = getdate()
                        where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearIdHidden.Value);
                Cmd.Parameters.Add("@ReservedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                Cmd.ExecuteNonQuery();

                SearchInfo(0);
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }

        }
    }
}