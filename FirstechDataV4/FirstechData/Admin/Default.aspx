﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstechData.Admin.Default" %>

<%@ MasterType VirtualPath="~/Admin/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- breadcrumbs -->
    <ul class="breadcrumbs ">
        <li><a href="/"><span class="entypo-home"></span> Admin Home</a></li>
    </ul>
    <!-- end of breadcrumbs -->

    <div class="box">
        <div class="box-header bg-transparent">
            <div class="pull-right box-tools">
                <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i></span>
            </div>
            <h3 class="box-title"><i class="entypo-info"></i>
                <span></span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
        </div>
    </div>
</asp:Content>
