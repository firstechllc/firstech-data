﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData.Admin
{
    public partial class SearchCounter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

            if (roleNames.Contains("Rep"))
            {
                IdentityHelper.RedirectToReturnUrl("~/Admin/", Response);
            }

            if (!IsPostBack)
            {
                RoleNameHidden.Value = string.Join(",", roleNames);

                StartDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today.AddDays(-7));
                EndDate.Value = string.Format("{0:MM/dd/yyyy}", DateTime.Today);
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            ShowList();
        }

        private void ShowList()
        { 
            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                SqlCommand Cmd = new SqlCommand("proc_SearchCounterSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DateTime.Parse(StartDate.Value);
                Cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = DateTime.Parse(EndDate.Value).AddDays(1);
                Cmd.Parameters.Add("@Number", SqlDbType.Int).Value = int.Parse(TopList.SelectedValue);
                
                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CounterList.DataSource = ds;
                CounterList.DataBind();

                if (!RoleNameHidden.Value.Contains("Administrator") && !RoleNameHidden.Value.Contains("Editor"))
                {
                    CounterList.Columns[5].Visible = false;
                }
                if (!RoleNameHidden.Value.Contains("Administrator"))
                {
                    CounterList.Columns[6].Visible = false;
                    CounterList.Columns[7].Visible = false;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
            InfoLabel.Visible = false;
        }

        protected void CounterList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField VehicleMakeModelYearId = ((HiddenField)(e.Row.FindControl("VehicleMakeModelYearId")));
                HyperLink VehicleLink = ((HyperLink)(e.Row.FindControl("VehicleLink")));
                VehicleLink.NavigateUrl = "/Admin/AdminVehicleInfo?Id=" + VehicleMakeModelYearId.Value;

                if (e.Row.Cells[5].Controls.Count > 0)
                {
                    LinkButton ReserveButton = e.Row.Cells[5].Controls[0] as LinkButton;
                    if (ReserveButton != null)
                    {
                        HiddenField Reserved = ((HiddenField)(e.Row.FindControl("Reserved")));
                        if (Reserved != null && Reserved.Value != "")
                        {
                            if (RoleNameHidden.Value.Contains("Administrator"))
                            {
                                ReserveButton.Text = "Cancel";
                                ReserveButton.OnClientClick = "return confirm('Are you sure you want to cancel?');";
                            }
                            else
                            {
                                ReserveButton.Visible = false;
                            }
                        }
                        else
                        {
                            ReserveButton.Text = "Reserve";
                            ReserveButton.OnClientClick = "return confirm('Are you sure you want to reserve?');";
                        }
                    }
                }
            }
        }

        protected void CounterList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearError();

            HiddenField VehicleMakeModelYearId = ((HiddenField)(CounterList.SelectedRow.FindControl("VehicleMakeModelYearId")));
            LinkButton ReserveButton = CounterList.SelectedRow.Cells[5].Controls[0] as LinkButton;

            SqlConnection Con = null;
            try
            {
                ClearError();

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                if (ReserveButton.Visible)
                {
                    if (ReserveButton.Text == "Reserve")
                    {
                        string sql = @"select isnull(count(*), 0) as Cnt
                        from dbo.VehicleMakeModelYear 
                        where dateadd(day, 3, ReservedAt) > getdate() and ReservedBy=@ReservedBy";

                        SqlCommand Cmd = new SqlCommand(sql, Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.Parameters.Add("@ReservedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                        int ReserveCnt = 0;
                        SqlDataReader reader = Cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            ReserveCnt = int.Parse(reader["Cnt"].ToString());
                        }
                        reader.Close();

                        if (ReserveCnt >= 5)
                        {
                            ShowError("You already reserved 5 vehicles.");
                            return;
                        }

                        sql = @"select *
                        from dbo.VehicleMakeModelYear 
                        where VehicleMakeModelYearId = @VehicleMakeModelYearId
                        and dateadd(day, 3, ReservedAt) > getdate() ";

                        Cmd = new SqlCommand(sql, Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId.Value);

                        reader = Cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            reader.Close();
                            ShowList();
                            ShowError("This vehicle is already reserved.");

                            return;
                        }
                        reader.Close();

                        sql = @"update dbo.VehicleMakeModelYear 
                        set ReservedBy=@ReservedBy, ReservedAt = getdate()
                        where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                        Cmd = new SqlCommand(sql, Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId.Value);
                        Cmd.Parameters.Add("@ReservedBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();

                        Cmd.ExecuteNonQuery();
                    }
                    else if (RoleNameHidden.Value.Contains("Administrator") && ReserveButton.Text == "Cancel")
                    {
                        string sql = @"update dbo.VehicleMakeModelYear 
                        set ReservedBy = null, ReservedAt = null
                        where VehicleMakeModelYearId = @VehicleMakeModelYearId";

                        SqlCommand Cmd = new SqlCommand(sql, Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId.Value);

                        Cmd.ExecuteNonQuery();
                    }

                    ShowList();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void CounterList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int idx = Convert.ToInt32(e.CommandArgument);
            GridView List = (GridView)sender;

            string VehicleMakeModelYearId = ((HiddenField)(List.Rows[idx].FindControl("VehicleMakeModelYearId"))).Value;
            string SearchCount = ((TextBox)(List.Rows[idx].FindControl("SearchCountTxt"))).Text;
            int SearchCountInt;

            if (RoleNameHidden.Value.Contains("Administrator"))
            {
                if (e.CommandName == "ChangeSearchCount" && int.TryParse(SearchCount, out SearchCountInt))
                {
                    SqlConnection Con = null;
                    try
                    {
                        ClearError();

                        string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                        Con = new SqlConnection(connStr);
                        Con.Open();

                        string sql = @"update dbo.VehicleMakeModelYear set SearchCount=@SearchCount where VehicleMakeModelYearId=@VehicleMakeModelYearId";

                        SqlCommand Cmd = new SqlCommand(sql, Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.Parameters.Add("@SearchCount", SqlDbType.Int).Value = SearchCountInt;
                        Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = int.Parse(VehicleMakeModelYearId);

                        Cmd.ExecuteNonQuery();

                        ShowList();
                    }
                    catch (Exception ex)
                    {
                        ShowError(ex.ToString());
                    }
                    finally
                    {
                        if (Con != null)
                            Con.Close();
                    }
                }
            }
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                SqlCommand cmd = new SqlCommand("proc_SearchCounterSearch", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@StartDate", SqlDbType.NVarChar, 2000).Value = DateTime.Parse(StartDate.Value);
                cmd.Parameters.Add("@EndDate", SqlDbType.NVarChar, 2000).Value = DateTime.Parse(EndDate.Value).AddDays(1);
                cmd.Parameters.Add("@Number", SqlDbType.Int).Value = int.Parse(TopList.SelectedValue);

                SqlDataReader reader = cmd.ExecuteReader();
                StringBuilder excel = new StringBuilder();

                excel.AppendLine("No,Vehicle,Counter,Last Updated Date");
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                while (reader.Read())
                {
                    excel.Append(reader["Num"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Vehicle"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Counter"].ToString());
                    excel.Append(",");
                    excel.Append(reader["UpdatedDt"].ToString());
                    excel.AppendLine("");
                }

                reader.Close();
                conn.Close();

                string filename = "SearchCounter.csv";
                string content_type = "text/csv";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = content_type;
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.Charset = "";
                this.EnableViewState = false;
                Response.Write(excel.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }
    }
}