﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstechData.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FirstechData
{
    internal class RoleAction
    {
        internal static void CreateAdmin()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IdentityResult IdRoleResult;
            IdentityResult IdUserResult;

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (!roleManager.RoleExists("Administrator"))
            {
                IdRoleResult = roleManager.Create(new IdentityRole("Administrator"));
                if (!IdRoleResult.Succeeded)
                {

                }
            }
            if (!roleManager.RoleExists("Editor"))
            {
                IdRoleResult = roleManager.Create(new IdentityRole("Editor"));
                if (!IdRoleResult.Succeeded)
                {

                }
            }
            if (!roleManager.RoleExists("Rep"))
            {
                IdRoleResult = roleManager.Create(new IdentityRole("Rep"));
                if (!IdRoleResult.Succeeded)
                {

                }
            }
            if (!roleManager.RoleExists("Office"))
            {
                IdRoleResult = roleManager.Create(new IdentityRole("Office"));
                if (!IdRoleResult.Succeeded)
                {

                }
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            /*
            var app = userManager.Find("Admin", "Admin1@3");
            userManager.Delete(app);
            */

            var adminUser = userManager.FindByEmail("admin@email.com");
            if (adminUser == null)
            {
                var appUser = new ApplicationUser()
                {
                    UserName = "admin@email.com",
                    Email = "admin@email.com",
                    FirstName = "Admin",
                    LastName = "Admin",
                    Approved = true,
                };


                IdUserResult = userManager.Create(appUser, "Admin1@3");

                if (IdUserResult.Succeeded)
                {
                    IdUserResult = userManager.AddToRole(appUser.Id, "Administrator");
                    if (!IdUserResult.Succeeded)
                    {

                    }
                }
                else
                {

                }
            }
        }
    }
}