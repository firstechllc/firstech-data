﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowInfo();
                if (!User.Identity.IsAuthenticated)
                {
                    SubmitTipPanelAuth.Visible = false;
                    SubmitTipPanelNoAuth.Visible = true;
                }
                else
                {
                    SubmitTipPanelAuth.Visible = true;
                    SubmitTipPanelNoAuth.Visible = false;
                }
            }
        }

        private void ShowInfo()
        {
            SqlConnection Con = null;
            try
            {
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string sql = "select BannerId, BannerCaption, BannerImage, BannerImageMobile, BannerURL, SortOrder ";
                sql += "from dbo.Banner ";
                sql += "where BannerLocation = @BannerLocation ";
                sql += "order by SortOrder";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@BannerLocation", SqlDbType.Int).Value = 1;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CarouselList.DataSource = ds;
                CarouselList.DataBind();

                CarouselIndicatorList.DataSource = ds;
                CarouselIndicatorList.DataBind();

                CarouselList2.DataSource = ds;
                CarouselList2.DataBind();

                CarouselIndicatorList2.DataSource = ds;
                CarouselIndicatorList2.DataBind();

                sql = "proc_NewsTickerLoad";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                adp = new SqlDataAdapter(Cmd);
                ds = new DataSet();

                adp.Fill(ds, "List");

                NewsFeedList.DataSource = ds;
                NewsFeedList.DataBind();


                /*
                string sql = "select MainBannerCaption1, MainBannerImage1, MainBannerURL1, MainBannerCaption2, MainBannerImage2, MainBannerURL2, ";
                sql += "MainBannerCaption3, MainBannerImage3, MainBannerURL3, LeftBannerCaption, LeftBannerImage, LeftBannerURL, ";
                sql += "MiddleBannerCaption, MiddleBannerImage, MiddleBannerURL, RightBannerCaption, RightBannerImage, RightBannerURL ";
                sql += "from dbo.Homepage ";

                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["MainBannerCaption1"] != null)
                        MainBannerCaption1.Text = reader["MainBannerCaption1"].ToString();
                    if (reader["MainBannerURL1"] != null)
                        MainBannerLink1.NavigateUrl = reader["MainBannerURL1"].ToString();
                    if (reader["MainBannerImage1"] != null && reader["MainBannerImage1"].ToString() != "")
                    {
                        MainBannerImage1.ImageUrl = SaveFolder + reader["MainBannerImage1"].ToString();
                        MainBannerImage1.Visible = true;
                    }

                    if (reader["MainBannerCaption2"] != null)
                        MainBannerCaption2.Text = reader["MainBannerCaption2"].ToString();
                    else
                        MainBannerCaption2.Text = MainBannerCaption1.Text;
                    if (reader["MainBannerURL2"] != null)
                        MainBannerLink2.NavigateUrl = reader["MainBannerURL2"].ToString();
                    else
                        MainBannerLink2.NavigateUrl = MainBannerLink1.NavigateUrl;
                    if (reader["MainBannerImage2"] != null && reader["MainBannerImage2"].ToString() != "")
                    {
                        MainBannerImage2.ImageUrl = SaveFolder + reader["MainBannerImage2"].ToString();
                        MainBannerImage2.Visible = true;
                    }
                    else
                    {
                        MainBannerImage2.ImageUrl = MainBannerImage1.ImageUrl;
                        MainBannerImage2.Visible = true;
                    }

                    if (reader["MainBannerCaption3"] != null)
                        MainBannerCaption3.Text = reader["MainBannerCaption3"].ToString();
                    else
                        MainBannerCaption3.Text = MainBannerCaption1.Text;
                    if (reader["MainBannerURL3"] != null)
                        MainBannerLink3.NavigateUrl = reader["MainBannerURL3"].ToString();
                    else
                        MainBannerLink3.NavigateUrl = MainBannerLink1.NavigateUrl;
                    if (reader["MainBannerImage3"] != null && reader["MainBannerImage3"].ToString() != "")
                    {
                        MainBannerImage3.ImageUrl = SaveFolder + reader["MainBannerImage3"].ToString();
                        MainBannerImage3.Visible = true;
                    }
                    else
                    {
                        MainBannerImage3.ImageUrl = MainBannerImage1.ImageUrl;
                        MainBannerImage3.Visible = true;
                    }

                    if (reader["LeftBannerCaption"] != null)
                        LeftBannerCaption.Text = reader["LeftBannerCaption"].ToString();
                    if (reader["LeftBannerURL"] != null)
                        LeftAttachImage.PostBackUrl = reader["LeftBannerURL"].ToString();
                    if (reader["LeftBannerImage"] != null && reader["LeftBannerImage"].ToString() != "")
                    {
                        LeftAttachImage.ImageUrl = SaveFolder + reader["LeftBannerImage"].ToString();
                        LeftAttachImage.Visible = true;
                    }

                    if (reader["MiddleBannerCaption"] != null)
                        MiddleBannerCaption.Text = reader["MiddleBannerCaption"].ToString();
                    if (reader["MiddleBannerURL"] != null)
                        MiddleAttachImage.PostBackUrl = reader["MiddleBannerURL"].ToString();
                    if (reader["MiddleBannerImage"] != null && reader["MiddleBannerImage"].ToString() != "")
                    {
                        MiddleAttachImage.ImageUrl = SaveFolder + reader["MiddleBannerImage"].ToString();
                        MiddleAttachImage.Visible = true;
                    }

                    if (reader["RightBannerCaption"] != null)
                        RightBannerCaption.Text = reader["RightBannerCaption"].ToString();
                    if (reader["RightBannerURL"] != null)
                        RightAttachImage.PostBackUrl = reader["RightBannerURL"].ToString();
                    if (reader["RightBannerImage"] != null && reader["RightBannerImage"].ToString() != "")
                    {
                        RightAttachImage.ImageUrl = SaveFolder + reader["RightBannerImage"].ToString();
                        RightAttachImage.Visible = true;
                    }
                }
                reader.Close();
                */
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        private void ShowError(string error)
        {
            ErrorLabel.Text = error;
            ErrorLabel.Visible = true;
        }

        private void ClearError()
        {
            ErrorLabel.Visible = false;
        }

        protected void CarouselList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField BannerCaption = (HiddenField)(e.Item.FindControl("RatingLabel"));
                HiddenField BannerURL = (HiddenField)(e.Item.FindControl("BannerURL"));
                HiddenField BannerImage = (HiddenField)(e.Item.FindControl("BannerImage"));

                Label MainBannerCaption = (Label)(e.Item.FindControl("MainBannerCaption"));
                HyperLink MainBannerLink = (HyperLink)(e.Item.FindControl("MainBannerLink"));
                Image MainBannerImage = (Image)(e.Item.FindControl("MainBannerImage"));

                if (BannerCaption != null)
                    MainBannerCaption.Text = BannerCaption.Value;
                if (BannerURL != null)
                    MainBannerLink.NavigateUrl = BannerURL.Value;
                if (BannerImage != null && BannerImage.Value != "")
                {
                    MainBannerImage.ImageUrl = SaveFolderHidden.Value + BannerImage.Value;
                    MainBannerImage.Visible = true;
                }
            }
        }

        protected void CarouselList2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField BannerCaption = (HiddenField)(e.Item.FindControl("RatingLabel"));
                HiddenField BannerURL = (HiddenField)(e.Item.FindControl("BannerURL"));
                HiddenField BannerImageMobile = (HiddenField)(e.Item.FindControl("BannerImageMobile"));
                HiddenField BannerImage = (HiddenField)(e.Item.FindControl("BannerImage"));

                Label MainBannerCaption = (Label)(e.Item.FindControl("MainBannerCaption"));
                HyperLink MainBannerLink = (HyperLink)(e.Item.FindControl("MainBannerLink"));
                Image MainBannerImage = (Image)(e.Item.FindControl("MainBannerImage"));

                if (BannerCaption != null)
                    MainBannerCaption.Text = BannerCaption.Value;
                if (BannerURL != null)
                    MainBannerLink.NavigateUrl = BannerURL.Value;
                if (BannerImageMobile != null && BannerImageMobile.Value != "")
                {
                    MainBannerImage.ImageUrl = SaveFolderHidden.Value + BannerImageMobile.Value;
                    MainBannerImage.Visible = true;
                }
                else if (BannerImage != null && BannerImage.Value != "")
                {
                    MainBannerImage.ImageUrl = SaveFolderHidden.Value + BannerImage.Value;
                    MainBannerImage.Visible = true;
                }
            }
        }

        protected void NewsFeedList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField NewsfeedId = (HiddenField)(e.Item.FindControl("NewsfeedId"));
                HyperLink NewsLink = (HyperLink)(e.Item.FindControl("NewsLink"));
                HiddenField Loc = (HiddenField)(e.Item.FindControl("Loc"));

                //NewsLink.NavigateUrl = "javascript:openNewFeedPopup()";
                if (NewsfeedId != null)
                {
                    if (Loc.Value == "1")
                    {
                        NewsLink.NavigateUrl = "Newsfeed?Id=" + NewsfeedId.Value;
                    }
                    else if (Loc.Value == "2")
                    {
                        NewsLink.NavigateUrl = "Search/Search?Id=" + NewsfeedId.Value;
                    }
                }
            }
        }

        protected void TipSaveButton_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            string TipEmail = System.Configuration.ConfigurationManager.AppSettings["TipEmail"];
            var tipuser = manager.FindByEmail(TipEmail);

            string FileName = "";
            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
            if (TipFile.PostedFile != null && TipFile.PostedFile.FileName != "")
            {
                string FullPath = HttpContext.Current.Server.MapPath(SaveFolder);

                string Prefix = "Tip";
                string filepath = TipFile.PostedFile.FileName;
                FileName = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                FileName = Prefix + "_" + string.Format("{0:yyymmdd_HHmmss}", System.DateTime.Now) + "_" + FileName;
                FileName = FileName.Replace(" ", "");

                int IntFileSize = TipFile.PostedFile.ContentLength;
                byte[] myData = new byte[IntFileSize];
                TipFile.PostedFile.InputStream.Read(myData, 0, IntFileSize);

                FileStream newFile = new FileStream(FullPath + FileName, FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();
            }

            string subject = "Tip: " + TypeOfTips.SelectedValue;

            var user = manager.FindById(User.Identity.GetUserId());

            StringBuilder mailContent = new StringBuilder();
            mailContent.AppendLine("Tip: " + TypeOfTips.SelectedValue + "<br/>");
            mailContent.AppendLine("User: " + user.LastName + " " + user.FirstName + ", " + user.Email + "<br/>");
            mailContent.AppendLine("Vehicle Year: " + TipYear.Text + "<br/>");
            mailContent.AppendLine("Vehicle Make: " + TipMake.Text + "<br/>");
            mailContent.AppendLine("Vehicle Model: " + TipModel.Text + "<br/>");
            mailContent.AppendLine("Key Style: " + KeyStyleList.SelectedValue + "<br/>");
            mailContent.AppendLine("Description: " + TipDesc.Text + "<br/>");
            if (FileName != "")
            {
                string url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/";
                mailContent.AppendLine("File: <a href='" + url + SaveFolder.Replace("~/", "") + FileName + "'>" + FileName + "</a>");
            }

            try
            { 
                manager.SendEmail(tipuser.Id, subject, mailContent.ToString());
            }
            catch (Exception)
            {
                string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                var admin = manager.FindByEmail(AdminEmail);

                string title = "Sending Failure - " + subject;
                string content = "Sending Failure - " + tipuser.Email + "<br/>" + mailContent.ToString();
                manager.SendEmail(admin.Id, title, content);
            }

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "insert into dbo.SubmitTip (SubmitBy, SubmitDt, TypeOfTip, VehicleYear, VehicleMake, VehicleModel, KeyStyle, UpdatedFilepath, TipDescription) ";
                sql += "select @SubmitBy, getdate(), @TypeOfTip, @VehicleYear, @VehicleMake, @VehicleModel, @KeyStyle, @UpdatedFilepath, @TipDescription";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@SubmitBy", SqlDbType.NVarChar, 128).Value = User.Identity.GetUserId();
                cmd.Parameters.Add("@TypeOfTip", SqlDbType.VarChar, 20).Value = TypeOfTips.SelectedValue;
                cmd.Parameters.Add("@VehicleYear", SqlDbType.NVarChar, 20).Value = TipYear.Text;
                cmd.Parameters.Add("@VehicleMake", SqlDbType.NVarChar, 30).Value = TipMake.Text;
                cmd.Parameters.Add("@VehicleModel", SqlDbType.NVarChar, 50).Value = TipModel.Text;
                cmd.Parameters.Add("@KeyStyle", SqlDbType.NVarChar, 50).Value = KeyStyleList.SelectedValue;
                cmd.Parameters.Add("@UpdatedFilepath", SqlDbType.NVarChar, 500).Value = FileName;
                cmd.Parameters.Add("@TipDescription", SqlDbType.NVarChar, 4000).Value = TipDesc.Text;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}