﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disclaimer.aspx.cs" Inherits="FirstechData.Disclaimer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="box" style="margin-top:0px;">
        <div class="box-header bg-transparent">
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="entypo-phone"></i>
                <span style="color: black; font-size:medium"><b>DISCLAIMER</b></span>
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <asp:Label runat="server" ID="DisclaimerLabel"></asp:Label>
        </div>
    </div>
    </form>
</body>
</html>
