namespace FirstechData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Approved : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Approved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Approved");
        }
    }
}
