﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="NewsfeedPopup.aspx.cs" Inherits="FirstechData.NewsfeedPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/Scripts/css/foundation.css" />

    <!-- Custom styles for this template -->

    <link rel="stylesheet" href="/Scripts/css/dashboard.css" />
    <link rel="stylesheet" href="/Scripts/css/style.css" />
    <link rel="stylesheet" href="/Scripts/css/dripicon.css" />
    <link rel="stylesheet" href="/Scripts/css/typicons.css" />
    <link rel="stylesheet" href="/Scripts/css/font-awesome.css" />
    <link rel="stylesheet" href="/Scripts/css/theme.css" />

    <link href="/Content/blueimp-gallery.css" rel="stylesheet" />
    <link href="/Scripts/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="/Scripts/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="AdminPanel" runat="server">
        <ContentTemplate>
            <div class="row" style="margin-top: -20px">
                <div class="large-12 columns">
                    <div class="box">
                        <div class="box-header bg-transparent">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                            </div>
                            <h3 class="box-title"><i class="icon-document"></i>
                                <span style="color: black; font-size: medium"><b>Latest Updates/News</b></span>
                            </h3>
                        </div>
                        <div class="box-body " style="display: block;">
                            <div class="row">
                                <div class="large-12 columns">
                                    <asp:ListView runat="server" ID="NewsFeedList" OnItemDataBound="NewsFeedList_ItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="large-12 columns">
                                                    <article class="reading-nest">
                                                        <div class="row">
                                                            <div class="large-6 small-6 columns">
                                                                <b><asp:HyperLink runat="server" Target="_blank" ID="NewsLink" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ></asp:HyperLink></b><asp:HiddenField runat="server" ID="NewsfeedId" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' ></asp:HiddenField>
                                                            </div>
                                                            <div class="large-3 small-3 columns">
                                                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UpdateType") %>' ></asp:Label>
                                                            </div>
                                                            <div class="large-3 small-3 columns">
                                                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UpdateDt") %>' ></asp:Label><asp:HiddenField ID="Loc" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Loc") %>' ></asp:HiddenField>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="SaveFolderHidden" />
            <asp:HiddenField runat="server" ID="FullPathHidden" />
            <asp:HiddenField runat="server" ID="NewsfeedId" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>