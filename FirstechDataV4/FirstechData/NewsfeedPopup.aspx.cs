﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class NewsfeedPopup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SaveFolderHidden.Value = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];
                FullPathHidden.Value = HttpContext.Current.Server.MapPath(SaveFolderHidden.Value);

                int NewsfeedIdInt = 0;
                if (Request["Id"] != null && int.TryParse(Request["Id"], out NewsfeedIdInt))
                {
                    NewsfeedId.Value = Request["Id"];
                }
                ShowNewsFeed();
            }
        }

        private void ShowNewsFeed()
        {
            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                int NewsfeedIdInt = 0;
                if (NewsfeedId.Value != "" && int.TryParse(NewsfeedId.Value, out NewsfeedIdInt))
                {
                    NewsfeedIdInt = int.Parse(NewsfeedId.Value);
                }

                SqlCommand Cmd = new SqlCommand("proc_NewsTickerTop50Load", Con);
                Cmd.CommandType = CommandType.Text;

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                NewsFeedList.DataSource = ds;
                NewsFeedList.DataBind();
            }
            catch (Exception ex)
            {
                //ShowError(ex.ToString());
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }

        protected void NewsFeedList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField NewsfeedId = (HiddenField)(e.Item.FindControl("NewsfeedId"));
                HyperLink NewsLink = (HyperLink)(e.Item.FindControl("NewsLink"));
                HiddenField Loc = (HiddenField)(e.Item.FindControl("Loc"));

                if (NewsfeedId != null)
                {
                    if (Loc.Value == "1")
                    {
                        NewsLink.NavigateUrl = "Newsfeed?Id=" + NewsfeedId.Value;
                    }
                    else if (Loc.Value == "2")
                    {
                        NewsLink.NavigateUrl = "Search/Search?Id=" + NewsfeedId.Value;
                    }
                }
            }
        }
    }
}