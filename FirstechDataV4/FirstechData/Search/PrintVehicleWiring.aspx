﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintVehicleWiring.aspx.cs" Inherits="FirstechData.PrintVehicleWiring" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/Scripts/css/foundation.css" />

    <!-- Custom styles for this template -->

    <link rel="stylesheet" href="/Scripts/css/dashboard.css" />
    <link rel="stylesheet" href="/Scripts/css/style.css" />
    <link rel="stylesheet" href="/Scripts/css/dripicon.css" />
    <link rel="stylesheet" href="/Scripts/css/typicons.css" />
    <link rel="stylesheet" href="/Scripts/css/font-awesome.css" />
    <link rel="stylesheet" href="/Scripts/css/theme.css" />
    <style>
        table tr th, table tr td {
            padding: 3px 5px 3px 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row" style="padding:1px 1px 1px 1px">
                <div class="large-6 columns">
                    <h3><asp:Literal runat="server" ID="TitleLabel"></asp:Literal></h3>
                </div>
                <div class="large-6 columns right" style="height:30px;">
                    <img src="/Content/Images/firstechDataPrint.jpg" style="height:50px;"/>
                </div>
            </div>
                <hr />
            <div class="row">
                <h5>Vehicle Wiring</h5>
            </div>
            <div class="row">
                <asp:Literal runat="server" ID="NoteLabel"></asp:Literal>
            </div>
            <div class="row">
                <asp:Literal runat="server" ID="InstallationType"></asp:Literal>
            </div>
            <div class="row">
                <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="WireFunctionName" HeaderText="Wire Function" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px"  />
                        <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                        <asp:BoundField DataField="Polarity" HeaderText="Polarity" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    </Columns>
                </asp:GridView>
            </div>
                <hr />
            <div class="row">
            </div>
        </div>
    </form>
</body>
</html>
