﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class PrintVehicleWiring : System.Web.UI.Page
    {
        HashSet<string> AllowedIPs = new HashSet<string>()
        {
            "50.149.87.214",
            "198.22.123.14",
            "198.22.123.79",
            "198.22.123.103",
            "198.22.123.104",
            "198.22.123.105",
            "198.22.123.108",
            "198.22.123.109",
            "198.22.122.4",
            "70.60.1.174",
            "199.60.113.30",
        };

        IPAddressRange IPRange1 = new IPAddressRange(IPAddress.Parse("198.22.122.0"), IPAddress.Parse("198.22.122.24"));
        IPAddressRange IPRange2 = new IPAddressRange(IPAddress.Parse("168.94.245.0"), IPAddress.Parse("168.94.245.24"));
        IPAddressRange IPRange3 = new IPAddressRange(IPAddress.Parse("168.94.239.0"), IPAddress.Parse("168.94.239.24"));

        protected void Page_Load(object sender, EventArgs e)
        {
            string ipAddress = IPAddressRange.GetIPAddress();
            IPAddress ipaddr = IPAddress.Parse(ipAddress);

            if (!AllowedIPs.Contains(ipAddress) && !IPRange1.IsInRange(ipaddr) && !IPRange2.IsInRange(ipaddr) && !IPRange3.IsInRange(ipaddr) && !User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FSearch");
            }

            SqlConnection Con = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                Con = new SqlConnection(connStr);
                Con.Open();

                string VehicleName = "";

                string sql = "select VehicleMakeName from dbo.VehicleMake where VehicleMakeId = @VehicleMakeId ";
                SqlCommand Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(Request["Make"].ToString());
                SqlDataReader reader = Cmd.ExecuteReader();
                if(reader.Read())
                {
                    VehicleName += reader["VehicleMakeName"].ToString() + " ";
                }
                reader.Close();

                sql = "select VehicleModelName from dbo.VehicleModel where VehicleModelId = @VehicleModelId ";
                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(Request["Model"].ToString());
                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    VehicleName += reader["VehicleModelName"].ToString() + " ";
                }
                reader.Close();

                VehicleName += Request["Year"].ToString();
                TitleLabel.Text = VehicleName;
                Page.Title = VehicleName + " Vehicle Wiring";


                sql = "select b.VehicleMakeModelYearId from dbo.VehicleMakeModelYear b WITH (NOLOCK) ";
                sql += "where b.VehicleMakeId = @VehicleMakeId ";
                sql += " and b.VehicleModelId = @VehicleModelId ";
                sql += " and b.VehicleYear = @VehicleYear ";

                int VehicleMakeModelYearId = 0;

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeId", SqlDbType.Int).Value = int.Parse(Request["Make"].ToString());
                Cmd.Parameters.Add("@VehicleModelId", SqlDbType.Int).Value = int.Parse(Request["Model"].ToString());
                Cmd.Parameters.Add("@VehicleYear", SqlDbType.Int).Value = int.Parse(Request["Year"].ToString());

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["VehicleMakeModelYearId"] != null && int.TryParse(reader["VehicleMakeModelYearId"].ToString(), out VehicleMakeModelYearId))
                    {
                        VehicleMakeModelYearId = int.Parse(reader["VehicleMakeModelYearId"].ToString());
                    }
                }
                reader.Close();


                sql = "select a.Note ";
                sql += "from dbo.VehicleWireNote a ";
                sql += "where a.VehicleMakeModelYearId = @VehicleMakeModelYearId and InstallationTypeId=@InstallationTypeId ";

                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = VehicleMakeModelYearId;
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(Request["InstId"].ToString());

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    NoteLabel.Text = reader["Note"].ToString() + " ";
                }
                reader.Close();

                string InstallationTypeName = "";
                sql = "select InstallationTypeName from InstallationType WITH (NOLOCK) where InstallationTypeId = @InstallationTypeId";
                Cmd = new SqlCommand(sql, Con);
                Cmd.CommandType = CommandType.Text;

                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(Request["InstId"].ToString());

                reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    InstallationTypeName = "Install Type: " + reader["InstallationTypeName"].ToString() + " ";
                }
                reader.Close();

                InstallationType.Text = InstallationTypeName;


                Cmd = new SqlCommand("proc_VehicleWireSearch", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@SaveFolder", SqlDbType.NVarChar, 500).Value = "";
                Cmd.Parameters.Add("@VehicleMakeModelYearId", SqlDbType.Int).Value = VehicleMakeModelYearId;
                Cmd.Parameters.Add("@InstallationTypeId", SqlDbType.Int).Value = int.Parse(Request["InstId"].ToString());

                SqlDataAdapter adp = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                WireList.DataSource = ds;
                WireList.DataBind();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (Con != null)
                    Con.Close();
            }
        }
    }
}