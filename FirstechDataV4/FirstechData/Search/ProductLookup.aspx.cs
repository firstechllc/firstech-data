﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class ProductLookup : System.Web.UI.Page
    {
        public string documenturl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FProductLookup");
                }

                Master.ChangeMenuCss("ProductLookupMenu");

                int Id = 0;
                if (Request["Id"] != null && int.TryParse(Request["Id"].ToString(), out Id))
                {
                    ShowProduct(Id);
                }


                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

                if (roleNames.Contains("Administrator"))
                {
                    AdminFccPanel1.Visible = true;
                    AdminFccPanel2.Visible = true;
                }
                else
                {
                    AdminFccPanel1.Visible = false;
                    AdminFccPanel2.Visible = false;
                }
            }
        }

        private void ShowError(string e)
        {
            ErrorMsg.Text = e;
            ErrorMsg.Visible = true;
        }

        private void ClearError()
        {
            ErrorMsg.Visible = false;
        }

        protected void ProductSearchButton_Click(object sender, EventArgs e)
        {
            ClearError();

            if (ProductSearch.Text.Trim() == "")
            {
                ShowError("Please enter Model Number or Part Number");
                return;
            }

            int ProductId = 0;

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select ProductId, ModelNumber, PartNumber
                    from dbo.Product a
                    join dbo.ProductType pt on a.ProductTypeId = pt.ProductTypeId
                    where ModelNumber like '%' + @Search + '%' 
                        or PartNumber like '%' + @Search + '%' 
                        or FccIdUSA like '%' + @Search + '%' 
                        or FccIdCanada like '%' + @Search + '%' 
                        or exists (select * from dbo.ProductAlternateName n where n.ProductId = a.ProductId and n.ProductAlternateName like '%' + @Search + '%')
                    order by pt.SortOrder
                ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@Search", SqlDbType.NVarChar, 100).Value = ProductSearch.Text.Trim();

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                adp.Fill(ds, "List");

                CloseWindow();

                if (ds.Tables[0].Rows.Count == 1)
                {
                    ProductId = int.Parse(ds.Tables[0].Rows[0]["ProductId"].ToString());
                }
                else if (ds.Tables[0].Rows.Count > 1)
                {
                    SearchList.DataSource = ds;
                    SearchList.DataBind();

                    SearchListPanel.Visible = true;
                }
                else
                {
                    ShowError("There's no product to match.");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

            if (ProductId > 0)
            {
                ShowProduct(ProductId);
            }
        }

        private void ShowProduct(int ProductId)
        { 
            ProductIdHidden.Value = ProductId.ToString();
            string SaveFolder = System.Configuration.ConfigurationManager.AppSettings["AttachmentFolder"];

            SqlConnection conn = null;
            try
            {
                string connStr = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = @"select ProductId, ProductTypeId, Picture1=isnull(Picture1, ''), ModelNumber, PartNumber, DocumentURL,
                        WayRemote, TwoWayAntenna, OneWayAntenna, EstimatedRangeFt, FccIdUSA, FccIdCanada, RequiredAntennaCable,
                        [Status], OperatingVoltage, BladeCompatible, DroneCompatible, AvailableForWarranty, IdleCurrent, Dataport, 
                        OperatingTempF, OperatingTempC, AntennaPort4Pin, AntennaPort6Pin, WaterResistant, Battery, EstimatedBatteryLifeDays
                    from dbo.Product a
                    where ProductId = @ProductId
                ";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = ProductId;

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    ProductTypeIdHidden.Value = reader["ProductTypeId"].ToString();
                    ProductIdHidden.Value = reader["ProductId"].ToString();

                    if (ProductSearch.Text.Trim() == "")
                    {
                        ProductSearch.Text = reader["PartNumber"].ToString();
                    }

                    if (ProductTypeIdHidden.Value == "1")
                    {
                        if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        {
                            ProductDocumentButton.OnClientClick = "OpenDocument('" + reader["DocumentURL"].ToString() + "')";
                            ProductDocumentButton.Visible = true;
                        }
                        else
                        {
                            ProductDocumentButton.OnClientClick = null;
                            ProductDocumentButton.Visible = false;
                        }

                        BrainModelNumber.Text = reader["ModelNumber"].ToString();
                        BrainPartNumber.Text = reader["PartNumber"].ToString();
                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductPictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductPictureView.Visible = true;
                        }
                        else
                        {
                            ProductPictureView.Visible = false;
                        }

                        /*
                        if (reader["TwoWayAntenna"] != null)
                        {
                            BrainTwoWayAntenna.Text = reader["TwoWayAntenna"].ToString();
                        }
                        if (reader["OneWayAntenna"] != null)
                        {
                            BrainOneWayAntenna.Text = reader["OneWayAntenna"].ToString();
                        }
                        */
                        if (reader["Status"] != null)
                        {
                            BrainStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            BrainAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["BladeCompatible"] != null)
                        {
                            BrainBladeCompatible.Text = reader["BladeCompatible"].ToString();
                        }
                        if (reader["DroneCompatible"] != null)
                        {
                            BrainDroneCompatible.Text = reader["DroneCompatible"].ToString();
                        }
                        if (reader["Dataport"] != null)
                        {
                            BrainDataport.Text = reader["Dataport"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            BrainOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["IdleCurrent"] != null)
                        {
                            BrainIdleCurrent.Text = reader["IdleCurrent"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            BrainOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            BrainOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                        if (reader["WaterResistant"] != null)
                        {
                            BrainWaterResistant.Text = reader["WaterResistant"].ToString();
                        }
                    }
                    else if (ProductTypeIdHidden.Value == "2")
                    {
                        if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        {
                            RemoteDocumentButton.OnClientClick = "OpenDocument('" + reader["DocumentURL"].ToString() + "')";
                            RemoteDocumentButton.Visible = true;
                        }
                        else
                        {
                            RemoteDocumentButton.OnClientClick = null;
                            RemoteDocumentButton.Visible = false;
                        }

                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductRemotePictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductRemotePictureView.Visible = true;
                        }
                        else
                        {
                            ProductRemotePictureView.Visible = false;
                        }
                        RemoteModelNumber.Text = reader["ModelNumber"].ToString();
                        RemotePartNumber.Text = reader["PartNumber"].ToString();

                        if (reader["TwoWayAntenna"] != null)
                        {
                            RemoteTwoWayAntenna.Text = reader["TwoWayAntenna"].ToString();
                        }
                        if (reader["OneWayAntenna"] != null)
                        {
                            RemoteOneWayAntenna.Text = reader["OneWayAntenna"].ToString();
                        }
                        if (reader["FccIdUSA"] != null)
                        {
                            RemoteFccIdUSA.Text = reader["FccIdUSA"].ToString();
                        }
                        if (reader["FccIdCanada"] != null)
                        {
                            RemoteFccIdCanada.Text = reader["FccIdCanada"].ToString();
                        }
                        if (reader["Status"] != null)
                        {
                            RemoteStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            RemoteAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["EstimatedRangeFt"] != null)
                        {
                            RemoteEstimatedRange.Text = reader["EstimatedRangeFt"].ToString();
                        }
                        if (reader["Battery"] != null)
                        {
                            RemoteBattery.Text = reader["Battery"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            RemoteOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["EstimatedBatteryLifeDays"] != null)
                        {
                            RemoteEstimatedLife.Text = reader["EstimatedBatteryLifeDays"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            RemoteOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            RemoteOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                        if (reader["WaterResistant"] != null)
                        {
                            RemoteWaterResistant.Text = reader["WaterResistant"].ToString();
                        }
                    }
                    else if (ProductTypeIdHidden.Value == "3")
                    {
                        if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        {
                            AntennaDocumentButton.OnClientClick = "OpenDocument('" + reader["DocumentURL"].ToString() + "')";
                            AntennaDocumentButton.Visible = true;
                        }
                        else
                        {
                            AntennaDocumentButton.OnClientClick = null;
                            AntennaDocumentButton.Visible = false;
                        }

                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductAntennaPictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductAntennaPictureView.Visible = true;
                        }
                        else
                        {
                            ProductAntennaPictureView.Visible = false;
                        }
                        AntennaModelNumber.Text = reader["ModelNumber"].ToString();
                        AntennaPartNumber.Text = reader["PartNumber"].ToString();

                        if (reader["RequiredAntennaCable"] != null)
                        {
                            AntennaRequiredCable.Text = reader["RequiredAntennaCable"].ToString();
                        }
                        if (reader["FccIdUSA"] != null)
                        {
                            AntennaFccIdUSA.Text = reader["FccIdUSA"].ToString();
                        }
                        if (reader["FccIdCanada"] != null)
                        {
                            AntennaFccIdCanada.Text = reader["FccIdCanada"].ToString();
                        }
                        if (reader["Status"] != null)
                        {
                            AntennaStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            AntennaAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            AntennaOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            AntennaOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            AntennaOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                        if (reader["IdleCurrent"] != null)
                        {
                            AntennaIdlCurrent.Text = reader["IdleCurrent"].ToString();
                        }
                    }
                    else if (ProductTypeIdHidden.Value == "4")
                    {
                        if (reader["DocumentURL"] != null && reader["DocumentURL"].ToString() != "")
                        {
                            AccessoriesDocumentButton.OnClientClick = "OpenDocument('" + reader["DocumentURL"].ToString() + "')";
                            AccessoriesDocumentButton.Visible = true;
                        }
                        else
                        {
                            AccessoriesDocumentButton.OnClientClick = null;
                            AccessoriesDocumentButton.Visible = false;
                        }
                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductAccessoriesPictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductAccessoriesPictureView.Visible = true;
                        }
                        else
                        {
                            ProductAccessoriesPictureView.Visible = false;
                        }
                        AccessoriesModelNumber.Text = reader["ModelNumber"].ToString();
                        AccessoriesPartNumber.Text = reader["PartNumber"].ToString();

                        if (reader["Status"] != null)
                        {
                            AccessoriesStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            AccessoriesAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            AccessoriesOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            AccessoriesOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            AccessoriesOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                    }
                    // Packages
                    else if (ProductTypeIdHidden.Value == "5")
                    {
                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductPackagePictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductPackagePictureView.Visible = true;
                        }
                        else
                        {
                            ProductPackagePictureView.Visible = false;
                        }
                        PackageModelNumber.Text = reader["ModelNumber"].ToString();
                        PackagePartNumber.Text = reader["PartNumber"].ToString();

                        if (reader["Status"] != null)
                        {
                            PackageStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            PackageAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            PackageOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            PackageOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            PackageOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                    }
                    // Drone
                    else if (ProductTypeIdHidden.Value == "6")
                    {
                        if (reader["Picture1"].ToString() != "")
                        {
                            ProductDronePictureView.ImageUrl = SaveFolder + reader["Picture1"].ToString();
                            ProductDronePictureView.Visible = true;
                        }
                        else
                        {
                            ProductDronePictureView.Visible = false;
                        }
                        DroneModelNumber.Text = reader["ModelNumber"].ToString();
                        DronePartNumber.Text = reader["PartNumber"].ToString();

                        if (reader["Status"] != null)
                        {
                            DroneStatus.Text = reader["Status"].ToString();
                        }
                        if (reader["AvailableForWarranty"] != null)
                        {
                            DroneAvailableForWarranty.Text = reader["AvailableForWarranty"].ToString();
                        }
                        if (reader["OperatingVoltage"] != null)
                        {
                            DroneOperatingVoltage.Text = reader["OperatingVoltage"].ToString();
                        }
                        if (reader["OperatingTempF"] != null)
                        {
                            DroneOperatingTempF.Text = reader["OperatingTempF"].ToString();
                        }
                        if (reader["OperatingTempC"] != null)
                        {
                            DroneOperatingTempC.Text = reader["OperatingTempC"].ToString();
                        }
                        if (reader["WaterResistant"] != null)
                        {
                            DroneWaterResistant.Text = reader["WaterResistant"].ToString();
                        }
                    }
                }
                reader.Close();

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                IList<string> roleNames = manager.GetRoles(Context.User.Identity.GetUserId());

                if (ProductIdHidden.Value != "")
                {
                    // Control Brain
                    if (ProductTypeIdHidden.Value == "1")
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainReplacementPartList.DataSource = ds;
                        BrainReplacementPartList.DataBind();

                        sql = @"select a.ProductBrainConvenienceId, a.ProductBrainConvenienceName 
                            from dbo.ProductBrainConvenience a with (nolock) 
                            join dbo.ProductProductBrainConvenience b with (nolock) on a.ProductBrainConvenienceId=b.ProductBrainConvenienceId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainConvenienceList.DataSource = ds;
                        BrainConvenienceList.DataBind();

                        sql = @"select a.ProductBrainAlarmId, a.ProductBrainAlarmName 
                            from dbo.ProductBrainAlarm a with (nolock) 
                            join dbo.ProductProductBrainAlarm b with (nolock) on a.ProductBrainAlarmId=b.ProductBrainAlarmId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainAlarmList.DataSource = ds;
                        BrainAlarmList.DataBind();

                        sql = @"select a.ProductBrainRemoteStartId, a.ProductBrainRemoteStartName 
                            from dbo.ProductBrainRemoteStart a with (nolock) 
                            join dbo.ProductProductBrainRemoteStart b with (nolock) on a.ProductBrainRemoteStartId=b.ProductBrainRemoteStartId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainRemoteStartList.DataSource = ds;
                        BrainRemoteStartList.DataBind();

                        sql = @"select a.ProductBrainInstallationId, a.ProductBrainInstallationName 
                            from dbo.ProductBrainInstallation a with (nolock) 
                            join dbo.ProductProductBrainInstallation b with (nolock) on a.ProductBrainInstallationId=b.ProductBrainInstallationId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainInstallationList.DataSource = ds;
                        BrainInstallationList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 2 
                                and a.WayRemote = '2Way'
                                and a.[Status] = 'Current'
                             ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        TwoWayRemoteList.DataSource = ds;
                        TwoWayRemoteList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 2 
                                and a.WayRemote = '2Way'
                                and a.[Status] = 'Discontinued'
                             ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        TwoWayRemoteDisList.DataSource = ds;
                        TwoWayRemoteDisList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 2 
                                and a.WayRemote = '1Way'
                                and a.[Status] = 'Current'
                             ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        OneWayRemoteList.DataSource = ds;
                        OneWayRemoteList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 2 
                                and a.WayRemote = '1Way'
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        OneWayRemoteDisList.DataSource = ds;
                        OneWayRemoteDisList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Drone
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 6
                                and a.[Status] = 'Current'
                             ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        DroneCurrentList.DataSource = ds;
                        DroneCurrentList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Drone
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 6
                                and a.[Status] = 'Discontinued'
                             ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        DroneDisList.DataSource = ds;
                        DroneDisList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Accessories
                            where b.ProductId_Brain = @ProductId
                                and a.ProductTypeId = 4
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        BrainAccessoriesList.DataSource = ds;
                        BrainAccessoriesList.DataBind();
                    }
                    if (ProductTypeIdHidden.Value == "2")
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteReplacementPartList.DataSource = ds;
                        RemoteReplacementPartList.DataBind();

                        sql = @"select a.ProductRemoteConvenienceId, a.ProductRemoteConvenienceName 
                            from dbo.ProductRemoteConvenience a with (nolock) 
                            join dbo.ProductProductRemoteConvenience b with (nolock) on a.ProductRemoteConvenienceId=b.ProductRemoteConvenienceId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteConvenienceList.DataSource = ds;
                        RemoteConvenienceList.DataBind();

                        sql = @"select a.ProductRemoteProgrammableId, a.ProductRemoteProgrammableName 
                            from dbo.ProductRemoteProgrammable a with (nolock) 
                            join dbo.ProductProductRemoteProgrammable b with (nolock) on a.ProductRemoteProgrammableId=b.ProductRemoteProgrammableId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteProgrammingList.DataSource = ds;
                        RemoteProgrammingList.DataBind();

                        sql = @"select a.ProductRemoteAuxiliaryId, a.ProductRemoteAuxiliaryName 
                            from dbo.ProductRemoteAuxiliary a with (nolock) 
                            join dbo.ProductProductRemoteAuxiliary b with (nolock) on a.ProductRemoteAuxiliaryId=b.ProductRemoteAuxiliaryId
                            where b.ProductId = @ProductId
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteAuxiliaryList.DataSource = ds;
                        RemoteAuxiliaryList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Remote = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteCompBrainCurrentList.DataSource = ds;
                        RemoteCompBrainCurrentList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleRemote b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Remote = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteCompBrainDisList.DataSource = ds;
                        RemoteCompBrainDisList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Antenna
                            where b.ProductId_Remote = @ProductId
                                and a.ProductTypeId = 3
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteAntennaCurrentList.DataSource = ds;
                        RemoteAntennaCurrentList.DataBind();

                        

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Antenna
                            where b.ProductId_Remote = @ProductId
                                and a.ProductTypeId = 3
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteAntennaDisList.DataSource = ds;
                        RemoteAntennaDisList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductRemoteCompanion b with (nolock) on a.ProductId=b.ProductId_Companion
                            where b.ProductId_Remote = @ProductId
                                and a.ProductTypeId = 2
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        RemoteCompanionList.DataSource = ds;
                        RemoteCompanionList.DataBind();
                    }
                    else if (ProductTypeIdHidden.Value == "3")
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        AntennaReplacementPartList.DataSource = ds;
                        AntennaReplacementPartList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Antenna = @ProductId
                                and a.ProductTypeId = 2
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AntennaCompRemoteCurrentList.DataSource = ds;
                        AntennaCompRemoteCurrentList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductRemoteCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Antenna = @ProductId
                                and a.ProductTypeId = 2
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AntennaCompRemoteDisList.DataSource = ds;
                        AntennaCompRemoteDisList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Antenna = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AntennaCompBrainCurrentList.DataSource = ds;
                        AntennaCompBrainCurrentList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleAntenna b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Antenna = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AntennaCompBrainDispList.DataSource = ds;
                        AntennaCompBrainDispList.DataBind();

                    }
                    else if (ProductTypeIdHidden.Value == "4")
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        AccessoriesReplacementPartList.DataSource = ds;
                        AccessoriesReplacementPartList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Accessories = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AccessoriesCompBrainCurrentList.DataSource = ds;
                        AccessoriesCompBrainCurrentList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleAccessories b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Accessories = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        AccessoriesCompBrainDispList.DataSource = ds;
                        AccessoriesCompBrainDispList.DataBind();
                    }
                    // Packages
                    else if (ProductTypeIdHidden.Value == "5")
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        PackageReplacementPartList.DataSource = ds;
                        PackageReplacementPartList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductPackageIncludeRemote b with (nolock) on a.ProductId=b.ProductId_Remote
                            where b.ProductId_Package = @ProductId
                                and a.ProductTypeId = 2
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        PackageRemoteList.DataSource = ds;
                        PackageRemoteList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductPackageIncludeBrain b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Package = @ProductId
                                and a.ProductTypeId = 1
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        PackageBrainList.DataSource = ds;
                        PackageBrainList.DataBind();
                    }
                    if (ProductTypeIdHidden.Value == "6") // Drone
                    {
                        sql = @"select a.ProductId, a.PartNumber 
                        from dbo.Product a with (nolock) 
                        join dbo.ProductReplacementPartNumber b with (nolock) on a.ProductId=b.ProductId_Replacement
                        where b.ProductId = @ProductId
                        ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        DroneReplacementPartList.DataSource = ds;
                        DroneReplacementPartList.DataBind();


                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Drone = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Current'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        DroneCompBrainCurrentList.DataSource = ds;
                        DroneCompBrainCurrentList.DataBind();

                        sql = @"select a.ProductId, a.PartNumber 
                            from dbo.Product a with (nolock) 
                            join dbo.ProductBrainCompatibleDrone b with (nolock) on a.ProductId=b.ProductId_Brain
                            where b.ProductId_Drone = @ProductId
                                and a.ProductTypeId = 1
                                and a.[Status] = 'Discontinued'
                            ";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        adp = new SqlDataAdapter(cmd);
                        ds = new DataSet();

                        adp.Fill(ds, "List");

                        DroneCompBrainDisList.DataSource = ds;
                        DroneCompBrainDisList.DataBind();
                    }

                    if (roleNames.Contains("Administrator") || roleNames.Contains("Office"))
                    {
                        sql = @"select a.ProductWikiSectionId, a.ProductWikiSectionDetailId, c.ProductWikiSectionTitle, a.ProductWikiSectionInfo
                        from dbo.ProductWikiSectionDetail a
                        join dbo.ProductWikiSection c on a.ProductWikiSectionId = c.ProductWikiSectionId
                        where c.ProductId=@ProductId 
                            and a.Approved=1
                        order by c.SortOrder";

                        cmd = new SqlCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = int.Parse(ProductIdHidden.Value);

                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        adp.Fill(ds, "List");

                        ProductWikiList.DataSource = ds;
                        ProductWikiList.DataBind();
                    }
                }



                CloseWindow();
                if (ProductIdHidden.Value == "")
                {
                    ShowError("Can't find product");
                    return;
                }
                else
                {
                    if (ProductTypeIdHidden.Value == "1")
                    {
                        MainBrainPanel.Visible = true;
                    }
                    else if (ProductTypeIdHidden.Value == "2")
                    {
                        MainRemotePanel.Visible = true;
                    }
                    else if (ProductTypeIdHidden.Value == "3")
                    {
                        MainAntennaPanel.Visible = true;
                    }
                    else if (ProductTypeIdHidden.Value == "4")
                    {
                        MainAccessoriesPanel.Visible = true;
                    }
                    else if (ProductTypeIdHidden.Value == "5")
                    {
                        MainPackagePanel.Visible = true;
                    }
                    else if (ProductTypeIdHidden.Value == "6")
                    {
                        MainDronePanel.Visible = true;
                    }

                    if (roleNames.Contains("Administrator") || roleNames.Contains("Office"))
                    {
                        MainWikiPanel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void CloseWindow()
        {
            SearchListPanel.Visible = false;
            MainBrainPanel.Visible = false;
            MainRemotePanel.Visible = false;
            MainAntennaPanel.Visible = false;
            MainAccessoriesPanel.Visible = false;
            MainPackagePanel.Visible = false;
            MainDronePanel.Visible = false;
            MainWikiPanel.Visible = false;
        }

        protected void SearchListList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink link = e.Row.FindControl("Link") as HyperLink;
                if (link != null)
                {
                    string ProductId = ((HiddenField)e.Row.FindControl("ProductId")).Value;
                    string ModelNumber = ((HiddenField)e.Row.FindControl("ModelNumber")).Value;

                    link.Text = ModelNumber;
                    link.NavigateUrl = "ProductLookup?Id=" + ProductId;
                }
            }
        }

        protected void ProductDocumentButton_Click(object sender, EventArgs e)
        {

        }
    }
}