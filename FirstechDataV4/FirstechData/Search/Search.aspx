﻿<%@ Page Title="Search By Vehicle" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="FirstechData.Search" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .installtypestyle {
            background-color: #EFEFEF;
            color: #000000;
        }
        .installtypestyle:hover {
            background-color: #EFEFEF;
            color: #000000;
        }
        .thumbbutton {
            width: 18px;
            height: 18px;
            color: black;
        }
        tr.topborder td {
            border-left: none 0 #000000; 
            border-right: none 0 #000000; 
            border-bottom: none 0 #000000; 
            border-top: solid 1px #dfdfdf; 
            padding-top:15px;
            padding-bottom: 10px;
            background-color: #ffffff;
        }

        .maxWidth {
            max-width: 150px;
        }

        .tabs .tab-title > a { border-radius: 7px 7px 0px 0px; border: 2px solid #e1e1e1; height: 40px; }

        .leftCol { 
            float: left;  
            margin-right: 10px;
        }

        .installselect {
            font-size: 16px;
            line-height: 1;
            border-width: 2px;
            border-color: #4cc8ed;
            border-radius: 5px;
            height: 34px;
            background-image: url(/Content/Images/br_down.png) !important;
            background-repeat:  no-repeat;
            background-position: right;
            background-color: #efefef;
            -webkit-appearance: none;
            -moz-appearance:none;
            padding-right:20px;
            cursor: pointer;
            max-width: 300px;
        }

        .dont-break-out {

          /* These are technically the same, but use both */
          overflow-wrap: break-word;
          word-wrap: break-word;

          -ms-word-break: break-all;
          /* This is the dangerous one in WebKit, as it breaks things wherever */
          word-break: break-all;
          /* Instead use this non-standard one: */
          word-break: break-word;

          /* Adds a hyphen where the word breaks, if supported (No Blink) */
          -ms-hyphens: auto;
          -moz-hyphens: auto;
          -webkit-hyphens: auto;
          hyphens: auto;

        }

        @media (min-width: 1020px) {
            .tabscroll {
                min-height:40px;
            }
        }
        @media (min-width: 1020px) {
            .tabscroll {
                min-height:40px;
            }
        }
    </style>
    <script>
        function imagepreview(input) {
            if (input.files && input.files[0])
            {
                var fildr = new FileReader();
                fildr.onload = function (e)
                {
                    $('#CommentImagePreview').attr('src', e.target.result);
                }
                fildr.readAsDataURL(input.files[0]);
            }
        }
        function replyimagepreview(input) {
            if (input.files && input.files[0]) {
                var fildr = new FileReader();
                fildr.onload = function (e) {
                    $('#ReplyCommentImagePreview').attr('src', e.target.result);
                }
                fildr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <div class="box" style="margin-top:0px;">
        <div class="box-header bg-transparent">
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div>
            <h3 class="box-title"><i class="icon-search"></i>
                <span style="color: black; font-size: medium"><b>SEARCH BY VEHICLE</b></span><asp:HiddenField runat="server" ID="SaveFolderHidden" />
            </h3>
        </div>
        <div class="box-body " style="display: block;">
            <div class="row">
                <div class="form-group form-horizontal">
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleMakeList" CssClass="filter-status form-control installtypestyle installselect" AutoPostBack="true" 
                            OnSelectedIndexChanged="VehicleMakeList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleYearList" CssClass="filter-status form-control installtypestyle installselect" AutoPostBack="true" 
                            OnSelectedIndexChanged="VehicleYearList_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList runat="server" ID="VehicleModelList" CssClass="filter-status form-control installtypestyle installselect" AutoPostBack="true" 
                            OnSelectedIndexChanged="VehicleModelList_SelectedIndexChanged"></asp:DropDownList>
                        <asp:HiddenField runat="server" ID="VehicleMakeModelYearIdHidden" /><asp:HiddenField runat="server" ID="RoleNameHidden" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row bg-white" runat="server" id="TitlePanel" visible="false">
        <div class="large-12 columns" style="padding-top: 5px;">
            <h4>
                <asp:Literal runat="server" ID="CurrentVehicle"></asp:Literal></h4>
        </div>
    </div>
    <div class="row bg-white" runat="server" id="UrgentMessagePanel" visible="false">
        <div class="large-12 columns" style="padding-top: 5px; padding-right:30px;">
            <table border="0" style="border-width:0px; width:100%; margin-bottom:0px;">
                <tr>
                    <td style="width:150px; text-align:left; padding-left:0px;"><h5>Urgent Message:</h5></td>
                    <td style="background-color:orange; color:white;"><asp:Literal runat="server" ID="UrgentMessageLabel"></asp:Literal></td>
                </tr>
            </table>
        </div>
    </div>

    <asp:Panel runat="server" ID="NodeWithSameSystemPanelMobile" ScrollBars="Auto" Width="100%" CssClass="tabscroll" >
    <ul class="tabs row" data-tab style="width: 1020px; white-space: nowrap;">
        <li class="tab-title active " runat="server" id="WiringPaneltab">
            <a href="#<%=WiringPanel.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="WiringHeader" Text="Wiring"></asp:Literal></a>
        </li>
        <li class="tab-title " runat="server" id="linksdisassemblytab">
            <a href="#<%=linksdisassembly.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DisassemblyHeader" Text="Disassembly"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkspreptab">
            <a href="#<%=linksprep.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="PrepHeader" Text="Prep"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkroutingtab">
            <a href="#<%=linkrouting.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="RoutingHeader" Text="Routing/Placement"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="linkprogrammingtab">
            <a href="#<%=linkprogramming.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="ProgrammingHeader" Text="Programming"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="FBPaneltab">
            <a href="#<%=FBPanel.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="FBResultHeader" Text="Facebook Result"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="DocumentPaneltab">
            <a href="#<%=DocumentPanel.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="DocumentHeader" Text="Documents"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="TSBPaneltab">
            <a href="#<%=TSBPanel.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="TSBHeader" Text="TSB"></asp:Literal></a>
        </li>
        <li class="tab-title" runat="server" id="Videostab">
            <a href="#<%=VideosPanel.ClientID%>" style="font-size: small; padding: 8px 15px 15px 15px; margin-right: 5px">
                <asp:Literal runat="server" ID="VideosHeader" Text="Videos"></asp:Literal></a>
        </li>
    </ul>
    </asp:Panel>
    <div class="tabs-content edumix-tab-horz">
        <div class="content active" runat="server" id="WiringPanel" style="padding-top:15px;">
            <div class="row">
                <div class="large-6 medium-12 small-12 columns pull-right">
                    <asp:Image runat="server" ID="VehiclePicture1" />
                </div>
                <div class="large-6 medium-12 small-12 columns">
                    <asp:DropDownList runat="server" ID="InstalltionTypeList" Visible="false" CssClass="filter-status form-control installtypestyle installselect" 
                        AutoPostBack="true" OnSelectedIndexChanged="InstalltionTypeList_SelectedIndexChanged" />
                    <asp:HiddenField runat="server" ID="DefaultInstallationTypeId" /><asp:HiddenField runat="server" ID="DefaultInstallationTypeId2" />
                    <asp:Panel runat="server" ID="NotesPanel" >
                        <b>Notes:</b><br />
                        <asp:Label runat="server" ID="NoteLabel"></asp:Label>
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <span class="entypo-print"></span>
                    <asp:HyperLink runat="server" ID="PrintVehicleWiringButton" Target="_blank" Text="Print Wiring" Visible="false"></asp:HyperLink>
                </div>
            </div>
            <div runat="server" id="linkwire" class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <asp:UpdatePanel ID="WireListUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="WireList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="WireList_RowDataBound"
                                OnRowCommand="WireList_RowCommand" Width="100%" Height="100%">
                                <Columns>
                                    <asp:BoundField DataField="WireFunctionName" HeaderText="Wire Function" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                    <asp:BoundField DataField="Colour" HeaderText="Vehicle Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                    <asp:BoundField DataField="VehicleColor" HeaderText="CM7X00/ADS Color" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                    <asp:BoundField DataField="PinOut" HeaderText="Pin Out" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px" HeaderText="Polarity">
                                        <ItemTemplate>
                                            <a id="PolarityDialogLink" data-toggle="modal" class="no-border"
                                                data-target="#PolarityDialog"><%# Eval("Polarity")%></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='Wiring' Width="30" Height="30"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px" HeaderText="Like/Dislike">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="WireThumbUpButton" CssClass="glyphicon icon-thumbs-up no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbUp" CommandArgument='<%# Bind("VehicleWireFunctionId") %>' />
                                            <asp:Label runat="server" ID="LikeCount" Text='<%# Eval("likecount")%>' CssClass="no-border"></asp:Label>
                                            &nbsp;&nbsp;
                                            <asp:LinkButton runat="server" ID="WireThumbDownButton" CssClass="glyphicon icon-thumbs-down no-border thumbbutton openCorrection" AlternateText="" Text="" data-toggle="modal" 
                                                data-target="#WireThumbdownDialog" data-id='<%# DataBinder.Eval(Container, "DataItem.VehicleWireFunctionId") %>' 
                                                data-wirefunction='<%# DataBinder.Eval(Container, "DataItem.WireFunctionName") %>'
                                                />

                                            <asp:Label runat="server" ID="DislikeCount" Text='<%# Eval("dislikecount")%>' CssClass="no-border"></asp:Label>
                                            <asp:HiddenField runat="server" ID="LikeDisLike" Value='<%# Eval("LikeDisLike")%>' />
                                            <asp:LinkButton runat="server" ID="ClearThumbButton" CssClass="glyphicon icon-trash no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbClear" CommandArgument='<%# Bind("VehicleWireFunctionId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="table-header" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:UpdatePanel ID="CommentListPanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row" runat="server" id="Div1" >
                        <div class="large-12 columns">
                            <b>COMMENTS</b>
                        </div>
                    </div>
                    <div class="row" runat="server" id="CommentPanel" visible="false">
                        <div class="large-1 columns">
                            <asp:Image runat="server" ID="ProfilePicture" Width="60px" Height="60px"/>
                            <asp:HiddenField runat="server" ID="UserProfilePictureFilepath" />
                        </div>
                        <div class="large-11 columns">
                            <CKEditor:CKEditorControl ID="CommentTxt" runat="server" BasePath="/Scripts/js/ckeditor" Toolbar="Cut|Copy|Paste|-|Undo|Redo
Bold|Italic|Underline|Strike|-|Subscript|Superscript
Link|Unlink|Anchor
NumberedList|BulletedList|-|Outdent|Indent
Styles|Format|Font|FontSize|TextColor|BGColor
Source">
                            </CKEditor:CKEditorControl>
                            <asp:Label runat="server" ID="CommentInfoLabel" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="CommentPanel2" visible="false">
                        <div class="large-1 columns">&nbsp;</div>
                        <div class="large-4 medium-12 small-12 columns" style="text-align:right">
                            <div class="row" >
                                <div class="large-3 columns left" style="text-align:left;">Image:</div>
                                <div class="large-9 columns left" style="text-align:left;"><asp:FileUpload runat="server" ID="CommentImageUpload" /></div>
                            </div>
                        </div>
                        <div class="large-5 medium-12 small-12 columns" style="text-align:right">
                            <img id="CommentImagePreview" />
                        </div>
                        <div class="large-3 medium-12 small-12 columns" style="text-align:right">
                            <asp:Button runat="server" ID="SaveCommentButton" CssClass="button tiny bg-black radius no-margin" Text="Save Comment" OnClick="SaveCommentButton_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <asp:GridView ID="CommentList" runat="server" AutoGenerateColumns="false" CssClass="small-font" Width="100%" BorderWidth="0" BorderStyle="None" BorderColor="#FFFFFF"
                                AllowPaging="true" OnPageIndexChanging="CommentList_PageIndexChanging" PageSize="10" ShowHeader="false" OnRowDataBound="CommentList_RowDataBound" 
                                OnRowCommand="CommentList_RowCommand" >
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="large-2 medium-12 small-12 columns" style="border: solid 1px #dfdfdf; padding:10px;">
                                                    <asp:Image runat="server" ID="ProfilePicture" Width="60px" Height="60px"/><br />
                                                    <asp:Label runat="server" ID="ProfileName" Text='<%# Eval("ProfileName")%>' Font-Bold="true" ></asp:Label><br />
                                                    Number of Posts: <asp:Label runat="server" ID="NoOfPosts" Text='<%# Eval("NoOfPosts")%>' Font-Bold="true" ></asp:Label><br />
                                                    Rank: <asp:Label runat="server" ID="Label1" Text='<%# Eval("RankName")%>' Font-Bold="true" ></asp:Label><br />
                                                    Member Since <asp:Label runat="server" ID="RegistrationDate" Text='<%# string.Format("{0:MM/dd/yyyy}", Eval("RegistrationDate"))%>' ></asp:Label>
                                                    <asp:HiddenField runat="server" ID="ProfilePictureFilepath" Value='<%# Eval("ProfilePicture")%>' />
                                                    <asp:HiddenField runat="server" ID="UpdatedBy" Value='<%# Eval("UpdatedBy")%>' />
                                                </div>
                                                <div class="large-10 medium-12 small-12 columns">
                                                    <div class="row" style="padding-left:10px;">
                                                        <asp:Label runat="server" ID="UpdatedDt" Text='<%# string.Format("{0:MM/dd/yyyy}", Eval("UpdatedDt"))%>'  ></asp:Label><br />
                                                    </div>
                                                    <div class="row" style="padding-left:10px;">
                                                        <asp:Image runat="server" ID="CommentImage" CssClass="leftCol img-responsive" />
                                                        <br />
                                                        <asp:Label runat="server" ID="Comment" CssClass="dont-break-out" Text='<%# Eval("Comment")%>' ></asp:Label><br />
                                                    </div>
                                                    <div class="row" style="padding-left:10px;">
                                                        <asp:LinkButton runat="server" ID="EditButton" Text="Edit |" Font-Bold="true" CommandName="EditComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="ReplyButton" Text="Reply" Font-Bold="true" CommandName="Reply" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:LinkButton>
                                                        &nbsp;|&nbsp;
                                                        <asp:LinkButton runat="server" ID="CommentThumbUpButton" CssClass="glyphicon icon-thumbs-up no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbUp" CommandArgument='<%# Bind("VehicleCommentId") %>' />
                                                        <asp:Label runat="server" ID="LikeCount" Text='<%# Eval("likecount")%>' CssClass="no-border"></asp:Label>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton runat="server" ID="CommentThumbDownButton" CssClass="glyphicon icon-thumbs-down no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbDown" CommandArgument='<%# Bind("VehicleCommentId") %>'/>
                                                        <asp:Label runat="server" ID="DislikeCount" Text='<%# Eval("dislikecount")%>' CssClass="no-border"></asp:Label>
                                                        <asp:HiddenField runat="server" ID="LikeDisLike" Value='<%# Eval("LikeDisLike")%>' />
                                                        <asp:HiddenField runat="server" ID="NoOfReplies" Value='<%# Eval("NoOfReplies")%>' />
                                                        <asp:HiddenField runat="server" ID="IsReported" Value='<%# Eval("IsReported")%>' />
                                                        <asp:HiddenField runat="server" ID="ImgFilename" Value='<%# Eval("ImgFilename")%>' />
                                                        <asp:Literal runat="server" ID="ShowReplySep" Text="&nbsp;|&nbsp"></asp:Literal>
                                                        <asp:LinkButton runat="server" ID="ShowReplyButton" Text="Show Replies" Font-Bold="true" CommandName="ShowReplyList" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:LinkButton>
                                                        <asp:Label runat="server" ID="ReplyCount" Text='<%# Eval("NoOfReplies")%>'></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton runat="server" ID="ReportCommentButton" Text="Report this comment" ForeColor="#FF7F27" CommandName="ReportComment" CommandArgument='<%# Bind("VehicleCommentId") %>' OnClientClick="javascript:alert('Reported this comment')"></asp:LinkButton>
                                                    </div>
                                                    <div class="row" style="padding-left:10px;">
                                                        <asp:Panel runat="server" ID="EditCommentPanel" Visible="false">
                                                            <br />
                                                            <div class="row" >
                                                                <b>Edit Comment</b>
                                                                <div class="large-12 columns">
                                                                    <CKEditor:CKEditorControl ID="EditCommentTxt" runat="server" Height="100%" BasePath="/Scripts/js/ckeditor" Toolbar="Cut|Copy|Paste|-|Undo|Redo
Bold|Italic|Underline|Strike|-|Subscript|Superscript
Link|Unlink|Anchor
NumberedList|BulletedList|-|Outdent|Indent
Styles|Format|Font|FontSize|TextColor|BGColor
Source">
                                                                    </CKEditor:CKEditorControl>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="large-12 columns" style="text-align:right">
                                                                    <asp:HiddenField runat="server" ID="EditVehicleCommentId" Value='<%# Eval("VehicleCommentId")%>' />
                                                                    <asp:Button runat="server" ID="CancelEditButton" CssClass="button tiny radius no-margin bg-gray" Text="Cancel" CommandName="CancelEditComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                                                                    <asp:Button runat="server" ID="SaveEditButton" CssClass="button tiny bg-black radius no-margin" Text="Save" CommandName="SaveEditComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" ID="ReplyCommentPanel" Visible="false">
                                                            <br />
                                                            <div class="row" >
                                                                <div class="large-1 columns">
                                                                    <asp:Image runat="server" ID="UserProfilePicture" Width="60px" Height="60px"/>
                                                                </div>
                                                                <div class="large-11 columns">
                                                                    <CKEditor:CKEditorControl ID="ReplyCommentTxt" runat="server" Height="100%" BasePath="/Scripts/js/ckeditor" Toolbar="Cut|Copy|Paste|-|Undo|Redo
Bold|Italic|Underline|Strike|-|Subscript|Superscript
Link|Unlink|Anchor
NumberedList|BulletedList|-|Outdent|Indent
Styles|Format|Font|FontSize|TextColor|BGColor
Source">
                                                                    </CKEditor:CKEditorControl>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="large-4 medium-12 small-12 columns" style="text-align:right">
                                                                    <div class="row" >
                                                                        <div class="large-3 columns left" style="text-align:left;">Image:</div>
                                                                        <div class="large-9 columns left" style="text-align:left;"><asp:FileUpload runat="server" ID="ReplyCommentImageUpload" /></div>
                                                                    </div>
                                                                </div>
                                                                <div class="large-5 medium-12 small-12 columns" style="text-align:right">
                                                                    <img id="ReplyCommentImagePreview" />
                                                                </div>
                                                                <div class="large-12 columns" style="text-align:right">
                                                                    <asp:HiddenField runat="server" ID="VehicleCommentId" Value='<%# Eval("VehicleCommentId")%>' />
                                                                    <asp:Button runat="server" ID="CancelReplyCommentButton" CssClass="button tiny radius no-margin bg-gray" Text="Cancel" CommandName="CancelReply" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                                                                    <asp:Button runat="server" ID="SaveReplyCommentButton" CssClass="button tiny bg-black radius no-margin" Text="Comment" CommandName="SaveReply" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" ></asp:Button>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="large-1 medium-1 small-1 columns" ></div>
                                                <div class="large-11 medium-11 small-11 columns" >
                                                    <asp:Panel runat="server" ID="ReplyCommentListPanel" Visible="false">
                                                        <br />
                                                        <asp:GridView ID="ReplyCommentList" runat="server" AutoGenerateColumns="false" CssClass="small-font" BorderWidth="0" BorderStyle="None" BorderColor="#FFFFFF"
                                                            AllowPaging="false" ShowHeader="false" OnRowDataBound="ReplyCommentList_RowDataBound" OnRowCommand="ReplyCommentList_RowCommand" >
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top"  ItemStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <div class="row">
                                                                            <div class="large-2 medium-4 small-12 columns" style="border: solid 1px #dfdfdf; padding:10px;">
                                                                                <asp:Image runat="server" ID="ProfilePicture" Width="60px" Height="60px"/><br />
                                                                                <asp:Label runat="server" ID="ProfileName" Text='<%# Eval("ProfileName")%>' Font-Bold="true" ></asp:Label><br />
                                                                                Number of Posts: <asp:Label runat="server" ID="NoOfPosts" Text='<%# Eval("NoOfPosts")%>' Font-Bold="true" ></asp:Label><br />
                                                                                Rank: <asp:Label runat="server" ID="Label1" Text='<%# Eval("RankName")%>' Font-Bold="true" ></asp:Label><br />
                                                                                Member Since <asp:Label runat="server" ID="RegistrationDate" Text='<%# string.Format("{0:MM/dd/yyyy}", Eval("RegistrationDate"))%>' ></asp:Label>
                                                                                <asp:HiddenField runat="server" ID="ProfilePictureFilepath" Value='<%# Eval("ProfilePicture")%>' />
                                                                                <asp:HiddenField runat="server" ID="UpdatedBy" Value='<%# Eval("UpdatedBy")%>' />
                                                                            </div>
                                                                            <div class="large-10 medium-8 small-12 columns">
                                                                                <asp:Label runat="server" ID="UpdatedDt" Text='<%# string.Format("{0:MM/dd/yyyy}", Eval("UpdatedDt"))%>'  ></asp:Label><br />
                                                                                <asp:Image runat="server" ID="CommentImage" CssClass="leftCol img-responsive" />
                                                                                <br />
                                                                                <asp:Label runat="server" ID="Comment" Text='<%# Eval("Comment")%>' ></asp:Label><br />
                                                                                <asp:LinkButton runat="server" ID="EditReplyButton" Text="Edit |" Font-Bold="true" CommandName="EditReplyComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:LinkButton>
                                                                                <asp:LinkButton runat="server" ID="CommentThumbUpButton" CssClass="glyphicon icon-thumbs-up no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbUp" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                                                                <asp:Label runat="server" ID="LikeCount" Text='<%# Eval("likecount")%>' CssClass="no-border"></asp:Label>
                                                                                &nbsp;&nbsp;
                                                                                <asp:LinkButton runat="server" ID="CommentThumbDownButton" CssClass="glyphicon icon-thumbs-down no-border thumbbutton" AlternateText="" Text="" CommandName="ThumbDown" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                                                                                <asp:Label runat="server" ID="DislikeCount" Text='<%# Eval("dislikecount")%>' CssClass="no-border"></asp:Label>
                                                                                <asp:HiddenField runat="server" ID="LikeDisLike" Value='<%# Eval("LikeDisLike")%>' />
                                                                                <asp:HiddenField runat="server" ID="VehicleCommentId" Value='<%# Eval("VehicleCommentId")%>' />
                                                                                <asp:HiddenField runat="server" ID="ParentVehicleCommentId" Value='<%# Eval("ParentVehicleCommentId")%>' />
                                                                                <asp:HiddenField runat="server" ID="IsReported" Value='<%# Eval("IsReported")%>' />
                                                                                <asp:HiddenField runat="server" ID="ImgFilename" Value='<%# Eval("ImgFilename")%>' />
                                                                                <br />
                                                                                <asp:LinkButton runat="server" ID="ReportCommentButton" Text="Report this comment" ForeColor="#FF7F27" CommandName="ReportComment" CommandArgument='<%# Bind("VehicleCommentId") %>' OnClientClick="javascript:alert('Reported this comment')"></asp:LinkButton>
                                                                                <asp:Panel runat="server" ID="EditReplyCommentPanel" Visible="false">
                                                                                    <br />
                                                                                    <div class="row" >
                                                                                        <b>Edit Comment</b>
                                                                                        <div class="large-12 columns">
                                                                                            <CKEditor:CKEditorControl ID="EditReplyCommentTxt" runat="server" Height="100%" BasePath="/Scripts/js/ckeditor">
                                                                                            </CKEditor:CKEditorControl>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="large-12 columns" style="text-align:right">
                                                                                            <asp:HiddenField runat="server" ID="EditReplyVehicleCommentId" Value='<%# Eval("VehicleCommentId")%>' />
                                                                                            <asp:Button runat="server" ID="CancelEditButton" CssClass="button tiny radius no-margin bg-gray" Text="Cancel" CommandName="CancelEditReplyComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                                                                                            <asp:Button runat="server" ID="SaveEditButton" CssClass="button tiny bg-black radius no-margin" Text="Save" CommandName="SaveEditReplyComment" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                                                                                        </div>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="table-header" />
                                                            <RowStyle CssClass="topborder" />
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="table-header" />
                                <RowStyle CssClass="topborder" />
                            </asp:GridView>
                            <asp:HiddenField runat="server" ID="ExpandReplyVehicleCommentId" Value="" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="SaveCommentButton" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="content" runat="server" id="linksdisassembly" style="padding-top:15px;">
            <asp:GridView ID="DisassemblyList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="DisassemblyList_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Disassembly Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linksprep" style="padding-top:15px;">
            <asp:GridView ID="PrepList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="PrepList_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Pre Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linkrouting" style="padding-top:15px;">
            <asp:GridView ID="RoutingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="RoutingList_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Routing / Placement Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="linkprogramming" style="padding-top:15px;">
            <asp:GridView ID="ProgrammingList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="ProgrammingList_RowDataBound" >
                <Columns>
                    <asp:BoundField DataField="Step" HeaderText="Step" ItemStyle-Width="60px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="Note" HeaderText="Description" HtmlEncode="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' ToolTip='<%# "Programming Step " + Eval("Step")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="FBPanel" style="padding-top:15px;">
            <asp:GridView ID="FBList" runat="server" AutoGenerateColumns="false" CssClass="demo" OnRowDataBound="FBList_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Facebook" HeaderStyle-Wrap="false" HeaderStyle-Font-Bold="true">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Note") %>
                            <br />
                            <asp:HyperLink runat="server" ID="Link" NavigateUrl='<%# Bind("URL") %>' Target="_blank">[Link]</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="ImageLink1" BorderWidth="0" NavigateUrl='<%# Eval("Attach1")%>' ImageUrl='<%# Eval("Attach1")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink2" BorderWidth="0" NavigateUrl='<%# Eval("Attach2")%>' ImageUrl='<%# Eval("Attach2")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink3" BorderWidth="0" NavigateUrl='<%# Eval("Attach3")%>' ImageUrl='<%# Eval("Attach3")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink4" BorderWidth="0" NavigateUrl='<%# Eval("Attach4")%>' ImageUrl='<%# Eval("Attach4")%>' Width="30" Height="30"></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="ImageLink5" BorderWidth="0" NavigateUrl='<%# Eval("Attach5")%>' ImageUrl='<%# Eval("Attach5")%>' Width="30" Height="30"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="DocumentPanel" style="padding-top:15px;">
            <asp:DataList runat="server" ID="DocumentList" BorderWidth="0px" RepeatDirection="Vertical" RepeatColumns="1" OnItemDataBound="DocumentList_ItemDataBound">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:HiddenField ID="VehicleDocumentId" runat="server" Value='<%# Bind("VehicleDocumentId") %>' />
                    <asp:HiddenField ID="DocumentName" runat="server" Value='<%# Bind("DocumentName") %>' />
                    <asp:HiddenField ID="AttachFile" runat="server" Value='<%# Bind("AttachFile") %>' />
                    <asp:Label ID="DocumentLabel" runat="server" BorderWidth="0"></asp:Label>
                </ItemTemplate>
                <ItemStyle BackColor="White" />
                <AlternatingItemStyle BackColor="White" />
            </asp:DataList>
        </div>
        <div class="content" runat="server" id="TSBPanel" style="padding-top:15px;">
            <asp:GridView ID="TSBList" runat="server" AutoGenerateColumns="false" CssClass="demo" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ReportReceivedDate" HeaderText="Date" ItemStyle-Width="100px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="NHTSACampaignNumber" HeaderText="Campaign #" ItemStyle-Width="120px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HeaderText="Recall">
                        <ItemTemplate>
                            <b>Component</b>: <%# DataBinder.Eval(Container.DataItem, "Component") %><br />
                            <b>Summary</b>: <%# DataBinder.Eval(Container.DataItem, "Summary") %><br />
                            <b>Conequence</b>: <%# DataBinder.Eval(Container.DataItem, "Conequence") %><br />
                            <b>Remedy</b>: <%# DataBinder.Eval(Container.DataItem, "Remedy") %><br />
                            <b>Notes</b>: <%# DataBinder.Eval(Container.DataItem, "Notes") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="content" runat="server" id="VideosPanel" style="padding-top:15px;">
            <asp:GridView ID="VideosList" runat="server" AutoGenerateColumns="false" CssClass="demo" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ReportReceivedDate" HeaderText="Date" ItemStyle-Width="100px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="NHTSACampaignNumber" HeaderText="Campaign #" ItemStyle-Width="120px" HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Top" />
                    <asp:TemplateField HeaderStyle-Font-Bold="true" ItemStyle-Font-Size="12px" HeaderText="Recall">
                        <ItemTemplate>
                            <b>Component</b>: <%# DataBinder.Eval(Container.DataItem, "Component") %><br />
                            <b>Summary</b>: <%# DataBinder.Eval(Container.DataItem, "Summary") %><br />
                            <b>Conequence</b>: <%# DataBinder.Eval(Container.DataItem, "Conequence") %><br />
                            <b>Remedy</b>: <%# DataBinder.Eval(Container.DataItem, "Remedy") %><br />
                            <b>Notes</b>: <%# DataBinder.Eval(Container.DataItem, "Notes") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Label runat="server" ID="ErrorLabel" ForeColor="Red"></asp:Label>
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="WireThumbdownDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">&nbsp;&nbsp;&nbsp;Correction to Wire</h3>
                </div>
                <div class="modal-body" style="color:black;">
                    <div class="row">
                        <div class="small-12 columns">&nbsp;&nbsp;&nbsp;Please send us correction.</div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns"><asp:TextBox runat="server" ID="correctwirefunction" CssClass="correctwirefunctionclass" ReadOnly="true" BorderColor="White" BorderWidth="0" BackColor="White" Font-Size="15px" /></div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <asp:TextBox runat="server" ID="CorrectionDesc"  Width="100%" Rows="5" TextMode="MultiLine" ></asp:TextBox>
                            <input type="hidden" id="VehicleWireFunctionIdHidden" runat="server" class="correctionwireid"/>
                            <i>If no correction is given, the Thumbs Down Rating will be void.</i>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button runat="server" ID="SubmitCorrectionButton" Text="Submit" CssClass="btn btn-primary" OnClick="SubmitCorrectionButton_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="PolarityDialog" tabindex="-1" role="dialog" aria-labelledby="PolarityLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="max-width:600px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="PolarityLabel">&nbsp;&nbsp;&nbsp;Polarity</h3>
                </div>
                <div class="modal-body" style="color:black;">
                    <div class="row">
                        <div class="small-2 columns">
                            +
                        </div>
                        <div class="small-10 columns">
                            Positive Voltage, circuit is constantly at 12 Volts or switches to 12 Volts.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            -
                        </div>
                        <div class="small-10 columns">
                            Ground, circuit constantly at Ground or switched to Ground.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            - N.C.
                        </div>
                        <div class="small-10 columns">
                            Normally Closed Ground Circuit, traditionally used on Ford/Mazda door triggers. When door is closed, wire will measure at ground till door opens.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            - (resistance)
                        </div>
                        <div class="small-10 columns">
                            Circuit will preform an action based on “seeing” a specific voltage. This is attained by using a Resistance (usually indicated) to Ground. Often times the same wire using different Resistances will perform different operations. Typically used in Chrysler Headlight Switches or Mazda Locks.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            MUX
                        </div>
                        <div class="small-10 columns">
                            Multiplex circuit, is a single wire using different resistances/voltages to “tell” the vehicle electronic module (BCM, GEM…) to preform an operation. This wire cant control a large number of functions.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            AC
                        </div>
                        <div class="small-10 columns">
                            Alternating Current, most commonly used to measure the Tach Signal of the vehicle. Good Tach Signal should be between .7 Volts – 5 Volts and fluctuates with throttle position. 
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            5 wire
                        </div>
                        <div class="small-10 columns">
                            Usually used for controlling actuators. The circuit rests at Ground and interrupts then feeds a 12 Volt source to activate the actuator in a direction. 
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            Data
                        </div>
                        <div class="small-10 columns">
                            Communication for digital circuits. Data term is used when circuit is not easily measured with tools readily available in most shops. 
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            Open
                        </div>
                        <div class="small-10 columns">
                            Indicates that the circuit needs to be interrupted. Often used in Ford Headlight Switches to interrupt the Ground to the switch so that a backfeed does not happen and burn out the switch.
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            
                        </div>
                        <div class="small-10 columns">
                            <br /><br />
                            For more detailed information, see our Glossary at
                            <a target="_blank" href="http://firstechdata.com/Search/Document?Id=1205">http://firstechdata.com/Search/Document?Id=1205</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", ".openCorrection", function () {
            var WireFunctionName = $(this).data('wirefunction');
            $(".modal-body .correctwirefunctionclass").val(WireFunctionName);

            var VehicleWireFunctionId = $(this).data('id');
            $(".modal-body .correctionwireid").val(VehicleWireFunctionId);


            // $('#addBookDialog').modal('show');
        });


        var linkwire_var = document.getElementById('<%=linkwire.ClientID%>');
        if (linkwire_var != null) {
            linkwire_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linkwire = this.getElementsByTagName('a');

                if (link.id.indexOf("PolarityDialogLink") < 0) {
                    if (typeof link.href !== "undefined" && link.href.indexOf("javascript") < 0) {
                        blueimp.Gallery(linkwire, options);
                    }
                    else {
                        $('[id*=WireList]').footable();
                    }
                }
            };
        }

        var linksprep_var = document.getElementById('<%=linksprep.ClientID%>');
        if (linksprep_var != null) {
            linksprep_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linksprep = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(linksprep, options);
                }
            };
        }

        var linksdisassembly_var = document.getElementById('<%=linksdisassembly.ClientID%>');
        if (linksdisassembly_var != null) {
            linksdisassembly_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linksdisassembly = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(linksdisassembly, options);
                }
            };
        }

        var linkrouting_var = document.getElementById('<%=linkrouting.ClientID%>');
        if (linkrouting_var != null) {
            linkrouting_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linkrouting = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(linkrouting, options);
                }
            };
        }

        var linkprogramming_var = document.getElementById('<%=linkprogramming.ClientID%>');
        if (linkprogramming_var != null) {
            linkprogramming_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    linkprogramming = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined") {
                    blueimp.Gallery(linkprogramming, options);
                }
            };
        }

        var FBPanel_var = document.getElementById('<%=FBPanel.ClientID%>');
        if (FBPanel_var != null) {
            FBPanel_var.onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {
                        index: link,
                        event: event,
                        onslide: function (index, slide) {
                            var link = this.list[index].getAttribute('data-link');
                            $(slide).children().attr('href', link)
                        }
                    },
                    FBPanel = this.getElementsByTagName('a');

                if (typeof link.href !== "undefined" && link.id.indexOf("ImageLink") >= 0) {
                    blueimp.Gallery(FBPanel, options);
                }
            };
        }
    </script>
    <script type="text/javascript">
    var isMobile = false; //initiate as false
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

    if (isMobile)
    {
        $(function () {
            $('[id*=WireList]').footable();
            $('[id*=PrepList]').footable();
            $('[id*=FBList]').footable();
            $('[id*=DisassemblyList]').footable();
            $('[id*=RoutingList]').footable();
            $('[id*=ProgrammingList]').footable();

            $('#<%=WireListUpdatePanel.ClientID %>').css('border', 'none');
        });

        //Re-Create for on page postbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).foundation('tab', 'reflow');

            $('[id*=WireList]').footable();
            $('[id*=PrepList]').footable();
            $('[id*=FBList]').footable();
            $('[id*=DisassemblyList]').footable();
            $('[id*=RoutingList]').footable();
            $('[id*=ProgrammingList]').footable();
        });
    }
    </script>

</asp:Content>
