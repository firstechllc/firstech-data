﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class VINLookup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login?ReturnUrl=%2FSearch%2FVinLookup");
                }

                Master.ChangeMenuCss("VinLookupMenu");
            }
        }

        private void ShowError(string e)
        {
            ErrorMsg.Text = e;
        }
        protected void VINSearchButton_Click(object sender, EventArgs e)
        {
            ShowError("");

            try
            {
                string vin = VINSearch.Text.Trim();

                if (vin == "")
                {
                    ShowError("Please enter VIN");
                    return;
                }
                /*
                VinLookupData data = JsonConvert.DeserializeObject<VinLookupData>(json);
                OutputLabel.Text = data.make.name + " " + data.model.name + " " + data.years[0].year;
                */

                string api = "z4e8sgaxmre353mbtwaqb6kz";

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://api.edmunds.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    VIN vin_data = null;
                    HttpResponseMessage vin_response = client.GetAsync("api/vehicle/v2/vins/" + vin + "?fmt=json&api_key=" + api).Result;
                    if (vin_response.IsSuccessStatusCode)
                    {
                        vin_data = vin_response.Content.ReadAsAsync<VIN>().Result;

                        YearLabel.Text = vin_data.years[0].year.ToString();
                        MakeLabel.Text = vin_data.make.name;
                        ModelLabel.Text = vin_data.model.name;
                        TrimLabel.Text = vin_data.years[0].styles[0].trim;
                        BodyLabel.Text = vin_data.years[0].styles[0].submodel.body;
                        NoOfDoorsLabel.Text = vin_data.numOfDoors;
                        if (vin_data.transmission != null)
                            TransmissionTypeLabel.Text = vin_data.transmission.transmissionType;
                        DrivenWheelsLabel.Text = vin_data.drivenWheels;

                        if (vin_data.years != null && vin_data.years.Count > 0 && vin_data.years[0].styles != null && vin_data.years[0].styles.Count > 0)
                        {
                            int StyleId = vin_data.years[0].styles[0].id;
                            Engine engine_data = null;

                            HttpResponseMessage engine_response = client.GetAsync("api/vehicle/v2/styles/" + StyleId + "/engines?fmt=json&api_key=" + api).Result;
                            if (engine_response.IsSuccessStatusCode)
                            {
                                engine_data = engine_response.Content.ReadAsAsync<Engine>().Result;

                                int idx = 1;
                                string info = "";
                                if (engine_data != null)
                                {
                                    foreach (var engine in engine_data.engines)
                                    {
                                        info += "Engine #" + idx + "<br/>";

                                        info += "Name: " + engine.name + "<br/>";
                                        info += "Displacement: " + engine.displacement.ToString() + "<br/>";
                                        info += "Fuel Type: " + engine.fuelType + "<br/>";
                                        info += "Compression Ratio: " + engine.compressionRatio.ToString() + "<br/>";
                                        info += "Cylinder: " + engine.cylinder.ToString() + "<br/>";
                                        info += "Size: " + engine.size.ToString() + "<br/>";
                                        info += "Horsepower: " + engine.horsepower.ToString() + "<br/>";
                                        info += "Torque: " + engine.torque.ToString() + "<br/>";

                                        idx++;
                                    }
                                    info += "<br/>";
                                }

                                EngineInfo.Text = info;
                            }

                            string modelyearid = "";
                            if (vin_data.years != null && vin_data.years.Count > 0)
                            {
                                ModelYear modelyear_data = null;
                                HttpResponseMessage modelyear_response = client.GetAsync("api/vehicle/v2/" + vin_data.make.niceName + "/" + vin_data.model.niceName + "/" + vin_data.years[0].year + "?fmt=json&api_key=" + api).Result;
                                if (modelyear_response.IsSuccessStatusCode)
                                {
                                    modelyear_data = modelyear_response.Content.ReadAsAsync<ModelYear>().Result;
                                    modelyearid = modelyear_data.id.ToString();
                                }
                            }

                            if (modelyearid != "")
                            {

                                TSB tsb_data = null;
                                HttpResponseMessage tsb_response = client.GetAsync("v1/api/maintenance/servicebulletinrepository/findbymodelyearid?modelyearid=" + modelyearid + "&fmt=json&api_key=" + api).Result;
                                if (tsb_response.IsSuccessStatusCode)
                                {
                                    tsb_data = tsb_response.Content.ReadAsAsync<TSB>().Result;

                                    int idx = 1;
                                    string info = "";
                                    if (tsb_data != null)
                                    {
                                        foreach (var svc in tsb_data.serviceBulletinHolder)
                                        {
                                            info += "Service Bulletin #" + idx + "<br/>";

                                            info += "Bulletin Number: " + svc.bulletinNumber + "<br/>";
                                            info += "Bulletin Date Month: " + svc.bulletinDateMonth + "<br/>";
                                            info += "Component Description: " + svc.componentDescription + "<br/>";
                                            info += "Summary Text: " + svc.summaryText + "<br/>";

                                            idx++;
                                        }
                                        info += "<br/>";
                                    }

                                    TSBInfo.Text = info;
                                }

                                Recall recall_data = null;
                                HttpResponseMessage recall_response = client.GetAsync("v1/api/maintenance/recallrepository/findbymodelyearid?modelyearid=" + modelyearid + "&fmt=json&api_key=" + api).Result;
                                if (recall_response.IsSuccessStatusCode)
                                {
                                    recall_data = recall_response.Content.ReadAsAsync<Recall>().Result;

                                    int idx = 1;
                                    string info = "";
                                    if (recall_data != null)
                                    {
                                        foreach (var holder in recall_data.recallHolder)
                                        {
                                            info += "Recall #" + idx + "<br/>";

                                            info += "Recall Number: " + holder.recallNumber + "<br/>";
                                            info += "Component Description: " + holder.componentDescription + "<br/>";
                                            info += "Manufactured To: " + holder.manufacturedTo + "<br/>";
                                            info += "Number Of Vehicles Affected: " + holder.numberOfVehiclesAffected + "<br/>";
                                            info += "Influenced By: " + holder.influencedBy + "<br/>";
                                            info += "Defect Consequence: " + holder.defectConsequence + "<br/>";
                                            info += "Defect Corrective Action: " + holder.defectCorrectiveAction + "<br/>";
                                            info += "Defect Description: " + holder.defectDescription + "<br/>";

                                            idx++;
                                        }
                                        info += "<br/>";
                                    }

                                    RecallInfo.Text = info;
                                }
                            }

                            Photo photo_data = null;

                            HttpResponseMessage photo_response = client.GetAsync("api/media/v2/styles/" + StyleId + "/photos?fmt=json&api_key=" + api).Result;
                            if (photo_response.IsSuccessStatusCode)
                            {
                                photo_data = photo_response.Content.ReadAsAsync<Photo>().Result;

                                string info = "";

                                int ex_height = 0;
                                string ex_imgurl = "";
                                int in_height = 0;
                                string in_imgurl = "";

                                if (photo_data != null)
                                {
                                    foreach (var photo in photo_data.photos)
                                    {
                                        /*
                                        info += "Photo #" + idx + "<br/>";
                                        info += "Title:" + photo.title + "<br/>";
                                        info += "Category:" + photo.category + "<br/>";
                                        */

                                        if (photo.category == "EXTERIOR")
                                        {
                                            if (photo != null && photo.sources.Count > 0)
                                            {
                                                foreach(var image in photo.sources)
                                                {
                                                    if (ex_height < image.size.height)
                                                    {
                                                        ex_height = image.size.height;
                                                        ex_imgurl = image.link.href;
                                                    }

                                                }
                                            }
                                        }
                                        else if (photo.category == "INTERIOR")
                                        {
                                            if (photo != null && photo.sources.Count > 0)
                                            {
                                                foreach (var image in photo.sources)
                                                {
                                                    if (in_height < image.size.height)
                                                    {
                                                        in_height = image.size.height;
                                                        in_imgurl = image.link.href;
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    if (ex_imgurl != "")
                                    {
                                        info += "<img src='http://media.ed.edmunds-media.com" + ex_imgurl + "' style='max-height:350px;'>";
                                    }
                                    if (in_imgurl != "")
                                    {
                                        info += "<img src='http://media.ed.edmunds-media.com" + in_imgurl + "' style='max-height:350px;'>";
                                    }
                                }

                                PhotoInfo.Text = info;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.ToString());
            }
        }

    }

    public class VIN_Make
    {
        public int id { get; set; }
        public string name { get; set; }
        public string niceName { get; set; }
    }

    public class VIN_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string niceName { get; set; }
    }

    public class VIN_Transmission
    {
        public string id { get; set; }
        public string name { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public string automaticType { get; set; }
        public string transmissionType { get; set; }
        public string numberOfSpeeds { get; set; }
    }

    public class VIN_Price
    {
        public int baseMSRP { get; set; }
        public int baseInvoice { get; set; }
        public bool estimateTmv { get; set; }
    }

    public class VIN_Option2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public string manufactureOptionName { get; set; }
        public string manufactureOptionCode { get; set; }
        public string category { get; set; }
        public VIN_Price price { get; set; }
    }

    public class VIN_Option
    {
        public string category { get; set; }
        public List<VIN_Option2> options { get; set; }
    }

    public class VIN_Primary
    {
        public int r { get; set; }
        public int g { get; set; }
        public int b { get; set; }
        public string hex { get; set; }
    }

    public class VIN_ColorChips
    {
        public VIN_Primary primary { get; set; }
    }

    public class VIN_Option3
    {
        public string id { get; set; }
        public string name { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public string manufactureOptionName { get; set; }
        public string manufactureOptionCode { get; set; }
        public string category { get; set; }
        public VIN_ColorChips colorChips { get; set; }
        public List<object> fabricTypes { get; set; }
    }

    public class VIN_Color
    {
        public string category { get; set; }
        public List<VIN_Option3> options { get; set; }
    }

    public class VIN_Price2
    {
        public float baseMSRP { get; set; }
        public float baseInvoice { get; set; }
        public float deliveryCharges { get; set; }
        public float usedTmvRetail { get; set; }
        public float usedPrivateParty { get; set; }
        public float usedTradeIn { get; set; }
        public bool estimateTmv { get; set; }
    }

    public class VIN_Categories
    {
        public string EPAClass { get; set; }
        public string primaryBodyType { get; set; }
        public string vehicleStyle { get; set; }
        public string vehicleType { get; set; }
    }

    public class VIN_Submodel
    {
        public string body { get; set; }
        public string modelName { get; set; }
        public string niceName { get; set; }
    }

    public class VIN_Style
    {
        public int id { get; set; }
        public string name { get; set; }
        public VIN_Submodel submodel { get; set; }
        public string trim { get; set; }
    }

    public class VIN_Year
    {
        public int id { get; set; }
        public int year { get; set; }
        public List<VIN_Style> styles { get; set; }
        public List<string> states { get; set; }
    }

    public class VIN_Equipment
    {
        public string id { get; set; }
        public string name { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public double compressionRatio { get; set; }
        public int cylinder { get; set; }
        public double size { get; set; }
        public int displacement { get; set; }
        public string configuration { get; set; }
        public string fuelType { get; set; }
        public int horsepower { get; set; }
        public int torque { get; set; }
        public int totalValves { get; set; }
        public string manufacturerEngineCode { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string compressorType { get; set; }
        public string automaticType { get; set; }
        public string transmissionType { get; set; }
        public string numberOfSpeeds { get; set; }
    }

    public class VIN_MPG
    {
        public string highway { get; set; }
        public string city { get; set; }
    }

    public class VIN
    {
        public VIN_Make make { get; set; }
        public VIN_Model model { get; set; }
        public VIN_Transmission transmission { get; set; }
        public string drivenWheels { get; set; }
        public string numOfDoors { get; set; }
        public List<VIN_Option> options { get; set; }
        public List<VIN_Color> colors { get; set; }
        public string manufacturerCode { get; set; }
        public VIN_Price2 price { get; set; }
        public VIN_Categories categories { get; set; }
        public string vin { get; set; }
        public string squishVin { get; set; }
        public List<VIN_Year> years { get; set; }
        public string matchingType { get; set; }
        public List<VIN_Equipment> equipment { get; set; }
        public VIN_MPG MPG { get; set; }
    }





    public class Engine_Attribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Engine_Price
    {
        public double baseMSRP { get; set; }
        public double baseInvoice { get; set; }
        public bool estimateTmv { get; set; }
    }

    public class Engine_Option
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public List<Engine_Attribute> attributes { get; set; }
        public string manufactureOptionName { get; set; }
        public string manufactureOptionCode { get; set; }
        public string category { get; set; }
        public Engine_Price price { get; set; }
    }

    public class Engine_Engine
    {
        public string id { get; set; }
        public string name { get; set; }
        public string equipmentType { get; set; }
        public string availability { get; set; }
        public List<Engine_Option> options { get; set; }
        public double compressionRatio { get; set; }
        public int cylinder { get; set; }
        public double size { get; set; }
        public double displacement { get; set; }
        public string configuration { get; set; }
        public string fuelType { get; set; }
        public int horsepower { get; set; }
        public int torque { get; set; }
        public int totalValves { get; set; }
        public string manufacturerEngineCode { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string compressorType { get; set; }
    }

    public class Engine
    {
        public List<Engine_Engine> engines { get; set; }
        public int enginesCount { get; set; }
    }



    public class ModelYear_Submodel
    {
        public string body { get; set; }
        public string modelName { get; set; }
        public string niceName { get; set; }
    }

    public class ModelYear_Style
    {
        public int id { get; set; }
        public string name { get; set; }
        public ModelYear_Submodel submodel { get; set; }
        public string trim { get; set; }
    }

    public class ModelYear
    {
        public int id { get; set; }
        public int year { get; set; }
        public List<ModelYear_Style> styles { get; set; }
    }



    public class TSB_ServiceBulletinHolder
    {
        public int id { get; set; }
        public string bulletinNumber { get; set; }
        public string bulletinDateMonth { get; set; }
        public int componentNumber { get; set; }
        public string componentDescription { get; set; }
        public string nhtsaItemNumber { get; set; }
        public int modelYearId { get; set; }
        public string summaryText { get; set; }
    }

    public class TSB
    {
        public List<TSB_ServiceBulletinHolder> serviceBulletinHolder { get; set; }
    }


    public class Recall_RecallHolder
    {
        public int id { get; set; }
        public string recallNumber { get; set; }
        public string componentDescription { get; set; }
        public string manufacturerRecallNumber { get; set; }
        public string manufacturedTo { get; set; }
        public string numberOfVehiclesAffected { get; set; }
        public string influencedBy { get; set; }
        public string defectConsequence { get; set; }
        public string defectCorrectiveAction { get; set; }
        public string defectDescription { get; set; }
        public string modelYear { get; set; }
    }

    public class Recall
    {
        public List<Recall_RecallHolder> recallHolder { get; set; }
    }



    public class Photo_Link
    {
        public string rel { get; set; }
        public string href { get; set; }
    }

    public class Photo_Size
    {
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Photo_Source
    {
        public Photo_Link link { get; set; }
        public string extension { get; set; }
        public Photo_Size size { get; set; }
    }

    public class Photo_Photo
    {
        public string title { get; set; }
        public string category { get; set; }
        public List<string> tags { get; set; }
        public string provider { get; set; }
        public List<Photo_Source> sources { get; set; }
        public string color { get; set; }
        public List<string> submodels { get; set; }
        public string shotTypeAbbreviation { get; set; }
    }

    public class Photo_Link2
    {
        public string rel { get; set; }
        public string href { get; set; }
    }

    public class Photo
    {
        public List<Photo_Photo> photos { get; set; }
        public int photosCount { get; set; }
        public List<Photo_Link2> links { get; set; }
    }

}