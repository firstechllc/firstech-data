﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstechData
{
    public partial class Send_E_Mail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ToEmail = Request["ToEmail"];
            string Title = Request["Title"];
            string Filename = Request["Filename"];
            string Domain = Request["Domain"];
            string StartDate = Request["StartDate"];
            string EndDate = Request["EndDate"];

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var ToUser = manager.FindByEmail(ToEmail);
            string content = "Report Period: " + StartDate + " - " + EndDate + "<br/>Download <a href=\"http://" + Domain  + "/Export/" + Filename + "\">Report File</a>";

            manager.SendEmail(ToUser.Id, Title, content);

        }
    }
}