﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstechDataWeeklyJobs
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = null;
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                string OutputPath = ConfigurationManager.AppSettings["OutputPath"].ToString() + @"\Export";
                string SendEmail = ConfigurationManager.AppSettings["SendWeb"].ToString();
                string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                string Domain = ConfigurationManager.AppSettings["Domain"].ToString();

                string PaidOutTitle = ConfigurationManager.AppSettings["PaidOutTitle"].ToString();
                string SearchCounterTitle = ConfigurationManager.AppSettings["SearchCounterTitle"].ToString();

                conn = new SqlConnection(connectionString);
                conn.Open();

                DateTime EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-1);
                DateTime StartDate = EndDate.AddDays(-7);

                string StartDateStr = string.Format("{0:yyyyMMdd}", StartDate);
                string EndDateStr = string.Format("{0:yyyyMMdd}", EndDate);

                // Search Counter
                SqlCommand cmd = new SqlCommand("proc_SearchCounterSearch", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@StartDate", SqlDbType.NVarChar, 2000).Value = StartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.NVarChar, 2000).Value = EndDate.AddDays(1);
                cmd.Parameters.Add("@Number", SqlDbType.Int).Value = 100000000;

                SqlDataReader reader = cmd.ExecuteReader();
                StringBuilder excel = new StringBuilder();

                excel.AppendLine("No,Vehicle,Counter,Last Updated Date");

                while (reader.Read())
                {
                    excel.Append(reader["Num"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Vehicle"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Counter"].ToString());
                    excel.Append(",");
                    excel.Append(reader["UpdatedDt"].ToString());
                    excel.AppendLine("");
                }

                reader.Close();

                if (!Directory.Exists(OutputPath))
                {
                    Directory.CreateDirectory(OutputPath);
                }
                string filename = string.Format("SearchCounter_{0:yyyyMMdd}_{0:yyyMMddHHmmss}.csv", EndDate, DateTime.Now);
                File.WriteAllText(OutputPath + @"\" + filename, excel.ToString());

                string url = SendEmail + "?ToEmail=" + ToEmail + "&Title=" + SearchCounterTitle + "&Domain=" + Domain + "&StartDate=" + StartDateStr + "&EndDate=" + EndDateStr + "&Filename=" + filename;
                System.Net.WebClient browser = new System.Net.WebClient();
                browser.DownloadData(url);

                
                // Paidout
                cmd = new SqlCommand("proc_SearchPaidout", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@StartDate", SqlDbType.NVarChar, 2000).Value = StartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.NVarChar, 2000).Value = EndDate.AddDays(1);

                reader = cmd.ExecuteReader();
                excel = new StringBuilder();

                excel.AppendLine("ID,MTCRepcode,InvoiceNumber,Email,AccountType,SubscriptionPlan,RewardType,RewardAmount,AddedDate");

                while (reader.Read())
                {
                    excel.Append(reader["ID"].ToString());
                    excel.Append(",");
                    excel.Append(reader["EditorMTCRepcode"].ToString());
                    excel.Append(",");
                    excel.Append(reader["InvoiceNumber"].ToString());
                    excel.Append(",");
                    excel.Append(reader["Email"].ToString());
                    excel.Append(",");
                    excel.Append(reader["AccountType"].ToString());
                    excel.Append(",");
                    excel.Append(reader["SubscriptionPlan"].ToString());
                    excel.Append(",");
                    excel.Append(reader["RewardType"].ToString());
                    excel.Append(",");
                    excel.Append(reader["RewardAmount"].ToString());
                    excel.Append(",");
                    excel.Append(string.Format("{0:MM/dd/yyyy}", reader["AddedDate"]));
                    excel.AppendLine("");
                }

                reader.Close();

                filename = string.Format("PaidOut_{0:yyyyMMdd}_{0:yyyMMddHHmmss}.csv", EndDate, DateTime.Now);
                File.WriteAllText(OutputPath + @"\" + filename, excel.ToString());

                url = SendEmail + "?ToEmail=" + ToEmail + "&Title=" + PaidOutTitle + "&Domain=" + Domain + "&StartDate=" + StartDateStr + "&EndDate=" + EndDateStr + "&Filename=" + filename;
                browser.DownloadData(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
        }
    }
}
