﻿
-- select count(*) from WireTestInput2

select distinct VehicleMake, VehicleModel, VehicleYear
from WireTestInput2

select *
from WireTestInput2
where VehicleMake = ''

select *
from (select distinct VehicleMake, VehicleModel, VehicleYear
from WireTestInput2) a1
full outer join (
	select m.VehicleMakeName, o.VehicleModelName, a.VehicleYear
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
	and a1.VehicleYear = a2.VehicleYear
where a1.VehicleMake is null

select *
from (select distinct VehicleMake, VehicleModel, VehicleYear
from WireTestInput2) a1
full outer join (
	select m.VehicleMakeName, o.VehicleModelName, a.VehicleYear
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
	and a1.VehicleYear = a2.VehicleYear
where a2.VehicleMakeName is null


select *
from (select distinct VehicleMake from WireTestInput2) a1
full outer join (
	select distinct m.VehicleMakeName
	from dbo.VehicleMake m 
) a2 on a1.VehicleMake = a2.VehicleMakeName
where a2.VehicleMakeName is null

select * from dbo.VehicleMake

insert into dbo.VehicleMake
(VehicleMakeName, CountryCode, UpdatedDt, Inactive, UpdatedBy)
select a1.VehicleMake, 'US', getdate(), 0, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c'
from (select distinct VehicleMake from WireTestInput2) a1
full outer join (
	select distinct m.VehicleMakeName
	from dbo.VehicleMake m 
) a2 on a1.VehicleMake = a2.VehicleMakeName
where a2.VehicleMakeName is null



select * from VehicleMake
where VehicleMakeName like 'Mase%'

Denbeigh
Maserati

select * from WireTestInput2
where VehicleMake = 'Maserati / Chrysler'


select *
from (select distinct VehicleModel
from WireTestInput2) a1
full outer join (
	select distinct o.VehicleModelName
	from dbo.VehicleModel o 
) a2 on a1.VehicleModel = a2.VehicleModelName
where a2.VehicleModelName is null

select *
from (select distinct VehicleMake, VehicleModel
from WireTestInput2) a1
full outer join (
	select distinct m.VehicleMakeName, o.VehicleModelName
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
where a2.VehicleMakeName is null

select * from VehicleModel
where VehicleModelName like 'Ram%'

select * from WireTestInput2
where VehicleModel like 'Ram%'
and VehicleMake = 'Jeep'

select * from dbo.VehicleModel
where VehicleModelName like 'Town%'

select * from WireTestInput
where VehicleModel = 'Town and Country'

select * from WireTestInput2
where VehicleModel = 'Town and Country'


update WireTestInput2
set VehicleMake = 'Chrysler'
where VehicleModel like 'Town & Country'

update WireTestInput2
set VehicleModel = 'Town and Country'
where VehicleModel like 'Town & Country'

and VehicleMake = 'Jeep'

insert into dbo.VehicleModel
(VehicleModelName, UpdatedDt, UpdatedBy)
select a1.VehicleModel, getdate(), '8e3b28e4-309a-49f6-9bab-c6e49d17e61c'
from (select distinct VehicleMake, VehicleModel
from WireTestInput2) a1
full outer join (
	select distinct m.VehicleMakeName, o.VehicleModelName
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
where a2.VehicleMakeName is null

select * from VehicleModel




select *
from (select 
distinct VehicleMake, VehicleModel, VehicleYear
from WireTestInput2) a1
full outer join (
	select m.VehicleMakeName, o.VehicleModelName, a.VehicleYear
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
	and a1.VehicleYear = a2.VehicleYear
where a2.VehicleMakeName is null

select *
from (select 
distinct VehicleMake, VehicleModel, VehicleYear, m.VehicleMakeId, o.VehicleModelId
from WireTestInput2 a
	join dbo.VehicleMake m on a.VehicleMake = m.VehicleMakeName
	join dbo.VehicleModel o on a.VehicleModel = o.VehicleModelName 
) a1
full outer join (
	select m.VehicleMakeName, o.VehicleModelName, a.VehicleYear
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
	and a1.VehicleYear = a2.VehicleYear
where a2.VehicleMakeName is null


insert into dbo.VehicleMakeModelYear
(VehicleMakeId, VehicleModelId, VehicleYear, UpdatedDt, UpdatedBy)
select a1.VehicleMakeId, a1.VehicleModelId, a1.VehicleYear, getdate(), '8e3b28e4-309a-49f6-9bab-c6e49d17e61c'
from (select 
distinct VehicleMake, VehicleModel, VehicleYear, m.VehicleMakeId, o.VehicleModelId
from WireTestInput2 a
	join dbo.VehicleMake m on a.VehicleMake = m.VehicleMakeName
	join dbo.VehicleModel o on a.VehicleModel = o.VehicleModelName 
) a1
full outer join (
	select m.VehicleMakeName, o.VehicleModelName, a.VehicleYear
	from [dbo].[VehicleMakeModelYear] a
	join dbo.VehicleMake m on a.VehicleMakeId = m.VehicleMakeId
	join dbo.VehicleModel o on a.VehicleModelId = o.VehicleModelId 
) a2 on a1.VehicleMake = a2.VehicleMakeName
	and a1.VehicleModel = a2.VehicleModelName
	and a1.VehicleYear = a2.VehicleYear
where a2.VehicleMakeName is null


select * from [dbo].[WireFunction]

select count(*) from [dbo].[VehicleWireFunction]
select * 
into dbo.VehicleWireFunction_20160901
from [dbo].[VehicleWireFunction]

select top 10 * from [dbo].[VehicleWireFunction]

truncate table dbo.VehicleWireFunction


select *
from (select distinct InstallationType from WireTestInput2) a1
full outer join (
	select distinct m.InstallationTypeName
	from InstallationType m 
) a2 on a1.InstallationType = a2.InstallationTypeName
where a2.InstallationTypeName is null

select *
from (select distinct WireFunction from WireTestInput2) a1
full outer join (
	select distinct m.WireFunctionName
	from WireFunction m 
) a2 on a1.WireFunction = a2.WireFunctionName
where a2.WireFunctionName is null

select * from WireFunction

insert into dbo.WireFunction
(WireFunctionName, SortOrder, UpdatedDt, Inactive, Show, UpdatedBy)
select a1.WireFunction, 10000, getdate(), 0, 1, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c'
from (select distinct WireFunction from WireTestInput2) a1
full outer join (
	select distinct m.WireFunctionName
	from WireFunction m 
) a2 on a1.WireFunction = a2.WireFunctionName
where a2.WireFunctionName is null

12 Volts Constant
12 Volt Constant

update WireFunction
set WireFunctionName = 'CAN High'
where WireFunctionName = 'CAN  High'

update WireTestInput2
set WireFunction = 'CAN High'
where WireFunction = 'CAN  High'

select top 10 * from VehicleWireFunction_20160901

truncate table dbo.VehicleWireFunction

insert into dbo.VehicleWireFunction
(
	VehicleMakeModelYearId, WireFunctionId, InstallationTypeId,
	Colour, Location, Polarity, UpdatedDt, Inactive, VehicleColor, PinOut, UpdatedBy
)
select 
	vm.VehicleMakeModelYearId, c.WireFunctionId, d.InstallationTypeId, 
	a.VehicleColor, a.Location, a.Polarity, getdate() as UpdatedDt, 0 as Inactive,  a.Color, 
	a.PinOut, 
	'8e3b28e4-309a-49f6-9bab-c6e49d17e61c' UpdatedBy
from WireTestInput2 a
	join dbo.VehicleMake m on a.VehicleMake = m.VehicleMakeName
	join dbo.VehicleModel o on a.VehicleModel = o.VehicleModelName 
	join dbo.VehicleMakeModelYear vm on vm.VehicleMakeId = m.VehicleMakeId 
		and vm.VehicleModelId = o.VehicleModelId
		and vm.VehicleYear = a.VehicleYear
	left join dbo.InstallationType d on d.InstallationTypeName = a.InstallationType
	left join dbo.WireFunction c on c.WireFunctionName = a.WireFunction

select max(len(Color)), max(len(Location)), max(len(Polarity)), max(len(VehicleColor)),max(len(PinOut))
from WireTestInput2

78	133	66	184	119

alter table VehicleWireFunction alter column Colour nvarchar(200) null
alter table VehicleWireFunction alter column VehicleColor nvarchar(200) null
alter table VehicleWireFunction alter column PinOut nvarchar(200) null



select 
   e.VehicleMakeModelYearId,
   c.WireFunctionId,
   d.InstallationTypeId,
   a.Colour,
   a.Location,
   a.Polarity,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Inactive,
   a.Guid
from compusta_wmagic.dbo.Wires a
join dbo.VehicleMakeModelYear e on e.OldGuid = a.VehGuid
join dbo.WireFunction c on c.WireFunctionName = a.WireFunction
left join dbo.InstallationType d on d.InstallationTypeName = a.InstallationType
left join dbo.VehicleWireFunction b on a.GUID = b.OldGUID
where b.OldGUID is null



select * from InstallationType
select InstallationTypeId, InstallationTypeName from dbo.InstallationType with (nolock) where Inactive=0

exec proc_VehicleWireSearch 
@SaveFolder = '', 
@VehicleMakeModelYearId = 10375,
@InstallationTypeId = 9
