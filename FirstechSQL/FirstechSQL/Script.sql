﻿-- drop table dbo.Country
if object_id('dbo.Country') is null
begin
   create table dbo.Country
   (
      CountryCode varchar(10) not null,
      CountryName varchar(100) not null
   )

   alter table dbo.Country add constraint CountryPK primary key (CountryCode)
end

if not exists (select * from dbo.Country where CountryCode = 'UK')
   insert into dbo.Country select 'UK', 'United Kingdom'
if not exists (select * from dbo.Country where CountryCode = 'FR')
   insert into dbo.Country select 'FR', 'France'
if not exists (select * from dbo.Country where CountryCode = 'DE')
   insert into dbo.Country select 'DE', 'Germany'
if not exists (select * from dbo.Country where CountryCode = 'IT')
   insert into dbo.Country select 'IT', 'Italy'
if not exists (select * from dbo.Country where CountryCode = 'JP')
   insert into dbo.Country select 'JP', 'Japan'
if not exists (select * from dbo.Country where CountryCode = 'KR')
   insert into dbo.Country select 'KR', 'Korea'
if not exists (select * from dbo.Country where CountryCode = 'RS')
   insert into dbo.Country select 'RS', 'Serbia'
if not exists (select * from dbo.Country where CountryCode = 'ES')
   insert into dbo.Country select 'ES', 'Spain'
if not exists (select * from dbo.Country where CountryCode = 'SE')
   insert into dbo.Country select 'SE', 'Sweden'
if not exists (select * from dbo.Country where CountryCode = 'US')
   insert into dbo.Country select 'US', 'USA'



-- drop table dbo.VehicleMake
if object_id('dbo.VehicleMake') is null
begin
   create table dbo.VehicleMake
   (
      VehicleMakeId int not null identity,
     VehicleMakeName nvarchar(100) not null,
     CountryCode varchar(10) null,
     UpdatedBy nvarchar(128) null,
     UpdatedDt datetime not null,
     Inactive int not null,
     OldId int null
   )

   alter table dbo.VehicleMake add constraint VehicleMakePK primary key (VehicleMakeId)
end

/*
   alter table dbo.VehicleMake drop column UpdatedBy
   alter table dbo.VehicleMake  add UpdatedBy nvarchar(128) null
*/


insert into dbo.VehicleMake
(VehicleMakeName, CountryCode, UpdatedBy, UpdatedDt, Inactive, OldId)
select 
   a.Manufacturer as VehicleMakeName,
   c.CountryCode as CountryCode,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Inactive,
   a.Id
from compusta_wmagic.dbo.vehicleMake a
join dbo.Country c on case when a.Country = 'England' or a.Country = 'UK' then 'United Kingdom' else a.Country end = c.CountryName
left join dbo.VehicleMake b
   on a.Id = b.OldId
where b.OldId is null
order by a.Manufacturer

-- select * from dbo.VehicleMake



-- drop table dbo.VehicleModel
if object_id('dbo.VehicleModel') is null
begin
   create table dbo.VehicleModel
   (
      VehicleModelId int not null identity,
     VehicleModelName nvarchar(100) not null,
     UpdatedBy nvarchar(128) null,
     UpdatedDt datetime not null,
     OldId int null
   )

   alter table dbo.VehicleModel add constraint VehicleModelPK primary key (VehicleModelId)
end

/*
   alter table dbo.VehicleModel drop column UpdatedBy
   alter table dbo.VehicleModel  add UpdatedBy nvarchar(128) null
*/


insert into dbo.VehicleModel
(VehicleModelName, UpdatedBy, UpdatedDt, OldId)
select 
   a.Model,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Id
from compusta_wmagic.dbo.vehicleModel a
left join dbo.VehicleModel b
   on a.Id = b.OldId
where b.OldId is null
order by a.Model

-- select * from dbo.VehicleModel


-- drop table dbo.VehicleMakeModelYear
if object_id('dbo.VehicleMakeModelYear') is null
begin
    create table dbo.VehicleMakeModelYear
    (
        VehicleMakeModelYearId int not null identity,
        VehicleMakeId int not null,
        VehicleModelId int not null,
        VehicleYear int not null,
        UpdatedBy nvarchar(128) null,
        UpdatedDt datetime not null,
        OldId int null,
        OldGuid int null,
        Approved int not null default(0),
        Picture1 nvarchar(2000) null,
        ReservedBy nvarchar(128) null,
        ReservedAt datetime null, 
        SearchCount int not null default(0)
    )

    alter table dbo.VehicleMakeModelYear add constraint VehicleMakeModelYearPK primary key (VehicleMakeModelYearId)
    create unique index VehicleMakeModelYearIdx1 on dbo.VehicleMakeModelYear (VehicleMakeId, VehicleModelId, VehicleYear)

    alter table dbo.VehicleMakeModelYear add constraint FK_VehicleMakeModelYear_VehicleMakeId foreign key (VehicleMakeId) references dbo.VehicleMake(VehicleMakeId) not for replication
    alter table dbo.VehicleMakeModelYear add constraint FK_VehicleMakeModelYear_VehicleModelId foreign key (VehicleMakeId) references dbo.VehicleModel(VehicleModelId) not for replication
end

/*
   alter table dbo.VehicleMakeModelYear drop column UpdatedBy
   alter table dbo.VehicleMakeModelYear  add UpdatedBy nvarchar(128) null

   alter table dbo.VehicleMakeModelYear add Approved int not null default(0)
   update dbo.VehicleMakeModelYear set Approved = 1

   alter table dbo.VehicleMakeModelYear add Picture1 nvarchar(2000) null

   alter table dbo.VehicleMakeModelYear add ReservedBy nvarchar(128) null
   alter table dbo.VehicleMakeModelYear add ReservedAt datetime null

   alter table dbo.VehicleMakeModelYear add SearchCount int not null default(0)
*/



insert into dbo.VehicleMakeModelYear
(VehicleMakeId, VehicleModelId, VehicleYear, UpdatedBy, UpdatedDt, OldId, OldGuid)
select 
   vm.VehicleMakeId,
   va.VehicleModelId,
   a.vYear,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Id,
   a.vGUID
from compusta_wmagic.dbo.vehicle2Model a
left join dbo.VehicleMakeModelYear b on a.Id = b.OldId
left join dbo.VehicleMake vm on vm.OldId = a.vMakeID
left join dbo.VehicleModel va on va.OldId = a.vModelID
where b.OldId is null
order by a.vYear


/*
select *
from dbo.VehicleMakeModelYear a
join dbo.VehicleMake b on a.VehicleMakeId = b.VehicleMakeId
join dbo.VehicleModel c on a.VehicleModelId = c.VehicleModelId
join dbo.VehicleMakeModelYearBodyType bt on bt.VehicleMakeModelYearId = a.VehicleMakeModelYearId
join dbo.VehicleBodyType v on v.VehicleBodyTypeId = bt.VehicleBodyTypeId
where a.VehicleMakeId = 1
*/



-- drop table dbo.VehicleBodyType
if object_id('dbo.VehicleBodyType') is null
begin
   create table dbo.VehicleBodyType
   (
      VehicleBodyTypeId int not null identity,
      VehicleBodyTypeName nvarchar(100) not null,
      VehicleBodyTypeNote nvarchar(100) null,
      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      OldId int null
   )

   alter table dbo.VehicleBodyType add constraint VehicleBodyTypePK primary key (VehicleBodyTypeId)
end


insert into dbo.VehicleBodyType
(VehicleBodyTypeName, VehicleBodyTypeNote, UpdatedBy, UpdatedDt, OldId)
select 
   a.Type,
   a.Note,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Id
from compusta_wmagic.dbo.vehicleBodyType a
left join dbo.VehicleBodyType b
   on a.Id = b.OldId
where b.OldId is null
order by a.Type

-- select * from dbo.VehicleBodyType



-- drop table dbo.VehicleMakeModelYearBodyType
if object_id('dbo.VehicleMakeModelYearBodyType') is null
begin
   create table dbo.VehicleMakeModelYearBodyType
   (
      VehicleMakeModelYearBodyTypeId int not null identity,
      VehicleMakeModelYearId int not null,
      VehicleBodyTypeId int not null,
      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      OldId int null,
   )

   alter table dbo.VehicleMakeModelYearBodyType add constraint VehicleMakeModelYearBodyTypePK primary key (VehicleMakeModelYearBodyTypeId)
    create unique index VehicleMakeModelYearBodyTypeIdx1 on dbo.VehicleMakeModelYearBodyType (VehicleMakeModelYearId, VehicleBodyTypeId)
end


insert into dbo.VehicleMakeModelYearBodyType
(VehicleMakeModelYearId, VehicleBodyTypeId, UpdatedBy, UpdatedDt, OldId)
select 
   vm.VehicleMakeModelYearId,
   va.VehicleBodyTypeId,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Id
from compusta_wmagic.dbo.vehicle2BodyType a
left join dbo.VehicleMakeModelYearBodyType b on a.Id = b.OldId
left join dbo.VehicleMakeModelYear vm on vm.OldId = a.vToModelID
left join dbo.VehicleBodyType va on va.OldId = a.vBodyTypeID
where b.OldId is null
order by a.vToModelID

/*
select * from dbo.VehicleMakeModelYearBodyType
*/







-- drop table dbo.WireFunction
if object_id('dbo.WireFunction') is null
begin
   create table dbo.WireFunction
   (
      WireFunctionId int not null identity,
      WireFunctionName nvarchar(100) not null,
      SortOrder int not null,
      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      Inactive bit not null,
      Show bit not null,
      OldGUID int null
   )

   alter table dbo.WireFunction add constraint WireFunctionPK primary key (WireFunctionId)
end

/*
   alter table dbo.WireFunction drop column UpdatedBy
   alter table dbo.WireFunction  add UpdatedBy nvarchar(128) null
*/


insert into dbo.WireFunction
(WireFunctionName, SortOrder, UpdatedBy, UpdatedDt, Inactive, Show, OldGUID)
select 
   a.WireFunction,
   a.SortOrder,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Inactive,
   a.Show,
   a.Guid
from compusta_wmagic.dbo.WireFunctions a
left join dbo.WireFunction b
   on a.GUID = b.OldGUID
where b.OldGUID is null
order by a.WireFunction

-- select * from dbo.WireFunction


-- drop table dbo.InstallationType
if object_id('dbo.InstallationType') is null
begin
   create table dbo.InstallationType
   (
      InstallationTypeId int not null identity,
      InstallationTypeName nvarchar(100) not null,
      SortOrder int not null,
      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      Inactive bit not null,
      OldGUID int null
   )

   alter table dbo.InstallationType add constraint InstallationTypePK primary key (InstallationTypeId)
end

/*
   alter table dbo.InstallationType drop column UpdatedBy
   alter table dbo.InstallationType  add UpdatedBy nvarchar(128) null
*/


insert into dbo.InstallationType
(InstallationTypeName, SortOrder, UpdatedBy, UpdatedDt, Inactive, OldGUID)
select 
   a.InstallationType,
   a.SortOrder,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Inactive,
   a.Guid
from compusta_wmagic.dbo.InstallationTypes a
left join dbo.InstallationType b
   on a.GUID = b.OldGUID
where b.OldGUID is null
order by a.InstallationType

-- select * from dbo.InstallationType




-- drop table dbo.VehicleWireFunction
if object_id('dbo.VehicleWireFunction') is null
begin
   create table dbo.VehicleWireFunction
   (
      VehicleWireFunctionId int not null identity,

      VehicleMakeModelYearId int not null,
      WireFunctionId int not null,
      InstallationTypeId int null,

      Colour nvarchar(200) null,
      VehicleColor nvarchar(200) null,
      PinOut nvarchar(200) null,
      Location nvarchar(200) null,
      Polarity nvarchar(100) null,

      UpdatedBy nvarchar(128) not null,
      UpdatedDt datetime not null,
      Inactive bit not null,
      OldGUID int null,

      Attach1 nvarchar(2000) null,
      Attach2 nvarchar(2000) null,
      Attach3 nvarchar(2000) null,
      Attach4 nvarchar(2000) null,
      Attach5 nvarchar(2000) null,

      likecount int not null default(0),
      dislikecount int not null default(0),

      IsApproved int not null default(0),
      ApprovedDate datetime null,
      EditorWorkId int null
   )

   alter table dbo.VehicleWireFunction add constraint VehicleWireFunctionPK primary key (VehicleWireFunctionId)
   create index Idx_VehicleWireFunction_VehicleMakeModelYearId on dbo.VehicleWireFunction (VehicleMakeModelYearId)
   create index Idx_VehicleWireFunction_UpdatedDt on dbo.VehicleWireFunction (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])

   alter table dbo.VehicleWireFunction add constraint FK_VehicleWireFunction_WireFunctionId foreign key (WireFunctionId) references dbo.WireFunction(WireFunctionId) not for replication
   alter table dbo.VehicleWireFunction add constraint FK_VehicleWireFunction_InstallationTypeId foreign key (InstallationTypeId) references dbo.InstallationType(InstallationTypeId) not for replication
end

/*

alter table dbo.VehicleWireFunction add IsApproved int not null default(0)
alter table dbo.VehicleWireFunction add ApprovedDate datetime null

alter table dbo.VehicleWireFunction add EditorWorkId int null
*/

-- drop table dbo.VehicleWireFunctionLike
if object_id('dbo.VehicleWireFunctionLike') is null
begin
   create table dbo.VehicleWireFunctionLike
   (
      UserId nvarchar(128) not null,
      VehicleWireFunctionId int not null,
      LikeDisLike int not null,  -- 1: Like, 0: Dislike
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleWireFunctionLike add constraint VehicleWireFunctionLikePK primary key (VehicleWireFunctionId, UserId)
end

/*
alter table dbo.VehicleWireFunction alter column Attach1 nvarchar(2000) null
alter table dbo.VehicleWireFunction alter column Attach2 nvarchar(2000) null
alter table dbo.VehicleWireFunction alter column Attach3 nvarchar(2000) null
alter table dbo.VehicleWireFunction alter column Attach4 nvarchar(2000) null
alter table dbo.VehicleWireFunction alter column Attach5 nvarchar(2000) null

alter table VehicleWireFunction alter column Colour nvarchar(200) null
alter table VehicleWireFunction alter column VehicleColor nvarchar(200) null
alter table VehicleWireFunction alter column PinOut nvarchar(200) null


alter table VehicleWireFunction add likecount int not null default(0)
alter table VehicleWireFunction add dislikecount int not null default(0)

*/

--alter table dbo.VehicleWireFunction add VehicleColor nvarchar(100) null
--alter table dbo.VehicleWireFunction add PinOut nvarchar(100) null

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, Location, Polarity, UpdatedBy, UpdatedDt, Inactive, OldGUID)
select 
   e.VehicleMakeModelYearId,
   c.WireFunctionId,
   d.InstallationTypeId,
   a.Colour,
   a.Location,
   a.Polarity,
   0 as UpdatedBy,
   getdate() as UpdatedDt,
   a.Inactive,
   a.Guid
from compusta_wmagic.dbo.Wires a
join dbo.VehicleMakeModelYear e on e.OldGuid = a.VehGuid
join dbo.WireFunction c on c.WireFunctionName = a.WireFunction
left join dbo.InstallationType d on d.InstallationTypeName = a.InstallationType
left join dbo.VehicleWireFunction b on a.GUID = b.OldGUID
where b.OldGUID is null

-- select * from dbo.VehicleWireFunction



/*
select * from dbo.VehicleMakeModelYear
where VehicleMakeId = 68
and  VehicleModelId = 184

select * from dbo.VehicleMake
select * from dbo.VehicleModel
where VehicleModelName like '4R%'

insert into dbo.VehicleMakeModelYear
(VehicleMakeId, VehicleModelId, VehicleYear, UpdatedBy, UpdatedDt)
select 68, 184, 2015, 0, getdate()

select top 10 * from dbo.VehicleMakeModelYear 
order by 1 desc

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, (select WireFunctionId from WireFunction where WireFunctionName = '12 Volts'),
null,  'Red', 'Green', 'Ign. Switch', '', '8 Pin White Connector, Pin 7', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, (select WireFunctionId from WireFunction where WireFunctionName = 'Ignition'),
null,  'Green', 'Black', 'Ign. Switch', '', '8 Pin White Connector, Pin 6', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Ignition 2'),
   null,  'Blue', 'Purple', 'Ign. Switch', '', '8 Pin White Connector, Pin 4', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Starter')
   ,null,  'Yellow', 'Red', 'Ign. Switch', '', '8 Pin White Connector, Pin 8', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Starter 2')
   ,null,  'White', 'Black', 'Ign. Switch', '', '8 Pin White Connector, Pin 1', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Parklights')
   ,null,  'Green / White', 'Red', 'Headlamp Switch', '-', '20 Pin White Connector, Pin 18', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Horn')
   ,null,  'White', 'Grey', 'Steering Column', '', '12 Pin Black Connector, Pin 8', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'GROUND OUT (-)')
   ,null,  'Black + *Grey/Red', 'White / Black', 'Wiper Switch', '', '7 Pin White Connector, Pin 2', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'CANBUS HIGH')
   ,null,  'Brown / Red', 'Purple', 'OBD2', '', '16 Pin White Connector, Pin 6', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'CANBUS LOW')
   ,null,  'Brown / Yellow', 'White', 'OBD2', '', '16 Pin White Connector, Pin 14', 0, getdate(), 0



insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Immo. Data')
   ,null,  'Orange / Black', 'Green', 'OBD2', '', '16 Pin White Connector, Pin 7', 0, getdate(), 0

insert into dbo.VehicleWireFunction
(VehicleMakeModelYearId, WireFunctionId, InstallationTypeId, Colour, VehicleColor, Location, Polarity, PinOut, UpdatedBy, UpdatedDt, Inactive)
select 10807, 
   (select WireFunctionId from WireFunction where WireFunctionName = 'Doorlock Data')
   ,null,  'Orange / White', 'Pink', 'BCM', '', '30 Pin White Connector, Pin 26', 0, getdate(), 0


select * from dbo.WireFunction
where WireFunctionName like '%Doo%'

insert into dbo.WireFunction
(WireFunctionName, SortOrder, UpdatedBy, UpdatedDt, Inactive, Show)
select 'Immo. Data', 2000, 0, getdate(), 0, 1

insert into dbo.WireFunction
(WireFunctionName, SortOrder, UpdatedBy, UpdatedDt, Inactive, Show)
select 'Doorlock Data', 2010, 0, getdate(), 0, 1

*/


-- drop table dbo.VehicleWireNote
if object_id('dbo.VehicleWireNote') is null
begin
   create table dbo.VehicleWireNote
   (
      VehicleWireNoteId int not null identity,
      VehicleMakeModelYearId int not null,
      InstallationTypeId int null,
      Note nvarchar(max) null,

      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      EditorWorkId int null
   )

   alter table dbo.VehicleWireNote add constraint VehicleWireNotePK primary key (VehicleWireNoteId)
   create index Idx_VehicleWireNote_VehicleMakeModelYearId on dbo.VehicleWireNote (VehicleMakeModelYearId)
   create index Idx_VehicleWireNote_UpdatedDt on dbo.VehicleWireNote (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*

alter table dbo.VehicleWireNote add InstallationTypeId int null

*/

insert into dbo.VehicleWireNote
(VehicleMakeModelYearId, Note, UpdatedBy, UpdatedDt)
select 10807, '* Only required if OEM hood pin is NOT present', 0, getdate()




-- drop table dbo.VehicleWirePrep
if object_id('dbo.VehicleWirePrep') is null
begin
   create table dbo.VehicleWirePrep
   (
      VehicleWirePrepId int not null identity,
      VehicleMakeModelYearId int not null,
      Step int not null,
      Note nvarchar(max) null,
      Attach1 nvarchar(2000) null,
      Attach2 nvarchar(2000) null,
      Attach3 nvarchar(2000) null,
      Attach4 nvarchar(2000) null,
      Attach5 nvarchar(2000) null,

      UpdatedBy int not null,
      UpdatedDt datetime not null,

      EditorWorkId int null
   )

   alter table dbo.VehicleWirePrep add constraint VehicleWirePrepPK primary key (VehicleWirePrepId)
   create index Idx_VehicleWirePrep_VehicleMakeModelYearId on dbo.VehicleWirePrep (VehicleMakeModelYearId)
   create index Idx_VehicleWirePrep_UpdatedDt on dbo.VehicleWirePrep (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*

alter table dbo.VehicleWirePrep alter column Attach1 nvarchar(2000) null
alter table dbo.VehicleWirePrep alter column Attach2 nvarchar(2000) null
alter table dbo.VehicleWirePrep alter column Attach3 nvarchar(2000) null
alter table dbo.VehicleWirePrep alter column Attach4 nvarchar(2000) null
alter table dbo.VehicleWirePrep alter column Attach5 nvarchar(2000) null

insert into dbo.VehicleWirePrep
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 1, 'Combine and tape the following groups of wires: <br/>
<ul><li>12Volts, Ign,  Ign 2, Start, Start 2 (Red, Green, Blue, Yellow, White)</li>
<li>Immo. Data, CanH, CanL, Doorlock Data (Orange/Black, Brown/Red, Brown/Yellow, Orange/White)</li>
<li>Parklights (-), Horn (Green/White, White)</li>
<li>*Ground Input (Gray/Red)</li></ul>
*Without OEM hood status only', 
'Toyota_4Runner_2015_Pre_1.png', 'Toyota_4Runner_2015_Pre_2.png', null, null, null, 0, getdate()

insert into dbo.VehicleWirePrep
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 2, 'Open cover on CM7200 and cut Green/White loop, move jumper for white ACC wire to start postion', 
'Toyota_4Runner_2015_Pre_3.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWirePrep
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 3, 'Finished Product before install', 
'Toyota_4Runner_2015_Pre_4.png', null, null, null, null, 0, getdate()

select * from dbo.VehicleWirePrep
*/

-- drop table dbo.VehicleWireDisassembly
if object_id('dbo.VehicleWireDisassembly') is null
begin
   create table dbo.VehicleWireDisassembly
   (
      VehicleWireDisassemblyId int not null identity,
      VehicleMakeModelYearId int not null,
      Step int not null,
      Note nvarchar(max) null,
      Attach1 nvarchar(2000) null,
      Attach2 nvarchar(2000) null,
      Attach3 nvarchar(2000) null,
      Attach4 nvarchar(2000) null,
      Attach5 nvarchar(2000) null,

      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      EditorWorkId int null
   )

   alter table dbo.VehicleWireDisassembly add constraint VehicleWireDisassemblyPK primary key (VehicleWireDisassemblyId)
   create index Idx_VehicleWireDisassembly_VehicleMakeModelYearId on dbo.VehicleWireDisassembly (VehicleMakeModelYearId)
   create index Idx_VehicleWireDisassembly_UpdatedDt on dbo.VehicleWireDisassembly (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*
alter table dbo.VehicleWireDisassembly alter column Attach1 nvarchar(2000) null
alter table dbo.VehicleWireDisassembly alter column Attach2 nvarchar(2000) null
alter table dbo.VehicleWireDisassembly alter column Attach3 nvarchar(2000) null
alter table dbo.VehicleWireDisassembly alter column Attach4 nvarchar(2000) null
alter table dbo.VehicleWireDisassembly alter column Attach5 nvarchar(2000) null


insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 1, 'Remove scuff plate by placing fingers under interior side of panel and gently pull away and up to remove panel.', 
'Toyota_4Runner_2015_Disassembly_1.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 2, 'Remove thumbscrew from kick panel, and place fingers behind panel in same area and pull towards rear of vehicle to remove panel.', 
'Toyota_4Runner_2015_Disassembly_2.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 3, 'Remove trim from around Temp control knob by gripping both sides and pulling towards the rear of vehicle. Use small pry tool to open cover on lower dash trim.', 
'Toyota_4Runner_2015_Disassembly_3.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 4, 'Remove 2, 10MM bolts from lower dash pad and pull outward to remove.', 
'Toyota_4Runner_2015_Disassembly_4.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 5, 'Remove 4, 10MM bolts in airbag, carefully lift airbag outward and unclip OBD2 connector and unplug airbag connector by lifting yellow insert to release it.', 
'Toyota_4Runner_2015_Disassembly_5.png', 'Toyota_4Runner_2015_Disassembly_6.png', null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 6, 'Remove 2, Phillips screws from steering column shroud, and remove lower section of shroud.', 
'Toyota_4Runner_2015_Disassembly_7.png', null, null, null, null, 0, getdate()

insert into dbo.VehicleWireDisassembly
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 7, 'Open cover on driver’s side A-Pillar, to expose 1, 10MM bolt. Remove bolt and release A-Pillar trim.', 
'Toyota_4Runner_2015_Disassembly_8.png', 'Toyota_4Runner_2015_Disassembly_9.png', null, null, null, 0, getdate()

select * from dbo.VehicleWireDisassembly
*/

-- drop table dbo.VehicleWirePlacementRouting
if object_id('dbo.VehicleWirePlacementRouting') is null
begin
   create table dbo.VehicleWirePlacementRouting
   (
      VehicleWirePlacementRoutingId int not null identity,
      VehicleMakeModelYearId int not null,
      Step int not null,
      Note nvarchar(max) null,
      Attach1 nvarchar(2000) null,
      Attach2 nvarchar(2000) null,
      Attach3 nvarchar(2000) null,
      Attach4 nvarchar(2000) null,
      Attach5 nvarchar(2000) null,

      UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      EditorWorkId int null
   )

   alter table dbo.VehicleWirePlacementRouting add constraint VehicleWirePlacementRoutingPK primary key (VehicleWirePlacementRoutingId)
   create index Idx_VehicleWirePlacementRouting_VehicleMakeModelYearId on dbo.VehicleWirePlacementRouting (VehicleMakeModelYearId)
   create index Idx_VehicleWirePlacementRouting_UpdatedDt on dbo.VehicleWirePlacementRouting (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*
alter table dbo.VehicleWirePlacementRouting alter column Attach1 nvarchar(2000) null
alter table dbo.VehicleWirePlacementRouting alter column Attach2 nvarchar(2000) null
alter table dbo.VehicleWirePlacementRouting alter column Attach3 nvarchar(2000) null
alter table dbo.VehicleWirePlacementRouting alter column Attach4 nvarchar(2000) null
alter table dbo.VehicleWirePlacementRouting alter column Attach5 nvarchar(2000) null


insert into dbo.VehicleWirePlacementRouting
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 1, 'Gently Pry A-pillar trim outwards to release panel. Place antenna on windshield and route cable behind headliner and airbag; continue to route cable down A-pillar.', 
'Toyota_4Runner_2015_Placement_Routing_1.png', 'Toyota_4Runner_2015_Placement_Routing_2.png', 'Toyota_4Runner_2015_Placement_Routing_3.png', null, null, 0, getdate()

insert into dbo.VehicleWirePlacementRouting
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 2, 'Route antenna cable across dash over the steering column and into CM7200. Mount CM7200 above the gas pedal, route all wires forward.', 
'Toyota_4Runner_2015_Placement_Routing_4.png', 'Toyota_4Runner_2015_Placement_Routing_5.png', null, null, null, 0, getdate()

insert into dbo.VehicleWirePlacementRouting
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 3, 'Route Canbus wires over steering column to OBD2 harness, remaining wires will continue to steering column.', 
'Toyota_4Runner_2015_Placement_Routing_6.png', 'Toyota_4Runner_2015_Placement_Routing_7.png', 'Toyota_4Runner_2015_Placement_Routing_8.png', null, null, 0, getdate()

insert into dbo.VehicleWirePlacementRouting
(VehicleMakeModelYearId, Step, Note, Attach1, Attach2, Attach3, Attach4, Attach5, UpdatedBy, UpdatedDt)
select 10807, 4, '*Drone Install Only* If installing a drone with the CM7200, mount it above the BCM', 
'Toyota_4Runner_2015_Placement_Routing_9.png', null, null, null, null, 0, getdate()

select * from dbo.VehicleWirePlacementRouting
*/

if object_id('dbo.VehicleWireFacebook') is null
begin
   create table dbo.VehicleWireFacebook
   (
        [VehicleWireFacebookId] [int] IDENTITY(1,1) NOT NULL,
        [VehicleMakeModelYearId] [int] NOT NULL,
        [Note] [nvarchar](max) NULL,
        [URL] [nvarchar](500) NULL,
        [Attach1] [nvarchar](2000) NULL,
        [Attach2] [nvarchar](2000) NULL,
        [Attach3] [nvarchar](2000) NULL,
        [Attach4] [nvarchar](2000) NULL,
        [Attach5] [nvarchar](2000) NULL,
        [UpdatedBy] [nvarchar](128) NULL,
        [UpdatedDt] [datetime] NOT NULL,
        EditorWorkId int null
   )

   alter table dbo.VehicleWireFacebook add constraint VehicleWireFacebookPK primary key (VehicleWireFacebookId)
   create index Idx_VehicleWireFacebook_VehicleMakeModelYearId on dbo.VehicleWireFacebook (VehicleMakeModelYearId)
   create index Idx_VehicleWireFacebook_UpdatedDt on dbo.VehicleWireFacebook (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*
alter table dbo.VehicleWireFacebook alter column Attach1 nvarchar(2000) null
alter table dbo.VehicleWireFacebook alter column Attach2 nvarchar(2000) null
alter table dbo.VehicleWireFacebook alter column Attach3 nvarchar(2000) null
alter table dbo.VehicleWireFacebook alter column Attach4 nvarchar(2000) null
alter table dbo.VehicleWireFacebook alter column Attach5 nvarchar(2000) null

*/


-- drop table dbo.VehicleDocument
if object_id('dbo.VehicleDocument') is null
begin
   create table dbo.VehicleDocument
   (
        VehicleDocumentId int IDENTITY NOT NULL,
        VehicleMakeModelYearId int NOT NULL,
        DocumentName nvarchar(200) NOT NULL,
        AttachFile nvarchar(200) NULL,
        UpdatedBy nvarchar(128) NULL,
        UpdatedDt datetime NOT NULL,
        EditorWorkId int null
   )

   alter table dbo.VehicleDocument add constraint VehicleDocumentPK primary key (VehicleDocumentId)
   create index Idx_VehicleDocument_VehicleMakeModelYearId on dbo.VehicleDocument (VehicleMakeModelYearId)
end



if object_id('dbo.Document') is null
begin
    CREATE TABLE [dbo].[Document](
        [DocumentId] [int] IDENTITY(1,1) NOT NULL,
        [ParentDocumentId] [int] NOT NULL,
        [DocumentName] [nvarchar](200) NOT NULL,
        [DocumentType] [int] NOT NULL,
        [AttachFile] [nvarchar](2000) NULL,
        [UpdatedBy] [nvarchar](128) NULL,
        [UpdatedDt] [datetime] NOT NULL,
        [SortOrder] [int] NOT NULL default(1)
    )

   alter table dbo.Document add constraint DocumentPK primary key (DocumentId)
end

/*

alter table dbo.Document alter column AttachFile nvarchar(2000) null

alter table dbo.Document add [SortOrder] [int] NOT NULL default(1)
*/


if object_id('dbo.Banner') is null
begin
    CREATE TABLE [dbo].Banner (
        [BannerId] [int] IDENTITY(1,1) NOT NULL,
        [BannerLocation] [int] NOT NULL,
        [BannerCaption] [nvarchar](500) NULL,
        [BannerImage] [nvarchar](500) NULL,
        [BannerURL] [nvarchar](1000) NULL,
        [UpdatedBy] [nvarchar](128) NULL,
        [UpdatedDt] [datetime] NOT NULL,
        [SortOrder] [int] NOT NULL,
        [BannerImageMobile] [nvarchar](500) NULL
    )

   alter table dbo.Banner add constraint BannerPK primary key (BannerId)
end



if object_id('dbo.Newsfeed') is null
begin

    CREATE TABLE [dbo].[Newsfeed](
        [NewsfeedId] [int] IDENTITY(1,1) NOT NULL,
        [NewsfeedTitle] [nvarchar](200) NOT NULL,
        [NewsfeedUrl] [nvarchar](500) NULL,
        [NewsfeedContent] [nvarchar](max) NOT NULL,
        [NewsfeedDt] [date] NOT NULL,
        [NewsfeedImage] [nvarchar](2000) NULL,
        [UpdatedBy] [nvarchar](128) NULL,
        [UpdatedDt] [datetime] NOT NULL
    )

   alter table dbo.Newsfeed add constraint NewsfeedPK primary key (NewsfeedId)
end

/*
alter table dbo.Newsfeed alter column NewsfeedImage nvarchar(2000) null


alter table dbo.Newsfeed add [NewsfeedImage] [nvarchar](500) NULL
*/



-- drop table dbo.VehicleUrgentMessage
if object_id('dbo.VehicleUrgentMessage') is null
begin
    create table dbo.VehicleUrgentMessage
    (
        VehicleUrgentMessageId int not null identity,
        VehicleMakeModelYearId int not null,
        Note nvarchar(300) null,

        UpdatedBy [nvarchar](128) NULL,
        UpdatedDt datetime not null
    )

    alter table dbo.VehicleUrgentMessage add constraint VehicleUrgentMessagePK primary key (VehicleUrgentMessageId)
    create unique index VehicleUrgentMessageIdx1 on dbo.VehicleUrgentMessage (VehicleMakeModelYearId)
end



-- drop table dbo.VehicleComment
if object_id('dbo.VehicleComment') is null
begin
    create table dbo.VehicleComment
    (
        VehicleCommentId int not null identity,
        VehicleMakeModelYearId int not null,
        Comment nvarchar(max) null,

        UpdatedBy [nvarchar](128) NULL,
        UpdatedDt datetime not null,

        likecount int not null default(0),
        dislikecount int not null default(0),
        NoOfReplies int not null default(0),
        ParentVehicleCommentId int not null default(0),
        IsReported int not null default(0),
        OriginalImgFilename nvarchar(500) null,
        ImgFilename nvarchar(500) null
    )

    alter table dbo.VehicleComment add constraint VehicleCommentPK primary key (VehicleCommentId)
    create index VehicleCommentIdx1 on dbo.VehicleComment (VehicleMakeModelYearId)
    create index Idx_VehicleComment_UpdatedDt on dbo.VehicleComment (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end

/*
alter table dbo.VehicleComment add likecount int not null default(0)
alter table dbo.VehicleComment add dislikecount int not null default(0)
alter table dbo.VehicleComment add NoOfReplies int not null default(0)
alter table dbo.VehicleComment add ParentVehicleCommentId int not null default(0)

alter table dbo.VehicleComment add IsReported int not null default(0)

alter table dbo.VehicleComment add OriginalImgFilename nvarchar(500) null
alter table dbo.VehicleComment add ImgFilename nvarchar(500) null

*/


-- drop table dbo.VehicleCommentLike
if object_id('dbo.VehicleCommentLike') is null
begin
   create table dbo.VehicleCommentLike
   (
      UserId nvarchar(128) not null,
      VehicleCommentId int not null,
      LikeDisLike int not null,  -- 1: Like, 0: Dislike
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleCommentLike add constraint VehicleCommentLikePK primary key (VehicleCommentId, UserId)
end

-- drop table dbo.VehicleCommentReport
if object_id('dbo.VehicleCommentReport') is null
begin
   create table dbo.VehicleCommentReport
   (
      UserId nvarchar(128) not null,
      VehicleCommentId int not null,
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleCommentReport add constraint VehicleCommentReportPK primary key (VehicleCommentId, UserId)
end




-- drop table dbo.VehicleInternalNote
if object_id('dbo.VehicleInternalNote') is null
begin
    create table dbo.VehicleInternalNote
    (
        VehicleInternalNoteId int not null identity,
        VehicleMakeModelYearId int not null,
        InternalNote nvarchar(max) null,

        UpdatedBy [nvarchar](128) NULL,
        UpdatedDt datetime not null
    )

    alter table dbo.VehicleInternalNote add constraint VehicleInternalNotePK primary key (VehicleInternalNoteId)
    create index VehicleInternalNoteIdx1 on dbo.VehicleInternalNote (VehicleMakeModelYearId)
    create index Idx_VehicleInternalNote_UpdatedDt on dbo.VehicleInternalNote (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end




-- drop table dbo.VehicleLink
if object_id('dbo.VehicleLink') is null
begin
    create table dbo.VehicleLink
    (
        VehicleLinkId int not null identity,
        VehicleMakeModelYearId int not null,
        LinkVehicleMakeModelYearId int not null,

        UpdatedBy [nvarchar](128) NULL,
        UpdatedDt datetime not null
    )

    alter table dbo.VehicleLink add constraint VehicleLinkPK primary key (VehicleLinkId)
    create unique index VehicleLinkIdx1 on dbo.VehicleLink (VehicleMakeModelYearId, LinkVehicleMakeModelYearId)
end



update InstallationType
set Inactive = 1
where InstallationTypeId not in (6, 9)



-- drop table dbo.SearchCounter
if object_id('dbo.SearchCounter') is null
begin
    create table dbo.SearchCounter
    (
        VehicleMakeModelYearId int not null,
        SearchDt datetime not null default(getdate())
    )

    create unique index SearchCounterIdx1 on dbo.SearchCounter (SearchDt)
end





-- drop table dbo.VehicleWireProgramming
if object_id('dbo.VehicleWireProgramming') is null
begin
   create table dbo.VehicleWireProgramming
   (
      VehicleWireProgrammingId int not null identity,
      VehicleMakeModelYearId int not null,
      Step int not null,
      Note nvarchar(max) null,
      Attach1 nvarchar(2000) null,
      Attach2 nvarchar(2000) null,
      Attach3 nvarchar(2000) null,
      Attach4 nvarchar(2000) null,
      Attach5 nvarchar(2000) null,

     UpdatedBy nvarchar(128) null,
      UpdatedDt datetime not null,
      EditorWorkId int null
   )

   alter table dbo.VehicleWireProgramming add constraint VehicleWireProgrammingPK primary key (VehicleWireProgrammingId)
   create index Idx_VehicleWireProgramming_VehicleMakeModelYearId on dbo.VehicleWireProgramming (VehicleMakeModelYearId)
   create index Idx_VehicleWireProgramming_UpdatedDt on dbo.VehicleWireProgramming (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
end




-- drop table dbo.Disclaimer
if object_id('dbo.Disclaimer') is null
begin
    create table dbo.Disclaimer
    (
        Disclaimer nvarchar(max) null,
        UpdatedBy nvarchar(128) null,
        UpdatedDt datetime not null
    )
end




-- drop table dbo.SubmitTip
if object_id('dbo.SubmitTip') is null
begin
    create table dbo.SubmitTip
    (
        SubmitTipId int not null identity,
        SubmitBy nvarchar(128) null,
        SubmitDt datetime not null,

        TypeOfTip varchar(20) not null,
        VehicleYear nvarchar(20) not null,
        VehicleMake nvarchar(30) not null,
        VehicleModel nvarchar(50) not null,
        KeyStyle nvarchar(50) null,

        UpdatedFilepath nvarchar(500) null,
        TipDescription nvarchar(4000) null
   )

   alter table dbo.SubmitTip add constraint SubmitTipPK primary key (SubmitTipId)
   create index Idx_SubmitTip_SubmitBy on dbo.SubmitTip (SubmitDt, SubmitBy)
end


-- drop table dbo.UserProfile
if object_id('dbo.UserProfile') is null
begin
    create table dbo.UserProfile
    (
        UserId nvarchar(128) not null,
        ProfileName nvarchar(200) not null,
        ProfilePicture nvarchar(500) null,
        RegistrationDate datetime not null default(getdate()),
        ApproveDate datetime null,
        NoOfPosts int not null default(0),
        RepZipCodes nvarchar(500) null,
        RepCities nvarchar(500) null,
        RepStates nvarchar(500) null,
        EditorMTCRepcode nvarchar(100) null,
        CommentCount int not null default(0),
        RankId int not null default(1)
   )

   alter table dbo.UserProfile add constraint UserProfilePK primary key (UserId)
end

/*
alter table dbo.UserProfile add ProfilePicture nvarchar(500) null
alter table dbo.UserProfile add RegistrationDate datetime not null default(getdate())
alter table dbo.UserProfile add NoOfPosts int not null default(0)


alter table dbo.UserProfile add RepZipCodes nvarchar(500) null
alter table dbo.UserProfile add RepCities nvarchar(500) null
alter table dbo.UserProfile add RepStates nvarchar(500) null

alter table dbo.UserProfile add EditorMTCRepcode nvarchar(100) null
*/






-- drop table dbo.Rank
if object_id('dbo.Rank') is null
begin
    create table dbo.Rank
    (
        RankId int identity not null,
        RankNo int not null,
        RankName nvarchar(200) not null,
        MinHit int not null,
        MaxHit int not null,
        UpdatedBy nvarchar(128) null,
        UpdatedDt datetime not null
   )

   alter table dbo.Rank add constraint RankPK primary key (RankId)
end

create table #Rank
(
    RankId int not null,
    RankNo int not null,
    RankName nvarchar(200) not null,
    MinHit int not null,
    MaxHit int not null,
    UpdatedBy nvarchar(128) null,
    UpdatedDt datetime not null
)
    
insert into #Rank
select 1, 1, 'FNG', 0, 24, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 2, 2, 'Private', 25, 49, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 3, 3, 'Private Frist Class', 50, 74, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 4, 4, 'Lance Corporal', 75, 99, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 5, 5, 'Corporal', 100, 124, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 6, 6, 'Sergeant', 125, 174, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 7, 7, 'Staff Sergeant', 175, 224, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 8, 8, 'Gunnery Sergeant', 225, 274, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 9, 9, 'Master Sergeant', 275, 324, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 10, 10, 'First Sergeant', 325, 374, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 11, 11, 'Master Gunnery Sergeant', 375, 449, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 12, 12, 'Sergeant Major', 450, 524, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 13, 13, '2nd Lieutenant', 525, 599, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 14, 14, '1sr Lieutenant', 600, 674, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 15, 15, 'Captain', 675, 749, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 16, 16, 'Major', 750, 849, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 17, 17, 'Lieutenant Colonel', 850, 949, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 18, 18, 'Colonel', 950, 1049, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 19, 19, 'Brigadier General', 1050, 1149, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 20, 20, 'Major General', 1150, 1249, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 21, 21, 'Lieutenant General', 1250, 1499, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 22, 22, 'General', 1500, 1999, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 23, 23, 'Commander', 2000, 100000000, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() 


set identity_insert dbo.Rank on

insert into dbo.Rank
(RankId, RankNo, RankName, MinHit, MaxHit, UpdatedBy, UpdatedDt)
select a.RankId, a.RankNo, a.RankName, a.MinHit, a.MaxHit, a.UpdatedBy, a.UpdatedDt
from #Rank a
left join dbo.Rank b on a.RankId = b.RankId
where b.RankId is null

set identity_insert dbo.Rank off

update a
set a.RankNo = b.RankNo, a.RankName = b.RankName, 
    a.MinHit = b.MinHit, a.MaxHit = b.MaxHit, a.UpdatedBy=b.UpdatedBy, a.UpdatedDt=b.UpdatedDt
from dbo.Rank a
join #Rank b on a.RankId = b.RankId

delete a
from dbo.Rank a
left join #Rank b on a.RankId = b.RankId
where b.RankId is null








-- drop table dbo.EditorWork
if object_id('dbo.EditorWork') is null
begin
    create table dbo.EditorWork
    (
        EditorWorkId int not null identity,
        EditorId nvarchar(128) not null,
        EditorWorkDt datetime not null,
        EditorWorkAction int not null, -- 0: Add, 1: Edit, 2: Delete
        StatusId int not null default(0), -- 0: Draft, 1: Approved, 2: Denied
        ApprovedBy nvarchar(128) null,
        ApprovedDt datetime null,
        DeniedBy nvarchar(128) null,
        DeniedDt datetime null
    )


   alter table dbo.EditorWork add constraint EditorWorkPK primary key (EditorWorkId)
end

-- drop table dbo.VehicleWireFunctionEditor
if object_id('dbo.VehicleWireFunctionEditor') is null
begin
    create table dbo.VehicleWireFunctionEditor
    (
        VehicleWireFunctionEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireFunctionId int null,

        VehicleMakeModelYearId int null,
        WireFunctionId int null,
        InstallationTypeId int null,

        Colour nvarchar(200) null,
        VehicleColor nvarchar(200) null,
        PinOut nvarchar(200) null,
        Location nvarchar(200) null,
        Polarity nvarchar(100) null,

        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
   )

   alter table dbo.VehicleWireFunctionEditor add constraint VehicleWireFunctionEditorIdPK primary key (VehicleWireFunctionEditorId)
   create index Idx_VehicleWireFunctionEditor_EditorWorkId on dbo.VehicleWireFunctionEditor (EditorWorkId, VehicleWireFunctionId)

   alter table dbo.VehicleWireFunctionEditor add constraint FK_VehicleWireFunctionEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


-- drop table dbo.VehicleWireDisassemblyEditor
if object_id('dbo.VehicleWireDisassemblyEditor') is null
begin
    create table dbo.VehicleWireDisassemblyEditor
    (
        VehicleWireDisassemblyEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireDisassemblyId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null,
   )

   alter table dbo.VehicleWireDisassemblyEditor add constraint VehicleWireDisassemblyEditorPK primary key (VehicleWireDisassemblyEditorId)
   create index Idx_VehicleWireDisassemblyEditory_VehicleMakeModelYearId on dbo.VehicleWireDisassemblyEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireDisassemblyEditor add constraint FK_VehicleWireDisassemblyEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end



-- drop table dbo.VehicleWireFacebookEditor
if object_id('dbo.VehicleWireFacebookEditor') is null
begin
    create table dbo.VehicleWireFacebookEditor
    (
        VehicleWireFacebookEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireFacebookId int NULL,

        VehicleMakeModelYearId int NOT NULL,
        Note nvarchar(max) NULL,
        [URL] nvarchar(500) NULL,
        Attach1 nvarchar(2000) NULL,
        Attach2 nvarchar(2000) NULL,
        Attach3 nvarchar(2000) NULL,
        Attach4 nvarchar(2000) NULL,
        Attach5 nvarchar(2000) NULL
   )

   alter table dbo.VehicleWireFacebookEditor add constraint VehicleWireFacebookEditorPK primary key (VehicleWireFacebookEditorId)
   create index Idx_VehicleWireFacebookEditor_VehicleMakeModelYearId on dbo.VehicleWireFacebookEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireFacebookEditor add constraint FK_VehicleWireFacebookEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end




-- drop table dbo.VehicleDocumentEditor
if object_id('dbo.VehicleDocumentEditor') is null
begin
   create table dbo.VehicleDocumentEditor
   (
        VehicleDocumentEditorId int IDENTITY NOT NULL,
        EditorWorkId int not null,
        VehicleDocumentId int NULL,

        VehicleMakeModelYearId int NOT NULL,
        DocumentName nvarchar(200) NOT NULL,
        AttachFile nvarchar(200) NULL,
   )

   alter table dbo.VehicleDocumentEditor add constraint VehicleDocumentEditorPK primary key (VehicleDocumentEditorId)
   create index Idx_VehicleDocumentEditor_VehicleMakeModelYearId on dbo.VehicleDocumentEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleDocumentEditor add constraint FK_VehicleDocumentEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end





-- drop table dbo.VehicleWirePrepEditor
if object_id('dbo.VehicleWirePrepEditor') is null
begin
    create table dbo.VehicleWirePrepEditor
    (
        VehicleWirePrepEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWirePrepId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWirePrepEditor add constraint VehicleWirePrepEditorPK primary key (VehicleWirePrepEditorId)
    create index Idx_VehicleWirePrepEditor_VehicleMakeModelYearId on dbo.VehicleWirePrepEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWirePrepEditor add constraint FK_VehicleWirePrepEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


-- drop table dbo.VehicleWirePlacementRoutingEditor
if object_id('dbo.VehicleWirePlacementRoutingEditor') is null
begin
    create table dbo.VehicleWirePlacementRoutingEditor
    (
        VehicleWirePlacementRoutingEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWirePlacementRoutingId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWirePlacementRoutingEditor add constraint VehicleWirePlacementRoutingEditorPK primary key (VehicleWirePlacementRoutingEditorId)
    create index Idx_VehicleWirePlacementRoutingEditor_VehicleMakeModelYearId on dbo.VehicleWirePlacementRoutingEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWirePlacementRoutingEditor add constraint FK_VehicleWirePlacementRoutingEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


-- drop table dbo.VehicleWireProgrammingEditor
if object_id('dbo.VehicleWireProgrammingEditor') is null
begin
    create table dbo.VehicleWireProgrammingEditor
    (
        VehicleWireProgrammingEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireProgrammingId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWireProgrammingEditor add constraint VehicleWireProgrammingEditorPK primary key (VehicleWireProgrammingEditorId)
    create index Idx_VehicleWireProgrammingEditor_VehicleMakeModelYearId on dbo.VehicleWireProgrammingEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireProgrammingEditor add constraint FK_VehicleWireProgrammingEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


-- drop table dbo.VehicleWireNoteEditor
if object_id('dbo.VehicleWireNoteEditor') is null
begin
    create table dbo.VehicleWireNoteEditor
    (
        VehicleWireNoteEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireNoteId int null,

        VehicleMakeModelYearId int not null,
        InstallationTypeId int null,
        Note nvarchar(max) null
    )

    alter table dbo.VehicleWireNoteEditor add constraint VehicleWireNoteEditorPK primary key (VehicleWireNoteEditorId)
    create index Idx_VehicleWireNoteEditor_VehicleMakeModelYearId on dbo.VehicleWireNoteEditor (VehicleMakeModelYearId)

    alter table dbo.VehicleWireNoteEditor add constraint FK_VehicleWireNoteEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end

