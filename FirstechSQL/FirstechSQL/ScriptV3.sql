﻿
alter table dbo.Banner add [BannerImageMobile] [nvarchar](500) NULL




create index Idx_VehicleWireFunction_UpdatedDt on dbo.VehicleWireFunction (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWireNote_UpdatedDt on dbo.VehicleWireNote (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWirePrep_UpdatedDt on dbo.VehicleWirePrep (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWireDisassembly_UpdatedDt on dbo.VehicleWireDisassembly (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWirePlacementRouting_UpdatedDt on dbo.VehicleWirePlacementRouting (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWireFacebook_UpdatedDt on dbo.VehicleWireFacebook (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleWireProgramming_UpdatedDt on dbo.VehicleWireProgramming (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleInternalNote_UpdatedDt on dbo.VehicleInternalNote (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])
create index Idx_VehicleComment_UpdatedDt on dbo.VehicleComment (UpdatedDt) INCLUDE ([VehicleMakeModelYearId])



-- drop table dbo.Disclaimer
if object_id('dbo.Disclaimer') is null
begin
    create table dbo.Disclaimer
    (
        Disclaimer nvarchar(max) null,
        UpdatedBy nvarchar(128) null,
        UpdatedDt datetime not null
    )
end



-- Add account cstocklin@compustar.com
-- Add TipEmail to web.config


alter table dbo.VehicleMakeModelYear add Approved int not null default(0)
update dbo.VehicleMakeModelYear set Approved = 1
alter table dbo.VehicleMakeModelYear add Picture1 nvarchar(2000) null

alter table dbo.VehicleMakeModelYear add ReservedBy nvarchar(128) null
alter table dbo.VehicleMakeModelYear add ReservedAt datetime null



alter table VehicleWireFunction add likecount int not null default(0)
alter table VehicleWireFunction add dislikecount int not null default(0)



alter table dbo.VehicleWireFunction add IsApproved int not null default(0)
alter table dbo.VehicleWireFunction add ApprovedDate datetime null

update dbo.VehicleWireFunction set IsApproved = 1




-- drop table dbo.VehicleWireFunctionLike
if object_id('dbo.VehicleWireFunctionLike') is null
begin
   create table dbo.VehicleWireFunctionLike
   (
      UserId nvarchar(128) not null,
      VehicleWireFunctionId int not null,
      LikeDisLike int not null,  -- 1: Like, 0: Dislike
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleWireFunctionLike add constraint VehicleWireFunctionLikePK primary key (VehicleWireFunctionId, UserId)
end


update InstallationType
set InstallationTypeName = 'Vehicle Wiring'
where rtrim(InstallationTypeName) like 'Remote Start'

update InstallationType
set InstallationTypeName = 'Vehicle Wiring w/ Blade AL'
where InstallationTypeName like 'Remote Start w/ Blade AL'

update InstallationType
set InstallationTypeName = 'CM-7X00 w/ Blade AL'
where InstallationTypeName like 'Vehicle Wiring w/ Blade AL'


set identity_insert dbo.InstallationType on 

insert into dbo.InstallationType
(InstallationTypeId, InstallationTypeName, SortOrder, UpdatedDt, Inactive, OldGUID, UpdatedBy)
select 2, 'Audio', 20, '2015-09-04 20:59:53.923',	0, 4908, NULL

set identity_insert dbo.InstallationType off


update InstallationType
set SortOrder = 1
where InstallationTypeName like 'CM-7X00 w/ Blade AL'

update InstallationType
set SortOrder = 2
where InstallationTypeName like 'Vehicle Wiring'


update InstallationType
set Inactive = 0, SortOrder = 4
where InstallationTypeName like 'Audio'


update InstallationType
set Inactive = 0, SortOrder = 3
where InstallationTypeName like 'Memento' or InstallationTypeName like 'Momento'


update InstallationType
set InstallationTypeName = 'Momento'
where InstallationTypeName like 'Memento'


-- drop table dbo.SubmitTip
if object_id('dbo.SubmitTip') is null
begin
    create table dbo.SubmitTip
    (
        SubmitTipId int not null identity,
        SubmitBy nvarchar(128) null,
        SubmitDt datetime not null,

        TypeOfTip varchar(20) not null,
        VehicleYear nvarchar(20) not null,
        VehicleMake nvarchar(30) not null,
        VehicleModel nvarchar(50) not null,
        KeyStyle nvarchar(50) null,

        UpdatedFilepath nvarchar(500) null,
        TipDescription nvarchar(4000) null
   )

   alter table dbo.SubmitTip add constraint SubmitTipPK primary key (SubmitTipId)
   create index Idx_SubmitTip_SubmitBy on dbo.SubmitTip (SubmitDt, SubmitBy)
end

/*
alter table dbo.SubmitTip drop column VehicleTrim
alter table dbo.SubmitTip add KeyStyle nvarchar(50) null
*/

-- drop table dbo.UserProfile
if object_id('dbo.UserProfile') is null
begin
    create table dbo.UserProfile
    (
        UserId nvarchar(128) not null,
        ProfileName nvarchar(200) not null,
        ProfilePicture nvarchar(500) null,
        RegistrationDate datetime not null default(getdate()),
        ApproveDate datetime null,
        NoOfPosts int not null default(0),
        RepZipCodes nvarchar(500) null,
        RepCities nvarchar(500) null,
        RepStates nvarchar(500) null,
        EditorMTCRepcode nvarchar(100) null,
        RankId int not null default(1),
        Country nvarchar(50) null,
        WhereBuyProduct nvarchar(100) null
   )

    alter table dbo.UserProfile add constraint UserProfilePK primary key (UserId)
    create index UserProfileIdx1 on dbo.UserProfile (ProfileName)
end

/*
alter table dbo.UserProfile add ProfilePicture nvarchar(500) null
alter table dbo.UserProfile add RegistrationDate datetime not null default(getdate())
alter table dbo.UserProfile add NoOfPosts int not null default(0)


alter table dbo.UserProfile add RepZipCodes nvarchar(500) null
alter table dbo.UserProfile add RepCities nvarchar(500) null
alter table dbo.UserProfile add RepStates nvarchar(500) null

alter table dbo.UserProfile add EditorMTCRepcode nvarchar(100) null

alter table dbo.UserProfile add RankId int not null default(1)
alter table dbo.UserProfile add Country nvarchar(50) null

alter table dbo.UserProfile add WhereBuyProduct nvarchar(100) null
*/

insert into dbo.UserProfile
(UserId, ProfileName)
select a.Id, a.FirstName + '@' + isnull(a.StoreName, '')
from dbo.AspNetUsers a
left join dbo.UserProfile b on a.Id = b.UserId
where b.UserId is null


-- Copy noprofile.png from Attachment folder

-- Need to update UserProfile.RegistrationDate for existing Users


alter table dbo.VehicleComment add likecount int not null default(0)
alter table dbo.VehicleComment add dislikecount int not null default(0)
alter table dbo.VehicleComment add NoOfReplies int not null default(0)
alter table dbo.VehicleComment add ParentVehicleCommentId int not null default(0)
alter table dbo.VehicleComment add IsReported int not null default(0)

-- drop table dbo.VehicleCommentLike
if object_id('dbo.VehicleCommentLike') is null
begin
   create table dbo.VehicleCommentLike
   (
      UserId nvarchar(128) not null,
      VehicleCommentId int not null,
      LikeDisLike int not null,  -- 1: Like, 0: Dislike
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleCommentLike add constraint VehicleCommentLikePK primary key (VehicleCommentId, UserId)
end



-- drop table dbo.VehicleCommentReport
if object_id('dbo.VehicleCommentReport') is null
begin
   create table dbo.VehicleCommentReport
   (
      UserId nvarchar(128) not null,
      VehicleCommentId int not null,
      UpdatedDt datetime not null
   )

   alter table dbo.VehicleCommentReport add constraint VehicleCommentReportPK primary key (VehicleCommentId, UserId)
end



-- drop table dbo.Rank
if object_id('dbo.Rank') is null
begin
    create table dbo.Rank
    (
        RankId int identity not null,
        RankNo int not null,
        RankName nvarchar(200) not null,
        MinHit int not null,
        MaxHit int not null,
        UpdatedBy nvarchar(128) null,
        UpdatedDt datetime not null
   )

   alter table dbo.Rank add constraint RankPK primary key (RankId)
end

create table #Rank
(
    RankId int not null,
    RankNo int not null,
    RankName nvarchar(200) not null,
    MinHit int not null,
    MaxHit int not null,
    UpdatedBy nvarchar(128) null,
    UpdatedDt datetime not null
)
    
insert into #Rank
select 1, 1, 'FNG', 0, 24, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 2, 2, 'Private', 25, 49, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 3, 3, 'Private Frist Class', 50, 74, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 4, 4, 'Lance Corporal', 75, 99, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 5, 5, 'Corporal', 100, 124, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 6, 6, 'Sergeant', 125, 174, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 7, 7, 'Staff Sergeant', 175, 224, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 8, 8, 'Gunnery Sergeant', 225, 274, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 9, 9, 'Master Sergeant', 275, 324, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 10, 10, 'First Sergeant', 325, 374, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 11, 11, 'Master Gunnery Sergeant', 375, 449, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 12, 12, 'Sergeant Major', 450, 524, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 13, 13, '2nd Lieutenant', 525, 599, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 14, 14, '1sr Lieutenant', 600, 674, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 15, 15, 'Captain', 675, 749, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 16, 16, 'Major', 750, 849, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 17, 17, 'Lieutenant Colonel', 850, 949, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 18, 18, 'Colonel', 950, 1049, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 19, 19, 'Brigadier General', 1050, 1149, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 20, 20, 'Major General', 1150, 1249, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 21, 21, 'Lieutenant General', 1250, 1499, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 22, 22, 'General', 1500, 1999, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() union all
select 23, 23, 'Commander', 2000, 100000000, '8e3b28e4-309a-49f6-9bab-c6e49d17e61c', getdate() 


set identity_insert dbo.Rank on

insert into dbo.Rank
(RankId, RankNo, RankName, MinHit, MaxHit, UpdatedBy, UpdatedDt)
select a.RankId, a.RankNo, a.RankName, a.MinHit, a.MaxHit, a.UpdatedBy, a.UpdatedDt
from #Rank a
left join dbo.Rank b on a.RankId = b.RankId
where b.RankId is null

set identity_insert dbo.Rank off

update a
set a.RankNo = b.RankNo, a.RankName = b.RankName, 
    a.MinHit = b.MinHit, a.MaxHit = b.MaxHit, a.UpdatedBy=b.UpdatedBy, a.UpdatedDt=b.UpdatedDt
from dbo.Rank a
join #Rank b on a.RankId = b.RankId

delete a
from dbo.Rank a
left join #Rank b on a.RankId = b.RankId
where b.RankId is null




update a
set a.NoOfPosts = isnull(b.Cnt, 0)
from dbo.UserProfile a
join (
    select UpdatedBy, count(*) as Cnt
    from dbo.VehicleComment
    group by UpdatedBy
) b on a.UserId = b.UpdatedBy

update a
set a.RankId = b.RankId
from dbo.UserProfile a
join dbo.Rank b on a.NoOfPosts between b.MinHit and b.MaxHit




-- drop table dbo.EditorWork
if object_id('dbo.EditorWork') is null
begin
    create table dbo.EditorWork
    (
        EditorWorkId int not null identity,
        EditorId nvarchar(128) not null,
        EditorWorkDt datetime not null,
        EditorWorkAction int not null, -- 0: Add, 1: Edit, 2: Delete
        StatusId int not null default(0), -- 0: Draft, 1: Approved, 2: EditApproved, 3: Denied
        ApprovedBy nvarchar(128) null,
        ApprovedDt datetime null,
        DeniedBy nvarchar(128) null,
        DeniedDt datetime null
    )


   alter table dbo.EditorWork add constraint EditorWorkPK primary key (EditorWorkId)
end

-- drop table dbo.VehicleWireFunctionEditor
if object_id('dbo.VehicleWireFunctionEditor') is null
begin
    create table dbo.VehicleWireFunctionEditor
    (
        VehicleWireFunctionEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireFunctionId int null,

        VehicleMakeModelYearId int null,
        WireFunctionId int null,
        InstallationTypeId int null,

        Colour nvarchar(200) null,
        VehicleColor nvarchar(200) null,
        PinOut nvarchar(200) null,
        Location nvarchar(200) null,
        Polarity nvarchar(100) null,

        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
   )

   alter table dbo.VehicleWireFunctionEditor add constraint VehicleWireFunctionEditorIdPK primary key (VehicleWireFunctionEditorId)
   create index Idx_VehicleWireFunctionEditor_EditorWorkId on dbo.VehicleWireFunctionEditor (EditorWorkId, VehicleWireFunctionId)

   alter table dbo.VehicleWireFunctionEditor add constraint FK_VehicleWireFunctionEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end



alter table dbo.VehicleWireFunction add EditorWorkId int null




-- drop table dbo.VehicleWireDisassemblyEditor
if object_id('dbo.VehicleWireDisassemblyEditor') is null
begin
    create table dbo.VehicleWireDisassemblyEditor
    (
        VehicleWireDisassemblyEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireDisassemblyId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null,
   )

   alter table dbo.VehicleWireDisassemblyEditor add constraint VehicleWireDisassemblyEditorPK primary key (VehicleWireDisassemblyEditorId)
   create index Idx_VehicleWireDisassemblyEditory_VehicleMakeModelYearId on dbo.VehicleWireDisassemblyEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireDisassemblyEditor add constraint FK_VehicleWireDisassemblyEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end



alter table dbo.VehicleWireDisassembly add EditorWorkId int null



-- drop table dbo.VehicleWireFacebookEditor
if object_id('dbo.VehicleWireFacebookEditor') is null
begin
    create table dbo.VehicleWireFacebookEditor
    (
        VehicleWireFacebookEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireFacebookId int NULL,

        VehicleMakeModelYearId int NOT NULL,
        Note nvarchar(max) NULL,
        [URL] nvarchar(500) NULL,
        Attach1 nvarchar(2000) NULL,
        Attach2 nvarchar(2000) NULL,
        Attach3 nvarchar(2000) NULL,
        Attach4 nvarchar(2000) NULL,
        Attach5 nvarchar(2000) NULL
   )

   alter table dbo.VehicleWireFacebookEditor add constraint VehicleWireFacebookEditorPK primary key (VehicleWireFacebookEditorId)
   create index Idx_VehicleWireFacebookEditor_VehicleMakeModelYearId on dbo.VehicleWireFacebookEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireFacebookEditor add constraint FK_VehicleWireFacebookEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


alter table dbo.VehicleWireFacebook add EditorWorkId int null




-- drop table dbo.VehicleDocumentEditor
if object_id('dbo.VehicleDocumentEditor') is null
begin
   create table dbo.VehicleDocumentEditor
   (
        VehicleDocumentEditorId int IDENTITY NOT NULL,
        EditorWorkId int not null,
        VehicleDocumentId int NULL,

        VehicleMakeModelYearId int NOT NULL,
        DocumentName nvarchar(200) NOT NULL,
        AttachFile nvarchar(200) NULL,
   )

   alter table dbo.VehicleDocumentEditor add constraint VehicleDocumentEditorPK primary key (VehicleDocumentEditorId)
   create index Idx_VehicleDocumentEditor_VehicleMakeModelYearId on dbo.VehicleDocumentEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleDocumentEditor add constraint FK_VehicleDocumentEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end

alter table dbo.VehicleDocument add EditorWorkId int null




-- drop table dbo.VehicleWirePrepEditor
if object_id('dbo.VehicleWirePrepEditor') is null
begin
    create table dbo.VehicleWirePrepEditor
    (
        VehicleWirePrepEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWirePrepId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWirePrepEditor add constraint VehicleWirePrepEditorPK primary key (VehicleWirePrepEditorId)
    create index Idx_VehicleWirePrepEditor_VehicleMakeModelYearId on dbo.VehicleWirePrepEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWirePrepEditor add constraint FK_VehicleWirePrepEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end

alter table dbo.VehicleWirePrep add EditorWorkId int null


-- drop table dbo.VehicleWirePlacementRoutingEditor
if object_id('dbo.VehicleWirePlacementRoutingEditor') is null
begin
    create table dbo.VehicleWirePlacementRoutingEditor
    (
        VehicleWirePlacementRoutingEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWirePlacementRoutingId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWirePlacementRoutingEditor add constraint VehicleWirePlacementRoutingEditorPK primary key (VehicleWirePlacementRoutingEditorId)
    create index Idx_VehicleWirePlacementRoutingEditor_VehicleMakeModelYearId on dbo.VehicleWirePlacementRoutingEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWirePlacementRoutingEditor add constraint FK_VehicleWirePlacementRoutingEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end

alter table dbo.VehicleWirePlacementRouting add EditorWorkId int null


-- drop table dbo.VehicleWireProgrammingEditor
if object_id('dbo.VehicleWireProgrammingEditor') is null
begin
    create table dbo.VehicleWireProgrammingEditor
    (
        VehicleWireProgrammingEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireProgrammingId int null,

        VehicleMakeModelYearId int not null,
        Step int not null,
        Note nvarchar(max) null,
        Attach1 nvarchar(2000) null,
        Attach2 nvarchar(2000) null,
        Attach3 nvarchar(2000) null,
        Attach4 nvarchar(2000) null,
        Attach5 nvarchar(2000) null
    )

    alter table dbo.VehicleWireProgrammingEditor add constraint VehicleWireProgrammingEditorPK primary key (VehicleWireProgrammingEditorId)
    create index Idx_VehicleWireProgrammingEditor_VehicleMakeModelYearId on dbo.VehicleWireProgrammingEditor (VehicleMakeModelYearId)

   alter table dbo.VehicleWireProgrammingEditor add constraint FK_VehicleWireProgrammingEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end


alter table dbo.VehicleWireProgramming add EditorWorkId int null



-- drop table dbo.VehicleWireNoteEditor
if object_id('dbo.VehicleWireNoteEditor') is null
begin
    create table dbo.VehicleWireNoteEditor
    (
        VehicleWireNoteEditorId int not null identity,
        EditorWorkId int not null,
        VehicleWireNoteId int null,

        VehicleMakeModelYearId int not null,
        InstallationTypeId int null,
        Note nvarchar(max) null
    )

    alter table dbo.VehicleWireNoteEditor add constraint VehicleWireNoteEditorPK primary key (VehicleWireNoteEditorId)
    create index Idx_VehicleWireNoteEditor_VehicleMakeModelYearId on dbo.VehicleWireNoteEditor (VehicleMakeModelYearId)

    alter table dbo.VehicleWireNoteEditor add constraint FK_VehicleWireNoteEditor_EditorWorkId foreign key (EditorWorkId) references dbo.EditorWork(EditorWorkId) not for replication
end

alter table dbo.VehicleWireNote add EditorWorkId int null






alter table dbo.VehicleMakeModelYear add SearchCount int not null default(0)





alter table dbo.VehicleComment add OriginalImgFilename nvarchar(500) null
alter table dbo.VehicleComment add ImgFilename nvarchar(500) null

















-- ProductType
if object_id('dbo.ProductType') is null
begin
    create table dbo.ProductType
    (
        ProductTypeId int not null identity,
        ProductTypeName nvarchar(100) not null,
        SortOrder int null
    )

    alter table dbo.ProductType add constraint ProductTypePK primary key (ProductTypeId)
end

/*
alter table dbo.ProductType add SortOrder int null
*/

create table #ProductType
(
    ProductTypeId int not null,
    ProductTypeName nvarchar(100) not null,
    SortOrder int null
)
    
insert into #ProductType
select 1, 'Control Brain', 2 union all
select 2, 'Remote', 1 union all
select 3, 'Antenna', 3 union all
select 4, 'Accessories', 4


set identity_insert dbo.ProductType on

insert into dbo.ProductType
(ProductTypeId, ProductTypeName, SortOrder)
select a.ProductTypeId, a.ProductTypeName, a.SortOrder
from #ProductType a
left join dbo.ProductType b on a.ProductTypeId = b.ProductTypeId
where b.ProductTypeId is null

set identity_insert dbo.ProductType off

update a
set a.ProductTypeName = b.ProductTypeName, a.SortOrder= b.SortOrder
from dbo.ProductType a
join #ProductType b on a.ProductTypeId = b.ProductTypeId

delete a
from dbo.ProductType a
left join #ProductType b on a.ProductTypeId = b.ProductTypeId
where b.ProductTypeId is null



-- ProductBrainConvenience
if object_id('dbo.ProductBrainConvenience') is null
begin
    create table dbo.ProductBrainConvenience
    (
        ProductBrainConvenienceId int not null identity,
        ProductBrainConvenienceName nvarchar(100) not null
    )

    alter table dbo.ProductBrainConvenience add constraint ProductBrainConveniencePK primary key (ProductBrainConvenienceId)
end

create table #ProductBrainConvenience
(
    ProductBrainConvenienceId int not null,
    ProductBrainConvenienceName nvarchar(100) not null
)
    
insert into #ProductBrainConvenience
select 1, 'Manual Transmission' union all
select 2, 'Ignition Controlled Locks' union all
select 3, 'Defrost Control' union all
select 4, 'HVAC Control' union all
select 5, 'Driver Priority Unlock' union all
select 6, 'Passive Arming' union all
select 7, 'Auto Lock / Rearm' union all
select 8, 'EZGo Compatible' union all
select 9, 'Siren / Horn Mute' union all
select 10, 'Parking Light Reminder' union all
select 11, 'Valet Mode' union all
select 12, 'Low Battery Warning' 


set identity_insert dbo.ProductBrainConvenience on

insert into dbo.ProductBrainConvenience
(ProductBrainConvenienceId, ProductBrainConvenienceName)
select a.ProductBrainConvenienceId, a.ProductBrainConvenienceName
from #ProductBrainConvenience a
left join dbo.ProductBrainConvenience b on a.ProductBrainConvenienceId = b.ProductBrainConvenienceId
where b.ProductBrainConvenienceId is null

set identity_insert dbo.ProductBrainConvenience off

update a
set a.ProductBrainConvenienceName = b.ProductBrainConvenienceName
from dbo.ProductBrainConvenience a
join #ProductBrainConvenience b on a.ProductBrainConvenienceId = b.ProductBrainConvenienceId

delete a
from dbo.ProductBrainConvenience a
left join #ProductBrainConvenience b on a.ProductBrainConvenienceId = b.ProductBrainConvenienceId
where b.ProductBrainConvenienceId is null




-- ProductBrainRemoteStart
if object_id('dbo.ProductBrainRemoteStart') is null
begin
    create table dbo.ProductBrainRemoteStart
    (
        ProductBrainRemoteStartId int not null identity,
        ProductBrainRemoteStartName nvarchar(100) not null
    )

    alter table dbo.ProductBrainRemoteStart add constraint ProductBrainRemoteStartPK primary key (ProductBrainRemoteStartId)
end

create table #ProductBrainRemoteStart
(
    ProductBrainRemoteStartId int not null,
    ProductBrainRemoteStartName nvarchar(100) not null
)
    
insert into #ProductBrainRemoteStart
select 1, 'Adjustable Runtime' union all
select 2, 'Diesel Timer' union all
select 3, 'Turbo Timer' union all
select 4, 'Timer Start' union all
select 5, 'Temperature Start' union all
select 6, '24 Hour Repeat w/ Cold Starting' union all
select 7, 'Low Battery Start' union all
select 8, 'Anti-Grind' union all
select 9, 'Cold or Hot Start'


set identity_insert dbo.ProductBrainRemoteStart on

insert into dbo.ProductBrainRemoteStart
(ProductBrainRemoteStartId, ProductBrainRemoteStartName)
select a.ProductBrainRemoteStartId, a.ProductBrainRemoteStartName
from #ProductBrainRemoteStart a
left join dbo.ProductBrainRemoteStart b on a.ProductBrainRemoteStartId = b.ProductBrainRemoteStartId
where b.ProductBrainRemoteStartId is null

set identity_insert dbo.ProductBrainRemoteStart off

update a
set a.ProductBrainRemoteStartName = b.ProductBrainRemoteStartName
from dbo.ProductBrainRemoteStart a
join #ProductBrainRemoteStart b on a.ProductBrainRemoteStartId = b.ProductBrainRemoteStartId

delete a
from dbo.ProductBrainRemoteStart a
left join #ProductBrainRemoteStart b on a.ProductBrainRemoteStartId = b.ProductBrainRemoteStartId
where b.ProductBrainRemoteStartId is null






-- ProductBrainAlarm
if object_id('dbo.ProductBrainAlarm') is null
begin
    create table dbo.ProductBrainAlarm
    (
        ProductBrainAlarmId int not null identity,
        ProductBrainAlarmName nvarchar(100) not null
    )

    alter table dbo.ProductBrainAlarm add constraint ProductBrainAlarmPK primary key (ProductBrainAlarmId)
end

create table #ProductBrainAlarm
(
    ProductBrainAlarmId int not null,
    ProductBrainAlarmName nvarchar(100) not null
)
    
insert into #ProductBrainAlarm
select 1, 'Battery Back Up' union all
select 2, 'Starter Kill' union all
select 4, 'Hood Trigger' union all
select 5, 'Door Triggers' union all
select 6, 'Trunk Triggers' union all
select 7, 'Dual Stage Shock Sensor' union all
select 8, 'Tilt Sensor' union all
select 9, 'Auxiliary Sensor Port'



set identity_insert dbo.ProductBrainAlarm on

insert into dbo.ProductBrainAlarm
(ProductBrainAlarmId, ProductBrainAlarmName)
select a.ProductBrainAlarmId, a.ProductBrainAlarmName
from #ProductBrainAlarm a
left join dbo.ProductBrainAlarm b on a.ProductBrainAlarmId = b.ProductBrainAlarmId
where b.ProductBrainAlarmId is null

set identity_insert dbo.ProductBrainAlarm off

update a
set a.ProductBrainAlarmName = b.ProductBrainAlarmName
from dbo.ProductBrainAlarm a
join #ProductBrainAlarm b on a.ProductBrainAlarmId = b.ProductBrainAlarmId

delete a
from dbo.ProductBrainAlarm a
left join #ProductBrainAlarm b on a.ProductBrainAlarmId = b.ProductBrainAlarmId
where b.ProductBrainAlarmId is null





-- ProductBrainInstallation
if object_id('dbo.ProductBrainInstallation') is null
begin
    create table dbo.ProductBrainInstallation
    (
        ProductBrainInstallationId int not null identity,
        ProductBrainInstallationName nvarchar(100) not null
    )

    alter table dbo.ProductBrainInstallation add constraint ProductBrainInstallationPK primary key (ProductBrainInstallationId)
end

create table #ProductBrainInstallation
(
    ProductBrainInstallationId int not null,
    ProductBrainInstallationName nvarchar(100) not null
)
    
insert into #ProductBrainInstallation
select 1, 'Double Pulse Lock / Unlock' union all
select 2, 'Double Pulse Disarm' union all
select 3, 'Ignition / Accessory Output Upon Disarm' union all
select 4, 'Dome Light Delay' union all
select 5, 'POC Flexibility' union all
select 6, 'PIC Flexibility' union all
select 7, 'Extended Aux Outputs' union all
select 8, 'N.C. Negative Door Triggers' union all
select 9, 'Double Pulse Unlock' union all
select 10, 'Lock / Unlock Pulse Duration' union all
select 11, 'Trigger Start' union all
select 12, 'PTS Remote Programming' union all
select 13, 'Adjustable Pulse Duration on AUX Outputs' union all
select 14, 'Unlock Before, Lock After Start' union all
select 15, 'Adjustable Crank Time' union all
select 16, 'Tach Engine Sensing' union all
select 17, 'Alternator Engine Sensing' union all
select 18, 'Voltage Engine Sensing' union all
select 19, 'Hybrid Vehicle, Extended Crank'


set identity_insert dbo.ProductBrainInstallation on

insert into dbo.ProductBrainInstallation
(ProductBrainInstallationId, ProductBrainInstallationName)
select a.ProductBrainInstallationId, a.ProductBrainInstallationName
from #ProductBrainInstallation a
left join dbo.ProductBrainInstallation b on a.ProductBrainInstallationId = b.ProductBrainInstallationId
where b.ProductBrainInstallationId is null

set identity_insert dbo.ProductBrainInstallation off

update a
set a.ProductBrainInstallationName = b.ProductBrainInstallationName
from dbo.ProductBrainInstallation a
join #ProductBrainInstallation b on a.ProductBrainInstallationId = b.ProductBrainInstallationId

delete a
from dbo.ProductBrainInstallation a
left join #ProductBrainInstallation b on a.ProductBrainInstallationId = b.ProductBrainInstallationId
where b.ProductBrainInstallationId is null



-- ProductRemoteConvenience
if object_id('dbo.ProductRemoteConvenience') is null
begin
    create table dbo.ProductRemoteConvenience
    (
        ProductRemoteConvenienceId int not null identity,
        ProductRemoteConvenienceName nvarchar(100) not null
    )

    alter table dbo.ProductRemoteConvenience add constraint ProductRemoteConveniencePK primary key (ProductRemoteConvenienceId)
end

create table #ProductRemoteConvenience
(
    ProductRemoteConvenienceId int not null,
    ProductRemoteConvenienceName nvarchar(100) not null
)
    
insert into #ProductRemoteConvenience
select 1, 'Lock' union all
select 2, 'Unlock' union all
select 3, 'Trunk' union all
select 4, 'Remote Start' union all
select 5, 'Panic' union all
select 6, 'Run Time Extender' union all
select 7, 'Car Check' union all
select 8, 'Siren Mute' union all
select 9, 'Valet On / Off' union all
select 10, 'Timer' union all
select 11, 'Passive Arming' union all
select 12, 'Shock Sensor On / Off' union all
select 13, 'Drive Lock' union all
select 14, 'Turbo Timer' union all
select 15, '2nd Car Mode' union all
select 16, 'Alarm Notifications'

set identity_insert dbo.ProductRemoteConvenience on

insert into dbo.ProductRemoteConvenience
(ProductRemoteConvenienceId, ProductRemoteConvenienceName)
select a.ProductRemoteConvenienceId, a.ProductRemoteConvenienceName
from #ProductRemoteConvenience a
left join dbo.ProductRemoteConvenience b on a.ProductRemoteConvenienceId = b.ProductRemoteConvenienceId
where b.ProductRemoteConvenienceId is null

set identity_insert dbo.ProductRemoteConvenience off

update a
set a.ProductRemoteConvenienceName = b.ProductRemoteConvenienceName
from dbo.ProductRemoteConvenience a
join #ProductRemoteConvenience b on a.ProductRemoteConvenienceId = b.ProductRemoteConvenienceId

delete a
from dbo.ProductRemoteConvenience a
left join #ProductRemoteConvenience b on a.ProductRemoteConvenienceId = b.ProductRemoteConvenienceId
where b.ProductRemoteConvenienceId is null





-- ProductRemoteProgrammable
if object_id('dbo.ProductRemoteProgrammable') is null
begin
    create table dbo.ProductRemoteProgrammable
    (
        ProductRemoteProgrammableId int not null identity,
        ProductRemoteProgrammableName nvarchar(100) not null
    )

    alter table dbo.ProductRemoteProgrammable add constraint ProductRemoteProgrammablePK primary key (ProductRemoteProgrammableId)
end

create table #ProductRemoteProgrammable
(
    ProductRemoteProgrammableId int not null,
    ProductRemoteProgrammableName nvarchar(100) not null
)
    
insert into #ProductRemoteProgrammable
select 1, 'Half / Full Mode' union all
select 2, 'Silent / Vibrate Mode' union all
select 3, 'Button Lock' 



set identity_insert dbo.ProductRemoteProgrammable on

insert into dbo.ProductRemoteProgrammable
(ProductRemoteProgrammableId, ProductRemoteProgrammableName)
select a.ProductRemoteProgrammableId, a.ProductRemoteProgrammableName
from #ProductRemoteProgrammable a
left join dbo.ProductRemoteProgrammable b on a.ProductRemoteProgrammableId = b.ProductRemoteProgrammableId
where b.ProductRemoteProgrammableId is null

set identity_insert dbo.ProductRemoteProgrammable off

update a
set a.ProductRemoteProgrammableName = b.ProductRemoteProgrammableName
from dbo.ProductRemoteProgrammable a
join #ProductRemoteProgrammable b on a.ProductRemoteProgrammableId = b.ProductRemoteProgrammableId

delete a
from dbo.ProductRemoteProgrammable a
left join #ProductRemoteProgrammable b on a.ProductRemoteProgrammableId = b.ProductRemoteProgrammableId
where b.ProductRemoteProgrammableId is null




-- ProductRemoteAuxiliary
if object_id('dbo.ProductRemoteAuxiliary') is null
begin
    create table dbo.ProductRemoteAuxiliary
    (
        ProductRemoteAuxiliaryId int not null identity,
        ProductRemoteAuxiliaryName nvarchar(100) not null
    )

    alter table dbo.ProductRemoteAuxiliary add constraint ProductRemoteAuxiliaryPK primary key (ProductRemoteAuxiliaryId)
end

create table #ProductRemoteAuxiliary
(
    ProductRemoteAuxiliaryId int not null,
    ProductRemoteAuxiliaryName nvarchar(100) not null
)
    
insert into #ProductRemoteAuxiliary
select 1, 'Aux 1' union all
select 2, 'Aux 2' union all
select 3, 'Aux 3' union all
select 4, 'Aux 4' union all
select 5, 'Aux 5' union all
select 6, 'Aux 6' union all
select 7, 'Aux 7' 


set identity_insert dbo.ProductRemoteAuxiliary on

insert into dbo.ProductRemoteAuxiliary
(ProductRemoteAuxiliaryId, ProductRemoteAuxiliaryName)
select a.ProductRemoteAuxiliaryId, a.ProductRemoteAuxiliaryName
from #ProductRemoteAuxiliary a
left join dbo.ProductRemoteAuxiliary b on a.ProductRemoteAuxiliaryId = b.ProductRemoteAuxiliaryId
where b.ProductRemoteAuxiliaryId is null

set identity_insert dbo.ProductRemoteAuxiliary off

update a
set a.ProductRemoteAuxiliaryName = b.ProductRemoteAuxiliaryName
from dbo.ProductRemoteAuxiliary a
join #ProductRemoteAuxiliary b on a.ProductRemoteAuxiliaryId = b.ProductRemoteAuxiliaryId

delete a
from dbo.ProductRemoteAuxiliary a
left join #ProductRemoteAuxiliary b on a.ProductRemoteAuxiliaryId = b.ProductRemoteAuxiliaryId
where b.ProductRemoteAuxiliaryId is null





-- Product
if object_id('dbo.Product') is null
begin
    create table dbo.Product
    (
        ProductId int not null identity,
        ProductTypeId int not null,

        ModelNumber nvarchar(100) not null,
        PartNumber nvarchar(100) not null,
        Picture1 nvarchar(2000) null,

        WayRemote nvarchar(20) null,
        TwoWayAntenna nvarchar(20) null,
        OneWayAntenna nvarchar(20) null,
        [Status] nvarchar(20) null,
        AvailableForWarranty nvarchar(20) null,
        BladeCompatible nvarchar(20) null,
        DroneCompatible nvarchar(20) null,
        Dataport nvarchar(20) null,
        OperatingVoltage nvarchar(20) null,
        IdleCurrent nvarchar(20) null,
        OperatingTempF nvarchar(20) null,
        OperatingTempC nvarchar(20) null,
        WaterResident nvarchar(20) null,
        AntennaPort4Pin nvarchar(20) null,
        AntennaPort6Pin nvarchar(20) null,

        FccIdUSA nvarchar(30) null,
        FccIdCanada nvarchar(30) null,
        EstimatedRangeFt nvarchar(20) null,
        Battery nvarchar(20) null,
        EstimatedBatteryLifeDays nvarchar(50) null,
        WaterResistant nvarchar(20) null,
        RequiredAntennaCable nvarchar(30) null,

        DocumentURL nvarchar(3000) null,

        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.Product add constraint ProductPK primary key (ProductId)
    create index Idx_Product_ModelNumber on dbo.Product (ModelNumber)
    create index Idx_Product_PartNumber on dbo.Product (PartNumber)
end

/*
alter table dbo.Product add DocumentURL nvarchar(3000) null
*/


-- drop table ProductAlternateName
if object_id('dbo.ProductAlternateName') is null
begin
    create table dbo.ProductAlternateName
    (
        ProductAlternateNameId int not null identity,
        ProductId int not null,
        ProductAlternateName nvarchar(100) not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductAlternateName add constraint ProductAlternateNamePK primary key (ProductAlternateNameId)
end

-- drop table ProductReplacementPartNumber
if object_id('dbo.ProductReplacementPartNumber') is null
begin
    create table dbo.ProductReplacementPartNumber
    (
        ProductReplacementPartNumberId int not null identity,
        ProductId int not null,
        ProductId_Replacement int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductReplacementPartNumber add constraint ProductReplacementPartNumberPK primary key (ProductReplacementPartNumberId)
    create unique index ProductReplacementPartNumberIdx1 on dbo.ProductReplacementPartNumber (ProductId, ProductId_Replacement)
end



-- drop table ProductProductBrainConvenience
if object_id('dbo.ProductProductBrainConvenience') is null
begin
    create table dbo.ProductProductBrainConvenience
    (
        ProductProductBrainConvenienceId int not null identity,
        ProductId int not null,
        ProductBrainConvenienceId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductBrainConvenience add constraint ProductProductBrainConveniencePK primary key (ProductProductBrainConvenienceId)
    create unique index ProductProductBrainConvenienceIdx1 on dbo.ProductProductBrainConvenience (ProductId, ProductBrainConvenienceId)
end


-- drop table ProductProductBrainRemoteStart
if object_id('dbo.ProductProductBrainRemoteStart') is null
begin
    create table dbo.ProductProductBrainRemoteStart
    (
        ProductProductBrainRemoteStartId int not null identity,
        ProductId int not null,
        ProductBrainRemoteStartId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductBrainRemoteStart add constraint ProductProductBrainRemoteStartPK primary key (ProductProductBrainRemoteStartId)
    create unique index ProductProductBrainRemoteStartIdx1 on dbo.ProductProductBrainRemoteStart (ProductId, ProductBrainRemoteStartId)
end



-- drop table ProductProductBrainAlarm
if object_id('dbo.ProductProductBrainAlarm') is null
begin
    create table dbo.ProductProductBrainAlarm
    (
        ProductProductBrainAlarmId int not null identity,
        ProductId int not null,
        ProductBrainAlarmId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductBrainAlarm add constraint ProductProductBrainAlarmPK primary key (ProductProductBrainAlarmId)
    create unique index ProductProductBrainAlarmIdx1 on dbo.ProductProductBrainAlarm (ProductId, ProductBrainAlarmId)
end


-- drop table ProductProductBrainInstallation
if object_id('dbo.ProductProductBrainInstallation') is null
begin
    create table dbo.ProductProductBrainInstallation
    (
        ProductProductBrainInstallationId int not null identity,
        ProductId int not null,
        ProductBrainInstallationId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductBrainInstallation add constraint ProductProductBrainInstallationPK primary key (ProductProductBrainInstallationId)
    create unique index ProductProductBrainInstallationIdx1 on dbo.ProductProductBrainInstallation (ProductId, ProductBrainInstallationId)
end



-- ProductProductRemoteConvenience
if object_id('dbo.ProductProductRemoteConvenience') is null
begin
    create table dbo.ProductProductRemoteConvenience
    (
        ProductProductRemoteConvenienceId int not null identity,
        ProductId int not null,
        ProductRemoteConvenienceId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductRemoteConvenience add constraint ProductProductRemoteConveniencePK primary key (ProductProductRemoteConvenienceId)
    create unique index ProductProductRemoteConvenienceIdx1 on dbo.ProductProductRemoteConvenience (ProductId, ProductRemoteConvenienceId)
end



-- ProductProductRemoteProgrammable
if object_id('dbo.ProductProductRemoteProgrammable') is null
begin
    create table dbo.ProductProductRemoteProgrammable
    (
        ProductProductRemoteProgrammableId int not null identity,
        ProductId int not null,
        ProductRemoteProgrammableId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductRemoteProgrammable add constraint ProductProductRemoteProgrammablePK primary key (ProductProductRemoteProgrammableId)
    create unique index ProductProductRemoteProgrammableIdx1 on dbo.ProductProductRemoteProgrammable (ProductId, ProductRemoteProgrammableId)
end


-- ProductProductRemoteAuxiliary
if object_id('dbo.ProductProductRemoteAuxiliary') is null
begin
    create table dbo.ProductProductRemoteAuxiliary
    (
        ProductProductRemoteAuxiliaryId int not null identity,
        ProductId int not null,
        ProductRemoteAuxiliaryId int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductProductRemoteAuxiliary add constraint ProductProductRemoteAuxiliaryPK primary key (ProductProductRemoteAuxiliaryId)
    create unique index ProductProductRemoteAuxiliaryIdx1 on dbo.ProductProductRemoteAuxiliary (ProductId, ProductRemoteAuxiliaryId)
end




-- drop table ProductBrainCompatibleRemote
if object_id('dbo.ProductBrainCompatibleRemote') is null
begin
    create table dbo.ProductBrainCompatibleRemote
    (
        ProductBrainCompatibleRemoteId int not null identity,
        ProductId_Brain int not null,
        ProductId_Remote int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductBrainCompatibleRemote add constraint ProductBrainCompatibleRemotePK primary key (ProductBrainCompatibleRemoteId)
    create unique index ProductBrainCompatibleRemoteIdx1 on dbo.ProductBrainCompatibleRemote (ProductId_Brain, ProductId_Remote)
end




-- ProductBrainCompatibleAntenna
if object_id('dbo.ProductBrainCompatibleAntenna') is null
begin
    create table dbo.ProductBrainCompatibleAntenna
    (
        ProductBrainCompatibleAntennaId int not null identity,
        ProductId_Brain int not null,
        ProductId_Antenna int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductBrainCompatibleAntenna add constraint ProductBrainCompatibleAntennaPK primary key (ProductBrainCompatibleAntennaId)
    create unique index ProductBrainCompatibleAntennaIdx1 on dbo.ProductBrainCompatibleAntenna (ProductId_Brain, ProductId_Antenna)
end


-- drop table ProductBrainCompatibleAccessories
if object_id('dbo.ProductBrainCompatibleAccessories') is null
begin
    create table dbo.ProductBrainCompatibleAccessories
    (
        ProductBrainCompatibleAccessoriesId int not null identity,
        ProductId_Brain int not null,
        ProductId_Accessories int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductBrainCompatibleAccessories add constraint ProductBrainCompatibleAccessoriesPK primary key (ProductBrainCompatibleAccessoriesId)
    create unique index ProductBrainCompatibleAccessoriesIdx1 on dbo.ProductBrainCompatibleAccessories (ProductId_Brain, ProductId_Accessories)
end


-- drop table ProductRemoteCompatibleAntenna
if object_id('dbo.ProductRemoteCompatibleAntenna') is null
begin
    create table dbo.ProductRemoteCompatibleAntenna
    (
        ProductRemoteCompatibleAntennaId int not null identity,
        ProductId_Remote int not null,
        ProductId_Antenna int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductRemoteCompatibleAntenna add constraint ProductRemoteCompatibleAntennaPK primary key (ProductRemoteCompatibleAntennaId)
    create unique index ProductRemoteCompatibleAntennaIdx1 on dbo.ProductRemoteCompatibleAntenna (ProductId_Remote, ProductId_Antenna)
end
