﻿


-- drop table ProductPackageIncludeRemote
if object_id('dbo.ProductPackageIncludeRemote') is null
begin
    create table dbo.ProductPackageIncludeRemote
    (
        ProductPackageIncludeRemoteId int not null identity,
        ProductId_Package int not null,
        ProductId_Remote int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductPackageIncludeRemote add constraint ProductPackageIncludeRemotePK primary key (ProductPackageIncludeRemoteId)
    create unique index ProductPackageIncludeRemoteIdx1 on dbo.ProductPackageIncludeRemote (ProductId_Package, ProductId_Remote)
end

-- drop table ProductPackageIncludeBrain
if object_id('dbo.ProductPackageIncludeBrain') is null
begin
    create table dbo.ProductPackageIncludeBrain
    (
        ProductPackageIncludeBrainId int not null identity,
        ProductId_Package int not null,
        ProductId_Brain int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductPackageIncludeBrain add constraint ProductPackageIncludeBrainPK primary key (ProductPackageIncludeBrainId)
    create unique index ProductPackageIncludeBrainIdx1 on dbo.ProductPackageIncludeBrain (ProductId_Package, ProductId_Brain)
end











-- drop table ProductRemoteCompanion
if object_id('dbo.ProductRemoteCompanion') is null
begin
    create table dbo.ProductRemoteCompanion
    (
        ProductRemoteCompanionId int not null identity,
        ProductId_Remote int not null,
        ProductId_Companion int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductRemoteCompanion add constraint ProductRemoteCompanionPK primary key (ProductRemoteCompanionId)
    create unique index ProductRemoteCompanionIdx1 on dbo.ProductRemoteCompanion (ProductId_Remote, ProductId_Companion)
end











alter table dbo.UserProfile add WhereBuyProduct nvarchar(100) null



-- drop table ProductWikiSection
if object_id('dbo.ProductWikiSection') is null
begin
    create table dbo.ProductWikiSection
    (
        ProductWikiSectionId int not null identity,
        ProductId int not null,
        ProductWikiSectionTitle nvarchar(200) not null,
        SortOrder int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null
    )

    alter table dbo.ProductWikiSection add constraint ProductWikiSectionPK primary key (ProductWikiSectionId)
    create index ProductWikiSectionIdx1 on dbo.ProductWikiSection (ProductId)
end

-- drop table ProductWikiSectionDetail
if object_id('dbo.ProductWikiSectionDetail') is null
begin
    create table dbo.ProductWikiSectionDetail
    (
        ProductWikiSectionDetailId int not null identity,

        ProductWikiSectionId int not null,
        ProductWikiSectionInfo ntext not null,

        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,

        Approved int not null default (0),
        ApprovedBy nvarchar(128) not null default (0),
        ApprovedDt datetime null
    )

    alter table dbo.ProductWikiSectionDetail add constraint ProductWikiSectionDetailPK primary key (ProductWikiSectionDetailId)
    create index ProductWikiSectionDetailIdx1 on dbo.ProductWikiSectionDetail (ProductWikiSectionId)
end


if object_id ('tempdb..#ProductType') is not null
    drop table #ProductType

create table #ProductType
(
    ProductTypeId int not null,
    ProductTypeName nvarchar(100) not null,
    SortOrder int null
)
    
insert into #ProductType
select 1, 'Control Brain', 2 union all
select 2, 'Remote', 1 union all
select 3, 'Antenna', 3 union all
select 4, 'Accessories', 4 union all
select 5, 'Packages', 5 union all
select 6, 'Drone', 6


set identity_insert dbo.ProductType on

insert into dbo.ProductType
(ProductTypeId, ProductTypeName, SortOrder)
select a.ProductTypeId, a.ProductTypeName, a.SortOrder
from #ProductType a
left join dbo.ProductType b on a.ProductTypeId = b.ProductTypeId
where b.ProductTypeId is null

set identity_insert dbo.ProductType off

update a
set a.ProductTypeName = b.ProductTypeName, a.SortOrder= b.SortOrder
from dbo.ProductType a
join #ProductType b on a.ProductTypeId = b.ProductTypeId

delete a
from dbo.ProductType a
left join #ProductType b on a.ProductTypeId = b.ProductTypeId
where b.ProductTypeId is null




-- drop table ProductBrainCompatibleDrone
if object_id('dbo.ProductBrainCompatibleDrone') is null
begin
    create table dbo.ProductBrainCompatibleDrone
    (
        ProductBrainCompatibleDroneId int not null identity,
        ProductId_Brain int not null,
        ProductId_Drone int not null,

        Updated int not null,
        UpdatedBy nvarchar(128) not null,
        UpdatedDt datetime not null,
    )

    alter table dbo.ProductBrainCompatibleDrone add constraint ProductBrainCompatibleDronePK primary key (ProductBrainCompatibleDroneId)
    create unique index ProductBrainCompatibleDroneIdx1 on dbo.ProductBrainCompatibleDrone (ProductId_Brain, ProductId_Drone)
end

