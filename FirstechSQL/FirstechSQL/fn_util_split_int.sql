﻿if object_id('dbo.fn_util_split_int') is not null
    drop function fn_util_split_int
go

Create function [dbo].[fn_util_split_int](  
@vch_str varchar(8000),  
@chr_spliter char(1)  
)  
returns @returnTable table (int_idx int primary key identity, vch_item int)  
as  
begin  
declare @spliterIndex int  
select @vch_str = @vch_str + @chr_spliter  
while len(@vch_str) > 0  
 begin  
  select @spliterIndex = charindex(@chr_spliter,@vch_str)  
  if @spliterIndex = 1  
   insert @returnTable (vch_item)  
    values (null)  
  else  
   insert @returnTable (vch_item)  
    values (CAST(substring(@vch_str,1,@spliterIndex-1) as int))  
  select @vch_str = substring(@vch_str,@spliterIndex+1,len(@vch_str)-@spliterIndex)  
 end  
return  
end  


go
