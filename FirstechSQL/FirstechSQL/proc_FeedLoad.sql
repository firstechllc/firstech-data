﻿if object_id('dbo.proc_FeedLoad') is not null
    drop proc dbo.proc_FeedLoad
go	

CREATE PROCEDURE dbo.proc_FeedLoad
    @StartDate datetime = null,
    @EndDate datetime = null,
    @Email nvarchar(256) = null,
    @WhereBuyProduct nvarchar(100) = null,
    @VehicleMakeId int = null,
    @VehicleYear int = null,
    @VehicleModelId int = null
AS

select
    Id,  
    Dt,
    Feed,
    FeedType,
    IsReported
from (
    select 
        Id = a.UserId,
        Dt = RegistrationDate,
        Feed = b.Email + ' joined FirstechData',
        FeedType = 'Join',
        IsReported = 0
    from dbo.UserProfile a with (nolock)
    join dbo.AspNetUsers b with (nolock) on a.UserId = b.Id
    where (@StartDate is null or RegistrationDate >= @StartDate)
        and (@EndDate is null or RegistrationDate < @EndDate)
        and (@Email is null or b.Email like '%' + @Email + '%')
        and (@WhereBuyProduct is null or a.WhereBuyProduct like '%' + @WhereBuyProduct + '%')
        and ((@VehicleMakeId is null and @VehicleYear is null and @VehicleModelId is null) or 1=0)
    
    union all

    select
        Id = convert(nvarchar, a.VehicleCommentId),
        Dt = a.UpdatedDt,
        Feed = b.Email  + ' commented on a ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        FeedType = 'Comment',
        a.IsReported
    from dbo.VehicleComment a with (nolock)
    join dbo.AspNetUsers b with (nolock) on a.UpdatedBy = b.Id
    join dbo.VehicleMakeModelYear vc with (NOLOCK) on a.VehicleMakeModelYearId = vc.VehicleMakeModelYearId
    left join dbo.UserProfile p with (NOLOCK) on p.UserId = b.Id
    where (@StartDate is null or a.UpdatedDt >= @StartDate)
        and (@EndDate is null or a.UpdatedDt < @EndDate)
        and (@Email is null or b.Email like '%' + @Email + '%')
        and (@VehicleMakeId is null or vc.VehicleMakeId = @VehicleMakeId)
        and (@VehicleYear is null or vc.VehicleYear = @VehicleYear)
        and (@VehicleModelId is null or vc.VehicleModelId = @VehicleModelId)
        and (@WhereBuyProduct is null or p.WhereBuyProduct like '%' + @WhereBuyProduct + '%')

    union all
    
    select
        Id = convert(nvarchar, a.VehicleWireFunctionId),
        Dt = a.UpdatedDt,
        Feed = b.Email  + case when a.LikeDisLike=1 then ' liked ' else ' disliked ' end + ' wire ' + w.WireFunctionName + ' on a ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, c.VehicleMakeModelYearId) + '">vehicle</a>',
        FeedType = 'WireLike',
        IsReported = 0
    from dbo.VehicleWireFunctionLike a with (nolock)
    join dbo.AspNetUsers b with (nolock) on a.UserId = b.Id
    join dbo.VehicleWireFunction c with (nolock) on c.VehicleWireFunctionId = a.VehicleWireFunctionId
    join dbo.WireFunction w with (nolock) on w.WireFunctionId = c.WireFunctionId
    join dbo.VehicleMakeModelYear vc with (NOLOCK) on c.VehicleMakeModelYearId = vc.VehicleMakeModelYearId
    left join dbo.UserProfile p with (NOLOCK) on p.UserId = b.Id
    where (@StartDate is null or a.UpdatedDt >= @StartDate)
        and (@EndDate is null or a.UpdatedDt < @EndDate)
        and (@Email is null or b.Email like '%' + @Email + '%')
        and (@VehicleMakeId is null or vc.VehicleMakeId = @VehicleMakeId)
        and (@VehicleYear is null or vc.VehicleYear = @VehicleYear)
        and (@VehicleModelId is null or vc.VehicleModelId = @VehicleModelId)
        and (@WhereBuyProduct is null or p.WhereBuyProduct like '%' + @WhereBuyProduct + '%')

    union all

    select
        Id = convert(nvarchar, a.SubmitTipId),
        Dt = a.SubmitDt,
        Feed = b.Email  + 'submit tip ' + a.TypeOfTip + ', ' + VehicleYear + ', ' + VehicleMake + ', ' + VehicleModel + ', ' + KeyStyle + ': ' + TipDescription,
        FeedType = 'Tip',
        IsReported = 0
    from dbo.SubmitTip a with (nolock)
    join dbo.AspNetUsers b with (nolock) on a.SubmitBy = b.Id
    left join dbo.UserProfile p with (NOLOCK) on p.UserId = b.Id
    where (@StartDate is null or a.SubmitDt >= @StartDate)
        and (@EndDate is null or a.SubmitDt < @EndDate)
        and (@Email is null or b.Email like '%' + @Email + '%')
        and ((@VehicleMakeId is null and @VehicleYear is null and @VehicleModelId is null) or 1=0)
        and (@WhereBuyProduct is null or p.WhereBuyProduct like '%' + @WhereBuyProduct + '%')
) a
order by Dt desc
