﻿if object_id('dbo.proc_NewsTickerLoad') is not null
    drop proc dbo.proc_NewsTickerLoad
go	

CREATE PROCEDURE [dbo].proc_NewsTickerLoad
AS

declare @Last1Month datetime = dateadd(month, -1, getdate())

select 
    *
from (
    select top 3 
        Loc = 1,
        Id = NewsfeedId, 
        Title = NewsfeedTitle, 
        UpdateDt = NewsfeedDt 
    from dbo.Newsfeed with (NOLOCK)

    union all

    select 
        Loc = 2,
        tbl2.Id,
        Title = max(a.VehicleMakeName + ' ' + b.VehicleModelName + ' ' + convert(varchar, v.VehicleYear)),
        UpdateDt = max(tbl2.UpdateDt)
    from (
        -- Add/Update Vehicle
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleMakeModelYear with (NOLOCK)
        where UpdatedDt > @Last1Month

        union all

        -- Add/Update Wire
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWireFunction with (NOLOCK)
        where UpdatedDt > @Last1Month

        union all

        -- Add/Update Note
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWireNote with (NOLOCK)
        where UpdatedDt > @Last1Month

        union all

        -- Add/Update Prep
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWirePrep with (NOLOCK)
        where UpdatedDt > @Last1Month    

        union all

        -- Add/Update Disassembly
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWireDisassembly with (NOLOCK)
        where UpdatedDt > @Last1Month    

        union all

        -- Add/Update Routing
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWirePlacementRouting with (NOLOCK)
        where UpdatedDt > @Last1Month  

        union all

        -- Add/Update Facebook
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWireFacebook with (NOLOCK)
        where UpdatedDt > @Last1Month  

        union all

        -- Add/Update Comment
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleComment with (NOLOCK)
        where UpdatedDt > @Last1Month      

        union all

        -- Add/Update Internal Note
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleInternalNote with (NOLOCK)
        where UpdatedDt > @Last1Month      

        union all

        -- Add/Update Programming
        select 
            Id = VehicleMakeModelYearId,
            UpdateDt = UpdatedDt
        from dbo.VehicleWireProgramming with (NOLOCK)
        where UpdatedDt > @Last1Month      
    ) tbl2
    join dbo.VehicleMakeModelYear v on tbl2.Id = v.VehicleMakeModelYearId
    join dbo.VehicleModel b WITH(NOLOCK) on v.VehicleModelId = b.VehicleModelId 
    join dbo.VehicleMake a WITH (NOLOCK) on v.VehicleMakeId = a.VehicleMakeId
    group by tbl2.Id
) tbl
order by Loc asc, UpdateDt desc
