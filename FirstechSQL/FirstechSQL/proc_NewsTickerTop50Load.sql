﻿if object_id('dbo.proc_NewsTickerTop50Load') is not null
    drop proc dbo.proc_NewsTickerTop50Load
go	

CREATE PROCEDURE dbo.proc_NewsTickerTop50Load
AS

select 
    top 50
    *
from (
    select 
        Loc = 1,
        Id = NewsfeedId, 
        UpdateType = 'News',
        Title = NewsfeedTitle, 
        UpdateDt = NewsfeedDt 
    from dbo.Newsfeed with (NOLOCK)

    union all

    select 
        Loc = 2,
        tbl2.Id,
        UpdateType,
        Title = max(a.VehicleMakeName + ' ' + b.VehicleModelName + ' ' + convert(varchar, v.VehicleYear)),
        UpdateDt = max(tbl2.UpdateDt)
    from (
        -- Add/Update Vehicle
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Vehicle',
            UpdateDt = UpdatedDt
        from dbo.VehicleMakeModelYear with (NOLOCK)

        union all

        -- Add/Update Wire
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Wiring',
            UpdateDt = UpdatedDt
        from dbo.VehicleWireFunction with (NOLOCK)

        union all

        -- Add/Update Note
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Wiring Note',
            UpdateDt = UpdatedDt
        from dbo.VehicleWireNote with (NOLOCK)

        union all

        -- Add/Update Prep
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Prep',
            UpdateDt = UpdatedDt
        from dbo.VehicleWirePrep with (NOLOCK)

        union all

        -- Add/Update Disassembly
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Disassembly',
            UpdateDt = UpdatedDt
        from dbo.VehicleWireDisassembly with (NOLOCK)

        union all

        -- Add/Update Routing
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Routing',
            UpdateDt = UpdatedDt
        from dbo.VehicleWirePlacementRouting with (NOLOCK)

        union all

        -- Add/Update Facebook
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Facebook',
            UpdateDt = UpdatedDt
        from dbo.VehicleWireFacebook with (NOLOCK)

        union all

        -- Add/Update Programming
        select 
            Id = VehicleMakeModelYearId,
            UpdateType = 'Edit Programming',
            UpdateDt = UpdatedDt
        from dbo.VehicleWireProgramming with (NOLOCK)

        union all
        
        -- Comment
        select
            Id = VehicleMakeModelYearId,
            UpdateType = 'Comment',
            UpdateDt = UpdatedDt
        from dbo.VehicleComment with (NOLOCK)

    ) tbl2
    join dbo.VehicleMakeModelYear v on tbl2.Id = v.VehicleMakeModelYearId
    join dbo.VehicleModel b WITH(NOLOCK) on v.VehicleModelId = b.VehicleModelId 
    join dbo.VehicleMake a WITH (NOLOCK) on v.VehicleMakeId = a.VehicleMakeId
    group by tbl2.Id, UpdateType
) tbl
order by UpdateDt desc
