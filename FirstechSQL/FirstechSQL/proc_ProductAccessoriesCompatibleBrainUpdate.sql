﻿if object_id('dbo.proc_ProductAccessoriesCompatibleBrainUpdate') is not null
    drop proc dbo.proc_ProductAccessoriesCompatibleBrainUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductAccessoriesCompatibleBrainUpdate
    @ProductId_Accessories int,
    @ProductId_Brains varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Accessories int, ProductId_Brain int)

    IF @ProductId_Brains IS NOT NULL AND @ProductId_Brains <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Accessories, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Brains, ',')


    update dbo.ProductBrainCompatibleAccessories 
    set Updated=0 
    where ProductId_Accessories=@ProductId_Accessories


    insert into dbo.ProductBrainCompatibleAccessories
    (
        ProductId_Brain,
        ProductId_Accessories,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Brain,
        a.ProductId_Accessories,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleAccessories b
        on a.ProductId_Accessories = b.ProductId_Accessories and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Brain is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleAccessories b
    join #tbl a
        on a.ProductId_Accessories = b.ProductId_Accessories and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleAccessories 
    where Updated=0 
        and ProductId_Accessories=@ProductId_Accessories

