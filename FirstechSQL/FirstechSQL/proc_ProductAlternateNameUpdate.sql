﻿if object_id('dbo.proc_ProductAlternateNameUpdate') is not null
    drop proc dbo.proc_ProductAlternateNameUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductAlternateNameUpdate
    @ProductId int,
    @Names varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductAlternateName varchar(150))

    IF @Names IS NOT NULL AND @Names <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split(@Names, ',')


    update dbo.ProductAlternateName 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductAlternateName
    (
        ProductId,
        ProductAlternateName,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductAlternateName,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductAlternateName b
        on a.ProductAlternateName = b.ProductAlternateName and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductAlternateName b
    join #tbl a
        on a.ProductAlternateName = b.ProductAlternateName and a.ProductId = b.ProductId
    
    delete from dbo.ProductAlternateName 
    where Updated=0 
        and ProductId=@ProductId

