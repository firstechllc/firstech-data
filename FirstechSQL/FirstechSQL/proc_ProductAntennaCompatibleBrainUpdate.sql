﻿if object_id('dbo.proc_ProductAntennaCompatibleBrainUpdate') is not null
    drop proc dbo.proc_ProductAntennaCompatibleBrainUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductAntennaCompatibleBrainUpdate
    @ProductId_Antenna int,
    @ProductId_Brains varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Antenna int, ProductId_Brain int)

    IF @ProductId_Brains IS NOT NULL AND @ProductId_Brains <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Antenna, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Brains, ',')


    update dbo.ProductBrainCompatibleAntenna 
    set Updated=0 
    where ProductId_Antenna=@ProductId_Antenna


    insert into dbo.ProductBrainCompatibleAntenna
    (
        ProductId_Antenna,
        ProductId_Brain,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Antenna,
        a.ProductId_Brain,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleAntenna b
        on a.ProductId_Antenna = b.ProductId_Antenna and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Antenna is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleAntenna b
    join #tbl a
        on a.ProductId_Antenna = b.ProductId_Antenna and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleAntenna 
    where Updated=0 
        and ProductId_Antenna=@ProductId_Antenna

