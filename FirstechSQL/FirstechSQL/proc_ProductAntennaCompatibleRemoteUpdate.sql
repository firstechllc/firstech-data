﻿if object_id('dbo.proc_ProductAntennaCompatibleRemoteUpdate') is not null
    drop proc dbo.proc_ProductAntennaCompatibleRemoteUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductAntennaCompatibleRemoteUpdate
    @ProductId_Antenna int,
    @ProductId_Remotes varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Antenna int, ProductId_Remote int)

    IF @ProductId_Remotes IS NOT NULL AND @ProductId_Remotes <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Antenna, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Remotes, ',')


    update dbo.ProductRemoteCompatibleAntenna 
    set Updated=0 
    where ProductId_Antenna=@ProductId_Antenna


    insert into dbo.ProductRemoteCompatibleAntenna
    (
        ProductId_Remote,
        ProductId_Antenna,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Remote,
        a.ProductId_Antenna,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductRemoteCompatibleAntenna b
        on a.ProductId_Antenna = b.ProductId_Antenna and a.ProductId_Remote = b.ProductId_Remote
    where b.ProductId_Antenna is null

    update b
    set b.Updated = 1
    from dbo.ProductRemoteCompatibleAntenna b
    join #tbl a
        on a.ProductId_Antenna = b.ProductId_Antenna and a.ProductId_Remote = b.ProductId_Remote
    
    delete from dbo.ProductRemoteCompatibleAntenna 
    where Updated=0 
        and ProductId_Antenna=@ProductId_Antenna

