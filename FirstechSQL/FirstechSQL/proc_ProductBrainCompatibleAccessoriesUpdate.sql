﻿if object_id('dbo.proc_ProductBrainCompatibleAccessoriesUpdate') is not null
    drop proc dbo.proc_ProductBrainCompatibleAccessoriesUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductBrainCompatibleAccessoriesUpdate
    @ProductId_Brain int,
    @ProductId_Accessories varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Brain int, ProductId_Accessories int)

    IF @ProductId_Accessories IS NOT NULL AND @ProductId_Accessories <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Brain, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Accessories, ',')


    update dbo.ProductBrainCompatibleAccessories 
    set Updated=0 
    where ProductId_Brain=@ProductId_Brain


    insert into dbo.ProductBrainCompatibleAccessories
    (
        ProductId_Brain,
        ProductId_Accessories,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Brain,
        a.ProductId_Accessories,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleAccessories b
        on a.ProductId_Accessories = b.ProductId_Accessories and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Brain is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleAccessories b
    join #tbl a
        on a.ProductId_Accessories = b.ProductId_Accessories and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleAccessories 
    where Updated=0 
        and ProductId_Brain=@ProductId_Brain

