﻿if object_id('dbo.proc_ProductBrainCompatibleDroneUpdate') is not null
    drop proc dbo.proc_ProductBrainCompatibleDroneUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductBrainCompatibleDroneUpdate
    @ProductId_Brain int,
    @ProductId_Drones varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Brain int, ProductId_Drone int)

    IF @ProductId_Drones IS NOT NULL AND @ProductId_Drones <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Brain, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Drones, ',')


    update dbo.ProductBrainCompatibleDrone 
    set Updated=0 
    where ProductId_Brain=@ProductId_Brain


    insert into dbo.ProductBrainCompatibleDrone
    (
        ProductId_Brain,
        ProductId_Drone,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Brain,
        a.ProductId_Drone,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleDrone b
        on a.ProductId_Drone = b.ProductId_Drone and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Brain is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleDrone b
    join #tbl a
        on a.ProductId_Drone = b.ProductId_Drone and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleDrone 
    where Updated=0 
        and ProductId_Brain=@ProductId_Brain

