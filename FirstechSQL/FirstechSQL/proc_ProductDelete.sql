﻿if object_id('dbo.proc_ProductDelete') is not null
    drop proc dbo.proc_ProductDelete
go	

CREATE PROCEDURE [dbo].proc_ProductDelete
    @ProductId int
AS

    delete dbo.ProductAlternateName
    where ProductId = @ProductId

    delete dbo.ProductReplacementPartNumber
    where ProductId = @ProductId

    delete dbo.ProductProductBrainConvenience
    where ProductId = @ProductId

    delete dbo.ProductProductBrainRemoteStart
    where ProductId = @ProductId

    delete dbo.ProductProductBrainAlarm
    where ProductId = @ProductId

    delete dbo.ProductProductBrainInstallation
    where ProductId = @ProductId

    delete dbo.ProductProductRemoteConvenience
    where ProductId = @ProductId

    delete dbo.ProductProductRemoteProgrammable
    where ProductId = @ProductId

    delete dbo.ProductProductRemoteAuxiliary
    where ProductId = @ProductId

    delete dbo.ProductBrainCompatibleRemote
    where ProductId_Brain = @ProductId

    delete dbo.ProductBrainCompatibleRemote
    where ProductId_Remote = @ProductId

    delete dbo.ProductBrainCompatibleAntenna
    where ProductId_Brain = @ProductId

    delete dbo.ProductBrainCompatibleAntenna
    where ProductId_Antenna = @ProductId

    delete dbo.ProductBrainCompatibleAccessories
    where ProductId_Brain = @ProductId

    delete dbo.ProductBrainCompatibleAccessories
    where ProductId_Accessories = @ProductId

    delete dbo.ProductRemoteCompatibleAntenna
    where ProductId_Remote = @ProductId

    delete dbo.ProductRemoteCompatibleAntenna
    where ProductId_Antenna = @ProductId

    delete dbo.ProductBrainCompatibleDrone
    where ProductId_Brain = @ProductId

    delete dbo.ProductBrainCompatibleDrone
    where ProductId_Drone = @ProductId

    delete dbo.ProductRemoteCompanion
    where ProductId_Remote = @ProductId

    delete dbo.ProductRemoteCompanion
    where ProductId_Companion = @ProductId


    delete dbo.Product
    where ProductId = @ProductId


