﻿if object_id('dbo.proc_ProductDroneCompatibleBrainUpdate') is not null
    drop proc dbo.proc_ProductDroneCompatibleBrainUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductDroneCompatibleBrainUpdate
    @ProductId_Drone int,
    @ProductId_Brains varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Drone int, ProductId_Brain int)

    IF @ProductId_Brains IS NOT NULL AND @ProductId_Brains <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Drone, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Brains, ',')


    update dbo.ProductBrainCompatibleDrone 
    set Updated=0 
    where ProductId_Drone=@ProductId_Drone


    insert into dbo.ProductBrainCompatibleDrone
    (
        ProductId_Drone,
        ProductId_Brain,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Drone,
        a.ProductId_Brain,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleDrone b
        on a.ProductId_Drone = b.ProductId_Drone and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Drone is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleDrone b
    join #tbl a
        on a.ProductId_Drone = b.ProductId_Drone and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleDrone 
    where Updated=0 
        and ProductId_Drone=@ProductId_Drone

