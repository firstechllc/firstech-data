﻿if object_id('dbo.proc_ProductPackageBrainUpdate') is not null
    drop proc dbo.proc_ProductPackageBrainUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductPackageBrainUpdate
    @ProductId_Package int,
    @ProductId_Brain varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Package int, ProductId_Brain int)

    IF @ProductId_Brain IS NOT NULL AND @ProductId_Brain <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Package, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Brain, ',')


    update dbo.ProductPackageIncludeBrain 
    set Updated=0 
    where ProductId_Package=@ProductId_Package


    insert into dbo.ProductPackageIncludeBrain
    (
        ProductId_Brain,
        ProductId_Package,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Brain,
        a.ProductId_Package,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductPackageIncludeBrain b
        on a.ProductId_Package = b.ProductId_Package and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Brain is null

    update b
    set b.Updated = 1
    from dbo.ProductPackageIncludeBrain b
    join #tbl a
        on a.ProductId_Package = b.ProductId_Package and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductPackageIncludeBrain 
    where Updated=0 
        and ProductId_Package=@ProductId_Package

