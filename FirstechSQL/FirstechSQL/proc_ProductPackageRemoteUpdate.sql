﻿if object_id('dbo.proc_ProductPackageRemoteUpdate') is not null
    drop proc dbo.proc_ProductPackageRemoteUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductPackageRemoteUpdate
    @ProductId_Package int,
    @ProductId_Remote varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Package int, ProductId_Remote int)

    IF @ProductId_Remote IS NOT NULL AND @ProductId_Remote <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Package, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Remote, ',')


    update dbo.ProductPackageIncludeRemote 
    set Updated=0 
    where ProductId_Package=@ProductId_Package


    insert into dbo.ProductPackageIncludeRemote
    (
        ProductId_Remote,
        ProductId_Package,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Remote,
        a.ProductId_Package,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductPackageIncludeRemote b
        on a.ProductId_Package = b.ProductId_Package and a.ProductId_Remote = b.ProductId_Remote
    where b.ProductId_Remote is null

    update b
    set b.Updated = 1
    from dbo.ProductPackageIncludeRemote b
    join #tbl a
        on a.ProductId_Package = b.ProductId_Package and a.ProductId_Remote = b.ProductId_Remote
    
    delete from dbo.ProductPackageIncludeRemote 
    where Updated=0 
        and ProductId_Package=@ProductId_Package

