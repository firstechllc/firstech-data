﻿if object_id('dbo.proc_ProductProductBrainAlarmUpdate') is not null
    drop proc dbo.proc_ProductProductBrainAlarmUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductBrainAlarmUpdate
    @ProductId int,
    @ProductBrainAlarmIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductBrainAlarmId int)

    IF @ProductBrainAlarmIds IS NOT NULL AND @ProductBrainAlarmIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductBrainAlarmIds, ',')


    update dbo.ProductProductBrainAlarm 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductBrainAlarm
    (
        ProductId,
        ProductBrainAlarmId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductBrainAlarmId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductBrainAlarm b
        on a.ProductBrainAlarmId = b.ProductBrainAlarmId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductBrainAlarm b
    join #tbl a
        on a.ProductBrainAlarmId = b.ProductBrainAlarmId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductBrainAlarm 
    where Updated=0 
        and ProductId=@ProductId


