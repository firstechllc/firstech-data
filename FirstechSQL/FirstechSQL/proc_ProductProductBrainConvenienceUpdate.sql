﻿if object_id('dbo.proc_ProductProductBrainConvenienceUpdate') is not null
    drop proc dbo.proc_ProductProductBrainConvenienceUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductBrainConvenienceUpdate
    @ProductId int,
    @ProductBrainConvenienceIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductBrainConvenienceId int)

    IF @ProductBrainConvenienceIds IS NOT NULL AND @ProductBrainConvenienceIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductBrainConvenienceIds, ',')


    update dbo.ProductProductBrainConvenience 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductBrainConvenience
    (
        ProductId,
        ProductBrainConvenienceId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductBrainConvenienceId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductBrainConvenience b
        on a.ProductBrainConvenienceId = b.ProductBrainConvenienceId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductBrainConvenience b
    join #tbl a
        on a.ProductBrainConvenienceId = b.ProductBrainConvenienceId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductBrainConvenience 
    where Updated=0 
        and ProductId=@ProductId


