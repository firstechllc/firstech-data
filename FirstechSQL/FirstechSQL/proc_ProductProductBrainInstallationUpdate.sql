﻿if object_id('dbo.proc_ProductProductBrainInstallationUpdate') is not null
    drop proc dbo.proc_ProductProductBrainInstallationUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductBrainInstallationUpdate
    @ProductId int,
    @ProductBrainInstallationIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductBrainInstallationId int)

    IF @ProductBrainInstallationIds IS NOT NULL AND @ProductBrainInstallationIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductBrainInstallationIds, ',')


    update dbo.ProductProductBrainInstallation 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductBrainInstallation
    (
        ProductId,
        ProductBrainInstallationId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductBrainInstallationId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductBrainInstallation b
        on a.ProductBrainInstallationId = b.ProductBrainInstallationId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductBrainInstallation b
    join #tbl a
        on a.ProductBrainInstallationId = b.ProductBrainInstallationId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductBrainInstallation 
    where Updated=0 
        and ProductId=@ProductId


