﻿if object_id('dbo.proc_ProductProductBrainRemoteStartUpdate') is not null
    drop proc dbo.proc_ProductProductBrainRemoteStartUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductBrainRemoteStartUpdate
    @ProductId int,
    @ProductBrainRemoteStartIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductBrainRemoteStartId int)

    IF @ProductBrainRemoteStartIds IS NOT NULL AND @ProductBrainRemoteStartIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductBrainRemoteStartIds, ',')


    update dbo.ProductProductBrainRemoteStart 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductBrainRemoteStart
    (
        ProductId,
        ProductBrainRemoteStartId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductBrainRemoteStartId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductBrainRemoteStart b
        on a.ProductBrainRemoteStartId = b.ProductBrainRemoteStartId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductBrainRemoteStart b
    join #tbl a
        on a.ProductBrainRemoteStartId = b.ProductBrainRemoteStartId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductBrainRemoteStart 
    where Updated=0 
        and ProductId=@ProductId


