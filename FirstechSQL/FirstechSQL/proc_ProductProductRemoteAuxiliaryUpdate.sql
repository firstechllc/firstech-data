﻿if object_id('dbo.proc_ProductProductRemoteAuxiliaryUpdate') is not null
    drop proc dbo.proc_ProductProductRemoteAuxiliaryUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductRemoteAuxiliaryUpdate
    @ProductId int,
    @ProductRemoteAuxiliaryIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductRemoteAuxiliaryId int)

    IF @ProductRemoteAuxiliaryIds IS NOT NULL AND @ProductRemoteAuxiliaryIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductRemoteAuxiliaryIds, ',')


    update dbo.ProductProductRemoteAuxiliary 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductRemoteAuxiliary
    (
        ProductId,
        ProductRemoteAuxiliaryId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductRemoteAuxiliaryId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductRemoteAuxiliary b
        on a.ProductRemoteAuxiliaryId = b.ProductRemoteAuxiliaryId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductRemoteAuxiliary b
    join #tbl a
        on a.ProductRemoteAuxiliaryId = b.ProductRemoteAuxiliaryId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductRemoteAuxiliary 
    where Updated=0 
        and ProductId=@ProductId


