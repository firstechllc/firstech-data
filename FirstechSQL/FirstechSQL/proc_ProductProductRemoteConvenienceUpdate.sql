﻿if object_id('dbo.proc_ProductProductRemoteConvenienceUpdate') is not null
    drop proc dbo.proc_ProductProductRemoteConvenienceUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductRemoteConvenienceUpdate
    @ProductId int,
    @ProductRemoteConvenienceIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductRemoteConvenienceId int)

    IF @ProductRemoteConvenienceIds IS NOT NULL AND @ProductRemoteConvenienceIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductRemoteConvenienceIds, ',')


    update dbo.ProductProductRemoteConvenience 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductRemoteConvenience
    (
        ProductId,
        ProductRemoteConvenienceId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductRemoteConvenienceId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductRemoteConvenience b
        on a.ProductRemoteConvenienceId = b.ProductRemoteConvenienceId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductRemoteConvenience b
    join #tbl a
        on a.ProductRemoteConvenienceId = b.ProductRemoteConvenienceId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductRemoteConvenience 
    where Updated=0 
        and ProductId=@ProductId


