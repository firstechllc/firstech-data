﻿if object_id('dbo.proc_ProductProductRemoteProgrammableUpdate') is not null
    drop proc dbo.proc_ProductProductRemoteProgrammableUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductProductRemoteProgrammableUpdate
    @ProductId int,
    @ProductRemoteProgrammableIds varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

if object_id('tempdb..#tbl') is not null
    drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductRemoteProgrammableId int)

    IF @ProductRemoteProgrammableIds IS NOT NULL AND @ProductRemoteProgrammableIds <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductRemoteProgrammableIds, ',')


    update dbo.ProductProductRemoteProgrammable 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductProductRemoteProgrammable
    (
        ProductId,
        ProductRemoteProgrammableId,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductRemoteProgrammableId,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductProductRemoteProgrammable b
        on a.ProductRemoteProgrammableId = b.ProductRemoteProgrammableId and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductProductRemoteProgrammable b
    join #tbl a
        on a.ProductRemoteProgrammableId = b.ProductRemoteProgrammableId and a.ProductId = b.ProductId
    
    delete from dbo.ProductProductRemoteProgrammable 
    where Updated=0 
        and ProductId=@ProductId


