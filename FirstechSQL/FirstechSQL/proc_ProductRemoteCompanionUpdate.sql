﻿if object_id('dbo.proc_ProductRemoteCompanionUpdate') is not null
    drop proc dbo.proc_ProductRemoteCompanionUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductRemoteCompanionUpdate
    @ProductId_Remote int,
    @ProductId_Companions varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Remote int, ProductId_Companion int)

    IF @ProductId_Companions IS NOT NULL AND @ProductId_Companions <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Remote, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Companions, ',')


    update dbo.ProductRemoteCompanion 
    set Updated=0 
    where ProductId_Remote=@ProductId_Remote


    insert into dbo.ProductRemoteCompanion
    (
        ProductId_Remote,
        ProductId_Companion,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Remote,
        a.ProductId_Companion,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductRemoteCompanion b
        on a.ProductId_Companion = b.ProductId_Companion and a.ProductId_Remote = b.ProductId_Remote
    where b.ProductId_Remote is null

    update b
    set b.Updated = 1
    from dbo.ProductRemoteCompanion b
    join #tbl a
        on a.ProductId_Companion = b.ProductId_Companion and a.ProductId_Remote = b.ProductId_Remote
    
    delete from dbo.ProductRemoteCompanion 
    where Updated=0 
        and ProductId_Remote=@ProductId_Remote

