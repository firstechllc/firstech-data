﻿if object_id('dbo.proc_ProductRemoteCompatibleBrainUpdate') is not null
    drop proc dbo.proc_ProductRemoteCompatibleBrainUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductRemoteCompatibleBrainUpdate
    @ProductId_Remote int,
    @ProductId_Brains varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId_Remote int, ProductId_Brain int)

    IF @ProductId_Brains IS NOT NULL AND @ProductId_Brains <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId_Remote, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Brains, ',')


    update dbo.ProductBrainCompatibleRemote 
    set Updated=0 
    where ProductId_Remote=@ProductId_Remote


    insert into dbo.ProductBrainCompatibleRemote
    (
        ProductId_Remote,
        ProductId_Brain,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId_Remote,
        a.ProductId_Brain,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductBrainCompatibleRemote b
        on a.ProductId_Remote = b.ProductId_Remote and a.ProductId_Brain = b.ProductId_Brain
    where b.ProductId_Remote is null

    update b
    set b.Updated = 1
    from dbo.ProductBrainCompatibleRemote b
    join #tbl a
        on a.ProductId_Remote = b.ProductId_Remote and a.ProductId_Brain = b.ProductId_Brain
    
    delete from dbo.ProductBrainCompatibleRemote 
    where Updated=0 
        and ProductId_Remote=@ProductId_Remote

