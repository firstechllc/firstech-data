﻿if object_id('dbo.proc_ProductReplacementPartNumberUpdate') is not null
    drop proc dbo.proc_ProductReplacementPartNumberUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductReplacementPartNumberUpdate
    @ProductId int,
    @ProductId_Replacements varchar(4000),
    @UpdatedBy nvarchar(128) 
AS

    if object_id('tempdb..#tbl') is not null
        drop table #tbl

    create table #tbl (int_idx int, ProductId int, ProductId_Replacement int)

    IF @ProductId_Replacements IS NOT NULL AND @ProductId_Replacements <> ''
        INSERT INTO #tbl
        SELECT int_idx, @ProductId, vch_item
        FROM dbo.fn_util_split_int(@ProductId_Replacements, ',')


    update dbo.ProductReplacementPartNumber 
    set Updated=0 
    where ProductId=@ProductId


    insert into dbo.ProductReplacementPartNumber
    (
        ProductId,
        ProductId_Replacement,

        Updated,
        UpdatedBy,
        UpdatedDt
    )
    select
        a.ProductId,
        a.ProductId_Replacement,

        1,
        @UpdatedBy,
        getdate()
    from #tbl a
    left join dbo.ProductReplacementPartNumber b
        on a.ProductId_Replacement = b.ProductId_Replacement and a.ProductId = b.ProductId
    where b.ProductId is null

    update b
    set b.Updated = 1
    from dbo.ProductReplacementPartNumber b
    join #tbl a
        on a.ProductId_Replacement = b.ProductId_Replacement and a.ProductId = b.ProductId
    
    delete from dbo.ProductReplacementPartNumber 
    where Updated=0 
        and ProductId=@ProductId

