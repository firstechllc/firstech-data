﻿if object_id('dbo.proc_ProductUpdate') is not null
    drop proc dbo.proc_ProductUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductUpdate
    @ProductId int,

    @WayRemote nvarchar(20) = null,
    @TwoWayAntenna nvarchar(20) = null,
    @OneWayAntenna nvarchar(20) = null,
    @Status nvarchar(20) = null,
    @AvailableForWarranty nvarchar(20) = null,
    @BladeCompatible nvarchar(20) = null,
    @DroneCompatible nvarchar(20) = null,
    @Dataport nvarchar(20) = null,
    @OperatingVoltage nvarchar(20) = null,
    @IdleCurrent nvarchar(20) = null,
    @OperatingTempF nvarchar(20) = null,
    @OperatingTempC nvarchar(20) = null,
    @WaterResident nvarchar(20) = null,
    @AntennaPort4Pin nvarchar(20) = null,
    @AntennaPort6Pin nvarchar(20) = null,

    @EstimatedRangeFt nvarchar(20) = null,
    @Battery nvarchar(20) = null,
    @EstimatedBatteryLifeDays nvarchar(50) = null,
    @WaterResistant nvarchar(20) = null,
    @RequiredAntennaCable nvarchar(30) = null,

    @DocumentURL nvarchar(3000) = null,

    @UpdatedBy nvarchar(128) 
AS

    update dbo.Product 
    set 
        WayRemote = @WayRemote,
        TwoWayAntenna = @TwoWayAntenna,
        OneWayAntenna = @OneWayAntenna,
        [Status] = @Status,
        AvailableForWarranty = @AvailableForWarranty,
        BladeCompatible = @BladeCompatible,
        DroneCompatible = @DroneCompatible,
        Dataport = @Dataport,
        OperatingVoltage = @OperatingVoltage,
        IdleCurrent = @IdleCurrent,
        OperatingTempF = @OperatingTempF,
        OperatingTempC = @OperatingTempC,
        WaterResident = @WaterResident,

        EstimatedRangeFt = @EstimatedRangeFt,
        Battery = @Battery,
        EstimatedBatteryLifeDays = @EstimatedBatteryLifeDays,
        WaterResistant = @WaterResistant,
        RequiredAntennaCable = @RequiredAntennaCable,

        DocumentURL = @DocumentURL,

        UpdatedDt = getdate(),
        UpdatedBy = @UpdatedBy

    where ProductId=@ProductId


