﻿if object_id('dbo.proc_ProductWikiSectionDelete') is not null
    drop proc dbo.proc_ProductWikiSectionDelete
go	

CREATE PROCEDURE [dbo].proc_ProductWikiSectionDelete
    @ProductWikiSectionId int,
    @ProductWikiSectionDetailId int
AS

BEGIN TRY
    BEGIN TRAN
        delete from dbo.ProductWikiSectionDetail 
        where ProductWikiSectionDetailId=@ProductWikiSectionDetailId

        if not exists (select * from dbo.ProductWikiSectionDetail where ProductWikiSectionId = @ProductWikiSectionId)
        begin
            delete from dbo.ProductWikiSection
            where ProductWikiSectionId = @ProductWikiSectionId
        end
    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
