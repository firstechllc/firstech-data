﻿
if object_id('dbo.proc_ProductWikiSectionUpDn') is not null
    drop proc dbo.proc_ProductWikiSectionUpDn
go	

CREATE PROCEDURE [dbo].proc_ProductWikiSectionUpDn
    @UpDn int,
    @ProductId int,
    @ProductWikiSectionId int
AS

BEGIN TRY
    BEGIN TRAN
        declare @SortOrder int
        declare @MaxSortOrder int

        select @SortOrder = SortOrder
        from dbo.ProductWikiSection (nolock) 
        where ProductWikiSectionId = @ProductWikiSectionId

        select @MaxSortOrder = max(SortOrder) 
        from dbo.ProductWikiSection (nolock) 
        where ProductId = @ProductId

        if @UpDn = 1  -- Up
        begin
            update dbo.ProductWikiSection 
            set SortOrder = SortOrder + 1
            where ProductId = @ProductId
                and SortOrder = @SortOrder - 1

            update dbo.ProductWikiSection 
            set SortOrder = (case when SortOrder=1 then SortOrder else SortOrder-1 end)
            where ProductWikiSectionId = @ProductWikiSectionId
        end
        else
        begin
            update dbo.ProductWikiSection 
            set SortOrder = SortOrder - 1
            where ProductId = @ProductId
                and SortOrder = @SortOrder + 1


            update dbo.ProductWikiSection 
            set SortOrder = (case when SortOrder = @MaxSortOrder then SortOrder else SortOrder+1 end)
            where ProductWikiSectionId = @ProductWikiSectionId
        end
    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
