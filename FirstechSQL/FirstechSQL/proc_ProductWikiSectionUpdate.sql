﻿if object_id('dbo.proc_ProductWikiSectionUpdate') is not null
    drop proc dbo.proc_ProductWikiSectionUpdate
go	

CREATE PROCEDURE [dbo].proc_ProductWikiSectionUpdate
    @ProductId int,
    @ProductWikiSectionTitle nvarchar(200),
    @ProductWikiSectionInfo ntext,
    @UpdatedBy nvarchar(128),
    @ProductWikiSectionDetailId int = null,
    @ProductWikiSectionId int = null

AS

BEGIN TRY
    BEGIN TRAN
        if @ProductWikiSectionId is null
        begin
            insert into dbo.ProductWikiSection
            (
                ProductId,
                ProductWikiSectionTitle,
                SortOrder,
                UpdatedBy,
                UpdatedDt
            )
            select
                @ProductId,
                @ProductWikiSectionTitle,
                (select isnull(max(SortOrder), 0) + 1 from dbo.ProductWikiSection (nolock) where ProductId = @ProductId),
                @UpdatedBy,
                getdate()

            select @ProductWikiSectionId = SCOPE_IDENTITY()
        end
        else
        begin
            update dbo.ProductWikiSection
            set ProductWikiSectionTitle = @ProductWikiSectionTitle
            where ProductWikiSectionId = @ProductWikiSectionId
        end

        insert into dbo.ProductWikiSectionDetail
        (
            ProductWikiSectionId,
            ProductWikiSectionInfo,
            UpdatedBy,
            UpdatedDt
        )
        select
            @ProductWikiSectionId,
            @ProductWikiSectionInfo,
            @UpdatedBy,
            getdate()

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
