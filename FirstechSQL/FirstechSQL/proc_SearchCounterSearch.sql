﻿if object_id('dbo.proc_SearchCounterSearch') is not null
    drop proc dbo.proc_SearchCounterSearch
go	

CREATE PROCEDURE [dbo].proc_SearchCounterSearch
    @StartDate datetime = null,
    @EndDate datetime = null,
    @Number int,
    @VehicleMakeModelYearId int = null
AS

select *
from (
    select 
        rank() over (order by count(*) + max(v.SearchCount) desc) as Num,
        Vehicle = m.VehicleMakeName + ' ' + o.VehicleModelName  + ' ' + convert(nvarchar, v.VehicleYear),
        Counter = count(*) + max(v.SearchCount),
        VehicleMakeModelYearId = max(v.VehicleMakeModelYearId),
        UpdatedDt = max(e.UpdatedDt),
        ReservedBy = max(v.ReservedBy),
        Reserved = case when dateadd(day, 3, max(v.ReservedAt)) > getdate() then max(au.ProfileName) + ' (Expires at ' + convert(varchar, dateadd(day, 3, max(v.ReservedAt))) + ')' else '' end,
        SearchCount = max(v.SearchCount)
    from dbo.SearchCounter a
    join dbo.VehicleMakeModelYear v WITH (NOLOCK) on v.VehicleMakeModelYearId = a.VehicleMakeModelYearId 
    join dbo.VehicleMake m WITH (NOLOCK) on v.VehicleMakeId = m.VehicleMakeId 
    join dbo.VehicleModel o WITH (NOLOCK) on v.VehicleModelId = o.VehicleModelId
    left join dbo.UserProfile au with (nolock) on v.ReservedBy = au.UserId
    left join (
        select 
            VehicleMakeModelYearId,
            UpdatedDt = max(UpdatedDt)
        from (
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleMakeModelYear with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWireFunction with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWireNote with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWirePrep with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWireDisassembly with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWirePlacementRouting with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWireFacebook with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleComment with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleInternalNote with (NOLOCK)
            union all
            select VehicleMakeModelYearId, UpdatedDt from dbo.VehicleWireProgramming with (NOLOCK)
        ) tbl
        group by VehicleMakeModelYearId
    ) e on e.VehicleMakeModelYearId = a.VehicleMakeModelYearId
    where (@VehicleMakeModelYearId is null and (SearchDt >= @StartDate and SearchDt < dateadd(day, 1, @EndDate)))
        or a.VehicleMakeModelYearId = @VehicleMakeModelYearId

    group by m.VehicleMakeName + ' ' + o.VehicleModelName  + ' ' + convert(nvarchar, v.VehicleYear)
) a
where Num <= @Number
order by 1
