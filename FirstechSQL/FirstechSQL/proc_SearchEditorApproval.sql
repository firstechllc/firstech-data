﻿if object_id('dbo.proc_SearchEditorApproval') is not null
    drop proc dbo.proc_SearchEditorApproval
go	

CREATE PROCEDURE [dbo].proc_SearchEditorApproval
    @StartDate datetime = null,
    @EndDate datetime = null,
    @StatusId int = null
AS

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Wire - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWireFunctionEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)

    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Disassembly - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWireDisassemblyEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)

    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Facebook - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWireFacebookEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)

    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Document - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleDocumentEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)

    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Prep - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWirePrepEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)
    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Routing/Placement - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWirePlacementRoutingEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)
    union all

    select 
        a.EditorWorkId, 
        ew.StatusId,
        ew.EditorWorkDt, 
        au.ProfileName, 
        EditorLog = case EditorWorkAction when 0 then 'Add' when 1 then 'Edit' when 2 then 'Delete' end + ' Programming - ' + '<a href="/Admin/AdminVehicleInfo?Id=' + convert(varchar, a.VehicleMakeModelYearId) + '">vehicle</a>',
        StatusName = case StatusId when 0 then 'Draft' when 1 then 'Approved' when 2 then 'Approved' when 3 then 'Denied' end,
        ew.EditorWorkDt
    from dbo.VehicleWireProgrammingEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    where (@StatusId is null or (@StatusId=1 and ew.StatusId in (1, 2)) or ew.StatusId = @StatusId)
        and (@StartDate is null or EditorWorkDt >= @StartDate)
        and (@EndDate is null or EditorWorkDt < @EndDate)


    order by ew.EditorWorkDt desc

