﻿if object_id('dbo.proc_SearchPaidout') is not null
    drop proc dbo.proc_SearchPaidout
go	

CREATE PROCEDURE [dbo].proc_SearchPaidout
    @StartDate datetime = null,
    @EndDate datetime = null
AS

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWireFunctionEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)

    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWireDisassemblyEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)



    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWireFacebookEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)


    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.AttachFile, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleDocumentEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)

    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWirePrepEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)


    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWirePlacementRoutingEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)


    union all

    select 
        '' as ID,
        au.EditorMTCRepcode,
        'FTD' + REPLICATE('0',10-LEN(ew.EditorWorkId)) + convert(varchar, ew.EditorWorkId) as InvoiceNumber,
        anu.Email,
        'Secondary' as AccountType,
        'FTD Submitted Information' as SubscriptionPlan,
        'Cash' as RewardType,
        case when isnull(a.Attach1, '') != '' or isnull(a.Attach2, '') != '' or isnull(a.Attach3, '') != '' or isnull(a.Attach4, '') != '' or isnull(a.Attach5, '') != '' then 2 else 1 end as RewardAmount,
        ew.ApprovedDt as AddedDate,
        EditorWorkDt
    from dbo.VehicleWireProgrammingEditor a 
    join dbo.EditorWork ew on ew.EditorWorkId = a.EditorWorkId
    join dbo.UserProfile au with (nolock) on ew.EditorId = au.UserId
    join dbo.AspNetUsers anu with (NOLOCK) on anu.Id = ew.EditorId
    where ew.StatusId in (1, 2)
        and (@StartDate is null or ew.ApprovedDt >= @StartDate)
        and (@EndDate is null or ew.ApprovedDt < @EndDate)



    order by EditorWorkDt asc

