﻿if object_id('dbo.proc_UserProfileLoad') is not null
    drop proc dbo.proc_UserProfileLoad
go	

CREATE PROCEDURE [dbo].proc_UserProfileLoad
    @UserId nvarchar(128)
AS

if not exists (select * from dbo.UserProfile where UserId = @UserId)
begin
    insert into dbo.UserProfile
    (
        UserId,
        ProfileName,
        ProfilePicture,
        Country,
        WhereBuyProduct
    )
    select
        @UserId,
        FirstName + '@' + isnull(StoreName, ''),
        '',
        '',
        ''
    from dbo.AspNetUsers
    where Id = @UserId
end

select 
    ProfileName,
    ProfilePicture,
    Country,
    EditorMTCRepcode = isnull(EditorMTCRepcode, ''),
    WhereBuyProduct = isnull(WhereBuyProduct, '')
from dbo.UserProfile 
where UserId = @UserId



