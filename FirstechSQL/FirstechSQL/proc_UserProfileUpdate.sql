﻿if object_id('dbo.proc_UserProfileUpdate') is not null
    drop proc dbo.proc_UserProfileUpdate
go	

CREATE PROCEDURE [dbo].proc_UserProfileUpdate
    @UserId nvarchar(128),
    @ProfileName nvarchar(200),
    @ProfilePicture nvarchar(500) = '',
    @Country nvarchar(50) = '',
    @EditorMTCRepcode nvarchar(100) = null,
    @WhereBuyProduct nvarchar(100) = null
AS


if not exists (select * from dbo.UserProfile where UserId = @UserId)
begin
    insert into dbo.UserProfile
    (
        UserId,
        ProfileName,
        ProfilePicture,
        Country,
        WhereBuyProduct
    )
    select
        @UserId,
        @ProfileName,
        @ProfilePicture,
        @Country,
        @WhereBuyProduct
end
else
begin
    update dbo.UserProfile
    set ProfileName = @ProfileName,
        ProfilePicture = @ProfilePicture,
        Country = @Country,
        WhereBuyProduct = @WhereBuyProduct
    where UserId = @UserId

    if @EditorMTCRepcode is not null
    begin
        update dbo.UserProfile
        set EditorMTCRepcode = @EditorMTCRepcode
        where UserId = @UserId
    end
end

