﻿if object_id('dbo.proc_VehicleCommentLoad') is not null
    drop proc dbo.proc_VehicleCommentLoad
go	

CREATE PROCEDURE [dbo].proc_VehicleCommentLoad
    @VehicleMakeModelYearId int,
    @UserId nvarchar(128) = null,
    @ParentVehicleCommentId int = 0
AS

select 
    a.VehicleCommentId, 
    a.Comment, 
    p.ProfileName,
    p.ProfilePicture, 
    p.RegistrationDate,
    p.NoOfPosts,
    a.UpdatedDt,
    a.likecount,
    a.dislikecount,
    LikeDisLike = isnull(l.LikeDisLike, -1),
    a.NoOfReplies,
    a.ParentVehicleCommentId,
    r.RankName,
    a.UpdatedBy,
    a.IsReported,
    a.ImgFilename
from dbo.VehicleComment a WITH (NOLOCK)
join dbo.AspNetUsers b WITH (NOLOCK) on a.UpdatedBy = b.Id 
join dbo.UserProfile p WITH (NOLOCK) on b.Id = p.UserId
left join dbo.Rank r with (NOLOCK) on r.RankId = p.RankId
left join dbo.VehicleCommentLike l WITH (NOLOCK) on l.VehicleCommentId = a.VehicleCommentId and l.UserId = isnull(@UserId, '')
where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
    and a.ParentVehicleCommentId = @ParentVehicleCommentId
order by a.UpdatedDt desc 

