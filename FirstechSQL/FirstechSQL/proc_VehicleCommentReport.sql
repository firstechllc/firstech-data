﻿if object_id('dbo.proc_VehicleCommentReport') is not null
    drop proc dbo.proc_VehicleCommentReport
go	

CREATE PROCEDURE [dbo].proc_VehicleCommentReport
    @UserId nvarchar(128),
    @VehicleCommentId int
AS
BEGIN TRY
    BEGIN TRAN
    
    if not exists (select * from dbo.VehicleCommentReport where UserId=@UserId and VehicleCommentId=@VehicleCommentId)
        insert into dbo.VehicleCommentReport
        (UserId, VehicleCommentId, UpdatedDt)
        select @UserId, @VehicleCommentId, getdate()

    update dbo.VehicleComment
    set IsReported = 1
    where VehicleCommentId = @VehicleCommentId

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
