﻿if object_id('dbo.proc_VehicleCommentSave') is not null
    drop proc dbo.proc_VehicleCommentSave
go	

CREATE PROCEDURE [dbo].proc_VehicleCommentSave
    @VehicleMakeModelYearId int,
    @Comment nvarchar(max),
    @UpdatedBy nvarchar(128),
    @ParentVehicleCommentId int,
    @OriginalImgFilename nvarchar(500) = '',
    @ImgFilename nvarchar(500) = ''
AS

BEGIN TRY
    BEGIN TRAN
        insert into dbo.VehicleComment (
            VehicleMakeModelYearId, 
            Comment, 
            UpdatedDt, 
            UpdatedBy, 
            ParentVehicleCommentId,
            OriginalImgFilename,
            ImgFilename
        ) 
        select @VehicleMakeModelYearId, 
            @Comment, 
            getdate(), 
            @UpdatedBy, 
            @ParentVehicleCommentId,
            @OriginalImgFilename,
            @ImgFilename;

        if @ParentVehicleCommentId > 0
        begin
            update dbo.VehicleComment
            set NoOfReplies = isnull(NoOfReplies, 0) + 1
            where VehicleCommentId = @ParentVehicleCommentId
        end

        declare @RankId int
        declare @NoOfPosts int = 1

        select @NoOfPosts = isnull(NoOfPosts, 0) + 1
        from dbo.UserProfile
        where UserId = @UpdatedBy

        select @RankId = RankId 
        from dbo.Rank with (NOLOCK)
        where @NoOfPosts between MinHit and MaxHit

        update dbo.UserProfile
        set NoOfPosts = @NoOfPosts, RankId = @RankId
        where UserId = @UpdatedBy

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
