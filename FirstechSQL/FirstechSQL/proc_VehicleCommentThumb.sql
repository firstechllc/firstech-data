﻿if object_id('dbo.proc_VehicleCommentThumb') is not null
    drop proc dbo.proc_VehicleCommentThumb
go	

CREATE PROCEDURE [dbo].proc_VehicleCommentThumb
    @UserId nvarchar(128),
    @VehicleCommentId int,
    @LikeDisLike int  -- 1: Like, 0: Dislike
AS
BEGIN TRY
    BEGIN TRAN
    
    declare @CurrentLikeDisLike int = null

    select @CurrentLikeDisLike = LikeDisLike 
    from dbo.VehicleCommentLike 
    where UserId = @UserId 
        and VehicleCommentId = @VehicleCommentId

    if @CurrentLikeDisLike is not null
    begin
        if @CurrentLikeDisLike != @LikeDisLike
        begin
            update dbo.VehicleCommentLike
            set LikeDisLike = @LikeDisLike,
                UpdatedDt = getdate()
            where UserId = @UserId
                and VehicleCommentId = @VehicleCommentId

            if @LikeDisLike = 1
            begin
                update dbo.VehicleComment
                set likecount = likecount + 1,
                    dislikecount = dislikecount - 1
                where VehicleCommentId = @VehicleCommentId
            end
            else
            begin
                update dbo.VehicleComment
                set likecount = likecount - 1,
                    dislikecount = dislikecount + 1
                where VehicleCommentId = @VehicleCommentId
            end
        end
        else
        begin
            delete dbo.VehicleCommentLike
            where UserId = @UserId
                and VehicleCommentId = @VehicleCommentId

            if @LikeDisLike = 1
            begin
                update dbo.VehicleComment
                set likecount = likecount - 1
                where VehicleCommentId = @VehicleCommentId
            end
            else
            begin
                update dbo.VehicleComment
                set dislikecount = dislikecount - 1
                where VehicleCommentId = @VehicleCommentId
            end
        end
    end
    else
    begin
        insert into dbo.VehicleCommentLike
        (UserId, VehicleCommentId, LikeDisLike, UpdatedDt)
        select @UserId, @VehicleCommentId, @LikeDisLike, getdate()

        if @LikeDisLike = 1
        begin
            update dbo.VehicleComment
            set likecount = likecount + 1
            where VehicleCommentId = @VehicleCommentId
        end
        else
        begin
            update dbo.VehicleComment
            set dislikecount = dislikecount + 1
            where VehicleCommentId = @VehicleCommentId
        end
    end

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
