﻿if object_id('dbo.proc_VehicleCommentUpdate') is not null
    drop proc dbo.proc_VehicleCommentUpdate
go	

CREATE PROCEDURE [dbo].proc_VehicleCommentUpdate
    @VehicleCommentId int,
    @Comment nvarchar(max),
    @UpdatedBy nvarchar(128)
AS

BEGIN TRY
    update dbo.VehicleComment 
    set Comment = @Comment,
        UpdatedDt = getdate()
    where VehicleCommentId = @VehicleCommentId

    RETURN 1
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
