﻿if object_id('dbo.proc_VehicleDocumentSearch') is not null
    drop proc dbo.proc_VehicleDocumentSearch
go	

CREATE PROCEDURE [dbo].proc_VehicleDocumentSearch
    @VehicleMakeModelYearId int
AS
select *
from 
(
    -- Linked Vehicle Only
    select a2.VehicleDocumentId, a2.DocumentName, a2.AttachFile
    from dbo.VehicleLink b2 WITH (NOLOCK) 
    join dbo.VehicleDocument a2 WITH (NOLOCK) on a2.VehicleMakeModelYearId = b2.LinkVehicleMakeModelYearId
    where b2.VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    -- This Vehicle 
    select a.VehicleDocumentId, a.DocumentName, a.AttachFile
    from dbo.VehicleDocument a WITH (NOLOCK) 
    where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
) tbl

