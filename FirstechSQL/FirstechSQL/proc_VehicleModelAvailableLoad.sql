﻿if object_id('dbo.proc_VehicleModelAvailableLoad') is not null
    drop proc dbo.proc_VehicleModelAvailableLoad
go	

CREATE PROCEDURE [dbo].proc_VehicleModelAvailableLoad
    @VehicleMakeId int,
    @VehicleYear int
AS

    select a.VehicleModelId, VehicleModelName  
    from dbo.VehicleMakeModelYear a WITH (NOLOCK)  
    join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  
    join (
        select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK) where InstallationTypeId in (select InstallationTypeId from InstallationType with (NOLOCK) where Inactive = 0)
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireNote with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWirePrep with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireDisassembly with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWirePlacementRouting with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireFacebook with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleComment with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleInternalNote with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireProgramming with (NOLOCK) 
    ) w on w.VehicleMakeModelYearId=a.VehicleMakeModelYearId  
    where a.VehicleMakeId = @VehicleMakeId 
        and a.VehicleYear = @VehicleYear
        and a.Approved = 1
   
    union 
       
    select a.VehicleModelId, VehicleModelName  
    from dbo.VehicleMakeModelYear a WITH (NOLOCK)  
    join dbo.VehicleModel b WITH(NOLOCK) on a.VehicleModelId = b.VehicleModelId  
    join dbo.VehicleLink l with (NOLOCK) on l.VehicleMakeModelYearId = a.VehicleMakeModelYearId 
    join (
        select distinct VehicleMakeModelYearId from dbo.VehicleWireFunction WITH (NOLOCK) where InstallationTypeId in (select InstallationTypeId from InstallationType with (NOLOCK) where Inactive = 0)
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireNote with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWirePrep with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireDisassembly with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWirePlacementRouting with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireFacebook with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleComment with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleInternalNote with (NOLOCK) 
        union select distinct VehicleMakeModelYearId from dbo.VehicleWireProgramming with (NOLOCK) 
    ) w on w.VehicleMakeModelYearId=l.LinkVehicleMakeModelYearId  
    where a.VehicleMakeId = @VehicleMakeId 
        and a.VehicleYear = @VehicleYear
        and a.Approved = 1
    order by VehicleModelName  

