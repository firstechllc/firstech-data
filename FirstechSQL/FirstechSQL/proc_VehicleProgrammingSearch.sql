﻿if object_id('dbo.proc_VehicleProgrammingSearch') is not null
    drop proc dbo.proc_VehicleProgrammingSearch
go	

CREATE PROCEDURE [dbo].proc_VehicleProgrammingSearch
    @SaveFolder nvarchar(500),
    @VehicleMakeModelYearId int
AS
select *
from 
(
    -- Linked Vehicle Only
    select a2.Step, a2.Note, 
        case when a2.Attach1 is not null then case when left(a2.Attach1, 7) = 'http://' or left(a2.Attach1, 8) = 'https://' then a2.Attach1 when a2.Attach1 <> '' then @SaveFolder + a2.Attach1 else '' end else '' end as Attach1,
        case when a2.Attach2 is not null then case when left(a2.Attach2, 7) = 'http://' or left(a2.Attach2, 8) = 'https://' then a2.Attach2 when a2.Attach2 <> '' then @SaveFolder + a2.Attach2 else '' end else '' end as Attach2,
        case when a2.Attach3 is not null then case when left(a2.Attach3, 7) = 'http://' or left(a2.Attach3, 8) = 'https://' then a2.Attach3 when a2.Attach3 <> '' then @SaveFolder + a2.Attach3 else '' end else '' end as Attach3,
        case when a2.Attach4 is not null then case when left(a2.Attach4, 7) = 'http://' or left(a2.Attach4, 8) = 'https://' then a2.Attach4 when a2.Attach4 <> '' then @SaveFolder + a2.Attach4 else '' end else '' end as Attach4,
        case when a2.Attach5 is not null then case when left(a2.Attach5, 7) = 'http://' or left(a2.Attach5, 8) = 'https://' then a2.Attach5 when a2.Attach5 <> '' then @SaveFolder + a2.Attach5 else '' end else '' end as Attach5
    from dbo.VehicleLink b2 WITH (NOLOCK) 
    join dbo.VehicleWireProgramming a2 WITH (NOLOCK) on a2.VehicleMakeModelYearId = b2.LinkVehicleMakeModelYearId
    left join (
        select a.Step
        from dbo.VehicleWireProgramming a WITH (NOLOCK)
        where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
    ) d on d.Step = a2.Step
    where b2.VehicleMakeModelYearId = @VehicleMakeModelYearId
        and d.Step is null

    union all

    -- This Vehicle 
    select a.Step, a.Note, 
        case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then @SaveFolder + a.Attach1 else '' end else '' end as Attach1,
        case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then @SaveFolder + a.Attach2 else '' end else '' end as Attach2,
        case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then @SaveFolder + a.Attach3 else '' end else '' end as Attach3,
        case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then @SaveFolder + a.Attach4 else '' end else '' end as Attach4,
        case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then @SaveFolder + a.Attach5 else '' end else '' end as Attach5
    from dbo.VehicleWireProgramming a WITH (NOLOCK) 
    where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
) tbl
order by Step

