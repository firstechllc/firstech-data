﻿if object_id('dbo.proc_VehicleUpdateDate') is not null
    drop proc dbo.proc_VehicleUpdateDate
go	

CREATE PROCEDURE [dbo].proc_VehicleUpdateDate
    @VehicleMakeModelYearId int
AS

select max(UpdatedDt) as UpdatedDt
from 
(
    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWireFunction WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWireNote WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWirePrep WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWireDisassembly WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWirePlacementRouting WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWireFacebook WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleUrgentMessage WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleInternalNote WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    union all

    select max(UpdatedDt) as UpdatedDt
    from dbo.VehicleWireProgramming WITH (NOLOCK) 
    where VehicleMakeModelYearId = @VehicleMakeModelYearId

    
) tbl

