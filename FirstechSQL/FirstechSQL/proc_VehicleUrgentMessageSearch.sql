﻿if object_id('dbo.proc_VehicleUrgentMessageSearch') is not null
    drop proc dbo.proc_VehicleUrgentMessageSearch
go	

CREATE PROCEDURE [dbo].proc_VehicleUrgentMessageSearch
    @VehicleMakeModelYearId int
AS

if exists (select * from dbo.VehicleUrgentMessage WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId)
    and exists (select * from dbo.VehicleUrgentMessage WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId and Note is not null and rtrim(Note) <> '')
    select 
        VehicleUrgentMessageId, 
        Note 
    from dbo.VehicleUrgentMessage WITH (NOLOCK)
    where VehicleMakeModelYearId = @VehicleMakeModelYearId
else
    select 
        VehicleUrgentMessageId, 
        Note 
    from dbo.VehicleUrgentMessage m WITH (NOLOCK)
    join dbo.VehicleLink l on m.VehicleMakeModelYearId = l.LinkVehicleMakeModelYearId
    where l.VehicleMakeModelYearId = @VehicleMakeModelYearId

insert into dbo.SearchCounter (VehicleMakeModelYearId) select @VehicleMakeModelYearId
