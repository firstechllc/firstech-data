﻿if object_id('dbo.proc_VehicleWireNoteSearch') is not null
    drop proc dbo.proc_VehicleWireNoteSearch
go	

CREATE PROCEDURE [dbo].proc_VehicleWireNoteSearch
    @VehicleMakeModelYearId int,
    @InstallationTypeId int
AS

if exists (select * from dbo.VehicleWireNote WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId and InstallationTypeId = @InstallationTypeId)
    and exists (select * from dbo.VehicleWireNote WITH (NOLOCK) where VehicleMakeModelYearId = @VehicleMakeModelYearId and InstallationTypeId = @InstallationTypeId and Note is not null and rtrim(Note) <> '')
    select 
        VehicleWireNoteId, 
        Note 
    from dbo.VehicleWireNote WITH (NOLOCK)
    where VehicleMakeModelYearId = @VehicleMakeModelYearId
        and InstallationTypeId = @InstallationTypeId
else
    select 
        VehicleWireNoteId, 
        Note 
    from dbo.VehicleWireNote m WITH (NOLOCK)
    join dbo.VehicleLink l on m.VehicleMakeModelYearId = l.LinkVehicleMakeModelYearId
    where l.VehicleMakeModelYearId = @VehicleMakeModelYearId
        and m.InstallationTypeId = @InstallationTypeId

