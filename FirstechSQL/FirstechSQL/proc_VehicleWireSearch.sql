﻿if object_id('dbo.proc_VehicleWireSearch') is not null
    drop proc dbo.proc_VehicleWireSearch
go	

CREATE PROCEDURE [dbo].proc_VehicleWireSearch
    @SaveFolder nvarchar(500),
    @VehicleMakeModelYearId int,
    @InstallationTypeId int = null,
    @UserId nvarchar(128) = null
AS
select *
from 
(
    -- Linked Vehicle Only
    select c2.WireFunctionName, d2.InstallationTypeName, a2.Colour, a2.Location, a2.Polarity, a2.VehicleColor, a2.PinOut, c2.SortOrder,
        case when a2.Attach1 is not null then case when left(a2.Attach1, 7) = 'http://' or left(a2.Attach1, 8) = 'https://' then a2.Attach1 when a2.Attach1 <> '' then @SaveFolder + a2.Attach1 else '' end else '' end as Attach1,
        case when a2.Attach2 is not null then case when left(a2.Attach2, 7) = 'http://' or left(a2.Attach2, 8) = 'https://' then a2.Attach2 when a2.Attach2 <> '' then @SaveFolder + a2.Attach2 else '' end else '' end as Attach2,
        case when a2.Attach3 is not null then case when left(a2.Attach3, 7) = 'http://' or left(a2.Attach3, 8) = 'https://' then a2.Attach3 when a2.Attach3 <> '' then @SaveFolder + a2.Attach3 else '' end else '' end as Attach3,
        case when a2.Attach4 is not null then case when left(a2.Attach4, 7) = 'http://' or left(a2.Attach4, 8) = 'https://' then a2.Attach4 when a2.Attach4 <> '' then @SaveFolder + a2.Attach4 else '' end else '' end as Attach4,
        case when a2.Attach5 is not null then case when left(a2.Attach5, 7) = 'http://' or left(a2.Attach5, 8) = 'https://' then a2.Attach5 when a2.Attach5 <> '' then @SaveFolder + a2.Attach5 else '' end else '' end as Attach5,
        a2.likecount, a2.dislikecount, a2.VehicleWireFunctionId, LikeDisLike = isnull(l.LikeDisLike, -1)
    from dbo.VehicleLink b2 WITH (NOLOCK) 
    join dbo.VehicleWireFunction a2 WITH (NOLOCK) on a2.VehicleMakeModelYearId = b2.LinkVehicleMakeModelYearId
    join dbo.WireFunction c2 WITH (NOLOCK) on a2.WireFunctionId = c2.WireFunctionId and a2.WireFunctionId=a2.WireFunctionId
    left join dbo.InstallationType d2 WITH (NOLOCK) on a2.InstallationTypeId = d2.InstallationTypeId and d2.InstallationTypeId=d2.InstallationTypeId
    left join (
        select a.WireFunctionId, a.InstallationTypeId
        from dbo.VehicleWireFunction a WITH (NOLOCK)
        join dbo.WireFunction c WITH (NOLOCK) on a.WireFunctionId = c.WireFunctionId 
        where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
    ) d on d.WireFunctionId = a2.WireFunctionId and d.InstallationTypeId = a2.InstallationTypeId
    left join dbo.VehicleWireFunctionLike l on l.VehicleWireFunctionId = a2.VehicleWireFunctionId and l.UserId = isnull(@UserId, '')
    where b2.VehicleMakeModelYearId = @VehicleMakeModelYearId
        and d.WireFunctionId is null
        and (@InstallationTypeId is null or a2.InstallationTypeId = @InstallationTypeId)

    union all

    -- This Vehicle 
    select c.WireFunctionName, d.InstallationTypeName, a.Colour, a.Location, a.Polarity, a.VehicleColor, a.PinOut, c.SortOrder,
        case when a.Attach1 is not null then case when left(a.Attach1, 7) = 'http://' or left(a.Attach1, 8) = 'https://' then a.Attach1 when a.Attach1 <> '' then @SaveFolder + a.Attach1 else '' end else '' end as Attach1,
        case when a.Attach2 is not null then case when left(a.Attach2, 7) = 'http://' or left(a.Attach2, 8) = 'https://' then a.Attach2 when a.Attach2 <> '' then @SaveFolder + a.Attach2 else '' end else '' end as Attach2,
        case when a.Attach3 is not null then case when left(a.Attach3, 7) = 'http://' or left(a.Attach3, 8) = 'https://' then a.Attach3 when a.Attach3 <> '' then @SaveFolder + a.Attach3 else '' end else '' end as Attach3,
        case when a.Attach4 is not null then case when left(a.Attach4, 7) = 'http://' or left(a.Attach4, 8) = 'https://' then a.Attach4 when a.Attach4 <> '' then @SaveFolder + a.Attach4 else '' end else '' end as Attach4,
        case when a.Attach5 is not null then case when left(a.Attach5, 7) = 'http://' or left(a.Attach5, 8) = 'https://' then a.Attach5 when a.Attach5 <> '' then @SaveFolder + a.Attach5 else '' end else '' end as Attach5,
        a.likecount, a.dislikecount, a.VehicleWireFunctionId, LikeDisLike = isnull(l.LikeDisLike, -1)
    from dbo.VehicleWireFunction a WITH (NOLOCK) 
    join dbo.VehicleMakeModelYear b WITH (NOLOCK) on a.VehicleMakeModelYearId = b.VehicleMakeModelYearId 
    join dbo.WireFunction c WITH (NOLOCK) on a.WireFunctionId = c.WireFunctionId 
    left join dbo.InstallationType d WITH (NOLOCK) on a.InstallationTypeId = d.InstallationTypeId 
    left join dbo.VehicleWireFunctionLike l on l.VehicleWireFunctionId = a.VehicleWireFunctionId and l.UserId = isnull(@UserId, '')
    where a.VehicleMakeModelYearId = @VehicleMakeModelYearId
        and (@InstallationTypeId is null or a.InstallationTypeId = @InstallationTypeId)

) tbl
order by SortOrder 

