﻿if object_id('dbo.proc_VehicleWireThumb') is not null
    drop proc dbo.proc_VehicleWireThumb
go	

CREATE PROCEDURE [dbo].proc_VehicleWireThumb
    @UserId nvarchar(128),
    @VehicleWireFunctionId int,
    @LikeDisLike int  -- 1: Like, 0: Dislike
AS
BEGIN TRY
    BEGIN TRAN
    
    declare @CurrentLikeDisLike int = null

    select @CurrentLikeDisLike = LikeDisLike 
    from dbo.VehicleWireFunctionLike 
    where UserId = @UserId 
        and VehicleWireFunctionId = @VehicleWireFunctionId

    if @CurrentLikeDisLike is not null
    begin
        if @CurrentLikeDisLike != @LikeDisLike
        begin
            update dbo.VehicleWireFunctionLike
            set LikeDisLike = @LikeDisLike,
                UpdatedDt = getdate()
            where UserId = @UserId
                and VehicleWireFunctionId = @VehicleWireFunctionId

            if @LikeDisLike = 1
            begin
                update dbo.VehicleWireFunction
                set likecount = likecount + 1,
                    dislikecount = dislikecount - 1
                where VehicleWireFunctionId = @VehicleWireFunctionId
            end
            else
            begin
                update dbo.VehicleWireFunction
                set likecount = likecount - 1,
                    dislikecount = dislikecount + 1
                where VehicleWireFunctionId = @VehicleWireFunctionId
            end
        end
        else
        begin
            delete dbo.VehicleWireFunctionLike
            where UserId = @UserId
                and VehicleWireFunctionId = @VehicleWireFunctionId

            if @LikeDisLike = 1
            begin
                update dbo.VehicleWireFunction
                set likecount = likecount - 1
                where VehicleWireFunctionId = @VehicleWireFunctionId
            end
            else
            begin
                update dbo.VehicleWireFunction
                set dislikecount = dislikecount - 1
                where VehicleWireFunctionId = @VehicleWireFunctionId
            end
        end
    end
    else
    begin
        insert into dbo.VehicleWireFunctionLike
        (UserId, VehicleWireFunctionId, LikeDisLike, UpdatedDt)
        select @UserId, @VehicleWireFunctionId, @LikeDisLike, getdate()

        if @LikeDisLike = 1
        begin
            update dbo.VehicleWireFunction
            set likecount = likecount + 1
            where VehicleWireFunctionId = @VehicleWireFunctionId
        end
        else
        begin
            update dbo.VehicleWireFunction
            set dislikecount = dislikecount + 1
            where VehicleWireFunctionId = @VehicleWireFunctionId
        end
    end

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
