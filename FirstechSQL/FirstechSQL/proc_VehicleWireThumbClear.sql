﻿if object_id('dbo.proc_VehicleWireThumbClear') is not null
    drop proc dbo.proc_VehicleWireThumbClear
go	

CREATE PROCEDURE [dbo].proc_VehicleWireThumbClear
    @VehicleWireFunctionId int
AS
BEGIN TRY
    BEGIN TRAN

    delete
    from dbo.VehicleWireFunctionLike 
    where LikeDisLike = 0
        and VehicleWireFunctionId = @VehicleWireFunctionId

    update dbo.VehicleWireFunction
    set dislikecount = 0
    where VehicleWireFunctionId = @VehicleWireFunctionId

    COMMIT TRAN;

    RETURN 1
END TRY
BEGIN CATCH
    ROLLBACK TRAN;

    DECLARE @ErrorMessage NVARCHAR(4000)
    SELECT @ErrorMessage = ERROR_MESSAGE();
    RAISERROR (
            @ErrorMessage
            ,16
            ,1
            );

    RETURN 0
END CATCH
GO
